﻿#include "pch.h"
#include "extern.h"


// CPlasmaBookDlg 대화 상자

IMPLEMENT_DYNAMIC(CPlasmaBookDlg, CDialogEx)

CPlasmaBookDlg::CPlasmaBookDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PLASMA_BOOK_DIALOG, pParent)
	, m_strBookHour(_T("00"))
	, m_strBookMinute(_T("00"))
{
	m_hSetBookTimeEvent = NULL;

	m_hSetBookTimeEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
}

CPlasmaBookDlg::~CPlasmaBookDlg()
{
	CloseHandle(m_hSetBookTimeEvent);
	m_hSetBookTimeEvent = NULL;
}

void CPlasmaBookDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_BOOK_EDIT_HOUR, m_strBookHour);
	DDX_Text(pDX, IDC_BOOK_EDIT_MINUTE, m_strBookMinute);
}


BEGIN_MESSAGE_MAP(CPlasmaBookDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BOOK_BUTTON, &CPlasmaBookDlg::OnBnClickedBookButton)
	ON_NOTIFY(UDN_DELTAPOS, IDC_BOOK_SPIN_MINUTE, &CPlasmaBookDlg::OnDeltaposBookSpinMinute)
	ON_NOTIFY(UDN_DELTAPOS, IDC_BOOK_SPIN_HOUR, &CPlasmaBookDlg::OnDeltaposBookSpinHour)
END_MESSAGE_MAP()


// CPlasmaBookDlg 메시지 처리기
BOOL CPlasmaBookDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	((CSpinButtonCtrl*)GetDlgItem(IDC_BOOK_SPIN_HOUR))->SetRange(0, 23);
	((CSpinButtonCtrl*)GetDlgItem(IDC_BOOK_SPIN_MINUTE))->SetRange(0, 59);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CPlasmaBookDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CPlasmaBookDlg::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::DestroyWindow();
}

void CPlasmaBookDlg::OnBnClickedBookButton()
{
	this->ShowWindow(SW_HIDE);
	SetEvent(m_hSetBookTimeEvent);
}

void CPlasmaBookDlg::OnDeltaposBookSpinHour(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString strTemp = "";
	UpdateData(TRUE);
	pNMUpDown->iPos = _ttoi(m_strBookHour);
	int nValue = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nValue > -1) && (nValue < 24))
	{
		m_strBookHour.Format(_T("%02d"), nValue);
		UpdateData(FALSE);
		*pResult = 0;
	}
}

void CPlasmaBookDlg::OnDeltaposBookSpinMinute(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString strTemp = "";
	UpdateData(TRUE);
	pNMUpDown->iPos = _ttoi(m_strBookMinute);
	int nValue = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nValue > -1) && (nValue < 60))
	{
		m_strBookMinute.Format(_T("%02d"), nValue);
		UpdateData(FALSE);
		*pResult = 0;
	}
}
