#pragma once
class CBISSCommunication: public CSerialCom
{
public :
	CBISSCommunication();
	~CBISSCommunication();

	//int m_nSetWatt;
	//int m_nSetHour;
	//int m_nSetMinute;
	//int m_nSetSecond;
	//int m_nActualWatt;
	//int m_nActualWattR;
	//int m_nActualHour;
	//int m_nActualMinute;
	//int m_nActualSecond;
	//double m_dActualVDC;
	//double m_dActualTorr;

	bool m_bOldPowerStatus;
	bool m_bisPowerON;

	virtual int	OpenPort(CString sPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity);
	virtual int	SendData(int nSize, char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual	int	ReceiveData(char *lParam, DWORD dwRead); ///< Recive

	HANDLE m_hSetWattEvent;
	HANDLE m_hSetHourEvent;
	HANDLE m_hSetMinuteEvent;
	HANDLE m_hGetSetParameterEvent;
	HANDLE m_hGetActualParameterEvent;
	HANDLE m_hPowerOnEvent;
	HANDLE m_hPowerOffEvent;

	int sendPowerOn();
	int sendPowerOff();
	int sendSetParameterWatt(int watt);
	int sendSetParameterHour(int hour);
	int sendSetParameterMinute(int minute);
	int sendGetParameterSet();
	int sendGetParameterAct();
	int sendGetParameterVDC();
	int sendGetParameterTorr();
	int sendGetParameterPower();
	int openBISSController();
};

