﻿#pragma once


// CPlasmaBookDlg 대화 상자

class CPlasmaBookDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CPlasmaBookDlg)

public:
	CPlasmaBookDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CPlasmaBookDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PLASMA_BOOK_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBookButton();
	afx_msg void OnDeltaposBookSpinHour(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposBookSpinMinute(NMHDR *pNMHDR, LRESULT *pResult);
	CString m_strBookHour;
	CString m_strBookMinute;
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL DestroyWindow();
	HANDLE m_hSetBookTimeEvent;
};
