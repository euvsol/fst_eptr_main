﻿// CPlasmaCleaner.cpp: 구현 파일
//

#include "pch.h"
#include "Plasma_cleaning.h"
#include "afxdialogex.h"


// CPlasmaCleaner 대화 상자

IMPLEMENT_DYNAMIC(CPlasmaCleanerDlg, CDialogEx)

CPlasmaCleanerDlg::CPlasmaCleanerDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PLASMA_CLEANER_DIALOG, pParent)
	, m_strDispSetWatt(_T("50"))
	, m_strDispSetHour(_T("03"))
	, m_strDispSetMinute(_T("00"))
	, m_strDispSetSecond(_T("00"))
	, m_strDispActualWatt(_T("00"))
	, m_strDispActualWattR(_T("00"))
	, m_strDispActualHour(_T("00"))
	, m_strDispActualMinute(_T("00"))
	, m_strDispActualSecond(_T("00"))
	, m_strDispActualVDC(_T("0.0"))
	, m_strDispActualTorr(_T("0e0"))
	, m_strDispBookHour(_T("00"))
	, m_strDispBookMinute(_T("00"))
{
	m_pRunThread = NULL;
	m_pUpdateThread = NULL;
	m_pToggleThread = NULL;

	m_bisRunThreadExitFlag = FALSE;
	m_bisToggleThreadExitFlag = FALSE;
	m_bisOpenSerialThreadExitFlag = FALSE;

	m_bisManual = FALSE;

	m_nActualWatt = 0; 
	m_nActualWattR = 0;
	m_nActualHour = 0;
	m_nActualMinute = 0;
	m_nActualSecond = 0;
	m_dActualVDC = 0;
	m_dActualTorr = 0;
}

CPlasmaCleanerDlg::~CPlasmaCleanerDlg()
{



}

void CPlasmaCleanerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PLASMA_EDIT_SETWATT, m_strDispSetWatt);
	DDX_Text(pDX, IDC_PLASMA_EDIT_SETHOUR, m_strDispSetHour);
	DDX_Text(pDX, IDC_PLASMA_EDIT_SETMINUTE, m_strDispSetMinute);
	DDX_Text(pDX, IDC_PLASMA_EDIT_SETSECOND, m_strDispSetSecond);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPWATT, m_strDispActualWatt);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPWATTRR, m_strDispActualWattR);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPHOUR, m_strDispActualHour);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPMINUTE, m_strDispActualMinute);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPSECOND, m_strDispActualSecond);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPVDC, m_strDispActualVDC);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPTORR, m_strDispActualTorr);
	DDX_Text(pDX, IDC_PLASMA_EDIT_BOOKHOUR, m_strDispBookHour);
	DDX_Text(pDX, IDC_PLASMA_EDIT_BOOKMINUTE, m_strDispBookMinute);
}


BEGIN_MESSAGE_MAP(CPlasmaCleanerDlg, CDialogEx)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_RUN, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonRun)
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_OPEN, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonOpen)
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_CANCEL, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonCancel)
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_UPDATE, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonUpdate)
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_CLOSE, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonClose)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PLASMA_SPIN_SETHOUR, &CPlasmaCleanerDlg::OnDeltaposPlasmaSpinSethour)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PLASMA_SPIN_SETMINUTE, &CPlasmaCleanerDlg::OnDeltaposPlasmaSpinSetminute)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PLASMA_SPIN_SETSECOND, &CPlasmaCleanerDlg::OnDeltaposPlasmaSpinSetsecond)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PLASMA_SPIN_BOOKHOUR, &CPlasmaCleanerDlg::OnDeltaposPlasmaSpinBookhour)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PLASMA_SPIN_BOOKMINUTE, &CPlasmaCleanerDlg::OnDeltaposPlasmaSpinBookminute)
	ON_BN_CLICKED(IDC_PLASMA_CHECK, &CPlasmaCleanerDlg::OnBnClickedPlasmaCheck)
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_TIMERRUN, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonTimerrun)
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_MINIMIZE, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonMinimize)
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_TCPOPEN, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonTcpopen)
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_TCPCLOSE, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonTcpclose)
END_MESSAGE_MAP()


// CPlasmaCleaner 메시지 처리기


void CPlasmaCleanerDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case 0:
		updateui();
		break;
	default:
		break;
	}
	CDialogEx::OnTimer(nIDEvent);
}


BOOL CPlasmaCleanerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDGREEN), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDRED), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	// 아이콘을 그립니다.
	((CStatic*)GetDlgItem(IDC_PLASMA_ICON_RUN))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_PLASMA_ICON_BISSCOMMUNICATION))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_PLASMA_ICON_SREMCOMMUNICATION))->SetIcon(m_LedIcon[0]);


	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_SETHOUR))->SetRange(0, 99);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_SETMINUTE))->SetRange(0, 59);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_SETSECOND))->SetRange(0, 59);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_BOOKHOUR))->SetRange(0, 23);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_BOOKMINUTE))->SetRange(0, 59);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_SETHOUR))->SetPos(0);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_SETMINUTE))->SetPos(0);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_SETSECOND))->SetPos(0);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_BOOKHOUR))->SetPos(0);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_BOOKMINUTE))->SetPos(0);

	SetDlgItemTextA(IDC_PLASMA_EDIT_DISPCURRENTTIME, "00:00:00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_DISPSTARTTIME, "00:00:00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_DISPENDTIME, "00:00:00");

	SetDlgItemTextA(IDC_PLASMA_EDIT_SETWATT, "50");
	SetDlgItemTextA(IDC_PLASMA_EDIT_SETHOUR, "03");
	SetDlgItemTextA(IDC_PLASMA_EDIT_SETMINUTE, "00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_SETSECOND, "00");

	GetDlgItem(IDC_PLASMA_EDIT_BOOKHOUR)->EnableWindow(FALSE);
	GetDlgItem(IDC_PLASMA_EDIT_BOOKMINUTE)->EnableWindow(FALSE);
	GetDlgItem(IDC_PLASMA_SPIN_BOOKHOUR)->EnableWindow(FALSE);
	GetDlgItem(IDC_PLASMA_SPIN_BOOKMINUTE)->EnableWindow(FALSE);
	GetDlgItem(IDC_PLASMA_BUTTON_TIMERRUN)->EnableWindow(FALSE);

	GetDlgItem(IDC_PLASMA_BUTTON_TCPOPEN)->EnableWindow(FALSE);
	GetDlgItem(IDC_PLASMA_BUTTON_TCPCLOSE)->EnableWindow(FALSE);

	((CDateTimeCtrl*)GetDlgItem(IDC_PLASMA_DATETIMEPICKER))->SetFormat("dd");

	setControlUI(FALSE);
	SetTimer(0, 100, NULL);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CPlasmaCleanerDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CPlasmaCleanerDlg::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	KillTimer(0);
	g_pBISSCom->CloseSerialPort();
	g_pSREMCom->CloseTcpIpSocket();

	m_bisRunThreadExitFlag = TRUE;
	m_bisToggleThreadExitFlag = TRUE;
	m_bisOpenSerialThreadExitFlag = TRUE;
	Sleep(3000);

	m_pRunThread = NULL;
	m_pUpdateThread = NULL;
	m_pToggleThread = NULL;

	return CDialogEx::DestroyWindow();
}


void CPlasmaCleanerDlg::updateui()
{
	if (g_pBISSCom->m_bSerialConnected == TRUE)
	{
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_COMMUNICATION))->SetIcon(m_LedIcon[1]);
		//if (g_pSREMCom->m_bConnected == TRUE)
		//{
		//	g_pSREMCom->sendBISSOn();
		//}
		CString strTemp = "";
		//strTemp.Format(_T("%02d"), g_pBISSCom->m_nActualWatt);
		//SetDlgItemTextA(IDC_PLASMA_EDIT_DISPWATT, strTemp);
		//strTemp.Format(_T("%02d"), g_pBISSCom->m_nActualWattR);
		//SetDlgItemTextA(IDC_PLASMA_EDIT_DISPWATTRR, strTemp);
		//strTemp.Format(_T("%02d"), g_pBISSCom->m_nActualHour);
		//SetDlgItemTextA(IDC_PLASMA_EDIT_DISPHOUR, strTemp);
		//strTemp.Format(_T("%02d"), g_pBISSCom->m_nActualMinute);
		//SetDlgItemTextA(IDC_PLASMA_EDIT_DISPMINUTE, strTemp);
		//strTemp.Format(_T("%02d"), g_pBISSCom->m_nActualSecond);
		//SetDlgItemTextA(IDC_PLASMA_EDIT_DISPSECOND, strTemp);
		//strTemp.Format(_T("%0.1e"), g_pBISSCom->m_dActualTorr);
		//SetDlgItemTextA(IDC_PLASMA_EDIT_DISPTORR, strTemp);
		//strTemp.Format(_T("%0.1f"), g_pBISSCom->m_dActualVDC);
		//SetDlgItemTextA(IDC_PLASMA_EDIT_DISPVDC, strTemp);
		UpdateData(TRUE);
		m_nActualWatt = _ttoi(m_strDispActualWatt);
		m_nActualWattR = _ttoi(m_strDispActualWattR);
		m_nActualHour = _ttoi(m_strDispActualHour);
		m_nActualMinute = _ttoi(m_strDispActualMinute);
		m_nActualSecond = _ttoi(m_strDispActualSecond);
		m_dActualVDC = _ttof(m_strDispActualVDC);
		m_dActualTorr = _ttof(m_strDispActualTorr);
		if (g_pBISSCom->m_bisPowerON == false)
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_RUN))->SetIcon(m_LedIcon[0]);
			if (g_pBISSCom->m_bisPowerON != g_pBISSCom->m_bOldPowerStatus)
			{
				GetDlgItem(IDC_PLASMA_BUTTON_UPDATE)->EnableWindow(TRUE);
				SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Plasma Cleaning :: End")));	//통신 상태 기록.
				CTime curTime = CTime::GetCurrentTime();
				CString strTime;
				strTime.Format(_T("%02d:%02d:%02d"), curTime.GetHour(), curTime.GetMinute(), curTime.GetSecond());
				SetDlgItemTextA(IDC_PLASMA_EDIT_DISPENDTIME, strTime);
				if (g_pSREMCom->m_bConnected == TRUE)
				{
					g_pSREMCom->sendPlasmaCleaningOff();
					g_pSREMCom->sendTime(FALSE);
				}
			}
			g_pBISSCom->m_bOldPowerStatus = g_pBISSCom->m_bisPowerON;
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_RUN))->SetIcon(m_LedIcon[1]);
			if (g_pBISSCom->m_bisPowerON != g_pBISSCom->m_bOldPowerStatus)
			{
				if (g_pSREMCom->m_bConnected == TRUE)
				{
					g_pSREMCom->sendPlasmaCleaningOn();
				}
			}
			g_pBISSCom->m_bOldPowerStatus = g_pBISSCom->m_bisPowerON;
		}
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_COMMUNICATION))->SetIcon(m_LedIcon[0]);
		//if (g_pSREMCom->m_bConnected == TRUE)
		//{
		//	g_pSREMCom->sendBISSOff();
		//}
	}

	if (g_pSREMCom->m_bConnected == TRUE)
	{
		GetDlgItem(IDC_PLASMA_BUTTON_TCPOPEN)->EnableWindow(FALSE);
		GetDlgItem(IDC_PLASMA_BUTTON_TCPCLOSE)->EnableWindow(TRUE);
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_SREMCOMMUNICATION))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		GetDlgItem(IDC_PLASMA_BUTTON_TCPOPEN)->EnableWindow(TRUE);
		GetDlgItem(IDC_PLASMA_BUTTON_TCPCLOSE)->EnableWindow(FALSE);
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_SREMCOMMUNICATION))->SetIcon(m_LedIcon[0]);

	}
}


void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonRun()
{
	CECommon::SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Run Button Clicked")));	//통신 상태 기록.
	CString strTemp = "";
	int nTempRet = 0;
	if (g_pBISSCom->m_bSerialConnected == TRUE)	// communicaton check
	{
		strTemp = "플라즈마 클리닝을 즉시 실행하시겠습니까";
		int nRet = AfxMessageBox(strTemp, MB_YESNO);
		if (nRet == IDYES)
		{
			SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Message Box Yes Button Clicked")));	//통신 상태 기록.
		}
		else
		{
			SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Message Box Cancel Button Clicked")));	//통신 상태 기록.
			return;
		}
		CTime curTime = CTime::GetCurrentTime();
		m_Starttime = curTime;
		m_bisManual = TRUE;
		runPlasmaCleaner();
	}
	else
	{
		AfxMessageBox("통신 상태를 확인하세요.");
		SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Communication Not connected")));	//통신 상태 기록.
	}
}

void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonTimerrun()
{
	CECommon::SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Timer Run Button Clicked")));	//통신 상태 기록.
	CString strTemp = "";
	BOOL isTimer = FALSE;
	int nTempRet = 0;
	if (g_pBISSCom->m_bSerialConnected)	// communicaton check
	{
		strTemp = "플라즈마 클리닝을 타이머로 실행하시겠습니까";
		int nRet = AfxMessageBox(strTemp, MB_YESNO);
		if (nRet == IDYES)
		{
			SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Message Box Yes Button Clicked")));	//통신 상태 기록.
			isTimer = TRUE;
		}
		else
		{
			SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Message Box Cancel Button Clicked")));	//통신 상태 기록.
			return;
		}
		if (checkTime() == FALSE)
		{
			AfxMessageBox("Timer 설정을 확인하세요.");
			return;
		}
		m_bisManual = TRUE;
		runPlasmaCleaner();

		//runPlasmaCleaner(isTimer);
	}
	else
	{
		AfxMessageBox("통신 상태를 확인하세요.");
		SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Communication Not connected")));	//통신 상태 기록.
	}
}


void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonCancel()
{
	m_bisManual = TRUE;
	stopPlasmaCleaner();
}

void CPlasmaCleanerDlg::setControlUI(bool status)
{
	GetDlgItem(IDC_PLASMA_EDIT_SETWATT)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_SETHOUR)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_SETMINUTE)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_SETSECOND)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_DISPWATT)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_DISPWATTRR)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_DISPHOUR)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_DISPMINUTE)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_DISPSECOND)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_SPIN_SETHOUR)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_SPIN_SETMINUTE)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_SPIN_SETSECOND)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_BUTTON_OPEN)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_BUTTON_CLOSE)->EnableWindow(status);
	//GetDlgItem(IDC_PLASMA_BUTTON_UPDATE)->EnableWindow(!status);
	GetDlgItem(IDC_PLASMA_CHECK)->EnableWindow(status);

	if (IsDlgButtonChecked(IDC_PLASMA_CHECK) == BST_CHECKED && status == TRUE)
	{
		GetDlgItem(IDC_PLASMA_BUTTON_RUN)->EnableWindow(FALSE);
		GetDlgItem(IDC_PLASMA_BUTTON_TIMERRUN)->EnableWindow(status);
		GetDlgItem(IDC_PLASMA_EDIT_BOOKHOUR)->EnableWindow(status);
		GetDlgItem(IDC_PLASMA_EDIT_BOOKMINUTE)->EnableWindow(status);
		GetDlgItem(IDC_PLASMA_SPIN_BOOKHOUR)->EnableWindow(status);
		GetDlgItem(IDC_PLASMA_SPIN_BOOKMINUTE)->EnableWindow(status);
		GetDlgItem(IDC_PLASMA_DATETIMEPICKER)->EnableWindow(status);
	}
	else
	{
		GetDlgItem(IDC_PLASMA_BUTTON_RUN)->EnableWindow(status);
		GetDlgItem(IDC_PLASMA_BUTTON_TIMERRUN)->EnableWindow(FALSE);
		GetDlgItem(IDC_PLASMA_EDIT_BOOKHOUR)->EnableWindow(FALSE);
		GetDlgItem(IDC_PLASMA_EDIT_BOOKMINUTE)->EnableWindow(FALSE);
		GetDlgItem(IDC_PLASMA_SPIN_BOOKHOUR)->EnableWindow(FALSE);
		GetDlgItem(IDC_PLASMA_SPIN_BOOKMINUTE)->EnableWindow(FALSE);
		GetDlgItem(IDC_PLASMA_DATETIMEPICKER)->EnableWindow(FALSE);
	}
}

int CPlasmaCleanerDlg::getSetValue()
{
	int nRet = 0;
	if (g_pBISSCom->m_bSerialConnected == TRUE)
	{
		CString strTemp;
		int nRet = g_pBISSCom->sendGetParameterSet();
		if (nRet == 0) //check
		{
			SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Set Parameter Update Finished")));	//통신 상태 기록.
		}
		else // time out error
		{
			SaveLogFile("PlasmaCleaning_Status", _T("BISS :: Fail to read setting parameters !! (Error Code : Timeout Error)"));	//통신 상태 기록.
		}
	}
	else
	{
		SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Serial Communication Error")));	//통신 상태 기록.
		AfxMessageBox(_T("Device not connected !!"));
		nRet = -1;
	}
	if (nRet == 0 && g_pSREMCom->m_bConnected == TRUE)
	{
		g_pSREMCom->sendSetParameter();
		g_pSREMCom->sendTimerParameter();
	}
	return nRet;
}

int CPlasmaCleanerDlg::checkSetParamter()
{
	int nRet = 0;
	CString strTemp;

	if (g_pBISSCom->m_bSerialConnected == TRUE)
	{
		nRet = g_pBISSCom->sendGetParameterSet();
		if (nRet != 0) //check
		{
			strTemp.Format(_T("BISS :: Fail to read setting parameters !! sendGetParameterSet (Error Code : {%d})"), nRet);
			SaveLogFile("PlasmaCleaning_Status", strTemp);	//통신 상태 기록.
		}
	}
	return nRet;
}

void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonUpdate()
{
	SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Local Mode Button Clicked")));	//통신 상태 기록.
	int nRet = 0;
	m_bisToggleThreadExitFlag = TRUE;
	if (m_pToggleThread != NULL)
	{
		//dwResult = WaitForSingleObject(m_pToggleThread->m_hThread, /*INFINITE*/2000);
		if (WaitForSingleObject(m_pToggleThread->m_hThread, /*INFINITE*/2000) == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(m_pToggleThread->m_hThread, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
#if FALSE
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
#else
				m_pToggleThread->SuspendThread();
				m_pToggleThread->ExitInstance();
#endif
			}
		}
		m_pToggleThread = NULL;
	}

	setControlUI(TRUE);

	m_bisToggleThreadExitFlag = FALSE;
	m_Toggletime = CTime::GetCurrentTime();
	m_pToggleThread = ::AfxBeginThread(ToggleThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
	//m_pToggleThread->m_bAutoDelete = TRUE;

}

void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonOpen()
{
	int nRet = 0;
	SaveLogFile("PlasmaCleaning_Status", (LPSTR)(LPCTSTR)("BISS :: Open Button Clicked"));	//통신 상태 기록.
	if (g_pBISSCom->m_bSerialConnected == TRUE)
	{
		g_pBISSCom->CloseSerialPort();
		if (m_pUpdateThread != NULL)
		{
			if (WaitForSingleObject(m_pUpdateThread->m_hThread, 2000) != WAIT_OBJECT_0)
			{
				CECommon::SaveLogFile("PlasmaCleaning_Status", (LPSTR)(LPCTSTR)("BISS :: Initialize Fail"));	//통신 상태 기록.
				return;
			}
		}
	}
	nRet = g_pBISSCom->openBISSController();
	if (nRet == 0)
	{
		nRet = getSetValue();
		if (nRet != 0)
		{
			g_pBISSCom->CloseSerialPort();
			return;
		}
		m_pUpdateThread = ::AfxBeginThread(UpdateParameterThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
		//Sleep(1000);
		if (g_pSREMCom->m_bConnected == TRUE)
		{
			g_pSREMCom->sendBISSOn();
		}
	}
	else
	{
		CECommon::SaveLogFile("PlasmaCleaning_Status", (LPSTR)(LPCTSTR)("BISS :: Open Fail"));	//통신 상태 기록.
		AfxMessageBox(_T("Open Fail"));
	}
}


void CPlasmaCleanerDlg::OnDeltaposPlasmaSpinSethour(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString strTemp = "";
	GetDlgItemTextA(IDC_PLASMA_EDIT_SETHOUR, strTemp);
	pNMUpDown->iPos = (_ttoi(strTemp));
	int nValue = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nValue > -1) && (nValue < 100))
	{
		strTemp.Format(_T("%02d"), nValue);
		SetDlgItemTextA(IDC_PLASMA_EDIT_SETHOUR, strTemp);
		*pResult = 0;
	}
}


void CPlasmaCleanerDlg::OnDeltaposPlasmaSpinSetminute(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString strTemp = "";
	GetDlgItemTextA(IDC_PLASMA_EDIT_SETMINUTE, strTemp);
	pNMUpDown->iPos = (_ttoi(strTemp));
	int nValue = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nValue > -1) && (nValue < 60))
	{
		strTemp.Format(_T("%02d"), nValue);
		SetDlgItemTextA(IDC_PLASMA_EDIT_SETMINUTE, strTemp);
		*pResult = 0;
	}
}


void CPlasmaCleanerDlg::OnDeltaposPlasmaSpinSetsecond(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString strTemp = "";
	GetDlgItemTextA(IDC_PLASMA_EDIT_SETSECOND, strTemp);
	pNMUpDown->iPos = (_ttoi(strTemp));
	int nValue = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nValue > -1) && (nValue < 59))
	{
		strTemp.Format(_T("%02d"), nValue);
		SetDlgItemTextA(IDC_PLASMA_EDIT_SETSECOND, strTemp);
		*pResult = 0;
	}
}


void CPlasmaCleanerDlg::OnDeltaposPlasmaSpinBookhour(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString strTemp = "";
	GetDlgItemTextA(IDC_PLASMA_EDIT_BOOKHOUR, strTemp);
	pNMUpDown->iPos = (_ttoi(strTemp));
	int nValue = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nValue > -1) && (nValue < 24))
	{
		strTemp.Format(_T("%02d"), nValue);
		SetDlgItemTextA(IDC_PLASMA_EDIT_BOOKHOUR, strTemp);
		*pResult = 0;
	}
}


void CPlasmaCleanerDlg::OnDeltaposPlasmaSpinBookminute(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString strTemp = "";
	GetDlgItemTextA(IDC_PLASMA_EDIT_BOOKMINUTE, strTemp);
	pNMUpDown->iPos = (_ttoi(strTemp));
	int nValue = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nValue > -1) && (nValue < 59))
	{
		strTemp.Format(_T("%02d"), nValue);
		SetDlgItemTextA(IDC_PLASMA_EDIT_BOOKMINUTE, strTemp);
		*pResult = 0;
	}
}

UINT CPlasmaCleanerDlg::PlasmaCleaningThread(LPVOID lParam)
{

	int nRet = 0; CString strTemp;

	if (g_pPlasmaDlg->m_bisManual == TRUE && g_pSREMCom->m_bConnected == TRUE)
	{
		g_pSREMCom->sendSetParameter();
		g_pSREMCom->sendTimerParameter();
	}
	if (g_pSREMCom->m_bConnected == TRUE)
	{
		g_pSREMCom->sendTime(FALSE);
	}

	while (g_pPlasmaDlg->m_bisRunThreadExitFlag == FALSE)
	{
		if (g_pBISSCom->m_bSerialConnected)
		{
			CTime curTime = CTime::GetCurrentTime();
			if (g_pPlasmaDlg->m_Starttime <= curTime)
			{
				CString strTime, strSetting;
				strTime.Format(_T("%02d:%02d:%02d"), curTime.GetHour(), curTime.GetMinute(), curTime.GetSecond());
				g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_DISPSTARTTIME, strTime);
				strSetting.Format(_T("BISS :: Setting Parameter : W %s, H %s, M %s, S %s"), g_pPlasmaDlg->m_strDispSetWatt, g_pPlasmaDlg->m_strDispSetHour, g_pPlasmaDlg->m_strDispSetMinute, g_pPlasmaDlg->m_strDispSetSecond);
				g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", (LPSTR)(LPCTSTR)strSetting);	//통신 상태 기록.
				g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_DISPCURRENTTIME, "00:00:00");
				g_pSREMCom->m_isInterlock = FALSE;
				g_pSREMCom->m_isPreesureCheck = FALSE;

				if (g_pSREMCom->m_bConnected == TRUE)
				{
					nRet = g_pSREMCom->sendGetInterlock();
					if (nRet != WAIT_OBJECT_0)
					{
						nRet = -1;
						g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", (LPSTR)(LPCTSTR)("BISS :: Timer Thread END :: Interlock Listen Fail"));	//통신 상태 기록.
						break;
					}
				}
				else
				{
					if (g_pPlasmaDlg->m_bisManual == FALSE)
					{
						nRet = -1;
						g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", (LPSTR)(LPCTSTR)("BISS :: Timer Thread END :: Interlock Communication Disconnected"));	//통신 상태 기록.
						break;
					}

					if(g_pPlasmaDlg->IsDlgButtonChecked(IDC_PLASMA_CHECK) == BST_UNCHECKED) // Not Timer Run
					{
						int nTempRet = AfxMessageBox("Interlock Listen Fail\r 계속 진행하시겠습니까", MB_YESNO);
						if (nTempRet == IDYES)
						{
							g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", (LPSTR)(LPCTSTR)("BISS :: Yes Button Clicked"));	//통신 상태 기록.
							CPasswordDlg test(g_pPlasmaDlg);
							test.DoModal();
							if (test.m_strPassword == "1111")
							{
								g_pSREMCom->m_isInterlock = TRUE;
								g_pSREMCom->m_isPreesureCheck = TRUE;
							}
							else
							{
								nRet = -2;
								strTemp.Format(_T("비밀번호가 틀렸습니다."));
								break;
							}
						}
						else
						{
							nRet = -3;
							g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", (LPSTR)(LPCTSTR)("BISS :: No Button Clicked"));	//통신 상태 기록.
							strTemp.Format(_T("진행 취소"));
							break;
						}
					}
				}

				if (g_pSREMCom->m_isInterlock == FALSE || g_pSREMCom->m_isPreesureCheck == FALSE)
				{
					nRet = -9;
					strTemp.Format(_T("BISS :: Timer Thread END :: Interlock Fail"));
					//if (g_pPlasmaDlg->m_bisManual == TRUE && g_pPlasmaDlg->IsDlgButtonChecked(IDC_PLASMA_CHECK) == BST_UNCHECKED)
					//{
					//	int nRet = AfxMessageBox("Interlock Fail\r 계속 진행하시겠습니까", MB_YESNO);
					//	if (nRet == IDYES)
					//	{
					//		g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", (LPSTR)(LPCTSTR)("Yes Button Clicked"));	//통신 상태 기록.
					//	}
					//	else
					//	{
					//		break;
					//	}
					//}
					//else
					//{
					//	break;
					//}
					break;
				}

				g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", (LPSTR)(LPCTSTR)("BISS :: Plasma Cleaning :: Start"));	//통신 상태 기록.
				nRet = g_pBISSCom->sendPowerOn();
				if (nRet != WAIT_OBJECT_0)
				{
					strTemp.Format(_T("BISS :: Fail to power on!! (Error Code : %d"), nRet);
				}
				if (g_pSREMCom->m_bConnected == TRUE)
				{
					g_pSREMCom->sendTime(FALSE);
				}
				break;
			}
			else
			{
				CTimeSpan timeTemp;
				timeTemp = g_pPlasmaDlg->m_Starttime - curTime;
				CString strTemp;
				strTemp.Format(_T("%02d:%02d:%02d"), timeTemp.GetDays()*24 + timeTemp.GetHours(), timeTemp.GetMinutes(), timeTemp.GetSeconds());
				g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_DISPCURRENTTIME, strTemp);
				if (g_pSREMCom->m_bConnected == TRUE)
				{
					g_pSREMCom->sendTime();
				}
				Sleep(500);
			}
		}
		else
		{
			strTemp.Format(_T("BISS :: Timer Thread END :: Serial Communication Disconnected."));
			nRet = -999;
			break;
		}
	}
	if (g_pPlasmaDlg->m_bisRunThreadExitFlag == TRUE)
	{
		strTemp.Format(_T("BISS :: Timer Thread END :: Cancel Button Clicked"));
		nRet = -99;
	}

	if (nRet != 0)
	{
		if (g_pSREMCom->m_bConnected)
		{
			g_pSREMCom->sendThreaddEnd();
		}
		g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", strTemp);
		g_pPlasmaDlg->GetDlgItem(IDC_PLASMA_BUTTON_UPDATE)->EnableWindow(TRUE);
	}
	else
	{ 
		g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", "BISS :: Timer Thread END :: Sucessful Close");
	}
	return 0;
}

UINT CPlasmaCleanerDlg::UpdateParameterThread(LPVOID lParam)
{
	int nRet = 0, nErrorCount = 0, nRetry = 3;
	CString strTemp = "";
	while (g_pPlasmaDlg->m_bisOpenSerialThreadExitFlag == FALSE)
	{
		for (nErrorCount = 0; nErrorCount < nRetry; nErrorCount++)
		{
			nRet = g_pBISSCom->sendGetParameterAct();
			if (nRet == WAIT_OBJECT_0)
			{
				Sleep(300);
				break;
			}
			else
			{
				strTemp.Format(_T("BISS :: WAIT_FAILED : Count %d"), nErrorCount);
				g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", strTemp);
				nRet = -1;
			}
		}
		if (nErrorCount == nRetry)
		{
			break;
		}
		g_pBISSCom->sendGetParameterPower();
		Sleep(300);
		g_pBISSCom->sendGetParameterTorr();
		Sleep(300);
		g_pBISSCom->sendGetParameterVDC();
		Sleep(300);
	}

	if (nRet != WAIT_OBJECT_0 && g_pBISSCom->m_bSerialConnected == TRUE)
	{
		AfxMessageBox(_T("Communication Time Error"));
		g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", _T("BISS :: No response. Close serial."));
		((CStatic*)g_pPlasmaDlg->GetDlgItem(IDC_PLASMA_ICON_RUN))->SetIcon(g_pPlasmaDlg->m_LedIcon[0]);
		g_pPlasmaDlg->GetDlgItem(IDC_PLASMA_BUTTON_UPDATE)->EnableWindow(TRUE);
		g_pBISSCom->CloseSerialPort();
		if (g_pSREMCom->m_bConnected == TRUE)
		{
			g_pSREMCom->sendBISSOff();
		}
	}

	return 0;
}

UINT CPlasmaCleanerDlg::ToggleThread(LPVOID lParam)
{
	g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", "BISS :: Offline Mode On");
	CTimeSpan temptime(0, 0, 0, 30);
	CTimeSpan tempttime(0, 0, 0, 10);
	int nRet = 0;

	TRACE("Toggle Thread Start..\n");
	while (g_pPlasmaDlg->m_bisToggleThreadExitFlag == FALSE)
	{
		CTime curTime = CTime::GetCurrentTime();
		if (curTime > g_pPlasmaDlg->m_Toggletime + temptime )
		{
			nRet = -1;

			break;
		}
		Sleep(100);
	}
	if (nRet == -1)
	{
		g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", "BISS :: Dialog Locked");
		g_pPlasmaDlg->setControlUI(FALSE);
	}
	else
	{
		return 0;
	}
	temptime += tempttime;

	while (g_pPlasmaDlg->m_bisToggleThreadExitFlag == FALSE)
	{
		CTime curTime = CTime::GetCurrentTime();
		if (curTime > g_pPlasmaDlg->m_Toggletime + temptime)
		{
			nRet = -1;
			break;
		}
		Sleep(100);
	}

	if (nRet == -1)
	{
		g_pPlasmaDlg->SaveLogFile("PlasmaCleaning_Status", "BISS :: Offline Mode Off");
		if (g_pSREMCom->m_bConnected == TRUE)
		{
			g_pMainDlg->minimizeDialog();
		}
	}
	else
	{

	}
	TRACE("Toggle Thread off..\n");
	return 0;
}

void CPlasmaCleanerDlg::runPlasmaCleaner()
{
	int nRet = 0;
	CString strTemp = "";
	m_bisRunThreadExitFlag = false;
	setControlUI(false);
	GetDlgItem(IDC_PLASMA_BUTTON_UPDATE)->EnableWindow(FALSE);
	if (g_pBISSCom->m_bSerialConnected == TRUE)	// communicaton check
	{
		int nTemp = 0;

		GetDlgItemTextA(IDC_PLASMA_EDIT_SETWATT, strTemp);
		nTemp = _ttoi(strTemp);
		nRet = g_pBISSCom->sendSetParameterWatt(nTemp);
		if (nRet != 0)
		{
			strTemp.Format(_T("BISS :: Fail to set watt value. (Error Code : %d)"), nRet);
			CECommon::SaveLogFile("PlasmaCleaning_Status", strTemp);	//통신 상태 기록.
			AfxMessageBox(strTemp);
			//setControlUI(TRUE);
			if (m_bisManual == TRUE)
			{
				GetDlgItem(IDC_PLASMA_BUTTON_UPDATE)->EnableWindow(TRUE);
			}
			else
			{
				if (g_pSREMCom->m_bConnected)
				{
					g_pSREMCom->sendThreaddEnd();
				}
			}
			return;
		}
		else
		{
			strTemp.Format(_T("BISS :: Set Watt Success : %02d"), nTemp);
			CECommon::SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)strTemp));	//통신 상태 기록.
		}

		GetDlgItemTextA(IDC_PLASMA_EDIT_SETHOUR, strTemp);
		nTemp = _ttoi(strTemp);
		nRet = g_pBISSCom->sendSetParameterHour(nTemp);
		if (nRet != 0)
		{
			strTemp.Format(_T("BISS :: Fail to set hour value. (Error Code : %d)"), nRet);
			CECommon::SaveLogFile("PlasmaCleaning_Status", strTemp);	//통신 상태 기록.
			AfxMessageBox(strTemp);
			//setControlUI(TRUE);
			if (m_bisManual == TRUE)
			{
				GetDlgItem(IDC_PLASMA_BUTTON_UPDATE)->EnableWindow(TRUE);
			}
			else
			{
				if (g_pSREMCom->m_bConnected)
				{
					g_pSREMCom->sendThreaddEnd();
				}
			}
			return;
		}
		else
		{
			strTemp.Format(_T("BISS :: Set Hour Success : %02d"), nTemp);
			CECommon::SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)strTemp));	//통신 상태 기록.
		}

		GetDlgItemTextA(IDC_PLASMA_EDIT_SETMINUTE, strTemp);
		nTemp = _ttoi(strTemp);
		nRet = g_pBISSCom->sendSetParameterMinute(nTemp);
		if (nRet != 0)
		{
			strTemp.Format(_T("BISS :: Fail to set minute value. (Error Code : %d)"), nRet);
			CECommon::SaveLogFile("PlasmaCleaPlasmaCleaning_Statusning_Com", strTemp);	//통신 상태 기록.
			AfxMessageBox(strTemp);
			//setControlUI(TRUE);
			if (m_bisManual == TRUE)
			{
				GetDlgItem(IDC_PLASMA_BUTTON_UPDATE)->EnableWindow(TRUE);
			}
			else
			{
				if (g_pSREMCom->m_bConnected)
				{
					g_pSREMCom->sendThreaddEnd();
				}
			}
			return;
		}
		else
		{
			strTemp.Format(_T("BISS :: Set Minute Success : %02d"), nTemp);
			CECommon::SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)strTemp));	//통신 상태 기록.
		}

		nRet = checkSetParamter();
		if (nRet != 0)
		{
			CECommon::SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Set Parameter Check Fail")));
			//setControlUI(TRUE);
			if (m_bisManual == TRUE)
			{
				GetDlgItem(IDC_PLASMA_BUTTON_UPDATE)->EnableWindow(TRUE);
			}
			else
			{
				if (g_pSREMCom->m_bConnected == TRUE)
				{
					g_pSREMCom->sendThreaddEnd();
				}
			}
			return;
		}

		CString temp = "";
		temp.Format(_T("%02d:%02d:%02d"), m_Starttime.GetHour(), m_Starttime.GetMinute(), m_Starttime.GetSecond());
		SetDlgItemTextA(IDC_PLASMA_EDIT_DISPSTARTTIME, temp);
		SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)"BISS :: Start Time Set : " + temp));	//통신 상태 기록.
		CTimeSpan temptime(0, _ttoi(m_strDispSetHour), _ttoi(m_strDispSetMinute), _ttoi(m_strDispSetSecond));
		CTime Endtime = m_Starttime + temptime;
		temp.Format(_T("%02d:%02d:%02d"), Endtime.GetHour(), Endtime.GetMinute(), Endtime.GetSecond());
		SetDlgItemTextA(IDC_PLASMA_EDIT_DISPENDTIME, temp);
		SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)"BISS :: End Time Set : " + temp));	//통신 상태 기록.
		m_pRunThread = ::AfxBeginThread(PlasmaCleaningThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
		SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)"BISS :: Thread Start"));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: PC -> Plasma : Device not connected.")));	//통신 상태 기록.
		GetDlgItem(IDC_PLASMA_BUTTON_UPDATE)->EnableWindow(TRUE);
		if (g_pSREMCom->m_bConnected)
		{
			g_pSREMCom->sendThreaddEnd();
		}
	}
}

void CPlasmaCleanerDlg::stopPlasmaCleaner()
{
	int nRet = 0;
	CString strTemp;
	SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Cancel Button Clicked")));	//통신 상태 기록.
	//setControlUI(true);
	if (g_pBISSCom->m_bSerialConnected == TRUE)
	{
		if (g_pBISSCom->m_bisPowerON == TRUE)
		{
			nRet = g_pBISSCom->sendPowerOff();
			if (nRet != 0)
			{
				strTemp.Format(_T("BISS :: Fail to power off !! (Error Code : %d)"), nRet);
				SaveLogFile("PlasmaCleaning_Status", strTemp);
				AfxMessageBox(strTemp);
			}
			else
			{
				//g_pSREMCom->sendPlasmaCleaningOff();
			}
		}
	}
	m_bisRunThreadExitFlag = true;
	if (m_pRunThread != NULL)
	{
		nRet = WaitForSingleObject(m_pRunThread->m_hThread, 1000);
		m_pRunThread = NULL;
	}
	//GetDlgItem(IDC_PLASMA_BUTTON_UPDATE)->EnableWindow(TRUE);
}

void CPlasmaCleanerDlg::setTimerWindowState(BOOL status)
{
	GetDlgItem(IDC_PLASMA_EDIT_BOOKHOUR)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_BOOKMINUTE)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_SPIN_BOOKHOUR)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_SPIN_BOOKMINUTE)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_DATETIMEPICKER)->EnableWindow(status);
}


void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonClose()
{
	SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Serial close button clicked!")));	//통신 상태 기록.
	if (g_pBISSCom->m_bSerialConnected == TRUE)
	{
		SaveLogFile("PlasmaCleaning_Com", _T((LPSTR)(LPCTSTR)("BISS :: SERIAL PORT CLOSE")));	//통신 상태 기록.
		SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: SERIAL PORT CLOSE!")));	//통신 상태 기록.

		g_pBISSCom->CloseSerialPort();
		if (g_pSREMCom->m_bConnected == TRUE)
		{
			g_pSREMCom->sendBISSOff();
		}
	}
}


void CPlasmaCleanerDlg::OnBnClickedPlasmaCheck()
{
	if (IsDlgButtonChecked(IDC_PLASMA_CHECK) == BST_CHECKED)
	{
		setTimerWindowState(TRUE);
		GetDlgItem(IDC_PLASMA_BUTTON_RUN)->EnableWindow(FALSE);
		GetDlgItem(IDC_PLASMA_BUTTON_TIMERRUN)->EnableWindow(TRUE);
	}
	else
	{
		setTimerWindowState(FALSE);
		GetDlgItem(IDC_PLASMA_BUTTON_RUN)->EnableWindow(TRUE);
		GetDlgItem(IDC_PLASMA_BUTTON_TIMERRUN)->EnableWindow(FALSE);
	}
}



void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonMinimize()
{
	g_pMainDlg->minimizeDialog();
}


void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonTcpopen()
{
	g_pSREMCom->openSocketServer();
}


void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonTcpclose()
{
	g_pSREMCom->CloseTcpIpSocket();
}

BOOL CPlasmaCleanerDlg::checkTime()
{
	CTime timeSet;
	((CDateTimeCtrl*)GetDlgItem(IDC_PLASMA_DATETIMEPICKER))->GetTime(timeSet);
	CString strHour, strMinute;
	GetDlgItemTextA(IDC_PLASMA_EDIT_BOOKHOUR, strHour);
	GetDlgItemTextA(IDC_PLASMA_EDIT_BOOKMINUTE, strMinute);

	CTime timeStart(timeSet.GetYear(), timeSet.GetMonth(), timeSet.GetDay(), _ttoi(strHour), _ttoi(strMinute), 0);
	m_Starttime = timeStart;
	CTime timeCur = CTime::GetCurrentTime();
	if (m_Starttime <= timeCur)
	{
		SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("BISS :: Timer Setting Fail")));
		return FALSE;
	}
	return TRUE;
}
