#include "pch.h"
#include "extern.h"
#include <vector>

using namespace std;

CSREMCommunication::CSREMCommunication()
{
	m_hInterlcokCheckEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

	m_isInterlock = FALSE;
	m_isPreesureCheck = FALSE;

	m_strReceiveEndOfStreamSymbol = "\n";
}

CSREMCommunication::~CSREMCommunication()
{
	CloseHandle(m_hInterlcokCheckEvent);
}

int CSREMCommunication::SendData(char * lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	CString strSendData = lParam;

	nRet = Send(lParam, nTimeOut, nRetryCnt);
	if (nRet != 0)
	{
		CString strTemp;
		strTemp.Format(_T("PC -> Plasma : SEND FAIL (%d)"), nRet);
		SaveLogFile("PlasmaCleaning_SREM", (LPSTR)(LPCTSTR)strTemp);	//통신 상태 기록.
	}
	else
		SaveLogFile("PlasmaCleaning_BISS_TCP", _T((LPSTR)(LPCTSTR)("Plasma -> SREM: " + strSendData.Mid(0, strSendData.GetLength() - 2))));	//통신 상태 기록.

	return nRet;
}

int CSREMCommunication::ReceiveData(char * lParam)
{
	CString strTemp, strText(_T(""));
	vector<CString> vecRecvData;

	int nIndex = 0;
	while (FALSE != AfxExtractSubString(strText, lParam, nIndex, _T('\r')))
	{
		vecRecvData.push_back(strText);
		nIndex++;
	}

	for (int i = 0; i < nIndex - 1; i++)
	{
		if (vecRecvData[i].Left(1) == "I") // run
		{
			SaveLogFile("PlasmaCleaning_BISS_TCP", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			vector<CString> vecTempData;
			strText = _T("");

			int nIdx = 0;
			while (FALSE != AfxExtractSubString(strText, vecRecvData[i], nIdx, _T(',')))
			{
				vecTempData.push_back(strText);
				nIdx++;
			}
			m_isInterlock = atoi(vecTempData[0].Mid(2, 2));
			m_isPreesureCheck = atoi(vecTempData[1].Mid(1, 2));
			vecTempData.clear();
			SetEvent(m_hInterlcokCheckEvent);

		}
		else if (vecRecvData[i] == "RUN") // run
		{
			SaveLogFile("PlasmaCleaning_BISS_TCP", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			g_pPlasmaDlg->m_bisManual = FALSE;
			g_pPlasmaDlg->runPlasmaCleaner();
		}
		else if (vecRecvData[i].Mid(0, 4) == "STOP") // get Watt
		{
			SaveLogFile("PlasmaCleaning_BISS_TCP", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			g_pPlasmaDlg->stopPlasmaCleaner();
		}
		else if (vecRecvData[i].Mid(0, 4) == "SetP") // get Watt
		{
			SaveLogFile("PlasmaCleaning_BISS_TCP", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			vector<CString> vecTempData;
			strText = _T("");
			//SetEvent(m_hGetSetParameterEvent);

			int nIdx = 0;
			while (FALSE != AfxExtractSubString(strText, vecRecvData[i], nIdx, _T(',')))
			{
				vecTempData.push_back(strText);
				nIdx++;
			}
			//g_pPlasmaDlg->m_nDispSetWatt = vecTempData[0].Mid(6, 2);
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_SETWATT, vecTempData[0].Mid(6, 2));
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_SETHOUR, vecTempData[1].Mid(1, 2));
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_SETMINUTE, vecTempData[2].Mid(1, 2));
			g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_SETSECOND, vecTempData[3].Mid(1, 2));
			//g_pPlasmaDlg->m_nDispSetHour = vecTempData[1].Mid(1, 2);
			//g_pPlasmaDlg->m_nDispSetMinute = vecTempData[2].Mid(1, 2);
			//g_pPlasmaDlg->m_nDispSetSecond = vecTempData[3].Mid(1, 2);
			//g_pPlasmaDlg->UpdateData(FALSE);
			vecTempData.clear();
		}
		else if (vecRecvData[i].Mid(0, 4) == "SetT") // get Watt
		{
			SaveLogFile("PlasmaCleaning_BISS_TCP", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			vector<CString> vecTempData;
			strText = _T("");
			//SetEvent(m_hGetSetParameterEvent);

			int nIdx = 0;
			while (FALSE != AfxExtractSubString(strText, vecRecvData[i], nIdx, _T(',')))
			{
				vecTempData.push_back(strText);
				nIdx++;
			}
			if (vecTempData[0].Right(1) == "1")
			{
				g_pPlasmaDlg->CheckDlgButton(IDC_PLASMA_CHECK,TRUE);
				g_pPlasmaDlg->setTimerWindowState(TRUE);
				g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_BOOKHOUR, vecTempData[1].Mid(1, 2));
				g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_BOOKMINUTE, vecTempData[2].Mid(1, 2));
				CTime timeTemp(_ttoi(vecTempData[3]), _ttoi(vecTempData[4]), _ttoi(vecTempData[5]), _ttoi(vecTempData[1].Mid(1, 2)), _ttoi(vecTempData[2].Mid(1, 2)), 0);
				((CDateTimeCtrl*)g_pPlasmaDlg->GetDlgItem(IDC_PLASMA_DATETIMEPICKER))->SetTime(&timeTemp);
				g_pPlasmaDlg->m_Starttime = timeTemp;
			}
			else
			{
				g_pPlasmaDlg->CheckDlgButton(IDC_PLASMA_CHECK, FALSE);
				g_pPlasmaDlg->setTimerWindowState(FALSE);
				g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_BOOKHOUR, "00");
				g_pPlasmaDlg->SetDlgItemTextA(IDC_PLASMA_EDIT_BOOKMINUTE, "00");
				CTime timeTemp = CTime::GetCurrentTime();
				((CDateTimeCtrl*)g_pPlasmaDlg->GetDlgItem(IDC_PLASMA_DATETIMEPICKER))->SetTime(&timeTemp);
				g_pPlasmaDlg->m_Starttime = timeTemp;
			}
			vecTempData.clear();
		}
		else if (vecRecvData[i].Mid(0, 4) == "GetS") // get Watt
		{
			SaveLogFile("PlasmaCleaning_BISS_TCP", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			sendSetParameter();
		}
		else if (vecRecvData[i].Mid(0, 4) == "GetT") // get Watt
		{
			SaveLogFile("PlasmaCleaning_BISS_TCP", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			sendTimerParameter();
		}
		else if (vecRecvData[i].Mid(0, 4) == "GetA") // get Watt
		{
			SaveLogFile("PlasmaCleaning_BISS_TCP", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			sendActualParameter();
		}
		else if (vecRecvData[i] == "APP SHOW") // get Watt
		{
			SaveLogFile("PlasmaCleaning_BISS_TCP", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			g_pMainDlg->restoreDialog();
		}
		else if (vecRecvData[i] == "APP HIDE") // get Watt
		{
			SaveLogFile("PlasmaCleaning_BISS_TCP", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			g_pMainDlg->minimizeDialog();
		}
		else if (vecRecvData[i] == "CHECK") // get Watt
		{
			SaveLogFile("PlasmaCleaning_BISS_TCP", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1)))); //통신 상태 기록.

			if (g_pBISSCom->m_bSerialConnected == TRUE)
			{
				sendBISSOn();
			}
			else
			{
				sendBISSOff();
			}

			if (g_pBISSCom->m_bisPowerON == TRUE)
			{
				sendPlasmaCleaningOn();
			}
			else
			{
				sendPlasmaCleaningOff();
			}
		}
		else
		{
			SaveLogFile("PlasmaCleaning_BISS_TCP", _T((LPSTR)(LPCTSTR)("Parsing Error, Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1))));
		}
	}
	vecRecvData.clear();
	return 0;
}

int CSREMCommunication::On_Accept()
{
	int nRet = 0;
	CString temp = "";

	TRACE("Accept Thread Start..\n");

	while (m_bKillAcceptThread == FALSE)
	{
		m_AcceptSocket = accept(m_Socket, NULL, NULL);
		if (m_AcceptSocket == INVALID_SOCKET)
		{
			closesocket(m_Socket);
			WSACleanup();
			break;
		}
		//temp.Format(_T("Accepted : %d"), m_Socket);
		//SaveLogFile("test", temp);
		m_bKillReceiveThread = FALSE;
		m_bConnected = TRUE;
		m_pReceiveThread = ::AfxBeginThread(ReceiveThreadFunc, this, THREAD_PRIORITY_NORMAL, 0, 0);
	}

	TRACE("Accept Thread End..\n");

	return nRet;
}

int CSREMCommunication::WaitReceiveEventThread()
{
	int		nRet = 0;
	int		i = 0;
	char*	pInBuf = new char[m_nReceiveMaxBufferSize];
	char*	pOutBuf = new char[m_nReceiveMaxBufferSize];
	int		total = 0;


	int		nCondition = 0;
	unsigned long	tmp = 0;
	struct timeval timeout;
	fd_set fds;

	SOCKET	tmpSocket = INVALID_SOCKET;
	if (m_bIsServerSocket)
	{
		tmpSocket = m_AcceptSocket;
	}
	else
	{
		tmpSocket = m_Socket;
	}

	//TRACE("TCPIP Receive Thread Start..\n");

	memset(pInBuf, '\0', m_nReceiveMaxBufferSize);
	while (m_bKillReceiveThread == FALSE)
	{
		m_bConnected = TRUE;
		timeout.tv_sec = 0;
		timeout.tv_usec = 100;
		FD_ZERO(&fds);
		FD_SET(m_Socket, &fds);
		m_nSocketStatus = select(sizeof(fds) * 8, &fds, NULL, NULL, &timeout);

		if (m_nSocketStatus == -1)
		{
			m_bKillReceiveThread = TRUE;
			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);

			CString Temp;
			Temp.Format(_T("::::::: m_nSocketStatus [ -1 ] IP [ %s ] Port [ %d ]"), m_IpAddr, m_nPort);
			SaveLogFile("TcpComm", (LPSTR)(LPCTSTR)Temp);
		}

		if (tmpSocket != INVALID_SOCKET)
		{
			memset(pOutBuf, '\0', m_nReceiveMaxBufferSize);
			int nResult = recv(tmpSocket, pInBuf, m_nReceiveMaxBufferSize, NULL);
			if (m_bKillReceiveThread)
			{
				break;
			}

			if (strcmp(m_IpAddr, "192.168.20.10") == 0)
				TRACE("nResult=%d, pInBuf=%s", nResult, pInBuf);
			/*	CString str;
				str = pInBuf;
				SaveLogFile("ADAM_Com", _T((LPSTR)(LPCTSTR)("ADAM -> MainPC_TCP/IP Socket:" + str + "Transfered")));*/
			SetEvent(m_hSocketEvent);
			total = 0;
			if (nResult > 0)
			{
				for (i = 0; i < nResult; i++)
				{
					pOutBuf[total] = pInBuf[i];
					if (pOutBuf[total] == (char)m_strReceiveEndOfStreamSymbol.GetAt(0))
					{
						pOutBuf[total] = '\0';
						nRet = ReceiveData(pOutBuf);
						m_nRet = nRet;
						memset(pOutBuf, '\0', m_nReceiveMaxBufferSize);
						total = 0;
						if (i == nResult)
							memset(pInBuf, '\0', m_nReceiveMaxBufferSize);
					}
					else
					{
						total++;
					}
				}
			}
			else
			{
				if (nResult == 0)
				{
					if (strcmp(m_IpAddr, "192.168.20.10") != 0)
					{
						m_bKillReceiveThread = TRUE;
						m_bConnected = FALSE;

						CString Temp;
						Temp.Format(_T("::::::: recv return value [ 0 ] IP [ %s ] Port [ %d ]"), m_IpAddr, m_nPort);
						SaveLogFile("TcpComm", (LPSTR)(LPCTSTR)Temp);
					}
				}


				if (nResult == SOCKET_ERROR)
				{
					m_bKillReceiveThread = TRUE;
					m_bConnected = FALSE;
					break;
				}
				//SaveLogFile("ADAM_Com", (LPSTR)(LPCTSTR)Temp); //ihlee test


			}
		}
		else
		{
			break;
		}
		//Sleep(1); //김영덕 변경
		usleep(50);
	}
	int iResult = 0;
	if (m_bIsServerSocket)
	{
		iResult = shutdown(m_AcceptSocket, SD_SEND);
		if (iResult == SOCKET_ERROR) {
			//	printf("shutdown failed with error: %d\n", WSAGetLastError());
			closesocket(m_AcceptSocket);
			WSACleanup();
		}
	}
	else
	{
		iResult = shutdown(m_Socket, SD_SEND);
		if (iResult == SOCKET_ERROR) {
			//	printf("shutdown failed with error: %d\n", WSAGetLastError());
			closesocket(m_Socket);
			WSACleanup();
		}
	}

	if (m_bIsServerSocket)
	{
		m_AcceptSocket = INVALID_SOCKET;
	}
	else
	{
		m_Socket = INVALID_SOCKET;
	}

	m_bConnected = FALSE;
	nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
	m_nRet = nRet;
	SetEvent(m_hSocketEvent);

	delete[] pInBuf;
	pInBuf = NULL;
	delete[] pOutBuf;
	pOutBuf = NULL;
	TRACE("TCPIP Receive Thread End..\n");

	CString Temp;
	Temp.Format(_T("::::::: Line TCPIP Socket Disconnected IP [ %s ] Port [ %d ]"), m_IpAddr, m_nPort);
	SaveLogFile("TcpComm", (LPSTR)(LPCTSTR)Temp);

	return nRet;
}

int CSREMCommunication::openSocketServer()
{
	if (m_bConnected == FALSE)
	{
		OpenTcpIpSocket((LPSTR)(LPCTSTR)_T("127.0.0.1"), 9192, TRUE);
	}
	return 0;
}

int CSREMCommunication::sendGetInterlock()
{
	int nRet = 0;
	m_isPreesureCheck = FALSE;
	m_isInterlock = FALSE;

	ResetEvent(m_hInterlcokCheckEvent);
	CString strTemp = _T("I\r\n");
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		SetEvent(m_hInterlcokCheckEvent);
	}
	else
	{
		nRet = CECommon::WaitEvent(m_hInterlcokCheckEvent, 10000);
	}

	return nRet;
}

int CSREMCommunication::sendPlasmaCleaningOn()
{
	int nRet = 0;
	//ResetEvent(m_hInterlcokCheckEvent);
	CString strTemp = _T("ON\r\n");
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		//SetEvent(m_hInterlcokCheckEvent);
	}
	else
	{
		//nRet = CECommon::WaitEvent(m_hInterlcokCheckEvent, 10000);
	}

	return nRet;
}

int CSREMCommunication::sendPlasmaCleaningOff()
{
	int nRet = 0;
	//ResetEvent(m_hInterlcokCheckEvent);
	CString strTemp = _T("OFF\r\n");
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		//SetEvent(m_hInterlcokCheckEvent);
	}
	else
	{
		//nRet = CECommon::WaitEvent(m_hInterlcokCheckEvent, 10000);
	}

	return nRet;
}

int CSREMCommunication::sendBISSOn()
{
	int nRet = 0;
	//ResetEvent(m_hInterlcokCheckEvent);
	CString strTemp = _T("BISSOn\r\n");
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		//SetEvent(m_hInterlcokCheckEvent);
	}
	else
	{
		//nRet = CECommon::WaitEvent(m_hInterlcokCheckEvent, 10000);
	}

	return nRet;
}

int CSREMCommunication::sendBISSOff()
{
	int nRet = 0;
	//ResetEvent(m_hInterlcokCheckEvent);
	CString strTemp = _T("BISSOff\r\n");
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		//SetEvent(m_hInterlcokCheckEvent);
	}
	else
	{
		//nRet = CECommon::WaitEvent(m_hInterlcokCheckEvent, 10000);
	}

	return nRet;
}

int CSREMCommunication::sendTime(bool isRemainTime)
{
	int nRet = 0;
	//ResetEvent(m_hInterlcokCheckEvent);
	CString strTemp = "";
	if (isRemainTime == TRUE)
	{
		//CTime curTime = CTime::GetCurrentTime();
		//CTimeSpan timeTemp;
		//timeTemp = g_pPlasmaDlg->m_Starttime - curTime;
		//strTemp.Format(_T("TimeR%02d:%02d:%02d\r\n"), timeTemp.GetHours(), timeTemp.GetMinutes(), timeTemp.GetSeconds());
		CString strTempTime = "";
		g_pPlasmaDlg->GetDlgItemTextA(IDC_PLASMA_EDIT_DISPCURRENTTIME, strTempTime);
		strTemp.Format(_T("TimeR%s\r\n"), strTempTime);
	}
	else
	{
		CString strStartTime = "", strEndTime = "";
		g_pPlasmaDlg->GetDlgItemTextA(IDC_PLASMA_EDIT_DISPSTARTTIME, strStartTime);
		g_pPlasmaDlg->GetDlgItemTextA(IDC_PLASMA_EDIT_DISPENDTIME, strEndTime);
		strTemp.Format(_T("Time,%s,%s\r\n"), strStartTime, strEndTime);
	}

	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		//SetEvent(m_hInterlcokCheckEvent);
	}
	else
	{
		//nRet = CECommon::WaitEvent(m_hInterlcokCheckEvent, 10000);
	}

	return nRet;
}

int CSREMCommunication::sendThreadStart()
{
	int nRet = 0;
	//ResetEvent(m_hInterlcokCheckEvent);
	CString strTemp = "";
	strTemp = _T("ThreadStart\r\n");
	
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		//SetEvent(m_hInterlcokCheckEvent);
	}
	else
	{
		//nRet = CECommon::WaitEvent(m_hInterlcokCheckEvent, 10000);
	}

	return nRet;
}

int CSREMCommunication::sendThreaddEnd()
{
	int nRet = 0;
	//ResetEvent(m_hInterlcokCheckEvent);
	CString strTemp = "";
	strTemp = _T("ThreadEnd\r\n");

	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		//SetEvent(m_hInterlcokCheckEvent);
	}
	else
	{
		//nRet = CECommon::WaitEvent(m_hInterlcokCheckEvent, 10000);
	}

	return nRet;
}

int CSREMCommunication::sendActualParameter()
{
	int nRet = 0;
	//ResetEvent(m_hInterlcokCheckEvent);
	CString strTemp = "";
	//strTemp.Format(_T("ActualP,%02d,%02d,%02d,%02d,%02d,%02d,%02d,%02d\r\n"), _ttoi(g_pPlasmaDlg->m_strDispActualWatt), _ttoi(g_pPlasmaDlg->m_strDispActualWattR), _ttoi(g_pPlasmaDlg->m_strDispActualHour),
	//	_ttoi(g_pPlasmaDlg->m_strDispActualMinute), _ttoi(g_pPlasmaDlg->m_strDispActualSecond), _ttoi(g_pPlasmaDlg->m_strDispActualVDC), _ttoi(g_pPlasmaDlg->m_strDispActualTorr), g_pBISSCom->m_bisPowerON);
	strTemp.Format(_T("ActualP,%02d,%02d,%02d,%02d,%02d,%0.1f,%0.1e,%02d\r\n"), g_pPlasmaDlg->m_nActualWatt, g_pPlasmaDlg->m_nActualWattR, g_pPlasmaDlg->m_nActualHour, g_pPlasmaDlg->m_nActualMinute,
		g_pPlasmaDlg->m_nActualSecond, g_pPlasmaDlg->m_dActualVDC, g_pPlasmaDlg->m_dActualTorr, g_pBISSCom->m_bisPowerON);
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		//SetEvent(m_hInterlcokCheckEvent);
	}
	else
	{
		//nRet = CECommon::WaitEvent(m_hInterlcokCheckEvent, 10000);
	}

	return nRet;
}

int CSREMCommunication::sendSetParameter()
{
	int nRet = 0;
	//ResetEvent(m_hInterlcokCheckEvent);
	CString strTemp = "";
	strTemp.Format(_T("SetP,%s,%s,%s,%s\r\n"), g_pPlasmaDlg->m_strDispSetWatt, g_pPlasmaDlg->m_strDispSetHour,
		g_pPlasmaDlg->m_strDispSetMinute,g_pPlasmaDlg->m_strDispSetSecond);

	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		//SetEvent(m_hInterlcokCheckEvent);
	}
	else
	{
		//nRet = CECommon::WaitEvent(m_hInterlcokCheckEvent, 10000);
	}

	return nRet;
}

int CSREMCommunication::sendTimerParameter()
{
	int nRet = 0;
	BOOL isChcked = FALSE;
	if (g_pPlasmaDlg->IsDlgButtonChecked(IDC_PLASMA_CHECK) == BST_CHECKED)
	{
		isChcked = TRUE;
	}
	else
	{
		isChcked = FALSE;
	}
	CString strTemp = "", strStartTime = "", strEndTime = "";
	CTime timeTemp;
	g_pPlasmaDlg->GetDlgItemTextA(IDC_PLASMA_EDIT_DISPSTARTTIME, strStartTime);
	g_pPlasmaDlg->GetDlgItemTextA(IDC_PLASMA_EDIT_DISPENDTIME, strEndTime);
	((CDateTimeCtrl*)g_pPlasmaDlg->GetDlgItem(IDC_PLASMA_DATETIMEPICKER))->GetTime(timeTemp);
	int nYear = timeTemp.GetYear();
	int nMonth = timeTemp.GetMonth();
	int nDay = timeTemp.GetDay();
	strTemp.Format(_T("SetT,S%02d,H%02d,M%02d,%s,%s,%04d,%02d,%02d\r\n"), isChcked, _ttoi(g_pPlasmaDlg->m_strDispBookHour), _ttoi(g_pPlasmaDlg->m_strDispBookMinute), strStartTime, strEndTime, nYear, nMonth, nDay);
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	return nRet;
}
