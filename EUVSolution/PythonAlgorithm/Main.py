
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from PythonAlgorithm import *



###################### MAIN ########################## 
x = np.linspace(172.76676899999998, 172.77076900000012, 101)
offset = 172.768669
#refAbs =2000
#refMl = 8000

refAbs = np.array([2000.])
refMl = np.array([8000.])

refAbs[0]=2000
refMl[0]=8000
#dir = 0
#slope = 1
#y = AlignFunXIncrease(x, offset, refAbs, refMl)

dir = 0
slope = -1
y = AlignFunXDecrease(x, offset, refAbs, refMl)
y2 = AlignFunXIncrease(x, offset, refAbs, refMl)
plt.plot(x, y)
plt.plot(x, y2)

#dir = 1
#slope = 1
#y = AlignFunYIncrease(x, offset, refAbs, refMl)

#dir = 1
#slope = -1
#y = AlignFunYDecrease(x, offset, refAbs, refMl)

#plt.plot(x, y)

np.random.seed(1729)
err_noise = 1 + (np.random.rand(y.size) - 0.5) * 2 * 0.05 *0 # 5%
y_noise = y * err_noise
plt.plot(x, y_noise)

output = FineAlignCurveFit2(x, y_noise, dir, slope, refAbs, refMl)


plt.plot(x, output[8])
        
test =0