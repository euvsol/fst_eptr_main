
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from PythonAlgorithm import *
import math




#################################################################################

# FFT  그래프 그리기 1


#St=0.0005

#Fs = 1/St                    # Sampling frequency
Fs = 2000                    # Sampling frequency
T = 1/Fs                     # Sample interval time
t_end= 0.5                     # End of time
t = np.arange(0, t_end, T)   # Time vector (T간격으로 te까지)

## Sum of a 50 Hz sinusoid and a 120 Hz sinusoid
noise = np.random.normal(0,0.05,len(t))
x = 0.6*np.cos(2*np.pi*60*t+np.pi/2) + np.cos(2*np.pi*120*t) #주파수 60, 120Hz
y = x  + noise     # Sinusoids plus noise


n= 524288
#T =0.5
#y = np.array([1,2,3,4,5])
n_array = np.array([n], dtype = np.int32)
T_array = np.array([T], dtype = np.float64)

(magnitude,phase, frequency) = EPhaseFFT(y*1000,n_array,T_array)


#frequency = np.fft.rfftfreq(n, d=T)
#amplitude = np.fft.rfft(y,norm=None)
#magnitude = np.abs(amplitude)/n*2
#phase = np.angle(amplitude)*180/np.pi;


# figure 1 ..................................
plt.figure(num=2,dpi=100,facecolor='white')
plt.subplots_adjust(hspace = 0.6, wspace = 0.3)
plt.subplot(3,1,1)
plt.plot(t,y,'r')
plt.title('Signal FFT analysis')
plt.xlabel('time($sec$)')
plt.ylabel('y')
#plt.xlim( 0, 0.1)

# Amplitude ....
#plt.figure(num=2,dpi=100,facecolor='white')
plt.subplot(3,1,2)
# Plot single-sided amplitude spectrum.
#plt.plot(f0,amplitude_Hz,'r')   #  2* ???
plt.plot(frequency,magnitude,'b')   #  2* ???
plt.xticks(np.arange(0,500,20))
#plt.xlim( 0, 200)
#plt.ylim( 0, 1.2)
#plt.title('Single-Sided Amplitude Spectrum of y(t)')
plt.xlabel('frequency($Hz$)')
plt.ylabel('amplitude')
plt.grid()

# Phase ....
#plt.figure(num=2,dpi=100,facecolor='white')
plt.subplot(3,1,3)
#plt.plot(f0,phase_ang,'r')   #  2* ???
plt.plot(frequency,phase,'b')   #  2* ???
plt.xlim( 0, 200)
plt.ylim( -180, 180)
#plt.title('Single-Sided Phase Spectrum of y(t)')
plt.xlabel('frequency($Hz$)')
plt.ylabel('phase($deg.$)')
plt.xticks([0, 60, 120, 200])
plt.yticks([-180, -90, 0, 90, 180])
plt.grid()




#################################################################################
#plt.figure(3)
#N=1000
#t = np.arange(256)
#sp = np.fft.fft(np.sin(t), n=N)
#freq = np.fft.fftfreq(N)
#plt.plot(freq, sp.real, freq, sp.imag)
#plt.show()

#a= np.array([[1,2],[3,4]])

#################################################################################
#plt.figure(4)
#test1 = sinClass.sinWaveForm(amp = 1, freq=1, endTime = 5)
#test2 = sinClass.sinWaveForm(amp = 2, freq=5, endTime = 5)
#test3 = sinClass.sinWaveForm(amp = 4, freq=10, endTime = 5)

#t = test1.calcDomain()
#resultTest1 = test1.calcSinValue(t)
#resultTest2 = test2.calcSinValue(t)
#resultTest3 = test3.calcSinValue(t)

#Ts = test1.sampleTime 					# sampling interval
#Fs = 1/Ts 						# sampling rate
#t = test1.calcDomain()			
#y = resultTest1 + resultTest2 + resultTest3

#n = len(y) 					# length of the signal
#k = np.arange(n)
#T = n/Fs                  #총시간
#freq = k/T 					# two sides frequency range
#freq = freq[range(int(n/2))] 			# one side frequency range

#Y = np.fft.fft(y)/n 				# fft computing and normalization
#Y = Y[range(int(n/2))]

#fig, ax = plt.subplots(2, 1)
#ax[0].plot(t, y)
#ax[0].set_xlabel('Time')
#ax[0].set_ylabel('Amplitude')
#ax[0].grid(True)
#ax[1].plot(freq, abs(Y), 'r', linestyle=' ', marker='^') 
#ax[1].set_xlabel('Freq (Hz)')
#ax[1].set_ylabel('|Y(freq)|')
#ax[1].vlines(freq, [0], abs(Y))
#ax[1].grid(True)
#plt.show()

#################################################################################
#plt.figure(5)
#fs = 100 
#t = np.arange(0, 3, 1 / fs)
#f1 = 35
#f2 = 10
#signal = 0.6 * np.sin(2 * np.pi * f1 * t) + 3 * np.cos(2 * np.pi * f2 * t + np.pi/2)
 
#fft = np.fft.fft(signal) / len(signal)  #normalization 
 
#fft_magnitude = abs(fft)    

#length = len(signal)
#f = np.linspace(-(fs / 2), fs / 2, length)   
#plt.stem(f, np.fft.fftshift(fft_magnitude)) 
#plt.ylim(0,2.5)                                                                                     
#plt.grid()                                                                                 
 
#plt.show()


##

#y_fft = np.fft.fftshift(np.fft.fft(y).real)
#plt.plot(x, y_fft)

#freqs = np.fft.fftfreq(10, 0.1) # n, Window length.d spacing
##freqs
##array([ 0.,  1.,  2., ..., -3., -2., -1.])
#np.fft.fftshift(freqs)
#array([-5., -4., -3., -2., -1.,  0.,  1.,  2.,  3.,  4.])
#################################

#from numpy import fft
#import numpy as np
#import matplotlib.pyplot as plt
#n = 1000 # Number of data points
#dx = 5.0 # Sampling period (in meters)
#x = dx*np.arange(0,n) # x coordinates
#w1 = 110.0 # wavelength (meters)
#w2 = 20.0 # wavelength (meters)
#fx = np.sin(2*np.pi*x/w1) + 2*np.cos(2*np.pi*x/w2) # signal
#Fk = fft.fft(fx)/n # Fourier coefficients (divided by n)
#nu = fft.fftfreq(n,dx) # Natural frequencies
#Fk = fft.fftshift(Fk) # Shift zero freq to center
#nu = fft.fftshift(nu) # Shift zero freq to center
#f, ax = plt.subplots(3,1,sharex=True)
#ax[0].plot(nu, np.real(Fk)) # Plot Cosine terms
#ax[0].set_ylabel(r'$Re[F_k]$', size = 'x-large')
#ax[1].plot(nu, np.imag(Fk)) # Plot Sine terms
#ax[1].set_ylabel(r'$Im[F_k]$', size = 'x-large')
#ax[2].plot(nu, np.absolute(Fk)**2) # Plot spectral power
#ax[2].set_ylabel(r'$\vert F_k \vert ^2$', size = 'x-large')
#ax[2].set_xlabel(r'$\widetilde{\nu}$', size = 'x-large')
#plt.show()



#plt.figure(2)
#Fk1 =fft.rfft(fx)/n  ###한쪽만 계산
#f = fft.fftfreq(n)
#freq = np.abs(f[0:int(n/2+1)]) # odd freq = f[0:n/2+1]
#plt.plot(freq,Fk1,'r') 

#test =0;

#dot = x1*x2 + y1*y2      # dot product AB cos(theata)
#det = x1*y2 - y1*x2      # determinant AB sin(theta)
#angle = atan2(det, dot)  # atan2(y, x) or atan2(sin, cos)


#import math
#a,b,c,d = input().split(" ")
#a,b,c,d=int(a),int(b),int(c),int(d)

#def angle_of_vectors(a,b,c,d):
    
#     dotProduct = a*c + b*d
#         # for three dimensional simply add dotProduct = a*c + b*d  + e*f 
#     modOfVector1 = math.sqrt( a*a + b*b)*math.sqrt(c*c + d*d) 
#         # for three dimensional simply add modOfVector = math.sqrt( a*a + b*b + e*e)*math.sqrt(c*c + d*d +f*f) 
#     angle = dotProduct/modOfVector1
#     print("Cosθ =",angle)
#     angleInDegree = math.degrees(math.acos(angle))
#     print("θ =",angleInDegree,"°")



