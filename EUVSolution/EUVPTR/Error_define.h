#pragma once

//1. Loading/Unloading Process Error



//2. Pumping/Venting Process Error



//3. Ethernet/Serial Communication Error



//4. MTS Error



//5. 



//[Windows 64bit C++ 응용 프로그램 개발시 유의 사항]
//
//64bit 프로그램 개발 시 가장 큰 변화는 바로 데이터 타입의 크기 변화입니다. 
//64bit 환경에서는 많은 데이터 타입들이 자연스럽게 8byte 크기로 변경되었고, 반면에 어떤 데이터 타입들은 4byte를 그대로 유지하고 있는 것도 있습니다.
//
//64bit 사이즈를 가지는 데이터 타입

//
//• 일반적인 포인터 타입(ex: int*, char*)
//• HANDLE 타입(HWND, HDC, HBITMAP 등의 모든 핸들)
//• WPARAM, LPARAM, LRESULT
//• size_t, time_t
//• 기본 타입명 + _PTR 의 이름을 가지는 타입(HALF_PTR 제외, ex: INT_PTR, DWORD_PTR)
//• 이름에 64를 포함한 타입(ex: INT64, ULONG64)
//
//32bit 사이즈를 가지는 데이터 타입

//
//• int, long
//• DWORD, HRESULT
//• 이름에 32를 포함한 타입(ex: INT32, ULONG32)


//<ALARM_CODE 체계>
//-XXXX : -X(Module) X(Sub Module) XX(ALARM종류)
//
//< ALARM_CODE 분류 >
//MTS		Unknown					- 19__
//					OPENER			- 11__
//					ATR				- 12__
//					ROTATOR			- 13__
//					FLIPPER			- 14__
//VMTR		Unknown					- 29__
//STAGE		Unknown					- 39__
//					XYSTAGE			- 31__
//					SCANSTAGE		- 32__
//					SQ1STAGE		- 33__
//					PI1AXISSTAGE	- 34__
//EUV		Unknown					- 49__
//OM		Unknown					- 59__
//					CAMERA			- 51__
//					LAMP			- 52__
//					AF				- 53__
//ADAM		Unknown					- 69__
//SOURCE	Unknown					- 79__
//					LASER			- 71__
//UILITY	Unknown					- 89__
//					MC PUMP			- 81__
//					LLC PUMP		- 82__
//					SENSOR			- 83__
//					AIR				- 84__
//					WATER			- 85__
//					N2				- 86__
//					IO				- 87__
//PROCESS	Unknown					- 99__
//					LOADING			- 91__
//					UNLOADING		- 92__
//					ALIGN			- 93__
//					MESURE			- 94__
//					AUTOMATION		- 95__
//					DATA R / W		- 96__
//					Vacuum			- 97__
//					Venting			- 98__
//
//< ALARM_CATEGORY>
//1 : Critical = > 더이상 진행불가.엔지니어 조치 필요

//2 : Warning = > 현재 Job 진행 불가.다음 Job 계속 진행 가능



//설비 ERROR CODE(Error Code: -XXXXX(1~2 XX: Module(10:설비Common,11:MTS,12:VMTR,13:Navigation Stage,14:Scan Stage,...), 3~5 XXX: Error 종류))  ---> 문서로 따로 정리

#define SYSTEM_OK								0
#define SYSTEM_ERROR							-10001
#define SYSTEM_STAGE_NOT_LOADING_POS			-10009
#define SYSTEM_STAGE_NOT_UNLOADING_POS			-10010

#define SYSTEM_ISOLATOR_NOT_WORKING				-10020



#define STAGE_COMMUNICATION_FAIL				-13001
#define SCANSTAGE_COMMUNICATION_FAIL			-14001
#define LLC_VENTING_TIMEOUT						-15001
#define LLC_PUMPING_TIMEOUT						-15002
//#define MC_VENTING_TIMEOUT						-16001
//#define MC_PUMPING_TIMEOUT						-16002

//MTS ERROR
#define MTS_COMMUNICATION_ACK_TIMEOUT			-11001
#define MTS_COMMUNICATION_ROTATOR_DONE_TIMEOUT	-11002
#define MTS_COMMUNICATION_FLIPPER_DONE_TIMEOUT	-11003
#define MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT	-11004
#define MTS_COMMUNICATION_LPM_DONE_TIMEOUT		-11005
#define MTS_COMMUNICATION_INIT_TIMEOUT			-11006
#define MTS_COMMUNICATION_TAG_READ_TIMEOUT		-11007
#define MTS_COMMUNICATION_SPEED_READ_TIMEOUT	-11008
#define	MTS_INITIALIZE_ALL_MODULE_ERROR			-11009
#define MTS_INITIALIZE_LPM_ERROR				-11010
#define MTS_INITIALIZE_ROBOT_ERROR				-11011
#define MTS_INITIALIZE_ROTATOR_ERROR			-11012
#define MTS_INITIALIZE_FLIPPER_ERROR			-11013
#define	MTS_LOADING_POD_ERROR					-11014
#define MTS_UNLOADING_POD_ERROR					-11015
#define MTS_OPEN_POD_ERROR						-11016
#define MTS_CLOSE_POD_ERROR						-11017
#define MTS_ROTATE_MASK_ERROR					-11018
#define MTS_ORIGIN_ROTATOR_ERROR				-11019
#define MTS_FLIP_MASK_ERROR						-11020
#define MTS_ORIGIN_FLIPPER_ERROR				-11021
#define MTS_ROBOT_HOME_ERROR					-11022
#define MTS_ROBOT_PICK_LPM_ERROR				-11023
#define MTS_ROBOT_PLACE_LPM_ERROR				-11024
#define MTS_ROBOT_PICK_ROTATOR_ERROR			-11025
#define MTS_ROBOT_PLACE_ROTATOR_ERROR			-11026
#define MTS_ROBOT_PICK_FLIPPER_ERROR			-11027
#define MTS_ROBOT_PLACE_FLIPPER_ERROR			-11028
#define MTS_ROBOT_PICK_LLC_STAGE_ERROR			-11029
#define	MTS_ROBOT_PLACE_LLC_STAGE_ERROR			-11030
#define MTS_ROBOT_MOVE_READY_POS_ERROR			-11031



//VMTR ERROR								
#define VMTR_COMMUNICATION_ACK_TIMEOUT			-12001
#define VMTR_COMMUNICATION_DONE_TIMEOUT			-12002
#define VMTR_COMMUNICATION_REQ_TIMEOUT			-12003
#define VMTR_SERVO_DRIVER_ERROR					-12004
#define VMTR_SOFTWARE_LIMIT_ERROR				-12005
#define VMTR_SERVO_OFF_ERROR					-12006
#define VMTR_ROBOT_POSITION_ERROR				-12007
#define VMTR_EMERGENCY_ERROR					-12008
#define VMTR_SLOT_NUMBER_ERROR					-12009
#define VMTR_ARM_SELECT_ERROR					-12010
#define VMTR_HOME_POSITION_ERROR				-12011
#define VMTR_ROBOT_STOP_ERROR					-12012
#define VMTR_CONTROLLER_BATTERY_ERROR			-12013
#define VMTR_CONTROLLER_CPU_ERROR				-12014
#define VMTR_GOTO_MOTION_ERROR					-12015
#define VMTR_AWC_RANGE_ERROR					-12016
#define VMTR_MECHATROLINK_SYNC_ERROR			-12017
#define VMTR_MECHATROLINK_COMM_ERROR			-12018
#define VMTR_TORQUE_LIMIT_ERROR					-12019
#define VMTR_T_AXIS_POSITION_MOVING_ERROR		-12020
#define VMTR_TR_AXIS_POSITION_MOVING_ERROR		-12021
#define VMTR_UNKNOWN_COMMAND					-12022
#define VMTR_TOO_FEW_ARGUMENT					-12023
#define VMTR_ILLEGAL_STATION_NO					-12024
#define VMTR_ILLEGAL_ARM_SELECT					-12025
#define VMTR_ILLEGAL_SLOT_NO					-12026
#define VMTR_ILLEGAL_GOTO_POSITION				-12027
#define VMTR_DELTA_PICK_LIMIT					-12028
#define VMTR_ILLEGAL_OPERATION					-12029
#define VMTR_UNKNOWN_ARGUMENT					-12030
#define VMTR_OFFLINE_MODE						-12031
#define VMTR_ROBOT_IS_RUNNING					-12032
#define VMTR_SERVO_OFF							-12033
#define VMTR_ROBOT_IS_ERROR_STATE				-12034
#define VMTR_ROBOT_IS_NOT_HOME_YET				-12035
#define VMTR_EXIST_MASK_LLC_AFTER_LLC_PICK		-12036
#define VMTR_TILT_MASK_LLC_AFTER_LLC_PICK		-12037
#define VMTR_NO_MASK_ROBOT_AFTER_LLC_PICK		-12038
#define VMTR_NO_MASK_LLC_AFTER_LLC_PLACE		-12039
#define VMTR_TILT_MASK_LLC_AFTER_LLC_PLACE		-12040
#define VMTR_EXIST_MASK_ROBOT_AFTER_LLC_PLACE	-12041
#define VMTR_EXIST_MASK_MC_AFTER_MC_PICK		-12042
#define VMTR_TILT_MASK_MC_AFTER_MC_PICK			-12043
#define VMTR_NO_MASK_ROBOT_AFTER_MC_PICK		-12044
#define VMTR_NO_MASK_MC_AFTER_MC_PLACE			-12045
#define VMTR_TILT_MASK_MC_AFTER_MC_PLACE		-12046
#define VMTR_EXIST_MASK_ROBOT_AFTER_MC_PLACE	-12047
#define VMTR_CAM_SENSOR_TRIGGER_ERROR			-12048

//LIGHT CONTROLLER
#define LIGHT_COMMUNICATION_ACK_TIMEOUT			-16001
#define LIGHT_NOT_REMOTE_MODE_ERROR				-16002
#define LIGHT_CHANNEL_ERROR						-16003
#define LIGHT_COMMAND_ERROR						-16004
#define LIGHT_PARAMETER_ERROR					-16005
#define LIGHT_CHECKSUM_ERROR					-16006
#define LIGHT_UNKNOWN_ERROR						-16007

//Loading / Unloading Sequence
#define SEQ_ALREADY_JOB_WORKING					-18001
#define SEQ_UNKNOWN_VACUUM_STATE				-18002
#define SEQ_FAIL_LOADING_PROCESS				-18003
#define SEQ_FAIL_UNLOADING_PROCESS				-18004
#define SEQ_FAIL_LLC_PUMPING_PROCESS			-18005
#define SEQ_FAIL_LLC_VENTING_PROCESS			-18006
#define SEQ_FAIL_RECOVERY_PROCESS				-18007
#define SEQ_NO_POD_ON_LPM						-18008
#define SEQ_POD_NOT_OPENED						-18009
#define SEQ_DETECTED_SEVERAL_MASK_IN_EQ			-18010
#define SEQ_NO_MASK_IN_EQ						-18011
#define SEQ_NO_MASK_ON_LLC						-18012
#define SEQ_TILT_MASK_ON_LLC					-18013
#define SEQ_EXIST_MASK_ON_LLC					-18014
#define SEQ_NO_MASK_ON_CHUCK					-18015
#define SEQ_TILT_MASK_ON_CHUCK					-18016
#define SEQ_EXIST_MASK_ON_CHUCK					-18017
#define SEQ_NO_MASK_IN_POD						-18018
#define SEQ_EXIST_MASK_IN_POD					-18019
#define SEQ_NO_MASK_ON_ROTATOR					-18020
#define SEQ_EXIST_MASK_ON_ROTATOR				-18021
#define SEQ_NO_MASK_ON_FLIPPER					-18022
#define SEQ_EXIST_MASK_ON_FLIPPER				-18023
#define SEQ_NO_MASK_ON_MTS_ROBOT				-18024
#define SEQ_EXIST_MASK_ON_MTS_ROBOT				-18025
#define SEQ_NO_MASK_ON_VAC_ROBOT				-18026
#define SEQ_EXIST_MASK_ON_VAC_ROBOT				-18027
#define SEQ_OPEN_LLC_GATE_VALVE_ERROR			-18028
#define SEQ_CLOSE_LLC_GATE_VALVE_ERROR			-18029
#define SEQ_OPEN_TR_GATE_VALVE_ERROR			-18030
#define SEQ_CLOSE_TR_GATE_VALVE_ERROR			-18031
#define SEQ_LLC_GATE_VALVE_NOT_OPENED			-18032
#define SEQ_TR_GATE_VALVE_NOT_OPENED			-18033
#define SEQ_ATM_ROBOT_NOT_RETRACTED				-18034
#define SEQ_VAC_ROBOT_NOT_RETRACTED				-18035
#define SEQ_CAM_SENSOR_TRIGGER_ON_ERROR			-18036
#define SEQ_CAM_SENSOR_TRIGGER_OFF_ERROR		-18037
#define SEQ_MTS_LPM_WORKING						-18038
#define SEQ_MTS_ROTATOR_WORKING					-18039
#define SEQ_MTS_FLIPPER_WORKING					-18040



// DRY PUMP										- 80__
#define MC_DRY_PUMP_ERROR_OFF					-80001
#define LLC_DRY_PUMP_ERROR_OFF					-80002
#define MC_DRY_PUMP_ALARM						-80003
#define LLC_DRY_PUMP_ALARM						-80004
#define LLC_DRY_PUMP_ALARM_STATE				-80004
#define LLC_DRY_PUMP_ERROR_STATE				-80005
#define LLC_DRY_PUMP_STOP_STATE					-80006
#define MC_DRY_PUMP_ALARM_STATE					-80007
#define MC_DRY_PUMP_ERROR_STATE					-80008
#define MC_DRY_PUMP_STOP_STATE					-80009

// MC TMP										- 81__
#define MC_TMP_CONNECTION_ERROR					-81001
#define MC_TMP_SEND_ERROR						-81002
#define MC_TMP_ACCELERATION_STATE				-82003
#define MC_TMP_DECELERATION_STATE				-82004
#define MC_TMP_OFFLINE_STATE					-82005
#define MC_TMP_ERROR_STATE						-82006

// LLC TMP										- 82__
#define LLC_TMP_CONNECTION_ERROR				-82001
#define LLC_TMP_SEND_ERROR						-82002
#define LLC_TMP_ACCELERATION_STATE				-82003
#define LLC_TMP_DECELERATION_STATE				-82004
#define LLC_TMP_OFFLINE_STATE					-82005
#define LLC_TMP_ERROR_STATE						-82006

// SENSOR										- 83__
#define VACCUM_SENSOR_CONNECTION_ERROR			-83001
#define VACCUM_SENSOR_SEND_ERROR				-83002
#define VACCUM_SENSOR_OFFLINE_STATE				-83003
#define VACCUM_SENSOR_ERROR_STATE				-83004

// AIR											- 84__
#define AIR_SUPPLY_ERROR						-84001

// WATER										- 85__
#define WATER_SUPPLY_ERROR						-85001
#define WATER_RETURN_TEMP_ALARM_ERROR			-85002

// N2											- 86__
#define N2_SUPPLY_ERROR							-86001

//IO											- 87__
#define IO_WRITE_ERROR							-87000
#define IO_CONNECTION_ERROR						-87001
#define IO_OPER_MAINT_CHANGE_ERROR				-87002


#define IO_TOWER_LED_WRITE_ERROR									-87010
#define IO_TOWER_YELLOW_WRITE_ERROR									-87011
#define IO_TOWER_GREEN_WRITE_ERROR									-87012
#define IO_TOWER_BUZZER_WRITE_ERROR									-87013
#define IO_LLC_GATE_OPEN_WRITE_ERROR								-87014
#define IO_LLC_GATE_CLSOE_WRITE_ERROR								-87015
#define IO_LLC_SLOW_ROUGH_WRITE_ERROR								-87016
#define IO_LLC_FAST_ROUGH_WRITE_ERROR								-87017
#define IO_FAST_VENT_INLET_WRITE_ERROR								-87018
#define IO_SLOW_VENT_INLET_WRITE_ERROR								-87019
#define IO_MC_SLOW_ROUGH_WRITE_ERROR								-87020
#define IO_MC_FAST_ROUGH_WRITE_ERROR								-87021
#define IO_LLC_FORELINE_WRITE_ERROR									-87022
#define IO_MC_FORELINE_WRITE_ERROR									-87023
#define IO_FAST_VENT_OUTLET_WRITE_ERROR								-87024
#define IO_SLOW_VENT_OUTLET_WRITE_ERROR								-87025
#define IO_LLC_TMP_GATE_WRITE_ERROR									-87026
#define IO_MC_TMP_GATE_WRITE_ERROR									-87027
#define IO_TR_GATE_OPEN_WRITE_ERROR									-87028
#define IO_TR_GATE_CLOSE_WRITE_ERROR								-87029
#define IO_DOOR_COVER_1_WRITE_ERROR									-87030
#define IO_DOOR_COVER_2_WRITE_ERROR									-87031
#define IO_DOOR_COVER_3_WRITE_ERROR									-87032
#define IO_DOOR_COVER_4_WRITE_ERROR									-87033
#define IO_DRY_PUMP_1_WRITE_ERROR									-87034
#define IO_DRY_PUMP_2_WRITE_ERROR									-87035
#define IO_DOOR_COVER_WRITE_ERROR									-87036
#define IO_CCD_CAMERA_WRITE_ERROR									-87037
#define IO_DVR_1_LAMP_WRITE_ERROR									-87038
#define IO_DVR_2_LAMP_WRITE_ERROR									-87039
#define IO_DVR_3_LAMP_WRITE_ERROR									-87040
#define IO_DVR_4_LAMP_WRITE_ERROR									-87041
#define IO_SHUTTER_WRITE_ERROR										-87042
#define IO_WATER_SUPPLY_WRITE_ERROR									-87043
#define IO_WATER_RETURN_WRITE_ERROR									-87044
#define IO_CAM_SENSOR_WRITE_ERROR									-87045

#define	CLOSE_SOURCEGATE_CHECK_ERROR								-86100		//Source Gate Close 에 실패하였습니다(TimeOut)
#define	OPEN_SOURCEGATE_CHECK_ERROR									-86101		//Source Gate Open 에 실패하였습니다(TimeOut)
#define	LLCGATEOPEN_LLCLINEATMSTATUSERROR							-86102		//LLC Gate Open 시 ATM 상태가 아니므로, 에러가 발생했습니다.
#define	LLCGATEOPEN_FASTVENTCLOSEERROR								-86103		//LLC Gate Open 시 FastVent Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCGATEOPEN_SLOWVENTCLOSEERROR								-86104		//LLC Gate Open 시 SlowVent Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCGATEOPEN_LLCFASTROUGHCLOSEERROR							-86105		//LLC Gate Open 시 LLC Fast Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCGATEOPEN_LLCSLOWROUGHCLOSEERROR							-86106		//LLC Gate Open 시 LLC Slow Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCGATEOPEN_LLCTMPGATECLOSEERROR							-86107		//LLC Gate Open 시 LLC Tmp Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCGATEOPEN_TRGATECLOSEERROR								-86108		//LLC Gate Open 시 TR Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCGATEOPEN_TIMEOUTERROR									-86109		//LLC Gate Open 시 TimeOut 에러가 발생했습니다.
#define	LLCGATEOPEN_WRITEERROR										-86110		//LLC Gate Open 시 IO Write Command 에러가 발생했습니다.
#define	LLCGATEOPEN_UNKNOWNERROR									-86111		//LLC Gate Open 시 에러가 발생했습니다.
#define	TRGATEOPEN_TIMEOUTERROR										-86112		//TR Gate Open 시 TimeOut 에러가 발생했습니다.
#define	TRGATEOPEN_WRITEERROR										-86113		//TR Gate Open 시 IO Write Command 에러가 발생했습니다.
#define	TRGATEOPEN_VACUUMRANGEERROR									-86114		//TR Gate Open 시 Open 가능 진공 값이 아니므로 에러가 발생했습니다.
#define	SLOWVENTINLETOPEN_TRGATECLOSEERROR							-86115		//Slow Vent Inlet Open 시 TR Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	SLOWVENTINLETOPEN_LLCGATECLOSEERROR							-86116		//Slow Vent Inlet Open 시 LLC Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	SLOWVENTINLETOPEN_LLCFORELINECLOSEERROR						-86117		//Slow Vent Inlet Open 시 LLC Foreline Close 되어 있지 않아 에러가 발생했습니다.
#define	SLOWVENTINLETOPEN_LLCFASTROUGHCLOSEERROR					-86118		//Slow Vent Inlet Open 시 LLC Fast Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	SLOWVENTINLETOPEN_LLCSLOWROUGHCLOSEERROR					-86119		//Slow Vent Inlet Open 시 LLC Slow Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	SLOWVENTINLETOPEN_FASTVENTINLETCLOSEERROR					-86120		//Slow Vent Inlet Open 시 Fast Vent Inlet Close 되어 있지 않아 에러가 발생했습니다.
#define	SLOWVENTINLETOPEN_FASTVENTOUTLETCLOSEERROR					-86121		//Slow Vent Inlet Open 시 Fast Vent Outlet Close 되어 있지 않아 에러가 발생했습니다.
#define	SLOWVENTINLETOPEN_WRITEERROR								-86122		//Slow Vent Inlet Open 시 IO Write Command 에러가 발생했습니다.
#define	SLOWVENTINLETOPEN_UNKNOWNERROR								-86123		//Slow Vent Inlet Open 시 에러가 발생했습니다.
#define	SLOWVENTOUTLETOPEN_SLOWVENTINLETOPENERROR					-86124		//Slow Vent Outlet Open 시 Slow Vent Inlet Open 되어 있지 않아 에러가 발생했습니다.
#define	SLOWVENTOUTLETOPEN_WRITEERROR								-86125		//Slow Vent Outlet Open 시 IO Write Command 에러가 발생했습니다.
#define	SLOWVENTOUTLETOPEN_UNKNOWNERROR								-86126		//Slow Vent Outlet Open 시 에러가 발생했습니다.
#define	SLOWVENTOPEN_TRGATEOPENERROR								-86127		//Slow Vent Open 시 TR Gate Close 되어 있지 않아 에러가 발생했습니다. (MC Venting Sequence 시에는 X)
#define	SLOWVENTOPEN_LLCGATEOPENERROR								-86128		//Slow Vent Open 시 LLC Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	SLOWVENTOPEN_LLCFASTROUGHOPENERROR							-86129		//Slow Vent Open 시 LLC Fast Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	SLOWVENTOPEN_TIMEOUTERROR									-86130		//Slow Vent Open 시 TimeOut 에러가 발생했습니다.
#define	SLOWVENTOPEN_WRITEERROR										-86131		//Slow Vent Open 시 IO Write Command 에러가 발생했습니다.
#define	SLOWVENTOPEN_UNKNOWNERROR									-86132		//Slow Vent Open 시 에러가 발생했습니다.
#define	FASTVENTINLETOPEN_MCFORELINEGATEOPENERROR					-86133		//Fast Vent Inlet Open 시 MC Foreline Gate Open 되어 있지 않아 에러가 발생했습니다. (TR OPEN 경우)
#define	FASTVENTINLETOPEN_LLCGATECLOSEERROR							-86134		//Fast Vent Inlet Open 시 LLC Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	FASTVENTINLETOPEN_LLCFORELINEGATEOPENERROR					-86135		//Fast Vent Inlet Open 시 LLC Foreline Gate Open 되어 있지 않아 에러가 발생했습니다.
#define	FASTVENTINLETOPEN_LLCTMPGATECLOSEERROR						-86136		//Fast Vent Inlet Open 시 LLC Tmp Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	FASTVENTINLETOPEN_MCTMPGATECLOSEERROR						-86137		//Fast Vent Inlet Open 시 MC Tmp Gate Close 되어 있지 않아 에러가 발생했습니다.  (TR OPEN 경우)
#define	FASTVENTINLETOPEN_LLCFASTROUGHCLOSEERROR					-86138		//Fast Vent Inlet Open 시 LLC Fast Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	FASTVENTINLETOPEN_LLCSLOWROUGHCLOSEERROR					-86139		//Fast Vent Inlet Open 시 LLC Slow Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	FASTVENTINLETOPEN_MCFASTROUGHCLOSEERROR						-86140		//Fast Vent Inlet Open 시 MC Fast Rough Close 되어 있지 않아 에러가 발생했습니다.  (TR OPEN 경우)
#define	FASTVENTINLETOPEN_MCSLOWROUGHCLOSEERROR						-86141		//Fast Vent Inlet Open 시 MC Slow Rough Close 되어 있지 않아 에러가 발생했습니다.  (TR OPEN 경우)
#define	FASTVENTINLETOPEN_SLOWVENTINLETCLOSEERROR					-86142		//Fast Vent Inlet Open 시 Slow Vent Inlet Valve Close 되어 있지 않아 에러가 발생했습니다.
#define	FASTVENTINLETOPEN_SLOWVENTOUTLETCLOSEERROR					-86143		//Fast Vent Inlet Open 시 Slow Vent Outlet Valve Close 되어 있지 않아 에러가 발생했습니다.
#define	FASTVENTINLETOPEN_TIMEOUTERROR								-86144		//Fast Vent Open 시 TimeOut 에러가 발생했습니다.
#define	FASTVENTINLETOPEN_WRITEERROR								-86145		//Fast Vent Open 시 IO Write Command 에러가 발생했습니다.
#define	FASTVENTINLETOPEN_UNKNOWNERROR								-86146		//Fast Vent Open 시 에러가 발생했습니다.
#define	FASTVENTOUTLETOPEN_FASTVENTINLETOPENERROR					-86147		//Fast Vent Outlet Open 시 Fast Vent Outlet Open 되어 있지 않아 에러가 발생했습니다.
#define	FASTVENTOUTLETOPEN_WRITEERROR								-86148		//Fast Vent Outlet Open 시 IO Write Command 에러가 발생했습니다.
#define	FASTVENTOUTLETOPENU_NKNOWNERROR								-86149		//Fast Vent Outlet Open 시 에러가 발생했습니다.
#define	FASTVENTOPEN_TRGATECLOSEERROR								-86150		//Fast Vent Open 시 TR Gate Close 되어 있지 않아 에러가 발생했습니다. (MC Venting Sequence 시에는 X)
#define	FASTVENTOPEN_LLCGATECLOSEERROR								-86151		//Fast Vent Open 시 LLC Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	FASTVENTOPEN_LLCFASTROUGHCLOSEERROR							-86152		//Fast Vent Open 시 LLC Roughing Valve Close 되어 있지 않아 에러가 발생했습니다.
#define	FASTVENTOPEN_TIMEOUTERROR									-86153		//Fast Vent Open 시 TimeOut 에러가 발생했습니다.
#define	FASTVENTOPEN_WRITEERROR										-86154		//Fast Vent Open 시 IO Write Command 에러가 발생했습니다.
#define	FASTVENTOPEN_UNKNOWNERROR									-86155		//Fast Vent Open 시 에러가 발생했습니다.
#define	MCSLOWROUGHOPEN_DRYPUMPERROR								-86156		//MC Slow Rough Open 시 MC Dry Pump 정상 구동 상태가 되어 있지 않아 에러가 발생했습니다.
#define	MCSLOWROUGHOPEN_MCFORELINECLOSEERROR						-86157		//MC Slow Rough Open 시 MC Foreline Valve Close 되어 있지 않아 에러가 발생했습니다.
#define	MCSLOWROUGHOPEN_MCTMPGATECLOSEERROR							-86158		//MC Slow Rough Open 시 MC Tmp Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	MCSLOWROUGHOPEN_LASERSOURCECLOSEERROR						-86159		//MC Slow Rough Open 시 Laser Source Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	MCSLOWROUGHOPEN_MCFASTROUGHCLOSEERROR						-86160		//MC Slow Rough Open 시 MC Fast Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	MCSLOWROUGHOPEN_TRGATECLOSEERROR							-86161		//MC Slow Rough Open 시 TR Gate Close 되어 있지 않아 에러가 발생 했습니다

#define	MCSLOWROUGHOPEN_TIMEOUTERROR								-86162		//MC Slow Rough Open 시 TimeOut 에러가 발생했습니다.
#define	MCSLOWROUGHOPEN_WRITEERROR									-86163		//MC Slow Rough Open 시 IO Write Command 에러가 발생했습니다.
#define	MCSLOWROUGHOPEN_UNKNOWNERROR								-86164		//MC Slow Rough Open 시 에러가 발생했습니다

#define	MCFASTROUGHOPEN_DRYPUMPERROR								-86165		//MC Fast Rough Open 시 MC Dry Pump 정상 구동 상태가 되어 있지 않아 에러가 발생했습니다.
#define	MCFASTROUGHOPEN_MCFORELINECLOSEERROR						-86166		//MC Fast Rough Open 시 MC Foreline Valve Close 되어 있지 않아 에러가 발생했습니다.
#define	MCFASTROUGHOPEN_MCTMPGATECLOSEERROR							-86167		//MC Fast Rough Open 시 MC Tmp Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	MCFASTROUGHOPEN_LASERSOURCECLOSEERROR						-86168		//MC Fast Rough Open 시 Laser Source Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	MCFASTROUGHOPEN_MCSLOWROUGHCLOSEERROR						-86169		//MC Fast Rough Open 시 MC Slow Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	MCFASTROUGHOPEN_TRGATECLOSEERROR							-86170		//MC Fast Rough Open 시 TR Gate Close 되어 있지 않아 에러가 발생 했습니다

#define	MCFASTROUGHOPEN_TIMEOUTERROR								-86171		//MC Fast Rough Open 시 TimeOut 에러가 발생했습니다.
#define	MCFASTROUGHOPEN_WRITEERROR									-86172		//MC Fast Rough Open 시 IO Write Command 에러가 발생했습니다.
#define	MCFASTROUGHOPEN_UNKNOWNERROR								-86173		//MC Fast Rough Open 시 에러가 발생했습니다

#define	MCTMPGATEOPEN_MCFASTROUGHCLOSEERROR							-86174		//MC TMP Gate Open 시 MC Fast Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	MCTMPGATEOPEN_LASERSOURCECLOSEERROR							-86175		//MC TMP Gate Open 시 Laser Source Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	MCTMPGATEOPEN_TRGATECLOSEERROR								-86176		//MC TMP Gate Open 시 TR Gate Close 되어 있지 않아 에러가 발생 했습니다

#define	MCTMPGATEOPEN_DRYPUMPERROR									-86177		//MC TMP Gate Open 시 MC Dry Pump 정상 구동 상태가 되어 있지 않아 에러가 발생했습니다.
#define	MCTMPGATEOPEN_MCFORELINEOPENERROR							-86178		//MC TMP Gate Open 시 MC Foreline Valve Open 되어 있지 않아 에러가 발생했습니다.
#define	MCTMPGATEOPEN_TIMEOUTERROR									-86179		//MC TMP Gate Open 시 TimeOut 에러가 발생했습니다.
#define	MCTMPGATEOPEN_WRITEERROR									-86180		//MC TMP Gate Open 시 IO Write Command 에러가 발생했습니다.
#define	MCTMPGATEOPEN_UNKNOWNERROR									-86181		//MC TMP Gate Open 시 에러가 발생했습니다

#define	LLCSLOWROUGHOPEN_DRYPUMPERROR								-86182		//LLC Slow Roughing Open 시 LLC Dry Pump 정상 구동 상태가 되어 있지 않아 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN_LLCGATECLOSEERROR							-86183		//LLC Slow Roughing Open 시 LLC Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN_LLCFASTROUGHCLOSEERROR						-86184		//LLC Slow Roughing Open 시 LLC Fast Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN_FASTVENTCLOSEERROR							-86185		//LLC Slow Roughing Open 시 Fast Vent Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN_SLOWVENTCLOSEERROR							-86186		//LLC Slow Roughing Open 시 Slow Vent Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN_LLCTMPGATECLOSEERROR						-86187		//LLC Slow Roughing Open 시 LLC Tmp Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN_LLCFORELINECLOSEERROR						-86188		//LLC Slow Roughing Open 시 LLC Foreline Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN_TRGATECLOSEERROR							-86189		//LLC Slow Roughing Open 시 TR Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN_TIMEOUTERROR								-86190		//LLC Slow Roughing Open 시 TimeOut 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN_WRITEERROR									-86191		//LLC Slow Roughing Open 시 IO Write Command 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN_UNKNOWNERROR								-86192		//LLC Slow Roughing Open 시 에러가 발생했습니다

#define	LLCFASTROUGHOPEN_DRYPUMPERROR								-86193		//LLC Fast Roughing Open 시 LLC Dry Pump 정상 구동 상태가 되어 있지 않아 에러가 발생했습니다.
#define	LLCFASTROUGHOPEN_LLCGATECLOSEERROR							-86194		//LLC Fast Roughing Open 시 LLC Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCFASTROUGHOPEN_FASTVENTCLOSEERROR							-86195		//LLC Fast Roughing Open 시 Fast Vent Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCFASTROUGHOPEN_SLOWVENTCLOSEERROR							-86196		//LLC Fast Roughing Open 시 Slow Vent Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCFASTROUGHOPEN_LLCTMPGATECLOSEERROR						-86197		//LLC Fast Roughing Open 시 LLC Tmp Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCFASTROUGHOPEN_LLCFORELINECLOSEERROR						-86198		//LLC Fast Roughing Open 시 LLC Foreline Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCFASTROUGHOPEN_TRGATECLOSEERROR							-86199		//LLC Fast Roughing Open 시 TR Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCFASTROUGHOPEN_TIMEOUTERROR								-86200		//LLC Fast Roughing Open 시 TimeOut 에러가 발생했습니다.
#define	LLCFASTROUGHOPEN_WRITEERROR									-86201		//LLC Fast Roughing Open 시 IO Write Command 에러가 발생했습니다.
#define	LLCFASTROUGHOPEN_UNKNOWNERROR								-86202		//LLC Fast Roughing Open 시 에러가 발생했습니다

#define	LLCTMPGATEOPEN_LLCFASTROUGHCLOSEERROR						-86203		//LLC Tmp Gate Open 시 LLC Fast Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCTMPGATEOPEN_SLOWVENTCLOSEERROR							-86204		//LLC Tmp Gate Open 시 Slow Vent Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCTMPGATEOPEN_FASTVENTCLOSEERROR							-86205		//LLC Tmp Gate Open 시 Fast Vent Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCTMPGATEOPEN_LLCGATECLOSEERROR							-86206		//LLC Tmp Gate Open 시 LLC Gate Close 되어 있지 않아 에러가 발생했습니다

#define	LLCTMPGATEOPEN_TRGATECLOSEERROR								-86207		//LLC Tmp Gate Open 시 TR Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCTMPGATEOPEN_DRYPUMPERROR									-86208		//LLC Tmp Gate Open 시 LLC Dry Pump 정상 구동 상태가 되어 있지 않아 에러가 발생했습니다.
#define	LLCTMPGATEOPEN_LLCFORELINEOPENERROR							-86209		//LLC Tmp Gate Open 시 LLC Foreline Open 되어 있지 않아 에러가 발생했습니다.
#define	LLCTMPGATEOPEN_TIMEOUTERROR									-86210		//LLC Tmp Gate Open 시 TimeOut 에러가 발생했습니다.
#define	LLCTMPGATEOPEN_WRITEERROR									-86211		//LLC Tmp Gate Open 시 IO Write Command 에러가 발생했습니다.
#define	LLCTMPGATEOPEN_UNKNOWNERROR									-86212		//LLC Tmp Gate Open 시 에러가 발생했습니다

#define	LLCFORELINEOPEN_TRGATECLOSEERROR							-86213		//LLC Foreline Valve Open 시 TR Gate Close 되어 있지 않아 에러가 발생했습니다. (MC Venting Sequence 시에는 X)
#define	LLCFORELINEOPEN_DRYPUMPERROR								-86214		//LLC Foreline Valve Open 시 LLC Dry Pump 정상 구동 상태가 되어 있지 않아 에러가 발생했습니다.
#define	LLCFORELINEOPEN_LLCGATECLOSEERROR							-86215		//LLC Foreline Valve Open 시 LLC Gate Close 되어 있지 않아 에러가 발생했습니다

#define	LLCFORELINEOPEN_LLCTMPGATECLOSEERROR						-86216		//LLC Foreline Valve Open 시 LLC Tmp Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCFORELINEOPEN_SLOWVENTCLOSEERROR							-86217		//LLC Foreline Valve Open 시 Slow Vent Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCFORELINEOPEN_FASTVENTCLOSEERROR							-86218		//LLC Foreline Valve Open 시 Fast Vent Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCFORELINEOPEN_LLCFASTROUGHCLOSEERROR						-86219		//LLC Foreline Valve Open 시 LLC Fast Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCFORELINEOPEN_LLCSLOWROUGHCLOSEERROR						-86220		//LLC Foreline Valve Open 시 LLC Slow Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCFORELINEOPEN_TIMEOUTERROR								-86221		//LLC Foreline Valve Open 시 TimeOut 에러가 발생했습니다.
#define	LLCFORELINEOPEN_WRITEERROR									-86222		//LLC Foreline Valve Open 시 IO Write Command 에러가 발생했습니다.
#define	LLCFORELINEOPEN_UNKNOWNERROR								-86223		//LLC Foreline Valve Open 시 에러가 발생했습니다

#define	MCFORELINEOPEN_TRGATECLOSEERROR								-86224		//MC Foreline Valve Open 시 TR Gate Close 되어 있지 않아 에러가 발생했습니다. (MC Venting Sequence 시에는 X)
#define	MCFORELINEOPEN_DRYPUMPERROR									-86225		//MC Foreline Valve Open 시 MC Dry Pump 정상 구동 상태가 되어 있지 않아 에러가 발생했습니다.
#define	MCFORELINEOPEN_MCFASTROUGHCLOSEERROR						-86226		//MC Foreline Valve Open 시 MC Fast Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	MCFORELINEOPEN_MCSLOWROUGHCLOSEERROR						-86227		//MC Foreline Valve Open 시 MC Slow Rough Close 되어 있지 않아 에러가 발생했습니다.
#define	MCFORELINEOPEN_LASERSOURCECLOSEERROR						-86228		//MC Foreline Valve Open 시 Laser Source Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	MCFORELINEOPEN_TIMEOUTERROR									-86229		//MC Foreline Valve Open 시 TimeOut 에러가 발생했습니다.
#define	MCFORELINEOPEN_WRITEERROR									-86230		//MC Foreline Valve Open 시 IO Write Command 에러가 발생했습니다.
#define	MCFORELINEOPEN_UNKNOWNERROR									-86231		//MC Foreline Valve Open 시 에러가 발생했습니다

#define	LLCGATECLOSE_TIMEOUTERROR									-86232		//LLC Gate Valve Close 시 TimeOut 에러가 발생했습니다.
#define	LLCGATECLOSE_WRITEERROR										-86233		//LLC Gate Valve Close 시 IO Write Command 에러가 발생했습니다.
#define	LLCGATECLOSE_UNKNOWNERROR									-86234		//LLC Gate Valve Close 시 에러가 발생했습니다

#define	TRGATECLOSE_TIMEOUTERROR									-86235		//TR Gate Valve Close 시 TimeOut 에러가 발생했습니다.
#define	TRGATECLOSE_WRITEERROR										-86236		//TR Gate Valve Close 시 IO Write Command 에러가 발생했습니다.
#define	TRGATECLOSE_UNKNOWNERROR									-86237		//TR Gate Valve Close 시 에러가 발생했습니다

#define	TRGATECLOSE_VAC_ARM_RETRACT_ERROr							-86238		//TR Gate Valve Close 시  VAC ROBOT ARM RETRACT 되어 있지 않아 에러가 발생했습니다

#define	SLOWVENTOUTLETCLOSE_SlOWVENTINLETCLOSEERROR					-86239		//Slow Vent Outlet Valve Close 시 Slow Vent Inlet Close 되어 있지 않아 에러가 발생했습니다.
#define	SLOWVENTOUTLETCLOSE_WRITEERROR								-86240		//Slow Vent Outlet Valve Close 시 IO Write Command 에러가 발생했습니다.
#define	SLOWVENTCLOSE_TIMEOUTERROR									-86241		//Slow Vent Valve Close 시 TimeOut 에러가 발생했습니다.
#define	SLOWVENTCLOSE_WRITEERROR									-86242		//Slow Vent Valve Close 시 IO Write Command 에러가 발생했습니다.
#define	FASTVENTINLETCLOSE_TIMEOUTERROR								-86243		//Fast Vent Inlet Valve Close 시 TimeOut 에러가 발생했습니다.
#define	FASTVENTINLETCLOSE_WRITEERROR								-86244		//Fast Vent Inlet Valve Close 시 IO Write Command 에러가 발생했습니다.
#define	FASTVENTOUTLETCLOSE_TIMEOUTERROR							-86245		//Fast Vent Outlet Valve Close 시 TimeOut 에러가 발생했습니다.
#define	FASTVENTOUTLETCLOSE_WRITEERROR								-86246		//Fast Vent Outlet Valve Close 시 IO Write Command 에러가 발생했습니다.
#define	FASTVENTCLOSE_TIMEOUTERROR									-86247		//Fast Vent Valve Close 시 TimeOut 에러가 발생했습니다.
#define	FASTVENTCLOSE_WRITEERROR									-86248		//Fast Vent Valve Close 시 IO Write Command 에러가 발생했습니다.
#define	MCSLOWROUGHCLOSE_TIMEOUTERROR								-86249		//MC Slow Rough Valve Close 시 TimeOut 에러가 발생했습니다.
#define	MCSLOWROUGHCLOSE_WRITEERROR									-86250		//MC Slow Rough Valve Close 시 IO Write Command 에러가 발생했습니다.
#define	MCFASTROUGHCLOSE_TIMEOUTERROR								-86251		//MC Fast Rough Valve Close 시 TimeOut 에러가 발생했습니다.
#define	MCFASTROUGHCLOSE_WRITEERROR									-86252		//MC Fast Rough Valve Close 시 IO Write Command 에러가 발생했습니다.
#define	MCTMPGATECLOSE_TIMEOUTERROR									-86253		//MC Tmp Gate Close 시 TimeOut 에러가 발생했습니다.
#define	MCTMPGATECLOSE_WRITEERROR									-86254		//MC Tmp Gate Close 시 IO Write Command 에러가 발생했습니다.
#define	MCFORELINECLOSE_TIMEOUTERROR								-86255		//MC Foreline Gate Close 시 TimeOut 에러가 발생했습니다.
#define	MCFORELINECLOSE_WRITEERROR									-86256		//MC Foreline Gate Close 시 IO Write Command 에러가 발생했습니다.
#define	LLCSLOWROUGHCLOSE_TIMEOUTERROR								-86257		//LLC Slow Rough Valve Close 시 TimeOut 에러가 발생했습니다.
#define	LLCSLOWROUGHCLOSE_WRITEERROR								-86258		//LLC Slow Rough Valve Close 시 IO Write Command 에러가 발생했습니다.
#define	LLCFASTROUGHCLOSE_TIMEOUTERROR								-86259		//LLC Fast Rough Valve Close 시 TimeOut 에러가 발생했습니다.
#define	LLCFASTROUGHCLOSE_WRITEERROR								-86260		//LLC Fast Rough Valve Close 시 IO Write Command 에러가 발생했습니다.
#define	LLCTMPGATECLOSE_TIMEOUTERROR								-86261		//LLC Tmp Gate Close 시 TimeOut 에러가 발생했습니다.
#define	LLCTMPGATECLOSE_WRITEERROR									-86262		//LLC Tmp Gate Close 시 IO Write Command 에러가 발생했습니다.
#define	LLCFORELINECLOSE_TIMEOUTERROR								-86263		//LLC Foreline Gate Close 시 TimeOut 에러가 발생했습니다.
#define	LLCFORELINECLOSE_WRITEERROR									-86264		//LLC Foreline Gate Close 시 IO Write Command 에러가 발생했습니다.
#define	SLOWVENTINLETCLOSE_TIMEOUTERROR								-86265		//Slow Vent Inlet Valve Close 시 TimeOut 에러가 발생했습니다. 
#define	SLOWVENTINLETCLOSE_WRITEERROR								-86266		//Slow Vent Inlet Valve Close 시 IO Write Command 에러가 발생했습니다. 
#define	SLOWVENTINLETOPEN_TIMEOUTERROR								-86267		//Slow Vent Inlet Open 시 TimeOut 에러가 발생했습니다.
#define SLOWVENTINLETOPEN_LLCTMPGATECLOSEERROR						-86268		//Slow Vent Inlet Open 시 LLC Tmp Gate Close 되어 있지 않아 에러가 발생했습니다.
#define SLOWVENTINLETOPEN_LLCFORELINEOPENERROR						-86269		//Slow Vent Inlet Open 시 LLC Foreline Open 되어 있지 않아 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN_LLCLIDCLOSEERROR							-86270		//LLC Slow Roughing Open 시 LLC LID Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN_SLOWMFCOFFERROR							-86271		//LLC Slow Roughing Open 시 LLC SLOW MFC OFF 되어 있지 않아 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN_FASTMFCOFFERROR							-86272		//LLC Slow Roughing Open 시 LLC FAST MFC OFF 되어 있지 않아 에러가 발생했습니다.
#define	LLCSLOWROUGHOPEN__TRGATEOPEN_LASERCLOSEERROR				-86273		//LLC Slow Roughing Open 시 Laser Source Gate Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	LLCSLOWROUGHOPEN__TRGATEOPEN_MCTMPGATECLOSEERROR			-86274		//LLC Slow Roughing Open 시 MC Tmp Gate Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	LLCSLOWROUGHOPEN__TRGATEOPEN_MCFORELINECLOSEERRO			-86275		//LLC Slow Roughing Open 시 MC Tmp Foreline Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	LLCSLOWROUGHOPEN__TRGATEOPEN_TRLIDCLOSEERROR				-86276		//LLC Slow Roughing Open 시 TR LID Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	LLCSLOWROUGHOPEN__TRGATEOPEN_MCLIDCLOSEERROR				-86277		//LLC Slow Roughing Open 시 MC LID Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	LLCSLOWROUGHOPEN__TRGATEOPEN_MCDRYPUMPERROR					-86278		//LLC Slow Roughing Open 시 MC DRY Pump 정상 구동 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	LLCFASTROUGHOPEN_LLCLIDCLOSEERROR							-86279		//LLC Fast Roughing Open 시 LLC LID Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCFASTROUGHOPEN_SLOWMFCOFFERROR							-86280		//LLC Fast Roughing Open 시 LLC SLOW MFC OFF 되어 있지 않아 에러가 발생했습니다.
#define	LLCFASTROUGHOPEN_FASTMFCOFFERROR							-86281		//LLC Fast Roughing Open 시 LLC FAST MFC OFF 되어 있지 않아 에러가 발생했습니다.
#define	LLCFASTROUGHOPEN__TRGATEOPEN_LASERCLOSEERROR				-86282		//LLC Fast Roughing Open 시 Laser Source Gate Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	LLCFASTROUGHOPEN__TRGATEOPEN_MCTMPGATECLOSEERROR			-86283		//LLC Fast Roughing Open 시 MC Tmp Gate Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	LLCFASTROUGHOPEN__TRGATEOPEN_MCFORELINECLOSEERRO			-86284		//LLC Fast Roughing Open 시 MC Tmp Foreline Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	LLCFASTROUGHOPEN__TRGATEOPEN_TRLIDCLOSEERROR				-86285		//LLC Fast Roughing Open 시 TR LID Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	LLCFASTROUGHOPEN__TRGATEOPEN_MCLIDCLOSEERROR				-86286		//LLC Fast Roughing Open 시 MC LID Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	LLCFASTROUGHOPEN__TRGATEOPEN_MCDRYPUMPERROR					-86287		//LLC Fast Roughing Open 시 MC DRY Pump 정상 구동 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	LLCTMPGATEOPEN_SLOWMFCOFFERROR								-86288		//LLC Tmp Gate Open 시 Slow Mfc Close 되어 있지 않아 에러가 발생했습니다
#define	LLCTMPGATEOPEN_FASTMFCOFFERROR								-86289		//LLC Tmp Gate Open 시 Fast Mfc Close 되어 있지 않아 에러가 발생했습니다
#define	LLCTMPGATEOPEN_LLCLIDCLOSEERROR								-86290		//LLC Tmp Gate Open 시 LLC Lid Close 되어 있지 않아 에러가 발생했습니다
#define	LLCTMPGATEOPEN__TRGATEOPEN_MCROUGHCLOSEERROR				-86291		//LLC Tmp Gate Open 시 MC Rough Close 되어 있지 않아 에러가 발생했습니다 (TR Gate OPen 시)
#define	LLCTMPGATEOPEN__TRGATEOPEN_TRLIDCLOSEERROR					-86292		//LLC Tmp Gate Open 시 TR Lid Close 되어 있지 않아 에러가 발생했습니다 (TR Gate OPen 시)
#define	LLCTMPGATEOPEN__TRGATEOPEN_MCLIDCLOSEERROR					-86293		//LLC Tmp Gate Open 시 MC Lid Close 되어 있지 않아 에러가 발생했습니다 (TR Gate OPen 시)
#define	LLCTMPGATEOPEN__TRGATEOPEN_LASERCLOSEERROR					-86294		//LLC Tmp Gate Open 시 Laser Valve Close 되어 있지 않아 에러가 발생했습니다 (TR Gate OPen 시)
#define	LLCFORELINEOPEN__TRGATEOPEN_MCROUGHCLOSEERROR				-86295		//LLC Foreline Valve Open 시 MC Rough Close 되어 있지 않아 에러가 발생했습니다.(TR Gate OPen 시)
#define	MCSLOWROUGHOPEN_MCLIDCLOSEERROR								-86296		//MC Slow Rough Open 시 MC Lid Close 되어 있지 않아 에러가 발생했습니다.
#define	MCSLOWROUGHOPEN_TRLIDCLOSEERROR								-86297		//MC Slow Rough Open 시 TR Lid Close 되어 있지 않아 에러가 발생했습니다.
#define	MCSLOWROUGHOPEN__TROPEN_SLOWVENTCLOSEERROR					-86298		//MC Slow Rough Open 시 Slow Vent Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCSLOWROUGHOPEN__TROPEN_FASTVENTCLOSEERROR					-86299		//MC Slow Rough Open 시 Fast Vent Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCSLOWROUGHOPEN__TROPEN_LLCLIDCLOSEERROR					-86300		//MC Slow Rough Open 시 LLC Lid Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCSLOWROUGHOPEN__TROPEN_SLOWMFCLOSEERROR					-86301		//MC Slow Rough Open 시 Slow Mfc Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCSLOWROUGHOPEN__TROPEN_FASTMFCLOSEERROR					-86302		//MC Slow Rough Open 시 Fast Mfc Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCSLOWROUGHOPEN__TROPEN_LLCGATECLOSEERROR					-86303		//MC Slow Rough Open 시 LLC Gate Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCSLOWROUGHOPEN__TROPEN_LLCTMPFORELINECLOSEERROR			-86304		//MC Slow Rough Open 시 LLC TMP Foreline Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCSLOWROUGHOPEN__TROPEN_LLCTMPGATECLOSEERROR				-86305		//MC Slow Rough Open 시 LLC TMP Gate Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCSLOWROUGHOPEN__TROPEN_LLCDRYERROR							-86306		//MC Slow Rough Open 시 LLC Dry Pump 가 정상 구동 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCFASTROUGHOPEN_MCLIDCLOSEERROR								-86307		//MC Fast Rough Open 시 MC Lid Close 되어 있지 않아 에러가 발생했습니다.
#define	MCFASTROUGHOPEN_TRLIDCLOSEERROR								-86308		//MC Fast Rough Open 시 TR Lid Close 되어 있지 않아 에러가 발생했습니다.
#define	MCFASTROUGHOPEN__TROPEN_SLOWVENTCLOSEERROR					-86309		//MC Fast Rough Open 시 Slow Vent Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCFASTROUGHOPEN__TROPEN_FASTVENTCLOSEERROR					-86310		//MC Fast Rough Open 시 Fast Vent Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCFASTROUGHOPEN__TROPEN_LLCLIDCLOSEERROR					-86311		//MC Fast Rough Open 시 LLC Lid Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCFASTROUGHOPEN__TROPEN_SLOWMFCLOSEERROR					-86312		//MC Fast Rough Open 시 Slow Mfc Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCFASTROUGHOPEN__TROPEN_FASTMFCLOSEERROR					-86313		//MC Fast Rough Open 시 Fast Mfc Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCFASTROUGHOPEN__TROPEN_LLCGATECLOSEERROR					-86314		//MC Fast Rough Open 시 LLC Gate Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCFASTROUGHOPEN__TROPEN_LLCTMPFORELINECLOSEERROR			-86315		//MC Fast Rough Open 시 LLC TMP Foreline Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCFASTROUGHOPEN__TROPEN_LLCTMPGATECLOSEERROR				-86316		//MC Fast Rough Open 시 LLC TMP Gate Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCFASTROUGHOPEN__TROPEN_LLCDRYERROR							-86317		//MC Fast Rough Open 시 LLC Dry Pump 가 정상 구동 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCTMPGATEOPEN_MCLIDCLOSEERROR								-86318		//MC TMP Gate Open 시 MC LID Valve Close 되어 있지 않아 에러가 발생했습니다.
#define	MCTMPGATEOPEN_TRLIDCLOSEERROR								-86319		//MC TMP Gate Open 시 TR Lid Valve Close 되어 있지 않아 에러가 발생했습니다.
#define	MCTMPGATEOPEN__TROPEN_SLOWVENTCLOSEERROR					-86320		//MC TMP Gate Open 시 Slow Vent Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCTMPGATEOPEN__TROPEN_FASTVENTCLOSEERROR					-86321		//MC TMP Gate Open 시 Fast Vent Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCTMPGATEOPEN__TROPEN_LLCLIDCLOSEERROR						-86322		//MC TMP Gate Open 시 LLC Lid Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCTMPGATEOPEN__TROPEN_SLOWMFCLOSEERROR						-86323		//MC TMP Gate Open 시 Slow Mfc Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCTMPGATEOPEN__TROPEN_FASTMFCLOSEERROR						-86324		//MC TMP Gate Open 시 Fast Mfc Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCTMPGATEOPEN__TROPEN_LLCGATECLOSEERROR						-86325		//MC TMP Gate Open 시 LLC Gate Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCTMPGATEOPEN__TROPEN_LLCROUGHCLOSEERROR					-86326		//MC TMP Gate Open 시 LLC Rough Valve Close 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCTMPGATEOPEN__TROPEN_LLCDRYERROR							-86327		//MC TMP Gate Open 시 LLC Dry Pump 가 정상 구동 되어 있지 않아 에러가 발생했습니다. (TR Gate OPen 시)
#define	MCFORELINEOPEN_LASERCLOSEERROR								-86328		//MC Foreline Valve Open 시 Laser Source Close 되어 있지 않아 에러가 발생했습니다.
#define	MCFORELINEOPEN_MCTMPGATECLOSEERROR							-86329		//MC Foreline Valve Open 시 MC Tmp Gate Close 되어 있지 않아 에러가 발생했습니다.
#define	MCFORELINEOPEN__TROPEN_LLCROUGHCLOSEERROR					-86330		//MC Foreline Valve Open 시 LLC Rough Close 되어 있지 않아 에러가 발생했습니다.(TR Gate OPen 시)
#define	MCFORELINEOPEN__TROPEN_LLCGATECLOSEERROR					-86331		//MC Foreline Valve Open 시 LLC Gate Close 되어 있지 않아 에러가 발생했습니다.(TR Gate OPen 시)
#define	LLCGATEOPEN_LLCLIDCLOSEERROR								-86332		//LLC Gate Open 시 LLC LID Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCGATEOPEN_SLOWMFCCLOSEERROR								-86333		//LLC Gate Open 시 SLOW MFC Close 되어 있지 않아 에러가 발생했습니다.
#define	LLCGATEOPEN_FASTMFCCLOSEERROR								-86334		//LLC Gate Open 시 FAST MFC Close 되어 있지 않아 에러가 발생했습니다.
#define SLOWVENTINLETOPEN__TROPEN_MCTMPGATECLOSEERRO				-86335		//Slow Vent Inlet Open 시 MC Tmp Gate Close 되어 있지 않아 에러가 발생했습니다..(TR Gate OPen 시)
#define SLOWVENTINLETOPEN__TROPEN_MCROUGHCLOSEERROR					-86336		//Slow Vent Inlet Open 시 MC ROUGH Gate Close 되어 있지 않아 에러가 발생했습니다..(TR Gate OPen 시)
#define SLOWVENTINLETOPEN__TROPEN_LASERCLOSEERROR					-86337		//Slow Vent Inlet Open 시 LASER SOURCE Gate Close 되어 있지 않아 에러가 발생했습니다..(TR Gate OPen 시)


#define INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL							-88001		// Sequence 동작 중 명령 알람 발생

#define INTERLOCK_TRGATE_CLOSE_CHECK_FAIL							-88002		// 
#define INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL						-88003		// 
#define INTERLOCK_LLCFORELINE_CLOSE_CHECK_FAIL						-88004		// 
#define INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL						-88005		// 
#define INTERLOCK_LLCSLOWROUGH_CLOSE_CHECK_FAIL						-88006		// 
#define INTERLOCK_MCTMPGATE_CLOSE_CHECK_FAIL						-88007		// 
#define INTERLOCK_MCFORELINE_CLOSE_CHECK_FAIL						-88008		// 
#define INTERLOCK_MCFASTROUGH_CLOSE_CHECK_FAIL						-88009		// 
#define INTERLOCK_MCSLOWROUGH_CLOSE_CHECK_FAIL						-88010		// 
#define INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL							-88011		// 
#define INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL							-88012		// 
#define INTERLOCK_LLCGATE_OPEN_CHECK_FAIL							-88013		// .
#define INTERLOCK_SLOWVENT_OPEN_CHECK_FAIL							-88014		// 
#define INTERLOCK_FASTVENT_OPEN_CHECK_FAIL							-88015		// 
#define INTERLOCK_MCFASTROUGH_OPEN_CHECK_FAIL						-88016		// 
#define INTERLOCK_MCSLOWROUGH_OPEN_CHECK_FAIL						-88017		// 
#define INTERLOCK_LLCFASTROUGH_OPEN_CHECK_FAIL						-88018		// 
#define INTERLOCK_LLCSLOWROUGH_OPEN_CHECK_FAIL						-88019		// 
#define INTERLOCK_LLCFORELINE_OPEN_CHECK_FAIL						-88020		// 
#define INTERLOCK_MCFORELINE_OPEN_CHECK_FAIL						-88021		// 
#define INTERLOCK_MCTMPGATE_OPEN_CHECK_FAIL							-88022		// 
#define INTERLOCK_LLCTMPGATE_OPEN_CHECK_FAIL						-88023		// 
#define INTERLOCK_VACUUMVALUE_CHECK_FAIL							-88024		// 
#define INTERLOCK_TRGATE_OPEN_CHECK_FAIL							-88025		// 
#define INTERLOCK_NOT_PUMPABLE_RANGE_CHECK_FAIL						-88026		// 
#define INTERLOCK_NOT_VENTINGABLE_RANGE_CHECK_FAIL					-88027		// 
#define INTERLOCK_TMPROUGHING_TIMEOUT_FAIL							-88028		// 
#define INTERLOCK_MC_STANDBYVENTING_TIMEOUT_FAIL					-88029		// 
#define INTERLOCK_LLC_STANDBYVENTING_TIMEOUT_FAIL					-88030		// 
#define INTERLOCK_MC_SLOWVENTING_TIMEOUT_FAIL						-88031		// 
#define INTERLOCK_MC_FASTVENTING_TIMEOUT_FAIL						-88032		// 
#define INTERLOCK_LLC_SLOWVENTING_TIMEOUT_FAIL						-88033		// 
#define INTERLOCK_LLC_FASTVENTING_TIMEOUT_FAIL						-88034		// 
#define INTERLOCK_VACUUM_DIFFERENCE_BETWEEN_MC_AND_LLC_FAIL			-88035		// 
#define INTERLOCK_LLC_SLOWROUGH_TIMEOUT_FAIL						-88036		// 
#define INTERLOCK_LLC_FASTROUOGH_TIMEOUT_FAIL						-88037		// 