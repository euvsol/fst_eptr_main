﻿#pragma once

#include "ChartViewer.h"

// CSqOneAutoAlignDlg 대화 상자

class CBeamAutoAlignDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CBeamAutoAlignDlg)

public:
	CBeamAutoAlignDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CBeamAutoAlignDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_BEAMAUTOALIGN_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	CChartViewer m_XYchartViewer;
	CChartViewer m_ContourChartViewer;
	XYChart *ptrXYChart;
	XYChart *ptrContourChart;

	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonAutoAlignRun();

	double dataHorizontal[GatherNum];
	double dataVertical[GatherNum];
	double dataPitch[GatherNum];
	double dataYaw[GatherNum];
	double dataIntensity[GatherNum];
	double dataIteration[GatherNum];

	void drawXYChart(CChartViewer *viewer);
	void drawContourChart(CChartViewer *viewer);
	void OnViewPortChangedXYChart();
	void OnViewPortChangedPITCHTYAWChart();

	bool m_isIntensityOn = false;
	bool m_isAutoAlignOn = false;
	void updataPosition();

	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void initialChart();

	void run2DAutoAlign();
	int m_nAutoAlignStatus;

	void setEditbox(double dPitchStart, double dYawStart, BOOL bIsPitchInverse , BOOL bIsYawInverse);
	bool m_bisFineScan;
	bool m_bisCorseScan;
	bool m_bisCirculScan;

	CWinThread *m_pThread;
	static UINT run2DAutoAlignThread(LPVOID lParam);
};
