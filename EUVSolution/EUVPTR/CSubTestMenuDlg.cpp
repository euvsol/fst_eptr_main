﻿// CSubTestMenuDlg.cpp: 구현 파일
//


#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CSubTestMenuDlg 대화 상자

IMPLEMENT_DYNAMIC(CSubTestMenuDlg, CDialogEx)

CSubTestMenuDlg::CSubTestMenuDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SUB_TEST_MENU_DIALOG, pParent)
{

}

CSubTestMenuDlg::~CSubTestMenuDlg()
{
}

void CSubTestMenuDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSubTestMenuDlg, CDialogEx)
	ON_BN_CLICKED(IDC_SUB_TEST_BUTTON_1, &CSubTestMenuDlg::OnBnClickedSubTestButton1)
	ON_BN_CLICKED(IDC_SUB_TEST_BUTTON_2, &CSubTestMenuDlg::OnBnClickedSubTestButton2)
	ON_BN_CLICKED(IDC_SUB_TEST_BUTTON_3, &CSubTestMenuDlg::OnBnClickedSubTestButton3)
	ON_BN_CLICKED(IDC_SUB_TEST_BUTTON_0, &CSubTestMenuDlg::OnBnClickedSubTestButton0)
END_MESSAGE_MAP()


// CSubTestMenuDlg 메시지 처리기


void CSubTestMenuDlg::OnBnClickedSubTestButton1()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubTestMenuDlg::OnBnClickedFlatnessTestButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_FLATNESS_TEST);
}


void CSubTestMenuDlg::OnBnClickedSubTestButton2()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubTestMenuDlg::OnBnClickedNavigationStageTestButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_NAVIGATION_STAGE_TEST);
}

void CSubTestMenuDlg::OnBnClickedSubTestButton3()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubTestMenuDlg::OnBnClickedSourceTestButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_SOURCE_TEST);
}


int CSubTestMenuDlg::SetDisplay(int Mode)
{
	g_pSubTestMenu->ShowWindow(SW_SHOW);
	g_pSubMenu->ShowWindow(SW_HIDE);
	g_pIO->ShowWindow(SW_HIDE);
	g_pEfem->ShowWindow(SW_HIDE);
	g_pNavigationStage->ShowWindow(SW_HIDE);
	//g_pScanStage->ShowWindow(SW_HIDE);
	g_pMCTmp_IO->ShowWindow(SW_HIDE);
	g_pLLCTmp_IO->ShowWindow(SW_HIDE);
	g_pGauge_IO->ShowWindow(SW_HIDE);
	g_pConfig->ShowWindow(SW_HIDE);
	g_pProcessEditor->ShowWindow(SW_HIDE);
	g_pCommStat->ShowWindow(SW_HIDE);
	g_pTest->ShowWindow(SW_HIDE);
	g_pVacuumRobot->ShowWindow(SW_HIDE);
	g_pMaindialog->ShowWindow(SW_HIDE);
	g_pCamera->ShowWindow(SW_HIDE);
	g_pCamZoneplate->ShowWindow(SW_HIDE);
	g_pEUVSource->ShowWindow(SW_HIDE);
	g_pChart->ShowWindow(SW_HIDE);
	g_pChartline->ShowWindow(SW_HIDE);
	g_pChartstage->ShowWindow(SW_HIDE);
	g_pFilterStage->ShowWindow(SW_HIDE);
	g_pLog->ShowWindow(SW_HIDE);
	g_pRecipe->ShowWindow(SW_HIDE);
	//g_pXrayCamera->ShowWindow(SW_HIDE);
	//g_pPhase->ShowWindow(SW_HIDE);
	//g_pXrayCameraConfig->ShowWindow(SW_HIDE);
	g_pLightCtrl->ShowWindow(SW_HIDE);
	if (g_pPTR != NULL) g_pPTR->ShowWindow(SW_HIDE);
	//g_pScanStageTest->ShowWindow(SW_HIDE);
	g_pNavigationStageTest->ShowWindow(SW_HIDE);
	g_pSourceTest->ShowWindow(SW_HIDE);


	switch (Mode)
	{
	case DISPLAY_BASIC_TEST:
		g_pLog->ShowWindow(SW_SHOW);
		g_pTest->ShowWindow(SW_SHOW);
		break;
	case DISPLAY_FLATNESS_TEST:
		//g_pLog->ShowWindow(SW_SHOW);
		//g_pScanStageTest->ShowWindow(SW_SHOW);
		//g_pChart->ShowWindow(SW_SHOW);
		//g_pChartline->ShowWindow(SW_SHOW);
		break;
	case DISPLAY_NAVIGATION_STAGE_TEST:
		g_pLog->ShowWindow(SW_SHOW);
		g_pNavigationStageTest->ShowWindow(SW_SHOW);
		break;
	case DISPLAY_SOURCE_TEST:
		g_pLog->ShowWindow(SW_SHOW);
		g_pSourceTest->ShowWindow(SW_SHOW);
		break;
	default:
		break;
	}

	return 0;
}





void CSubTestMenuDlg::OnBnClickedSubTestButton0()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubTestMenuDlg::OnBnClickedSubTestButton0() 버튼 클릭!"));

	SetDisplay(DISPLAY_BASIC_TEST);
}
