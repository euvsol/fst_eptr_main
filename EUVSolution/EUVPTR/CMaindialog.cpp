﻿// CMaindialog.cpp: 구현 파일
//


#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include <iomanip>

IMPLEMENT_DYNAMIC(CMaindialog, CDialogEx)

CMaindialog::CMaindialog(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SREM_DIALOG, pParent)
	, m_dMcStandbyVentValue(0)
	, m_dMcSlowVentValue(0)
	, m_dMcFastVentValue(0)
	, m_dMcSlowRoughValue(0)
	, m_dMcFastRoughValue(0)
	, m_dMcTmpRoughValue(0)
	, m_nMcTmpRoughTime(0)
	, m_nMcSlowVentTime(0)
	, m_nMcFastRoughTime(0)
	, m_nMcFastVentTime(0)
	, m_nMcSlowRoughTime(0)
	, m_nMcStandbyVentTime(0)
	, m_dLlcSlowRoughValue(0)
	, m_dLlcFastRoughValue(0)
	, m_dLlcTmpRoughValue(0)
	, m_dLlcStandbyVentValue(0)
	, m_dLlcSlowVentValue(0)
	, m_dLlcFastVentValue(0)
	, m_nLlcSlowRoughTime(0)
	, m_nLlcFastRoughTime(0)
	, m_nLlcTmpRoughTime(0)
	, m_nLlcStandbyVentTime(0)
	, m_nLlcSlowVentTime(0)
	, m_nLlcFastVentTime(0)
{

	m_bLlcSeqStop = FALSE;
	m_bMcSeqStop = FALSE;
	m_bMcPumpingFlag = FALSE;
	m_bLLcPumpingFlag = FALSE;
}

CMaindialog::~CMaindialog()
{
	m_brush.DeleteObject();
	m_font.DeleteObject();

	m_main_dialog_brush.DeleteObject();


}

void CMaindialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

}

BEGIN_MESSAGE_MAP(CMaindialog, CDialogEx)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_MC_PUMP, &CMaindialog::OnBnClickedMcPump)
	ON_BN_CLICKED(IDC_MC_VENT, &CMaindialog::OnBnClickedMcVent)
	ON_BN_CLICKED(IDC_MC_STOP, &CMaindialog::OnBnClickedMcStop)
	ON_BN_CLICKED(IDC_LLC_PUMP, &CMaindialog::OnBnClickedLlcPump)
	ON_BN_CLICKED(IDC_LLC_VENT, &CMaindialog::OnBnClickedLlcVent)
	ON_BN_CLICKED(IDC_LLC_STOP, &CMaindialog::OnBnClickedLlcStop)
	ON_BN_CLICKED(IDC_MC_ON, &CMaindialog::OnBnClickedMcOn)
	ON_BN_CLICKED(IDC_MC_DEFAULT, &CMaindialog::OnBnClickedMcDefault)
	ON_BN_CLICKED(IDC_LLC_ON, &CMaindialog::OnBnClickedLlcOn)
	ON_BN_CLICKED(IDC_LLC_DEFAULT, &CMaindialog::OnBnClickedLlcDefault)
	ON_BN_CLICKED(IDC_MC_AUTO_SEQ_STATE_RESET, &CMaindialog::OnBnClickedMcAutoSeqStateReset)
	ON_BN_CLICKED(IDC_LLC_AUTO_SEQ_STATE_RESET, &CMaindialog::OnBnClickedLlcAutoSeqStateReset)
	ON_BN_CLICKED(IDC_LLC_TMP_ON, &CMaindialog::OnBnClickedLlcTmpOn)
	ON_BN_CLICKED(IDC_LLC_TMP_OFF, &CMaindialog::OnBnClickedLlcTmpOff)
	ON_BN_CLICKED(IDC_MC_TMP_ON, &CMaindialog::OnBnClickedMcTmpOn)
	ON_BN_CLICKED(IDC_MC_TMP_OFF, &CMaindialog::OnBnClickedMcTmpOff)
	ON_WM_CTLCOLOR()
	ON_STN_CLICKED(IDC_ICON_TRGATE, &CMaindialog::OnStnClickedIconTrgate)
	ON_STN_CLICKED(IDC_ICON_LLCGATE, &CMaindialog::OnStnClickedIconLlcgate)
	ON_STN_CLICKED(IDC_ICON_LLC_TMP_GATE, &CMaindialog::OnStnClickedIconLlcTmpGate)
	ON_STN_CLICKED(IDC_ICON_MC_TMP_GATE, &CMaindialog::OnStnClickedIconMcTmpGate)
	ON_STN_CLICKED(IDC_ICON_MC_ROUGH_VALVE, &CMaindialog::OnStnClickedIconMcRoughValve)
	ON_STN_CLICKED(IDC_ICON_MC_FAST_ROUGH, &CMaindialog::OnStnClickedIconMcFastRough)
	ON_STN_CLICKED(IDC_ICON_MC_FORELINE, &CMaindialog::OnStnClickedIconMcForeline)
	ON_STN_CLICKED(IDC_ICON_LLC_FORELINE, &CMaindialog::OnStnClickedIconLlcForeline)
	ON_STN_CLICKED(IDC_ICON_LLC_ROUGH_VALVE, &CMaindialog::OnStnClickedIconLlcRoughValve)
	ON_STN_CLICKED(IDC_ICON_LLC_FAST_ROUGH, &CMaindialog::OnStnClickedIconLlcFastRough)
	ON_STN_CLICKED(IDC_ICON_SLOW_MFC_IN, &CMaindialog::OnStnClickedIconSlowMfcIn)
	ON_STN_CLICKED(IDC_ICON_SLOW_MFC_OUT, &CMaindialog::OnStnClickedIconSlowMfcOut)
	ON_STN_CLICKED(IDC_ICON_FAST_MFC_IN, &CMaindialog::OnStnClickedIconFastMfcIn)
	ON_STN_CLICKED(IDC_ICON_FAST_MFC_OUT, &CMaindialog::OnStnClickedIconFastMfcOut)
END_MESSAGE_MAP()

// CMaindialog 메시지 처리기

BOOL CMaindialog::OnInitDialog()
{
	m_CheckTimer = 1;

	CString ini;
	ini.Empty();

	m_bLlcSeqStop = FALSE;
	m_bMcSeqStop = FALSE;

	ini = _T("0");
	Cnt_Error_Slow_MFC = 0;
	Cnt_Error_Fast_MFC = 0;

	// ICON
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);

	// TR, LLC GATE 
	m_GateBITMAP[0] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_GATE_CLOSE), IMAGE_BITMAP, 15, 120, LR_DEFAULTCOLOR);
	m_GateBITMAP[1] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_GATE_OPEN), IMAGE_BITMAP, 15, 120, LR_DEFAULTCOLOR);

	// TMP GATE VALVE
	m_GateValveBITMAP[0] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_GATE_VALVE_CLOSE), IMAGE_BITMAP, 100, 30, LR_DEFAULTCOLOR);
	m_GateValveBITMAP[1] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_GATE_VALVE_OPEN), IMAGE_BITMAP, 100, 30, LR_DEFAULTCOLOR);
	
	// VALVE
	m_ValveBITMAP[0] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_VALVE_CLOSE), IMAGE_BITMAP, 70, 30, LR_DEFAULTCOLOR);
	m_ValveBITMAP[1] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_VALVE_OPEN), IMAGE_BITMAP, 70, 30, LR_DEFAULTCOLOR);
	
	// TMP
	m_TmpBITMAP[0] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_TMP_OFF), IMAGE_BITMAP, 120, 20, LR_DEFAULTCOLOR);
	m_TmpBITMAP[1] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_TMP_ON), IMAGE_BITMAP, 120, 20, LR_DEFAULTCOLOR);

	// MFC
	m_MFCBITMAP[0] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_MFC_OFF), IMAGE_BITMAP, 50, 20, LR_DEFAULTCOLOR);
	m_MFCBITMAP[1] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_MFC_ON), IMAGE_BITMAP, 50, 20, LR_DEFAULTCOLOR);

	GetDlgItem(IDC_SREM_MAIN_BITMAP)->ModifyStyle(0, WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 0);
	GetDlgItem(IDC_ICON_TRGATE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_LLCGATE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_MC_TMP_GATE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_LLC_TMP_GATE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_MC_ROUGH_VALVE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_MC_FAST_ROUGH)->BringWindowToTop();
	GetDlgItem(IDC_ICON_MC_FORELINE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_LLC_ROUGH_VALVE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_LLC_FAST_ROUGH)->BringWindowToTop();
	GetDlgItem(IDC_ICON_LLC_FORELINE)->BringWindowToTop();
	GetDlgItem(IDC_ICON_LLC_TMP)->BringWindowToTop();
	GetDlgItem(IDC_ICON_MC_TMP)->BringWindowToTop();
	GetDlgItem(IDC_ICON_SLOW_MFC_IN)->BringWindowToTop();
	GetDlgItem(IDC_ICON_SLOW_MFC)->BringWindowToTop();
	GetDlgItem(IDC_ICON_SLOW_MFC_OUT)->BringWindowToTop();
	GetDlgItem(IDC_ICON_FAST_MFC_IN)->BringWindowToTop();
	GetDlgItem(IDC_ICON_FAST_MFC)->BringWindowToTop();
	GetDlgItem(IDC_ICON_FAST_MFC_OUT)->BringWindowToTop();


	m_brush.CreateSolidBrush(RGB(0, 0, 50)); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial")); 
	// Gague 글자
	GetDlgItem(IDC_STATIC_LLC_VACCUM_GAUGE)->SetFont(&m_font);
	GetDlgItem(IDC_STATIC_MC_VACCUM_GAUGE)->SetFont(&m_font);

	m_main_dialog_brush.CreateSolidBrush(RGB(255, 234, 234)); // Dialog 배경 색

	CString sTempValue;

	m_dMcStandbyVentValue = g_pConfig->m_dPressure_ChangeToVent;
	m_dMcSlowVentValue = g_pConfig->m_dPressure_ChangeToSlow_Vent;
	m_dMcFastVentValue = g_pConfig->m_dPressure_Vent_End;
	m_dMcSlowRoughValue = g_pConfig->m_dPressure_ChangeToFast_MC_Rough;
	m_dMcFastRoughValue = g_pConfig->m_dPressure_ChangeToMCTMP_Rough;
	m_dMcTmpRoughValue = g_pConfig->m_dPressure_Rough_End;

	m_dLlcStandbyVentValue = g_pConfig->m_dPressure_ChangeToVent_LLC;
	m_dLlcSlowVentValue = g_pConfig->m_dPressure_ChangeToSlow2_Vent;
	m_dLlcFastVentValue = g_pConfig->m_dPressure_Vent_End;
	m_dLlcSlowRoughValue = g_pConfig->m_dPressure_ChangeToFast_Rough;
	m_dLlcFastRoughValue = g_pConfig->m_dPressure_ChangeToTMP_Rough;
	m_dLlcTmpRoughValue = g_pConfig->m_dPressure_Rough_End;

	m_nMcStandbyVentTime = g_pConfig->m_nTimeout_sec_MCLLCVent;
	m_nMcSlowVentTime = g_pConfig->m_nTimeout_sec_MCSlow1Vent;
	m_nMcFastVentTime = g_pConfig->m_nTimeout_sec_MCFastVent;
	m_nMcSlowRoughTime = g_pConfig->m_nTimeout_sec_MCSlowRough;
	m_nMcFastRoughTime = g_pConfig->m_nTimeout_sec_MCFastRough;
	m_nMcTmpRoughTime = g_pConfig->m_nTimeout_sec_MCTmpEnd;

	m_nLlcStandbyVentTime = g_pConfig->m_nTimeout_sec_LLKStandbyVent;
	m_nLlcSlowVentTime = g_pConfig->m_nTimeout_sec_LLKSlow1Vent;
	m_nLlcFastVentTime = g_pConfig->m_nTimeout_sec_LLKFastVent;
	m_nLlcSlowRoughTime = g_pConfig->m_nTimeout_sec_LLKSlowRough;
	m_nLlcFastRoughTime = g_pConfig->m_nTimeout_sec_LLKFastRough;
	m_nLlcTmpRoughTime = g_pConfig->m_nTimeout_sec_LLKRoughEnd;

	sTempValue.Format("%lf", m_dMcStandbyVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dMcSlowVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dMcFastVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dMcSlowRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dMcFastRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dMcTmpRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);

	sTempValue.Format("%d", m_nMcStandbyVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcSlowVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcFastVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcSlowRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcFastRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcTmpRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);

	sTempValue.Format("%lf", m_dLlcStandbyVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dLlcSlowVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dLlcFastVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dLlcSlowRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dLlcFastRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%lf", m_dLlcTmpRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);

	sTempValue.Format("%d", m_nLlcStandbyVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcSlowVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcFastVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcSlowRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcFastRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcTmpRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(ini);

	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);

	((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_ERROR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);



	((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_ERROR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		SetTimer(m_CheckTimer, 500, NULL);
	}
	else
	{
		initial();
	}
	return TRUE;
}

void CMaindialog::OnDestroy()
{
	CDialogEx::OnDestroy();

	KillTimer(m_CheckTimer);
}

void CMaindialog::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == m_CheckTimer)
	{
		Update();
		SetTimer(m_CheckTimer, 500, NULL);
	}
	else if (nIDEvent == MC_SEQUENCE_CHECK_TIMER)
	{
		MC_Sequence_Time_Check();
		SetTimer(MC_SEQUENCE_CHECK_TIMER, 100, NULL);
	}
	else if (nIDEvent == LLC_SEQUENCE_CHECK_TIMER)
	{
		LLC_Sequence_Time_Check();
		SetTimer(LLC_SEQUENCE_CHECK_TIMER, 100, NULL);
	}
	else if (nIDEvent == XRAYCAMERA_TEMP_CHECK_TIMER_ONLY_MCVENTING)
	{
		//XrayCameraGetTempe();
		SetTimer(XRAYCAMERA_TEMP_CHECK_TIMER_ONLY_MCVENTING, 100, NULL);
	}
	CDialogEx::OnTimer(nIDEvent);
}

void CMaindialog::initial()
{
	SetDlgItemText(IDC_MC_SEQUENCE_STATE, _T("연결 확인 필요"));
	SetDlgItemText(IDC_LLC_SEQUENCE_STATE, _T("연결 확인 필요"));

	SetDlgItemText(IDC_SLOW_MFC_INPUT, _T("연결 확인 필요"));
	SetDlgItemText(IDC_FAST_MFC_INPUT, _T("연결 확인 필요"));
	SetDlgItemText(IDC_SLOW_MFC_OUTPUT, _T("연결 확인 필요"));
	SetDlgItemText(IDC_FAST_MFC_OUTPUT, _T("연결 확인 필요"));

	SetDlgItemText(IDC_MC_LINE_STATE_1, _T("연결 확인 필요"));
	SetDlgItemText(IDC_MC_LINE_STATE_2, _T("연결 확인 필요"));
	SetDlgItemText(IDC_LLC_LINE_STATE_1, _T("연결 확인 필요"));
	SetDlgItemText(IDC_LLC_LINE_STATE_2, _T("연결 확인 필요"));

	SetDlgItemText(IDC_LLC_VACCUM_GAUGE, _T("연결 확인 필요"));
	SetDlgItemText(IDC_MC_VACCUM_GAUGE, _T("연결 확인 필요"));

	SetDlgItemText(IDC_STATIC_LLC_VACCUM_GAUGE, _T("연결 확인 필요"));
	SetDlgItemText(IDC_STATIC_MC_VACCUM_GAUGE, _T("연결 확인 필요"));
	SetDlgItemText(IDC_STATIC_LLC_VACCUM_GAUGE_STATE, _T("연결 확인 필요"));
	SetDlgItemText(IDC_STATIC_MC_VACCUM_GAUGE_STATE, _T("연결 확인 필요"));
	SetDlgItemText(IDC_STATIC_MC_TMP_STATE, _T("연결 확인 필요"));
	SetDlgItemText(IDC_STATIC_LLC_TMP_STATE, _T("연결 확인 필요"));

	((CStatic*)GetDlgItem(IDC_ICON_TRGATE))->SetBitmap(m_GateBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_TMP))->SetBitmap(m_TmpBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_FORELINE))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_LINE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP))->SetBitmap(m_TmpBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_FORELINE))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_LINE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLCGATE))->SetBitmap(m_GateBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_IN))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC))->SetBitmap(m_MFCBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_OUT))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_IN))->SetBitmap(m_ValveBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC))->SetBitmap(m_MFCBITMAP[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_OUT))->SetBitmap(m_ValveBITMAP[0]);

	((CStatic*)GetDlgItem(IDC_ICON_MC_DRYPUMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_DRYPUMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_WATER_SUPPLY))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_WATER_RETURN))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_AIR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MAIN_WATER))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_WATER_LEAK_LLC_TMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_WATER_LEAK_MC_TMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SMOKE_DETACT_CB))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SMOKE_DETACT_VAC_ST))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_ON_MC))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_ON_LLC))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_MC_ERROR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_MC_ERROR2))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC_ERROR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC_ERROR2))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_WATER_RETURN_TEMP_ALARM))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SOURCE_GATE_ON))->SetIcon(m_LedIcon[0]); 
	((CStatic*)GetDlgItem(IDC_ICON_MC_VAC_EXTEND))->SetIcon(m_LedIcon[0]);			// VAC ROBOT HAND EXTEND STATUS OFF (MC)
	((CStatic*)GetDlgItem(IDC_ICON_LLC_VAC_EXTEND))->SetIcon(m_LedIcon[0]);			// VAC ROBOT HAND EXTEND STATUS OFF (LLC)
	((CStatic*)GetDlgItem(IDC_ICON_VAC_RETRACT))->SetIcon(m_LedIcon[0]);			// VAC ROBOT ARM RETRACT STATUS ON
	((CStatic*)GetDlgItem(IDC_ICON_ATM_RETRACT))->SetIcon(m_LedIcon[0]);			// ATM ROBOT ARM RETRACT STATUS ON
	((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MASK_LOADING_POSITION))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_MASK_CHECK_VAC_ROBOT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_MASK_CHECK_VAC_ROBOT))->SetIcon(m_LedIcon[0]);

	
	
	
	
}

void CMaindialog::Update()
{
	CString str;
	str.Empty();

	//Main Stage Loading Position Check
	Main_Stage_Loading_Position_Check();

	//Mask location Check
	Mask_Location_Check();

	//IO state check
	IO_State_Update();

	//Line state check _ mks 722b sensor
	Line_State_Update();

	//Vent State
	Vent_State_Update();

	//LLC TMP 
	LLC_Tmp_State_Update();

	//MC TMP 
	MC_Tmp_State_Update();

	//Gauge
	Gauge_State_Update();

	// Sequence Status Check 
	SequenceLLCICONUpdate(g_pVP->m_LlcVacuumState);
	SequenceMCICONUpdate(g_pVP->m_McVacuumState);

	if (g_pVP->m_nMcErrorCode != 0)
	{
		str.Format(_T("%d"), g_pVP->m_nMcErrorCode);
		((CStatic*)GetDlgItem(IDC_EDIT_MC_AUTO_SEQ_ERROR_CODE))->SetWindowText(str);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_ERROR))->SetIcon(m_LedIcon[2]);
	}
	else
	{
		str.Format(_T("No Error"));
		((CStatic*)GetDlgItem(IDC_EDIT_MC_AUTO_SEQ_ERROR_CODE))->SetWindowText(str);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_ERROR))->SetIcon(m_LedIcon[0]);
	}
	
	if (g_pVP->m_nLlcErrorCode != 0)
	{
		str.Format(_T("%d"), g_pVP->m_nLlcErrorCode);
		((CStatic*)GetDlgItem(IDC_EDIT_LLC_AUTO_SEQ_ERROR_CODE))->SetWindowText(str);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_ERROR))->SetIcon(m_LedIcon[2]);
	}
	else
	{
		str.Format(_T("No Error"));
		((CStatic*)GetDlgItem(IDC_EDIT_LLC_AUTO_SEQ_ERROR_CODE))->SetWindowText(str);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_ERROR))->SetIcon(m_LedIcon[0]);
	}

		
}
void CMaindialog::SequenceLLCICONUpdate(int StateNum)
{
	switch (StateNum)
	{
	case g_pVP->LlcSequenceState::State_IDLE:
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->LlcSequenceState::Pumping_START:
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->LlcSequenceState::Pumping_SLOWROUGH:
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->LlcSequenceState::Pumping_FASTROUGH:
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->LlcSequenceState::Pumping_TMPROUGH:
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[1]);
		break;
	case g_pVP->LlcSequenceState::Pumping_COMPLETE:
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->LlcSequenceState::Venting_START:
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->LlcSequenceState::Venting_SLOWVENT:
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->LlcSequenceState::Venting_FASTVENT:
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->LlcSequenceState::Venting_COMPLETE:
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	default:
		break;
	}
}

void CMaindialog::SequenceMCICONUpdate(int StateNum)
{
	switch (StateNum)
	{
	case g_pVP->McSequenceState::MC_State_IDLE:

		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->McSequenceState::MC_Pumping_START:
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->McSequenceState::MC_Pumping_SLOWROUGH:
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->McSequenceState::MC_Pumping_FASTROUGH:
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->McSequenceState::MC_Pumping_TMPROUGH:
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[1]);
		break;
	case g_pVP->McSequenceState::MC_Pumping_COMPLETE:
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->McSequenceState::MC_Venting_START:
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->McSequenceState::MC_Venting_SLOWVENT:
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->McSequenceState::MC_Venting_FASTVENT:
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	case g_pVP->McSequenceState::MC_Venting_COMPLETE:
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
		break;
	default:
		break;
	}
}

void CMaindialog::IO_State_Update()
{

	// m_LedIcon[0] : Gray
	// m_LedIcon[1] : Green
	// m_LedIcon[2] : Red

	// MC MASK 유무 확인
	if(g_pIO->Is_Mask_Check_Only_OnChuck() == MASK_ON)
		((CStatic*)GetDlgItem(IDC_ICON_MASK_ON_MC))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MASK_ON_MC))->SetIcon(m_LedIcon[0]);
	
	// LLC MASK 유무 확인
	if(g_pIO->Is_Mask_Check_Only_OnLLC() == MASK_ON)
		((CStatic*)GetDlgItem(IDC_ICON_MASK_ON_LLC))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MASK_ON_LLC))->SetIcon(m_LedIcon[0]);
	
	// MC MASK TILT #1 감지 유무
	if(g_pIO->Is_MC_Mask_Slant1_Check() != TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC_ERROR))->SetIcon(m_LedIcon[0]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC_ERROR))->SetIcon(m_LedIcon[2]);

	// MC MASK TILT #2 감지 유무
	if (g_pIO->Is_MC_Mask_Slant2_Check() != TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC_ERROR2))->SetIcon(m_LedIcon[0]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC_ERROR2))->SetIcon(m_LedIcon[2]);

	// LLC MASK TILT #1 감지 유무
	if (g_pIO->Is_LLC_Mask_Slant1_Check()!=TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC_ERROR))->SetIcon(m_LedIcon[0]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC_ERROR))->SetIcon(m_LedIcon[2]);
	
	// LLC MASK TILT #2 감지 유무
	if (g_pIO->Is_LLC_Mask_Slant2_Check() != TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC_ERROR2))->SetIcon(m_LedIcon[0]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC_ERROR2))->SetIcon(m_LedIcon[2]);

	// Water Leak Sensor (LLC TMP)
	if(g_pIO->Is_Tmp_LLC_Leak_Status() != RUN  )
		((CStatic*)GetDlgItem(IDC_ICON_WATER_LEAK_LLC_TMP))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_WATER_LEAK_LLC_TMP))->SetIcon(m_LedIcon[0]);
	
	// Water leak Sensor (MC TMP)
	if(g_pIO->ls_Tmp_MC_Leak_Status() != RUN)
		((CStatic*)GetDlgItem(IDC_ICON_WATER_LEAK_MC_TMP))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_WATER_LEAK_MC_TMP))->SetIcon(m_LedIcon[0]);
		
	// Smoke Detect Sensor (AC RACK)
	if(g_pIO->Is_Ac_Rack_detect_Status()!=TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_SMOKE_DETACT_CB))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_SMOKE_DETACT_CB))->SetIcon(m_LedIcon[0]);

	// Smoke Detect Sensor (control Rack)
	if(g_pIO->Is_Control_Rack_detect_Status() != TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_SMOKE_DETACT_VAC_ST))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_SMOKE_DETACT_VAC_ST))->SetIcon(m_LedIcon[0]);
	
	// Water Temp Alarm
	// TRUE : 감지
	// FALSE : 미감지
	if(g_pIO->Is_Water_Temp_Alarm_Status() != TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_WATER_RETURN_TEMP_ALARM))->SetIcon(m_LedIcon[0]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_WATER_RETURN_TEMP_ALARM))->SetIcon(m_LedIcon[2]);

	// Water Supply Valve
	// TRUE : OPEN
	// FASLE : CLOSE
	if(g_pIO->Is_Water_Supply_Valve_Status() != TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_WATER_SUPPLY))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_WATER_SUPPLY))->SetIcon(m_LedIcon[1]);
	
	// Water Return Valve
	// TRUE : 공급
	// FASLE : 미공급
	if(g_pIO->Is_Water_Return_Valve_Open() != TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_WATER_RETURN))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_WATER_RETURN))->SetIcon(m_LedIcon[1]);
	
	// Main Air On/OFF
	// TURE : 공급
	// FALSE : 미공급
	if(g_pIO->Is_Air_Supply_Status() !=TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_AIR))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_AIR))->SetIcon(m_LedIcon[1]);

		
	// Water Supply 
	// TRUE : 공급
	// FASLE : 미공급
	if(g_pIO->Is_Water_Supply_Status() != TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_WATER))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MAIN_WATER))->SetIcon(m_LedIcon[1]);

	// MC TMP GATE 
	// TRUE : OPEN
	// FALSE : CLOSE
	if(g_pIO->Is_MC_TMP_GateValve_Open() != VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_MC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[0]); // MC TMP GATE CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_MC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[1]); // MC TMP GATE OPEN

	// MC FORELINE GATE 
	// TRUE : OPEN
	// FALSE : CLOSE
	if(g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_MC_FORELINE))->SetBitmap(m_ValveBITMAP[0]); // MC FORELINE GATE CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_MC_FORELINE))->SetBitmap(m_ValveBITMAP[1]); // MC FORELINE GATE OPEN

	// MC FAST ROUGH GATE 
	// TRUE : OPEN
	// FALSE : CLOSE
	if(g_pIO->Is_MC_FastRoughValve_Open() != VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_MC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[0]); // MC FAST ROUGH CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_MC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[1]); // MC FAST ROUGH OPEN

	// MC SLOW ROUGH GATE
	// TRUE : OPEN
	// FALSE : CLOSE
	if(g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_MC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[0]); // MC SLOW ROUGH CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_MC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[1]); // MC SLOW ROUGH OPEN

	// TR GATE
	// TRUE : OPEN
	// FALSE : CLOSE
	if(g_pIO->Is_TRGateValve_Open()!=VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_TRGATE))->SetBitmap(m_GateBITMAP[0]); // TR GATE CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_TRGATE))->SetBitmap(m_GateBITMAP[1]);// TR GATE OPEN

	// LLC FORELINE
	// TRUE : OPEN
	// FALSE : CLOSE
	if(g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[0]); // LLC TMP GATE CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP_GATE))->SetBitmap(m_GateValveBITMAP[1]); // LLC TMP GATE OPEN

	// LLC FORELINE
	// TRUE : OPEN
	// FALSE : CLOSE
	if(g_pIO->Is_LLC_TMP_ForelineValve_Open() !=VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_LLC_FORELINE))->SetBitmap(m_ValveBITMAP[0]);  // LLC FORELINE CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_LLC_FORELINE))->SetBitmap(m_ValveBITMAP[1]); // LLC FORELINE OPEN

	// LLC GATE
	// TRUE : OPEN
	// FALSE : CLOSE
	if (g_pIO->Is_LLCGateValve_Open() != VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_LLCGATE))->SetBitmap(m_GateBITMAP[0]); // LLC GATE CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_LLCGATE))->SetBitmap(m_GateBITMAP[1]);// LLC GATE OPEN
	
	// LLC FAST ROUGH
	// TRUE : OPEN
	// FALSE : CLOSE
	if(g_pIO->Is_LLC_FastRoughValve_Open() !=VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_LLC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[0]); // LLC FAST ROUGH CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_LLC_FAST_ROUGH))->SetBitmap(m_ValveBITMAP[1]); // LLC FAST ROUGH OPEN

	// LLC SLOW ROUGH
	// TRUE : OPEN
	// FALSE : CLOSE
	if(g_pIO->Is_LLC_SlowRoughValve_Open() !=VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_LLC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[0]); // LLC SLOW ROUGH CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_LLC_ROUGH_VALVE))->SetBitmap(m_ValveBITMAP[1]);// LLC SLOW ROUGH OPEN

	// MFC FAST OUTLET
	// TRUE : OPEN
	// FALSE : CLOSE
	if(g_pIO->Is_FastVent_Outlet_Valve_Open() !=VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_OUT))->SetBitmap(m_ValveBITMAP[0]);  // MFC FAST OUTLET CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_OUT))->SetBitmap(m_ValveBITMAP[1]); // MFC FAST OUTLET OPEN

	// MFC SLOW OUTLET
	// TRUE : OPEN
	// FALSE : CLOSE
	if(g_pIO->Is_SlowVent_Outlet_Valve_Open() != VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_OUT))->SetBitmap(m_ValveBITMAP[0]); // MFC SLOW OUTLET CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_OUT))->SetBitmap(m_ValveBITMAP[1]); // MFC SLOW OUTLET OPEN

	// MFC SLOW OUTLET
	// TRUE : OPEN
	// FALSE : CLOSE
	if(g_pIO->Is_FastVent_Inlet_Valve_Open() != VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_IN))->SetBitmap(m_ValveBITMAP[0]);  // MFC FAST INLET CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC_IN))->SetBitmap(m_ValveBITMAP[1]);  // MFC FAST INLET OPEN

	// MFC SLOW INLET
	// TRUE : OPEN
	// FALSE : CLOSE
	if(g_pIO->Is_SlowVent_Inlet_Valve_Open() != VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_IN))->SetBitmap(m_ValveBITMAP[0]); // MFC SLOW INLET CLOSE
	else
		((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC_IN))->SetBitmap(m_ValveBITMAP[1]); // MFC SLOW INLET OPEN


	// LLC DRY PUMP ON
	// TRUE : ON
	// FALSE : OFF Or Error
	if(g_pIO->Is_LLC_DryPump_Status() != DRYPUMP_RUN)
		((CStatic*)GetDlgItem(IDC_ICON_LLC_DRYPUMP))->SetIcon(m_LedIcon[2]); // LLC DRY PUMP OFF
	else
		((CStatic*)GetDlgItem(IDC_ICON_LLC_DRYPUMP))->SetIcon(m_LedIcon[1]); // LLC DRY PUMP ON

	// MC DRY PUMP ON
	// TRUE : ON
	// FALSE : OFF Or Error
	if(g_pIO->Is_MC_DryPump_Status() != DRYPUMP_RUN)
		((CStatic*)GetDlgItem(IDC_ICON_MC_DRYPUMP))->SetIcon(m_LedIcon[2]);  // MC DRY PUMP OFF
	else
		((CStatic*)GetDlgItem(IDC_ICON_MC_DRYPUMP))->SetIcon(m_LedIcon[1]); // MC DRY PUMP ON

	// Source Gate On
	// TRUE : ON
	// FALSE : OFF Or Error
	if(g_pIO->Is_SourceGate_OpenOn_Check() != VALVE_OPEN)
		((CStatic*)GetDlgItem(IDC_ICON_SOURCE_GATE_ON))->SetIcon(m_LedIcon[0]);  // SOURCE GATE OFF
	else
		((CStatic*)GetDlgItem(IDC_ICON_SOURCE_GATE_ON))->SetIcon(m_LedIcon[1]);  // SOURCE GATE ON

	// VAC ROBOT HAND RETRACT STATUS
	// TRUE : ON
	// FALSE : OFF
	if (g_pIO->Is_VAC_Robot_MC_Arm_Retract())
		((CStatic*)GetDlgItem(IDC_ICON_MC_VAC_EXTEND))->SetIcon(m_LedIcon[0]);  // VAC ROBOT HAND EXTEND STATUS ON (MC)
	else
		((CStatic*)GetDlgItem(IDC_ICON_MC_VAC_EXTEND))->SetIcon(m_LedIcon[1]);  // VAC ROBOT HAND EXTEND STATUS OFF (MC)

	// VAC ROBOT HAND RETRACT STATUS
	// TRUE : ON
	// FALSE : OFF 
	if (g_pIO->Is_VAC_Robot_LLC_Arm_Retract())
		((CStatic*)GetDlgItem(IDC_ICON_LLC_VAC_EXTEND))->SetIcon(m_LedIcon[0]);  // VAC ROBOT HAND EXTEND STATUS ON (LLC)
	else
		((CStatic*)GetDlgItem(IDC_ICON_LLC_VAC_EXTEND))->SetIcon(m_LedIcon[1]);  // VAC ROBOT HAND EXTEND STATUS OFF (LLC)
	
	if (g_pIO->Is_VAC_Robot_Arm_Retract())
		((CStatic*)GetDlgItem(IDC_ICON_VAC_RETRACT))->SetIcon(m_LedIcon[1]);  // VAC ROBOT ARM RETRACT STATUS ON
	else
		((CStatic*)GetDlgItem(IDC_ICON_VAC_RETRACT))->SetIcon(m_LedIcon[0]);  // VAC ROBOT ARM RETRACT STATUS OFF
	
	if (g_pIO->Is_ATM_Robot_Arm_Retract())
		((CStatic*)GetDlgItem(IDC_ICON_ATM_RETRACT))->SetIcon(m_LedIcon[1]);  // ATM ROBOT ARM RETRACT STATUS ON
	else
		((CStatic*)GetDlgItem(IDC_ICON_ATM_RETRACT))->SetIcon(m_LedIcon[0]);  // ATM ROBOT ARM RETRACT STATUS ON

	if (g_pIO->Is_ATM_Robot_Arm_Retract())
		((CStatic*)GetDlgItem(IDC_ICON_ATM_RETRACT))->SetIcon(m_LedIcon[1]);  // ATM ROBOT ARM RETRACT STATUS ON
	else
		((CStatic*)GetDlgItem(IDC_ICON_ATM_RETRACT))->SetIcon(m_LedIcon[0]);  // ATM ROBOT ARM RETRACT STATUS ON

	if(g_pIO->Is_Mask_OnVMTR())
		((CStatic*)GetDlgItem(IDC_ICON_MC_MASK_CHECK_VAC_ROBOT))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MC_MASK_CHECK_VAC_ROBOT))->SetIcon(m_LedIcon[0]);

	if (g_pIO->Is_Mask_OnVMTR_LLC())
		((CStatic*)GetDlgItem(IDC_ICON_LLC_MASK_CHECK_VAC_ROBOT))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_LLC_MASK_CHECK_VAC_ROBOT))->SetIcon(m_LedIcon[0]);


	// LLC TMP 
	//	NoError = 0,
	//  NoConnection,
	//  PumpOverTemp,
	//  ControllOverTemp = 4,
	//  PowerFail = 8,
	//  AuxFail = 16,
	//  OverVlotage = 32,
	//  HIghLoad = 128,
	if (g_pLLCTmp_IO->GetLlcTmpErrorCode() != g_pLLCTmp_IO->NoError)
		((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP))->SetBitmap(m_TmpBITMAP[0]); // LLC TMP OFF
	else
		((CStatic*)GetDlgItem(IDC_ICON_LLC_TMP))->SetBitmap(m_TmpBITMAP[1]); // LLC TMP ON

	// MC TMP 
	//Stop = 0,
	//TMP_TURN_OFF,
	//TMP_TURN_ON,
	//Deceleration,
	//Acceleration,
	//Running,
	//Fail,
	//Error,
	if (g_pMCTmp_IO->GetMcTmpState() != g_pMCTmp_IO->Running)
		((CStatic*)GetDlgItem(IDC_ICON_MC_TMP))->SetBitmap(m_TmpBITMAP[0]);// MC TMP OFF
	else
		((CStatic*)GetDlgItem(IDC_ICON_MC_TMP))->SetBitmap(m_TmpBITMAP[1]); // MC TMP ON
	
}


void CMaindialog::Line_State_Update()
{
	CString nCh_1_str;
	CString nCh_2_str;

	double nCh_1 = (double)(g_pIO->m_nDecimal_Analog_in_ch_1) / ANALOG_INPUT_UNIT_CONVERSION; // LLC FORELINE GAUGE 722b monitor
	double nCh_2 = (double)(g_pIO->m_nDecimal_Analog_in_ch_2) / ANALOG_INPUT_UNIT_CONVERSION; // MC FORELINE GAUGE 722b monitor

	nCh_1_str.Format("%0.2f", nCh_1);
	nCh_2_str.Format("%0.2f", nCh_2);

	double nCh_1_torr = nCh_1 / 10;
	double nCh_2_torr = nCh_2 / 10;

	if (nCh_1_torr <= 0.3)
	{
		nCh_1_torr = 0.005;
	}

	if (nCh_2_torr <= 0.3)
	{
		nCh_2_torr = 0.005;
	}

	//double nCh_1_torr = 0.0000456;
	//double nCh_2_torr = 7600;

	CString nch_1_torr_string;
	CString nch_2_torr_string;

	nch_1_torr_string.Format(_T("%.2E Torr"), nCh_1_torr);
	nch_2_torr_string.Format(_T("%.2E Torr"), nCh_2_torr);


	// MC, LLC LINE MKS 722B GAUGE
	SetDlgItemText(IDC_LLC_LINE_STATE_1, nch_1_torr_string);
	SetDlgItemText(IDC_MC_LINE_STATE_1, nch_2_torr_string);


	// LLC LINE STATE (MKS 722B)
	if (nCh_1 > 9.0)
	{
		SetDlgItemText(IDC_LLC_LINE_STATE_2, _T("Line 대기상태"));
		((CStatic*)GetDlgItem(IDC_ICON_LLC_LINE))->SetIcon(m_LedIcon[2]);
	}
	else if (nCh_1 < 9.0 && nCh_1 > 2.0)
	{
		SetDlgItemText(IDC_LLC_LINE_STATE_2, _T("Line Pumping 중"));
		((CStatic*)GetDlgItem(IDC_ICON_LLC_LINE))->SetIcon(m_LedIcon[2]);
	}
	else if (nCh_1 < 2.0)
	{
		SetDlgItemText(IDC_LLC_LINE_STATE_2, _T("Line 진공상태"));
		((CStatic*)GetDlgItem(IDC_ICON_LLC_LINE))->SetIcon(m_LedIcon[1]);
	}

	// MC LINE STATE (MKS 722B)
	if (nCh_2 > 9.0)
	{
		SetDlgItemText(IDC_MC_LINE_STATE_2, _T("Line 대기상태"));
		((CStatic*)GetDlgItem(IDC_ICON_MC_LINE))->SetIcon(m_LedIcon[2]);
	}
	else if (nCh_2 < 9.0 && nCh_2 > 2.0)
	{
		SetDlgItemText(IDC_MC_LINE_STATE_2, _T("Line Pumping 중"));
		((CStatic*)GetDlgItem(IDC_ICON_MC_LINE))->SetIcon(m_LedIcon[2]);
	}
	else if (nCh_2 < 2.0)
	{
		SetDlgItemText(IDC_MC_LINE_STATE_2, _T("Line 진공상태"));
		((CStatic*)GetDlgItem(IDC_ICON_MC_LINE))->SetIcon(m_LedIcon[1]);
	}

}

void CMaindialog::Vent_State_Update()
{
	CString str;
	CString nCh_5_str;
	CString nCh_6_str;
	CString nCh_5_str_sccm;
	CString nCh_6_str_sccm;
	CString nCh_5_out_str_sccm;
	CString nCh_6_out_str_sccm;

	unsigned long long mfc_ret = 0x00;

	double nCh_5_diff;
	double nCh_6_diff;

	//double nCh_5 = (double)(g_pIO->m_nDecimal_Analog_in_ch_5) / 1638.0; // #mfc#2 N2 10 sccm
	//double nCh_6 = (double)(g_pIO->m_nDecimal_Analog_in_ch_6) / 1638.0; // #mfc#1 N2 20000 sccm

	//double nCh_5 = (double)(g_pIO->m_nDecimal_Analog_in_ch_5) / 409.5; // #mfc#2 N2 10 sccm
	//double nCh_6 = (double)(g_pIO->m_nDecimal_Analog_in_ch_6) / 409.5; // #mfc#1 N2 20000 sccm
	//double nCh_5_out = (double)(g_pIO->m_nDecimal_Analog_out_ch_1) / 409.5; // #mfc#2 N2 10 sccm
	//double nCh_6_out = (double)(g_pIO->m_nDecimal_Analog_out_ch_2) / 409.5; // #mfc#1 N2 20000 sccm

	double nCh_5 = g_pIO->Get_MFC2_N2_10sccm_In();			// #mfc#2 Input N2 10 sccm
	double nCh_6 = g_pIO->Get_MFC1_N2_20000sccm_In();		// #mfc#1 Input N2 20000 sccm
	double nCh_5_out = g_pIO->Get_MFC2_N2_10sccm_Out();		// #mfc#2 Output N2 10 sccm
	double nCh_6_out = g_pIO->Get_MFC1_N2_20000sccm_Out();	// #mfc#1 Output N2 20000 sccm
	
	double nCh_5_MFC2 = nCh_5 * 2;
	double nCh_6_MFC1 = nCh_6 * 4000;

	double nCh_5_MFC2_OUT = nCh_5_out * 2;
	double nCh_6_MFC1_OUT = nCh_6_out * 4000;



	// (MFC 입력값 - MFC 출력값)
	nCh_5_diff = (nCh_5_MFC2_OUT - nCh_5_MFC2);
	nCh_6_diff = (nCh_6_MFC1_OUT - nCh_6_MFC1);

	//nCh_5_diff = (nCh_5_MFC2 - nCh_5_MFC2_OUT);
	//nCh_6_diff = (nCh_6_MFC1 - nCh_6_MFC1_OUT);

	nCh_5_str.Format("%0.2f", nCh_5);
	nCh_6_str.Format("%0.2f", nCh_6);

	nCh_5_str_sccm.Format("%0.2f", nCh_5_MFC2);
	nCh_6_str_sccm.Format("%0.1f", nCh_6_MFC1);

	nCh_5_out_str_sccm.Format("%0.2f", nCh_5_MFC2_OUT);
	nCh_6_out_str_sccm.Format("%0.1f", nCh_6_MFC1_OUT);


	// MFC STATE 
	SetDlgItemText(IDC_SLOW_MFC_INPUT, nCh_5_str_sccm);
	SetDlgItemText(IDC_FAST_MFC_INPUT, nCh_6_str_sccm);
	SetDlgItemText(IDC_SLOW_MFC_OUTPUT, nCh_5_out_str_sccm);
	SetDlgItemText(IDC_FAST_MFC_OUTPUT, nCh_6_out_str_sccm);

	//SaveLogFile("SLOW_MFC_Log", _T((LPSTR)(LPCTSTR)(" Input : " + nCh_5_str_sccm + " Output : " + nCh_5_out_str_sccm))); // input mfc
	//SaveLogFile("FAST_MFC_Log", _T((LPSTR)(LPCTSTR)(" Input : " + nCh_6_str_sccm + " Output : " + nCh_6_out_str_sccm))); // input mfc

	// Sequence 시 MFC 유량 Log 
	if ((g_pVP->m_bVacuumThreadStop == FALSE) || (g_pVP->m_b_MC_VacuumThreadStop == FALSE)) // MC 또는 LLC sequence 가 실행 중이라면..
	{
		if ((g_pVP->m_McVacuumState == g_pVP->MC_Venting_SLOWVENT)
			|| (g_pVP->m_McVacuumState == g_pVP->MC_Venting_FASTVENT)
			|| (g_pVP->m_LlcVacuumState == g_pVP->Venting_SLOWVENT) 
			|| (g_pVP->m_LlcVacuumState == g_pVP->Venting_FASTVENT))
		{
			SaveLogFile("SLOW_MFC_Log", _T((LPSTR)(LPCTSTR)(" Input : " + nCh_5_str_sccm + " Output : " + nCh_5_out_str_sccm))); // input mfc
			SaveLogFile("FAST_MFC_Log", _T((LPSTR)(LPCTSTR)(" Input : " + nCh_6_str_sccm + " Output : " + nCh_6_out_str_sccm))); // input mfc
		}
	}

	// SLOW VENT
	if (g_pIO->Is_SlowVentValve_Open() == VALVE_OPENED)
	{
		SetDlgItemText(IDC_SLOW_MFC_1, nCh_5_str + " V");
		SetDlgItemText(IDC_SLOW_MFC_2, nCh_5_str_sccm + " Sccm");
		if (nCh_5 > 0.1)
			((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC))->SetBitmap(m_MFCBITMAP[1]);
		else if (nCh_5 < 0.1)
			((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC))->SetBitmap(m_MFCBITMAP[0]);
	}
	else
	{
	
		SetDlgItemText(IDC_SLOW_MFC_1, _T(" 0 V"));
		SetDlgItemText(IDC_SLOW_MFC_2, _T("0 Sccm"));
		((CStatic*)GetDlgItem(IDC_ICON_SLOW_MFC))->SetBitmap(m_MFCBITMAP[0]);
		Cnt_Error_Slow_MFC = 0;
	}
	
	// FAST VENT
	if (g_pIO->Is_FastVentValve_Open() == VALVE_OPENED)
	{
		SetDlgItemText(IDC_FAST_MFC_1, nCh_6_str + " V");
		SetDlgItemText(IDC_FAST_MFC_2, nCh_6_str_sccm + " Sccm");
		if (nCh_6 > 0.1)
			((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC))->SetBitmap(m_MFCBITMAP[1]);
		else if (nCh_6 < 0.1)
			((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC))->SetBitmap(m_MFCBITMAP[0]);
	}
	else
	{
		SetDlgItemText(IDC_FAST_MFC_1, _T(" 0 V"));
		SetDlgItemText(IDC_FAST_MFC_2, _T(" 0 Sccm"));
		((CStatic*)GetDlgItem(IDC_ICON_FAST_MFC))->SetBitmap(m_MFCBITMAP[0]);
		Cnt_Error_Fast_MFC = 0;
	}
	

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 질소 유량 적을 시 알람 띄우고 멈춤 함수
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	///////////////////////
	// SLOW MFC 유량 확인
	///////////////////////

	if ((SLOW_MFC_LIMIT < nCh_5_diff))
	{
		Cnt_Error_Slow_MFC++;
		if (CHECK_MFC_NUM < Cnt_Error_Slow_MFC)
		{
			if (g_pIO->Is_SlowVentValve_Open() == VALVE_OPENED)
			{
				SetDlgItemText(IDC_SLOW_MFC_STATE, _T("ERROR : 질소 확인 필요"));
				str = " Slow Vent Error :: 질소 유량 부족에 따른 Error 발생";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				str = " Slow Vent Error :: 질소 유량 부족에 따른 Slow Vent Valve Close";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				/////////////////////////////////////
				// 별도의 ERROR SEQUENCE 추가 해야함.
				/////////////////////////////////////
				if (g_pIO->Close_SlowVentValve() != TRUE)
				{
					str = " Slow Vent Error :: 질소 확인 ERROR 에 따른 SLOW VENT VALVE CLOSE 실패";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);
				}
				g_pLog->Display(0, g_pIO->Log_str);
	
				//20210811 jkseo, 실제로 뭔가 동작중에 쓰레드를 강제로 종료하면 ??? 로딩/언로딩 시에 질소 부족이면??? 에러는 ?
				if (g_pVP->m_bVacuumThreadStop == FALSE) // LLC sequence 가 실행 중이라면..
				{
					str = " Slow Vent Error :: 질소 유량 부족에 따른 LLC Sequence 정지";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

					g_pVP->m_pVaccumThread->SuspendThread();
					DWORD dwResult;
					::GetExitCodeThread(g_pVP->m_pVaccumThread, &dwResult);
	
					delete g_pVP->m_pVaccumThread;
					g_pVP->m_pVaccumThread = NULL;
	

					str = " [LLC Venting] Slow Vent Error :: 질소 유량 부족에 따른 LLC Vacuum Sequence 정지";
					SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);
	
					//m_VentingState = FALSE;

					//g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
					//g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
				}
				else if (g_pVP->m_b_MC_VacuumThreadStop == FALSE) // MC sequence 가 실행 중이라면..
				{
					str = " Slow Vent Error :: 질소 유량 부족에 따른 MC Sequence 정지";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

					g_pVP->m_p_MC_VaccumThread->SuspendThread();
					DWORD dwResult;
					::GetExitCodeThread(g_pVP->m_p_MC_VaccumThread, &dwResult);
	
					delete g_pVP->m_p_MC_VaccumThread;
					g_pVP->m_p_MC_VaccumThread = NULL;

					str = " [MC Venting] Slow Vent Error :: 질소 유량 부족에 따른 MC Vacuum Sequence 정지";
					SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);
					g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
				}

				str = " Slow Vent Error :: 질소 유량 부족에 따른 Slow MFC Module 정지";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
				
				g_pIO->WriteOutputData(0, &mfc_ret, 2);  //Slow Mfc 유량 0 으로 reset
				::AfxMessageBox("질소 부족 Slow Vent Error", MB_ICONERROR);
				Cnt_Error_Slow_MFC = 0;
			}
			else
			{
				SetDlgItemText(IDC_SLOW_MFC_STATE, _T("ERROR : Valve Line 확인 필요"));
				str = " Slow Vent Error :: Valve Line 확인 필요 ";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
				Cnt_Error_Slow_MFC = 0;
				//m_VentingState = FALSE;
			}
		}
	}
	else
	{
		SetDlgItemText(IDC_SLOW_MFC_STATE, _T("정상 작동 중"));
		Cnt_Error_Slow_MFC = 0;
		//m_VentingState = TRUE;
	}


	/////////////////////////
	//// FAST MFC 유량 확인
	/////////////////////////

	if ((FAST_MFC_LIMIT < nCh_6_diff))
	{
		Cnt_Error_Fast_MFC++;
		if (CHECK_MFC_NUM < Cnt_Error_Fast_MFC)
		{
			if (g_pIO->Is_FastVentValve_Open() == VALVE_OPENED)
			{
				SetDlgItemText(IDC_FAST_MFC_STATE, _T("ERROR : 질소 확인 필요"));
				str = " Fast Vent Error :: 질소 유량 부족에 따른 Error 발생";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				str = " Fast Vent Error :: 질소 유량 부족에 따른 Fast Vent Valve Close";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				/////////////////////////////////////////////////
				// Fast Vent Valve 가 Open 되어 있다면 Close.
				/////////////////////////////////////////////////
				if (g_pIO->Close_FastVentValve() != OPERATION_COMPLETED)
				{
					str = " Fast Vent Error :: 질소 확인 ERROR 에 따른 FAST VENT VALVE CLOSE 실패";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
				}
				g_pLog->Display(0, g_pIO->Log_str);

				/////////////////////////////////////////////////
				// LLC sequence 가 실행 중이라면..Sequence 종료
				/////////////////////////////////////////////////
				if (g_pVP->m_bVacuumThreadStop == FALSE) 
				{

					str = " Fast Vent Error :: 질소 유량 부족에 따른 LLC Sequence 정지";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

					g_pVP->m_pVaccumThread->SuspendThread();
					DWORD dwResult;
					::GetExitCodeThread(g_pVP->m_pVaccumThread, &dwResult);
	
					delete g_pVP->m_pVaccumThread;
					g_pVP->m_pVaccumThread = NULL;
					//g_pVP->m_Vacuum_State = g_pVP->Pumping_ERROR;
	
					str = " [LLC Venting] Fast Vent Error :: 질소 유량 부족에 따른 LLC Vacuum Sequence 정지";
					SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);
					//m_VentingState = FALSE;
					g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
				}
				/////////////////////////////////////////////////
				// MC sequence 가 실행 중이라면...Sequence 종료
				/////////////////////////////////////////////////
				else if (g_pVP->m_b_MC_VacuumThreadStop == FALSE) 
				{

					str = " Fast Vent Error :: 질소 유량 부족에 따른 MC Sequence 정지";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

					g_pVP->m_p_MC_VaccumThread->SuspendThread();
					DWORD dwResult;
					::GetExitCodeThread(g_pVP->m_p_MC_VaccumThread, &dwResult);
	
					delete g_pVP->m_p_MC_VaccumThread;
					g_pVP->m_p_MC_VaccumThread = NULL;

					str = " [MC Venting] Fast Vent Error :: 질소 유량 부족에 따른 MC Vacuum Sequence 정지";
					SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);

					g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
				}


				str = " Fast Vent Error :: 질소 유량 부족에 따른 Fast MFC Module 정지";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				g_pIO->WriteOutputData(2, &mfc_ret, 2); //Fast Mfc 유량 0 으로 reset
				::AfxMessageBox("질소 부족 Fast Vent Error",MB_ICONERROR);
				Cnt_Error_Fast_MFC = 0;
			}
			else
			{
				SetDlgItemText(IDC_FAST_MFC_STATE, _T("ERROR : Valve Line 확인 필요"));
				str = " Fast Vent Error :: Valve Line 확인 필요 ";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
				Cnt_Error_Fast_MFC = 0;
			}
		}
	}
	else
	{
		SetDlgItemText(IDC_FAST_MFC_STATE, _T("정상 작동 중"));
		Cnt_Error_Fast_MFC = 0;
	}
	

	if (nCh_5 < 0.5)
	{
		SetDlgItemText(IDC_SLOW_MFC_STATE, _T("동작 대기 중"));
	}


	if (nCh_6 < 0.5)
	{
		SetDlgItemText(IDC_FAST_MFC_STATE, _T("동작 대기 중"));
	}

}

void CMaindialog::Gauge_State_Update()
{
	// MKS 390 Gauge 
	CString sLlcVacuumRate, sMcVacuumRate;
	CString sTemp;

	sLlcVacuumRate.Format(_T("%.2e Torr"), g_pGauge_IO->GetLlcVacuumRate());
	sMcVacuumRate.Format(_T("%.2e Torr"), g_pGauge_IO->GetMcVacuumRate());

	SetDlgItemText(IDC_LLC_VACCUM_GAUGE, sLlcVacuumRate);
	SetDlgItemText(IDC_MC_VACCUM_GAUGE, sMcVacuumRate);
	SetDlgItemText(IDC_STATIC_LLC_VACCUM_GAUGE, sLlcVacuumRate);
	SetDlgItemText(IDC_STATIC_MC_VACCUM_GAUGE, sMcVacuumRate);

	if (g_pGauge_IO->GetGaugeState() == g_pGauge_IO->NotConnected)
	{
		sTemp = _T("Conn Fail");
	}
	else if (g_pGauge_IO->GetGaugeState() == g_pGauge_IO->IDLE)
	{
		sTemp = _T("Idle");
	}
	else if (g_pGauge_IO->GetGaugeState() == g_pGauge_IO->Running)
	{
		sTemp = _T("Running");
	}
	else if (g_pGauge_IO->GetGaugeState() == g_pGauge_IO->Error)
	{
		sTemp = _T("Error");
	}

	SetDlgItemText(IDC_STATIC_LLC_VACCUM_GAUGE_STATE, sTemp);
	SetDlgItemText(IDC_STATIC_MC_VACCUM_GAUGE_STATE, sTemp);
}

void CMaindialog::LLC_Tmp_State_Update()
{
	//LLC TMP 
	CString str;

	str.Format(_T("%d hz"), g_pLLCTmp_IO->GetLlcTmphz());
	SetDlgItemText(IDC_STATIC_LLC_TMP_HZ, str);

	str.Format(_T("%05d"), g_pLLCTmp_IO->GetLlcTmpErrorCode());
	SetDlgItemText(IDC_STATIC_LLC_TMP_ERROR, str);
	int a = g_pLLCTmp_IO->GetLlcTmpState();
	switch (g_pLLCTmp_IO->GetLlcTmpState())
	{
	case g_pLLCTmp_IO->Stop:
		str.Format(_T("Stop"));
		break;
	case g_pLLCTmp_IO->Working_Intlk:
		str.Format(_T("Working Intlk"));
		break;
	case g_pLLCTmp_IO->Starting:
		str.Format(_T("Starting"));
		break;
	case g_pLLCTmp_IO->Auto_Tuning:
		str.Format(_T("Auto Tuning"));
		break;
	case g_pLLCTmp_IO->Breaking:
		str.Format(_T("Breaking"));
		break;
	case g_pLLCTmp_IO->Running:
		str.Format(_T("Running"));
		break;
	case g_pLLCTmp_IO->Fail:
		str.Format(_T("Fail"));
		break;
	default:
		break;
	}
	SetDlgItemText(IDC_STATIC_LLC_TMP_STATE,str);
}

void CMaindialog::MC_Tmp_State_Update()
{
	//MC TMP
	CString str;

	str.Format(_T("%d hz"), g_pMCTmp_IO->GetMcTmpHz());
	SetDlgItemText(IDC_STATIC_MC_TMP_HZ, str);

	str.Format(_T("%05d"), g_pMCTmp_IO->GetMcTmpErrorCode());
	SetDlgItemText(IDC_STATIC_MC_TMP_ERROR, str);
	int a = g_pMCTmp_IO->GetMcTmpState();
	switch (g_pMCTmp_IO->GetMcTmpState())
	{
	case g_pMCTmp_IO->Stop:
		str.Format(_T("Stop"));
		break;
	case g_pMCTmp_IO->TMP_TURN_OFF:
		str.Format(_T("Tmp Turn Off"));
		break;
	case g_pMCTmp_IO->TMP_TURN_ON:
		str.Format(_T("Tmp Turn On"));
		break;
	case g_pMCTmp_IO->Deceleration:
		str.Format(_T("Deceleration"));
		break;
	case g_pMCTmp_IO->Acceleration:
		str.Format(_T("Acceleration"));
		break;
	case g_pMCTmp_IO->Running:
		str.Format(_T("Running"));
		break;
	case g_pMCTmp_IO->Fail:
		str.Format(_T("Fail"));
		break;
	case g_pMCTmp_IO->Error:
		str.Format(_T("Errir"));
		break;
	default:
		str.Format(_T("Unknown"));
		break;
	}

	SetDlgItemText(IDC_STATIC_MC_TMP_STATE, str);
}

void CMaindialog::Mask_Location_Check()
{
	int state = g_pConfig->m_nMaterialLocation;

	switch (state)
	{
	case MTS_POD:
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);
		break;
	case MTS_ROBOT_HAND:
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);

		break;
	case MTS_ROTATOR:
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);
		break;
	case LLC :
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);
		break;
	case VACUUM_ROBOT_HAND :
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[1]);
		break;
	case CHUCK :
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);
		break;
	default:
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_POD))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROTATOR))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LLC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MC))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_MTS_ROBOT_HAND))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MASK_VACUUM_ROBOT))->SetIcon(m_LedIcon[0]);
		break;
	}
	
}

void CMaindialog::Main_Stage_Loading_Position_Check()
{
	//if (g_pIO->Is_Loading_Position_Status()!=FALSE)
	if (g_pNavigationStage->Is_Loading_Positioin() == TRUE)
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LOADING_POSITION))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_MASK_LOADING_POSITION))->SetIcon(m_LedIcon[0]);
	}
}

void CMaindialog::OnBnClickedMcPump()
{
	CString str = "MC Pumping 을 실행하시겠습니까?";
	g_pLog->Display(0, str);
	if (AfxMessageBox(str, MB_YESNO) == IDYES)
	{


		m_bMcSeqStop = FALSE;
		m_bMcPumpingFlag = TRUE;
		if (m_bLLcPumpingFlag)
			g_pWarning->m_strWarningMessageVal = " MC & LLC Pumping 중 ! ";
		else
			g_pWarning->m_strWarningMessageVal = " MC Pumping 중 ! ";
		
		g_pWarning->UpdateData(FALSE);
		g_pWarning->ShowWindow(SW_SHOW);
		

		GetDlgItem(IDC_MC_PUMP)->EnableWindow(false);
		GetDlgItem(IDC_MC_VENT)->EnableWindow(false);

		MC_Sequence_Time_Ini();
		SetTimer(MC_SEQUENCE_CHECK_TIMER, 100, NULL);

		int ret = g_pVP->MC_Pumping_Start();
		if (ret != 0)
			AfxMessageBox(_T("MC Pumping 동작이 실패했습니다!!"));
		else
		{
			while (TRUE)
			{
				WaitSec(1);
				if (g_pVP->m_p_MC_VaccumThread == NULL && g_pVP->m_McVacuumState == g_pVP->MC_Pumping_COMPLETE)
				{
					AfxMessageBox(_T("MC Pumping이 완료되었습니다."));
					break;
				}

				if (g_pVP->m_nMcErrorCode != 0)
				{
					CString sTemp;
					sTemp.Format(_T("Fail to mc pumping. (error code : %d)"), g_pVP->m_nMcErrorCode);
					AfxMessageBox(sTemp);
					break;
				}

				if (m_bMcSeqStop == TRUE)
				{
					AfxMessageBox(_T("MC Pumping 동작이 정지되었습니다."));
					break;
				}
			}
		}
		m_bMcPumpingFlag = FALSE;
		g_pWarning->ShowWindow(SW_HIDE);
		KillTimer(MC_SEQUENCE_CHECK_TIMER);
		GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
	}	GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
}

void CMaindialog::OnBnClickedMcVent()
{
	CString str;
	str.Format(_T("MC Venting을 실행하시겠습니까?"));
	g_pLog->Display(0, str);
	if (AfxMessageBox(str, MB_YESNO) == IDYES)
	{

		if (g_pEUVSource->Is_EUV_On() != FALSE)
		{
			if (g_pEUVSource->SetEUVSourceOn(FALSE) != 0)
			{
				AfxMessageBox("Euv Off 동작이 실패했습니다.");
				return;
			}

			if (g_pEUVSource->SetMechShutterOpen(FALSE) != 0)
			{
				AfxMessageBox("메카니컬 셔터 Close 동작이 실패했습니다.");
				return;
			}
		}



		m_bMcSeqStop = FALSE;
		g_pWarning->m_strWarningMessageVal = " MC Venting 중 ! ";
		g_pWarning->UpdateData(FALSE);
		g_pWarning->ShowWindow(SW_SHOW);

		GetDlgItem(IDC_MC_PUMP)->EnableWindow(false);
		GetDlgItem(IDC_MC_VENT)->EnableWindow(false);
		GetDlgItem(IDC_LLC_PUMP)->EnableWindow(false);
		GetDlgItem(IDC_LLC_VENT)->EnableWindow(false);

		MC_Sequence_Time_Ini();
		LLC_Sequence_Time_Ini();
		SetTimer(MC_SEQUENCE_CHECK_TIMER, 100, NULL);

		int ret = g_pVP->MC_Venting_Start();
		if (ret != 0)
			AfxMessageBox(_T("MC Venting 동작이 실패했습니다 !!"));
		else
		{
			while (TRUE)
			{
				WaitSec(1);
				if (g_pVP->m_p_MC_VaccumThread == NULL && g_pVP->m_McVacuumState == g_pVP->MC_Venting_COMPLETE)
				{
					g_pWarning->ShowWindow(SW_HIDE);
					AfxMessageBox(_T("MC Venting이 완료되었습니다."));
					break;
				}

				if (g_pVP->m_nMcErrorCode != 0)
				{
					g_pWarning->ShowWindow(SW_HIDE);
					CString sTemp;
					sTemp.Format(_T("MC Venting 동작이 실패했습니다. (error code : %d)"), g_pVP->m_nMcErrorCode);
					AfxMessageBox(sTemp);
					break;
				}

				if (m_bMcSeqStop == TRUE)
				{
					AfxMessageBox(_T("MC Venting 동작이 정지되었습니다."));
					break;
				}
			}
		}
		g_pWarning->ShowWindow(SW_HIDE);
		KillTimer(MC_SEQUENCE_CHECK_TIMER);
		GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
		GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
		GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
		GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
	}
}

void CMaindialog::OnBnClickedMcStop()
{
	if (g_pVP->m_p_MC_VaccumThread == NULL)
	{
		::AfxMessageBox("Sequence 가 이미 종료 되었거나, 실행중인 Sequence 가 없습니다", MB_ICONEXCLAMATION);
	}
	else
	{
		g_pVP->m_p_MC_VaccumThread->SuspendThread();
		DWORD dwResult;
		::GetExitCodeThread(g_pVP->m_p_MC_VaccumThread, &dwResult);

		delete g_pVP->m_p_MC_VaccumThread;
		g_pVP->m_p_MC_VaccumThread = NULL;

		//MC Sequence 상태에 따라 Stop 동작 추가 해야함.
		//

		m_bMcSeqStop = TRUE;
	}
}

void CMaindialog::OnBnClickedLlcPump()
{
	CString str = "LLC Pumping 을 실행하시겠습니까?";
	g_pLog->Display(0, str);
	if (AfxMessageBox(str, MB_YESNO) == IDYES)
	{
		m_bLlcSeqStop = FALSE;
		m_bLLcPumpingFlag = TRUE;
		if (m_bMcPumpingFlag)
			g_pWarning->m_strWarningMessageVal = " MC & LLC Pumping 중 ! ";
		else
			g_pWarning->m_strWarningMessageVal = " LLC Pumping 중 ! ";

		g_pWarning->UpdateData(FALSE);
		g_pWarning->ShowWindow(SW_SHOW);

		GetDlgItem(IDC_LLC_PUMP)->EnableWindow(false);
		GetDlgItem(IDC_LLC_VENT)->EnableWindow(false);

		LLC_Sequence_Time_Ini();
		SetTimer(LLC_SEQUENCE_CHECK_TIMER, 100, NULL);

		int ret = g_pVP->LLC_Pumping_Start();
		if (ret != 0)
			AfxMessageBox(_T("LLC Pumping 동작이 실패했습니다."));
		else
		{
			while (TRUE)
			{
				WaitSec(1);
				if (g_pVP->m_pVaccumThread == NULL && g_pVP->m_LlcVacuumState == g_pVP->Pumping_COMPLETE)
				{
					AfxMessageBox(_T("LLC Pumping 동작이 완료되었습니다."));
					break;
				}

				if (g_pVP->m_nLlcErrorCode != 0)
				{
					CString sTemp;
					sTemp.Format(_T("LLC Pumping 동작이 실패했습니다. (error code : %d)"), g_pVP->m_nLlcErrorCode);
					AfxMessageBox(sTemp);
					break;
				}

				if (m_bLlcSeqStop == TRUE)
				{
					AfxMessageBox(_T("LLC Pumping 동작이 정지되었습니다."));
					break;
				}
			}
		}

		m_bLLcPumpingFlag = FALSE;
		KillTimer(LLC_SEQUENCE_CHECK_TIMER);
		GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
		GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
		g_pWarning->ShowWindow(SW_HIDE);
	}
}

void CMaindialog::OnBnClickedLlcVent()
{
	CString str;
	str.Format(_T("LLC Venting 을 실행하시겠습니까?"));
	g_pLog->Display(0, str);

	if (AfxMessageBox(str, MB_YESNO) == IDYES)
	{

		if (g_pEUVSource->Is_EUV_On() == TRUE)
		{
			if (g_pEUVSource->SetEUVSourceOn(FALSE) != 0)
			{
				AfxMessageBox("Euv Off 동작이 실패했습니다.");
				return;
			}

			if (g_pEUVSource->SetMechShutterOpen(FALSE) != 0)
			{
				AfxMessageBox("메카니컬 셔터 Close 동작이 실패했습니다.");
				return;
			}
		}

		g_pWarning->m_strWarningMessageVal = " LLC Venting 중 ! ";
		g_pWarning->UpdateData(FALSE);
		g_pWarning->ShowWindow(SW_SHOW);

		m_bLlcSeqStop = FALSE;
		GetDlgItem(IDC_LLC_PUMP)->EnableWindow(false);
		GetDlgItem(IDC_LLC_VENT)->EnableWindow(false);
		
		LLC_Sequence_Time_Ini();
		SetTimer(LLC_SEQUENCE_CHECK_TIMER, 100, NULL);

		int ret = g_pVP->LLC_Venting_Start();
		if (ret != 0)
			AfxMessageBox(_T("LLC Venting 동작이 실패했습니다."));
		else
		{
			while (TRUE)
			{
				WaitSec(1);
				if (g_pVP->m_pVaccumThread == NULL && g_pVP->m_LlcVacuumState == g_pVP->Venting_COMPLETE)
				{
					AfxMessageBox(_T("LLC Venting이 완료되었습니다."));
					break;
				}

				if (g_pVP->m_nLlcErrorCode != 0)
				{
					CString sTemp;
					sTemp.Format(_T("LLC Venting이 실패했습니다. (error code : %d)"), g_pVP->m_nLlcErrorCode);
					AfxMessageBox(sTemp);
					break;
				}

				if (m_bLlcSeqStop == TRUE)
				{
					AfxMessageBox(_T("LLC Venting이 정지되었습니다."));
					break;
				}
			}
		}

		KillTimer(LLC_SEQUENCE_CHECK_TIMER);
		GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
		GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
		g_pWarning->ShowWindow(SW_HIDE);
	}
}

void CMaindialog::OnBnClickedLlcStop()
{
	if (g_pVP->m_pVaccumThread == NULL)
	{
		::AfxMessageBox("Sequence 가 이미 종료 되었거나, 실행중인 Sequence 가 없습니다", MB_ICONEXCLAMATION);
	}
	else
	{
		g_pVP->m_pVaccumThread->SuspendThread();
		DWORD dwResult;
		::GetExitCodeThread(g_pVP->m_pVaccumThread, &dwResult);

		delete g_pVP->m_pVaccumThread;
		g_pVP->m_pVaccumThread = NULL;

		if ((g_pVP->m_LlcVacuumState == g_pVP->Venting_START) 
			|| (g_pVP->m_LlcVacuumState == g_pVP->Venting_SLOWVENT) 
			|| (g_pVP->m_LlcVacuumState == g_pVP->Venting_FASTVENT))
		{
			g_pVP->LLCVentingSequenceStop();
		}
		else if ((g_pVP->m_LlcVacuumState == g_pVP->Pumping_START) 
			|| (g_pVP->m_LlcVacuumState == g_pVP->Pumping_SLOWROUGH) 
			|| (g_pVP->m_LlcVacuumState == g_pVP->Pumping_FASTROUGH) 
			|| (g_pVP->m_LlcVacuumState == g_pVP->Pumping_TMPROUGH))
		{
			g_pVP->LLCPumpingSequenceStop();
		}

		m_bLlcSeqStop = TRUE;
	}
}

void CMaindialog::OnBnClickedMcOn()
{
	CString sTempValue;

	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dMcStandbyVentValue);
	m_dMcStandbyVentValue = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dMcSlowVentValue);
	m_dMcSlowVentValue = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dMcFastVentValue);
	m_dMcFastVentValue = atof(sTempValue);


	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dMcSlowRoughValue);
	m_dMcSlowRoughValue = atof(sTempValue);


	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dMcFastRoughValue);
	m_dMcFastRoughValue = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dMcTmpRoughValue);
	m_dMcTmpRoughValue = atof(sTempValue);


	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_TIME))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%d", &m_nMcStandbyVentTime);
	m_nMcStandbyVentTime = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_TIME))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%d", &m_nMcSlowVentTime);
	m_nMcSlowVentTime = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_TIME))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%d", &m_nMcFastVentTime);
	m_nMcFastVentTime = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_TIME))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%d", &m_nMcSlowRoughTime);
	m_nMcSlowRoughTime = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_TIME))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%d", &m_nMcFastRoughTime);
	m_nMcFastRoughTime = atof(sTempValue);

	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_TIME))->GetWindowTextA(sTempValue);
	//sscanf(sTempValue, "%d", &m_nMcTmpRoughTime);
	m_nMcTmpRoughTime = atof(sTempValue);


	g_pConfig->m_dPressure_ChangeToVent = m_dMcStandbyVentValue;
	g_pConfig->m_dPressure_ChangeToSlow_Vent = m_dMcSlowVentValue;
	g_pConfig->m_dPressure_Vent_End = m_dMcFastVentValue;
	g_pConfig->m_dPressure_ChangeToFast_MC_Rough = m_dMcSlowRoughValue;
	g_pConfig->m_dPressure_ChangeToMCTMP_Rough = m_dMcFastRoughValue;
	g_pConfig->m_dPressure_Rough_End = m_dMcTmpRoughValue;
	
	g_pConfig->m_nTimeout_sec_MCLLCVent = m_nMcStandbyVentTime;
	g_pConfig->m_nTimeout_sec_MCSlow1Vent = m_nMcSlowVentTime;
	g_pConfig->m_nTimeout_sec_MCFastVent = m_nMcFastVentTime;
	g_pConfig->m_nTimeout_sec_MCSlowRough = m_nMcSlowRoughTime;
	g_pConfig->m_nTimeout_sec_MCFastRough = m_nMcFastRoughTime;
	g_pConfig->m_nTimeout_sec_MCTmpEnd = m_nMcTmpRoughTime;
	

	g_pConfig->SaveVacuumSeqData();

	
	sTempValue.Format("%.6f", m_dMcStandbyVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcSlowVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcFastVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcSlowRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcFastRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcTmpRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);

	sTempValue.Format("%d", m_nMcStandbyVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcSlowVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcFastVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcSlowRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcFastRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcTmpRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
}

void CMaindialog::OnBnClickedMcDefault()
{
	CString sTempValue;

	m_dMcStandbyVentValue = g_pConfig->m_dPressure_ChangeToVent			 = g_pConfig->m_dPressure_ChangeToVent_ini;
	m_dMcSlowVentValue	  = g_pConfig->m_dPressure_ChangeToSlow_Vent	 = g_pConfig->m_dPressure_ChangeToSlow_Vent_ini;
	m_dMcFastVentValue    = g_pConfig->m_dPressure_Vent_End				 = g_pConfig->m_dPressure_Vent_End_ini;
	m_dMcSlowRoughValue   = g_pConfig->m_dPressure_ChangeToFast_MC_Rough = g_pConfig->m_dPressure_ChangeToFast_MC_Rough_ini;
	m_dMcFastRoughValue   = g_pConfig->m_dPressure_ChangeToMCTMP_Rough   = g_pConfig->m_dPressure_ChangeToMCTMP_Rough_ini;
	m_dMcTmpRoughValue    = g_pConfig->m_dPressure_Rough_End			 = g_pConfig->m_dPressure_Rough_End_ini;
	 
	m_nMcStandbyVentTime = g_pConfig->m_nTimeout_sec_MCLLCVent	 = g_pConfig->m_nTimeout_sec_MCLLCVent_ini;
	m_nMcSlowVentTime    = g_pConfig->m_nTimeout_sec_MCSlow1Vent = g_pConfig->m_nTimeout_sec_MCSlow1Vent_ini;
	m_nMcFastVentTime    = g_pConfig->m_nTimeout_sec_MCFastVent  = g_pConfig->m_nTimeout_sec_MCFastVent_ini;
	m_nMcSlowRoughTime   = g_pConfig->m_nTimeout_sec_MCSlowRough = g_pConfig->m_nTimeout_sec_MCSlowRough_ini;
	m_nMcFastRoughTime   = g_pConfig->m_nTimeout_sec_MCFastRough = g_pConfig->m_nTimeout_sec_MCFastRough_ini;
	m_nMcTmpRoughTime    = g_pConfig->m_nTimeout_sec_MCTmpEnd    = g_pConfig->m_nTimeout_sec_MCTmpEnd_ini;
	

	sTempValue.Format("%.6f", m_dMcStandbyVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcSlowVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcFastVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcSlowRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcFastRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dMcTmpRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW))->SetWindowTextA(sTempValue);

	sTempValue.Format("%d", m_nMcStandbyVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcSlowVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcFastVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcSlowRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcFastRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nMcTmpRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW_TIME))->SetWindowTextA(sTempValue);

	g_pConfig->SaveVacuumSeqData();
}

void CMaindialog::OnBnClickedLlcOn()
{
	CString sTempValue;

	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC))->GetWindowTextA(sTempValue);
	m_dLlcStandbyVentValue = atof(sTempValue);
	//sscanf_s(sTempValue, "%3.6f", &m_dLlcStandbyVentValue);

	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC))->GetWindowTextA(sTempValue);
	m_dLlcSlowVentValue = atof(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dLlcSlowVentValue);
	
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC))->GetWindowTextA(sTempValue);
	m_dLlcFastVentValue = atof(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dLlcFastVentValue);
	
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC))->GetWindowTextA(sTempValue);
	m_dLlcSlowRoughValue = atof(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dLlcSlowRoughValue);
	
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC))->GetWindowTextA(sTempValue);
	m_dLlcFastRoughValue = atof(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dLlcFastRoughValue);
	
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC))->GetWindowTextA(sTempValue);
	m_dLlcTmpRoughValue = atof(sTempValue);
	//sscanf(sTempValue, "%.6f", &m_dLlcTmpRoughValue);

	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_TIME))->GetWindowTextA(sTempValue);
	m_nLlcStandbyVentTime = atof(sTempValue);
	//sscanf(sTempValue, "%d", &m_nLlcStandbyVentTime);
	
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_TIME))->GetWindowTextA(sTempValue);
	m_nLlcSlowVentTime = atof(sTempValue);
	//sscanf(sTempValue, "%d", &m_nLlcSlowVentTime);
	
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_TIME))->GetWindowTextA(sTempValue);
	m_nLlcFastVentTime = atof(sTempValue);
	//sscanf(sTempValue, "%d", &m_nLlcFastVentTime);
	
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_TIME))->GetWindowTextA(sTempValue);
	m_nLlcSlowRoughTime = atof(sTempValue);
	//sscanf(sTempValue, "%d", &m_nLlcSlowRoughTime);
	
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_TIME))->GetWindowTextA(sTempValue);
	m_nLlcFastRoughTime = atof(sTempValue);
	//sscanf(sTempValue, "%d", &m_nLlcFastRoughTime);
	
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_TIME))->GetWindowTextA(sTempValue);
	m_nLlcTmpRoughTime = atof(sTempValue);
	//sscanf(sTempValue, "%d", &m_nLlcTmpRoughTime);

	g_pConfig->m_dPressure_ChangeToVent_LLC = m_dLlcStandbyVentValue;
	g_pConfig->m_dPressure_ChangeToSlow2_Vent = m_dLlcSlowVentValue;
	g_pConfig->m_dPressure_Vent_End = m_dLlcFastVentValue;
	g_pConfig->m_dPressure_ChangeToFast_Rough = m_dLlcSlowRoughValue;
	g_pConfig->m_dPressure_ChangeToTMP_Rough = m_dLlcFastRoughValue;
	g_pConfig->m_dPressure_Rough_End = m_dLlcTmpRoughValue;
	
	g_pConfig->m_nTimeout_sec_LLKStandbyVent = m_nLlcStandbyVentTime;
	g_pConfig->m_nTimeout_sec_LLKSlow1Vent = m_nLlcSlowVentTime;
	g_pConfig->m_nTimeout_sec_LLKFastVent = m_nLlcFastVentTime;
	g_pConfig->m_nTimeout_sec_LLKSlowRough = m_nLlcSlowRoughTime;
	g_pConfig->m_nTimeout_sec_LLKFastRough = m_nLlcFastRoughTime;
	g_pConfig->m_nTimeout_sec_LLKRoughEnd = m_nLlcTmpRoughTime;
	
	g_pConfig->SaveVacuumSeqData();

	sTempValue.Format("%.6f", m_dLlcStandbyVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcSlowVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcFastVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcSlowRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcFastRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcTmpRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);

	sTempValue.Format("%d", m_nLlcStandbyVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcSlowVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcFastVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcSlowRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcFastRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcTmpRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
}

void CMaindialog::OnBnClickedLlcDefault()
{
	CString sTempValue;

	m_dLlcStandbyVentValue = g_pConfig->m_dPressure_ChangeToVent_LLC   = g_pConfig->m_dPressure_ChangeToVent_LLC_ini;
	m_dLlcSlowVentValue    = g_pConfig->m_dPressure_ChangeToSlow2_Vent = g_pConfig->m_dPressure_ChangeToSlow2_Vent_ini;
	m_dLlcFastVentValue    = g_pConfig->m_dPressure_Vent_End		   = g_pConfig->m_dPressure_Vent_End_ini;
	m_dLlcSlowRoughValue   = g_pConfig->m_dPressure_ChangeToFast_Rough = g_pConfig->m_dPressure_ChangeToFast_Rough_ini;
	m_dLlcFastRoughValue   = g_pConfig->m_dPressure_ChangeToTMP_Rough  = g_pConfig->m_dPressure_ChangeToTMP_Rough_ini;
	m_dLlcTmpRoughValue    = g_pConfig->m_dPressure_Rough_End		   = g_pConfig->m_dPressure_Rough_End_ini;
	
	m_nLlcStandbyVentTime = g_pConfig->m_nTimeout_sec_LLKStandbyVent = g_pConfig->m_nTimeout_sec_LLKStandbyVent_ini;
	m_nLlcSlowVentTime    = g_pConfig->m_nTimeout_sec_LLKSlow1Vent   = g_pConfig->m_nTimeout_sec_LLKSlow1Vent_ini;
	m_nLlcFastVentTime    = g_pConfig->m_nTimeout_sec_LLKFastVent    = g_pConfig->m_nTimeout_sec_LLKFastVent_ini;
	m_nLlcSlowRoughTime   = g_pConfig->m_nTimeout_sec_LLKSlowRough   = g_pConfig->m_nTimeout_sec_LLKSlowRough_ini;
	m_nLlcFastRoughTime   = g_pConfig->m_nTimeout_sec_LLKFastRough   = g_pConfig->m_nTimeout_sec_LLKFastRough_ini;
	m_nLlcTmpRoughTime    = g_pConfig->m_nTimeout_sec_LLKRoughEnd    = g_pConfig->m_nTimeout_sec_LLKRoughEnd_ini;
	

	sTempValue.Format("%.6f", m_dLlcStandbyVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcSlowVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcFastVentValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcSlowRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcFastRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	sTempValue.Format("%.6f", m_dLlcTmpRoughValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW))->SetWindowTextA(sTempValue);
	
	sTempValue.Format("%d", m_nLlcStandbyVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcSlowVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcFastVentTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcSlowRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcFastRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);
	sTempValue.Format("%d", m_nLlcTmpRoughTime);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_TIME))->SetWindowTextA(sTempValue);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME))->SetWindowTextA(sTempValue);

	g_pConfig->SaveVacuumSeqData();
}

void CMaindialog::OnBnClickedMcAutoSeqStateReset()
{
	((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_ERROR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
	g_pVP->m_McVacuumState = g_pVP->McSequenceState::MC_State_IDLE;
	g_pVP->m_nMcErrorCode = 0;
}

void CMaindialog::OnBnClickedLlcAutoSeqStateReset()
{
	((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_ERROR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_VENT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_VENT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_SLOW_PUMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_FAST_PUMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLC_SEQ_TMP_ROUGH))->SetIcon(m_LedIcon[0]);
	g_pVP->m_LlcVacuumState = g_pVP->LlcSequenceState::State_IDLE;
	g_pVP->m_nLlcErrorCode = 0;
}

void CMaindialog::OnBnClickedLlcTmpOn()
{
	CString str;

	if (g_pLLCTmp_IO->LLC_Tmp_On() != RUN)
	{
		::AfxMessageBox("LLC TMP ON ERROR");
		str.Format(_T("LLC TMP ON 실패 하였습니다."));
		g_pLog->Display(0, str);
	}
	else
	{
		str.Format(_T("LLC TMP ON 되었습니다."));
		g_pLog->Display(0, str);
	}
}

void CMaindialog::OnBnClickedLlcTmpOff()
{
	CString str;

	if (g_pLLCTmp_IO->LLC_Tmp_Off() != RUN_OFF)
	{
		::AfxMessageBox("LLC TMP OFF ERROR");
		str.Format(_T("LLC TMP OFF 실패 하였습니다."));
		g_pLog->Display(0, str);
	}
	else
	{
		str.Format(_T("LLC TMP OFF 되었습니다."));
		g_pLog->Display(0, str);
	}
}

void CMaindialog::OnBnClickedMcTmpOn()
{
	CString str;

	if (g_pMCTmp_IO->Mc_Tmp_On() != RUN)
	{
		::AfxMessageBox("MC TMP ON ERROR");
		str.Format(_T("MC TMP ON 실패 하였습니다."));
		g_pLog->Display(0, str);
	}
	else
	{
		str.Format(_T( "MC TMP ON 되었습니다."));
		g_pLog->Display(0, str);
	}
}

void CMaindialog::OnBnClickedMcTmpOff()
{
	CString str;

	if (g_pMCTmp_IO->Mc_Tmp_Off() != RUN_OFF)
	{
		::AfxMessageBox("MC TMP OFF ERROR");
		str.Format(_T("MC TMP OFF 실패 하였습니다."));
		g_pLog->Display(0, str);
	}
	else
	{
		str.Format(_T("MC TMP OFF 되었습니다."));
		g_pLog->Display(0, str);
	}
}

HBRUSH CMaindialog::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = __super::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (pWnd->GetDlgCtrlID() == IDC_STATIC_LLC_VACCUM_GAUGE)
		{
			pDC->SetBkColor(RGB(0, 0, 50));  
			pDC->SetTextColor(RGB(0, 255, 0)); 
			return m_brush;
		}
		else if (pWnd->GetDlgCtrlID() == IDC_STATIC_MC_VACCUM_GAUGE)
		{
			pDC->SetBkColor(RGB(0, 0, 50));  
			pDC->SetTextColor(RGB(0, 255, 0));  
			return m_brush;
		}
		else if ((pWnd->GetDlgCtrlID() == IDC_MC_VACCUM_GAUGE) 
			|| (pWnd->GetDlgCtrlID() == IDC_LLC_VACCUM_GAUGE)
			|| (pWnd->GetDlgCtrlID() == IDC_MC_LINE_STATE_1) 
			|| (pWnd->GetDlgCtrlID() == IDC_MC_LINE_STATE_2) 
			|| (pWnd->GetDlgCtrlID() == IDC_LLC_LINE_STATE_1) 
			|| (pWnd->GetDlgCtrlID() == IDC_LLC_LINE_STATE_2) 
			|| (pWnd->GetDlgCtrlID() == IDC_SLOW_MFC_1) 
			|| (pWnd->GetDlgCtrlID() == IDC_SLOW_MFC_2)
			|| (pWnd->GetDlgCtrlID() == IDC_FAST_MFC_1) 
			|| (pWnd->GetDlgCtrlID() == IDC_FAST_MFC_2))
		{
			pDC->SetBkColor(RGB(255, 234, 234));
			pDC->SetTextColor(RGB(0, 0, 0));
			return m_main_dialog_brush;
		}
	
	}
	return hbr;
}

void CMaindialog::MC_Sequence_factory_initialization_Value_Time()
{

	// Initial Value (공정값으로 초기화)
	g_pConfig->m_dPressure_ChangeToFast_MC_Rough_ini = 50.0;	// MC Slow rough -> Fast Rough 범위 (50 Torr 까지 Slow Rough 진행)
	g_pConfig->m_dPressure_ChangeToMCTMP_Rough_ini = 0.035;		// MC Fast rough -> Tmp Rough 범위 (0.035 Torr 까지 Fast Rough 진행)
	g_pConfig->m_dPressure_Rough_End_ini = 0.000009;				// Tmp Rough -> Pumping 완료 범위 (0.000009 Torr 까지 Fast Rough 진행)
	g_pConfig->m_dPressure_ChangeToVent_ini = 0.003;			// 대기 Venting -> SlowVent 범위 (0.003 Torr 부터 Slow Vent 진행)
	g_pConfig->m_dPressure_ChangeToSlow_Vent_ini = 0.1;			// Slow Venting  ( 0.1 까지 Slow Vent 진행)	
	g_pConfig->m_dPressure_Vent_End_ini = 760.0;				// Fast Venting  (760 Torr 까지 Fast Vent 진행)
	// Initial Time (공정값으로 초기화)
	g_pConfig->m_nTimeout_sec_MCSlowRough_ini = 600;			// MC Slow rough -> 600 초 10분 동안 진행
	g_pConfig->m_nTimeout_sec_MCFastRough_ini = 600;			// MC Fast rough -> 600 초 10분 동안 진행
	g_pConfig->m_nTimeout_sec_MCTmpEnd_ini = 3600;				// MC Tmp rough -> 3600 초 60분 동안 진행
	g_pConfig->m_nTimeout_sec_MCLLCVent_ini = 1800;				// MC Vent 시 TMP Gate Close 후 대기 venting 시간.(30분)
	g_pConfig->m_nTimeout_sec_MCSlow1Vent_ini = 1800;			// MC Slow Vent -> 1800 초 30분 동안 진행
	g_pConfig->m_nTimeout_sec_MCFastVent_ini = 1800;			// MC Fast Vent -> 1800 초 30분 동안 진행

}

void CMaindialog::MC_Sequence_Time_Check()
{
	CString mc_slow_rough_time_cnt_str, mc_fast_rough_time_cnt_str, mc_tmp_rough_time_cnt_str;
	CString mc_standby_vent_time_cnt_str, mc_slow_vent_time_cnt_str, mc_fast_vent_time_cnt_str;
	CString llc_slow_rough_time_cnt_str, llc_fast_rough_time_cnt_str, llc_tmp_rough_time_cnt_str;
	CString llc_standby_vent_time_cnt_str, llc_slow_vent_time_cnt_str, llc_fast_vent_time_cnt_str;


	mc_slow_rough_time_cnt_str.Empty();
	mc_fast_rough_time_cnt_str.Empty();
	mc_tmp_rough_time_cnt_str.Empty();
	mc_standby_vent_time_cnt_str.Empty();
	mc_slow_vent_time_cnt_str.Empty();
	mc_fast_vent_time_cnt_str.Empty();
	llc_slow_rough_time_cnt_str.Empty();
	llc_fast_rough_time_cnt_str.Empty();
	llc_tmp_rough_time_cnt_str.Empty();
	llc_standby_vent_time_cnt_str.Empty();
	llc_slow_vent_time_cnt_str.Empty();
	llc_fast_vent_time_cnt_str.Empty();

	mc_slow_rough_time_cnt_str.Format("%d", g_pVP->m_nMc_slow_rough_time_cnt);
	mc_fast_rough_time_cnt_str.Format("%d", g_pVP->m_nMc_fast_rough_time_cnt);
	mc_tmp_rough_time_cnt_str.Format("%d", g_pVP->m_nMc_tmp_rough_time_cnt);
	mc_standby_vent_time_cnt_str.Format("%d", g_pVP->m_nMc_standby_vent_time_cnt);
	mc_slow_vent_time_cnt_str.Format("%d", g_pVP->m_nMc_slow_vent_time_cnt);
	mc_fast_vent_time_cnt_str.Format("%d", g_pVP->m_nMc_fast_vent_time_cnt);
	llc_slow_rough_time_cnt_str.Format("%d", g_pVP->m_nLlc_slow_rough_time_cnt);
	llc_fast_rough_time_cnt_str.Format("%d", g_pVP->m_nLlc_fast_rough_time_cnt);
	llc_tmp_rough_time_cnt_str.Format("%d", g_pVP->m_nLlc_tmp_rough_time_cnt);
	llc_standby_vent_time_cnt_str.Format("%d", g_pVP->m_nLlc_standby_vent_time_cnt);
	llc_slow_vent_time_cnt_str.Format("%d", g_pVP->m_nLlc_slow_vent_time_cnt);
	llc_fast_vent_time_cnt_str.Format("%d", g_pVP->m_nLlc_fast_vent_time_cnt);


	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(mc_slow_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(mc_fast_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(mc_tmp_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW_TIME2))->SetWindowTextA(mc_standby_vent_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW_TIME2))->SetWindowTextA(mc_slow_vent_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW_TIME2))->SetWindowTextA(mc_fast_vent_time_cnt_str);

	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(llc_slow_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(llc_fast_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(llc_tmp_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME2))->SetWindowTextA(llc_standby_vent_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME2))->SetWindowTextA(llc_slow_vent_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME2))->SetWindowTextA(llc_fast_vent_time_cnt_str);

}
void CMaindialog::LLC_Sequence_Time_Check()
{
	CString llc_slow_rough_time_cnt_str, llc_fast_rough_time_cnt_str, llc_tmp_rough_time_cnt_str;
	CString llc_standby_vent_time_cnt_str, llc_slow_vent_time_cnt_str, llc_fast_vent_time_cnt_str;


	llc_slow_rough_time_cnt_str.Empty();
	llc_fast_rough_time_cnt_str.Empty();
	llc_tmp_rough_time_cnt_str.Empty();
	llc_standby_vent_time_cnt_str.Empty();
	llc_slow_vent_time_cnt_str.Empty();
	llc_fast_vent_time_cnt_str.Empty();

	llc_slow_rough_time_cnt_str.Format("%d", g_pVP->m_nLlc_slow_rough_time_cnt);
	llc_fast_rough_time_cnt_str.Format("%d", g_pVP->m_nLlc_fast_rough_time_cnt);
	llc_tmp_rough_time_cnt_str.Format("%d", g_pVP->m_nLlc_tmp_rough_time_cnt);
	llc_standby_vent_time_cnt_str.Format("%d", g_pVP->m_nLlc_standby_vent_time_cnt);
	llc_slow_vent_time_cnt_str.Format("%d", g_pVP->m_nLlc_slow_vent_time_cnt);
	llc_fast_vent_time_cnt_str.Format("%d", g_pVP->m_nLlc_fast_vent_time_cnt);



	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(llc_slow_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(llc_fast_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(llc_tmp_rough_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME2))->SetWindowTextA(llc_standby_vent_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME2))->SetWindowTextA(llc_slow_vent_time_cnt_str);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME2))->SetWindowTextA(llc_fast_vent_time_cnt_str);
}
void CMaindialog::MC_Sequence_Time_Ini()
{
	CString ini;
	ini.Empty();
	ini = _T("0");

	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_MC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_MC_VIEW_TIME2))->SetWindowTextA(ini);

}

void CMaindialog::LLC_Sequence_Time_Ini()
{
	CString ini;
	ini.Empty();
	ini = _T("0");

	((CStatic*)GetDlgItem(IDC_EDIT_S_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_T_ROUGH_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_S_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
	((CStatic*)GetDlgItem(IDC_EDIT_F_VENT_LLC_VIEW_TIME2))->SetWindowTextA(ini);
}

void CMaindialog::OnStnClickedIconTrgate()
{
	// TRGATE OPEN / CLOSE

	//if (g_pIO->Is_TRGateValve_Open() == VALVE_OPENED)
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[g_pIO->DI::TR_GATE_VALVE_OPEN] == true) && (g_pIO->m_bDigitalIn[g_pIO->DI::TR_GATE_VALVE_CLOSE] == false)) // TR  GATE OPEN
		{
			if (AfxMessageBox("TR Gate Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_TRGateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("TR Gate Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("TR Gate Close 완료");
				}

			}
		}
		//else if (g_pIO->Is_TRGateValve_Open() == VALVE_CLOSED)
		else if ((g_pIO->m_bDigitalIn[g_pIO->DI::TR_GATE_VALVE_OPEN] == false) && (g_pIO->m_bDigitalIn[g_pIO->DI::TR_GATE_VALVE_CLOSE] == true)) // TR GATE CLOSE
		{
			if (AfxMessageBox("TR Gate Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_TRGateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("TR Gate Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("TR Gate Open 완료");
				}

			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}

}

void CMaindialog::OnStnClickedIconLlcgate()
{
	// LLCGATE OPEN / CLOSE
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[g_pIO->DI::LLC_GATE_VALVE_OPEN] == true) && (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_GATE_VALVE_CLOSE] == false)) // LLC GATE OPEN
		{
			if (AfxMessageBox("LLC Gate Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_LLCGateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Gate Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Gate Close 완료");
				}
			}
		}
		else if ((g_pIO->m_bDigitalIn[g_pIO->DI::LLC_GATE_VALVE_OPEN] == false) && (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_GATE_VALVE_CLOSE] == true)) // LLC GATE CLOSE
		{
			if (AfxMessageBox("LLC Gate Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_LLCGateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Gate Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Gate Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconLlcTmpGate()
{
	// LLC TMP GATE OPEN / CLOSE
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[g_pIO->DI::LLC_TMP_GATE_VALVE_OPEN] == true) && (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_TMP_GATE_VALVE_CLOSE] == false)) // LLC TMP GATE OPEN
		{
			if (AfxMessageBox("LLC Tmp Gate Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Close_LLC_TMP_GateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Tmp Gate Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Tmp Gate Close 완료");
				}
			}
		}
		else if ((g_pIO->m_bDigitalIn[g_pIO->DI::LLC_TMP_GATE_VALVE_OPEN] == false) && (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_TMP_GATE_VALVE_CLOSE] == true)) // LLC TMP GATE CLOSE
		{
			if (AfxMessageBox("LLC Tmp Gate Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Open_LLC_TMP_GateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Tmp Gate Open 실패", MB_ICONERROR);
					g_pLog->Display(0, g_pIO->Log_str);
				}
				else
				{
					AfxMessageBox("LLC Tmp Gate Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconMcTmpGate()
{
	// MC TMP GATE OPEN / CLOSE
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[g_pIO->DI::MC_TMP_GATE_VALVE_OPEN] == true) && (g_pIO->m_bDigitalIn[g_pIO->DI::MC_TMP_GATE_VALVE_CLOSE] == false))// MC TMP GATE OPEN
		{
			if (AfxMessageBox("MC Tmp Gate Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Close_MC_TMP_GateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Tmp Gate Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Tmp Gate Close 완료");
				}
			}
		}
		else if ((g_pIO->m_bDigitalIn[g_pIO->DI::MC_TMP_GATE_VALVE_OPEN] == false) && (g_pIO->m_bDigitalIn[g_pIO->DI::MC_TMP_GATE_VALVE_CLOSE] == true))// MC TMP GATE CLOSE
		{
			if (AfxMessageBox("MC Tmp Gate Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_MC_TMP_GateValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Tmp Gate Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Tmp Gate Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconMcRoughValve() 
{
	// MC SLOW ROUGH VALVE OPEN / CLOSE
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if (g_pIO->m_bDigitalOut[g_pIO->DO::MC_SLOW_ROUGHING] == true) // MC SLOW ROUGH OPEN
		{
			if (AfxMessageBox("MC Slow Rough Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Close_MC_SlowRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Slow Rough Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Slow Rough Valve Close 완료");
				}
			}
		}
		else if (g_pIO->m_bDigitalOut[g_pIO->DO::MC_SLOW_ROUGHING] == false) // MC SLOW ROUGH CLOSE
		{
			if (AfxMessageBox("MC Slow Rough Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Open_MC_SlowRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Slow Rough Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Slow Rough Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconMcFastRough()
{
	// MC FAST ROUGH VALVE OPEN / CLOSE
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[g_pIO->DI::MC_FAST_ROUGHING_VALVE_OPEN] == true) && (g_pIO->m_bDigitalIn[g_pIO->DI::MC_ROUGHING_VALVE_CLOSE] == false)) // MC FAST ROUGH OPEN
		{
			if (AfxMessageBox("MC Fast Rough Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Close_MC_FastRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Fast Rough Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Fast Rough Valve Close 완료");
				}
			}
		}
		else if ((g_pIO->m_bDigitalIn[g_pIO->DI::MC_FAST_ROUGHING_VALVE_OPEN] == false) && (g_pIO->m_bDigitalIn[g_pIO->DI::MC_ROUGHING_VALVE_CLOSE] == true)) // MC FAST ROUGH CLOSE
		{
			if (AfxMessageBox("MC Fast Rough Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{

				if (g_pIO->Open_MC_FastRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Fast Rough Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Fast Rough Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconMcForeline()
{
	// MC FORELINE VALVE OPEN/ CLOSE
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[g_pIO->DI::MC_FORELINE_VALVE_OPEN] == true) && (g_pIO->m_bDigitalIn[g_pIO->DI::MC_FORELINE_VALVE_CLOSE] == false)) // MC FORELINE GATE OPEN
		{
			if (AfxMessageBox("MC Tmp Foreline Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_MC_TMP_ForelineValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Tmp Foreline Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Tmp Foreline Valve Close 완료");
				}
			}
		}
		else if ((g_pIO->m_bDigitalIn[g_pIO->DI::MC_FORELINE_VALVE_OPEN] == false) && (g_pIO->m_bDigitalIn[g_pIO->DI::MC_FORELINE_VALVE_CLOSE] == true))  // MC FORELINE GATE CLOSE
		{
			if (AfxMessageBox("MC Tmp Foreline Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_MC_TMP_ForelineValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MC Tmp Foreline Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MC Tmp Foreline Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconLlcForeline()
{
	// LLC FORELINE VAVLE OPEN / CLOSE
	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		if ((g_pIO->m_bDigitalIn[g_pIO->DI::LLC_FORELINE_VALVE_OPEN] == true) && (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_FORELINE_VALVE_CLOSE] == false)) // LLC FORELINE OPEN
		{
			if (AfxMessageBox("LLC Tmp Foreline Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_LLC_TMP_ForelineValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Tmp Foreline Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Tmp Foreline Valve Close 완료");
				}
			}
		}
		else if ((g_pIO->m_bDigitalIn[g_pIO->DI::LLC_FORELINE_VALVE_OPEN] == false) && (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_FORELINE_VALVE_CLOSE] == true))// LLC FORELINE CLOSE
		{
			if (AfxMessageBox("LLC Tmp Foreline Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_LLC_TMP_ForelineValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Tmp Foreline Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Tmp Foreline Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconLlcRoughValve()
{
	// LLC SLOW ROUGH VALVE OPEN / CLOSE
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if (g_pIO->m_bDigitalOut[7] == true)// LLC SLOW ROUGH OPEN
		{
			if (AfxMessageBox("LLC Slow Rough Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_LLC_SlowRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Slow Rough Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Slow Rough Valve Close 완료");
				}
			}
		}
		else if (g_pIO->m_bDigitalOut[7] == false)// LLC SLOW ROUGH CLOSE
		{
			if (AfxMessageBox("LLC Slow Rough Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_LLC_SlowRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Slow Rough Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Slow Rough Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}

void CMaindialog::OnStnClickedIconLlcFastRough()
{
	// LLC FAST ROUGH VALVE OPEN / CLOSE
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_FAST_ROUGHING_VALVE_OPEN] == true)// LLC FAST ROUGH OPEN
		{
			if (AfxMessageBox("LLC Fast Rough Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_LLC_FastRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Fast Rough Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Fast Rough Valve Close 완료");
				}
			}
		}
		else if (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_FAST_ROUGHING_VALVE_OPEN] == true)// LLC FAST ROUGH CLOSE
		{
			if (AfxMessageBox("LLC Fast Rough Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_LLC_FastRoughValve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("LLC Fast Rough Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("LLC Fast Rough Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}



void CMaindialog::OnStnClickedIconSlowMfcIn()
{
	// SLOW VENT INLET
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if (g_pIO->m_bDigitalOut[g_pIO->DO::SLOW_VENT_INLET] == true)// MFC SLOW INLET OPEN
		{
			if (AfxMessageBox("MFC Slow Vent Inlet Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_SlowVent_Inlet_Valve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MFC Slow Vent Inlet Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MFC Slow Vent Inlet Valve Close 완료");
				}
			}
		}
		if (g_pIO->m_bDigitalOut[g_pIO->DO::SLOW_VENT_INLET] == false)// MFC SLOW INLET CLOSE
		{
			if (AfxMessageBox("MFC Slow Vent Inlet Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_SlowVent_Inlet_Valve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MFC Slow Vent Inlet Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MFC Slow Vent Inlet Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}


void CMaindialog::OnStnClickedIconSlowMfcOut()
{
	// SLOW VENT OUTLET
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if (g_pIO->m_bDigitalOut[g_pIO->DO::SLOW_VENT_OUTLET] == true)// MFC SLOW OUTLET OPEN
		{
			if (AfxMessageBox("MFC Slow Vent Outlet Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_SlowVent_Outlet_Valve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MFC Slow Vent Outlet Valve Close 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MFC Slow Vent Outlet Valve Close 완료");
				}
			}
		}
		else if (g_pIO->m_bDigitalOut[g_pIO->DO::SLOW_VENT_OUTLET] == false)// MFC SLOW OUTLET CLOSE
		{
			if (AfxMessageBox("MFC Slow Vent Outlet Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_SlowVent_Outlet_Valve() != OPERATION_COMPLETED)
				{
					AfxMessageBox("MFC Slow Vent Outlet Valve Open 실패", MB_ICONERROR);
				}
				else
				{
					AfxMessageBox("MFC Slow Vent Outlet Valve Open 완료");
				}
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}


void CMaindialog::OnStnClickedIconFastMfcIn()
{
	// FAST VENT INLET
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if (g_pIO->m_bDigitalOut[g_pIO->DO::FAST_VENT_INLET] == true)// MFC FAST INLET OPEN
		{
			if (AfxMessageBox("MFC Fast Vent Inlet Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_FastVent_Inlet_Valve() != OPERATION_COMPLETED)
					AfxMessageBox("MFC Fast Vent Inlet Valve Close 실패", MB_ICONERROR);
				else
					AfxMessageBox("MFC Fast Vent Inlet Valve Close 완료");
			}
		}
		else if (g_pIO->m_bDigitalOut[g_pIO->DO::FAST_VENT_INLET] == false)// MFC SLOW INLET CLOSE
		{
			if (AfxMessageBox("MFC Fast Vent Inlet Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Open_FastVent_Inlet_Valve() != OPERATION_COMPLETED)
					AfxMessageBox("MFC Fast Vent Inlet Valve Open 실패", MB_ICONERROR);
				else
					AfxMessageBox("MFC Fast Vent Inlet Valve Open 완료");
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}


void CMaindialog::OnStnClickedIconFastMfcOut()
{
	// FAST VENT OUTLET
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if (g_pIO->m_bDigitalOut[g_pIO->DO::FAST_VENT_OUTLET] == true)// MFC FAST OUTLET OPEN
		{
			if (AfxMessageBox("MFC Fast Vent Outlet Valve Close \n 실행하시겠습니까?", MB_YESNO) == IDYES)
			{
				if (g_pIO->Close_FastVent_Outlet_Valve() != OPERATION_COMPLETED)
					AfxMessageBox("MFC Fast Vent Outlet Valve Close 실패", MB_ICONERROR);
				else
					AfxMessageBox("MFC Fast Vent Outlet Valve Close 완료");
			}
		}
		else if (g_pIO->m_bDigitalOut[g_pIO->DO::FAST_VENT_OUTLET] == false)// MFC FAST OUTLET CLOSE
		{
			if (AfxMessageBox("MFC Fast Vent Inlet Valve Open \n 실행하시겠습니까?", MB_YESNO) == IDYES, MB_ICONQUESTION)
			{
				if (g_pIO->Open_FastVent_Outlet_Valve() != OPERATION_COMPLETED)
					AfxMessageBox("MFC Fast Vent Inlet Valve Open 실패", MB_ICONERROR);
				else
					AfxMessageBox("MFC Fast Vent Inlet Valve Open 완료", MB_ICONINFORMATION);
			}
		}
	}
	else
	{
		AfxMessageBox("IO Connection Fail \n (IO 점검 요망)", MB_ICONERROR);
	}
}
