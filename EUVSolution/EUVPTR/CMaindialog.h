﻿#pragma once


// CMaindialog 대화 상자

class CMaindialog : public CDialogEx , public CECommon
{
	DECLARE_DYNAMIC(CMaindialog)

public:
	CMaindialog(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CMaindialog();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SREM_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	HICON	m_LedIcon[3];

	HBITMAP	m_GateValveBITMAP[2];
	HBITMAP m_GateBITMAP[2];
	HBITMAP m_ValveBITMAP[2];
	HBITMAP m_TmpBITMAP[2];
	HBITMAP m_MFCBITMAP[2];

	DECLARE_MESSAGE_MAP()
public:

	CImage m_image;

	clock_t	m_start_time, m_finish_time;

	BOOL m_bLlcSeqStop;
	BOOL m_bMcSeqStop;

	void Update();
	void initial();
	void LLC_Tmp_State_Update();
	void MC_Tmp_State_Update();
	void Gauge_State_Update();
	void Vent_State_Update();
	void Line_State_Update();
	void IO_State_Update();
	void Mask_Location_Check();
	void Main_Stage_Loading_Position_Check();
	/** Sequence 값 원복 함수 */
	void MC_Sequence_factory_initialization_Value_Time();


	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedMcPump();
	afx_msg void OnBnClickedMcVent();
	afx_msg void OnBnClickedMcStop();
	afx_msg void OnBnClickedLlcPump();
	afx_msg void OnBnClickedLlcVent();
	afx_msg void OnBnClickedLlcStop();
	afx_msg void OnBnClickedMcOn();
	afx_msg void OnBnClickedMcDefault();
	afx_msg void OnBnClickedLlcOn();
	afx_msg void OnBnClickedLlcDefault();
	afx_msg void OnBnClickedMcAutoSeqStateReset();
	afx_msg void OnBnClickedLlcAutoSeqStateReset();
	afx_msg void OnBnClickedLlcTmpOn();
	afx_msg void OnBnClickedLlcTmpOff();
	afx_msg void OnBnClickedMcTmpOn();
	afx_msg void OnBnClickedMcTmpOff();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	CBrush  m_brush;
	CFont	m_font;
	CBrush  m_main_dialog_brush;
	CString vacuum_data_file;

	int CTyear;
	int CTmon;
	int CTday;
	int CThour;
	int CTmin;
	int CTsec;
	int	m_CheckTimer;		//Timer for Gauge reading 
	int Cnt_Error_Slow_MFC;
	int Cnt_Error_Fast_MFC;
	bool vacuum_gauge_write_stop;


	void MC_Sequence_Time_Check();
	void LLC_Sequence_Time_Check();
	void LLC_Sequence_Time_Ini();
	void MC_Sequence_Time_Ini();

	HICON m_hIcon;
	
	afx_msg void OnStnClickedIconTrgate();
	afx_msg void OnStnClickedIconLlcgate();
	afx_msg void OnStnClickedIconLlcTmpGate();
	afx_msg void OnStnClickedIconMcTmpGate();
	afx_msg void OnStnClickedIconMcRoughValve();
	afx_msg void OnStnClickedIconMcFastRough();
	afx_msg void OnStnClickedIconMcForeline();
	afx_msg void OnStnClickedIconLlcForeline();
	afx_msg void OnStnClickedIconLlcRoughValve();
	afx_msg void OnStnClickedIconLlcFastRough();
	afx_msg void OnStnClickedIconSlowMfcIn();
	afx_msg void OnStnClickedIconSlowMfcOut();
	afx_msg void OnStnClickedIconFastMfcIn();
	afx_msg void OnStnClickedIconFastMfcOut();

	double m_dMcStandbyVentValue;
	double m_dMcSlowVentValue;
	double m_dMcFastVentValue;
	double m_dMcSlowRoughValue;
	double m_dMcFastRoughValue;
	double m_dMcTmpRoughValue;
	int m_nMcTmpRoughTime;
	int m_nMcSlowVentTime;
	int m_nMcFastRoughTime;
	int m_nMcFastVentTime;
	int m_nMcSlowRoughTime;
	int m_nMcStandbyVentTime;
	double m_dLlcSlowRoughValue;
	double m_dLlcFastRoughValue;
	double m_dLlcTmpRoughValue;
	double m_dLlcStandbyVentValue;
	double m_dLlcSlowVentValue;
	double m_dLlcFastVentValue;
	int m_nLlcSlowRoughTime;
	int m_nLlcFastRoughTime;
	int m_nLlcTmpRoughTime;
	int m_nLlcStandbyVentTime;
	int m_nLlcSlowVentTime;
	int m_nLlcFastVentTime;

	BOOL m_bMcPumpingFlag;
	BOOL m_bLLcPumpingFlag;

	void SequenceMCICONUpdate(int StateNum);
	void SequenceLLCICONUpdate(int StateNum);

	//void MCVentingStart();
	//void XrayCameraTempeSet(double SetPoint);
	//void XrayCameraGetTempe();
};
