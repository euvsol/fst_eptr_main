﻿// CMessageDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "include.h"


// CMessageDlg 대화 상자

IMPLEMENT_DYNAMIC(CMessageDlg, CDialogEx)

CMessageDlg::CMessageDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MESSAGE_DIALOG, pParent)
	, m_strMessage(_T(""))
{
	offsetpt.x = offsetpt.y = 0;
}

CMessageDlg::~CMessageDlg()
{
}

void CMessageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_MESSAGE_TEXT, m_strMessage);
}


BEGIN_MESSAGE_MAP(CMessageDlg, CDialogEx)
END_MESSAGE_MAP()


// CMessageDlg 메시지 처리기


BOOL CMessageDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	CRect rc;
	GetWindowRect(rc);
	int width = rc.Width();
	int height = rc.Height();
	rc.top = 600;
	rc.left = 1100;
	rc.bottom = rc.top + height;
	rc.right = rc.left + width;
	rc.OffsetRect(offsetpt);
	MoveWindow(rc);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

INT_PTR CMessageDlg::DoModal(CString str)
{
	m_strMessage = str;

	return CDialogEx::DoModal();
}
