﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CTurboPumpDlg 대화 상자

IMPLEMENT_DYNAMIC(CTurboPumpDlg, CDialogEx)

CTurboPumpDlg::CTurboPumpDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TURBO_PUMP_DIALOG, pParent)
{
	m_LLCTmpThread = NULL;
	m_LLCTmpThreadExitFlag = FALSE;
	m_nTmpState = 0;

	g_pDevMgr->RegisterObserver(this);
}

CTurboPumpDlg::~CTurboPumpDlg()
{
}

void CTurboPumpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTurboPumpDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_LLC_TMP_BTN_ON, &CTurboPumpDlg::OnBnClickedLlcTmpBtnOn)
	ON_BN_CLICKED(IDC_LLC_TMP_BTN_OFF, &CTurboPumpDlg::OnBnClickedLlcTmpBtnOff)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CTurboPumpDlg 메시지 처리기


BOOL CTurboPumpDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CTurboPumpDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_LLC_ICON_CON))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[0]);
	
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CTurboPumpDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	m_LLCTmpThreadExitFlag = TRUE;

	if (m_LLCTmpThread != NULL)
	{
		HANDLE threadHandle = m_LLCTmpThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/3000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_LLCTmpThread = NULL;
	}
}

int CTurboPumpDlg::OpenDevice()
{
	int nRet = OpenSerialPort(g_pConfig->m_chPORT[SERIAL_LLC_TMP], 
								g_pConfig->m_nBAUD_RATE[SERIAL_LLC_TMP], 
								g_pConfig->m_nUSE_BIT[SERIAL_LLC_TMP], 
								g_pConfig->m_nSTOP_BIT[SERIAL_LLC_TMP], 
								g_pConfig->m_nPARITY[SERIAL_LLC_TMP]);

	if (nRet != 0) 
	{
		SetDlgItemText(IDC_LLC_TMP_HZ, _T("Connection Fail"));
		SetDlgItemText(IDC_LLC_TMP_STATIC2, _T("Connection Fail"));
		SetDlgItemText(IDC_LLC_TMP_STATIC3, _T("Connection Fail"));
		SetDlgItemText(IDC_LLC_TMP_STATIC4, _T("Connection Fail"));
		((CStatic*)GetDlgItem(IDC_LLC_ICON_CON))->SetIcon(m_LedIcon[2]);
	}
	else 
	{
		m_LLCTmpThread = AfxBeginThread(Update_tmp_Thread, (LPVOID)this, THREAD_PRIORITY_NORMAL, 0, 0);
		
		SetTimer(LLC_TMP_UPDATE_TIMER, 100, NULL);
		((CStatic*)GetDlgItem(IDC_LLC_ICON_CON))->SetIcon(m_LedIcon[1]);
	}

	return nRet;
}

UINT CTurboPumpDlg::Update_tmp_Thread(LPVOID pParam)
{
	CTurboPumpDlg* tmprunthread = (CTurboPumpDlg*)pParam;

	while (!tmprunthread->m_LLCTmpThreadExitFlag)
	{
		tmprunthread->LLCTmpSendUpdate();
		Sleep(1000);
	}

	return 0;
}


void CTurboPumpDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	
	if (nIDEvent == LLC_TMP_UPDATE_TIMER)
	{
		if (!m_LLCTmpThreadExitFlag)
		{
			LLCTmpReceiveDataUpdate();
			SetTimer(nIDEvent, 100, NULL);
		}
	}

	__super::OnTimer(nIDEvent);
}

void CTurboPumpDlg::EmergencyStop()
{
	TRACE(_T("LLCTMP START\n"));
	
	CString log_str;

	if (g_pIO->Off_LlcDryPump_Valve())
	{
		log_str = " MAIN_ERROR_ON() : Error 발생 후 LLC TMP STOP , LLC Dry Pump Off 실행 !";
		SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
		//////////////////////////////////////////////////////////////////
		// TMP 완전 멈춤, 그후 DRY PUMP OFF 후 MC FORELINE GATE CLOSE
		//////////////////////////////////////////////////////////////////

		//////////////////////////
		// LLC FORELINE GATE CLOSE
		//////////////////////////
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP 정상 멈춤, 그 후 LLC Foreline Gate  Close !";
		SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
		
		if (g_pIO->Close_LLC_TMP_ForelineValve() != OPERATION_COMPLETED)
		{
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP 정상 멈춤. 그 후 LLC Foreline Gate Close 명령 에러 발생 !";
			SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, log_str);
			g_pLog->Display(0, g_pIO->Log_str);
			if (g_pIO->Close_LLC_TMP_ForelineValve() != OPERATION_COMPLETED)
			{
				log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP 정상 멈춤. 그 후 LLC Foreline Gate Close 명령 에러에 따른 재 실행 !";
				SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
				SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
				g_pLog->Display(0, log_str);
				g_pLog->Display(0, g_pIO->Log_str);
				return;
			}
		}
		SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, g_pIO->Log_str);

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_CLOSE) break;
		}

		if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSED)
		{
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP 정상 멈춤. 그 후 LLC Foreline Gate Close 확인 에러 발생 !";
			SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			g_pLog->Display(0, log_str);

			return ;
		}

		log_str = "MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP 정상 멈춤. 그 후 LLC Foreline Gate Close 확인 완료!";
		SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
	}
	else
	{
		log_str = " MAIN_ERROR_ON() : Error 발생 후 LLC TMP STOP , LLC Dry Pump Off 실행 Error (확인필요) !";
		SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
	}

	TRACE(_T("LLCTMP END\n"));
}


void CTurboPumpDlg::LLCTmpReceiveDataUpdate()
{
	CString str;

	str.Format(_T("%d hz"), GetLlcTmphz());
	SetDlgItemText(IDC_LLC_TMP_HZ, str);

	str.Format(_T("%d rpm"), GetLlcTmprpm());
	SetDlgItemText(IDC_LLC_TMP_RPM, str);

	str.Format(_T("%d ℃"), GetLlcTmpTemperature());
	SetDlgItemText(IDC_LLC_TMP_TEMP1, str);

	str.Format(_T("%d ℃"), GetLlcTmpTemperatureHeatSink());
	SetDlgItemText(IDC_LLC_TMP_TEMP2, str);

	str.Format(_T("%d ℃"), GetLlcTmpETemperatureAir());
	SetDlgItemText(IDC_LLC_TMP_TEMP3, str);

	str.Format(_T("%d"), GetLlcTmpErrorCode());
	SetDlgItemText(IDC_LLC_TMP_STATIC4, str);

	switch (m_nTmpState)
	{
		case Stop:
			str.Format(_T("Stop"));
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[2]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[0]);
			break;
		case Working_Intlk:
			str.Format(_T("Working Intlk"));
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[2]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[2]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[2]);
			break;
		case Starting:
			str.Format(_T("Starting"));
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[1]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[0]);
			break;
		case Auto_Tuning:
			str.Format(_T("Auto Tuning"));
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[2]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[2]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[2]);
			break;
		case Breaking:
			str.Format(_T("Breaking"));
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[2]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[0]);
			break;
		case Running:
			str.Format(_T("Running"));
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[1]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[0]);
			break;
		case Fail:
			str.Format(_T("Fail"));
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ON))->SetIcon(m_LedIcon[2]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_OFF))->SetIcon(m_LedIcon[2]);
			((CStatic*)GetDlgItem(IDC_LLC_ICON_ERROR))->SetIcon(m_LedIcon[2]);
			break;
		default:
			str.Format(_T("Unknown"));
			break;
	}

	SetDlgItemText(IDC_LLC_TMP_STATIC2, str);
}

void CTurboPumpDlg::OnBnClickedLlcTmpBtnOn()
{
	int ret = g_pLLCTmp_IO->LLCTmp_On_Com();
	if (ret != 0)
		AfxMessageBox(_T("로드락 터보 펌프를 On 하는데 실패했습니다."));
}

void CTurboPumpDlg::OnBnClickedLlcTmpBtnOff()
{
	int ret = LLCTmp_Off_Com();
	if (ret != 0)
		AfxMessageBox(_T("로드락 터보 펌프를 Off 하는데 실패했습니다."));
}

int CTurboPumpDlg::LLC_Tmp_Off()
{
	int ret = LLCTmp_Off_Com();

	if (ret != 0)
		return RUN;
	else 
		return RUN_OFF;
}

int CTurboPumpDlg::LLC_Tmp_On()
{
	int ret = LLCTmp_On_Com();

	if (ret != 0)
		return RUN_OFF;
	else
		return RUN;
}

void CTurboPumpDlg::LLCTmpSendUpdate()
{
	int ret = 0;

	ret = LLCTmp_Monitor_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Monitor_Com() _ Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Monitor_Com() ")));	//통신 상태 기록.
	}

	ret = LLCTmp_HzRead_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_HzRead_Com() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_HzRead_Com() ")));	//통신 상태 기록.
	}
	ret = LLCTmp_RpmRead_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_RpmRead_Com() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_RpmRead_Com() ")));	//통신 상태 기록.
	}
	ret = LLCTmp_ErrorCode_Check_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_ErrorCode_Check_Com() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_ErrorCode_Check_Com() ")));	//통신 상태 기록.
	}
	ret = LLCTmp_Temprerature_Check_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Temprerature_Check_Com() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Temprerature_Check_Com() ")));	//통신 상태 기록.
	}
	ret = LLCTmp_Controller_Heatsink_Temprerature_Check_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Controller_Heatsink_Temprerature_Check_Com() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Controller_Heatsink_Temprerature_Check_Com() ")));	//통신 상태 기록.
	}
	ret = LLCTmp_Controller_Air_Temprerature_Check_Com();
	Sleep(300);
	if (ret != 0)
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Controller_Air_Temprerature_Check_Com() Send Fail")));	//통신 상태 기록.
	}
	else
	{
		SaveLogFile("LLC_TMP_Com", _T((LPSTR)(LPCTSTR)("PC -> LLC TMP : LLCTmp_Controller_Air_Temprerature_Check_Com() ")));	//통신 상태 기록.
	}
}