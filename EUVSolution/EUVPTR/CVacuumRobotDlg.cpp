﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

// CVacuumRobotDlg 대화 상자

IMPLEMENT_DYNAMIC(CVacuumRobotDlg, CDialogEx)

CVacuumRobotDlg::CVacuumRobotDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_VACUUM_ROBOT_DIALOG, pParent)
{
	m_pVmtrThread		= NULL;
	g_pDevMgr->RegisterObserver(this);
}

CVacuumRobotDlg::~CVacuumRobotDlg()
{
}

void CVacuumRobotDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_VROBOT_Z_EDIT, m_ZPosCtrl);
	DDX_Control(pDX, IDC_VROBOT_T_EDIT, m_TPosCtrl);
	DDX_Control(pDX, IDC_VROBOT_L_EDIT, m_LPosCtrl);
	DDX_Control(pDX, IDC_VROBOT_ERRCODE_EDIT, m_ERRCodeCtrl);
}


BEGIN_MESSAGE_MAP(CVacuumRobotDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_VROBOT_HOME_BUTTON, &CVacuumRobotDlg::OnBnClickedVrobotHomeButton)
	ON_BN_CLICKED(IDC_VROBOT_READY_BUTTON, &CVacuumRobotDlg::OnBnClickedVrobotReadyButton)
	ON_BN_CLICKED(IDC_VROBOT_STOP_BUTTON, &CVacuumRobotDlg::OnBnClickedVrobotStopButton)
	ON_BN_CLICKED(IDC_VROBOT_RESET_BUTTON, &CVacuumRobotDlg::OnBnClickedVrobotResetButton)
	ON_BN_CLICKED(IDC_VROBOT_PAUSE_CHECK, &CVacuumRobotDlg::OnBnClickedVrobotPauseCheck)
	ON_BN_CLICKED(IDC_VROBOT_GETSPEED_BUTTON, &CVacuumRobotDlg::OnBnClickedVrobotGetspeedButton)
	ON_BN_CLICKED(IDC_VROBOT_SETSPEED_BUTTON, &CVacuumRobotDlg::OnBnClickedVrobotSetspeedButton)
	ON_BN_CLICKED(IDC_PICK_LLC_BUTTON, &CVacuumRobotDlg::OnBnClickedPickLlkButton)
	ON_BN_CLICKED(IDC_PLACE_LLC_BUTTON, &CVacuumRobotDlg::OnBnClickedPlaceLlkButton)
	ON_BN_CLICKED(IDC_PICK_MC_BUTTON, &CVacuumRobotDlg::OnBnClickedPickMcButton)
	ON_BN_CLICKED(IDC_PLACE_MC_BUTTON, &CVacuumRobotDlg::OnBnClickedPlaceMcButton)
	ON_BN_CLICKED(IDC_EXPICK_LLC_BUTTON, &CVacuumRobotDlg::OnBnClickedExpickLlkButton)
	ON_BN_CLICKED(IDC_EXPLACE_LLC_BUTTON, &CVacuumRobotDlg::OnBnClickedExplaceLlkButton)
	ON_BN_CLICKED(IDC_REPICK_LLC_BUTTON, &CVacuumRobotDlg::OnBnClickedRepickLlkButton)
	ON_BN_CLICKED(IDC_REPLACE_LLC_BUTTON, &CVacuumRobotDlg::OnBnClickedReplaceLlkButton)
	ON_BN_CLICKED(IDC_GOTOG1_LLC_BUTTON, &CVacuumRobotDlg::OnBnClickedGotog1LlkButton)
	ON_BN_CLICKED(IDC_GOTOP1_LLC_BUTTON, &CVacuumRobotDlg::OnBnClickedGotop1LlkButton)
	ON_BN_CLICKED(IDC_EXPICK_MC_BUTTON, &CVacuumRobotDlg::OnBnClickedExpickMcButton)
	ON_BN_CLICKED(IDC_EXPLACE_MC_BUTTON, &CVacuumRobotDlg::OnBnClickedExplaceMcButton)
	ON_BN_CLICKED(IDC_REPICK_MC_BUTTON, &CVacuumRobotDlg::OnBnClickedRepickMcButton)
	ON_BN_CLICKED(IDC_REPLACE_MC_BUTTON, &CVacuumRobotDlg::OnBnClickedReplaceMcButton)
	ON_BN_CLICKED(IDC_GOTOG1_MC_BUTTON, &CVacuumRobotDlg::OnBnClickedGotog1McButton)
	ON_BN_CLICKED(IDC_GOTOP1_MC_BUTTON, &CVacuumRobotDlg::OnBnClickedGotop1McButton)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_VROBOT_SRV_ON_BUTTON, &CVacuumRobotDlg::OnBnClickedVrobotSrvOnButton)
	ON_BN_CLICKED(IDC_VROBOT_SRV_OFF_BUTTON, &CVacuumRobotDlg::OnBnClickedVrobotSrvOffButton)
END_MESSAGE_MAP()


// CVacuumRobotDlg 메시지 처리기

BOOL CVacuumRobotDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CVacuumRobotDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	InitializeControls();
	
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CVacuumRobotDlg::InitializeControls()
{
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_VMTR_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_VMTR_HOME_COMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_VMTR_SERVO_ON))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_VMTR_READY_POS))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_VMTR_CHANGE_BATT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_VMTR_WORKING))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_VMTR_EMO))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_VMTR_ERROR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_VMTR_AUTO_MODE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_VMTR_TEACHING_MODE))->SetIcon(m_LedIcon[0]);
}

int CVacuumRobotDlg::OpenDevice()
{
	int nRet = 0;

	nRet = OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_VMTR], g_pConfig->m_nPORT[ETHERNET_VMTR], FALSE);

	if (nRet == 0)
	{
		VmtrState = GET_SPEED;
		m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
		SetTimer(VMTRROBOT_UPDATE_TIMER, 500, NULL);
	}

	return nRet;
}

UINT CVacuumRobotDlg::VRobotCommandThread(LPVOID pClass)
{
	CVacuumRobotDlg* pHWCntl = (CVacuumRobotDlg *)pClass;

	int nRet = 0;

	switch (pHWCntl->VmtrState)
	{
		case HOME:
			g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_INIT"));
			nRet = pHWCntl->MoveRobotHome();
			if (nRet != 0)
				nRet = pHWCntl->MoveRobotHome();
			break;
		case READY:
			g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_INIT"));
			nRet = pHWCntl->MoveRobotReady();
			if(nRet != 0)
				nRet = pHWCntl->MoveRobotReady();
			break;
		case GET_SPEED:
		{
			nRet = pHWCntl->ReqRobotSpeed();
			if (nRet == 0)
			{
				CString strSpeed = _T("");
				strSpeed.Format("%d", pHWCntl->m_nRobotSpeed);
				pHWCntl->SetDlgItemText(IDC_VROBOT_SPEED_EDIT, strSpeed);
			}
			break;
		}
		case SET_SPEED:
		{
			CString strSpeed = _T("");
			pHWCntl->GetDlgItemText(IDC_VROBOT_SPEED_EDIT, strSpeed);
			int nRobotSpeed = atoi(strSpeed);

			if (nRobotSpeed > 0 && nRobotSpeed <= 100)
				nRet = pHWCntl->SetRobotSpeed(nRobotSpeed);
			break;
		}
		case LLC_PICK:
			g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_PICK_LLC"));
			nRet = pHWCntl->PickMask(LLC_STATION);
			if (nRet != 0)
			{
				nRet = pHWCntl->PickMask(LLC_STATION);
				if(nRet != 0)
					break;
			}
			
			g_pConfig->SaveMaterialLocation(VACUUM_ROBOT_HAND);

			//nRet = g_pIO->Is_Mask_OnLLC();
			//if (nRet != MASK_NONE)
			//{
			//	nRet = VMTR_EXIST_MASK_LLC_AFTER_LLC_PICK;
			//	break;
			//}
			//
			//nRet = g_pIO->Is_LLC_Mask_Slant_Check();
			//if (nRet != FALSE)
			//{
			//	nRet = VMTR_TILT_MASK_LLC_AFTER_LLC_PICK;
			//	break;
			//}

			break;
		case LLC_PLACE:
			g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_PLACE_LLC"));
#if TRUE	//구분동작 테스트
			nRet = pHWCntl->PlaceMask(LLC_STATION);
			if (nRet != 0)
			{
				nRet = pHWCntl->PlaceMask(LLC_STATION);
				if(nRet != 0)
					break;
			}

			//nRet = g_pIO->Is_Mask_OnLLC();
			//if (nRet != MASK_ON)
			//{
			//	nRet = VMTR_NO_MASK_LLC_AFTER_LLC_PLACE;
			//	break;
			//}
			//else
			//	g_pConfig->SaveMaterialLocation(LLC);
			//
			//nRet = g_pIO->Is_LLC_Mask_Slant_Check();
			//if (nRet != FALSE)
			//{
			//	nRet = VMTR_TILT_MASK_LLC_AFTER_LLC_PLACE;
			//	break;
			//}
#else
			nRet = pHWCntl->MoveReservedPosition(LLC_STATION, _T("P4"));
			if (nRet != 0)
				break;

			nRet = g_pIO->Is_Mask_Check_OnLLC();
			if (nRet != MASK_ON)
			{
				nRet = VMTR_NO_MASK_LLC_AFTER_LLC_PLACE;
				break;
			}
			else
				g_pConfig->SaveMaterialLocation(LLC);

			nRet = g_pIO->Is_LLC_Mask_Slant_Check();
			if (nRet != FALSE)
			{
				nRet = VMTR_TILT_MASK_LLC_AFTER_LLC_PLACE;
				break;
			}

			nRet = pHWCntl->MoveReservedPosition(LLC_STATION, _T("P5"));
			if (nRet != 0)
				break;
#endif
			break;
		case MC_PICK:
			g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_PICK_MC"));
			nRet = pHWCntl->PickMask(CHUCK_STATION);
			if (nRet != 0)
			{
				nRet = pHWCntl->PickMask(CHUCK_STATION);
				if(nRet != 0)
					break;
			}

			//g_pConfig->SaveMaterialLocation(VACUUM_ROBOT_HAND);
			//
			//nRet = g_pIO->Is_Mask_OnChuck();
			//if (nRet != MASK_NONE)
			//{
			//	nRet = VMTR_EXIST_MASK_MC_AFTER_MC_PICK;
			//	break;
			//}
			//
			//nRet = g_pIO->Is_MC_Mask_Slant_Check();
			//if (nRet != FALSE)
			//{
			//	nRet = VMTR_TILT_MASK_MC_AFTER_MC_PICK;
			//	break;
			//}

			break;
		case MC_PLACE:
			g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_PLACE_MC"));
#if TRUE	//구분동작 테스트
			nRet = pHWCntl->PlaceMask(CHUCK_STATION);
			if (nRet != 0)
			{
				nRet = pHWCntl->PlaceMask(CHUCK_STATION);
				if(nRet != 0)
					break;
			}

			if (nRet == 0)
			{
				if (g_pIO->Is_Mask_Check_Only_OnChuck() == TRUE)
					g_pConfig->SaveMaterialLocation(CHUCK);
			}

			//nRet = g_pIO->Is_Mask_OnChuck();
			//if (nRet != MASK_ON)
			//{
			//	nRet = VMTR_NO_MASK_MC_AFTER_MC_PLACE;
			//	break;
			//}
			//else
			//	g_pConfig->SaveMaterialLocation(CHUCK);
			//
			//nRet = g_pIO->Is_MC_Mask_Slant_Check();
			//if (nRet != FALSE)
			//{
			//	nRet = VMTR_TILT_MASK_MC_AFTER_MC_PLACE;
			//	break;
			//}
#else
			nRet = pHWCntl->MoveReservedPosition(CHUCK_STATION, _T("P4"));
			if (nRet != 0)
				break;

			nRet = g_pIO->Is_Mask_OnChuck();
			if (nRet != MASK_ON)
			{
				nRet = VMTR_NO_MASK_MC_AFTER_MC_PLACE;
				break;
			}
			else
				g_pConfig->SaveMaterialLocation(CHUCK);

			nRet = g_pIO->Is_MC_Mask_Slant_Check();
			if (nRet != FALSE)
			{
				nRet = VMTR_TILT_MASK_MC_AFTER_MC_PLACE;
				break;
			}

			nRet = pHWCntl->MoveReservedPosition(CHUCK_STATION, _T("P5"));
			if (nRet != 0)
				break;
#endif
			break;
		case LLC_PICK_READY:
			g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_READY_LLC"));
			nRet = pHWCntl->MoveReservedPosition(LLC_STATION, GET_READY_POS);
			if(nRet != 0)
				nRet = pHWCntl->MoveReservedPosition(LLC_STATION, GET_READY_POS);
			break;
		case LLC_PICK_EXTEND:
			nRet = pHWCntl->ExtendToPickMask(LLC_STATION);
			break;
		case LLC_PICK_RETRACT:
			nRet = pHWCntl->RetractToPickMask(LLC_STATION);
			break;
		case LLC_PLACE_READY:
			g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_READY_LLC"));
			nRet = pHWCntl->MoveReservedPosition(LLC_STATION, PUT_READY_POS);
			if(nRet != 0)
				nRet = pHWCntl->MoveReservedPosition(LLC_STATION, PUT_READY_POS);
			break;
		case LLC_PLACE_EXTEND:
			nRet = pHWCntl->ExtendToPlaceMask(LLC_STATION);
			break;
		case LLC_PLACE_RETRACT:
			nRet = pHWCntl->RetractToPlaceMask(LLC_STATION);
			break;
		case MC_PICK_READY:
			g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_READY_MC"));
			nRet = pHWCntl->MoveReservedPosition(CHUCK_STATION, GET_READY_POS);
			if(nRet != 0)
				nRet = pHWCntl->MoveReservedPosition(CHUCK_STATION, GET_READY_POS);
			break;
		case MC_PICK_EXTEND:
			nRet = pHWCntl->ExtendToPickMask(CHUCK_STATION);
			break;
		case MC_PICK_RETRACT:
			nRet = pHWCntl->RetractToPickMask(CHUCK_STATION);
			break;
		case MC_PLACE_READY:
			g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_READY_MC"));
			nRet = pHWCntl->MoveReservedPosition(CHUCK_STATION, PUT_READY_POS);
			if(nRet != 0)
				nRet = pHWCntl->MoveReservedPosition(CHUCK_STATION, PUT_READY_POS);
			break;
		case MC_PLACE_EXTEND:
			nRet = pHWCntl->ExtendToPlaceMask(CHUCK_STATION);
			break;
		case MC_PLACE_RETRACT:
			nRet = pHWCntl->RetractToPlaceMask(CHUCK_STATION);
			break;
	}

	pHWCntl->m_pVmtrThread = NULL;

	if (nRet != 0)
		g_pAlarm->SetAlarm(nRet);

	return 0;
}


void CVacuumRobotDlg::GetDeviceStatus()
{
	if(m_bConnected == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_CONNECT))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_CONNECT))->SetIcon(m_LedIcon[0]);

	if (m_bHomeComplete == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_HOME_COMP))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_HOME_COMP))->SetIcon(m_LedIcon[0]);
	
	if (m_bReadyPosition == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_READY_POS))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_READY_POS))->SetIcon(m_LedIcon[0]);
	
	if (m_bServoMotorOn == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_SERVO_ON))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_SERVO_ON))->SetIcon(m_LedIcon[0]);
	
	if (m_bRobotWorking == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_WORKING))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_WORKING))->SetIcon(m_LedIcon[0]);
	
	if (m_bChangeBattery == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_CHANGE_BATT))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_CHANGE_BATT))->SetIcon(m_LedIcon[0]);
	
	if (m_bEMOStatus == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_EMO))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_EMO))->SetIcon(m_LedIcon[0]);
	
	if (m_bOccurredErr == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_ERROR))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_ERROR))->SetIcon(m_LedIcon[0]);		
	
	if (m_bRobotAutoMode == TRUE)
	{
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_AUTO_MODE))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_TEACHING_MODE))->SetIcon(m_LedIcon[0]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_AUTO_MODE))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_VMTR_TEACHING_MODE))->SetIcon(m_LedIcon[1]);
	}

	CString tmp;
	tmp.Format("%.2f", m_dPositionZ);
	if (m_ZPosCtrl.m_hWnd != NULL)
		m_ZPosCtrl.SetWindowText(tmp);
	tmp.Format("%.2f", m_dPositionL);
	if (m_LPosCtrl.m_hWnd != NULL)
		m_LPosCtrl.SetWindowText(tmp);
	tmp.Format("%.2f", m_dPositionT);
	if (m_TPosCtrl.m_hWnd != NULL)
		m_TPosCtrl.SetWindowText(tmp);
	tmp.Format("%s", m_sErrorCode);
	if (m_ERRCodeCtrl.m_hWnd != NULL)
		m_ERRCodeCtrl.SetWindowText(tmp);
}

void CVacuumRobotDlg::OnDestroy()
{
	CDialogEx::OnDestroy();
}

void CVacuumRobotDlg::OnBnClickedVrobotSrvOnButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedVrobotSrvOnButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	int nRet = ServoOn();
	if(nRet != 0)
		g_pAlarm->SetAlarm(nRet);
}


void CVacuumRobotDlg::OnBnClickedVrobotSrvOffButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedVrobotSrvOffButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	int nRet = ServoOff();
	if (nRet != 0)
		g_pAlarm->SetAlarm(nRet);
}

void CVacuumRobotDlg::OnBnClickedVrobotStopButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedVrobotStopButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	RobotMoveStop();
}


void CVacuumRobotDlg::OnBnClickedVrobotResetButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedVrobotResetButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	ResetError();
}


void CVacuumRobotDlg::OnBnClickedVrobotPauseCheck()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedVrobotPauseCheck() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	CButton* btn = (CButton*)GetDlgItem(IDC_VROBOT_PAUSE_CHECK);

	int nCheck = btn->GetCheck();

	if (nCheck)
	{
		RobotMovePause();
		btn->SetWindowTextA(_T("Resume"));
	}
	else
	{
		RobotMoveResume();
		btn->SetWindowTextA(_T("Pause"));
	}
}


void CVacuumRobotDlg::OnBnClickedVrobotGetspeedButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedVrobotGetspeedButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = GET_SPEED;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedVrobotSetspeedButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedVrobotSetspeedButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = SET_SPEED;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedVrobotHomeButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedVrobotHomeButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("ROBOT HOME 동작을 할까요?", MB_YESNO)) return;

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = HOME;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedVrobotReadyButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedVrobotReadyButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("ROBOT READY 동작을 할까요?", MB_YESNO)) return;

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = READY;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedPickLlkButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedPickLlkButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("LLC에 있는 MASK를 가져올까요?", MB_YESNO)) return;

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	//로봇암 마스크 유무 확인

	int ret = g_pIO->Is_Mask_OnLLC();
	if (ret != MASK_ON)
	{
		AfxMessageBox(_T("LLC에 마스크가 없습니다."), MB_ICONWARNING);
		return;
	}
	
	ret = g_pIO->Is_LLC_Mask_Slant_Check();
	if (ret != FALSE)
	{
		AfxMessageBox(_T("LLC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}
	

	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		AfxMessageBox(_T("TR Gate Valve가 닫혀있습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = LLC_PICK;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}

void CVacuumRobotDlg::OnBnClickedPlaceLlkButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedPlaceLlkButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("LLC에 MASK를 가져다 놓을까요?", MB_YESNO)) return;

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	//로봇암 마스크 유무 확인

	int ret = g_pIO->Is_Mask_OnLLC();
	if (ret != MASK_NONE)
	{
		AfxMessageBox(_T("LLC에 마스크가 있습니다."), MB_ICONWARNING);
		return;
	}
	
	ret = g_pIO->Is_LLC_Mask_Slant_Check();
	if (ret != FALSE)
	{
		AfxMessageBox(_T("LLC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}

	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		AfxMessageBox(_T("TR Gate Valve가 닫혀있습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = LLC_PLACE;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedPickMcButton()
{	
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedPickMcButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("CHUCK에 있는 MASK를 가져올까요?", MB_YESNO)) return;

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	//로봇암 마스크 유무 확인


	if (!g_pNavigationStage->Is_Loading_Positioin())
	{
		AfxMessageBox(_T("네비게이션 스테이지가 로딩위치가 아닙니다."), MB_ICONWARNING);
		return;
	}
	
	int ret = g_pIO->Is_Mask_OnChuck();
	if (ret != MASK_ON)
	{
		AfxMessageBox(_T("MC에 마스크가 없습니다."), MB_ICONWARNING);
		return;
	}
	
	ret = g_pIO->Is_MC_Mask_Slant_Check();
	if (ret != FALSE)
	{
		AfxMessageBox(_T("MC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = MC_PICK;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedPlaceMcButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedPlaceMcButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("CHUCK에 MASK를 가져다 놓을까요?", MB_YESNO)) return;

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	//로봇암 마스크 유무 확인

	if (!g_pNavigationStage->Is_Loading_Positioin())
	{
		AfxMessageBox(_T("네비게이션 스테이지가 로딩위치가 아닙니다."), MB_ICONWARNING);
		return;
	}
	
	int ret = g_pIO->Is_Mask_OnChuck();
	if (ret != MASK_NONE)
	{
		AfxMessageBox(_T("MC에 마스크가 있습니다."), MB_ICONWARNING);
		return;
	}
	
	ret = g_pIO->Is_MC_Mask_Slant_Check();
	if (ret != FALSE)
	{
		AfxMessageBox(_T("MC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = MC_PLACE;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedExpickLlkButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedExpickLlkButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	//로봇암 마스크 유무 확인

	int ret = g_pIO->Is_Mask_OnLLC();
	if (ret != MASK_ON)
	{
		AfxMessageBox(_T("LLC에 마스크가 없습니다."), MB_ICONWARNING);
		return;
	}

	ret = g_pIO->Is_LLC_Mask_Slant_Check();
	if (ret != FALSE)
	{
		AfxMessageBox(_T("LLC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}
	
	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		AfxMessageBox(_T("TR Gate Valve가 닫혀있습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = LLC_PICK_EXTEND;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedExplaceLlkButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedExplaceLlkButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	//로봇암 마스크 유무 확인

	int ret = g_pIO->Is_Mask_OnLLC();
	if (ret != MASK_NONE)
	{
		AfxMessageBox(_T("LLC에 마스크가 있습니다."), MB_ICONWARNING);
		return;
	}
	
	ret = g_pIO->Is_LLC_Mask_Slant_Check();
	if (ret != FALSE)
	{
		AfxMessageBox(_T("LLC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}

	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		AfxMessageBox(_T("TR Gate Valve가 닫혀있습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = LLC_PLACE_EXTEND;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedRepickLlkButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedRepickLlkButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = LLC_PICK_RETRACT;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedReplaceLlkButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedReplaceLlkButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = LLC_PLACE_RETRACT;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedGotog1LlkButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedGotog1LlkButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}
	
	//로봇암 마스크 유무 확인

	int ret = g_pIO->Is_Mask_OnLLC();
	if (ret != MASK_ON)
	{
		AfxMessageBox(_T("LLC에 마스크가 없습니다."), MB_ICONWARNING);
		return;
	}
	
	ret = g_pIO->Is_LLC_Mask_Slant_Check();
	if (ret != FALSE)
	{
		AfxMessageBox(_T("LLC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}

	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		AfxMessageBox(_T("TR Gate Valve가 닫혀있습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = LLC_PICK_READY;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedGotop1LlkButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedGotop1LlkButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	//로봇암 마스크 유무 확인

	int ret = g_pIO->Is_Mask_OnLLC();
	if (ret != MASK_NONE)
	{
		AfxMessageBox(_T("LLC에 마스크가 있습니다."), MB_ICONWARNING);
		return;
	}
	
	ret = g_pIO->Is_LLC_Mask_Slant_Check();
	if (ret != FALSE)
	{
		AfxMessageBox(_T("LLC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}

	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		AfxMessageBox(_T("TR Gate Valve가 닫혀있습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = LLC_PLACE_READY;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedExpickMcButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedExpickMcButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	//로봇암 마스크 유무 확인

	if (!g_pNavigationStage->Is_Loading_Positioin())
	{
		AfxMessageBox(_T("네비게이션 스테이지가 로딩위치가 아닙니다."), MB_ICONWARNING);
		return;
	}
	
	int ret = g_pIO->Is_Mask_OnChuck();
	if (ret != MASK_ON)
	{
		AfxMessageBox(_T("MC에 마스크가 없습니다."), MB_ICONWARNING);
		return;
	}
	
	ret = g_pIO->Is_MC_Mask_Slant_Check();
	if(ret != FALSE)
	{
		AfxMessageBox(_T("MC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = MC_PICK_EXTEND;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedExplaceMcButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedExplaceMcButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	//로봇암 마스크 유무 확인

	if (!g_pNavigationStage->Is_Loading_Positioin())
	{
		AfxMessageBox(_T("네비게이션 스테이지가 로딩위치가 아닙니다."), MB_ICONWARNING);
		return;
	}
	
	int ret = g_pIO->Is_Mask_OnChuck();
	if (ret != MASK_NONE)
	{
		AfxMessageBox(_T("MC에 마스크가 있습니다."), MB_ICONWARNING);
		return;
	}

	ret = g_pIO->Is_MC_Mask_Slant_Check();
	if (ret != FALSE)
	{
		AfxMessageBox(_T("MC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = MC_PLACE_EXTEND;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedRepickMcButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedRepickMcButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = MC_PICK_RETRACT;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedReplaceMcButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedReplaceMcButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = MC_PLACE_RETRACT;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedGotog1McButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedGotog1McButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	//로봇암 마스크 유무 확인

	if (!g_pNavigationStage->Is_Loading_Positioin())
	{
		AfxMessageBox(_T("네비게이션 스테이지가 로딩위치가 아닙니다."), MB_ICONWARNING);
		return;
	}

	int ret = g_pIO->Is_Mask_OnChuck();
	if (ret != MASK_ON)
	{
		AfxMessageBox(_T("MC에 마스크가 없습니다."), MB_ICONWARNING);
		return;
	}
	
	ret = g_pIO->Is_MC_Mask_Slant_Check();
	if (ret != FALSE)
	{
		AfxMessageBox(_T("MC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = MC_PICK_READY;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}


void CVacuumRobotDlg::OnBnClickedGotop1McButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CVacuumRobotDlg::OnBnClickedGotop1McButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bRobotWorking == TRUE || m_pVmtrThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	//로봇암 마스크 유무 확인

	if (!g_pNavigationStage->Is_Loading_Positioin())
	{
		AfxMessageBox(_T("네비게이션 스테이지가 로딩위치가 아닙니다."), MB_ICONWARNING);
		return;
	}

	int ret = g_pIO->Is_Mask_OnChuck();
	if (ret != MASK_NONE)
	{
		AfxMessageBox(_T("MC에 마스크가 있습니다."), MB_ICONWARNING);
		return;
	}
	
	ret = g_pIO->Is_MC_Mask_Slant_Check();
	if (ret != FALSE)
	{
		AfxMessageBox(_T("MC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}

	VmtrState = MC_PLACE_READY;
	m_pVmtrThread = AfxBeginThread(CVacuumRobotDlg::VRobotCommandThread, this);
}

int CVacuumRobotDlg::Transfer_Mask(int nSource, int nTarget)
{
	int ret = 0;

	if (nSource == VACUUM_ROBOT_HAND) //Place Mask
	{
		switch (nTarget)
		{
			case LLC:
				g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_PLACE_LLC"));
				ret = PlaceMask(LLC_STATION);
				if (ret == 0)
				{
					if (g_pIO->Is_Mask_Check_Only_OnLLC() == TRUE)
						g_pConfig->SaveMaterialLocation(LLC);
				}
				break;
			case CHUCK:
				g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_PLACE_MC"));
				ret = PlaceMask(CHUCK_STATION);
				if (ret == 0)
				{
					if (g_pIO->Is_Mask_Check_Only_OnChuck() == TRUE)
						g_pConfig->SaveMaterialLocation(CHUCK);
				}
				break;
		}
	}
	else //Pick Mask
	{
		switch (nSource)
		{
		case LLC:
			g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_PICK_LLC"));
			ret = PickMask(LLC_STATION);
			if (ret == 0)
			{
				//만약 진공로봇위에 마스크가 감지되면...
				g_pConfig->SaveMaterialLocation(VACUUM_ROBOT_HAND);
			}
			break;
		case CHUCK:
			g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_PICK_MC"));
			ret = PickMask(CHUCK_STATION);
			if (ret == 0)
			{
				//만약 진공로봇위에 마스크가 감지되면...
				g_pConfig->SaveMaterialLocation(VACUUM_ROBOT_HAND);
			}
			break;
		}
	}

	return ret;
}

int CVacuumRobotDlg::MovePickReadyPos(int nTarget)
{
	int ret = 0;

	switch (nTarget)
	{
	case LLC:
		g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_READY_LLC"));
		ret = MoveReservedPosition(LLC_STATION, GET_READY_POS);
		break;
	case CHUCK:
		g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_READY_MC"));
		ret = MoveReservedPosition(CHUCK_STATION, GET_READY_POS);
		break;
	}

	return ret;
}

int CVacuumRobotDlg::MovePlaceReadyPos(int nTarget)
{
	int ret = 0;

	switch (nTarget)
	{
	case LLC:
		g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_READY_LLC"));
		ret = MoveReservedPosition(LLC_STATION, PUT_READY_POS);
		break;
	case CHUCK:
		g_pAnimationGUI->SendMessageData(_T("EQ_VMTR_READY_MC"));
		ret = MoveReservedPosition(CHUCK_STATION, PUT_READY_POS);
		break;
	}

	return ret;
}

void CVacuumRobotDlg::EmergencyStop()
{
	CString str;
	str.Format(_T("CVacuumRobotDlg::EmergencyStop(), Robot Working %d"), m_bRobotWorking);
	g_pLog->Display(0, str);
	
	RobotMoveStop();
	str.Format(_T("CVacuumRobotDlg::EmergencyStop(), RobotMoveStop()"));
	g_pLog->Display(0, str);
}

void CVacuumRobotDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case VMTRROBOT_UPDATE_TIMER:
		ReqRobotStatus();
		Sleep(200);
		ReqRobotPosition();
		GetDeviceStatus();
		SetTimer(nIDEvent, 500, NULL);
		break;
	default:
		break;
	}

	__super::OnTimer(nIDEvent);
}

void CVacuumRobotDlg::RobotStop()
{
	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	RobotMoveStop();
}
