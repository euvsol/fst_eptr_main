﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CNavigationStageDlg 대화 상자

IMPLEMENT_DYNAMIC(CNavigationStageDlg, CDialogEx)

CNavigationStageDlg::CNavigationStageDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_NAVIGATION_STAGE_DIALOG, pParent)
	, m_strMovingDistance(_T("0.0"))
	, m_nStagePosNumber(0)
	, m_strGetXPos(_T("0.0"))
	, m_strGetYPos(_T("0.0"))
	, m_strStagePosition(_T("0"))
{
	m_dMovingDistance = 0.1;
	m_GetZOldPos = 0.0;
	m_nLoadingPosSensorX = 0;
	m_nLoadingPosSensorY = 0;

	/*m_bLaserValueBeforeSwitchingX = 0.0;
	m_bLaserValueBeforeSwitchingY = 0.0;
	m_bLaserValueAfterSwitchingX = 0.0;
	m_bLaserValueAfterSwitchingY = 0.0;
	m_bDeltaValueAfterSwitchingX = 0.0;
	m_bDeltaValueAfterSwitchingY = 0.0;*/

	g_pDevMgr->RegisterObserver(this);
}

CNavigationStageDlg::~CNavigationStageDlg()
{
}

void CNavigationStageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_INCSTEP, m_strMovingDistance);
	DDX_Control(pDX, IDC_EDIT_XFPOS, m_XPosCtrl);
	DDX_Control(pDX, IDC_EDIT_YFPOS, m_YPosCtrl);
	DDX_Text(pDX, IDC_EDIT_POSNUM, m_nStagePosNumber);
	DDX_Text(pDX, IDC_EDIT_GETXPOS, m_strGetXPos);
	DDX_Text(pDX, IDC_EDIT_GETYPOS, m_strGetYPos);
	DDX_Text(pDX, IDC_EDIT_COMMENT, m_strStagePosition);
	DDX_Control(pDX, IDC_CHECK_XENABLE, m_chkXEnableCtrl);
	DDX_Control(pDX, IDC_CHECK_YENABLE, m_chkYEnableCtrl);
	DDX_Control(pDX, IDC_CHECK_XINPOSITION, m_chkXInpositionCtrl);
	DDX_Control(pDX, IDC_CHECK_YINPOSITION, m_chkYInpositionCtrl);
	DDX_Control(pDX, IDC_CHECK_XMOVING, m_chkXMovingCtrl);
	DDX_Control(pDX, IDC_CHECK_YMOVING, m_chkYMovingCtrl);
	DDX_Control(pDX, IDC_CHECK_XACCEL, m_chkXAccelCtrl);
	DDX_Control(pDX, IDC_CHECK_YACCEL, m_chkYAccelCtrl);
	DDX_Control(pDX, IDC_CHECK_XLOADINGPOS, m_chkXLoadingPos);
	DDX_Control(pDX, IDC_CHECK_YLOADINGPOS, m_chkYLoadingPos);
	DDX_Control(pDX, IDC_CHECK_XPLUSLIMIT, m_chkXPlusLimitCtrl);
	DDX_Control(pDX, IDC_CHECK_YPLUSLIMIT, m_chkYPlusLimitCtrl);
	DDX_Control(pDX, IDC_CHECK_XMINUSLIMIT, m_chkXMinusLimitCtrl);
	DDX_Control(pDX, IDC_CHECK_YMINUSLIMIT, m_chkYMinusLimitCtrl);
	DDX_Control(pDX, IDC_CHECK_XSOFTPLUSLIMIT, m_chkXSoftPlusLimitCtrl);
	DDX_Control(pDX, IDC_CHECK_YSOFTPLUSLIMIT, m_chkYSoftPlusLimitCtrl);
	DDX_Control(pDX, IDC_CHECK_XSOFTMINUSLIMIT, m_chkXSoftMinusLimitCtrl);
	DDX_Control(pDX, IDC_CHECK_YSOFTMINUSLIMIT, m_chkYSoftMinusLimitCtrl);
	DDX_Control(pDX, IDC_CHECK_XMOTORFAULT, m_chkXMotorFaultCtrl);
	DDX_Control(pDX, IDC_CHECK_YMOTORFAULT, m_chkYMotorFaultCtrl);
	DDX_Control(pDX, IDC_POSNUMSPIN, m_PosNumSpin);
	DDX_Control(pDX, IDC_BUTTON_XENABLE, m_XEnableCtrl);
	DDX_Control(pDX, IDC_BUTTON_YENABLE, m_YEnableCtrl);
	DDX_Control(pDX, IDC_BUTTON_XMINUS, m_btn_X_Minus_Ctrl);
	DDX_Control(pDX, IDC_BUTTON_XPLUS, m_btn_X_Plus_Ctrl);
	DDX_Control(pDX, IDC_BUTTON_YMINUS, m_btn_Y_Minus_Ctrl);
	DDX_Control(pDX, IDC_BUTTON_YPLUS, m_btn_Y_Plus_Ctrl);
	DDX_Control(pDX, IDC_CHECK_STAGEMOVE_AT_ZORIGIN, m_CheckStageMoveAtZOCtrl);
}


BEGIN_MESSAGE_MAP(CNavigationStageDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_ZERORETURN, &CNavigationStageDlg::OnBnClickedButtonZeroreturn)
	ON_BN_CLICKED(IDC_BUTTON_ZEROSET, &CNavigationStageDlg::OnBnClickedButtonZeroset)
	ON_BN_CLICKED(IDC_BUTTON_XENABLE, &CNavigationStageDlg::OnBnClickedButtonXenable)
	ON_BN_CLICKED(IDC_BUTTON_YENABLE, &CNavigationStageDlg::OnBnClickedButtonYenable)
	ON_BN_CLICKED(IDC_BUTTON_STOP, &CNavigationStageDlg::OnBnClickedButtonStop)
	ON_BN_CLICKED(IDC_BUTTON_ESTOP, &CNavigationStageDlg::OnBnClickedButtonEstop)
	ON_BN_CLICKED(IDC_BUTTON_XMINUS, &CNavigationStageDlg::OnBnClickedButtonXminus)
	ON_BN_CLICKED(IDC_BUTTON_XPLUS, &CNavigationStageDlg::OnBnClickedButtonXplus)
	ON_BN_CLICKED(IDC_BUTTON_YPLUS, &CNavigationStageDlg::OnBnClickedButtonYplus)
	ON_BN_CLICKED(IDC_BUTTON_YMINUS, &CNavigationStageDlg::OnBnClickedButtonYminus)
	ON_BN_CLICKED(IDC_BUTTON_STAGEREADY, &CNavigationStageDlg::OnBnClickedButtonStageready)
	ON_EN_CHANGE(IDC_EDIT_INCSTEP, &CNavigationStageDlg::OnEnChangeEditIncstep)
	ON_EN_CHANGE(IDC_EDIT_POSNUM, &CNavigationStageDlg::OnEnChangeEditPosnum)
	ON_EN_CHANGE(IDC_EDIT_GETXPOS, &CNavigationStageDlg::OnEnChangeEditGetxpos)
	ON_EN_CHANGE(IDC_EDIT_GETYPOS, &CNavigationStageDlg::OnEnChangeEditGetypos)
	ON_BN_CLICKED(IDC_BUTTON_SAVEPOS, &CNavigationStageDlg::OnBnClickedButtonSavepos)
	ON_BN_CLICKED(IDC_BUTTON_MOVEPOS, &CNavigationStageDlg::OnBnClickedButtonMovepos)
	ON_BN_CLICKED(IDC_BUTTON_MOVEORIGIN, &CNavigationStageDlg::OnBnClickedButtonMoveorigin)
	ON_WM_TIMER()
	ON_NOTIFY(UDN_DELTAPOS, IDC_INCSTEPSPIN, &CNavigationStageDlg::OnDeltaposIncstepspin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_POSNUMSPIN, &CNavigationStageDlg::OnDeltaposPosnumspin)
	ON_NOTIFY(NM_CLICK, IDC_TEXT_STAGEPOS_GRID, OnStagePos_GridClick)
	ON_NOTIFY(NM_DBLCLK, IDC_TEXT_STAGEPOS_GRID, OnStagePos_GridDblClick)
	ON_NOTIFY(NM_RCLICK, IDC_TEXT_STAGEPOS_GRID, OnStagePos_GridRClick)
	ON_NOTIFY(GVN_BEGINLABELEDIT, IDC_TEXT_STAGEPOS_GRID, OnStagePos_GridStartEdit)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_TEXT_STAGEPOS_GRID, OnStagePos_GridEndEdit)
	ON_BN_CLICKED(IDC_CHECK_STAGEMOVE_AT_ZORIGIN, &CNavigationStageDlg::OnBnClickedCheckStagemoveAtZorigin)
	ON_BN_CLICKED(IDC_BUTTON_FAULTCLEAR, &CNavigationStageDlg::OnBnClickedButtonFaultclear)
	ON_BN_CLICKED(IDC_BUTTON_STAGE_SIMULATOR_MODE, &CNavigationStageDlg::OnBnClickedButtonStageSimulatorMode)
	ON_BN_CLICKED(IDC_BUTTON_STAGE_NORMAL_MODE, &CNavigationStageDlg::OnBnClickedButtonStageNormalMode)
	ON_BN_CLICKED(IDC_BUTTON_STAGE_SIMULATOR_X_HOME, &CNavigationStageDlg::OnBnClickedButtonStageSimulatorXHome)
	ON_BN_CLICKED(IDC_BUTTON_STAGE_SIMULATOR_Y_HOME, &CNavigationStageDlg::OnBnClickedButtonStageSimulatorYHome)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, &CNavigationStageDlg::OnBnClickedButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_DISCONNECT, &CNavigationStageDlg::OnBnClickedButtonDisconnect)
END_MESSAGE_MAP()


// CNavigationStageDlg 메시지 처리기


BOOL CNavigationStageDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CNavigationStageDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ICON
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);


	LOGFONT lf;

	GetDlgItem(IDC_EDIT_COMMENT)->GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	lf.lfHeight = 20;
	//strcpy(lf.lfFaceName, "Arial");//"Verdana"
	Font.CreateFontIndirect(&lf);

	GetDlgItem(IDC_EDIT_COMMENT)->SetFont(&Font);
	Font.DeleteObject();


	m_PosNumSpin.SetRange(0, 99);
	m_PosNumSpin.SetPos(m_nStagePosNumber);
	m_GetXPos = g_pConfig->m_stStagePos[99].x;
	m_GetYPos = g_pConfig->m_stStagePos[99].y;

	m_strGetXPos.Format("%0.5lf", m_GetXPos);
	m_strGetYPos.Format("%0.5lf", m_GetYPos);
	m_strStagePosition = g_pConfig->m_stStagePos[99].chStagePositionString;

	InitStagePosGrid2();

	m_btn_X_Minus_Ctrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2LT));
	m_btn_Y_Minus_Ctrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2DN));
	m_btn_X_Plus_Ctrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2RT));
	m_btn_Y_Plus_Ctrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2UP));

	UpdateData(false);

	Simulator_Mode = FALSE;

	GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_X_HOME)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_Y_HOME)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_STAGE_NORMAL_MODE)->EnableWindow(false);

	EquipmentMode_Check();

	SetTimer(NAVISTAGE_UPDATE_TIMER, 100, NULL);
	SetTimer(NAVISTAGE_LOG_WRITE_TIMER, 500, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CNavigationStageDlg::InitStagePosGrid2()
{
	CRect rect;
	GetDlgItem(IDC_TEXT_STAGEPOS_GRID)->GetWindowRect(rect);
	ScreenToClient(&rect);
	m_StagePosGrid2.Create(rect, this, IDC_TEXT_STAGEPOS_GRID);

	int nGap, col, row;
	col = 0;
	row = 0;
	nGap = 8;

	m_StagePosGrid2.SetEditable(false);
	m_StagePosGrid2.SetListMode(true);
	m_StagePosGrid2.SetSingleRowSelection(false);
	m_StagePosGrid2.EnableDragAndDrop(false);
	m_StagePosGrid2.SetFixedRowCount(1);
	m_StagePosGrid2.SetFixedColumnCount(1);
	m_StagePosGrid2.SetColumnCount(4);
	m_StagePosGrid2.SetRowCount(MAX_STAGE_POSITION + 1);
	//m_StagePosGrid.SetBkColor(RGB(192, 192, 192));
	m_StagePosGrid2.GetDefaultCell(FALSE, FALSE)->SetBackClr(WHITE);

	LOGFONT lf;
	CFont Font;
	m_StagePosGrid2.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	//lf.lfHeight=10;
	//strcpy(lf.lfFaceName,"Verdana");
	Font.CreateFontIndirect(&lf);
	m_StagePosGrid2.SetFont(&Font);
	Font.DeleteObject();

	CString str;
	str.Format("%s", "No");
	m_StagePosGrid2.SetItemText(0, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Stage Position Name");
	m_StagePosGrid2.SetItemText(0, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "X(mm)");
	m_StagePosGrid2.SetItemText(0, 2, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Y(mm)");
	m_StagePosGrid2.SetItemText(0, 3, (LPSTR)(LPCTSTR)str);

	m_StagePosGrid2.SetRowHeight(0, 35);
	m_StagePosGrid2.SetColumnWidth(0, 10);
	m_StagePosGrid2.SetColumnWidth(1, 400);
	m_StagePosGrid2.SetColumnWidth(2, 70);
	m_StagePosGrid2.SetColumnWidth(3, 70);
	m_StagePosGrid2.ExpandColumnsToFit();

	CString strNo, strX, strY;
	for (row = 1; row < MAX_STAGE_POSITION + 1; row++)
	{
		strNo.Format("%d", row - 1);
		m_StagePosGrid2.SetItemText(row, 0, strNo);
		m_StagePosGrid2.SetItemText(row, 1, g_pConfig->m_stStagePos[row - 1].chStagePositionString);
		strX.Format("%4.6f", g_pConfig->m_stStagePos[row - 1].x);
		m_StagePosGrid2.SetItemText(row, 2, strX);
		strY.Format("%4.6f", g_pConfig->m_stStagePos[row - 1].y);
		m_StagePosGrid2.SetItemText(row, 3, strY);
	}
	//m_StagePosGrid2.SetSelectedRow(1);
}



void CNavigationStageDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	if (Simulator_Mode)
	{

		DisconnectComm();
	}
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

BOOL CNavigationStageDlg::Is_Loading_Positioin()
{
	BOOL bRet = FALSE;
	CString str;

	//1. Stage Position이 맞는지 Check
	double current_posx_mm = 0.0, current_posy_mm = 0.0;
	current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	double difference_posx_mm = 0.0, difference_posy_mm = 0.0;
	difference_posx_mm = abs(g_pConfig->m_stStagePos[SYSTEM_LOADING_POSITION].x - current_posx_mm);
	difference_posy_mm = abs(g_pConfig->m_stStagePos[SYSTEM_LOADING_POSITION].y - current_posy_mm);
	if (difference_posx_mm > 0.3 || difference_posy_mm > 0.3)
	{
		str.Format("Is_Loading_Positioin(): x=%.3f , y=%.3f, distancex=%.3f, distancey=%.3f", current_posx_mm, current_posy_mm, difference_posx_mm, difference_posy_mm);
		SaveLogFile("Stage_Debug", (LPSTR)(LPCTSTR)str);

		current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
		difference_posx_mm = abs(g_pConfig->m_stStagePos[SYSTEM_LOADING_POSITION].x - current_posx_mm);
		difference_posy_mm = abs(g_pConfig->m_stStagePos[SYSTEM_LOADING_POSITION].y - current_posy_mm);
		if (difference_posx_mm > 0.3 || difference_posy_mm > 0.3)
		{
			str.Format("Is_Loading_Positioin()_retry: x=%.3f , y=%.3f, distancex=%.3f, distancey=%.3f", current_posx_mm, current_posy_mm, difference_posx_mm, difference_posy_mm);
			SaveLogFile("Stage_Debug", (LPSTR)(LPCTSTR)str);
			return FALSE;
		}
	}

	//2. Loading Position 센서에 감지되는지 Check
	m_nLoadingPosSensorX = m_chkXLoadingPos.GetCheck();
	m_nLoadingPosSensorY = m_chkYLoadingPos.GetCheck();
	return TRUE;

	if (m_nLoadingPosSensorX != 1 || m_nLoadingPosSensorY != 1)
	{
		str.Format("Is_Loading_Positioin(): m_nLoadingPosSensorX = %d , m_nLoadingPosSensorY = %d", m_nLoadingPosSensorX, m_nLoadingPosSensorY);
		SaveLogFile("Stage_Debug", (LPSTR)(LPCTSTR)str);

		m_nLoadingPosSensorX = m_chkXLoadingPos.GetCheck();
		m_nLoadingPosSensorY = m_chkYLoadingPos.GetCheck();
		if (m_nLoadingPosSensorX != 1 || m_nLoadingPosSensorY != 1)
		{
			str.Format("Is_Loading_Positioin()_retry: m_nLoadingPosSensorX = %d , m_nLoadingPosSensorY = %d", m_nLoadingPosSensorX, m_nLoadingPosSensorY);
			SaveLogFile("Stage_Debug", (LPSTR)(LPCTSTR)str);

			WaitSec(0.1);
			m_nLoadingPosSensorX = m_chkXLoadingPos.GetCheck();
			m_nLoadingPosSensorY = m_chkYLoadingPos.GetCheck();
			if (m_nLoadingPosSensorX != 1 || m_nLoadingPosSensorY != 1)
			{
				str.Format("Is_Loading_Positioin()_retry2: m_nLoadingPosSensorX = %d , m_nLoadingPosSensorY = %d", m_nLoadingPosSensorX, m_nLoadingPosSensorY);
				SaveLogFile("Stage_Debug", (LPSTR)(LPCTSTR)str);
				return FALSE;
			}
		}
	}
	return TRUE;

}


void CNavigationStageDlg::OnBnClickedButtonZeroreturn()
{
	g_pLog->Display(0, _T("CNavigationStageDlg::OnBnClickedButtonZeroreturn() Button Click!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CPasswordDlg pwdlg(this);
	pwdlg.DoModal();
	if (pwdlg.m_strTxt != "srem")
	{
		::AfxMessageBox("Password가 일치 하지 않습니다.");
		return;
	}

	//if (g_pScanStage != NULL)
	//	g_pScanStage->Move_Origin();
	WaitSec(1);

	Home(STAGE_ALL_AXIS);
}


void CNavigationStageDlg::OnBnClickedButtonZeroset()
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;
}


void CNavigationStageDlg::OnBnClickedButtonXenable()
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (m_chkXEnableCtrl.GetCheck())
	{
		SetAmpEnable(STAGE_X_AXIS, true);
	}
	else
	{
		SetAmpEnable(STAGE_X_AXIS, false);
	}
}


void CNavigationStageDlg::OnBnClickedButtonYenable()
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (m_chkYEnableCtrl.GetCheck())
	{
		SetAmpEnable(STAGE_Y_AXIS, true);
	}
	else
	{
		SetAmpEnable(STAGE_Y_AXIS, false);
	}
}


void CNavigationStageDlg::OnBnClickedButtonStop()
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	Stop(STAGE_ALL_AXIS);
}


void CNavigationStageDlg::OnBnClickedButtonEstop()
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	EStop(STAGE_ALL_AXIS);
}


void CNavigationStageDlg::OnBnClickedButtonXminus()
{
	if (Simulator_Mode)
	{

		g_pLog->Display(0, _T("[Stage Handle][Simulator Mode]  X- Button Click"));

		if (MoveRelativePosition(STAGE_X_AXIS, -m_dMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" [Simulator Mode] Stage 동작 불가!");
		}
	}
	else
	{
		g_pLog->Display(0, _T("[Stage Handle] X- Button Click"));

		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (MoveRelativePosition(STAGE_X_AXIS, -m_dMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
		}
	}
}


void CNavigationStageDlg::OnBnClickedButtonXplus()
{

	if (Simulator_Mode)
	{
		g_pLog->Display(0, _T("[Stage Handle][Simulator Mode] X+ Button Click"));
		if (MoveRelativePosition(STAGE_X_AXIS, m_dMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" [Simulator Mode] Stage 동작 불가!");
		}
	}
	else
	{
		g_pLog->Display(0, _T("[Stage Handle] X+ Button Click"));
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (MoveRelativePosition(STAGE_X_AXIS, m_dMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
		}
	}
}


void CNavigationStageDlg::OnBnClickedButtonYplus()
{
	if (Simulator_Mode)
	{
		g_pLog->Display(0, _T("[Stage Handle][Simulator Mode]  Y+ Button Click"));
		if (MoveRelativePosition(STAGE_Y_AXIS, m_dMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" [Simulator Mode] Stage 동작 불가!");
		}
	}
	else
	{
		g_pLog->Display(0, _T("[Stage Handle] Y+ Button Click"));

		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (MoveRelativePosition(STAGE_Y_AXIS, m_dMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
		}
	}
}


void CNavigationStageDlg::OnBnClickedButtonYminus()
{
	if (Simulator_Mode)
	{
		g_pLog->Display(0, _T("[Stage Handle][Simulator Mode]  Y- Button Click"));
		if (MoveRelativePosition(STAGE_Y_AXIS, -m_dMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" [Simulator Mode] Stage 동작 불가!");
		}
	}
	else
	{
		g_pLog->Display(0, _T("[Stage Handle] Y- Button Click"));

		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (MoveRelativePosition(STAGE_Y_AXIS, -m_dMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
		}
	}
}


void CNavigationStageDlg::OnBnClickedButtonStageready()
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

}


void CNavigationStageDlg::OnEnChangeEditIncstep()
{
	UpdateData(true);
	m_dMovingDistance = atof(m_strMovingDistance);
}


void CNavigationStageDlg::OnEnChangeEditPosnum()
{
	UpdateData(TRUE);
	if (m_nStagePosNumber > 99) m_nStagePosNumber = 99;
	if (m_nStagePosNumber < 0) m_nStagePosNumber = 0;


	m_GetXPos = g_pConfig->m_stStagePos[m_nStagePosNumber].x;
	m_GetYPos = g_pConfig->m_stStagePos[m_nStagePosNumber].y;
	m_strStagePosition = g_pConfig->m_stStagePos[m_nStagePosNumber].chStagePositionString;
}


void CNavigationStageDlg::OnEnChangeEditGetxpos()
{
	UpdateData(true);
	m_GetXPos = atof(m_strGetXPos);
}


void CNavigationStageDlg::OnEnChangeEditGetypos()
{
	UpdateData(true);
	m_GetYPos = atof(m_strGetYPos);
}

void CNavigationStageDlg::OnBnClickedButtonSavepos()
{
	g_pLog->Display(0, _T("CNavigationStageDlg::OnBnClickedButtonSavepos() Button Click!"));
	UpdateData(true);

	CString strX, strY;
	int row = 0;
	row = m_StagePosGrid2.GetSelectedRow();

	if (row > MAX_STAGE_POSITION - 20)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "srem")
		{
			::AfxMessageBox("Password가 일치 하지 않습니다.");
			return;
		}
	}

	sprintf(g_pConfig->m_stStagePos[row - 1].chStagePositionString, "%s", m_strStagePosition);
	g_pConfig->m_stStagePos[row - 1].x = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	g_pConfig->m_stStagePos[row - 1].y = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

	m_StagePosGrid2.SetItemText(row, 1, m_strStagePosition);
	strX.Format(_T("%4.6f"), g_pConfig->m_stStagePos[row - 1].x);
	m_StagePosGrid2.SetItemText(row, 2, strX);
	strY.Format(_T("%4.6f"), g_pConfig->m_stStagePos[row - 1].y);
	m_StagePosGrid2.SetItemText(row, 3, strY);

	m_StagePosGrid2.Invalidate(FALSE);

	g_pConfig->SaveStagePositionToDB();
}


void CNavigationStageDlg::OnBnClickedButtonMovepos()
{
	g_pLog->Display(0, _T("CNavigationStageDlg::OnBnClickedButtonMovepos() Button Click!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	UpdateData(true);

	if (MoveAbsolutePosition(atof(m_strGetXPos), atof(m_strGetYPos)) != XY_NAVISTAGE_OK)
	{
		::AfxMessageBox(" Stage 동작 불가!");
	}
}

void CNavigationStageDlg::OnBnClickedButtonMoveorigin()
{
	g_pLog->Display(0, _T("CNavigationStageDlg::OnBnClickedButtonMoveorigin() Button Click!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	//if (g_pScanStage != NULL)
	//	g_pScanStage->Move_Origin();
	WaitSec(1);

	Is_Loading_Positioin();
}

void CNavigationStageDlg::OnDeltaposIncstepspin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	UpdateData(true);

	if (pNMUpDown->iDelta < 0)
	{
		if (m_dMovingDistance > 0.0001f && m_dMovingDistance < 0.1f) m_dMovingDistance *= 10.f;
		else if (m_dMovingDistance >= 0.1f && m_dMovingDistance < 0.5f) m_dMovingDistance += 0.1f;
		else if (m_dMovingDistance >= 0.5f && m_dMovingDistance < 1.f) m_dMovingDistance = 1.0f;
		else if (m_dMovingDistance < 10.0f)	m_dMovingDistance *= 10.0f;
		else if (m_dMovingDistance >= 10.0f && m_dMovingDistance < SOFT_LIMIT_PLUS_X)	m_dMovingDistance += 10.0f;
	}
	else
	{
		if (m_dMovingDistance > 0.0001f && m_dMovingDistance <= 0.1f)
			m_dMovingDistance /= 10.f;
		else if (m_dMovingDistance > 0.1f && m_dMovingDistance <= 0.5f)
			m_dMovingDistance -= 0.1f;
		else if (m_dMovingDistance >= 0.5f && m_dMovingDistance <= 1.f)
			m_dMovingDistance = .5f;
		else if (m_dMovingDistance <= 10.0f)
			m_dMovingDistance = 1.0f;
		else if (m_dMovingDistance > 10.0f && m_dMovingDistance <= SOFT_LIMIT_PLUS_X)
			m_dMovingDistance -= 10.0f;
	}
	m_strMovingDistance.Format("%0.4f", m_dMovingDistance);

	UpdateData(false);
	*pResult = 0;
}

void CNavigationStageDlg::OnDeltaposPosnumspin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	UpdateData(true);

	if (pNMUpDown->iDelta > 0)
	{
		if (m_nStagePosNumber >= 0 && m_nStagePosNumber < 99) m_nStagePosNumber += 1;
		else if (m_nStagePosNumber >= 99)	m_nStagePosNumber = 99;
	}
	else
	{
		if (m_nStagePosNumber <= 0) m_nStagePosNumber = 0;
		else if (m_nStagePosNumber <= 99)	m_nStagePosNumber -= 1;
	}

	UpdateData(false);
	*pResult = 0;
}

void CNavigationStageDlg::OnTimer(UINT_PTR nIDEvent)
{
	int current_naviposx_submm = 0, current_naviposy_submm = 0;
	static int old_naviposx_submm = 0, old_naviposy_submm = 0;

	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case NAVISTAGE_UPDATE_TIMER:
		Navi_Stage_Connected_Check();
		if (m_bConnect == TRUE)
		{
			StagePeriodicCheck();
			//g_pTest->PI_Stage_Faltness_Monitor_Check();
		}
		SetTimer(nIDEvent, 100, NULL);
		break;
	case NAVISTAGE_LOG_WRITE_TIMER:
		if (m_bConnect)
		{
			current_naviposx_submm = (int)(GetPosmm(STAGE_X_AXIS)*10.);
			current_naviposy_submm = (int)(GetPosmm(STAGE_Y_AXIS)*10.);
			if (abs(old_naviposx_submm - current_naviposx_submm) > 0 || abs(old_naviposy_submm - current_naviposy_submm) > 0)
			{
				old_naviposx_submm = current_naviposx_submm;
				old_naviposy_submm = current_naviposy_submm;

				CString sLog;
				sLog.Format(_T("Navigation Stage - PosX [%3.7f]mm, PosY [%3.7f]mm"), GetPosmm(STAGE_X_AXIS), GetPosmm(STAGE_Y_AXIS));
				SaveLogFile("NaviStagePosition", sLog);
			}
		}
		SetTimer(nIDEvent, 500, NULL);
		break;
	default:
		break;
	}

	__super::OnTimer(nIDEvent);
}

void CNavigationStageDlg::ConvertToMaskFromStage(double stageX_mm, double stageY_mm, double& posx_mm, double& posy_mm)
{
	//CString tmp;

	double pos_mm[2];

	pos_mm[STAGE_X_AXIS] = stageX_mm;
	pos_mm[STAGE_Y_AXIS] = stageY_mm;

	//Mask Center 좌표계 Display
	double aa = 0.0, bb = 0.0, cc = 0.0, dd = 0.0;

	if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == SCOPE_OM4X && g_pMaskMap->m_bOMAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
	{
		aa = (g_pMaskMap->m_dOMAlignPointRT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		bb = (g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		cc = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		dd = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		posx_mm = (bb*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000. - MaskSize_mm / 2;
		posy_mm = (aa*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000. - MaskSize_mm / 2;
	}
	else if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV && g_pMaskMap->m_bEUVAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
	{
		aa = (g_pMaskMap->m_dEUVAlignPointRT_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		bb = (g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		cc = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		dd = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		posx_mm = (bb*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000. - MaskSize_mm / 2;
		posy_mm = (aa*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000. - MaskSize_mm / 2;
	}
	else
	{
		if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV)
		{
			posx_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - pos_mm[STAGE_X_AXIS];
			posy_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - pos_mm[STAGE_Y_AXIS];
		}
		else
		{
			posx_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - pos_mm[STAGE_X_AXIS];
			posy_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - pos_mm[STAGE_Y_AXIS];
		}
	}
	/*
		tmp.Format("%3.6f", posx_mm);
		g_pMaskMap->m_EditMaskCenterCoordXCtrl.SetWindowText(tmp);

		tmp.Format("%3.6f", posy_mm);
		g_pMaskMap->m_EditMaskCenterCoordYCtrl.SetWindowText(tmp);	*/
}

void  CNavigationStageDlg::ConvertToStageFromMask(double &stageX_mm, double &stageY_mm, double dMaskX_um, double dMaskY_um)
{
	double d1stAlignLaserX_mm = 0.0, d1stAlignLaserY_mm = 0.0;
	double d2ndAlignLaserX_mm = 0.0, d2ndAlignLaserY_mm = 0.0;
	double dRefAlignLaserX_mm = 0.0, dRefAlignLaserY_mm = 0.0;
	double maskposx_mm = 0.0, maskposy_mm = 0.0;
	double target_stageposx_mm = 0.0, target_stageposy_mm = 0.0;
	double compensate_maskposx_um = g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stOriginCoordinate_um.X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X + dMaskX_um;
	double compensate_maskposy_um = g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stOriginCoordinate_um.Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y + dMaskY_um;
	if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == SCOPE_OM4X && g_pMaskMap->m_bOMAlignComplete == TRUE)
	{
		maskposx_mm = (compensate_maskposx_um*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - (compensate_maskposx_um
				*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm
				- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y
					+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
						*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X)) + ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm
							- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X
								+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
									*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X));
		d1stAlignLaserX_mm = g_pMaskMap->m_dOMAlignPointLT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm;
		d2ndAlignLaserX_mm = g_pMaskMap->m_dOMAlignPointRT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm;
		dRefAlignLaserX_mm = g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm;
		maskposx_mm = (d2ndAlignLaserX_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.))*(compensate_maskposx_um / 1000.) - (dRefAlignLaserX_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.))*(compensate_maskposy_um / 1000.) + dRefAlignLaserX_mm;
		maskposy_mm = (compensate_maskposx_um*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - (compensate_maskposx_um
				*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm
				- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y
					+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
						*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X)) + ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm
							- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X
								+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
									*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X));
		d1stAlignLaserY_mm = g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLT_posy_mm;
		d2ndAlignLaserY_mm = g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointRT_posy_mm;
		dRefAlignLaserY_mm = g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm;
		maskposy_mm = (d2ndAlignLaserY_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.))*(compensate_maskposx_um / 1000.) - (dRefAlignLaserY_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.))*(compensate_maskposy_um / 1000.) + dRefAlignLaserY_mm;

		target_stageposx_mm = g_pMaskMap->m_dOMAlignPointLT_posx_mm + maskposx_mm;
		target_stageposy_mm = g_pMaskMap->m_dOMAlignPointLT_posy_mm - maskposy_mm;
	}
	else if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV && g_pMaskMap->m_bEUVAlignComplete == TRUE)
	{
		maskposx_mm = (compensate_maskposx_um*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - (compensate_maskposx_um
				*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm
				- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y
					+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
						*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X)) + ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm
							- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X
								+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
									*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X));
		d1stAlignLaserX_mm = g_pMaskMap->m_dEUVAlignPointLT_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm;
		d2ndAlignLaserX_mm = g_pMaskMap->m_dEUVAlignPointRT_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm;
		dRefAlignLaserX_mm = g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm;
		maskposx_mm = (d2ndAlignLaserX_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.))*(compensate_maskposx_um / 1000.) - (dRefAlignLaserX_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.))*(compensate_maskposy_um / 1000.) + dRefAlignLaserX_mm;
		maskposy_mm = (compensate_maskposx_um*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - (compensate_maskposx_um
				*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm
				- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y
					+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
						*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X)) + ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm
							- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X
								+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
									*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X));
		d1stAlignLaserY_mm = g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLT_posy_mm;
		d2ndAlignLaserY_mm = g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointRT_posy_mm;
		dRefAlignLaserY_mm = g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm;
		maskposy_mm = (d2ndAlignLaserY_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.))*(compensate_maskposx_um / 1000.) - (dRefAlignLaserY_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.))*(compensate_maskposy_um / 1000.) + dRefAlignLaserY_mm;
		double dDifference = 0.0;

		switch (g_pConfig->m_nEquipmentType)
		{
		case EUVPTR:
			target_stageposx_mm = g_pMaskMap->m_dEUVAlignPointLT_posx_mm + maskposx_mm;
			target_stageposy_mm = g_pMaskMap->m_dEUVAlignPointLT_posy_mm - maskposy_mm;
			break;
		default:
			break;
		}
		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
		{
			//g_pAdam->GetCurrentLIFPosValue();
			//g_pNavigationStage->m_bLaserValueAfterSwitchingX = g_pAdam->m_dCurrentXposition;
			//g_pNavigationStage->m_bLaserValueAfterSwitchingY = g_pAdam->m_dCurrentYposition;
			g_pNavigationStage->SetLaserMode();
			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
			WaitSec(1);
		}
	}
	else
	{
		//1.Reference 좌표계를 마스크좌표계로 변경(mm 단위)
		maskposx_mm = (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stOriginCoordinate_um.X + dMaskX_um) / 1000.;
		maskposy_mm = (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stOriginCoordinate_um.Y + dMaskY_um) / 1000.;

		//2.마스크좌표계를 스테이지 좌표계로 변경(mm 단위)
		if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV && g_pMaskMap->m_bOMAlignComplete == FALSE)
		{
			target_stageposx_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].x - maskposx_mm;
			target_stageposy_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].y - maskposy_mm;
		}
		else if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV && g_pMaskMap->m_bOMAlignComplete == TRUE)
		{

			maskposx_mm = (compensate_maskposx_um*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm))
				/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - (compensate_maskposx_um
					*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm))
				/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm
					- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y
						+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
							*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X)) + ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm
								- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X
									+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
										*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X));
			d1stAlignLaserX_mm = g_pMaskMap->m_dOMAlignPointLT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm;
			d2ndAlignLaserX_mm = g_pMaskMap->m_dOMAlignPointRT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm;
			dRefAlignLaserX_mm = g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm;
			maskposx_mm = (d2ndAlignLaserX_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.))*(compensate_maskposx_um / 1000.) - (dRefAlignLaserX_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.))*(compensate_maskposy_um / 1000.) + dRefAlignLaserX_mm;
			maskposy_mm = (compensate_maskposx_um*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm))
				/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - (compensate_maskposx_um
					*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm))
				/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm
					- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y
						+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
							*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X)) + ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm
								- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X
									+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
										*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X));
			d1stAlignLaserY_mm = g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLT_posy_mm;
			d2ndAlignLaserY_mm = g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointRT_posy_mm;
			dRefAlignLaserY_mm = g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm;
			maskposy_mm = (d2ndAlignLaserY_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.))*(compensate_maskposx_um / 1000.) - (dRefAlignLaserY_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.))*(compensate_maskposy_um / 1000.) + dRefAlignLaserY_mm;

			target_stageposx_mm = g_pMaskMap->m_dOMAlignPointLT_posx_mm + maskposx_mm - g_pConfig->m_dGlobalOffsetX_mm;
			target_stageposy_mm = g_pMaskMap->m_dOMAlignPointLT_posy_mm - maskposy_mm + g_pConfig->m_dGlobalOffsetY_mm;

			//target_stageposx_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].x - maskposx_mm;
			//target_stageposy_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].y - maskposy_mm;
		}
		else
		{
			target_stageposx_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - maskposx_mm;
			target_stageposy_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - maskposy_mm;
		}
	}

	stageX_mm = target_stageposx_mm;
	stageY_mm = target_stageposy_mm;
}


void CNavigationStageDlg::ConvertToStageFromWafer(double &stageX_mm, double &stageY_mm, double maskCoordX_um, double maskCoordY_um)
{

	double StageXmm = 0.0;
	double StageYmm = 0.0;

	if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == SCOPE_OM4X && g_pMaskMap->m_bOMAlignComplete == TRUE)
	{
		//OM 영역, align 완료
		ConvertToStageFromWaferELitho(StageXmm, StageYmm, maskCoordX_um, maskCoordY_um);
		stageX_mm = StageXmm;
		stageY_mm = StageYmm;

		//stageX_mm = c * (maskCoordX_um / 1000) - s * (maskCoordY_um / 1000) + center_x_mm;
		//stageY_mm = s * (maskCoordX_um / 1000) + c * (maskCoordY_um / 1000) + center_y_mm;
		//stageX_mm = c * maskCoordX_mm + s * maskCoordY_mm + center_x_mm;
		//stageY_mm = -s * maskCoordX_mm + c * maskCoordY_mm + center_y_mm;
	}
	else if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV && g_pMaskMap->m_bOMAlignComplete == TRUE)
	{
		//EUV 영역, align 완료
		//OM 결과 + EUV offset

		ConvertToStageFromWaferELitho(StageXmm, StageYmm, maskCoordX_um, maskCoordY_um);
		stageX_mm = StageXmm - g_pConfig->m_dGlobalOffsetX_mm;
		stageY_mm = StageYmm + g_pConfig->m_dGlobalOffsetY_mm;

	}
	else
	{
		//Align 안 된 경우
		//double maskposx_mm = g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stOriginCoordinate_um.X / 1000.0 + maskCoordX_mm;
		//double maskposy_mm = g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stOriginCoordinate_um.Y / 1000.0 + maskCoordY_mm;

		double maskposx_mm = g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stOriginCoordinate_um.X / 1000.0 + maskCoordX_um / 1000.0;
		double maskposy_mm = g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stOriginCoordinate_um.Y / 1000.0 + maskCoordY_um / 1000.0;

		//2.마스크좌표계를 스테이지 좌표계로 변경(mm 단위)
		if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV)
		{
			stageX_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].x - maskposx_mm;
			stageY_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].y - maskposy_mm;
		}
		else // OM 영역
		{
			stageX_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - maskposx_mm;
			stageY_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - maskposy_mm;
		}
	}
}

void CNavigationStageDlg::GetWaferCenterPosAndRotationELitho(double& StageCenterPosX, double& StageCenterPosY, double& RotateCos, double& RotateSin)
{
	//좌표 변환 공식
	double pi = 3.14159265358979323846;
	double rotation_rad = 0.0;

	/* Align 1 Point (recipe 기준) */
	vector<double> P1 = { g_pConfig->m_dOMAlignPointRT_X_mm, g_pConfig->m_dOMAlignPointRT_Y_mm, 0.0 };
	/* Align 2 Point (recipe 기준) */
	vector<double> P2 = { g_pConfig->m_dOMAlignPointLT_X_mm, g_pConfig->m_dOMAlignPointLT_Y_mm, 0.0 };
	/* Align 3 Point (recipe 기준) */
	vector<double> P3 = { g_pConfig->m_dOMAlignPointLB_X_mm, g_pConfig->m_dOMAlignPointLB_Y_mm, 0.0 };;
	/* Align Return 값 */
	vector<double> CenterPos;

	CenterPos = g_pMaskMap->CenterOfCircumCircle(P1, P2, P3);
	rotation_rad = g_pMaskMap->FindRotation(g_pConfig->m_dNotchAlignPoint1_X_mm, g_pConfig->m_dNotchAlignPoint1_Y_mm, g_pConfig->m_dNotchAlignPoint2_X_mm, g_pConfig->m_dNotchAlignPoint2_Y_mm);

	RotateCos = cos(rotation_rad + pi);
	RotateSin = sin(rotation_rad + pi);

	StageCenterPosX = CenterPos.at(0);
	StageCenterPosY = CenterPos.at(1);
}

void CNavigationStageDlg::ConvertToWaferFromStageELitho(double stageX_mm, double stageY_mm, double& maskCoordX_mm, double& maskCoordY_mm)
{	
	double CenterXpos = 0.0;
	double CenterYpos = 0.0;
	double RotateCospos = 0.0;
	double RotateSinpos = 0.0;

	GetWaferCenterPosAndRotationELitho(CenterXpos, CenterYpos, RotateCospos, RotateSinpos);

	maskCoordX_mm = RotateCospos * (stageX_mm - CenterXpos) + RotateSinpos * (stageY_mm - CenterYpos);
	maskCoordY_mm = -RotateSinpos * (stageX_mm - CenterXpos) + RotateCospos * (stageY_mm - CenterYpos);

}

void CNavigationStageDlg::ConvertToStageFromWaferELitho(double &stageX_mm, double &stageY_mm, double maskCoordX_um, double maskCoordY_um)
{
	double CenterXpos = 0.0;
	double CenterYpos = 0.0;
	double RotateCospos = 0.0;
	double RotateSinpos = 0.0;

	GetWaferCenterPosAndRotationELitho(CenterXpos, CenterYpos, RotateCospos, RotateSinpos);

	stageX_mm = RotateCospos * (maskCoordX_um / 1000) - RotateSinpos * (maskCoordY_um / 1000) + CenterXpos;
	stageY_mm = RotateSinpos * (maskCoordX_um / 1000) + RotateCospos * (maskCoordY_um / 1000) + CenterYpos;
}

void CNavigationStageDlg::StagePeriodicCheck()
{
	if (this == NULL || g_pMaskMap == NULL) return;

	int State, Fault, Value;
	CString str;
	double pos_mm[2];

	//////////////////////////////////////////////////// STAGE POSITION READ ////////////////////////////////////////////////////////////////////////////////////////////////////////
	for (int i = 0; i < STAGE_AXIS_NUMBER; i++) {
		pos_mm[i] = GetPosmm(i);
	}
	//g_pMaskMap->m_dStagePositionX = pos_mm[STAGE_X_AXIS];
	//g_pMaskMap->m_dStagePositionX = pos_mm[STAGE_Y_AXIS];
	//g_pMaskMap->UpdateData(FALSE);

	CString tmp;
	tmp.Format("%3.7f", pos_mm[STAGE_X_AXIS]);
	if (g_pMaskMap->m_EditStagePosXCtrl.m_hWnd != NULL)
		g_pMaskMap->m_EditStagePosXCtrl.SetWindowText(tmp);
	if (m_XPosCtrl.m_hWnd != NULL)
		m_XPosCtrl.SetWindowText(tmp);

	tmp.Format("%3.7f", pos_mm[STAGE_Y_AXIS]);
	if (g_pMaskMap->m_EditStagePosYCtrl.m_hWnd != NULL)
		g_pMaskMap->m_EditStagePosYCtrl.SetWindowText(tmp);
	if (m_YPosCtrl.m_hWnd != NULL)
		m_YPosCtrl.SetWindowText(tmp);

	if (g_pPTR != NULL)
		g_pPTR->PTRPositionUpdate(pos_mm[STAGE_X_AXIS], pos_mm[STAGE_Y_AXIS]);


	double posx_mm = 0.0, posy_mm = 0.0;
	double aa = 0.0, bb = 0.0, cc = 0.0, dd = 0.0;

	switch (g_pConfig->m_nEquipmentType)
	{
	default:
		//Mask LB 좌표계 Display
		if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == SCOPE_OM4X && g_pMaskMap->m_bOMAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
		{
			aa = (g_pMaskMap->m_dOMAlignPointRT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			bb = (g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			cc = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			dd = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			posx_mm = (bb*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000.;
			posy_mm = (aa*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000.;
		}
		else if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV && g_pMaskMap->m_bEUVAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
		{
			aa = (g_pMaskMap->m_dEUVAlignPointRT_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			bb = (g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			cc = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			dd = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			posx_mm = (bb*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000.;
			posy_mm = (aa*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000.;
		}
		else
		{
			if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV)
			{
				posx_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].x - pos_mm[STAGE_X_AXIS];
				posy_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].y - pos_mm[STAGE_Y_AXIS];
			}
			else
			{
				posx_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - pos_mm[STAGE_X_AXIS];
				posy_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - pos_mm[STAGE_Y_AXIS];

			}
		}
		tmp.Format("%3.6f", posx_mm);
		if (g_pMaskMap->m_EditMaskLBCoordXCtrl.m_hWnd != NULL)
			g_pMaskMap->m_EditMaskLBCoordXCtrl.SetWindowText(tmp);
		tmp.Format("%3.6f", posy_mm);
		if (g_pMaskMap->m_EditMaskLBCoordYCtrl.m_hWnd != NULL)
			g_pMaskMap->m_EditMaskLBCoordYCtrl.SetWindowText(tmp);

		//Mask Align Point LB 좌표계 Display
		if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == SCOPE_OM4X && g_pMaskMap->m_bOMAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
		{
			aa = (g_pMaskMap->m_dOMAlignPointRT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			bb = (g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			cc = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			dd = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			posx_mm = (bb*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd);
			posy_mm = (aa*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd);
		}
		else if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV && g_pMaskMap->m_bEUVAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
		{
			aa = (g_pMaskMap->m_dEUVAlignPointRT_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			bb = (g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			cc = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			dd = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			posx_mm = (bb*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd);
			posy_mm = (aa*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd);
		}
		else
		{
			if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV)
			{
				posx_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].x - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000. - pos_mm[STAGE_X_AXIS];
				posy_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000. - pos_mm[STAGE_Y_AXIS];
			}
			else
			{
				posx_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000. - pos_mm[STAGE_X_AXIS];
				posy_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000. - pos_mm[STAGE_Y_AXIS];
			}
		}
		tmp.Format("%3.6f", posx_mm);
		if (g_pMaskMap->m_EditLBAlignPointCoordXCtrl.m_hWnd != NULL)
			g_pMaskMap->m_EditLBAlignPointCoordXCtrl.SetWindowText(tmp);
		tmp.Format("%3.6f", posy_mm);
		if (g_pMaskMap->m_EditLBAlignPointCoordYCtrl.m_hWnd != NULL)
			g_pMaskMap->m_EditLBAlignPointCoordYCtrl.SetWindowText(tmp);

		//Mask Center 좌표계 Display
		if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == SCOPE_OM4X && g_pMaskMap->m_bOMAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
		{
			aa = (g_pMaskMap->m_dOMAlignPointRT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			bb = (g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			cc = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			dd = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			posx_mm = (bb*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000. - MaskSize_mm / 2;
			posy_mm = (aa*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000. - MaskSize_mm / 2;
		}
		else if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV && g_pMaskMap->m_bEUVAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
		{
			aa = (g_pMaskMap->m_dEUVAlignPointRT_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			bb = (g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			cc = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			dd = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			posx_mm = (bb*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000. - MaskSize_mm / 2;
			posy_mm = (aa*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000. - MaskSize_mm / 2;
		}
		else
		{
			if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV)
			{
				posx_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - pos_mm[STAGE_X_AXIS];
				posy_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - pos_mm[STAGE_Y_AXIS];
			}
			else
			{
				posx_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - pos_mm[STAGE_X_AXIS];
				posy_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - pos_mm[STAGE_Y_AXIS];
			}
		}

		tmp.Format("%3.6f", posx_mm);
		if (g_pMaskMap->m_EditMaskCenterCoordXCtrl.m_hWnd != NULL)
			g_pMaskMap->m_EditMaskCenterCoordXCtrl.SetWindowText(tmp);
		tmp.Format("%3.6f", posy_mm);
		if (g_pMaskMap->m_EditMaskCenterCoordYCtrl.m_hWnd != NULL)
			g_pMaskMap->m_EditMaskCenterCoordYCtrl.SetWindowText(tmp);

		break;
	}
	






	// 코드 변경 ihlee
	// PTR 미사용 ? jhkim

	//.IsLaserSwichingModeNew();
	//.IsLaserFeedbackNew();

	

	// 이전코드

	//IsLaserSwichingMode();
	//IsLaserFeedback();

	//if (m_bLaserSwichingModeFlag == TRUE && IsLaserFeedback())
	//{
	//	g_pMaskMap->m_CheckSwitchingOnOffCtrl.SetCheck(TRUE);
	//	g_pMaskMap->m_CheckSwitchingOnOffCtrl.SetWindowText(_T("Laser Feedback"));
	//}
	//else
	//{
	//	g_pMaskMap->m_CheckSwitchingOnOffCtrl.SetCheck(FALSE);
	//	g_pMaskMap->m_CheckSwitchingOnOffCtrl.SetWindowText(_T("Encoder Feedback"));
	//}

	//if (IsLaserSwichingMode())
	//{
	//	g_pMaskMap->m_CheckLaserSwichingCtrl.SetCheck(TRUE);
	//	g_pMaskMap->m_CheckLaserSwichingCtrl.SetWindowText(_T("Feedback Switching Ready"));
	//	//m_bLaserSwichingModeFlag = TRUE; //IsLaserSwichingMode() 에서 업데이트됨
	//}
	//else
	//{
	//	g_pMaskMap->m_CheckLaserSwichingCtrl.SetCheck(FALSE);
	//	g_pMaskMap->m_CheckLaserSwichingCtrl.SetWindowText(_T("Feedback Switching Not Ready"));
	//	//m_bLaserSwichingModeFlag = FALSE; //IsLaserSwichingMode() 에서 업데이트됨
	//}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////// Loading Position Sensor Digital Input Read ////////////////

	if (GetDigitalInput(0, &Value) != XY_NAVISTAGE_OK)
		return;
	if (Value == 1)
		m_chkXLoadingPos.SetCheck(TRUE);
	else
		m_chkXLoadingPos.SetCheck(FALSE);

	if (GetDigitalInput(1, &Value) != XY_NAVISTAGE_OK)
		return;
	if (Value == 1)
	{
		m_chkYLoadingPos.SetCheck(TRUE);
	}
	else
	{
		Sleep(10);
		if (GetDigitalInput(1, &Value) != XY_NAVISTAGE_OK)
			return;
		if (Value == 1)
		{
			m_chkYLoadingPos.SetCheck(TRUE);
		}
		else
		{
			Sleep(10);
			if (GetDigitalInput(1, &Value) != XY_NAVISTAGE_OK)
				return;
			if (Value == 1)
			{
				m_chkYLoadingPos.SetCheck(TRUE);
			}
			else
			{
				m_chkYLoadingPos.SetCheck(FALSE);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////// MOTOR STATUS READ /////////////////////////////////////////
	if (GetMotorStatus(STAGE_X_AXIS, &State) != XY_NAVISTAGE_OK)
		return;

	if (State & ACSC_MST_ENABLE)
	{
		m_chkXEnableCtrl.SetCheck(TRUE);
		m_XEnableCtrl.SetWindowText("X Disable");
	}
	else
	{
		m_chkXEnableCtrl.SetCheck(FALSE);
		m_XEnableCtrl.SetWindowText("X Enable");
	}

	if (State & ACSC_MST_INPOS)
		m_chkXInpositionCtrl.SetCheck(TRUE);
	else
		m_chkXInpositionCtrl.SetCheck(FALSE);

	if (State & ACSC_MST_MOVE)
		m_chkXMovingCtrl.SetCheck(TRUE);
	else
		m_chkXMovingCtrl.SetCheck(FALSE);

	if (GetMotorStatus(STAGE_Y_AXIS, &State) != XY_NAVISTAGE_OK)
		return;

	if (State & ACSC_MST_ENABLE)
	{
		m_chkYEnableCtrl.SetCheck(TRUE);
		m_YEnableCtrl.SetWindowText("Y Disable");
	}
	else
	{
		m_chkYEnableCtrl.SetCheck(FALSE);
		m_YEnableCtrl.SetWindowText("Y Enable");
	}

	if (State & ACSC_MST_INPOS)
		m_chkYInpositionCtrl.SetCheck(TRUE);
	else
		m_chkYInpositionCtrl.SetCheck(FALSE);

	if (State & ACSC_MST_MOVE)
		m_chkYMovingCtrl.SetCheck(TRUE);
	else
		m_chkYMovingCtrl.SetCheck(FALSE);
	///////////////////////////////////////////////////////////////////////////////////////

	//////////////////////////// SAFERY FAULT READ ////////////////////////////////////////
	if (GetFaultStatus(STAGE_X_AXIS, &Fault) != XY_NAVISTAGE_OK)
		return;

	if (Fault & ACSC_SAFETY_RL)
	{
		m_chkXPlusLimitCtrl.SetCheck(TRUE);
	}
	else
	{
		m_chkXPlusLimitCtrl.SetCheck(FALSE);
	}

	if (Fault & ACSC_SAFETY_LL)
		m_chkXMinusLimitCtrl.SetCheck(TRUE);
	else
		m_chkXMinusLimitCtrl.SetCheck(FALSE);

	if (Fault & ACSC_SAFETY_SRL)
		m_chkXSoftPlusLimitCtrl.SetCheck(TRUE);
	else
		m_chkXSoftPlusLimitCtrl.SetCheck(FALSE);

	if (Fault & ACSC_SAFETY_SLL)
		m_chkXSoftMinusLimitCtrl.SetCheck(TRUE);
	else
		m_chkXSoftMinusLimitCtrl.SetCheck(FALSE);

	if (Fault & ACSC_SAFETY_ES)
	{
		m_chkXMotorFaultCtrl.SetCheck(TRUE);
	}
	else
		m_chkXMotorFaultCtrl.SetCheck(FALSE);

	if (Fault & ACSC_SAFETY_HOT)
		m_chkXMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkXMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_ENCNC)
		m_chkXMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkXMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_DRIVE)
		m_chkXMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkXMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_ENC)
		m_chkXMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkXMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_PE)
		m_chkXMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkXMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_CPE)
		m_chkXMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkXMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_VL)
		m_chkXMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkXMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_AL)
		m_chkXMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkXMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_CL)
		m_chkXMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkXMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_SP)
		m_chkXMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkXMotorFaultCtrl.SetCheck(FALSE);

	if (GetFaultStatus(STAGE_Y_AXIS, &Fault) != XY_NAVISTAGE_OK)
		return;

	if (Fault & ACSC_SAFETY_RL)
	{
		m_chkYPlusLimitCtrl.SetCheck(TRUE);
	}
	else
	{
		m_chkYPlusLimitCtrl.SetCheck(FALSE);
	}

	if (Fault & ACSC_SAFETY_LL)
		m_chkYMinusLimitCtrl.SetCheck(TRUE);
	else
		m_chkYMinusLimitCtrl.SetCheck(FALSE);

	if (Fault & ACSC_SAFETY_SRL)
		m_chkYSoftPlusLimitCtrl.SetCheck(TRUE);
	else
		m_chkYSoftPlusLimitCtrl.SetCheck(FALSE);

	if (Fault & ACSC_SAFETY_SLL)
		m_chkYSoftMinusLimitCtrl.SetCheck(TRUE);
	else
		m_chkYSoftMinusLimitCtrl.SetCheck(FALSE);

	if (Fault & ACSC_SAFETY_ES)
	{
		m_chkYMotorFaultCtrl.SetCheck(TRUE);
	}
	else
		m_chkYMotorFaultCtrl.SetCheck(FALSE);

	if (Fault & ACSC_SAFETY_HOT)
		m_chkYMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkYMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_ENCNC)
		m_chkYMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkYMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_DRIVE)
		m_chkYMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkYMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_ENC)
		m_chkYMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkYMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_PE)
		m_chkYMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkYMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_CPE)
		m_chkYMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkYMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_VL)
		m_chkYMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkYMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_AL)
		m_chkYMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkYMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_CL)
		m_chkYMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkYMotorFaultCtrl.SetCheck(FALSE);
	if (Fault & ACSC_SAFETY_SP)
		m_chkYMotorFaultCtrl.SetCheck(TRUE);
	else
		m_chkYMotorFaultCtrl.SetCheck(FALSE);

}


void CNavigationStageDlg::OnStagePos_GridClick(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;

	m_strStagePosition = g_pConfig->m_stStagePos[row - 1].chStagePositionString;
	m_strGetXPos.Format(_T("%3.7f"), g_pConfig->m_stStagePos[row - 1].x);
	m_strGetYPos.Format(_T("%3.7f"), g_pConfig->m_stStagePos[row - 1].y);
	//m_StagePosGrid2.SetEditable(false);
	m_nStagePosNumber = row - 1;
	UpdateData(false);

}

void CNavigationStageDlg::OnStagePos_GridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;

	g_pLog->Display(0, _T("CMaskMapDlg::OnStagePosGridDblClick !"));

	if (g_pNavigationStage->Simulator_Mode)
	{
		MoveStageDBPositionNo(row - 1);
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (g_pConfig->m_nEquipmentType == PHASE)
			MoveStageDBPositionNo(row - 1, FALSE);
		else
			MoveStageDBPositionNo(row - 1);
	}
}

void CNavigationStageDlg::OnStagePos_GridRClick(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;
	//if (col == 1)
	//{
	//	if (row > MAX_STAGE_POSITION - 20)
	//	{
	//		CPasswordDlg pwdlg(this);
	//		pwdlg.DoModal();
	//		if (pwdlg.m_strTxt != "srem")
	//		{
	//			::AfxMessageBox("Password가 일치 하지 않습니다.");
	//			return;
	//		}
	//	}

	//	m_StagePosGrid2.SetEditable(true);
	//}
}

void CNavigationStageDlg::OnStagePos_GridStartEdit(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;
}

void CNavigationStageDlg::OnStagePos_GridEndEdit(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;

	//row = m_StagePosGrid2.GetSelectedRow();
	sprintf(g_pConfig->m_stStagePos[row - 1].chStagePositionString, "%s", m_StagePosGrid2.GetItemText(row, col));
	m_StagePosGrid2.SetEditable(false);
}

void CNavigationStageDlg::OnBnClickedCheckStagemoveAtZorigin()
{
	g_pLog->Display(0, _T("CNavigationStageDlg::OnBnClickedCheckStagemoveAtZorigin() Check Button Click!"));

	switch (m_CheckStageMoveAtZOCtrl.GetCheck())
	{
	case BST_CHECKED:
		g_pConfig->m_bNaviStageMoveAtZorigin = TRUE;
		break;
	case BST_UNCHECKED:
		g_pConfig->m_bNaviStageMoveAtZorigin = FALSE;
		break;
	default:
		break;
	}
}

int CNavigationStageDlg::Move_Loading_Position()
{
	//if(g_pScanStage->m_bZHoldOn==TRUE)
		//return 0;

	int ret = 0;
	ret = g_pAP->Check_STAGE_MovePossible();
	if (ret != XY_NAVISTAGE_OK)
		return ret;

	if (MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_LOADING_POSITION].x, g_pConfig->m_stStagePos[SYSTEM_LOADING_POSITION].y) != XY_NAVISTAGE_OK)
		return -1;	//추후 Error Code 정리 필요

	return 0;
}


// bZReturnAfterMove FALSE면, Z_INITIAL_POS_UM 로 이동 후 복귀안함
int CNavigationStageDlg::MoveAbsolutePosition(double dXmm, double dYmm, BOOL bZReturnAfterMove)
{
	//if (g_pScanStage->m_bZHoldOn == TRUE)
	//	return 0;

	if (Simulator_Mode)
	{
		if (MoveAbsoluteXY_UntilInposition(dXmm, dYmm) != XY_NAVISTAGE_OK)
			return !XY_NAVISTAGE_OK;
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
		{
			m_stStage.strStatus = "Stage 사용 금지 모드입니다!";
			return !XY_NAVISTAGE_OK;
		}
		if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage Moving Impossible!");
			return !XY_NAVISTAGE_OK;
		}

		if (g_pConfig->m_nEquipmentType == EUVPTR)
		{
			if (MoveAbsoluteXY_UntilInposition(dXmm, dYmm) != XY_NAVISTAGE_OK)
				return !XY_NAVISTAGE_OK;
		}
		else
		{
			//asdf
			//if ((int)(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= 0 && g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == SCOPE_OM4X)
			//if ((round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= Z_INITIAL_POS_UM) && g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == SCOPE_OM4X) //ihlee 변경				
			//{
				if (MoveAbsoluteXY_OnTheFly(dXmm, dYmm) != XY_NAVISTAGE_OK)
					return !XY_NAVISTAGE_OK;
			//}
			//else
			//{
				//TRACE("m_dPIStage_GetPos: %.3f\n", g_pScanStage->m_dPIStage_GetPos[Z_AXIS]);
				//g_pNavigationStage->m_GetZOldPos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
				//if(g_pAdam->m_nEuvImage_Fov != 1000)	//추후 Fast mode 만들어서 조건문을 바꾸자
				if (bZReturnAfterMove)
				{
					if (g_pConfig->m_nEquipmentType == PHASE)
					{
						//g_pScanStage->MoveZRelative_SlowInterlock(g_pConfig->m_dDownZHeightWhenMove_um, MINUS_DIRECTION);
					}
					else
					{
						//g_pScanStage->MoveZRelative_SlowInterlock(50, MINUS_DIRECTION);	//50um내려서 이동
					}

				}
				else
				{
					//g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
				}

				//WaitSec(0.5);
				if (MoveAbsoluteXY_UntilInposition(dXmm, dYmm) != XY_NAVISTAGE_OK)
					return !XY_NAVISTAGE_OK;

				if (bZReturnAfterMove)
				{
					if (g_pConfig->m_nEquipmentType == PHASE)
					{
						//g_pScanStage->MoveZRelative_SlowInterlock(g_pConfig->m_dDownZHeightWhenMove_um, PLUS_DIRECTION);
					}
					else
					{
						//g_pScanStage->MoveZRelative_SlowInterlock(50, PLUS_DIRECTION);	//50um 다시 올림
					}
				}
				else
				{
					// Z축 동작 안함..
				}
				TRACE("m_GetZOldPos: %.3f\n", g_pNavigationStage->m_GetZOldPos);
			//}
		}
	}
	return XY_NAVISTAGE_OK;
}

int CNavigationStageDlg::MoveRelativePosition(int nAxis, double dPosmm)
{
	//if (g_pScanStage->m_bZHoldOn == TRUE)
	//	return 0;

	if (Simulator_Mode)
	{
		if (MoveRelativeOnTheFly(nAxis, dPosmm) != XY_NAVISTAGE_OK)
			return !XY_NAVISTAGE_OK;
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
		{
			m_stStage.strStatus = "Stage 사용 금지 모드입니다!";
			return !XY_NAVISTAGE_OK;
		}
		if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage Moving Impossible!");
			return !XY_NAVISTAGE_OK;
		}


		if (g_pConfig->m_nEquipmentType == EUVPTR)
		{
			if (MoveRelativeOnTheFly(nAxis, dPosmm) != XY_NAVISTAGE_OK)
				return !XY_NAVISTAGE_OK;
		}
		else
		{
			//asdf
			//if ((int)(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= 0)
			//if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= Z_INITIAL_POS_UM)//ihlee 변경
			//{
			//	if (MoveRelativeOnTheFly(nAxis, dPosmm) != XY_NAVISTAGE_OK)
			//		return !XY_NAVISTAGE_OK;
			//}
			//else
			//{
			//	//TRACE("m_dPIStage_GetPos: %.3f\n", g_pScanStage->m_dPIStage_GetPos[Z_AXIS]);
			//	//g_pNavigationStage->m_GetZOldPos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
			//	if (g_pConfig->m_nEquipmentType == PHASE)
			//	{
			//		g_pScanStage->MoveZRelative_SlowInterlock(g_pConfig->m_dDownZHeightWhenMove_um, MINUS_DIRECTION);
			//	}
			//	else
			//	{
			//		g_pScanStage->MoveZRelative_SlowInterlock(50, MINUS_DIRECTION);	//50um내려서 이동
			//	}
			//
			//	WaitSec(0.1);
			//	if (MoveRelativeOnTheFly(nAxis, dPosmm) != XY_NAVISTAGE_OK)
			//		return !XY_NAVISTAGE_OK;
			//	WaitSec(1);
			//
			//	if (g_pConfig->m_nEquipmentType == PHASE)
			//	{
			//		g_pScanStage->MoveZRelative_SlowInterlock(g_pConfig->m_dDownZHeightWhenMove_um, PLUS_DIRECTION);
			//	}
			//	else
			//	{
			//		g_pScanStage->MoveZRelative_SlowInterlock(50, PLUS_DIRECTION);	//50um 복귀
			//	}
			//
			//	TRACE("m_GetZOldPos: %.3f\n", g_pNavigationStage->m_GetZOldPos);
			//}
		}
	}
	return XY_NAVISTAGE_OK;
}

int CNavigationStageDlg::MoveStageDBPositionNo(int nMesureNo, BOOL bZReturnAfterMove)
{
	//if (g_pScanStage->m_bZHoldOn == TRUE)
	//	return 0;

	if (Simulator_Mode)
	{
		if (MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[nMesureNo].x, g_pConfig->m_stStagePos[nMesureNo].y) != XY_NAVISTAGE_OK)
			return !XY_NAVISTAGE_OK;
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
		{
			m_stStage.strStatus = "Stage 사용 금지 모드입니다!";
			return !XY_NAVISTAGE_OK;
		}
		if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage Moving Impossible!");
			return !XY_NAVISTAGE_OK;
		}

		if (g_pConfig->m_nEquipmentType == EUVPTR)
		{
			if (MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[nMesureNo].x, g_pConfig->m_stStagePos[nMesureNo].y) != XY_NAVISTAGE_OK)
				return !XY_NAVISTAGE_OK;
		}
		else
		{
			//asdf
			//if ((int)(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= 0)
			//if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= Z_INITIAL_POS_UM) //ihlee
			//{
			//	if (MoveAbsoluteXY_OnTheFly(g_pConfig->m_stStagePos[nMesureNo].x, g_pConfig->m_stStagePos[nMesureNo].y) != XY_NAVISTAGE_OK)
			//		return !XY_NAVISTAGE_OK;
			//}
			//else
			//{
			//	//TRACE("m_dPIStage_GetPos: %.3f\n", g_pScanStage->m_dPIStage_GetPos[Z_AXIS]);
			//	//g_pNavigationStage->m_GetZOldPos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
			//	
			//	if (bZReturnAfterMove)
			//	{
			//		if (g_pConfig->m_nEquipmentType == PHASE)
			//		{
			//			g_pScanStage->MoveZRelative_SlowInterlock(g_pConfig->m_dDownZHeightWhenMove_um, MINUS_DIRECTION);	//50um내려서 이동
			//		}
			//		else
			//		{
			//			g_pScanStage->MoveZRelative_SlowInterlock(50, MINUS_DIRECTION);	//50um내려서 이동
			//		}
			//	}
			//	else
			//	{
			//		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
			//	}
			//
				WaitSec(0.1);
				
				if (MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[nMesureNo].x, g_pConfig->m_stStagePos[nMesureNo].y) != XY_NAVISTAGE_OK)
					return !XY_NAVISTAGE_OK;
				
				if (bZReturnAfterMove)
				{
					if (g_pConfig->m_nEquipmentType == PHASE)
					{
						//g_pScanStage->MoveZRelative_SlowInterlock(g_pConfig->m_dDownZHeightWhenMove_um, PLUS_DIRECTION);
					}
					else
					{
						//g_pScanStage->MoveZRelative_SlowInterlock(50, PLUS_DIRECTION);	//50um내려서 이동
					}
				}
				else
				{
					//Z축 복귀 안함
				}
				TRACE("m_GetZOldPos: %.3f\n", g_pNavigationStage->m_GetZOldPos);
			//}
		}

		if (nMesureNo == SYSTEM_LOADING_POSITION)
		{
			m_stStage.strStatus = "Stage Loading Position 입니다.";
			g_pMaskMap->m_strStageStatus.Format(_T("%s"), m_stStage.strStatus);
			g_pMaskMap->UpdateData(FALSE);
			//g_pScanStage->Move_Origin();
		}
	}
	return XY_NAVISTAGE_OK;
}


// 파악 필요. bWithoutZdown True면 Z축 안내리고 바로 이동 Phase만  사용, MoveToSelectedPointNo 에서만 사용(레시피 돌릴떄 사용)
// PTR에서는 기본적으로 z축 이동없음
int CNavigationStageDlg::MoveToMaskCenterCoordinate(double dMaskX_um, double dMaskY_um, BOOL bWithoutZdown)
{
	CAutoMessageDlg MsgBoxAuto(g_pNavigationStage);
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
	{
		m_stStage.strStatus = "Stage 사용 금지 모드입니다!";
		return !XY_NAVISTAGE_OK;
	}

	if (g_pMaskMap == NULL || g_pConfig == NULL)
		return !XY_NAVISTAGE_OK;

	double target_stageposx_mm = 0.0, target_stageposy_mm = 0.0;
	switch (g_pConfig->m_nEquipmentType)
	{
	case EUVPTR:
		ConvertToStageFromMask(target_stageposx_mm, target_stageposy_mm, dMaskX_um, dMaskY_um);
		MoveAbsoluteXY_UntilInposition(target_stageposx_mm, target_stageposy_mm); //z축 이동없음
		break;
	default:
		MoveAbsolutePosition(target_stageposx_mm, target_stageposy_mm);
		break;
	}

	return XY_NAVISTAGE_OK;
}


int CNavigationStageDlg::MoveToMaskCoordinateWithoutInpositionForPTR(double dMaskX_um, double dMaskY_um, double *target_xpos_out_mm, double *target_ypos_out_mm)
{
	//if (g_pScanStage->m_bZHoldOn == TRUE)
	//	return 0;

	CAutoMessageDlg MsgBoxAuto(g_pNavigationStage);
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
	{
		m_stStage.strStatus = "Stage 사용 금지 모드입니다!";
		return !XY_NAVISTAGE_OK;
	}

	if (g_pMaskMap == NULL || g_pConfig == NULL)
		return !XY_NAVISTAGE_OK;


	double d1stAlignLaserX_mm = 0.0, d1stAlignLaserY_mm = 0.0;
	double d2ndAlignLaserX_mm = 0.0, d2ndAlignLaserY_mm = 0.0;
	double dRefAlignLaserX_mm = 0.0, dRefAlignLaserY_mm = 0.0;
	double maskposx_mm = 0.0, maskposy_mm = 0.0;
	double compensate_maskposx_um = g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stOriginCoordinate_um.X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X + dMaskX_um;
	double compensate_maskposy_um = g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stOriginCoordinate_um.Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y + dMaskY_um;
	double target_stageposx_mm = 0.0, target_stageposy_mm = 0.0;
	if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == SCOPE_OM4X && g_pMaskMap->m_bOMAlignComplete == TRUE)
	{
		maskposx_mm = (compensate_maskposx_um*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - (compensate_maskposx_um
				*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm
				- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y
					+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
						*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X)) + ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm
							- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X
								+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
									*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X));
		d1stAlignLaserX_mm = g_pMaskMap->m_dOMAlignPointLT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm;
		d2ndAlignLaserX_mm = g_pMaskMap->m_dOMAlignPointRT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm;
		dRefAlignLaserX_mm = g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm;
		maskposx_mm = (d2ndAlignLaserX_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.))*(compensate_maskposx_um / 1000.) - (dRefAlignLaserX_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.))*(compensate_maskposy_um / 1000.) + dRefAlignLaserX_mm;
		maskposy_mm = (compensate_maskposx_um*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - (compensate_maskposx_um
				*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm
				- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y
					+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
						*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X)) + ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm
							- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X
								+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
									*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X));
		d1stAlignLaserY_mm = g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLT_posy_mm;
		d2ndAlignLaserY_mm = g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointRT_posy_mm;
		dRefAlignLaserY_mm = g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm;
		maskposy_mm = (d2ndAlignLaserY_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.))*(compensate_maskposx_um / 1000.) - (dRefAlignLaserY_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.))*(compensate_maskposy_um / 1000.) + dRefAlignLaserY_mm;

		target_stageposx_mm = g_pMaskMap->m_dOMAlignPointLT_posx_mm + maskposx_mm;
		target_stageposy_mm = g_pMaskMap->m_dOMAlignPointLT_posy_mm - maskposy_mm;
	}
	else if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV && g_pMaskMap->m_bEUVAlignComplete == TRUE)
	{
		maskposx_mm = (compensate_maskposx_um*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - (compensate_maskposx_um
				*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm
				- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y
					+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
						*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X)) + ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm
							- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X
								+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserX_mm + d1stAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
									*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X));
		d1stAlignLaserX_mm = g_pMaskMap->m_dEUVAlignPointLT_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm;
		d2ndAlignLaserX_mm = g_pMaskMap->m_dEUVAlignPointRT_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm;
		dRefAlignLaserX_mm = g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm;
		maskposx_mm = (d2ndAlignLaserX_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.))*(compensate_maskposx_um / 1000.) - (dRefAlignLaserX_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.))*(compensate_maskposy_um / 1000.) + dRefAlignLaserX_mm;
		maskposy_mm = (compensate_maskposx_um*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - (compensate_maskposx_um
				*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm))
			/ (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X) - ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm
				- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y
					+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
						*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X)) + ((dRefAlignLaserX_mm*d1stAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - dRefAlignLaserX_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserY_mm
							- dRefAlignLaserY_mm * d1stAlignLaserX_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*d2ndAlignLaserX_mm + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserX_mm*d2ndAlignLaserY_mm - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*d1stAlignLaserY_mm*d2ndAlignLaserX_mm)*(dRefAlignLaserY_mm*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d1stAlignLaserY_mm - dRefAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X
								+ g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*d2ndAlignLaserY_mm + d1stAlignLaserY_mm * g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*d2ndAlignLaserY_mm)) / ((dRefAlignLaserX_mm*d1stAlignLaserY_mm - dRefAlignLaserY_mm * d1stAlignLaserX_mm - dRefAlignLaserX_mm * d2ndAlignLaserY_mm + dRefAlignLaserY_mm * d2ndAlignLaserX_mm + d1stAlignLaserX_mm * d2ndAlignLaserY_mm - d1stAlignLaserY_mm * d2ndAlignLaserX_mm)
									*(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y*g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X));
		d1stAlignLaserY_mm = g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLT_posy_mm;
		d2ndAlignLaserY_mm = g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointRT_posy_mm;
		dRefAlignLaserY_mm = g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm;
		maskposy_mm = (d2ndAlignLaserY_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.))*(compensate_maskposx_um / 1000.) - (dRefAlignLaserY_mm / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.))*(compensate_maskposy_um / 1000.) + dRefAlignLaserY_mm;
		double dDifference = 0.0;


		target_stageposx_mm = g_pMaskMap->m_dEUVAlignPointLT_posx_mm + maskposx_mm;
		target_stageposy_mm = g_pMaskMap->m_dEUVAlignPointLT_posy_mm - maskposy_mm;

		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
		{
			//g_pAdam->GetCurrentLIFPosValue();
			//g_pNavigationStage->m_bLaserValueAfterSwitchingX = g_pAdam->m_dCurrentXposition;
			//g_pNavigationStage->m_bLaserValueAfterSwitchingY = g_pAdam->m_dCurrentYposition;
			g_pNavigationStage->SetLaserMode();
			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
			WaitSec(1);
		}
	}
	else
	{
		//1.Reference 좌표계를 마스크좌표계로 변경(mm 단위)
		maskposx_mm = (compensate_maskposx_um + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.;
		maskposy_mm = (compensate_maskposy_um + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.;

		//2.마스크좌표계를 스테이지 좌표계로 변경(mm 단위)
		if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV)
		{
			target_stageposx_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].x - maskposx_mm;
			target_stageposy_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].y - maskposy_mm;
		}
		else
		{
			target_stageposx_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - maskposx_mm;
			target_stageposy_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - maskposy_mm;
		}
	}


	MoveAbsoluteXY_OnTheFly(target_stageposx_mm, target_stageposy_mm); //z축 이동없음

	*target_xpos_out_mm = target_stageposx_mm;
	*target_ypos_out_mm = target_stageposy_mm;


	return XY_NAVISTAGE_OK;
}


// 파악 필요. phase에서는 1 포인트 이동시 z축 다시 올리지 않음
// 파악 필요. phase에서는 2 포인트, 이동시  z축 이동 없이 이동 (레시피 돌릴때만 적용)
int CNavigationStageDlg::MoveToSelectedPointNo(int nSelectedID, int nPointType, BOOL bWithoutZdown)
{
	//if (g_pScanStage->m_bZHoldOn == TRUE)
	//	return 0;

	CAutoMessageDlg MsgBoxAuto(g_pNavigationStage);
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
	{
		m_stStage.strStatus = "Stage 사용 금지 모드입니다!";
		return !XY_NAVISTAGE_OK;
	}

	if (g_pMaskMap == NULL || g_pConfig == NULL)
		return !XY_NAVISTAGE_OK;

	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.TotalMeasureNum == 0)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return !XY_NAVISTAGE_OK;
	}

	//물류 및 MASK여부 확인하고 아래 수행
	if (nPointType == FIRSRT_POINT)
	{
		if (g_pConfig->m_nEquipmentType == EUVPTR)
		{
			//MoveToMaskCenterCoordinate(g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[nSelectedID].ReferenceCoodX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[nSelectedID].ReferenceCoodY_um , TRUE); //+ 방향
			//MoveToMaskCenterCoordinate(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[nSelectedID].dReferenceCoodX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[nSelectedID].dReferenceCoodY_um, TRUE); //+ 방향
		}
	}
	else if (nPointType == SECOND_POINT)
	{
		//?????
		//MoveToMaskCenterCoordinate(g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[nSelectedID].dRightX_um + g_pConfig->m_dInspectorOffsetX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[nSelectedID].dRightY_um + g_pConfig->m_dInspectorOffsetY_um); //+ 방향
	}


	return XY_NAVISTAGE_OK;
}

int CNavigationStageDlg::MoveToSelectedPointNo(double dMaskPixelPosX, double dMaskPixelPosY)
{
	//if (g_pScanStage->m_bZHoldOn == TRUE)
	//	return 0;

	CAutoMessageDlg MsgBoxAuto(g_pNavigationStage);
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
	{
		m_stStage.strStatus = "Stage 사용 금지 모드입니다!";
		return !XY_NAVISTAGE_OK;
	}

	if (g_pMaskMap == NULL || g_pConfig == NULL)
		return !XY_NAVISTAGE_OK;

	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.TotalMeasureNum == 0)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return !XY_NAVISTAGE_OK;
	}

	//물류 및 MASK여부 확인하고 아래 수행
	//if (nPointType == FIRSRT_POINT)
	//{
	//	if (g_pConfig->m_nEquipmentType == EUVPTR)
		{
			MoveToMaskCenterCoordinate(dMaskPixelPosX, dMaskPixelPosY, TRUE); //+ 방향
		}
	//}
	//else if (nPointType == SECOND_POINT)
	//{
	//	//?????
	//	//MoveToMaskCenterCoordinate(g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[nSelectedID].dRightX_um + g_pConfig->m_dInspectorOffsetX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[nSelectedID].dRightY_um + g_pConfig->m_dInspectorOffsetY_um); //+ 방향
	//}


	return XY_NAVISTAGE_OK;
}

void CNavigationStageDlg::OnBnClickedButtonFaultclear()
{
	g_pLog->Display(0, _T("CNavigationStageDlg::OnBnClickedButtonFaultclear() Button Click!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	ClearAmpFault(STAGE_ALL_AXIS);
}

//int CNavigationStageDlg::SetFeedbackTypeDlg(int nFeedbackType)
//{
//	int nRet = -1;
//
//	nRet = SetFeedbackType(nFeedbackType);
//
//	if (nRet == XY_NAVISTAGE_OK)
//	{
//		if (g_pConfig != NULL)
//		{
//			if (nFeedbackType == FEEDBACK_LASER)
//			{
//				g_pConfig->m_bLaserFeedback_Flag = TRUE;
//			}
//			else if (nFeedbackType == FEEDBACK_ENCODER)
//			{				
//				g_pConfig->m_bLaserFeedback_Flag = FALSE;
//			}			
//				g_pConfig->SaveRecoveryData();
//		}		
//	}
//
//	return nRet;
//}

//int CNavigationStageDlg::SetFeedbackTypeDlg(int nFeedbackType)
//{
//	if (nFeedbackType == FEEDBACK_LASER)
//	{
//		if (StopBuffer(FEECBACK_SWITCHING_OFF_BUUFER) != XY_NAVISTAGE_OK)
//			return !XY_NAVISTAGE_OK;
//		if (RunBuffer(FEECBACK_SWITCHING_ON_BUUFER) != XY_NAVISTAGE_OK)
//			return !XY_NAVISTAGE_OK;
//		if (g_pConfig != NULL)
//			g_pConfig->m_bLaserFeedback_Flag = TRUE;
//	}
//	else if (nFeedbackType == FEEDBACK_ENCODER)
//	{
//		if (StopBuffer(FEECBACK_SWITCHING_ON_BUUFER) != XY_NAVISTAGE_OK)
//			return !XY_NAVISTAGE_OK;
//		if (RunBuffer(FEECBACK_SWITCHING_OFF_BUUFER) != XY_NAVISTAGE_OK)
//			return !XY_NAVISTAGE_OK;
//		if (g_pConfig != NULL)
//			g_pConfig->m_bLaserFeedback_Flag = FALSE;
//	}
//
//	if (g_pConfig != NULL)
//		g_pConfig->SaveRecoveryData();
//
//	return XY_NAVISTAGE_OK;
//}


int CNavigationStageDlg::Simulator_Start()
{
	int nRet = -1;

	if (Is_NAVI_Stage_Connected())
	{
		DisconnectComm();
	}

	nRet = ConnectACSController(SIMULATOR, g_pConfig->m_chIP[ETHERNET_NAVI_STAGE], g_pConfig->m_nPORT[ETHERNET_NAVI_STAGE]);
	if (nRet == FALSE)
	{
		g_pCommStat->NavigationStageCommStatus(FALSE);
		Simulator_Mode = FALSE;
		return Simulator_Mode;
	}
	else if (nRet == TRUE)
	{
		g_pCommStat->NavigationStageCommStatus(TRUE);
		Simulator_Mode = TRUE;
		return Simulator_Mode;
	}
}



void CNavigationStageDlg::OnBnClickedButtonStageSimulatorMode()
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
	{
		if (!Simulator_Start())
		{
			::AfxMessageBox(" Stage Simulator Mode Impossible!");
			g_pLog->Display(0, _T("Stage Simulator Mode Impossible!"));
			GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_X_HOME)->EnableWindow(false);
			GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_Y_HOME)->EnableWindow(false);

			Simulator_Mode = FALSE;
		}
		else
		{
			GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_MODE)->EnableWindow(false);
			GetDlgItem(IDC_BUTTON_STAGE_NORMAL_MODE)->EnableWindow(true);
			GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_X_HOME)->EnableWindow(true);
			GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_Y_HOME)->EnableWindow(true);

			if (!StageSimulatorSetup())
			{
				::AfxMessageBox(" Stage Simulator Mode Impossible (Stage Setup Fail) !", MB_ICONERROR);
				g_pLog->Display(0, _T(" Stage Simulator Mode Impossible (Stage Setup Fail) !"));
				GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_X_HOME)->EnableWindow(false);
				GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_Y_HOME)->EnableWindow(false);

				Simulator_Mode = FALSE;
			}

			if (!Set_RPosition(STAGE_X_AXIS, 0.0))
			{
				::AfxMessageBox(" Stage Simulator Mode Impossible (Stage X Axis Home Fail) !", MB_ICONERROR);
				g_pLog->Display(0, _T(" Stage Simulator Mode Impossible (Stage X Axis Home Fail) !"));
				GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_X_HOME)->EnableWindow(false);
				GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_Y_HOME)->EnableWindow(false);

				Simulator_Mode = FALSE;
			}

			if (!Set_RPosition(STAGE_Y_AXIS, 0.0))
			{
				::AfxMessageBox(" Stage Simulator Mode Impossible (Stage Y Axis Home Fail) !", MB_ICONERROR);
				g_pLog->Display(0, _T(" Stage Simulator Mode Impossible (Stage Y Axis Home Fail) !"));
				GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_X_HOME)->EnableWindow(false);
				GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_Y_HOME)->EnableWindow(false);

				Simulator_Mode = FALSE;
			}

			g_pLog->Display(0, _T(" Stage Simulator Mode Start !"));
			g_pWarning->m_strWarningMessageVal = "Stage Simulator Mode Start !";
			g_pWarning->UpdateData(FALSE);
			g_pWarning->ShowWindow(SW_SHOW);
			WaitSec(3);
			g_pWarning->ShowWindow(SW_HIDE);

		}
	}
	else
	{
		::AfxMessageBox(" Stage Simulator Mode Impossible (OFFLINE MODE 가 아닙니다) !", MB_ICONERROR);
		g_pLog->Display(0, _T(" Stage Simulator Mode Impossible  (OFFLINE MODE 가 아닙니다) !"));
	}
}


void CNavigationStageDlg::OnBnClickedButtonStageNormalMode()
{
	int nRet = -1;

	if (Simulator_Mode)
	{
		Set_RPosition(STAGE_X_AXIS, 0.0);
		Set_RPosition(STAGE_Y_AXIS, 0.0);

		g_pLog->Display(0, _T(" Stage Simulator Mode End !"));
		Simulator_Mode = FALSE;
		g_pWarning->m_strWarningMessageVal = "Stage Simulator Mode End !";
		g_pWarning->UpdateData(FALSE);
		g_pWarning->ShowWindow(SW_SHOW);
		DisconnectComm();
		WaitSec(3);
		g_pWarning->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_X_HOME)->EnableWindow(false);
		GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_Y_HOME)->EnableWindow(false);
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			nRet = g_pNavigationStage->ConnectACSController(ETHERNET, g_pConfig->m_chIP[ETHERNET_NAVI_STAGE], g_pConfig->m_nPORT[ETHERNET_NAVI_STAGE]);
			if (nRet == FALSE)
			{
				g_pCommStat->NavigationStageCommStatus(FALSE);
				WaitSec(3);
			}
			else
			{

				g_pCommStat->NavigationStageCommStatus(TRUE);
				//g_pNavigationStage->SetLaserSwitchingFunction(FALSE);
			}
		}
	}
	GetDlgItem(IDC_BUTTON_STAGE_NORMAL_MODE)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_STAGE_SIMULATOR_MODE)->EnableWindow(true);
}


void CNavigationStageDlg::OnBnClickedButtonStageSimulatorXHome()
{
	if (Simulator_Mode)
	{
		if (!Set_RPosition(STAGE_X_AXIS, 0.0))
		{
			::AfxMessageBox(" Stage Simulator Mode Impossible (Stage X Axis Home Fail) !", MB_ICONERROR);
		}
	}
}


void CNavigationStageDlg::OnBnClickedButtonStageSimulatorYHome()
{
	if (Simulator_Mode)
	{
		if (!Set_RPosition(STAGE_Y_AXIS, 0.0))
		{
			::AfxMessageBox(" Stage Simulator Mode Impossible (Stage Y Axis Home Fail) !", MB_ICONERROR);
		}
	}
}

int CNavigationStageDlg::StageSimulatorSetup()
{
	if (!Set_Acsc_0_Axis_Brushless_Motor_Off())
	{
		::AfxMessageBox(" Stage Simulator 0 Axis Brushlee Motor off Impossible!", MB_ICONERROR);
		Simulator_Mode = FALSE;
		return !XY_NAVISTAGE_OK;
	}

	if (!Set_Acsc_1_Axis_Brushless_Motor_Off())
	{
		::AfxMessageBox(" Stage Simulator 0 Axis Brushlee Motor off Impossible!", MB_ICONERROR);
		Simulator_Mode = FALSE;
		return !XY_NAVISTAGE_OK;
	}

	if (!Set_Acsc_0_Axis_HL_Off())
	{
		::AfxMessageBox(" Stage Simulator 0 Axis Hardware Limit off Impossible!", MB_ICONERROR);
		Simulator_Mode = FALSE;
		return !XY_NAVISTAGE_OK;
	}

	if (!Set_Acsc_1_Axis_HL_Off())
	{
		::AfxMessageBox(" Stage Simulator 1 Axis Hardware Limit off Impossible!", MB_ICONERROR);
		Simulator_Mode = FALSE;
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;
}



void CNavigationStageDlg::EquipmentMode_Check()
{
	if (g_pConfig->m_nEquipmentMode != OFFLINE)
	{
		((CStatic*)GetDlgItem(IDC_STATIC_EQUIPMENTMODE))->SetWindowTextA(_T("Online"));
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_STATIC_EQUIPMENTMODE))->SetWindowTextA(_T("Offline"));
	}

	if (g_pConfig->m_nEquipmentType == SREM033)
	{
		((CStatic*)GetDlgItem(IDC_STATIC_EQUIPMENTTYPE))->SetWindowTextA(_T("SREM033"));
	}
	else if (g_pConfig->m_nEquipmentType == PHASE)
	{
		((CStatic*)GetDlgItem(IDC_STATIC_EQUIPMENTTYPE))->SetWindowTextA(_T("PHASE"));
	}
	else if (g_pConfig->m_nEquipmentType == EUVPTR)
	{
		((CStatic*)GetDlgItem(IDC_STATIC_EQUIPMENTTYPE))->SetWindowTextA(_T("EUVPTR"));
	}
}



void CNavigationStageDlg::OnBnClickedButtonConnect()
{
	if (Is_NAVI_Stage_Connected())
	{
		if (m_hComm != ACSC_INVALID)
			DisconnectComm();
	}

	if (!Is_NAVI_Stage_Connected())
	{
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			if (!g_pNavigationStage->ConnectACSController(ETHERNET, g_pConfig->m_chIP[ETHERNET_NAVI_STAGE], g_pConfig->m_nPORT[ETHERNET_NAVI_STAGE]))
			{
				g_pCommStat->NavigationStageCommStatus(FALSE);
				AfxMessageBox(_T("Navigation Stage Connection Fail!"), MB_ICONERROR);
				WaitSec(3);
			}
			else
			{
				AfxMessageBox(_T("Navigation Stage Connection Success!"), MB_ICONINFORMATION);
			}
		}
	}
}


void CNavigationStageDlg::OnBnClickedButtonDisconnect()
{
	if (Is_NAVI_Stage_Connected())
	{
		DisconnectComm();
	}
}

void CNavigationStageDlg::Navi_Stage_Connected_Check()
{
	if (Get_Connection_info())
	{
		if (Is_NAVI_Stage_Connected())
		{
			if(g_pMaskMap != NULL)
				g_pMaskMap->m_Icon_NaviStageConnectState.SetIcon(m_LedIcon[0]);

			((CStatic*)GetDlgItem(IDC_ICON_NAVI_STAGE_CHECK))->SetIcon(m_LedIcon[0]);
			GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(false);
		}
		else
		{
			if (g_pMaskMap != NULL)
				g_pMaskMap->m_Icon_NaviStageConnectState.SetIcon(m_LedIcon[1]);

			((CStatic*)GetDlgItem(IDC_ICON_NAVI_STAGE_CHECK))->SetIcon(m_LedIcon[1]);
			GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(true);
		}
	}
	else
	{
		if (g_pMaskMap != NULL)
			g_pMaskMap->m_Icon_NaviStageConnectState.SetIcon(m_LedIcon[1]);

		((CStatic*)GetDlgItem(IDC_ICON_NAVI_STAGE_CHECK))->SetIcon(m_LedIcon[1]);
		GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(true);
	}
}

int CNavigationStageDlg::Start_Scan(double FovX_um, double FovY_um, double StepDistance_um, int nRepeatNo)
{
	int ret = 0;
	CString strLog;

	//g_pMaskMap->m_strStageStatus.Format(_T("Scan Start!"));
	//g_pMaskMap->UpdateData(FALSE);

	////1. 셔터 오픈
	//strLog.Format("Begin(Shutter_Open): Point:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum);
	//SaveLogFile("ProcessLogScanStage", strLog);

	//if (g_pEUVSource != NULL && g_pEUVSource->Is_SRC_Connected() == TRUE)
	//	g_pEUVSource->SetMechShutterOpen(TRUE);

	//strLog.Format("End(Shutter_Open): Point:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum);
	//SaveLogFile("ProcessLogScanStage", strLog);

	//2. 스캐닝
	strLog.Format("Begin(Scanning_Motion): Point:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum);
	SaveLogFile("ProcessLogScanStage", strLog);
	

	//Scan parameter 계산
	double stepVel_mm, scanVel_mm, stepDistance_mm, FovY_mm;
	int stepNo;
	double dt = 0.0002; //5khz laser
	 
	stepDistance_mm = StepDistance_um / 1000.0;
	FovY_mm = FovY_um / 1000.0;

	scanVel_mm = StepDistance_um / dt/1000.0;	
	stepNo = ceil(FovX_um / StepDistance_um) + 1;
	stepVel_mm = scanVel_mm;


	//Scan parameter set
	SetGlobalRealVariable("Scan_Vel", scanVel_mm);
	SetGlobalRealVariable("Step_Vel", stepVel_mm);
	SetGlobalRealVariable("Step_No", stepNo);
	SetGlobalRealVariable("Fov_mm", FovY_mm);
	SetGlobalRealVariable("Step_distance", stepDistance_mm);
	SetGlobalIntVariable("Scan_Repeat_No", nRepeatNo);
	/////////////////////////////////////////////////////////////////////



	RunBuffer(SCAN_MODE_BUFFER);
	MSG msg;
	while (IsBufferRun(SCAN_MODE_BUFFER))
	{
		if (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		Sleep(1);
	}
	WaitSec(3);

	//if (WaitTimeUntilBufferStop(SCANNING_TIMEOUT_SEC, SCAN_MODE_BUFFER))
	//{
	//	ret = -1;
	//	strLog.Format("Fail(Scanning_Motion): Point:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum);
	//	SaveLogFile("ProcessLogScanStage", strLog);
	//}
	//else
	//{
	//	strLog.Format("End(Scanning_Motion): Point:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum);
	//	SaveLogFile("ProcessLogScanStage", strLog);
	//}

	////3. 셔터 클로즈
	//strLog.Format("Begin(Shutter_Close): Point:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum);
	//SaveLogFile("ProcessLogScanStage", strLog);
	//if (g_pEUVSource != NULL && g_pEUVSource->Is_SRC_Connected() == TRUE)
	//	g_pEUVSource->SetMechShutterOpen(FALSE);
	//strLog.Format("End(Shutter_Close): Point:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum);
	//SaveLogFile("ProcessLogScanStage", strLog);


	strLog.Format("End(Start_Scan): Point:%d", g_pMaskMap->m_MaskMapWnd.m_ProcessData.CurrentMeasureNum);
	SaveLogFile("ProcessLogScanStage", strLog);

	//g_pMaskMap->m_strStageStatus.Format(_T("Scan End!"));
	//g_pMaskMap->UpdateData(FALSE);

	return ret;
}

int CNavigationStageDlg::Stop_Scan()
{
	int ret = 0;

	g_pNavigationStage->StopBuffer(SCAN_MODE_BUFFER);

	return ret;
}

void CNavigationStageDlg::EmergencyStop()
{
	int nRet = 0;
	CString str;

	nRet = EStop(STAGE_ALL_AXIS);
	str.Format(_T("CNavigationStageDlg::EmergencyStop(), EStop(STAGE_ALL_AXIS) Return %d"), nRet);
	g_pLog->Display(0, str);
}