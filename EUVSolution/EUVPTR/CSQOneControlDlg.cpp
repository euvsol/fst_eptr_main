﻿// CSQOneControlDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include "CSQOneControlDlg.h"
#define PI 3.14159265

// CSQOneControlDlg 대화 상자

IMPLEMENT_DYNAMIC(CSQOneControlDlg, CDialogEx)

CSQOneControlDlg::CSQOneControlDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SQONEMAIN_DIALOG, pParent)
{
	m_pThread = NULL;

	m_isThreadExitFlag = FALSE;
}

CSQOneControlDlg::~CSQOneControlDlg()
{


}

void CSQOneControlDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSQOneControlDlg, CDialogEx)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_SQONEMAIN_BUTTON_SHOWMANUALDLG, &CSQOneControlDlg::OnBnClickedButtonSqoneShowmanualdlg)
	ON_BN_CLICKED(IDC_SQONEMAIN_BUTTON_SHOWCONFIGDLG, &CSQOneControlDlg::OnBnClickedButtonSqoneShowconfigdlg)
	ON_BN_CLICKED(IDC_SQONEMAIN_BUTTON_SHOWDBDLG, &CSQOneControlDlg::OnBnClickedButtonSqoneShowpositiondata)
	ON_BN_CLICKED(IDC_SQONEMAIN_BUTTON_CONNECT, &CSQOneControlDlg::OnBnClickedButtonSqoneConnect)
	ON_BN_CLICKED(IDC_SQONEMAIN_BUTTON_DISCONNECT, &CSQOneControlDlg::OnBnClickedButtonSqoneDisconnect)
	ON_BN_CLICKED(IDC_SQONEMAIN_BUTTON_STOP, &CSQOneControlDlg::OnBnClickedButtonSqoneStop)
END_MESSAGE_MAP()


// CSQOneControlDlg 메시지 처리기



BOOL CSQOneControlDlg::OnInitDialog()
{
	__super::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);

	m_SqoneStage = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_SQONE), IMAGE_BITMAP, 300, 300, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_COMMUNICATION))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_EXCEEDSYSTEMRANGE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_MOVECOMPLETE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_SYSTEMSTATUS))->SetIcon(m_LedIcon[0]);

	((CStatic*)GetDlgItem(IDC_SQONEMAIN_BITMAP_AXIS))->SetBitmap(m_SqoneStage);
	SetTimer(0, 100, NULL);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CSQOneControlDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	KillTimer(0);
	switch (nIDEvent)
	{
	case 0:

		updateSQOneDlg();
		SetTimer(0, 100, NULL);
		break;
	default:
		break;
	}
	__super::OnTimer(nIDEvent);
}


void CSQOneControlDlg::OnDestroy()
{
	__super::OnDestroy();
	m_isThreadExitFlag = TRUE;
	KillTimer(0);
	if (m_pThread != NULL)
	{
		HANDLE threadHandle = m_pThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pThread = NULL;
	}
	CloseTcpIpSocket();
}


void CSQOneControlDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 __super::OnPaint()을(를) 호출하지 마십시오.
}

void CSQOneControlDlg::connectSQone()
{
	int nRet = 0;
	if (g_pSqOneControl->m_bConnected == FALSE)
	{
		nRet = g_pSqOneControl->OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_BEAM], 50122, false, 1024);
		Display(0, "Open TCP Socket");
		if (nRet == 0)
		{
			g_pSqOneControl->m_isThreadExitFlag = FALSE;
			Display(0, "Open TCP Socket Sucess");
			g_pSqOneControl->m_pThread = AfxBeginThread(g_pSqOneControl->updataUIThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
		}
		else
		{
			::AfxMessageBox("Open TCP Socket Fail");
			Display(0, "Open TCP Socket Fail");
		}
	}
	else
	{
		Display(0, "Communication Alread Connected");
	}
}

void CSQOneControlDlg::disconnectSQone()
{
	if (g_pSqOneControl->m_bConnected)
	{
		m_isThreadExitFlag = TRUE;
		Sleep(1000);
		if (m_pThread != NULL)
		{
			int nRet = WaitForSingleObject(m_pThread->m_hThread, 3000);
			if (nRet != WAIT_OBJECT_0)
			{
 				//AfxMessageBox("Thread Close Fail");
				// add terminate 
			}
			else
			{
				
			}
			m_pThread = NULL;
		}
		g_pSqOneControl->CloseTcpIpSocket();
	}
	else
	{
		Display(0, "Communication Already Closed");
	}
}

void CSQOneControlDlg::OnBnClickedButtonSqoneShowmanualdlg()
{
	g_pSqOneManual->ShowWindow(SW_SHOW);
	g_pSqOneDB->ShowWindow(SW_HIDE);
	g_pSqOneConfig->ShowWindow(SW_HIDE);
	Display(0, "Manual Dialog Open Button Click");
}


void CSQOneControlDlg::OnBnClickedButtonSqoneShowconfigdlg()
{
	g_pSqOneManual->ShowWindow(SW_HIDE);
	g_pSqOneDB->ShowWindow(SW_HIDE);
	g_pSqOneConfig->ShowWindow(SW_SHOW);
	Display(0, "Config Dialog Open Button Click");
}

void CSQOneControlDlg::OnBnClickedButtonSqoneShowpositiondata()
{
	g_pSqOneManual->ShowWindow(SW_HIDE);
	g_pSqOneDB->ShowWindow(SW_SHOW);
	g_pSqOneConfig->ShowWindow(SW_HIDE);
	Display(0, "SQ One DB Dialog Open Button Click");
}

void CSQOneControlDlg::waitSleep(int msec)
{
		int nRet = 0;

		DWORD flagWait = -1;
		LARGE_INTEGER ferquency = { 0, };
		LARGE_INTEGER start = { 0, };
		LARGE_INTEGER end = { 0, };
		double duration = 0;

		QueryPerformanceFrequency(&ferquency);
		QueryPerformanceCounter(&start);
		duration = 0;

		MSG msg;
		while (duration < msec)
		{
			Sleep(1);
			if (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			QueryPerformanceCounter(&end);
			duration = ((end.QuadPart - start.QuadPart) / (double)ferquency.QuadPart) * 1000; //ms second
		}
}

void CSQOneControlDlg::OnBnClickedButtonSqoneConnect()
{
	Display(0, "Communication Connect Button Clicked");
	if (m_bConnected == FALSE)
	{
		connectSQone();
	}
}


void CSQOneControlDlg::OnBnClickedButtonSqoneDisconnect()
{
	Display(0, "Close Communication Button Clicked");
	if (m_bConnected == TRUE)
	{
		disconnectSQone();
	}
}


void CSQOneControlDlg::OnBnClickedButtonSqoneStop()
{
	Display(0, "Stop Button Clicked");

	g_pSqOneControl->m_bStopFlag = TRUE;
	if (m_bConnected == TRUE)
	{
		g_pSqOneControl->sendStopCommand();
	}
}

int CSQOneControlDlg::checkLimit(double dHorizontal, double dVertical, double dBeam, double dPitch, double dYaw, double dRoll)
{
	int nRet = 0;
	if (dHorizontal <Xleftlimit || dHorizontal >Xrightlimit)
	{
		nRet = -1;
		::AfxMessageBox("X parameter limit error");
	}
	else if (dVertical <Yleftlimit || dVertical >Yrightlimit)
	{
		nRet = -1;
		::AfxMessageBox("Y parameter limit error");
	}
	else if (dBeam <Zleftlimit || dBeam >Zrightlimit)
	{
		nRet = -1;
		::AfxMessageBox("Z parameter limit error");
	}
	else if (dPitch <Rxleftlimit || dPitch >Rxrightlimit)
	{
		nRet = -1;
		::AfxMessageBox("RX parameter limit error");
	}
	else if (dYaw <Ryleftlimit || dYaw >Ryrightlimit)
	{
		nRet = -1;
		::AfxMessageBox("RY parameter limit error");
	}
	else if (dRoll <Rzleftlimit || dRoll >Rzrightlimit)
	{
		nRet = -1;
		::AfxMessageBox("RZ parameter limit error");
	}
	else
	{
		nRet = 0;
	}
	return nRet;
}

void CSQOneControlDlg::updateSQOneDlg()
{
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_COMMUNICATION))->SetIcon(m_LedIcon[1]);
		if (g_pSqOneControl->m_SQOneParseData.TargetExceedRanged == TRUE)
		{
			((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_EXCEEDSYSTEMRANGE))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_EXCEEDSYSTEMRANGE))->SetIcon(m_LedIcon[0]);
		}
		if (g_pSqOneControl->m_SQOneParseData.MoveComplete == TRUE)
		{
			((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_MOVECOMPLETE))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_MOVECOMPLETE))->SetIcon(m_LedIcon[0]);
		}
		if (g_pSqOneControl->m_SQOneParseData.TrisphereStatus == TRUE)
		{
			((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_SYSTEMSTATUS))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_SYSTEMSTATUS))->SetIcon(m_LedIcon[1]);
		}
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_COMMUNICATION))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_EXCEEDSYSTEMRANGE))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_MOVECOMPLETE))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_SQONEMAIN_ICON_SYSTEMSTATUS))->SetIcon(m_LedIcon[0]);
		m_isThreadExitFlag = TRUE;
	}
}

BOOL CSQOneControlDlg::Display(int nModule, CString strLogMsg, BOOL bDisplay, BOOL bSaveFile)
{
	CString strOuptLog;
	CListBox* pBox = (CListBox*)GetDlgItem(IDC_SQONEMAIN_LIST_LOG);
	if (pBox == NULL)
	{
		return FALSE;
	}
	int numItem = pBox->GetCount();
	if (numItem >= 1000)
	{
		pBox->ResetContent();
	}

	if (bDisplay)
	{
		CString strDate, strTime;
		GetSystemDateTime(&strDate, &strTime);

		CString str;
		str.Format("%s %s\r\n", strTime, strLogMsg);

		pBox->SetCurSel(pBox->AddString(str));
		pBox->GetHorizontalExtent();
		pBox->SetHorizontalExtent(256);
	}

	if (bSaveFile)
	{
		SaveLogFile("SQOneControlLog", (LPSTR)(LPCTSTR)strLogMsg);
	}

	return TRUE;
}

UINT CSQOneControlDlg::updataUIThread(LPVOID lParam)
{
	UINT ret = 0;
	while (g_pSqOneControl->m_isThreadExitFlag == FALSE)
	{
		g_pSqOneControl->sendGetCommand();
		g_pSqOneControl->waitSleep(400);
	}
	return ret;
}


BOOL CSQOneControlDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}
	return __super::PreTranslateMessage(pMsg);
}


BOOL CSQOneControlDlg::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return __super::DestroyWindow();
}

int CSQOneControlDlg::moveSQ1(double dHorizontal, double dVertical, double dBeam, double dPitch, double dYaw, double dRoll)
{
	int nRet = 0;
	g_pSqOneControl->Display(0, "Macro Fuction Run");
	g_pSqOneControl->m_bStopFlag = FALSE;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		nRet = g_pSqOneControl->checkLimit(dHorizontal, dVertical, dBeam, dPitch, dYaw, dRoll);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), dHorizontal, dVertical, dBeam, dPitch, dYaw, dRoll, m_SQOneParseData.Cx, m_SQOneParseData.Cy, m_SQOneParseData.Cz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(dHorizontal, dVertical, dBeam, dPitch, dYaw, dRoll, m_SQOneParseData.Cx, m_SQOneParseData.Cy, m_SQOneParseData.Cz);
			if (nRet == -2)
			{
				//::AfxMessageBox("Sq-1 Stage Stoped");
				return nRet;
			}
			else if (nRet != 0)
			{
				//::AfxMessageBox("Sq-1 Timeout Error");
				return nRet;
			}
			else
			{
				return nRet;
			}
		}
		else
		{
			nRet = -4;
			return nRet;
		}
	}
	else
	{
		nRet = -99;
		g_pSqOneControl->Display(0, "Communication Error");
		return nRet;
	}
}

