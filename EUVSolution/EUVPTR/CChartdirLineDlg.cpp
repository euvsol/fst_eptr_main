﻿// CChartdirLineDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CChartdirLineDlg 대화 상자

IMPLEMENT_DYNAMIC(CChartdirLineDlg, CDialogEx)

CChartdirLineDlg::CChartdirLineDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_LINE_CHART_DIALOG, pParent)
{

}

CChartdirLineDlg::~CChartdirLineDlg()
{
	if (Multiline_chart != NULL)
	{
		delete Multiline_chart;
		Multiline_chart = NULL;
	}
}

void CChartdirLineDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHART_VIEW_LINE, m_ChartViewer);
}


BEGIN_MESSAGE_MAP(CChartdirLineDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_LINE, &CChartdirLineDlg::OnBnClickedCheckLine)
END_MESSAGE_MAP()


// CChartdirLineDlg 메시지 처리기


BOOL CChartdirLineDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	font.CreateFont(23, 13, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("HY헤드라인M"));
	GetDlgItem(IDC_STATIC_SOURCE)->SetFont(&font);

	drawChart(&m_ChartViewer);
	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CChartdirLineDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


void CChartdirLineDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


void CChartdirLineDlg::drawChart(CChartViewer *viewer)
{
	// In this example, we simply use random data for the 3 data series.
	RanSeries *r = new RanSeries(129);
	//DoubleArray data0 = r->getSeries(100, 250, -15, 15);
	//DoubleArray data1 = r->getSeries(100, 160, -15, 15);
	//DoubleArray data2 = r->getSeries(100, 220, -15, 15);
	//DoubleArray timeStamps = r->getDateSeries(1000, Chart::chartTime(2014, 1, 1), 86400);

	// Create a XYChart object of size 600 x 400 pixels
	//Multiline_chart = new XYChart(600, 400);
	Multiline_chart = new XYChart(720,600);

	// Add a title box using grey (0x555555) 20pt Arial font
	//Multiline_chart->addTitle("Source Contamination Measurement", "arial.ttf", 20, 0x555555);
	Multiline_chart->addTitle("Source Contamination Measurement   ", "arial.ttf", 20);

	// Set the plotarea at (70, 70) and of size 500 x 300 pixels, with transparent background and
	// border and light grey (0xcccccc) horizontal grid lines
	Multiline_chart->setPlotArea(70, 120, 550, 400, Chart::Transparent, -1, Chart::Transparent, 0xcccccc);
	Multiline_chart->setBackground(LIGHT_GRAY, Transparent, 0);


	// Add a legend box with horizontal layout above the plot area at (70, 35). Use 12pt Arial font,
	// transparent background and border, and line style legend icon.
	LegendBox *title = Multiline_chart->addLegend(180, 50, false, "arial.ttf", 12);
	title->setBackground(Chart::Transparent, Chart::Transparent);
	title->setLineStyleKey();

	// Set axis label font to 12pt Arial
	Multiline_chart->xAxis()->setLabelStyle("arial.ttf", 12);
	Multiline_chart->yAxis()->setLabelStyle("arial.ttf", 12);

	// Set the x and y axis stems to transparent, and the x-axis tick color to grey (0xaaaaaa)
	Multiline_chart->xAxis()->setColors(Chart::Transparent, Chart::TextColor, Chart::TextColor, 0xaaaaaa);
	Multiline_chart->yAxis()->setColors(Chart::Transparent);

	// Set the major/minor tick lengths for the x-axis to 10 and 0.
	Multiline_chart->xAxis()->setTickLength(10, 0);

	// For the automatic axis labels, set the minimum spacing to 80/40 pixels for the x/y axis.
	Multiline_chart->xAxis()->setTickDensity(80);
	Multiline_chart->yAxis()->setTickDensity(10);

	// Add a title to the y axis using dark grey (0x555555) 14pt Arial font
	Multiline_chart->yAxis()->setTitle("Source Power (v)", "arial.ttf", 14, 0x555555);
	Multiline_chart->xAxis()->setTitle("Time (s)", "arial.ttf", 14, 0x555555);

	// Add a line layer to the chart with 3-pixel line width
	LineLayer *layer = Multiline_chart->addLineLayer();
	layer->setLineWidth(1);

	// Add 3 data series to the line layer
	double data_segment[] = { 0.725,	0.725,	0.727,	0.73,	0.731,	0.732,	0.731,	0.731,	0.728,	0.728,	0.73,	0.731,	0.73,	0.727,	0.726,	0.727,	0.723,	0.72,	0.721,	0.721,	0.719,	0.718,	0.714,	0.712,	0.713,	0.714,	0.715,	0.711,	0.707,	0.707,	0.701,	0.702,	0.704,	0.704,	0.705,	0.705,	0.704,	0.703,	0.698,	0.696,	0.696,	0.692,	0.694,	0.695,	0.695,	0.696,	0.695,	0.69,	0.688,	0.689,	0.688,	0.688,	0.687,	0.685,	0.686,	0.686,	0.684,	0.686,	0.685,	0.683,	0.682,	0.679,	0.677,	0.676,	0.673,	0.673,	0.675,	0.676,	0.675,	0.676,	0.676,	0.676,	0.673,	0.672,	0.672,	0.668,	0.668,	0.669,	0.665,	0.661,	0.657,	0.658,	0.662,	0.663,	0.659,	0.657,	0.657,	0.656,	0.656,	0.652,	0.648,	0.646,	0.651,	0.657,	0.653,	0.651,	0.652,	0.651,	0.648,	0.65,	0.652,	0.65,	0.649,	0.648,	0.647,	0.644,	0.641,	0.645,	0.649,	0.644,	0.641,	0.638,	0.636,	0.638,	0.637,	0.635,	0.632,	0.631,	0.632,	0.632,	0.632,	0.633,	0.633,	0.632,	0.63,	0.628,	0.626,	0.625,	0.626,	0.623,	0.622,	0.624,	0.623,	0.623,	0.623,	0.624,	0.626,	0.628,	0.628,	0.627,	0.628,	0.626,	0.621,	0.622,	0.621,	0.619,	0.617,	0.618,	0.62,	0.619,	0.618,	0.616,	0.614,	0.618,	0.619,	0.616,	0.614,	0.614,	0.609,	0.607,	0.606,	0.604,	0.604,	0.603,	0.602,	0.603,	0.604,	0.602,	0.599,	0.599,	0.597,	0.595,	0.594,	0.593,	0.595,	0.597,	0.597,	0.598,	0.596,	0.593,	0.595,	0.592,	0.592,	0.591,	0.592,	0.594,	0.592,	0.59,	0.587,	0.587,	0.586,	0.585,	0.587,	0.587,	0.584,	0.585,	0.584,	0.583,	0.582,	0.582,	0.582,	0.578,	0.575,	0.577,	0.577,	0.578,	0.581,	0.579,	0.574,	0.572,	0.571,	0.57,	0.566,	0.567,	0.572,	0.574,	0.57,	0.568,	0.569,	0.567,	0.566,	0.567,	0.566,	0.562,	0.562,	0.562,	0.559,	0.555,	0.554,	0.555,	0.553,	0.554,	0.552,	0.549,	0.548,	0.549,	0.551,	0.549,	0.55,	0.553,	0.553,	0.551,	0.551,	0.548,	0.546,	0.548,	0.549,	0.549,	0.548,	0.543,	0.544,	0.543,	0.543,	0.544,	0.544,	0.543,	0.543,	0.541,	0.539,	0.538,	0.538,	0.538,	0.533,	0.533,	0.534,	0.532,	0.533,	0.534,	0.531,	0.528,	0.529,	0.528,	0.527,	0.526,	0.525,	0.524,	0.524,	0.527,	0.527,	0.525,	0.523,	0.523,	0.523,	0.522,	0.523,	0.524,	0.522,	0.521,	0.52,	0.521,	0.522,	0.522,	0.52,	0.52,	0.521,	0.518,	0.517,	0.517,	0.517,	0.516,	0.515,	0.514,	0.514,	0.515,	0.514,	0.512,	0.514,	0.514,	0.511,	0.509,	0.507,	0.505,	0.506,	0.505,	0.503,	0.504,	0.504,	0.504,	0.503,	0.505,	0.509,	0.51,	0.511,	0.512,	0.511,	0.509,	0.506,	0.506,	0.506,	0.506,	0.505,	0.504,	0.503,	0.501,	0.496,	0.493,	0.491,	0.487,	0.484,	0.486,	0.491,	0.493,	0.495,	0.498,	0.499,	0.499,	0.495,	0.494,	0.494,	0.491,	0.491,	0.49,	0.492,	0.491,	0.49,	0.492,	0.489,	0.486,	0.488,	0.488,	0.484,	0.486,	0.488,	0.487,	0.485,	0.483,	0.483,	0.48,	0.481,	0.485,	0.483,	0.483,	0.482,	0.481,	0.482,	0.48,	0.478,	0.474,	0.472,	0.472,	0.472,	0.474,	0.475,	0.475,	0.471,	0.469,	0.47,	0.468,	0.467,	0.47,	0.469,	0.468,	0.471,	0.47,	0.467,	0.467,	0.467,	0.466,	0.467,	0.472,	0.474,	0.471,	0.47,	0.469,	0.47,	0.469,	0.464,	0.463,	0.463,	0.462,	0.46,	0.459,	0.458,	0.454,	0.454,	0.455,	0.458,	0.458,	0.456,	0.458,	0.458,	0.456,	0.455,	0.457,	0.456,	0.457,	0.452,	0.447,	0.447,	0.445,	0.443,	0.442,	0.441,	0.438,	0.442,	0.444,	0.441,	0.443,	0.444,	0.441,	0.441,	0.44,	0.442,	0.442,	0.44,	0.439,	0.438,	0.44,	0.444,	0.446,	0.444,	0.443,	0.44,	0.439,	0.44,	0.437,	0.435,	0.438,	0.44,	0.444,	0.441,	0.437,	0.438,	0.437,	0.434,	0.433,	0.431,	0.431,	0.432,	0.43,	0.426,	0.427,	0.428,	0.424,	0.423,	0.423,	0.422,	0.419,	0.419,	0.419,	0.418,	0.416,	0.414,	0.414,	0.416,	0.416,	0.416,	0.418,	0.418,	0.416,	0.413,	0.416,	0.421,	0.421,	0.421,	0.418,	0.418,	0.417,	0.414,	0.413,	0.411,	0.41,	0.412,	0.413,	0.41,	0.41,	0.411,	0.415,	0.415,	0.412,	0.412,	0.408,	0.408,	0.413,	0.414,	0.415,	0.416,	0.417,	0.417,	0.416,	0.416,	0.416,	0.414,	0.411,	0.41,	0.411,	0.412,	0.411,	0.413,	0.41,	0.407,	0.41,	0.411,	0.409,	0.409,	0.41,	0.407,	0.406,	0.407,	0.406,	0.407,	0.406,	0.403,	0.403,	0.4,	0.402,	0.401,	0.402,	0.403,	0.403,	0.405,	0.402,	0.401,	0.404,	0.404,	0.403,	0.402,	0.399,	0.398,	0.401,	0.398,	0.397,	0.401,	0.401,	0.401,	0.404,	0.403,	0.401,	0.397,	0.397,	0.398,	0.396,	0.398,	0.396,	0.394,	0.392,	0.39,	0.39,	0.391,	0.393,	0.392,	0.39,	0.39,	0.388,	0.387,	0.388,	0.389,	0.388,	0.389,	0.392,	0.392,	0.389,	0.387,	0.386,	0.383,	0.379,	0.377,	0.376,	0.377,	0.375,	0.376,	0.379,	0.381,	0.382,	0.382,	0.381,	0.38,	0.379,	0.379,	0.378,	0.377,	0.379,	0.378,	0.378,	0.379,	0.383,	0.382,	0.376,	0.373,	0.373,	0.374,	0.372,	0.369,	0.37,	0.372,	0.371,	0.368,	0.37,	0.371,	0.369,	0.366,	0.365,	0.365,	0.363,	0.363,	0.367,	0.367,	0.363,	0.363,	0.363,	0.361,	0.362
	};
	
	double data_center[] = { 0.383,	0.383,	0.386,	0.388,	0.388,	0.39,	0.389,	0.386,	0.385,	0.385,	0.385,	0.384,	0.384,	0.384,	0.383,	0.382,	0.382,	0.381,	0.381,	0.38,	0.378,	0.378,	0.378,	0.38,	0.382,	0.383,	0.384,	0.385,	0.384,	0.382,	0.378,	0.376,	0.377,	0.382,	0.383,	0.384,	0.386,	0.385,	0.381,	0.379,	0.376,	0.374,	0.375,	0.378,	0.379,	0.38,	0.379,	0.378,	0.377,	0.377,	0.378,	0.379,	0.377,	0.373,	0.373,	0.374,	0.375,	0.378,	0.378,	0.375,	0.374,	0.371,	0.374,	0.378,	0.378,	0.377,	0.38,	0.383,	0.384,	0.383,	0.383,	0.384,	0.385,	0.386,	0.387,	0.384,	0.382,	0.382,	0.381,	0.376,	0.376,	0.38,	0.384,	0.385,	0.383,	0.381,	0.382,	0.384,	0.381,	0.375,	0.37,	0.368,	0.371,	0.374,	0.374,	0.373,	0.374,	0.373,	0.372,	0.373,	0.374,	0.377,	0.379,	0.379,	0.378,	0.372,	0.37,	0.377,	0.382,	0.382,	0.381,	0.379,	0.379,	0.38,	0.382,	0.383,	0.382,	0.381,	0.381,	0.381,	0.382,	0.38,	0.38,	0.381,	0.381,	0.38,	0.381,	0.381,	0.379,	0.38,	0.38,	0.379,	0.382,	0.383,	0.381,	0.381,	0.382,	0.383,	0.385,	0.382,	0.381,	0.38,	0.379,	0.382,	0.382,	0.38,	0.378,	0.379,	0.38,	0.378,	0.378,	0.379,	0.381,	0.381,	0.379,	0.38,	0.382,	0.382,	0.38,	0.378,	0.379,	0.38,	0.379,	0.378,	0.378,	0.379,	0.381,	0.38,	0.379,	0.378,	0.377,	0.378,	0.379,	0.379,	0.377,	0.375,	0.376,	0.379,	0.379,	0.377,	0.374,	0.374,	0.375,	0.375,	0.377,	0.379,	0.381,	0.381,	0.379,	0.376,	0.373,	0.371,	0.372,	0.373,	0.374,	0.375,	0.373,	0.372,	0.374,	0.376,	0.376,	0.374,	0.373,	0.373,	0.374,	0.376,	0.377,	0.378,	0.378,	0.379,	0.376,	0.373,	0.372,	0.372,	0.374,	0.379,	0.379,	0.378,	0.379,	0.378,	0.38,	0.381,	0.383,	0.382,	0.382,	0.381,	0.381,	0.38,	0.38,	0.381,	0.38,	0.379,	0.379,	0.38,	0.379,	0.377,	0.376,	0.377,	0.378,	0.38,	0.381,	0.379,	0.379,	0.378,	0.377,	0.377,	0.378,	0.378,	0.378,	0.377,	0.377,	0.375,	0.374,	0.377,	0.376,	0.375,	0.377,	0.379,	0.38,	0.381,	0.379,	0.378,	0.379,	0.377,	0.376,	0.378,	0.38,	0.379,	0.375,	0.372,	0.374,	0.377,	0.375,	0.373,	0.374,	0.373,	0.374,	0.375,	0.376,	0.376,	0.375,	0.375,	0.374,	0.373,	0.376,	0.379,	0.38,	0.38,	0.376,	0.374,	0.376,	0.378,	0.38,	0.38,	0.381,	0.38,	0.378,	0.376,	0.377,	0.376,	0.376,	0.376,	0.373,	0.374,	0.374,	0.373,	0.373,	0.373,	0.372,	0.374,	0.377,	0.378,	0.378,	0.376,	0.376,	0.376,	0.375,	0.376,	0.376,	0.376,	0.378,	0.381,	0.38,	0.379,	0.379,	0.379,	0.382,	0.382,	0.379,	0.377,	0.376,	0.378,	0.378,	0.378,	0.378,	0.377,	0.375,	0.372,	0.369,	0.37,	0.372,	0.375,	0.377,	0.378,	0.38,	0.382,	0.382,	0.382,	0.379,	0.376,	0.376,	0.376,	0.375,	0.374,	0.372,	0.37,	0.368,	0.369,	0.37,	0.369,	0.371,	0.375,	0.375,	0.372,	0.371,	0.371,	0.372,	0.372,	0.372,	0.375,	0.376,	0.376,	0.373,	0.371,	0.371,	0.372,	0.371,	0.37,	0.371,	0.372,	0.373,	0.376,	0.376,	0.376,	0.377,	0.38,	0.379,	0.375,	0.376,	0.377,	0.376,	0.376,	0.376,	0.375,	0.376,	0.377,	0.377,	0.375,	0.375,	0.378,	0.379,	0.378,	0.379,	0.379,	0.377,	0.375,	0.376,	0.375,	0.373,	0.372,	0.372,	0.374,	0.373,	0.37,	0.368,	0.37,	0.372,	0.371,	0.37,	0.37,	0.37,	0.37,	0.37,	0.373,	0.375,	0.377,	0.377,	0.374,	0.373,	0.374,	0.374,	0.375,	0.376,	0.376,	0.376,	0.374,	0.372,	0.374,	0.376,	0.376,	0.377,	0.375,	0.374,	0.374,	0.374,	0.376,	0.377,	0.377,	0.377,	0.376,	0.377,	0.377,	0.375,	0.375,	0.376,	0.375,	0.376,	0.379,	0.38,	0.379,	0.38,	0.379,	0.376,	0.374,	0.373,	0.373,	0.373,	0.374,	0.375,	0.373,	0.373,	0.374,	0.376,	0.376,	0.375,	0.375,	0.374,	0.373,	0.372,	0.373,	0.371,	0.368,	0.369,	0.371,	0.371,	0.371,	0.372,	0.372,	0.371,	0.37,	0.37,	0.373,	0.374,	0.375,	0.374,	0.373,	0.373,	0.373,	0.373,	0.374,	0.376,	0.377,	0.377,	0.378,	0.376,	0.375,	0.377,	0.377,	0.376,	0.376,	0.377,	0.376,	0.371,	0.37,	0.37,	0.37,	0.373,	0.373,	0.373,	0.372,	0.372,	0.374,	0.375,	0.372,	0.369,	0.367,	0.368,	0.37,	0.371,	0.371,	0.37,	0.37,	0.37,	0.37,	0.372,	0.373,	0.373,	0.373,	0.371,	0.367,	0.365,	0.368,	0.371,	0.371,	0.37,	0.371,	0.374,	0.374,	0.37,	0.37,	0.371,	0.371,	0.372,	0.373,	0.373,	0.372,	0.371,	0.373,	0.373,	0.373,	0.372,	0.372,	0.375,	0.375,	0.374,	0.375,	0.375,	0.373,	0.372,	0.371,	0.371,	0.372,	0.374,	0.374,	0.372,	0.371,	0.372,	0.371,	0.37,	0.372,	0.372,	0.373,	0.373,	0.372,	0.371,	0.372,	0.371,	0.368,	0.368,	0.371,	0.372,	0.373,	0.371,	0.37,	0.37,	0.372,	0.373,	0.374,	0.375,	0.376,	0.373,	0.371,	0.372,	0.373,	0.376,	0.378,	0.376,	0.376,	0.378,	0.379,	0.379,	0.376,	0.372,	0.368,	0.369,	0.373,	0.378,	0.377,	0.375,	0.375,	0.376,	0.377,	0.375,	0.374,	0.373,	0.375,	0.373,	0.372,	0.373,	0.373,	0.372,	0.37,	0.367,	0.365,	0.366,	0.368,	0.37,	0.37,	0.369,	0.369,	0.369,	0.368
	};
	
	const int Segment_Data_Length = (int)(sizeof(data_segment) / sizeof(*data_segment));
	const int Center_Data_Length = (int)(sizeof(data_center) / sizeof(*data_center));

	DoubleArray timeStamps = r->getDateSeries(Segment_Data_Length,0,60,false);
	
	layer->addDataSet(Segment_Data_Length, data_segment, RED, "Segment");
	layer->addDataSet(Center_Data_Length, data_center, PURPLE, "Center");

	//layer->addDataSet(data1, 0xee9944, "Center");
	//layer->addDataSet(data2, 0x99bb55, "Gamma");

	// The x-coordinates for the line layer
	layer->setXData(timeStamps);



	// KJH 테스트 중
	// Output the chart
	//Multiline_chart->makeChart("multiline2.png");

	delete viewer->getChart();
	viewer->setChart(Multiline_chart);

	//free up resources
	delete r;
	//return 0;
}

void CChartdirLineDlg::OnBnClickedCheckLine()
{
	drawChart(&m_ChartViewer);
}
