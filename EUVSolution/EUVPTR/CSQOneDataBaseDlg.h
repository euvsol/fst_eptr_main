﻿#pragma once


// CSQOneDataBaseDlg 대화 상자

class CSQOneDataBaseDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSQOneDataBaseDlg)

public:
	CSQOneDataBaseDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CSQOneDataBaseDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SQONEDB_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	typedef struct _Pos {
		double Horizontal;
		double Vertical;
		double Beam;
		double Pitch;
		double Yaw;
		double Roll;
		char PosName[128];
		char Date[128];
	};
	_Pos m_stStagePos_SQONE[MAX_STAGE_POSITION];

	virtual BOOL DestroyWindow();
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	CGridCtrl m_SQONEGrid;

	void initializePositionDB();

	void loadSQONEStagePosition();
	void saveSQONEStagePosition();
	void addSQONEStagePosition();
	void deleteSQONEStagePosition();

	afx_msg void OnBnClickedSqonedbButtonAddposition();
	afx_msg void OnBnClickedSqonedbButtonLoadposition();
	afx_msg void OnBnClickedSqonedbButtonDeleteposition();
	afx_msg void OnHdnItemclickSqonedbList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickSqonedbList(NMHDR *pNMHDR, LRESULT *pResult);

	int m_nListIndex;
	BOOL m_isLoaded;

	afx_msg void OnNMDblclkSqonedbList(NMHDR *pNMHDR, LRESULT *pResult);
};
