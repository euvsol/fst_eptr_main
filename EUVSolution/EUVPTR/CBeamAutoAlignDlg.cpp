﻿// CSqOneAutoAlignDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CSqOneAutoAlignDlg 대화 상자

IMPLEMENT_DYNAMIC(CBeamAutoAlignDlg, CDialogEx)

CBeamAutoAlignDlg::CBeamAutoAlignDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_BEAMAUTOALIGN_DIALOG, pParent)
{
	m_isAutoAlignOn = FALSE;
	m_isIntensityOn = FALSE;
	m_bisCorseScan = FALSE;
	m_bisFineScan = FALSE;
	m_bisCirculScan = FALSE;

	m_nAutoAlignStatus = 0;
}

CBeamAutoAlignDlg::~CBeamAutoAlignDlg()
{
	delete m_XYchartViewer.getChart();
	delete m_ContourChartViewer.getChart();
}

void CBeamAutoAlignDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BEAMALIGN_BITMAP_XYCHART, m_XYchartViewer);
	DDX_Control(pDX, IDC_BEAMALIGN_BITMAP_PITCHTYAWCHART, m_ContourChartViewer);
}


BEGIN_MESSAGE_MAP(CBeamAutoAlignDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_CONTROL(CVN_ViewPortChanged, IDC_BEAMALIGN_BITMAP_XYCHART, OnViewPortChangedXYChart)
	ON_CONTROL(CVN_ViewPortChanged, IDC_BEAMALIGN_BITMAP_PITCHTYAWCHART, OnViewPortChangedPITCHTYAWChart)
	ON_BN_CLICKED(IDC_BEAMALIGN_BUTTON_RUN, &CBeamAutoAlignDlg::OnBnClickedButtonAutoAlignRun)
END_MESSAGE_MAP()


// CSqOneAutoAlignDlg 메시지 처리기


BOOL CBeamAutoAlignDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	initialChart();

	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_STARTPOSITION, _T("-2"));
	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_ITERATION, "9");
	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_INCREMENT, "0.5");
	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_ENDPOSITION, "2");
	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_STARTPOSITION, "-2");
	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_ITERATION, "9");
	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_INCREMENT, "0.5");
	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_ENDPOSITION, "2");
	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_DSIPDETINTENSITY, "0.10");
	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_DSIPSCANNUM, " 0 / 0");
	//SetTimer(0, 100, NULL);


	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CBeamAutoAlignDlg::OnDestroy()
{
	CDialogEx::OnDestroy();
	KillTimer(0);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


void CBeamAutoAlignDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch (nIDEvent)
	{
	case 0:
		updataPosition();
		break;
	default:
		break;
	}
	CDialogEx::OnTimer(nIDEvent);
}

void CBeamAutoAlignDlg::drawXYChart(CChartViewer *viewer)
{
	ptrXYChart = new XYChart(450, 500);
	//cartesian->setRoundedFrame(m_extBgColor);
	ptrXYChart->addTitle("XY Coordinate Chart", "arial.ttf", 14
	)->setBackground(0xdddddd, 0x000000, Chart::glassEffect());

	ptrXYChart->setPlotArea(80, 60, 350, 350, 0xffffff, -1, -1, 0xcccccc, 0xcccccc);
	ptrXYChart->setBackground(LIGHT_GRAY, Transparent, 0);

	ScatterLayer *slayer = ptrXYChart->addScatterLayer(DoubleArray(dataHorizontal, 1), DoubleArray(dataVertical, 1), "",
		Chart::GlassSphereShape, 15, 0xff3333, 0xff3333);

	ptrXYChart->getPlotArea()->moveGridBefore(slayer);


	ptrXYChart->xAxis()->setLabelStyle("arial.ttf", 12);
	ptrXYChart->yAxis()->setLabelStyle("arial.ttf", 12);

	ptrXYChart->xAxis()->setColors(Chart::Transparent, Chart::TextColor, Chart::TextColor, 0xaaaaaa);
	ptrXYChart->yAxis()->setColors(Chart::Transparent);

	//ptrTempChart->xAxis()->setTickLength(10, 0);
	//
	ptrXYChart->xAxis()->setTickDensity(50);
	ptrXYChart->yAxis()->setTickDensity(50);

	ptrXYChart->xAxis()->setWidth(2);
	ptrXYChart->yAxis()->setWidth(2);

	ptrXYChart->yAxis()->setTitle("Vertical (mm)", "arial.ttf", 14, 0x555555);
	ptrXYChart->xAxis()->setTitle("Horizontal (mm)", "arial.ttf", 14, 0x555555);

	delete viewer->getChart();
	viewer->setChart(ptrXYChart);
}

void CBeamAutoAlignDlg::drawContourChart(CChartViewer * viewer)
{
	ptrContourChart = new XYChart(550, 500);
	//cartesian->setRoundedFrame(m_extBgColor);
	ptrContourChart->addTitle("Intensity Contour Chart", "arial.ttf", 14
	)->setBackground(0xdddddd, 0x000000, Chart::glassEffect());

	//ptrTempChart->setPlotArea(80, 60, 350, 350, 0xffffff, -1, -1, 0xcccccc, 0xcccccc);
	ptrContourChart->setPlotArea(80, 60, 350, 350, 0xffffff, -1, -1, ptrContourChart->dashLineColor(0x80000000, Chart::DotLine), -1);
	ptrContourChart->setBackground(LIGHT_GRAY, Transparent, 0);

	//double dataX[] = { -2, -2, 2, 2 };
	//double dataY[] = { -2, 2, 2, -2 };
	//double dataZ[];

	ContourLayer *layer = ptrContourChart->addContourLayer(DoubleArray(g_pBeamMain->m_chartx, g_pBeamSearch2D->m_nScanNumber+1),
		DoubleArray(g_pBeamMain->m_charty, g_pBeamSearch2D->m_nScanNumber+1), DoubleArray(g_pBeamMain->m_chartz, g_pBeamSearch2D->m_nScanNumber+1));

	layer->setContourColor(Chart::Transparent);

	ptrContourChart->getPlotArea()->moveGridBefore(layer);


	ptrContourChart->xAxis()->setLabelStyle("arial.ttf", 12);
	ptrContourChart->yAxis()->setLabelStyle("arial.ttf", 12);

	ptrContourChart->xAxis()->setColors(Chart::Transparent, Chart::TextColor, Chart::TextColor, 0xaaaaaa);
	ptrContourChart->yAxis()->setColors(Chart::Transparent);
	CString strPitchStart = "", strPitchEnd = "", strYawStart = "", strYawEnd = "", strPItchIncrement = "", strYawIncrement = "";
	if (m_bisFineScan == TRUE)
	{
		GetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_STARTPOSITION, strPitchStart);
		GetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_ENDPOSITION, strPitchEnd);
		GetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_STARTPOSITION, strYawStart);
		GetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_ENDPOSITION, strYawEnd);
		GetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_INCREMENT, strPItchIncrement);
		GetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_INCREMENT, strYawIncrement);
		ptrContourChart->xAxis()->setLinearScale(_ttof(strPitchStart), _ttof(strPitchEnd), _ttof(strPItchIncrement));
		ptrContourChart->yAxis()->setLinearScale(_ttof(strYawStart), _ttof(strYawEnd), _ttof(strYawIncrement));
	}
	else if(m_bisCorseScan == TRUE)
	{
		GetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_STARTPOSITION, strPitchStart);
		GetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_ENDPOSITION, strPitchEnd);
		GetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_STARTPOSITION, strYawStart);
		GetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_ENDPOSITION, strYawEnd);
		GetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_INCREMENT, strPItchIncrement);
		GetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_INCREMENT, strYawIncrement);
		ptrContourChart->xAxis()->setLinearScale(_ttof(strPitchStart), _ttof(strPitchEnd), _ttof(strPItchIncrement));
		ptrContourChart->yAxis()->setLinearScale(_ttof(strYawStart), _ttof(strYawEnd), _ttof(strYawIncrement));
	}
	else if (m_bisCirculScan = TRUE)
	{
		float fPitchStart = g_pBeamMain->m_d2DScanpPrameter1[0] - 0.11;
		float fPitchEnd = g_pBeamMain->m_d2DScanpPrameter1[0] + 0.11;
		float fYawhStart = g_pBeamMain->m_d2DScanpPrameter2[0] - 0.11;
		float fYawhEnd = g_pBeamMain->m_d2DScanpPrameter2[0] + 0.11;
		ptrContourChart->xAxis()->setLinearScale(fPitchStart, fPitchEnd, 0.05);
		ptrContourChart->yAxis()->setLinearScale(fYawhStart, fYawhEnd, 0.05);
	}
	else
	{
		ptrContourChart->xAxis()->setAutoScale();
		ptrContourChart->yAxis()->setAutoScale();
	}
	ptrContourChart->xAxis()->setTickDensity(40);
	ptrContourChart->yAxis()->setTickDensity(40);

	//ptrTempChart->xAxis()->setWidth(2);
	//ptrTempChart->yAxis()->setWidth(2); 

	ptrContourChart->xAxis()->setTitle("Pitch (mrad)", "arial.ttf", 14, 0x555555);
	ptrContourChart->yAxis()->setTitle("Yaw (mrad)", "arial.ttf", 14, 0x555555);

	ColorAxis *cAxis = layer->setColorAxis(470, 250, Chart::Left, 350, Chart::Right);
	cAxis->setBoundingBox(0xeeeeee, 0x444444);

	//cAxis->setTitle("Intensity Value (  )", "arialbi.ttf", 12);

	//cAxis->setLabelStyle("arialbd.ttf");

	cAxis->setColorGradient(true);

	delete viewer->getChart();
	viewer->setChart(ptrContourChart);
}

void CBeamAutoAlignDlg::OnViewPortChangedXYChart()
{

	// Update the chart if necessary
	if (m_XYchartViewer.needUpdateChart())
		drawXYChart(&m_XYchartViewer);
}


void CBeamAutoAlignDlg::OnViewPortChangedPITCHTYAWChart()
{

	// Update the chart if necessary
	if (m_ContourChartViewer.needUpdateChart())
		drawContourChart(&m_ContourChartViewer);
}

void CBeamAutoAlignDlg::updataPosition()
{
//	if (g_pSqOneControl->m_bConnected)
//	{
//		SetDlgItemTextA(IDC_BEAMALIGN_EDIT_HORIZONTAL, g_pSqOneManual->m_strHorizontal);
//		SetDlgItemTextA(IDC_BEAMALIGN_EDIT_VERTICAL, g_pSqOneManual->m_strVertical);
//		SetDlgItemTextA(IDC_BEAMALIGN_EDIT_BEAM, g_pSqOneManual->m_strBeam);
//		SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH, g_pSqOneManual->m_strPitch);
//		SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW, g_pSqOneManual->m_strYaw);
//		SetDlgItemTextA(IDC_BEAMALIGN_EDIT_ROLL, g_pSqOneManual->m_strRoll);
//	}
}


void CBeamAutoAlignDlg::OnBnClickedButtonAutoAlignRun()
{
	//run2DAutoAligh();
	m_pThread = AfxBeginThread(run2DAutoAlignThread, this);
}

BOOL CBeamAutoAlignDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}

void CBeamAutoAlignDlg::initialChart()
{
	for (int i = 0; i < GatherNum; i++)
	{
		dataHorizontal[i] = Chart::NoValue;
		dataVertical[i] = Chart::NoValue;
		dataPitch[i] = Chart::NoValue;
		dataYaw[i] = Chart::NoValue;
		dataIntensity[i] = Chart::NoValue;
		dataIteration[i] = Chart::NoValue;
	}
	WaitSec(0.1);
	drawXYChart(&m_XYchartViewer);
	drawContourChart(&m_ContourChartViewer);

	//m_XYchartViewer.ShowWindow(SW_SHOW);
	m_ContourChartViewer.ShowWindow(SW_SHOW);
}

void CBeamAutoAlignDlg::run2DAutoAlign()
{
	int nTempRet = 0, nRet = 0;

	CString strLog = "", strTemp = "";
	GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(FALSE);
	g_pSqOneControl->m_bStopFlag = FALSE;
	m_isAutoAlignOn = TRUE;

	m_isIntensityOn = FALSE;

	m_nAutoAlignStatus = 0;
	m_bisFineScan = FALSE;
	m_bisCorseScan = FALSE;
	m_bisCirculScan = FALSE;

	initialChart();
	g_pBeamMain->resetDataset();

	g_pBeamSearch2D->m_combobox_Sqscan_parameter1.SetCurSel(0);
	g_pBeamSearch2D->m_combobox_Sqscan_parameter2.SetCurSel(1);

	////corse scan ////
	GetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_STARTPOSITION, strTemp);
	g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_STARTPOSITION, strTemp);
	GetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_ITERATION, strTemp);
	g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_ITERATION, strTemp);
	GetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_INCREMENT, strTemp);
	g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_INCREMENT, strTemp);
	GetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_STARTPOSITION, strTemp);
	g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_STARTPOSITION, strTemp);
	GetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_ITERATION, strTemp);
	g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_ITERATION, strTemp);
	GetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_INCREMENT, strTemp);
	g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_INCREMENT, strTemp);
	
	if (g_pSqOneControl->m_bStopFlag == FALSE)
	{
		if (g_pBeamSearch2D->checkScanSet() != 0)
		{
			m_isAutoAlignOn = false;
			GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(true);
			return;
		}
		else
		{
			m_nAutoAlignStatus = 1;
			g_pBeamSearch2D->GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_ENDPOSITION, strTemp);
			SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_ENDPOSITION, strTemp);
			g_pBeamSearch2D->GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_ENDPOSITION, strTemp);
			SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_ENDPOSITION, strTemp);
		}
	}
	else
	{
		m_isAutoAlignOn = FALSE;
		GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(TRUE);
		return;
	}

	if (g_pSqOneControl->m_bStopFlag == FALSE)
	{
		nRet = g_pBeamSearch2D->runSearch();
		if (nRet != 1)
		{
			if (nRet == -2)
			{
				AfxMessageBox(_T("Fail FInd Beam"));
			}
			m_isAutoAlignOn = FALSE;
			GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(TRUE);
			return;
		}
		else
		{
			m_nAutoAlignStatus = 2;
		}
	}
	else
	{
		m_isAutoAlignOn = FALSE;
		GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(TRUE);
		return;
	}

	// beam direction//
	if (g_pSqOneControl->m_bStopFlag == FALSE)
	{
		nRet = g_pBeamSearch2D->runCirculScan();
		if (nRet != 0)
		{
			m_isAutoAlignOn = FALSE;
			GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(TRUE);
			return;
		}
		else
		{
			m_nAutoAlignStatus = 2;
			float dStartPicth = 0.0, dStartYaw = 0.0;
			float dAreaPitch = _ttof(g_pBeamConfig->m_strGridPercent) * _ttof(g_pBeamConfig->m_strOmegaPitch) / 100;
			float dAreaYaw = _ttof(g_pBeamConfig->m_strGridPercent) * _ttof(g_pBeamConfig->m_strOmegaYaw) / 100;
			if (g_pBeamMain->m_d2DScanIntensity[0] > g_pBeamMain->m_dCurcleScanMaxIntensity[0] || g_pBeamMain->m_d2DScanIntensity[0] > _ttof(g_pBeamConfig->m_strSatIntensity))
			{
				g_pSqOneControl->moveSQ1(g_pSqOneControl->m_SQOneParseData.X, g_pSqOneControl->m_SQOneParseData.Y, g_pSqOneControl->m_SQOneParseData.Z, g_pBeamMain->m_d2DScanpPrameter1[0], g_pBeamMain->m_d2DScanpPrameter2[0], g_pSqOneControl->m_SQOneParseData.Rz);
				dStartPicth = g_pBeamMain->m_d2DScanpPrameter1[0] - (dAreaPitch / 2);
				dStartYaw = g_pBeamMain->m_d2DScanpPrameter2[0] - (dAreaYaw / 2);
				setEditbox(dStartPicth, dStartYaw, FALSE, FALSE);
			}
			else
			{
				if (g_pBeamSearch2D->m_nDirectionIndex == 0)
				{
					g_pSqOneControl->moveSQ1(g_pSqOneControl->m_SQOneParseData.X, g_pSqOneControl->m_SQOneParseData.Y, g_pSqOneControl->m_SQOneParseData.Z, g_pBeamMain->m_d2DScanpPrameter1[0], g_pBeamMain->m_d2DScanpPrameter2[0], g_pSqOneControl->m_SQOneParseData.Rz);
					dStartPicth = g_pBeamMain->m_d2DScanpPrameter1[0];
					dStartYaw = g_pBeamMain->m_d2DScanpPrameter2[0] - (dAreaYaw / 2);
					setEditbox(dStartPicth, dStartYaw, FALSE, FALSE);
				}
				else if (g_pBeamSearch2D->m_nDirectionIndex < 3)
				{
					g_pSqOneControl->moveSQ1(g_pSqOneControl->m_SQOneParseData.X, g_pSqOneControl->m_SQOneParseData.Y, g_pSqOneControl->m_SQOneParseData.Z, g_pBeamMain->m_d2DScanpPrameter1[0], g_pBeamMain->m_d2DScanpPrameter2[0], g_pSqOneControl->m_SQOneParseData.Rz);
					dStartPicth = g_pBeamMain->m_d2DScanpPrameter1[0];
					dStartYaw = g_pBeamMain->m_d2DScanpPrameter2[0];
					setEditbox(dStartPicth, dStartYaw, FALSE, FALSE);
				}
				else if (g_pBeamSearch2D->m_nDirectionIndex == 3)
				{
					g_pSqOneControl->moveSQ1(g_pSqOneControl->m_SQOneParseData.X, g_pSqOneControl->m_SQOneParseData.Y, g_pSqOneControl->m_SQOneParseData.Z, g_pBeamMain->m_d2DScanpPrameter1[0], g_pBeamMain->m_d2DScanpPrameter2[0], g_pSqOneControl->m_SQOneParseData.Rz);
					dStartPicth = g_pBeamMain->m_d2DScanpPrameter1[0] - (dAreaPitch / 2);
					dStartYaw = g_pBeamMain->m_d2DScanpPrameter2[0];
					setEditbox(dStartPicth, dStartYaw, FALSE, FALSE);

				}
				else if (g_pBeamSearch2D->m_nDirectionIndex < 6)
				{
					g_pSqOneControl->moveSQ1(g_pSqOneControl->m_SQOneParseData.X, g_pSqOneControl->m_SQOneParseData.Y, g_pSqOneControl->m_SQOneParseData.Z, g_pBeamMain->m_d2DScanpPrameter1[0], g_pBeamMain->m_d2DScanpPrameter2[0], g_pSqOneControl->m_SQOneParseData.Rz);
					dStartPicth = g_pBeamMain->m_d2DScanpPrameter1[0];
					dStartYaw = g_pBeamMain->m_d2DScanpPrameter2[0];
					setEditbox(dStartPicth, dStartYaw, TRUE, FALSE);

				}
				else if (g_pBeamSearch2D->m_nDirectionIndex == 6)
				{
					g_pSqOneControl->moveSQ1(g_pSqOneControl->m_SQOneParseData.X, g_pSqOneControl->m_SQOneParseData.Y, g_pSqOneControl->m_SQOneParseData.Z, g_pBeamMain->m_d2DScanpPrameter1[0], g_pBeamMain->m_d2DScanpPrameter2[0], g_pSqOneControl->m_SQOneParseData.Rz);
					dStartPicth = g_pBeamMain->m_d2DScanpPrameter1[0];
					dStartYaw = g_pBeamMain->m_d2DScanpPrameter2[0] - (dAreaYaw / 2);
					setEditbox(dStartPicth, dStartYaw, TRUE, FALSE);

				}
				else if (g_pBeamSearch2D->m_nDirectionIndex < 9)
				{
					g_pSqOneControl->moveSQ1(g_pSqOneControl->m_SQOneParseData.X, g_pSqOneControl->m_SQOneParseData.Y, g_pSqOneControl->m_SQOneParseData.Z, g_pBeamMain->m_d2DScanpPrameter1[0], g_pBeamMain->m_d2DScanpPrameter2[0], g_pSqOneControl->m_SQOneParseData.Rz);
					dStartPicth = g_pBeamMain->m_d2DScanpPrameter1[0];
					dStartYaw = g_pBeamMain->m_d2DScanpPrameter2[0];
					setEditbox(dStartPicth, dStartYaw, TRUE, TRUE);
				}
				else if (g_pBeamSearch2D->m_nDirectionIndex == 9)
				{
					g_pSqOneControl->moveSQ1(g_pSqOneControl->m_SQOneParseData.X, g_pSqOneControl->m_SQOneParseData.Y, g_pSqOneControl->m_SQOneParseData.Z, g_pBeamMain->m_d2DScanpPrameter1[0], g_pBeamMain->m_d2DScanpPrameter2[0], g_pSqOneControl->m_SQOneParseData.Rz);
					dStartPicth = g_pBeamMain->m_d2DScanpPrameter1[0] - (dAreaPitch / 2);
					dStartYaw = g_pBeamMain->m_d2DScanpPrameter2[0];
					setEditbox(dStartPicth, dStartYaw, FALSE, TRUE);
				}
				else
				{
					g_pSqOneControl->moveSQ1(g_pSqOneControl->m_SQOneParseData.X, g_pSqOneControl->m_SQOneParseData.Y, g_pSqOneControl->m_SQOneParseData.Z, g_pBeamMain->m_d2DScanpPrameter1[0], g_pBeamMain->m_d2DScanpPrameter2[0], g_pSqOneControl->m_SQOneParseData.Rz);
					dStartPicth = g_pBeamMain->m_d2DScanpPrameter1[0];
					dStartYaw = g_pBeamMain->m_d2DScanpPrameter2[0];
					setEditbox(dStartPicth, dStartYaw, FALSE, TRUE);
				}
			}
		}
		m_nAutoAlignStatus = 3;
	}
	else
	{
		m_isAutoAlignOn = FALSE;
		GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(TRUE);
		return;
	}

	////fine scan ////
	if (g_pSqOneControl->m_bStopFlag == FALSE)
	{
		nTempRet = g_pBeamSearch2D->checkScanSet();
		if (nTempRet != 0)
		{
			m_isAutoAlignOn = false;
			GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(true);
			//nTempRet = -4;
			return;
		}
		else
		{
			m_nAutoAlignStatus = 4;
			g_pBeamSearch2D->GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_ENDPOSITION, strTemp);
			SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_ENDPOSITION, strTemp);
			g_pBeamSearch2D->GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_ENDPOSITION, strTemp);
			SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_ENDPOSITION, strTemp);
		}
	}
	else
	{
		m_isAutoAlignOn = FALSE;
		GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(TRUE);
		return;
	}


	if (g_pSqOneControl->m_bStopFlag == FALSE)
	{
		nRet = g_pBeamSearch2D->runSearch();
		if (nRet != 0)
		{
			m_isAutoAlignOn = FALSE;
			GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(TRUE);
			return;
		}
		else
		{
			m_nAutoAlignStatus = 5;
			g_pSqOneControl->moveSQ1(g_pSqOneControl->m_SQOneParseData.X, g_pSqOneControl->m_SQOneParseData.Y, g_pSqOneControl->m_SQOneParseData.Z, g_pBeamMain->m_d2DScanpPrameter1[0], g_pBeamMain->m_d2DScanpPrameter2[0], g_pSqOneControl->m_SQOneParseData.Rz);
		}
	}
	else
	{
		m_isAutoAlignOn = FALSE;
		GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(TRUE);
		return;
	}

	strTemp.Format(_T("MAX Intensity = %f"), g_pBeamMain->m_d2DScanIntensity[0]);
	g_pBeamMain->Display(0, strTemp);

	m_isAutoAlignOn = FALSE;
	GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(true);

	strTemp.Format(_T("%02.4f"), _ttof(g_pBeamConfig->m_strOmegaPitch) * _ttof(g_pBeamConfig->m_strIncrementPercent) / 100);
	g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_INCREMENT, strTemp);
	strTemp.Format(_T("%02.4f"), _ttof(g_pBeamConfig->m_strOmegaYaw) * _ttof(g_pBeamConfig->m_strIncrementPercent) / 100);
	g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_INCREMENT, strTemp);
	g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_ITERATION, "5");
	g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_ITERATION, "5");
	m_nAutoAlignStatus = 6;
	return;
}

void CBeamAutoAlignDlg::setEditbox(double dPitchStart, double dYawStart, BOOL bIsPitchInverse, BOOL bIsYawInverse)
{
	CString strtemp = "", strPitchStart = "", strYawStart = "";

	float dAreaPitch = _ttoi(g_pBeamConfig->m_strGridPercent) * _ttof(g_pBeamConfig->m_strOmegaPitch) / 100;
	float dAreaYaw = _ttoi(g_pBeamConfig->m_strGridPercent) * _ttof(g_pBeamConfig->m_strOmegaYaw) / 100;
	int nIterationPitch = dAreaPitch / _ttof(g_pBeamConfig->m_strFineScanGrid) + 1;
	int nIterationYaw = dAreaYaw / _ttof(g_pBeamConfig->m_strFineScanGrid) + 1;

	strtemp.Format(_T("%d"), nIterationPitch);
	g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_ITERATION, strtemp);
	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_ITERATION, strtemp);
	if (bIsPitchInverse == TRUE)
	{
		float dStartPosition = dPitchStart - dAreaPitch;
		strPitchStart.Format(_T("%f"), dStartPosition);
		g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_STARTPOSITION, strPitchStart);
		SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_STARTPOSITION, strPitchStart);
	}
	else
	{
		strPitchStart.Format(_T("%f"), dPitchStart);
		g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_STARTPOSITION, strPitchStart);
		SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_STARTPOSITION, strPitchStart);
	}
	g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_INCREMENT, g_pBeamConfig->m_strFineScanGrid);
	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_INCREMENT, g_pBeamConfig->m_strFineScanGrid);

	strtemp.Format(_T("%d"), nIterationYaw);
	g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_ITERATION, strtemp);
	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_ITERATION, strtemp);
	
	if (bIsYawInverse == TRUE)
	{
		float dStartPosition = dYawStart - dAreaYaw;
		strYawStart.Format(_T("%f"), dStartPosition);
		g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_STARTPOSITION, strYawStart);
		SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_STARTPOSITION, strYawStart);
	}
	else
	{
		strYawStart.Format(_T("%f"), dPitchStart);
		g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_STARTPOSITION, strYawStart);
		SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_STARTPOSITION, strYawStart);
	}
	g_pBeamSearch2D->SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_INCREMENT, g_pBeamConfig->m_strFineScanGrid);
	SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_INCREMENT, g_pBeamConfig->m_strFineScanGrid);
}

UINT CBeamAutoAlignDlg::run2DAutoAlignThread(LPVOID lParam)
{
	g_pBeamAutoAlign->run2DAutoAlign();

	return 0;
}


