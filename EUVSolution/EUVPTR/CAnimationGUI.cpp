#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include <array>

CAnimationGUI::CAnimationGUI()
{
	m_bThreadExitFlag			= FALSE;
	m_bConnectThreadExitFlag	= FALSE;
	m_pStatusThread				= NULL;
	m_pConnectThread			= NULL;
}

CAnimationGUI::~CAnimationGUI()
{
	m_bConnectThreadExitFlag = TRUE;

	if (m_pConnectThread != NULL)
	{
		HANDLE threadHandle = m_pConnectThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pConnectThread = NULL;
	}

	m_bThreadExitFlag = TRUE;

	if (m_pStatusThread != NULL)
	{
		HANDLE threadHandle = m_pStatusThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pStatusThread = NULL;
	}
}

int CAnimationGUI::OpenDevice()
{
	int nRet = 0;
	nRet = OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_LOCAL], g_pConfig->m_nPORT[ETHERNET_LOCAL], FALSE);
	if (nRet == 0)
	{
		InitializeData();
		m_pStatusThread = AfxBeginThread(SendDataThread, (LPVOID)this, 0, 0, CREATE_SUSPENDED, 0);
		m_pStatusThread->m_bAutoDelete = FALSE;
		m_pStatusThread->ResumeThread();
	}
	
	m_pConnectThread = AfxBeginThread(TryConnectThread, (LPVOID)this, 0, 0, CREATE_SUSPENDED, 0);
	m_pConnectThread->m_bAutoDelete = FALSE;
	m_pConnectThread->ResumeThread();

	return nRet;
}

void CAnimationGUI::InitializeData()
{
	data.bConnAdam = FALSE;
	data.bConnCrevis = FALSE;
	data.bConnMts = FALSE;
	data.bConnVmtr = FALSE;
	data.bConnCam1 = FALSE;
	data.bConnScanStage = FALSE;
	data.bConnNaviStage = FALSE;
	data.bConnFilterStage = FALSE;
	data.bConnSourcePc = FALSE;
	data.bConnVacuumGauge = FALSE;
	data.bConnLlcTmp = FALSE;
	data.bConnMcTmp = FALSE;
	data.bConnAfm = FALSE;
	data.bConnRevolver = FALSE;
	data.bConnXrayCamera = FALSE;

	data.bSequenceWorking = FALSE;

	data.bLpmPodExist = FALSE;
	data.bLpmMaskExist = FALSE;
	data.bLpmInitComp = FALSE;
	data.bLpmOpened = FALSE;
	data.bLpmWorking = FALSE;
	data.bLpmError = FALSE;

	data.bAtrMaskExist = FALSE;
	data.bAtrInitComp = FALSE;
	data.bAtrWorking = FALSE;
	data.bAtrError = FALSE;

	data.bFlipperMaskExist = FALSE;
	data.bFlipperInitComp = FALSE;
	data.bFlipperWorking = FALSE;
	data.bFlipperError = FALSE;

	data.bRotatorMaskExist = FALSE;
	data.bRotatorInitComp = FALSE;
	data.bRotatorWorking = FALSE;
	data.bRotatorError = FALSE;

	data.bLlcMaskExist = FALSE;
	data.bLlcGateValveOpened = FALSE;
	data.bLlcTmpGateValveOpened = FALSE;
	data.bLlcTmpWorking = 0;
	data.bLlcTmpError = FALSE;
	data.bLlcForelineValveOpened = FALSE;
	data.bLlcSlowRoughingValveOpened = FALSE;
	data.bLlcFastRoughingValveOpened = FALSE;
	data.bLlcDryPumpWorking = FALSE;
	data.bLlcDryPumpAlarm = FALSE;
	data.bLlcDryPumpWarning = FALSE;
	strcpy(data.sLlcVacuumRate, _T(""));

	data.bMcChuckMaskExist = FALSE;
	data.bMcTrGateValveOpened = FALSE;
	data.bMcTmpGateValveOpened = FALSE;
	data.bMcTmpWorking = 0;
	data.bMcTmpError = FALSE;
	data.bMcForelineValveOpened = FALSE;
	data.bMcSlowRoughingValveOpened = FALSE;
	data.bMcFastRoughingValveOpened = FALSE;
	data.bMcDryPumpWorking = FALSE;
	data.bMcDryPumpAlarm = FALSE;
	data.bMcDryPumpWarning = FALSE;
	strcpy(data.sMcVacuumRate, _T(""));

	data.bVmtrMaskExist = FALSE;
	data.bVmtrServoOn = FALSE;
	data.bVmtrInitComp = FALSE;
	data.bVmtrWorking = FALSE;
	data.bVmtrExtended = FALSE;
	data.bVmtrRetracted = FALSE;
	data.bVmtrError = FALSE;
	data.bVmtrEMO = FALSE;
	data.dVmtrFeedbackPosT = 0.0;
	data.dVmtrFeedbackPosL = 0.0;
	data.dVmtrFeedbackPosZ = 0.0;

	data.bNaviLoadingPos = FALSE;
	strcpy(data.sNaviFeedbackPosX, _T("0.0"));
	strcpy(data.sNaviFeedbackPosY, _T("0.0"));

	data.bSlowVentValveOpened = FALSE;
	data.bSlowInletValveOpened = FALSE;
	data.bSlowOutletValveOpened = FALSE;
	data.bFastVentValveOpened = FALSE;
	data.bFastInletValveOpened = FALSE;
	data.bFastOutletValveOpened = FALSE;
	data.dMFC1N2 = 0.0;
	data.dMFC2N2 = 0.0;

	strcpy(data.sSrcMcVacuumRate, _T(""));
	strcpy(data.sSrcScVacuumRate, _T(""));
	strcpy(data.sSrcBcVacuumRate, _T(""));
	data.bEuvLaserShutterOpened = FALSE;
	data.bEuvOn = FALSE;
	data.bSrcMechShutterOpened = FALSE;
	strcpy(data.sSrcMirrorStagePos, _T(""));

	data.bWaterSupplyValveOpened = FALSE;
	data.bWaterReturnValveOpened = FALSE;
	data.bMainAir = FALSE;
	data.bMainWater = FALSE;
	data.bWaterLeakLlcTmp = FALSE;
	data.bWaterLeakMcTmp = FALSE;
	data.bSmokeDetectCb = FALSE;
	data.bSmokeDetectVacSt = FALSE;
	data.bAlarmWaterReturnTemp = FALSE;
	data.bAvailableSrcGateOpened = FALSE;

	data.bMcMaskTiltError1 = FALSE;
	data.bMcMaskTiltError2 = FALSE;
	data.bLlcMaskTiltError1 = FALSE;
	data.bLlcMaskTiltError2 = FALSE;

	data.nMaskLocation = -1;
}

int CAnimationGUI::SendMessageData(CString sMsg)
{
	int nRet;

	MessageData _data;
	_data.message_id = MSG_ID_MESSAGE;
	_data.message_size = sizeof(MessageData);
	strcpy(_data.Message, sMsg);

	nRet = SendStringMessage(_data);

	return nRet;
}

#if FALSE //사용 안함
int CAnimationGUI::SetEtcConfigData()
{
	int nRet;

	EtcConfigData _data;
	_data.message_id = MSG_ID_ETC;
	_data.message_size = sizeof(EtcConfigData);

	m_stEtcData->use_material_flip = g_pConfig->m_bUseFlip;
	m_stEtcData->use_material_rotate = g_pConfig->m_bUseRotate;
	m_stEtcData->rotate_angle = g_pConfig->m_nRotateAngle;
	//m_stEtcData->use_vacrobot_cam_sensor_interlock = g_pConfig->m_bUseCamSensor;
	m_stEtcData->auto_source_off_time = g_pConfig->m_nSourceAutoOffTime_min;

	nRet = SetEtcConfigDataCtrl(_data);

	return nRet;
}

int CAnimationGUI::SetCalibrationData()
{
	int nRet;

	CalibrationData _data;
	_data.message_id = MSG_ID_CALIBRATION;
	_data.message_size = sizeof(stCalibrationData);

	CString temp;
	temp.Format("%f", g_pConfig->m_dConfigCalTx_urad);
	strcpy(_data.scan_stage_calibration_tx, temp);
	temp.Format("%f", g_pConfig->m_dConfigCalTy_urad);
	strcpy(_data.scan_stage_calibration_ty, temp);

	temp.Format("%f", g_pConfig->m_dGlobalOffsetX_mm);
	strcpy(_data.global_offset_x, temp);
	temp.Format("%f", g_pConfig->m_dGlobalOffsetY_mm);
	strcpy(_data.global_offset_y, temp);

	temp.Format("%f", g_pConfig->m_dLBOffsetX_mm);
	strcpy(_data.align_lb_offset_x, temp);
	temp.Format("%f", g_pConfig->m_dLBOffsetY_mm);
	strcpy(_data.align_lb_offset_y, temp);
	temp.Format("%f", g_pConfig->m_dLTOffsetX_mm);
	strcpy(_data.align_lt_offset_x, temp);
	temp.Format("%f", g_pConfig->m_dLTOffsetY_mm);
	strcpy(_data.align_lt_offset_y, temp);
	temp.Format("%f", g_pConfig->m_dRTOffsetX_mm);
	strcpy(_data.align_rt_offset_x, temp);
	temp.Format("%f", g_pConfig->m_dRTOffsetY_mm);
	strcpy(_data.align_rt_offset_y, temp);

	temp.Format("%f", g_pConfig->m_dLB_LaserSwitching_OffsetX_mm);
	strcpy(_data.laser_switching_lb_offset_x, temp);
	temp.Format("%f", g_pConfig->m_dLB_LaserSwitching_OffsetY_mm);
	strcpy(_data.laser_switching_lb_offset_y, temp);
	temp.Format("%f", g_pConfig->m_dLT_LaserSwitching_OffsetX_mm);
	strcpy(_data.laser_switching_lt_offset_x, temp);
	temp.Format("%f", g_pConfig->m_dLT_LaserSwitching_OffsetY_mm);
	strcpy(_data.laser_switching_lt_offset_y, temp);
	temp.Format("%f", g_pConfig->m_dRT_LaserSwitching_OffsetX_mm);
	strcpy(_data.laser_switching_rt_offset_x, temp);
	temp.Format("%f", g_pConfig->m_dRT_LaserSwitching_OffsetY_mm);
	strcpy(_data.laser_switching_rt_offset_y, temp);

	temp.Format("%f", g_pConfig->m_dZdistanceCap1nStage_um);
	strcpy(_data.cap1_focus_distance_z, temp);
	temp.Format("%f", g_pConfig->m_dZdistanceCap2nStage_um);
	strcpy(_data.cap2_focus_distance_z, temp);
	temp.Format("%f", g_pConfig->m_dZdistanceCap3nStage_um);
	strcpy(_data.cap3_focus_distance_z, temp);
	temp.Format("%f", g_pConfig->m_dZdistanceCap4nStage_um);
	strcpy(_data.cap4_focus_distance_z, temp);

	temp.Format("%f", g_pConfig->m_dInspectorOffsetX_um);
	strcpy(_data.inspector_offset_x, temp);
	temp.Format("%f", g_pConfig->m_dInspectorOffsetY_um);
	strcpy(_data.inspector_offset_y, temp);

	nRet = SetCalibrationDataCtrl(_data);

	return nRet;
}

int CAnimationGUI::SetRecoveryData()
{
	int nRet;
	
	RecoveryData _data;
	_data.message_id = MSG_ID_RECOVERY;
	_data.message_size = sizeof(RecoveryData);
	
	CString temp;
	temp.Format("%f", g_pConfig->m_dOMAlignPointLB_X_mm);
	strcpy(_data.om_align_lb_posx, temp);
	temp.Format("%f", g_pConfig->m_dOMAlignPointLB_Y_mm);
	strcpy(_data.om_align_lb_posy, temp);
	temp.Format("%f", g_pConfig->m_dOMAlignPointLT_X_mm);
	strcpy(_data.om_align_lt_posx, temp);
	temp.Format("%f", g_pConfig->m_dOMAlignPointLT_Y_mm);
	strcpy(_data.om_align_lt_posy, temp);
	temp.Format("%f", g_pConfig->m_dOMAlignPointRT_X_mm);
	strcpy(_data.om_align_rt_posx, temp);
	temp.Format("%f", g_pConfig->m_dOMAlignPointRT_Y_mm);
	strcpy(_data.om_align_rt_posy, temp);

	temp.Format("%f", g_pConfig->m_dEUVAlignPointLB_X_mm);
	strcpy(_data.euv_align_lb_posx, temp);
	temp.Format("%f", g_pConfig->m_dEUVAlignPointLB_Y_mm);
	strcpy(_data.euv_align_lb_posy, temp);
	temp.Format("%f", g_pConfig->m_dEUVAlignPointLT_X_mm);
	strcpy(_data.euv_align_lt_posx, temp);
	temp.Format("%f", g_pConfig->m_dEUVAlignPointLT_Y_mm);
	strcpy(_data.euv_align_lt_posy, temp);
	temp.Format("%f", g_pConfig->m_dEUVAlignPointRT_X_mm);
	strcpy(_data.euv_align_rt_posx, temp);
	temp.Format("%f", g_pConfig->m_dEUVAlignPointRT_Y_mm);
	strcpy(_data.euv_align_rt_posy, temp);

	_data.om_align_complete_flag = g_pConfig->m_bOMAlignCompleteFlag;
	_data.euv_align_complete_flag = g_pConfig->m_bEUVAlignCompleteFlag;
	//_data.laser_feedback_available_flag = g_pConfig->m_bLaserFeedbackAvailable_Flag;
	_data.laser_feedback_flag = g_pConfig->m_bLaserFeedback_Flag;
	_data.euv_align_fov = g_pConfig->m_nEUVAlignFOV;
	_data.euv_align_grid = g_pConfig->m_nEUVAlignGrid;

	temp.Format("%f", g_pConfig->m_dZInterlock_um);
	strcpy(_data.zinterlock_position, temp);
	temp.Format("%f", g_pConfig->m_dZFocusPos_um);
	strcpy(_data.zfocus_position, temp);

	_data.mts_rotate_done_flag = g_pConfig->m_bMtsRotateDone_Flag;
	_data.mts_flip_done_flag = g_pConfig->m_bMtsFlipDone_Flag;
	_data.current_process = g_pConfig->m_nPreviousProcess;
	_data.material_location = g_pConfig->m_nMaterialLocation;
	
	nRet = SetRecoveryDataCtrl(_data);

	return nRet;
}

int CAnimationGUI::SetVacuumSeqData()
{
	int nRet;

	VacuumSeqData _data;
	_data.message_id = MSG_ID_VACUUM_SEQ;
	_data.message_size = sizeof(VacuumSeqData);

	CString temp;
	temp.Format("%lf", g_pConfig->m_dPressure_ChangeToVent);
	strcpy(_data.mc_standby_vent_value, temp);
	temp.Format("%lf", g_pConfig->m_dPressure_ChangeToSlow_Vent);
	strcpy(_data.mc_slow_vent_value, temp);
	temp.Format("%lf", g_pConfig->m_dPressure_Vent_End);
	strcpy(_data.mc_fast_vent_value, temp);
	temp.Format("%lf", g_pConfig->m_dPressure_ChangeToFast_MC_Rough);
	strcpy(_data.mc_slow_rough_value, temp);
	temp.Format("%lf", g_pConfig->m_dPressure_ChangeToMCTMP_Rough);
	strcpy(_data.mc_fast_rough_value, temp);
	temp.Format("%lf", g_pConfig->m_dPressure_Rough_End);
	strcpy(_data.mc_tmp_rough_value, temp);

	_data.mc_standby_vent_time = g_pConfig->m_nTimeout_sec_MCLLCVent;
	_data.mc_slow_vent_time    = g_pConfig->m_nTimeout_sec_MCSlow1Vent;
	_data.mc_fast_vent_time    = g_pConfig->m_nTimeout_sec_MCFastVent;
	_data.mc_slow_rough_time   = g_pConfig->m_nTimeout_sec_MCSlowRough;
	_data.mc_fast_rough_time   = g_pConfig->m_nTimeout_sec_MCFastRough;
	_data.mc_tmp_rough_time    = g_pConfig->m_nTimeout_sec_MCTmpEnd;

	temp.Format("%lf", g_pConfig->m_dPressure_ChangeToVent_LLC);
	strcpy(_data.llc_standby_vent_value, temp);
	temp.Format("%lf", g_pConfig->m_dPressure_ChangeToSlow2_Vent);
	strcpy(_data.llc_slow_vent_value, temp);
	temp.Format("%lf", g_pConfig->m_dPressure_Vent_End);
	strcpy(_data.llc_fast_vent_value, temp);
	temp.Format("%lf", g_pConfig->m_dPressure_ChangeToFast_Rough);
	strcpy(_data.llc_slow_rough_value, temp);
	temp.Format("%lf", g_pConfig->m_dPressure_ChangeToTMP_Rough);
	strcpy(_data.llc_fast_rough_value, temp);
	temp.Format("%lf", g_pConfig->m_dPressure_Rough_End);
	strcpy(_data.llc_tmp_rough_value, temp);

	_data.llc_standby_vent_time = g_pConfig->m_nTimeout_sec_LLKStandbyVent;
	_data.llc_slow_vent_time    = g_pConfig->m_nTimeout_sec_LLKSlow1Vent;
	_data.llc_fast_vent_time    = g_pConfig->m_nTimeout_sec_LLKFastVent;
	_data.llc_slow_rough_time   = g_pConfig->m_nTimeout_sec_LLKSlowRough;
	_data.llc_fast_rough_time   = g_pConfig->m_nTimeout_sec_LLKFastRough;
	_data.llc_tmp_rough_time    = g_pConfig->m_nTimeout_sec_LLKRoughEnd;

	nRet = SetVacuumSeqDataCtrl(_data);

	return nRet;
}

int CAnimationGUI::SetPtrData()
{
	int nRet;

	PtrData _data;
	_data.message_id = MSG_ID_PTR;
	_data.message_size = sizeof(PtrData);

	CString temp;
	temp.Format("%f", g_pConfig->m_dD1Ref);
	strcpy(_data.d1_reference, temp);
	temp.Format("%f", g_pConfig->m_dD2Ref);
	strcpy(_data.d2_reference, temp);
	temp.Format("%f", g_pConfig->m_dD3Ref);
	strcpy(_data.d3_reference, temp);
	temp.Format("%f", g_pConfig->m_dD1RefStd);
	strcpy(_data.d1_reference_std, temp);
	temp.Format("%f", g_pConfig->m_dD2RefStd);
	strcpy(_data.d2_reference_std, temp);
	temp.Format("%f", g_pConfig->m_dD3RefStd);
	strcpy(_data.d3_reference_std, temp);
	temp.Format("%f", g_pConfig->m_RefReflectance);
	strcpy(_data.reference_reflectance, temp);

	nRet = SetPtrDataCtrl(_data);

	return nRet;
}

int CAnimationGUI::SetPhaseData()
{
	int nRet;

	PhaseData _data;
	_data.message_id = MSG_ID_PHASE;
	_data.message_size = sizeof(PhaseData);

	CString temp;
	_data.align_ccd_x = g_pConfig->m_nAlignCcdX;
	_data.align_ccd_y = g_pConfig->m_nAlignCcdY;
	_data.align_ccd_width = g_pConfig->m_nAlignCcdWidth;
	_data.align_ccd_height = g_pConfig->m_nAlignCcdHeight;
	_data.align_ccd_binning = g_pConfig->m_nAlignCcdBinning;
	_data.align_image_x = g_pConfig->m_nAlignImageX;
	_data.align_image_y = g_pConfig->m_nAlignImageY;
	_data.align_image_width = g_pConfig->m_nAlignImageWidth;
	_data.align_image_height = g_pConfig->m_nAlignImageHeight;
	temp.Format("%f", g_pConfig->m_dAlignExposureTime);
	strcpy(_data.align_exposure_time, temp);

	_data.measure_ccd_x = g_pConfig->m_nMeasureCcdX;
	_data.measure_ccd_y = g_pConfig->m_nMeasureCcdY;
	_data.measure_ccd_width = g_pConfig->m_nMeasureCcdWidth;
	_data.measure_ccd_height = g_pConfig->m_nMeasureCcdHeight;
	_data.measure_ccd_binning = g_pConfig->m_nMeasureCcdBinning;
	_data.measure_image_x = g_pConfig->m_nMeasureImageX;
	_data.measure_image_y = g_pConfig->m_nMeasureImageY;
	_data.measure_image_width = g_pConfig->m_nMeasureImageWidth;
	_data.measure_image_height = g_pConfig->m_nMeasureImageHeight;
	temp.Format("%f", g_pConfig->m_dMeasureExposureTime);
	strcpy(_data.measure_exposure_time, temp);

	temp.Format("%f", g_pConfig->m_dHorizentalMargin_um);
	strcpy(_data.horizontal_margin, temp);
	temp.Format("%f", g_pConfig->m_dVerticalMargin_um);
	strcpy(_data.vertical_margin, temp);

	_data.maximum_try_count = g_pConfig->m_nMaximumTryCount;
	temp.Format("%f", g_pConfig->m_dCoarseAlignIntensityTolerancePercent);
	strcpy(_data.coarse_align_intensity_tolerence_percent, temp);

	temp.Format("%f", g_pConfig->m_dStepWidthX_um);
	strcpy(_data.step_width_x, temp);
	_data.half_number_of_step_x = g_pConfig->m_nHalfNumberOfStepX;
	temp.Format("%f", g_pConfig->m_dStepWidthY_um);
	strcpy(_data.step_width_y, temp);
	_data.half_number_of_step_y = g_pConfig->m_nHalfNumberOfStepY;

	_data.num_of_std_point = g_pConfig->m_nNumOfStdPoint;
	_data.fine_align_algorithm_type = g_pConfig->m_nFineAlignAlgorithmType;

	temp.Format("%f", g_pConfig->m_dSlitWidth_um);
	strcpy(_data.slit_width, temp);
	temp.Format("%f", g_pConfig->m_dSlitPitch_um);
	strcpy(_data.slit_pitch, temp);
	temp.Format("%f", g_pConfig->m_dSlitLength_um);
	strcpy(_data.slit_length, temp);

	//temp.Format("%f", g_pConfig->m_dInterlockMargin_um);
	//strcpy(_data.interlock_margin, temp);
	//temp.Format("%f", g_pConfig->m_dZControlTolerance_um);
	//strcpy(_data.z_control_tolerance, temp);

	//temp.Format("%f", g_pConfig->m_dCap1PosLimitMin_um);
	//strcpy(_data.cap1_pos_limit_min, temp);
	//temp.Format("%f", g_pConfig->m_dCap1PosLimitMax_um);
	//strcpy(_data.cap1_pos_limit_max, temp);
	//temp.Format("%f", g_pConfig->m_dCap2PosLimitMin_um);
	//strcpy(_data.cap2_pos_limit_min, temp);
	//temp.Format("%f", g_pConfig->m_dCap2PosLimitMax_um);
	//strcpy(_data.cap2_pos_limit_max, temp);
	//temp.Format("%f", g_pConfig->m_dCap3PosLimitMin_um);
	//strcpy(_data.cap3_pos_limit_min, temp);
	//temp.Format("%f", g_pConfig->m_dCap3PosLimitMax_um);
	//strcpy(_data.cap3_pos_limit_max, temp);
	//temp.Format("%f", g_pConfig->m_dCap4PosLimitMin_um);
	//strcpy(_data.cap4_pos_limit_min, temp);
	//temp.Format("%f", g_pConfig->m_dCap4PosLimitMax_um);
	//strcpy(_data.cap4_pos_limit_max, temp);

	//temp.Format("%f", g_pConfig->m_dCap1MeasurePos_um);
	//strcpy(_data.cap1_measure, temp);
	//temp.Format("%f", g_pConfig->m_dCap2MeasurePos_um);
	//strcpy(_data.cap2_measure, temp);
	//temp.Format("%f", g_pConfig->m_dCap3MeasurePos_um);
	//strcpy(_data.cap3_measure, temp);
	//temp.Format("%f", g_pConfig->m_dCap4MeasurePos_um);
	//strcpy(_data.cap4_measure, temp);

	//temp.Format("%f", g_pConfig->m_dZStageLimitMin_um);
	//strcpy(_data.z_stage_limit_min, temp);
	//temp.Format("%f", g_pConfig->m_dZStageLimitMax_um);
	//strcpy(_data.z_stage_limit_max, temp);

	temp.Format("%f", g_pConfig->m_dImageRotationAngle_deg);
	strcpy(_data.image_rotation_angle, temp);
	temp.Format("%f", g_pConfig->m_dXrayCameraReadTimeout_ms);
	strcpy(_data.xray_camera_read_timeout, temp);
	temp.Format("%f", g_pConfig->m_dDownZHeightWhenMove_um);
	strcpy(_data.down_z_height_when_move, temp);
	_data.nfft = g_pConfig->m_nNfft;
	temp.Format("%f", g_pConfig->m_dContinuousWave);
	strcpy(_data.continuous_wave, temp);
	_data.continuous_wave_date = g_pConfig->m_nContinuousWaveDate;
	_data.back_ground_height = g_pConfig->m_nBackGroundHeight;

	nRet = SetPhaseDataCtrl(_data);

	return nRet;
}

int CAnimationGUI::SetXRayCamData()
{
	int nRet;

	XRayCamData _data;
	_data.message_id = MSG_ID_XRAY_CAM;
	_data.message_size = sizeof(XRayCamData);

	CString temp;
	_data.adc_quality = g_pXrayCamera->XrayCameraControlParameterInConfig.tAdcQuaulity;
	temp.Format("%f", g_pXrayCamera->XrayCameraControlParameterInConfig.fAdcSpeed);
	strcpy(_data.adc_speed, temp);

	_data.adc_analog_gain = g_pXrayCamera->XrayCameraControlParameterInConfig.tAdcAnalogGain;
	temp.Format("%f", g_pXrayCamera->XrayCameraControlParameterInConfig.fSensorTemperatureSetPoint);
	strcpy(_data.sensor_temp_set_point, temp);

	_data.ccd_roi_pos_x = g_pXrayCamera->XrayCameraControlParameterInConfig.Roi.x;
	_data.ccd_roi_pos_y = g_pXrayCamera->XrayCameraControlParameterInConfig.Roi.y;
	_data.ccd_roi_width = g_pXrayCamera->XrayCameraControlParameterInConfig.Roi.width;
	_data.ccd_roi_height = g_pXrayCamera->XrayCameraControlParameterInConfig.Roi.height;
	_data.ccd_roi_x_binning = g_pXrayCamera->XrayCameraControlParameterInConfig.Roi.x_binning;
	_data.ccd_roi_y_binning = g_pXrayCamera->XrayCameraControlParameterInConfig.Roi.y_binning;
	_data.readout_control_mode = g_pXrayCamera->XrayCameraControlParameterInConfig.tReadoutControlMode;
	_data.readout_port_count = g_pXrayCamera->XrayCameraControlParameterInConfig.nReadoutPortCount;


	temp.Format("%f", g_pXrayCamera->XrayCameraControlParameterInConfig.fExposureTime);
	strcpy(_data.exposure_time, temp);

	_data.shutter_timming_mode = g_pXrayCamera->XrayCameraControlParameterInConfig.tShutterTimingMode;

	temp.Format("%f", g_pXrayCamera->XrayCameraControlParameterInConfig.fShutterDelayResolution);
	strcpy(_data.shutter_delay_resolution, temp);

	
	temp.Format("%f", g_pXrayCamera->XrayCameraControlParameterInConfig.fShutterOpeningDelay);
	strcpy(_data.shutter_opening_delay, temp);

	temp.Format("%f", g_pXrayCamera->XrayCameraControlParameterInConfig.fShutterClosingDelay);
	strcpy(_data.shutter_closing_delay, temp);

	_data.trigger_response = g_pXrayCamera->XrayCameraControlParameterInConfig.tTriggerResponse;
	_data.trigger_determination = g_pXrayCamera->XrayCameraControlParameterInConfig.tTriggerDetermination;
	_data.output_signal = g_pXrayCamera->XrayCameraControlParameterInConfig.tOutputSignal;
	_data.output_signal2 = g_pXrayCamera->XrayCameraControlParameterInConfig.tOutputSignal2;
	_data.invert_output_signal = g_pXrayCamera->XrayCameraControlParameterInConfig.nbInvertOutputsignal;
	_data.invert_output_signal2 = g_pXrayCamera->XrayCameraControlParameterInConfig.nbInvertOutputsignal2;
	_data.clean_cycle_count = g_pXrayCamera->XrayCameraControlParameterInConfig.nCleanCycleCount;
	_data.clean_cycle_height = g_pXrayCamera->XrayCameraControlParameterInConfig.nCleanCycleHeight;
	_data.roi_x = g_pXrayCamera->SupplymentaryParameter.ImageRoi.x;
	_data.roi_y = g_pXrayCamera->SupplymentaryParameter.ImageRoi.y;
	_data.roi_width = g_pXrayCamera->SupplymentaryParameter.ImageRoi.width;
	_data.roi_height = g_pXrayCamera->SupplymentaryParameter.ImageRoi.height;

	temp.Format("%f", g_pXrayCamera->SupplymentaryParameter.NormalTemperature);
	strcpy(_data.normal_temperature, temp);

	temp.Format("%f", g_pXrayCamera->SupplymentaryParameter.OperationTemperature);
	strcpy(_data.operation_temperature, temp);

	nRet = SetXRayCamDataCtrl(_data);

	return nRet;
}

int CAnimationGUI::GetConfigData(CString sMsg)
{
	int nRet;

	MessageData _data;
	_data.message_id = MSG_ID_MESSAGE;
	_data.message_size = sizeof(MessageData);
	strcpy(_data.Message, sMsg);

	nRet = GetConfigDataCtrl(_data);

	return nRet;
}
#endif

UINT CAnimationGUI::TryConnectThread(LPVOID pParam)
{
	CAnimationGUI* pCtrl = (CAnimationGUI*)pParam;
	
	while (!pCtrl->m_bConnectThreadExitFlag)
	{
		if (pCtrl->m_bConnected == FALSE)
		{
			int nRet = 0;

			//20210121 jkseo, CloseTcpIpSocket() 함수 호출 시 다른 모듈 문제 발생하여 임시 주석처리
			//pCtrl->CloseTcpIpSocket();
			nRet = pCtrl->OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_LOCAL], g_pConfig->m_nPORT[ETHERNET_LOCAL], FALSE);
			if (nRet == 0)
			{
				if (pCtrl->m_pStatusThread == NULL)
				{
					pCtrl->InitializeData();
					pCtrl->m_pStatusThread = AfxBeginThread(SendDataThread, (LPVOID)pCtrl, THREAD_PRIORITY_NORMAL);
				}
			}
		}

		Sleep(3000);
	}

	return 0;
}

UINT CAnimationGUI::SendDataThread(LPVOID pParam)
{
	CAnimationGUI* pCtrl = (CAnimationGUI*)pParam;

	while (!pCtrl->m_bThreadExitFlag)
	{
		if (pCtrl->m_bConnected == TRUE)
		{
			pCtrl->data.message_id = MSG_ID_MONITORING;
			pCtrl->data.message_size = sizeof(struct MonitoringData);

			if (g_pAdam != NULL)
			{
				pCtrl->data.bConnAdam = g_pAdam->Is_ADAM_Connected();
			}

			if (g_pIO != NULL)
			{
				pCtrl->data.bConnCrevis = g_pIO->Is_CREVIS_Connected();

				pCtrl->data.bLlcMaskExist = g_pIO->Is_Mask_Check_Only_OnLLC();
				pCtrl->data.bLlcGateValveOpened = g_pIO->Is_LLCGateValve_Open();
				pCtrl->data.bLlcTmpGateValveOpened = g_pIO->Is_LLC_TMP_GateValve_Open();
				
				pCtrl->data.bLlcForelineValveOpened = g_pIO->Is_LLC_TMP_ForelineValve_Open();
				pCtrl->data.bLlcSlowRoughingValveOpened = g_pIO->Is_LLC_SlowRoughValve_Open();
				pCtrl->data.bLlcFastRoughingValveOpened = g_pIO->Is_LLC_FastRoughValve_Open();
				pCtrl->data.bLlcDryPumpWorking = g_pIO->Is_LLC_DryPump_Status() == 1 ? TRUE : FALSE;
				pCtrl->data.bLlcDryPumpAlarm = g_pIO->Is_LLC_DryPump_Status() == -2 ? TRUE : FALSE;
				pCtrl->data.bLlcDryPumpWarning = g_pIO->Is_LLC_DryPump_Status() == -1 ? TRUE : FALSE;

				pCtrl->data.bMcChuckMaskExist = g_pIO->Is_Mask_Check_Only_OnChuck();
				pCtrl->data.bMcTrGateValveOpened = g_pIO->Is_TRGateValve_Open();
				pCtrl->data.bMcTmpGateValveOpened = g_pIO->Is_MC_TMP_GateValve_Open();

				pCtrl->data.bMcForelineValveOpened = g_pIO->Is_MC_TMP_ForelineValve_Open();
				pCtrl->data.bMcSlowRoughingValveOpened = g_pIO->Is_MC_SlowRoughValve_Open();
				pCtrl->data.bMcFastRoughingValveOpened = g_pIO->Is_MC_FastRoughValve_Open();
				pCtrl->data.bMcDryPumpWorking = g_pIO->Is_MC_DryPump_Status() == 1 ? TRUE : FALSE;
				pCtrl->data.bMcDryPumpAlarm = g_pIO->Is_MC_DryPump_Status() == -2 ? TRUE : FALSE;
				pCtrl->data.bMcDryPumpWarning = g_pIO->Is_MC_DryPump_Status() == -1 ? TRUE : FALSE;

				if (g_pIO->Is_Mask_OnVMTR() == TRUE || g_pIO->Is_Mask_OnVMTR_LLC() == TRUE)
					pCtrl->data.bVmtrMaskExist = TRUE;
				else
					pCtrl->data.bVmtrMaskExist = FALSE;

				pCtrl->data.bVmtrExtended = g_pIO->Is_VAC_Robot_Arm_Extend();
				pCtrl->data.bVmtrRetracted = g_pIO->Is_VAC_Robot_Arm_Retract();

				pCtrl->data.bSlowVentValveOpened = g_pIO->Is_SlowVentValve_Open();
				pCtrl->data.bSlowInletValveOpened = g_pIO->Is_SlowVent_Inlet_Valve_Open();
				pCtrl->data.bSlowOutletValveOpened = g_pIO->Is_SlowVent_Outlet_Valve_Open();
				pCtrl->data.bFastVentValveOpened = g_pIO->Is_FastVentValve_Open();
				pCtrl->data.bFastInletValveOpened = g_pIO->Is_FastVent_Inlet_Valve_Open();
				pCtrl->data.bFastOutletValveOpened = g_pIO->Is_FastVent_Outlet_Valve_Open();
				pCtrl->data.dMFC1N2 = g_pIO->Get_MFC2_N2_10sccm_In();
				pCtrl->data.dMFC2N2 = g_pIO->Get_MFC1_N2_20000sccm_In();

				pCtrl->data.bWaterSupplyValveOpened = g_pIO->Is_Water_Supply_Status();
				//pCtrl->data.bWaterReturnValveOpened = g_pIO->Water_Return_Valve_Open();
				pCtrl->data.bMainAir = g_pIO->Is_Air_Supply_Status();
				pCtrl->data.bMainWater = g_pIO->Is_Water_Supply_Status();
				pCtrl->data.bWaterLeakLlcTmp = g_pIO->Is_Tmp_LLC_Leak_Status() == 0 ? TRUE : FALSE;
				pCtrl->data.bWaterLeakMcTmp = g_pIO->ls_Tmp_MC_Leak_Status() == 0 ? TRUE : FALSE;
				pCtrl->data.bSmokeDetectCb = FALSE;
				pCtrl->data.bSmokeDetectVacSt = FALSE;
				pCtrl->data.bAlarmWaterReturnTemp = FALSE;
				pCtrl->data.bAvailableSrcGateOpened = g_pIO->Is_SourceGate_OpenOn_Check();

				pCtrl->data.bMcMaskTiltError1 = g_pIO->Is_MC_Mask_Slant1_Check();
				pCtrl->data.bMcMaskTiltError2 = g_pIO->Is_MC_Mask_Slant2_Check();
				pCtrl->data.bLlcMaskTiltError1 = g_pIO->Is_LLC_Mask_Slant1_Check();
				pCtrl->data.bLlcMaskTiltError2 = g_pIO->Is_LLC_Mask_Slant2_Check();
			}

			if (g_pEfem != NULL)
			{
				pCtrl->data.bConnMts = g_pEfem->Is_MTS_Connected();

				pCtrl->data.bLpmPodExist = g_pEfem->Is_POD_OnLPM();
				pCtrl->data.bLpmMaskExist = g_pEfem->Is_MASK_InPOD();
				pCtrl->data.bLpmInitComp = g_pEfem->Is_LPM_Initialized();
				pCtrl->data.bLpmOpened = g_pEfem->Is_POD_Open();
				pCtrl->data.bLpmWorking = g_pEfem->Is_LPM_Working();
				pCtrl->data.bLpmError = g_pEfem->Is_LPM_Error();

				pCtrl->data.bAtrMaskExist = g_pEfem->Is_MASK_OnMTSRobot();
				pCtrl->data.bAtrInitComp = g_pEfem->Is_ATR_Initialized();
				pCtrl->data.bAtrWorking = g_pEfem->Is_ATR_Working();
				pCtrl->data.bAtrError = g_pEfem->Is_ATR_Error();

				pCtrl->data.bRotatorMaskExist = g_pEfem->Is_MASK_InRotator();
				pCtrl->data.bRotatorInitComp = g_pEfem->Is_Rotator_Initialized();
				pCtrl->data.bRotatorWorking = g_pEfem->Is_Rotator_Working();
				pCtrl->data.bRotatorError = g_pEfem->Is_Rotator_Error();
			}

			if (g_pVacuumRobot != NULL)
			{
				pCtrl->data.bConnVmtr = g_pVacuumRobot->Is_VMTR_Connected();

				pCtrl->data.bVmtrServoOn = g_pVacuumRobot->Is_VMTR_ServoOn();
				pCtrl->data.bVmtrInitComp = g_pVacuumRobot->Is_VMTR_Home();
				pCtrl->data.bVmtrWorking = g_pVacuumRobot->Is_VMTR_Working();

				pCtrl->data.bVmtrError = g_pVacuumRobot->Is_VMTR_Error();
				pCtrl->data.bVmtrEMO = g_pVacuumRobot->Is_VMTR_EMO();

				pCtrl->data.dVmtrFeedbackPosT = g_pVacuumRobot->Get_Feedback_PosT();
				pCtrl->data.dVmtrFeedbackPosL = g_pVacuumRobot->Get_Feedback_PosL();
				pCtrl->data.dVmtrFeedbackPosZ = g_pVacuumRobot->Get_Feedback_PosZ();
			}

			if (g_pCamera != NULL)
			{
				pCtrl->data.bConnCam1 = g_pCamera->Is_CAM_Connected();
			}
			
			if (g_pNavigationStage != NULL)
			{
				pCtrl->data.bConnNaviStage = g_pNavigationStage->Is_NAVI_Stage_Connected();

				pCtrl->data.bNaviLoadingPos = g_pNavigationStage->Is_Loading_Positioin();

				CString strX;
				CString strY;
				strX.Format("%.3f", g_pNavigationStage->GetCurrentPosX());
				strY.Format("%.3f", g_pNavigationStage->GetCurrentPosY());
				strcpy(pCtrl->data.sNaviFeedbackPosX, strX);
				strcpy(pCtrl->data.sNaviFeedbackPosY, strY);
			}
			
			if (g_pFilterStage != NULL)
			{
				pCtrl->data.bConnFilterStage = g_pFilterStage->Is_FILTER_Stage_Connected();
			}

			if (g_pEUVSource != NULL)
			{
				CString strTemp;
				strTemp.Format(_T("%.2e"), g_pEUVSource->Get_Main_VacuumRate());
				strcpy(pCtrl->data.sSrcMcVacuumRate, strTemp);

				strTemp.Format(_T("%.2e"), g_pEUVSource->Get_Sub_VacuumRate());
				strcpy(pCtrl->data.sSrcScVacuumRate, strTemp);

				strTemp.Format(_T("%.2e"), g_pEUVSource->Get_Buf_VacuumRate());
				strcpy(pCtrl->data.sSrcBcVacuumRate, strTemp);

				strTemp.Format(_T("%.5f"), g_pEUVSource->Get_Stage_Position());
				strcpy(pCtrl->data.sSrcMirrorStagePos, strTemp);

				pCtrl->data.bConnSourcePc = g_pEUVSource->Is_SRC_Connected();
				pCtrl->data.bEuvLaserShutterOpened = g_pEUVSource->Is_LaserShutter_Opened();
				pCtrl->data.bEuvOn = g_pEUVSource->Is_EUV_On();
				pCtrl->data.bSrcMechShutterOpened = g_pEUVSource->Is_Shutter_Opened();
			}
			
			if (g_pGauge_IO != NULL)
			{
				pCtrl->data.bConnVacuumGauge = g_pGauge_IO->Is_GAUGE_Connected();

				sprintf(pCtrl->data.sLlcVacuumRate, _T("%.2e"), g_pGauge_IO->GetLlcVacuumRate());
				sprintf(pCtrl->data.sMcVacuumRate, _T("%.2e"), g_pGauge_IO->GetMcVacuumRate());
			}

			if (g_pLLCTmp_IO != NULL)
			{
				pCtrl->data.bConnLlcTmp = g_pLLCTmp_IO->Is_LLC_Tmp_Connected();
				pCtrl->data.bLlcTmpWorking = g_pLLCTmp_IO->GetLlcTmpState() == g_pLLCTmp_IO->Running ? true : false;
				pCtrl->data.bLlcTmpError = g_pLLCTmp_IO->GetLlcTmpErrorCode() != g_pLLCTmp_IO->NoError ? true : false;
			}

			if (g_pMCTmp_IO != NULL)
			{
				pCtrl->data.bConnMcTmp = g_pMCTmp_IO->Is_MC_Tmp_Connected();
				pCtrl->data.bMcTmpWorking = g_pMCTmp_IO->GetMcTmpState() == g_pMCTmp_IO->Running ? true : false;
				pCtrl->data.bMcTmpError = g_pMCTmp_IO->GetMcTmpErrorCode() == 0 ? true : false;
			}

			pCtrl->data.bConnAfm = FALSE;
			pCtrl->data.bConnRevolver = FALSE;

			//if (g_pXrayCamera != NULL)
			//{
			//	pCtrl->data.bConnXrayCamera = g_pXrayCamera->Is_XRAY_Connected();
			//}
			
			if (g_pMaskMap != NULL)
			{
				pCtrl->data.bSequenceWorking = g_pMaskMap->Is_SEQ_Working();
			}

			pCtrl->data.nMaskLocation = g_pConfig->m_nMaterialLocation;

			pCtrl->SendData(pCtrl->data.message_size, (char*)&pCtrl->data);
		}

		Sleep(500);
	}

	return 0;
}


