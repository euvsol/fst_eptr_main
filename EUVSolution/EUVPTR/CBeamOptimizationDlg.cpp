﻿// CSqOneOptimizationDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"



// CSqOneOptimizationDlg 대화 상자

IMPLEMENT_DYNAMIC(CBeamOptimizationDlg, CDialogEx)

CBeamOptimizationDlg::CBeamOptimizationDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_BEAMOPTIMIZATION_DIALOG, pParent)
{
	m_nOptimizationIteration = 0;
	m_isOptimization = FALSE;

	m_dOptimizationParameterX = 0.0;
	m_dOptimizationParameterY = 0.0;
	m_dOptimizationParameterZ = 0.0;
	m_dOptimizationParameterRx = 0.0;
	m_dOptimizationParameterRy = 0.0;
	m_dOptimizationParameterRz = 0.0;
	m_dOptimizationParameterCx = 0.0;
	m_dOptimizationParameterCy = 0.0;
	m_dOptimizationParameterCz = 0.0;
	m_dOptimizationParameterIntensity = 0.0;
}

CBeamOptimizationDlg::~CBeamOptimizationDlg()
{
}

void CBeamOptimizationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CBeamOptimizationDlg, CDialogEx)
ON_BN_CLICKED(IDC_BUTTON_BEAMOPTIMIZATION_RUNOPTIMIZATIOIN, &CBeamOptimizationDlg::OnBnClickedButtonSqoptimizationRunoptimizatioin)
ON_BN_CLICKED(IDC_BUTTON_BEAMOPTIMIZATION_SETOPTIMIZATIOIN, &CBeamOptimizationDlg::OnBnClickedButtonSqoptimizationSetoptimizatioin)
END_MESSAGE_MAP()


// CSqOneOptimizationDlg 메시지 처리기
int CBeamOptimizationDlg::cacGradient(double * rad)
{
	int nRet = 0, tempRet = 0;
	CString temp = "";
	double step[4] = { 0.1, 0.1, 0.1, 0.1 };

	double value[2] = { 0 , 0 };
	double horizontal = 0, vertical = 0, pitch = 0, yaw = 0;
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_HORIZONTAL, temp);
	step[0] = atof(_T(temp));
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_VERTICAL, temp);
	step[1] = atof(_T(temp));
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_PITCH, temp);
	step[2] = atof(_T(temp));
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_YAW, temp);
	step[3] = atof(_T(temp));
	if (!g_pSqOneControl->m_bStopFlag)
	{
		for (int i = 0; i < 4; i++)
		{
			horizontal = m_dOptimizationParameterX;
			vertical = m_dOptimizationParameterY;
			pitch = m_dOptimizationParameterRx;
			yaw = m_dOptimizationParameterRy;
			if (i == 0)
			{
				horizontal = m_dOptimizationParameterX - step[i];
			}
			else if (i == 1)
			{
				vertical = m_dOptimizationParameterY - step[i];
			}
			else if (i == 2)
			{
				pitch = m_dOptimizationParameterRx - step[2];
			}
			else
			{
				yaw = m_dOptimizationParameterRy - step[3];
			}

			tempRet = g_pSqOneControl->checkLimit(horizontal, vertical, m_dOptimizationParameterZ, pitch, yaw, m_dOptimizationParameterRz);
			if (tempRet != 0)
			{
				nRet = -1;
				break;
			}
			tempRet = g_pSqOneControl->waitUntilMoveComplete(horizontal, vertical, m_dOptimizationParameterZ, pitch, yaw, m_dOptimizationParameterRz, m_dOptimizationParameterCx, m_dOptimizationParameterCy, m_dOptimizationParameterCz);
			if (tempRet != 0)
			{
				nRet = -2;
				break;
			}
			if (g_pSqOneControl->IsDlgButtonChecked(IDC_CHECK_SQONE_SIMULATOR) == TRUE)
			{
				g_pBeamMain->caculateIntensty();
			}
			value[0] = g_pBeamMain->m_dIntensity;

			horizontal = m_dOptimizationParameterX;
			vertical = m_dOptimizationParameterY;
			pitch = m_dOptimizationParameterRx;
			yaw = m_dOptimizationParameterRy;
			if (i == 0)
			{
				horizontal = m_dOptimizationParameterX + step[i];
			}
			else if (i == 1)
			{
				vertical = m_dOptimizationParameterY + step[i];
			}
			else if (i == 2)
			{
				pitch = m_dOptimizationParameterRx + step[2];
			}
			else
			{
				yaw = m_dOptimizationParameterRy + step[3];
			}

			tempRet = g_pSqOneControl->checkLimit(horizontal, vertical, m_dOptimizationParameterZ, pitch, yaw, m_dOptimizationParameterRz);
			if (tempRet != 0)
			{
				nRet = -1;
				break;
			}
			tempRet = g_pSqOneControl->waitUntilMoveComplete(horizontal, vertical, m_dOptimizationParameterZ, pitch, yaw, m_dOptimizationParameterRz, m_dOptimizationParameterCx, m_dOptimizationParameterCy, m_dOptimizationParameterCz);
			if (tempRet != 0)
			{
				nRet = -2;
				break;
			}
			if (g_pSqOneControl->IsDlgButtonChecked(IDC_CHECK_SQONE_SIMULATOR) == TRUE)
			{
				g_pBeamMain->caculateIntensty();
			}
			value[1] = g_pBeamMain->m_dIntensity;

			rad[i] = (value[1] - value[0]) / (2 * step[i]);
		}
	}

	return nRet;
}

int CBeamOptimizationDlg::cac2DGradient(double * rad)
{
	int nRet = 0, tempRet = 0;
	CString temp = "";
	double step[2] = { 0.1, 0.1};

	double value[2] = { 0 , 0 };
	double horizontal = 0, vertical = 0, pitch = 0, yaw = 0;
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_PITCH, temp);
	step[0] = atof(_T(temp));
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_YAW, temp);
	step[1] = atof(_T(temp));
	if (!g_pSqOneControl->m_bStopFlag)
	{
		for (int i = 0; i < 2; i++)
		{
			horizontal = m_dOptimizationParameterX;
			vertical = m_dOptimizationParameterY;
			pitch = m_dOptimizationParameterRx;
			yaw = m_dOptimizationParameterRy;
			if (i == 0)
			{
				pitch = m_dOptimizationParameterRx - step[i];
			}
			else if (i == 1)
			{
				yaw = m_dOptimizationParameterRy - step[i];
			}

			tempRet = g_pSqOneControl->checkLimit(horizontal, vertical, m_dOptimizationParameterZ, pitch, yaw, m_dOptimizationParameterRz);
			if (tempRet != 0)
			{
				nRet = -1;
				break;
			}
			tempRet = g_pSqOneControl->waitUntilMoveComplete(horizontal, vertical, m_dOptimizationParameterZ, pitch, yaw, m_dOptimizationParameterRz, m_dOptimizationParameterCx, m_dOptimizationParameterCy, m_dOptimizationParameterCz);
			if (tempRet != 0)
			{
				nRet = -2;
				break;
			}
			if (g_pSqOneControl->IsDlgButtonChecked(IDC_CHECK_SQONE_SIMULATOR) == TRUE)
			{
				g_pBeamMain->caculateIntensty();
			}
			else
			{
				g_pBeamMain->getIntensityfromADAM();
			}
			value[0] = g_pBeamMain->m_dIntensity;

			horizontal = m_dOptimizationParameterX;
			vertical = m_dOptimizationParameterY;
			pitch = m_dOptimizationParameterRx;
			yaw = m_dOptimizationParameterRy;
			if (i == 0)
			{
				pitch = m_dOptimizationParameterRx + step[i];
			}
			else if (i == 1)
			{
				yaw = m_dOptimizationParameterRy + step[i];
			}

			tempRet = g_pSqOneControl->checkLimit(horizontal, vertical, m_dOptimizationParameterZ, pitch, yaw, m_dOptimizationParameterRz);
			if (tempRet != 0)
			{
				nRet = -1;
				break;
			}
			tempRet = g_pSqOneControl->waitUntilMoveComplete(horizontal, vertical, m_dOptimizationParameterZ, pitch, yaw, m_dOptimizationParameterRz, m_dOptimizationParameterCx, m_dOptimizationParameterCy, m_dOptimizationParameterCz);
			if (tempRet != 0)
			{
				nRet = -2;
				break;
			}
			if (g_pSqOneControl->IsDlgButtonChecked(IDC_CHECK_SQONE_SIMULATOR) == TRUE)
			{
				g_pBeamMain->caculateIntensty();
			}
			else
			{
				g_pBeamMain->getIntensityfromADAM();
			}
			value[1] = g_pBeamMain->m_dIntensity;

			rad[i] = (value[1] - value[0]) / (2 * step[i]);
		}
	}

	return nRet;
}

void CBeamOptimizationDlg::runOptimization()
{
	CString strLog = "";
	strLog.Format("Begin(Optimization) \r\n Initial Position : %02.4f, %02.4f, %02.4f, %02.4f, %02.4f, %02.4f", m_dOptimizationParameterX, m_dOptimizationParameterY, m_dOptimizationParameterZ, m_dOptimizationParameterRx, m_dOptimizationParameterRy, m_dOptimizationParameterRz);
	g_pBeamMain->Display(0, strLog);

	int nRet = 0, tempRet = 0;
	CString temp = "";
	double dTolerance = 1e-12;
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_MINTOLERENCE, temp);
	dTolerance = atof(_T(temp));
	double dpermin = 1e-6;
	int maxiter = 1000;
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_MAXITERATION, temp);
	maxiter = atof(_T(temp));

	double alpha[4] = { 1, 1, 1, 1 };
	double dgradient[4] = { 0,0,0,0 };
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_HORIZONTAL, temp);
	alpha[0] = atof(_T(temp));
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_VERTICAL, temp);
	alpha[1] = atof(_T(temp));
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_PITCH, temp);
	alpha[2] = atof(_T(temp));
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_YAW, temp);
	alpha[3] = atof(_T(temp));

	if (g_pSqOneControl->m_bStopFlag == FALSE)
	{
		m_isOptimization = TRUE;
		for (m_nOptimizationIteration = 0; m_nOptimizationIteration < maxiter; m_nOptimizationIteration++)
		{

			CString logOptimization = "";
			logOptimization.Format(_T("Optimization Num  = %03d, Intensity = %0.4f Current Position: %02.4f, %02.4f, %02.4f, %02.4f, %02.4f, %02.4f"), m_nOptimizationIteration, m_dOptimizationParameterIntensity, m_dOptimizationParameterX, m_dOptimizationParameterY, m_dOptimizationParameterZ, m_dOptimizationParameterRx, m_dOptimizationParameterRy, m_dOptimizationParameterRz);
			g_pBeamMain->Display(0, logOptimization);
			if (g_pBeamAutoAlign->m_isAutoAlignOn = TRUE)
			{
				CString strTemp = "";
				g_pBeamAutoAlign->GetDlgItemTextA(IDC_EDIT_BEAMAUTOALIGN_HORIZONTAL_OPTIMIZATIONSTEP, strTemp);
				g_pBeamOptimization->SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_HORIZONTAL, strTemp);
				g_pBeamAutoAlign->GetDlgItemTextA(IDC_EDIT_BEAMAUTOALIGN_HORIZONTAL_DIFFERENTIALSTEP, strTemp);
				g_pBeamOptimization->SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_HORIZONTAL, strTemp);
				g_pBeamAutoAlign->GetDlgItemTextA(IDC_EDIT_BEAMAUTOALIGN_VERTICAL_OPTIMIZATIONSTEP, strTemp);
				g_pBeamOptimization->SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_VERTICAL, strTemp);
				g_pBeamAutoAlign->GetDlgItemTextA(IDC_EDIT_BEAMAUTOALIGN_VERTICAL_DIFFERENTIALSTEP, strTemp);
				g_pBeamOptimization->SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_VERTICAL, strTemp);
				g_pBeamAutoAlign->GetDlgItemTextA(IDC_EDIT_BEAMAUTOALIGN_PITCH_OPTIMIZATIONSTEP, strTemp);
				g_pBeamOptimization->SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_PITCH, strTemp);
				g_pBeamAutoAlign->GetDlgItemTextA(IDC_EDIT_BEAMAUTOALIGN_PITCH_DIFFERENTIALSTEP, strTemp);
				g_pBeamOptimization->SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_PITCH, strTemp);
				g_pBeamAutoAlign->GetDlgItemTextA(IDC_EDIT_BEAMAUTOALIGN_YAW_OPTIMIZATIONSTEP, strTemp);
				g_pBeamOptimization->SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_YAW, strTemp);
				g_pBeamAutoAlign->GetDlgItemTextA(IDC_EDIT_BEAMAUTOALIGN_YAW_DIFFERENTIALSTEP, strTemp);
				g_pBeamOptimization->SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_YAW, strTemp);
			}
			GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_HORIZONTAL, temp);
			alpha[0] = atof(_T(temp));
			GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_VERTICAL, temp);
			alpha[1] = atof(_T(temp));
			GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_PITCH, temp);
			alpha[2] = atof(_T(temp));
			GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_YAW, temp);
			alpha[3] = atof(_T(temp));

			tempRet = cacGradient(dgradient);
			if (tempRet != 0)
			{
				if (tempRet == -1)
				{
					nRet = -1;
				}
				else
				{
					nRet = -2;
				}
				break;
			}

			m_dOptimizationParameterX += alpha[0] * dgradient[0];
			m_dOptimizationParameterY += alpha[1] * dgradient[1];
			m_dOptimizationParameterRx += alpha[2] * dgradient[2];
			m_dOptimizationParameterRy += alpha[3] * dgradient[3];
			int tempRet = g_pSqOneControl->checkLimit(m_dOptimizationParameterX, m_dOptimizationParameterY, m_dOptimizationParameterZ, m_dOptimizationParameterRx, m_dOptimizationParameterRy, m_dOptimizationParameterRz);
			if (tempRet != 0)
			{
				nRet = -1;
				break;
			}
			if (g_pSqOneControl->m_bStopFlag == FALSE)
			{
				tempRet = g_pSqOneControl->waitUntilMoveComplete(m_dOptimizationParameterX, m_dOptimizationParameterY, m_dOptimizationParameterZ, m_dOptimizationParameterRx, m_dOptimizationParameterRy, m_dOptimizationParameterRz, m_dOptimizationParameterCx, m_dOptimizationParameterCy, m_dOptimizationParameterCz);
				if (tempRet != 0)
				{
					nRet = -2;
					break;
				}
				if (g_pSqOneControl->IsDlgButtonChecked(IDC_CHECK_SQONE_SIMULATOR) == TRUE)
				{
					g_pBeamMain->caculateIntensty();
				}
				if (abs(m_dOptimizationParameterIntensity - g_pBeamMain->m_dIntensity) < dTolerance)
				{
					nRet = 1;
					break;
				}
				else
				{
					//OptimizationParameter[9] = g_pAdam->AdamData.m_dDetector1;
					m_dOptimizationParameterIntensity = g_pBeamMain->m_dIntensity;
					if (g_pBeamAutoAlign->m_isAutoAlignOn)
					{
						g_pBeamAutoAlign->shiftData(g_pBeamAutoAlign->dataHorizontal, sampleSize, g_pSqOneControl->m_SQOneParseData.X);
						g_pBeamAutoAlign->shiftData(g_pBeamAutoAlign->dataVertical, sampleSize, g_pSqOneControl->m_SQOneParseData.Y);
						g_pBeamAutoAlign->shiftData(g_pBeamAutoAlign->dataPitch, sampleSize, g_pSqOneControl->m_SQOneParseData.Rx);
						g_pBeamAutoAlign->shiftData(g_pBeamAutoAlign->dataYaw, sampleSize, g_pSqOneControl->m_SQOneParseData.Ry);
						g_pBeamAutoAlign->shiftData(g_pBeamAutoAlign->dataIntensity, sampleSize, g_pBeamMain->m_dIntensity);
						g_pBeamAutoAlign->shiftData(g_pBeamAutoAlign->dataIteration, sampleSize, g_pBeamOptimization->m_nOptimizationIteration);
						g_pBeamAutoAlign->m_IntensityChartVIewer.updateViewPort(true, false);
					}
				}
			}
			nRet = 2;
		}
	}

	if (nRet == -1)
	{
		g_pBeamMain->Display(0, "End Optimization : Divergence Error ");
		::AfxMessageBox("Divergence Error");
	}
	else if (nRet == -2)
	{
		g_pBeamMain->Display(0, "End Optimization : SqOne Move Error");
 		::AfxMessageBox("SqOne Move Error");
	}
	else if(nRet == 1 )
	{
		g_pBeamMain->Display(0, "End Optimization : Reach Min Tolerence");
		::AfxMessageBox("End Optimization : Reach Min Tolerence");
	}
	else if (nRet == 2)
	{
		g_pBeamMain->Display(0, "Optimization End : Reach Max Iteration");
		::AfxMessageBox("End Optimization : Reach Max Iteration");
	}

	GetDlgItem(IDC_BUTTON_BEAMOPTIMIZATION_RUNOPTIMIZATIOIN)->EnableWindow(FALSE);
	m_isOptimization = FALSE;
	return;
}

void CBeamOptimizationDlg::run2DOptimization()
{
	CString strLog = "";
	strLog.Format("Begin(Optimization) \r\n Initial Position : %02.4f, %02.4f, %02.4f, %02.4f, %02.4f, %02.4f", m_dOptimizationParameterX, m_dOptimizationParameterY, m_dOptimizationParameterZ, m_dOptimizationParameterRx, m_dOptimizationParameterRy, m_dOptimizationParameterRz);
	g_pBeamMain->Display(0, strLog);

	int nRet = 0, tempRet = 0;
	CString temp = "";
	double dTolerance = 1e-12;
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_MINTOLERENCE, temp);
	dTolerance = atof(_T(temp));
	double dpermin = 1e-6;
	int maxiter = 1000;
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_MAXITERATION, temp);
	maxiter = atof(_T(temp));

	double alpha[2] = { 1, 1 };
	double dgradient[2] = { 0 , 0 };
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_PITCH, temp);
	alpha[0] = atof(_T(temp));
	GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_YAW, temp);
	alpha[1] = atof(_T(temp));

	if (g_pSqOneControl->m_bStopFlag == FALSE)
	{
		m_isOptimization = TRUE;
		for (m_nOptimizationIteration = 0; m_nOptimizationIteration < maxiter; m_nOptimizationIteration++)
		{
			if (g_pBeam2DAutoAlign->m_isAutoAlignOn = TRUE)
			{
				CString strTemp = "";
				g_pBeam2DAutoAlign->GetDlgItemTextA(IDC_EDIT_BEAMAUTOALIGN_PITCH_OPTIMIZATIONSTEP, strTemp);
				g_pBeamOptimization->SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_PITCH, strTemp);
				g_pBeam2DAutoAlign->GetDlgItemTextA(IDC_EDIT_BEAMAUTOALIGN_PITCH_DIFFERENTIALSTEP, strTemp);
				g_pBeamOptimization->SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_PITCH, strTemp);
				g_pBeam2DAutoAlign->GetDlgItemTextA(IDC_EDIT_BEAMAUTOALIGN_YAW_OPTIMIZATIONSTEP, strTemp);
				g_pBeamOptimization->SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_YAW, strTemp);
				g_pBeam2DAutoAlign->GetDlgItemTextA(IDC_EDIT_BEAMAUTOALIGN_YAW_DIFFERENTIALSTEP, strTemp);
				g_pBeamOptimization->SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_YAW, strTemp);
			}
			GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_PITCH, temp);
			alpha[0] = atof(_T(temp));
			GetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_YAW, temp);
			alpha[1] = atof(_T(temp));

			tempRet = cac2DGradient(dgradient);
			if (tempRet != 0)
			{
				if (tempRet == -1)
				{
					nRet = -1;
				}
				else
				{
					nRet = -2;
				}
				break;
			}

			m_dOptimizationParameterRx += alpha[0] * dgradient[0];
			m_dOptimizationParameterRy += alpha[1] * dgradient[1];
			int tempRet = g_pSqOneControl->checkLimit(m_dOptimizationParameterX, m_dOptimizationParameterY, m_dOptimizationParameterZ, m_dOptimizationParameterRx, m_dOptimizationParameterRy, m_dOptimizationParameterRz);
			if (tempRet != 0)
			{
				nRet = -1;
				break;
			}
			if (g_pSqOneControl->m_bStopFlag == FALSE)
			{
				tempRet = g_pSqOneControl->waitUntilMoveComplete(m_dOptimizationParameterX, m_dOptimizationParameterY, m_dOptimizationParameterZ, m_dOptimizationParameterRx, m_dOptimizationParameterRy, m_dOptimizationParameterRz, m_dOptimizationParameterCx, m_dOptimizationParameterCy, m_dOptimizationParameterCz);
				if (tempRet != 0)
				{
					nRet = -2;
					break;
				}
				if (g_pSqOneControl->IsDlgButtonChecked(IDC_CHECK_SQONE_SIMULATOR) == TRUE)
				{
					g_pBeamMain->caculateIntensty();
				}
				else
				{
					g_pBeamMain->getIntensityfromADAM();
				}
				if (abs(m_dOptimizationParameterIntensity - g_pBeamMain->m_dIntensity) < dTolerance)
				{
					nRet = 1;
					break;
				}
				else
				{
					//OptimizationParameter[9] = g_pAdam->AdamData.m_dDetector1;
					m_dOptimizationParameterIntensity = g_pBeamMain->m_dIntensity;
					CString logOptimization = "";
					logOptimization.Format(_T("Optimization Num  = %03d, Intensity = %0.4f Current Position: %02.4f, %02.4f, %02.4f, %02.4f, %02.4f, %02.4f"), m_nOptimizationIteration, m_dOptimizationParameterIntensity, m_dOptimizationParameterX, m_dOptimizationParameterY, m_dOptimizationParameterZ, m_dOptimizationParameterRx, m_dOptimizationParameterRy, m_dOptimizationParameterRz);
					g_pBeamMain->Display(0, logOptimization);
					if (g_pBeam2DAutoAlign->m_isAutoAlignOn)
					{
						g_pBeam2DAutoAlign->shiftData(g_pBeam2DAutoAlign->dataHorizontal, sampleSize, g_pSqOneControl->m_SQOneParseData.X);
						g_pBeam2DAutoAlign->shiftData(g_pBeam2DAutoAlign->dataVertical, sampleSize, g_pSqOneControl->m_SQOneParseData.Y);
						g_pBeam2DAutoAlign->shiftData(g_pBeam2DAutoAlign->dataPitch, sampleSize, g_pSqOneControl->m_SQOneParseData.Rx);
						g_pBeam2DAutoAlign->shiftData(g_pBeam2DAutoAlign->dataYaw, sampleSize, g_pSqOneControl->m_SQOneParseData.Ry);
						g_pBeam2DAutoAlign->shiftData(g_pBeam2DAutoAlign->dataIntensity, sampleSize, g_pBeamMain->m_dIntensity);
						g_pBeam2DAutoAlign->shiftData(g_pBeam2DAutoAlign->dataIteration, sampleSize, g_pBeamOptimization->m_nOptimizationIteration);
						g_pBeam2DAutoAlign->m_IntensityChartVIewer.updateViewPort(true, false);
					}
				}
				nRet = 2;
			}
			else
			{
				nRet = -3;
			}
		}
	}

	if (nRet == -1)
	{
		g_pBeamMain->Display(0, "End Optimization : Divergence Error ");
		::AfxMessageBox("Divergence Error");
	}
	else if (nRet == -2)
	{
		g_pBeamMain->Display(0, "End Optimization : SqOne Move Error");
		::AfxMessageBox("SqOne Move Error");
	}
	else if (nRet == 1)
	{
		g_pBeamMain->Display(0, "End Optimization : Reach Min Tolerence");
		::AfxMessageBox("End Optimization : Reach Min Tolerence");
	}
	else if (nRet == 2)
	{
		g_pBeamMain->Display(0, "Optimization End : Reach Max Iteration");
		::AfxMessageBox("End Optimization : Reach Max Iteration");
	}

	GetDlgItem(IDC_BUTTON_BEAMOPTIMIZATION_RUNOPTIMIZATIOIN)->EnableWindow(FALSE);
	m_isOptimization = FALSE;
	return;
}

BOOL CBeamOptimizationDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_HORIZONTAL, "0.1");
	SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_VERTICAL, "0.1");
	SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_YAW, "0.1");
	SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_OPTIMIZATIONSTEPSIZE_PITCH, "0.1");
	SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_MAXITERATION, "1000");
	SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_MINTOLERENCE, "1e-6");
	SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_HORIZONTAL, "0.01");
	SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_VERTICAL, "0.01");
	SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_YAW, "0.01");
	SetDlgItemTextA(IDC_EDIT_BEAMOPTIMIZATION_DIFFRENTIALSTEPSIZE_PITCH, "0.01");
	((CButton*)GetDlgItem(IDC_RADIO_BEAMOPTIMIZATION_DIFFERENTIAL_FRONTANDREAR))->SetCheck(TRUE);
	GetDlgItem(IDC_BUTTON_BEAMOPTIMIZATION_RUNOPTIMIZATIOIN)->EnableWindow(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CBeamOptimizationDlg::OnBnClickedButtonSqoptimizationRunoptimizatioin()
{
	if (g_pSqOneControl->m_bConnected)
	{
		g_pBeamMain->Display(0, "Run Optimization Button Clikcked");
		runOptimization();
	}
	else
	{
		g_pBeamMain->Display(0, "Run Optimization Button Clikcked");
		g_pBeamMain->Display(0, "Communication Status Check!");
	}
}

void CBeamOptimizationDlg::OnBnClickedButtonSqoptimizationSetoptimizatioin()
{
	g_pBeamMain->setOptimizationInitial();
}

BOOL CBeamOptimizationDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}
