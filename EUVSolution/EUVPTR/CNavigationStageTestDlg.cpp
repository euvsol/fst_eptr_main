﻿// CNavigationStageTestDlg.cpp: 구현 파일
//
#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include <iomanip>
#include <vector>
// CNavigationStageTestDlg 대화 상자

#define  STAGE_CHECK_DATA_SIZE  50

IMPLEMENT_DYNAMIC(CNavigationStageTestDlg, CDialogEx)

CNavigationStageTestDlg::CNavigationStageTestDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_NAVIGATION_STAGE_TEST_DIALOG, pParent)
{

}

CNavigationStageTestDlg::~CNavigationStageTestDlg()
{
	CloseHandle(StageTestEvent);

	if (m_pFollowingErrorCheckThread != NULL)
	{
		m_bFollowingTestThreadStopFlag = TRUE;
		if (WaitForSingleObject(m_pFollowingErrorCheckThread->m_hThread, 5000) != WAIT_OBJECT_0)
		{
			::TerminateThread(m_pFollowingErrorCheckThread->m_hThread, 0);
		}
	}

	if (m_pFollowingErrorStageMoveThread != NULL)
	{
		if (WaitForSingleObject(m_pFollowingErrorStageMoveThread->m_hThread, 5000) != WAIT_OBJECT_0)
		{
			::TerminateThread(m_pFollowingErrorStageMoveThread->m_hThread, 0);
		}
	}
}
void CNavigationStageTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_VEL, m_comboxvelo);
	DDX_Control(pDX, IDC_EDIT_DEFECTCOORDX_TEST, m_Defectcoordxtest_x);
	DDX_Control(pDX, IDC_EDIT_DEFECTCOORDY_TEST, m_Defectcoordxtest_y);
	DDX_Control(pDX, IDC_EDIT_STAGECOORDX_TEST, m_maskcentercoordx_test);
	DDX_Control(pDX, IDC_EDIT_STAGECOORDY_TEST, m_maskcentercoordy_test);
	DDX_Control(pDX, IDC_EDIT_ORIGINCOORDX_TEST, m_masklbmcoordx_test);
	DDX_Control(pDX, IDC_EDIT_ORIGINCOORDY_TEST, m_masklbmcoordy_test);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_0, m_error_value_0);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_1, m_error_value_1);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_2, m_error_value_2);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_3, m_error_value_3);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_4, m_error_value_4);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_5, m_error_value_5);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_6, m_error_value_6);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_7, m_error_value_7);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_8, m_error_value_8);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_9, m_error_value_9);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_10, m_error_value_10);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_11, m_error_value_11);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_12, m_error_value_12);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_13, m_error_value_13);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_14, m_error_value_14);
	DDX_Control(pDX, IDC_EDIT_ERROR_VALUE_15, m_error_value_15);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_0, m_avg_value_0);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_1, m_avg_value_1);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_2, m_avg_value_2);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_3, m_avg_value_3);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_4, m_avg_value_4);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_5, m_avg_value_5);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_6, m_avg_value_6);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_7, m_avg_value_7);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_8, m_avg_value_8);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_9, m_avg_value_9);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_10, m_avg_value_10);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_11, m_avg_value_11);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_12, m_avg_value_12);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_13, m_avg_value_13);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_14, m_avg_value_14);
	DDX_Control(pDX, IDC_EDIT_AVG_VALUE_15, m_avg_value_15);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_0, m_max_value_0);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_1, m_max_value_1);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_2, m_max_value_2);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_3, m_max_value_3);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_4, m_max_value_4);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_5, m_max_value_5);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_6, m_max_value_6);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_7, m_max_value_7);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_8, m_max_value_8);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_9, m_max_value_9);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_10, m_max_value_10);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_11, m_max_value_11);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_12, m_max_value_12);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_13, m_max_value_13);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_14, m_max_value_14);
	DDX_Control(pDX, IDC_EDIT_MAX_VALUE_15, m_max_value_15);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_0, m_min_value_0);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_1, m_min_value_1);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_2, m_min_value_2);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_3, m_min_value_3);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_4, m_min_value_4);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_5, m_min_value_5);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_6, m_min_value_6);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_7, m_min_value_7);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_8, m_min_value_8);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_9, m_min_value_9);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_10, m_min_value_10);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_11, m_min_value_11);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_12, m_min_value_12);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_13, m_min_value_13);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_14, m_min_value_14);
	DDX_Control(pDX, IDC_EDIT_MIN_VALUE_15, m_min_value_15);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_0, m_edit_test_cnt_0);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_1, m_edit_test_cnt_1);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_3, m_edit_test_cnt_2);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_4, m_edit_test_cnt_3);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_5, m_edit_test_cnt_4);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_6, m_edit_test_cnt_5);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_7, m_edit_test_cnt_6);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_8, m_edit_test_cnt_7);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_9, m_edit_test_cnt_8);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_10, m_edit_test_cnt_9);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_11, m_edit_test_cnt_10);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_12, m_edit_test_cnt_11);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_13, m_edit_test_cnt_12);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_14, m_edit_test_cnt_13);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_15, m_edit_test_cnt_14);
	DDX_Control(pDX, IDC_EDIT_TEST_CNT_16, m_edit_test_cnt_15);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_0, m_edit_test_vel_0);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_1, m_edit_test_vel_1);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_2, m_edit_test_vel_2);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_3, m_edit_test_vel_3);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_4, m_edit_test_vel_4);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_5, m_edit_test_vel_5);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_6, m_edit_test_vel_6);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_7, m_edit_test_vel_7);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_8, m_edit_test_vel_8);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_9, m_edit_test_vel_9);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_10, m_edit_test_vel_10);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_11, m_edit_test_vel_11);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_12, m_edit_test_vel_12);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_13, m_edit_test_vel_13);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_14, m_edit_test_vel_14);
	DDX_Control(pDX, IDC_EDIT_TEST_VEL_15, m_edit_test_vel_15);
	DDX_Control(pDX, IDC_EDIT_ENCODER_X, m_stageencoderx_test);
	DDX_Control(pDX, IDC_EDIT_ENCODER_Y, m_stageencodery_test);
}


BEGIN_MESSAGE_MAP(CNavigationStageTestDlg, CDialogEx)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_STAGE_MEASUREMENT_START_CTL, &CNavigationStageTestDlg::OnBnClickedStageMeasurementStartCtl)
	ON_BN_CLICKED(IDC_STAGE_MEASUREMENT_STOP_CTL, &CNavigationStageTestDlg::OnBnClickedStageMeasurementStopCtl)
	ON_CBN_SELENDOK(IDC_COMBO_VEL, &CNavigationStageTestDlg::OnCbnSelendokComboVel)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_0, &CNavigationStageTestDlg::OnBnClickedBtnTestSet0)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_1, &CNavigationStageTestDlg::OnBnClickedBtnTestSet1)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_2, &CNavigationStageTestDlg::OnBnClickedBtnTestSet2)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_3, &CNavigationStageTestDlg::OnBnClickedBtnTestSet3)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_4, &CNavigationStageTestDlg::OnBnClickedBtnTestSet4)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_5, &CNavigationStageTestDlg::OnBnClickedBtnTestSet5)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_6, &CNavigationStageTestDlg::OnBnClickedBtnTestSet6)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_7, &CNavigationStageTestDlg::OnBnClickedBtnTestSet7)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_8, &CNavigationStageTestDlg::OnBnClickedBtnTestSet8)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_9, &CNavigationStageTestDlg::OnBnClickedBtnTestSet9)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_10, &CNavigationStageTestDlg::OnBnClickedBtnTestSet10)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_11, &CNavigationStageTestDlg::OnBnClickedBtnTestSet11)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_12, &CNavigationStageTestDlg::OnBnClickedBtnTestSet12)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_13, &CNavigationStageTestDlg::OnBnClickedBtnTestSet13)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_14, &CNavigationStageTestDlg::OnBnClickedBtnTestSet14)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_15, &CNavigationStageTestDlg::OnBnClickedBtnTestSet15)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_ALL, &CNavigationStageTestDlg::OnBnClickedBtnTestSetAll)
	ON_BN_CLICKED(IDC_BTN_TEST_SET_ALL2, &CNavigationStageTestDlg::OnBnClickedBtnTestSetAll2)
END_MESSAGE_MAP()



BOOL CNavigationStageTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_bNavigation_Stage_Test_Start = FALSE;

	// ICON
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);

	for (int nIdx = 0; nIdx < 16; nIdx++)
	{
		((CStatic*)GetDlgItem(IDC_ICON_0_0_POS + nIdx))->SetIcon(m_LedIcon[0]);
	}

	((CStatic*)GetDlgItem(IDC_ICON_TEST_START_STOP_ICON))->SetIcon(m_LedIcon[0]);

	StageTestEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

	m_comboxvelo.AddString(_T("2"));
	m_comboxvelo.AddString(_T("5"));
	m_comboxvelo.AddString(_T("10"));
	m_comboxvelo.AddString(_T("20"));
	
	m_comboxvelo.SetCurSel(0);
	m_nSetVelocity = 2;

	m_bFollowingTestThreadStopFlag = FALSE;
	m_pFollowingErrorCheckThread = NULL;

	m_VectrackingErrorTestPoint.clear();
	//trackingErrorTest_P1.emplace_back(0);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CNavigationStageTestDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case STAGE_MEASUREMENT_TIMER:
		SH_Stage_Check_Axis_Check();
		SetTimer(nIDEvent, 100, NULL);
		break;
	default:
		break;
	}
	CDialogEx::OnTimer(nIDEvent);
}


void CNavigationStageTestDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CNavigationStageTestDlg::SH_Stage_Check_Axis_Check()
{
	////////////////////////////////////////////////////////
	// 모니터를 위함.
	// 측정 중 Axis 계속 적으로 위치 값 모니터링
	// Stage 측정 중일때만 유효.
	///////////////////////////////////////////////////////

	double axis_data = 0.0;
	CString axis_data_str;
	CString axis_data_reset_str;
	axis_data_reset_str = _T("0.0");

	if (m_bNavigation_Stage_Test_Start)
	{
		//X POS
		axis_data = abs(g_pNavigationStage->GetPosmm(STAGE_X_AXIS));
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XPOS_CTL, axis_data_str);

		//Y POS
		axis_data = abs(g_pNavigationStage->GetPosmm(STAGE_Y_AXIS));
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YPOS_CTL, axis_data_str);

		//X LOAD
		//axis_data = abs(g_pNavigationStage->GetGlobalRealVariable("DOUT_1"));
		//axis_data_str.Format("%3.7f", axis_data);
		//SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XLOAD_CTL, axis_data_str);
		//
		////Y LOAD
		//axis_data = abs(g_pNavigationStage->GetGlobalRealVariable("DOUT_0"));
		//axis_data_str.Format("%3.7f", axis_data);
		//SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YLOAD_CTL, axis_data_str);
		//
		////X INPOS
		//axis_data = abs(g_pNavigationStage->GetGlobalRealVariable("PE1"));
		//axis_data_str.Format("%3.7e", axis_data);
		//SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XINPOS_CTL, axis_data_str);
		////
		////Y INPOS
		//axis_data = abs(g_pNavigationStage->GetGlobalRealVariable("PE0"));
		//axis_data_str.Format("%3.7e", axis_data);
		//SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YINPOS_CTL, axis_data_str);
	}
	else
	{
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XPOS, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YPOS, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XLOAD, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YLOAD, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XINPOS, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YINPOS, axis_data_reset_str);
	}
}

void CNavigationStageTestDlg::OnBnClickedStageMeasurementStartCtl()
{
	m_nNavigation_Stage_Test_Start();
	((CStatic*)GetDlgItem(IDC_ICON_TEST_START_STOP_ICON))->SetIcon(m_LedIcon[0]);
}

int CNavigationStageTestDlg::m_nNavigation_Stage_Test_Start()
{
	((CStatic*)GetDlgItem(IDC_ICON_TEST_START_STOP_ICON))->SetIcon(m_LedIcon[2]);
	CString ini_text;
	ini_text = _T("0.0");

	if (g_pNavigationStage == NULL || g_pAdam == NULL || g_pConfig == NULL)
	{
		AfxMessageBox(_T("구동 Error"));
		return -1;
	}
	if (!g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		AfxMessageBox(_T("Navigation Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return -1;
	}

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		AfxMessageBox(_T("Navigation Stage Moving Impossible!"), MB_ICONERROR);
		return -1;
	}

	/* 변수 m_Mask_Flatness_Measrue_Start_Flag -> Mask Flatness 측정 중인 경우 -> TRUE */
	/* 변수 m_PIstageManualThreadExitFlag -> PI SCan Stage Flatness 측정 중인 경우 -> FALSE */
	/* 변수 m_Navigation_Stage_Test_Start -> Navigation Stage 측정 중인 경우 -> TRUE */

	//if((g_pScanStageTest->m_Mask_Flatness_Measrue_Start_Flag) || (!g_pScanStageTest->m_PIstageManualThreadExitFlag) && m_bNavigation_Stage_Test_Start)
	//{
	//	::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
	//	GetDlgItem(IDC_CHECK_MASK_FLAT_SET_CTL)->EnableWindow(true);
	//	GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT_CTL)->EnableWindow(true);
	//	//GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);
	//	GetDlgItem(IDC_CHECK_MASK_FLAT_CTL)->EnableWindow(true);
	//	return -1;
	//}

	if (g_pNavigationStage->m_bConnect != TRUE)
	{
		AfxMessageBox(_T("Navigation Stage 연결 확인 필요"));
		return -1;
	}
	CAutoMessageDlg MsgBoxAuto(this);

	//g_pNavigationStage->RunBuffer(6);
	//MsgBoxAuto.DoModal(_T(" ACS Buffer 6 Start "), 5);

	//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(0.0, 0.0);
	//MsgBoxAuto.DoModal(_T(" Navigation Stage 원점 이동 중 "), 5);

	///////////////////////////////////////////////////
	// 측정 Thread
	///////////////////////////////////////////////////

	//m_stageThreadExitFlag = FALSE;
	//
	//if (m_pstageThread == NULL)
	//{
	//	m_pstageThread = ::AfxBeginThread(Stage_Check_Thread, this, THREAD_PRIORITY_NORMAL, 0, 0);
	//}

	g_pWarning->m_strWarningMessageVal = " Navigation Stage (SH) Measurement 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);


	if (m_nNavigation_Stage_Check() != RUN)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		::AfxMessageBox(_T("Navigation Stage 측정에 실패 하였습니다"), MB_ICONERROR);
		KillTimer(STAGE_MEASUREMENT_TIMER);
		m_bNavigation_Stage_Test_Start = FALSE;
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XPOS_CTL, ini_text);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YPOS_CTL, ini_text);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XLOAD_CTL, ini_text);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YLOAD_CTL, ini_text);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XINPOS_CTL, ini_text);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YINPOS_CTL, ini_text);
		((CStatic*)GetDlgItem(IDC_ICON_TEST_START_STOP_ICON))->SetIcon(m_LedIcon[1]);
		return -1;
	}

	//GetDlgItem(IDC_CHECK_MASK_FLAT_SET_CTL)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT_CTL)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_MASK_FLAT_CTL)->EnableWindow(true);

	m_bNavigation_Stage_Test_Start = FALSE;
	KillTimer(STAGE_MEASUREMENT_TIMER);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XPOS_CTL, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YPOS_CTL, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XLOAD_CTL, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YLOAD_CTL, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XINPOS_CTL, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YINPOS_CTL, ini_text);

	g_pWarning->ShowWindow(SW_HIDE);
	::AfxMessageBox(_T("Navigation Stage (SH) 측정이 완료 되었습니다"));
	((CStatic*)GetDlgItem(IDC_ICON_TEST_START_STOP_ICON))->SetIcon(m_LedIcon[1]);
	return 1;
}



int CNavigationStageTestDlg::m_nNavigation_Stage_Check()
{



	int cont = 0;
	int num = 0, x = 0, y = 0;
	int nRet = RUN;

	int x_step_move = 50;
	int y_step_move = 50;


	double get_xLoad_data[STAGE_CHECK_DATA_SIZE] = { 0.0, };
	double get_yLoad_data[STAGE_CHECK_DATA_SIZE] = { 0.0, };
	double get_xInPos_data[STAGE_CHECK_DATA_SIZE] = { 0.0, };
	double get_yInPos_data[STAGE_CHECK_DATA_SIZE] = { 0.0, };
	
	vector <double> Vec_Get_xLoad_data;
	vector <double> Vec_Get_yLoad_data;
	vector <double> Vec_Get_xInPos_data;
	vector <double> Vec_Get_yInPos_data;

	Vec_Get_xLoad_data.clear();
	Vec_Get_yLoad_data.clear();
	Vec_Get_xInPos_data.clear();
	Vec_Get_yInPos_data.clear();

	double XLoad = 0.0;
	double YLoad = 0.0;
	double XInPos = 0.0;
	double YInPos = 0.0;

	double Max_XLoad = 0.0;
	double Min_XLoad = 100.0;
	double Max_YLoad = 0.0;
	double Min_YLoad = 100.0;

	double XInposSum = 0.0;
	double YInposSum = 0.0;
	double XInposAvg = 0.0;
	double YInposAvg = 0.0;

	double XInposVariance = 0.0;
	double YInposVariance = 0.0;
	double XInposSTD = 0.0;
	double YInposSTD = 0.0;

	CString x_max_load_str;
	CString x_min_load_str;
	CString y_max_load_str;
	CString y_min_load_str;
	CString x_load_str;
	CString y_load_str;
	CString x_inpos_sub_str;
	CString y_inpos_sub_str;

	CString x_inpos_str;
	CString y_inpos_str;
	CString x_inpos_str_avg;
	CString y_inpos_str_avg;
	CString x_pos_str;
	CString y_pos_str;

	CString str;

	str.Empty();

	x_max_load_str.Empty();
	x_min_load_str.Empty();
	y_max_load_str.Empty();
	y_min_load_str.Empty();
	x_load_str.Empty();
	y_load_str.Empty();
	x_inpos_sub_str.Empty();
	y_inpos_sub_str.Empty();

	x_inpos_str.Empty();
	y_inpos_str.Empty();
	x_inpos_str_avg.Empty();
	y_inpos_str_avg.Empty();
	x_pos_str.Empty();
	y_pos_str.Empty();

	double orgin_x_pos = 0.0;
	double orgin_y_pos = 0.0;

	double x_pos = 0.0; // 현재 위치 값 읽어 상태를 전달 하는 변수
	double y_pos = 0.0;

	double x_pos_sub = 0.0; // 현재 위치 값 읽어 상태를 전달 하는 변수
	double y_pos_sub = 0.0;
	CString x_pos_sub_str;
	CString y_pos_sub_str;

	double move_x_pos;	// 실제 이동 위치 변수
	double move_y_pos;

	double X_value = 0.0;
	double Y_value = 0.0;

	//double z_pos;
	//double tx_pos;
	//double ty_pos;
	//CString z_pos_str;
	//CString tx_pos_str;
	//CString ty_pos_str;


	if (g_pNavigationStage == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pConfig == NULL)	return -1;





	//STAGE 움직임 가능 여부 확인. 
	CAutoMessageDlg MsgBoxAuto(this);

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 4);
		return nRet = -1;
	}

	

	for (int nIdx = 0; nIdx < 16; nIdx++)
	{
		((CStatic*)GetDlgItem(IDC_ICON_0_0_POS + nIdx))->SetIcon(m_LedIcon[1]);
	}



	// 측정 중 일 경우 TRUE, 측정 끝나거나 중지되면 FALSE, 
	m_bNavigation_Stage_Test_Start = TRUE;
	if (m_bNavigation_Stage_Test_Start)	SetTimer(STAGE_MEASUREMENT_TIMER, 100, NULL);

	g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
	MsgBoxAuto.DoModal(_T(" Euv TO Om Moving "), 4);
	WaitSec(2);

	///////////////////////////////////////////////////////////////
	// Z 축 원점 
	///////////////////////////////////////////////////////////////
	//g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	MsgBoxAuto.DoModal(_T(" Z 축 home Moving ! "), 4);
	WaitSec(2);
	//asdf
	//if ((int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) >= (Z_INITIAL_POS_UM - 2.0)) && (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= (Z_INITIAL_POS_UM + 2.0)))
	//{
	//	//////////////////////////////////////////////////
	//	// Stage 0,0 으로 초기화 
	//	//////////////////////////////////////////////////
	//	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(0.0, 0.0);
	//}
	//else
	//{
	//	::AfxMessageBox(" Z AXIS 원점이 아님.!");
	//	return nRet = -1;
	//}

	//g_pScanStage->MoveZAbsolute_SlowInterlock(0.0);
	//WaitSec(2);
	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
	//{
	//	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(0.0, 0.0);
	//}
	//else
	//{
	//	::AfxMessageBox(" Z AXIS 0.0 아님.!");
	//	return nRet = -1;
	//}
	//

	//CString start_x_pos_str;
	//CString start_y_pos_str;
	//
	//orgin_x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	//orgin_y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	//start_x_pos_str.Format("%3.7f", orgin_x_pos);
	//start_y_pos_str.Format("%3.7f", orgin_y_pos);
	//
	//SetDlgItemTextA(IDC_EDIT_TEST_MASK_X_START, start_x_pos_str);
	//SetDlgItemTextA(IDC_EDIT_TEST_MASK_Y_START, start_y_pos_str);

	//// 읽은 좌표값을 기준으로 측정 가능 범위 재 확인 (X, Y)
	//target_x_pos = orgin_x_pos - (10 * 15);
	//if (target_x_pos < 0)
	//{
	//	MsgBoxAuto.DoModal(_T(" 평탄도 x axis 측정 범위 벗어남 ! "), 4);
	//	return nRet = -1;
	//}
	//
	//target_y_pos = orgin_y_pos + (10 * 15);
	//if (target_y_pos > 200)
	//{
	//	MsgBoxAuto.DoModal(_T(" 평탄도 y axis 측정 범위 벗어남 ! "), 4);
	//	return nRet = -1;
	//}

	if (!m_bNavigation_Stage_Test_Start)
	{
		::AfxMessageBox(" Navigation Stage 측정 STOP 버튼 누름 ! ");
		return nRet = -1;
	}

	/////////////////////////////////////////////////////////////////////////
	// 측정 전 Z 축 충돌방지를 위한 0 위치 재확인.
	/////////////////////////////////////////////////////////////////////////

	//g_pScanStage->MoveZAbsolute_SlowInterlock(0.0);
	//WaitSec(2);
	//
	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) != 0)
	//{
	//	return nRet = -1;
	//}

	str = "Navigation Stage (SH) 측정 시작 !";
	SaveLogFile("SH_Stage_Check_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);


	y = 0;
	x = 0;

	//position = 0;
	//position_str.Format("%d", position);

	//GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM)->SetWindowTextA(position_str);
	//GetDlgItem(IDC_MASK_PRO)->SetWindowTextA(_T("측정 진행 중"));
	//
	int mon = CTime::GetCurrentTime().GetMonth();
	int day = CTime::GetCurrentTime().GetDay();
	int hour = CTime::GetCurrentTime().GetHour();
	int min = CTime::GetCurrentTime().GetMinute();

	CString datafile_sub;
	CString datafile;
	CString datafile_load;
	CString datafile_inpos;
	CString path;
	CString strThisPath;

	datafile_sub.Empty();
	datafile.Empty();
	datafile_load.Empty();
	datafile_inpos.Empty();
	path.Empty();
	strThisPath.Empty();


	path = LOG_PATH;

	if (!g_pLog->IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
	}

	CString strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	path += "\\";
	path += strDate;
	if (!g_pLog->IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
	}

	strThisPath.Format("%s\\Navigation_Stage_Measurement_Load_Inpos_%d_%d_%d_%d_start.xlsx", path, mon, day, hour, min);
	CXLEzAutomation XL(FALSE);
	XL.SetCellValue(1, 1, "SREM SH Stage Inpostion & Load Measurement Report");
	XL.SetCellValue(1, 5, "Inposition");
	XL.SetCellValue(1, 6, "Load");
	//XL.SetCellValue(2, 2, "Position X");
	//XL.SetCellValue(3, 2, "Position Y");
	//XL.SetCellValue(7, 2, "Axis 0 (Y) Inpos");
	//XL.SetCellValue(8, 2, "Axis 1 (X) Inpos");
	/////////////////////
	//좌표
	////////////////////
	XL.SetCellValue(2, 3, "0.0");
	XL.SetCellValue(3, 3, "0.0");
	XL.SetCellValue(2, 4, "Axis 0 (Y)");
	XL.SetCellValue(3, 4, "Axis 1 (X)");

	XL.SetCellValue(4, 3, "0.0");
	XL.SetCellValue(5, 3, "50.0");
	XL.SetCellValue(4, 4, "Axis 0 (Y)");
	XL.SetCellValue(5, 4, "Axis 1 (X)");

	XL.SetCellValue(6, 3, "0.0");
	XL.SetCellValue(7, 3, "100.0");
	XL.SetCellValue(6, 4, "Axis 0 (Y)");
	XL.SetCellValue(7, 4, "Axis 1 (X)");

	XL.SetCellValue(8, 3, "0.0");
	XL.SetCellValue(9, 3, "150.0");
	XL.SetCellValue(8, 4, "Axis 0 (Y)");
	XL.SetCellValue(9, 4, "Axis 1 (X)");

	XL.SetCellValue(10, 3, "100.0");
	XL.SetCellValue(11, 3, "0.0");
	XL.SetCellValue(10, 4, "Axis 0 (Y)");
	XL.SetCellValue(11, 4, "Axis 1 (X)");

	XL.SetCellValue(12, 3, "100.0)");
	XL.SetCellValue(13, 3, "50.0");
	XL.SetCellValue(12, 4, "Axis 0 (Y)");
	XL.SetCellValue(13, 4, "Axis 1 (X)");

	XL.SetCellValue(14, 3, "100.0");
	XL.SetCellValue(15, 3, "100.0");
	XL.SetCellValue(14, 4, "Axis 0 (Y)");
	XL.SetCellValue(15, 4, "Axis 1 (X)");

	XL.SetCellValue(16, 3, "100.0");
	XL.SetCellValue(17, 3, "150.0");
	XL.SetCellValue(16, 4, "Axis 0 (Y)");
	XL.SetCellValue(17, 4, "Axis 1 (X)");

	XL.SetCellValue(18, 3, "200.0");
	XL.SetCellValue(19, 3, "0.0");
	XL.SetCellValue(18, 4, "Axis 0 (Y)");
	XL.SetCellValue(19, 4, "Axis 1 (X)");

	XL.SetCellValue(20, 3, "200.0)");
	XL.SetCellValue(21, 3, "50.0)");
	XL.SetCellValue(20, 4, "Axis 0 (Y)");
	XL.SetCellValue(21, 4, "Axis 1 (X)");

	XL.SetCellValue(22, 3, "200.0)");
	XL.SetCellValue(23, 3, "100.0)");
	XL.SetCellValue(22, 4, "Axis 0 (Y)");
	XL.SetCellValue(23, 4, "Axis 1 (X)");

	XL.SetCellValue(24, 3, "200.0)");
	XL.SetCellValue(25, 3, "150.0)");
	XL.SetCellValue(24, 4, "Axis 0 (Y)");
	XL.SetCellValue(25, 4, "Axis 1 (X)");

	XL.SetCellValue(26, 3, "300.0)");
	XL.SetCellValue(27, 3, "0.0)");
	XL.SetCellValue(26, 4, "Axis 0 (Y)");
	XL.SetCellValue(27, 4, "Axis 1 (X)");

	XL.SetCellValue(28, 3, "300.0");
	XL.SetCellValue(29, 3, "50.0");
	XL.SetCellValue(28, 4, "Axis 0 (Y)");
	XL.SetCellValue(29, 4, "Axis 1 (X)");

	XL.SetCellValue(30, 3, "300.0");
	XL.SetCellValue(31, 3, "100.0");
	XL.SetCellValue(30, 4, "Axis 0 (Y)");
	XL.SetCellValue(31, 4, "Axis 1 (X)");

	XL.SetCellValue(32, 3, "300.0");
	XL.SetCellValue(33, 3, "150.0");
	XL.SetCellValue(32, 4, "Axis 0 (Y)");
	XL.SetCellValue(33, 4, "Axis 1 (X)");

	datafile_sub.Format(_T("%s\\SH_Stage_Measurement_Load_Inpos_Sub_Data_%d_%d_%d_%d.txt"), path, mon, day, hour, min);
	datafile.Format(_T("%s\\SH_Stage_Measurement_Data_%d_%d_%d_%d.txt"), path, mon, day, hour, min);
	datafile_load.Format(_T("%s\\SH_Stage_load_data.txt"), path);
	datafile_inpos.Format(_T("%s\\SH_Stage_inpos_data.txt"), path);

	std::ofstream sh_stage_measurement_load_Inpos_datafile_sub(datafile_sub);
	std::ofstream sh_stage_measurement_datafile(datafile);
	std::ofstream sh_stage_measurement_load_value(datafile_load);
	std::ofstream sh_stage_measurement_inpos_value(datafile_inpos);

	sh_stage_measurement_load_Inpos_datafile_sub << "\t" << "x_pos" << "\t" << "y_pos" << "\t" << "xLoad" << "\t" << "yLoad" << "\t" << "xInposition" << "\t" << "yInposition" << "\t" << "\n";
	sh_stage_measurement_datafile << "\t" << "x_pos" << "\t" << "y_pos" << "\t" << "xLoad_Max" << "\t" << "xLoad_Min" << "\t" << "yLoad_Max" << "\t" << "yLoad_Min" << "\t" << "xInPos" << "\t" << "yInPos" << "\n";
	sh_stage_measurement_load_value << "\t" << "x_pos" << "\t" << "y_pos" << "\t" << "xLoad_Max" << "\t" << "xLoad_Min" << "\t" << "yLoad_Max" << "\t" << "yLoad_Min" << "\n";
	sh_stage_measurement_inpos_value << "\t" << "x_pos" << "\t" << "y_pos" << "\t" << "xInPos_Avg" << "\t" << "yInPos_Avg" << "\t" << "xInPos_Std" << "\t" << "yInPos_Std" << "\n";


	if (!m_bNavigation_Stage_Test_Start)
	{
		::AfxMessageBox(" Navigation Stage 측정 STOP 버튼 누름 ! ");

		XL.SaveFileAs(strThisPath);
		XL.ReleaseExcel();

		sh_stage_measurement_datafile << std::endl;
		if (sh_stage_measurement_datafile.is_open() == true)
		{
			sh_stage_measurement_datafile.close();
		}

		sh_stage_measurement_load_value << std::endl;
		if (sh_stage_measurement_load_value.is_open() == true)
		{
			sh_stage_measurement_load_value.close();
		}

		sh_stage_measurement_inpos_value << std::endl;
		if (sh_stage_measurement_inpos_value.is_open() == true)
		{
			sh_stage_measurement_inpos_value.close();
		}

		sh_stage_measurement_load_Inpos_datafile_sub << std::endl;
		if (sh_stage_measurement_load_Inpos_datafile_sub.is_open() == true)
		{
			sh_stage_measurement_load_Inpos_datafile_sub.close();
		}
		return nRet = -1;
	}

	while (y <= Y_AXIS_0_SIZE) // Y축 이동
	{
		if (y == 0)	y_pos = orgin_y_pos;
		else y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

		while (x <= X_AXIS_1_SIZE) // X 축 이동
		{
			if (x == 0)	x_pos = orgin_x_pos;
			else x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);


			// SCAN STAGE AXIS 값 READ
			//z_pos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
			//tx_pos = g_pScanStage->m_dPIStage_GetPos[TIP_AXIS];
			//ty_pos = g_pScanStage->m_dPIStage_GetPos[TILT_AXIS];
			//z_pos_str.Format("%3.7f", z_pos);
			//tx_pos_str.Format("%3.7f", tx_pos);
			//ty_pos_str.Format("%3.7f", ty_pos);

			for (int i = 0; i < STAGE_CHECK_DATA_SIZE; i++)
			{
				get_xLoad_data[i] = 0.0;
				get_yLoad_data[i] = 0.0;
				get_xInPos_data[i] = 0.0;
				get_yInPos_data[i] = 0.0;
			}
			Vec_Get_xLoad_data.clear();
			Vec_Get_yLoad_data.clear();
			Vec_Get_xInPos_data.clear();
			Vec_Get_yInPos_data.clear();
	

			WaitSec(3);

			m_start_time = clock();
			m_finish_time = 10;
			
			while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
			{
				x_pos_sub = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
				y_pos_sub = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

				Vec_Get_xLoad_data.push_back(g_pNavigationStage->GetGlobalRealVariable("DOUT_1"));
				Vec_Get_yLoad_data.push_back(g_pNavigationStage->GetGlobalRealVariable("DOUT_0"));
				Vec_Get_xInPos_data.push_back(g_pNavigationStage->GetGlobalRealVariable("PE0"));
				Vec_Get_yInPos_data.push_back(g_pNavigationStage->GetGlobalRealVariable("PE1"));

			}

			//do
			//{
			//	WaitSec(1);
			//
			//	x_pos_sub = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
			//	y_pos_sub = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
			//	get_xLoad_data[num] = g_pNavigationStage->GetGlobalRealVariable("DOUT_1");
			//	get_yLoad_data[num] = g_pNavigationStage->GetGlobalRealVariable("DOUT_0");
			//	//get_xInPos_data[num] = abs(g_pNavigationStage->GetGlobalRealVariable("PE0"));
			//	//get_yInPos_data[num] = abs(g_pNavigationStage->GetGlobalRealVariable("PE1"));
			//	get_xInPos_data[num] = g_pNavigationStage->GetGlobalRealVariable("PE0");
			//	get_yInPos_data[num] = g_pNavigationStage->GetGlobalRealVariable("PE1");
			//
			//	x_pos_sub_str.Format("%3.7f", x_pos_sub);
			//	y_pos_sub_str.Format("%3.7f", y_pos_sub);
			//	x_load_str.Format("%3.7f", get_xLoad_data[num]);
			//	y_load_str.Format("%3.7f", get_yLoad_data[num]);
			//	x_inpos_sub_str.Format("%3.3e", get_xInPos_data[num]);
			//	y_inpos_sub_str.Format("%3.3e", get_yInPos_data[num]);
			//	num += 1;
			//
			//	sh_stage_measurement_load_Inpos_datafile_sub << std::setprecision(3) << "\t" << x_pos_sub_str << "\t" << y_pos_sub_str << "\t" << x_load_str << "\t" << y_load_str << "\t" << x_inpos_sub_str << "\t" << y_inpos_sub_str << "\n";
			//
			//} while (num < STAGE_CHECK_DATA_SIZE);
			//
			//num = 0;

			int VecDataSize = 0;

			VecDataSize = Vec_Get_xLoad_data.size();
			Max_XLoad = Vec_Get_xLoad_data.at(0);
			Min_XLoad = Vec_Get_xLoad_data.at(0);

			for (int i = 0; i < VecDataSize; i++)
			{
				if (Vec_Get_xLoad_data.at(i) > Max_XLoad) Max_XLoad = Vec_Get_xLoad_data.at(i);
				if (Vec_Get_xLoad_data.at(i) < Min_XLoad) Min_XLoad = Vec_Get_xLoad_data.at(i);
			}

			VecDataSize = Vec_Get_yLoad_data.size();
			Max_YLoad = Vec_Get_yLoad_data.at(0);
			Min_YLoad = Vec_Get_yInPos_data.at(0);

			for (int i = 0; i < VecDataSize; i++)
			{
				if (Vec_Get_yLoad_data.at(i) > Max_YLoad) Max_YLoad = Vec_Get_yLoad_data.at(i);
				if (Vec_Get_yLoad_data.at(i) > Min_YLoad) Min_YLoad = Vec_Get_yLoad_data.at(i);
			}

			//for (int i = 0; i < STAGE_CHECK_DATA_SIZE; i++)
			//{
			//	if (get_xLoad_data[i] > Max_XLoad) Max_XLoad = get_xLoad_data[i];
			//	if (get_yLoad_data[i] > Max_YLoad) Max_YLoad = get_yLoad_data[i];
			//	if (get_xLoad_data[i] < Min_XLoad) Min_XLoad = get_xLoad_data[i];
			//	if (get_yLoad_data[i] < Min_YLoad) Min_YLoad = get_yLoad_data[i];
			//}


			VecDataSize = Vec_Get_xInPos_data.size();
			for (int i = 0; i < VecDataSize; i++)
			{
				XInposSum += Vec_Get_xInPos_data.at(i);
			}
			XInposAvg = (XInposSum / VecDataSize);

			VecDataSize = Vec_Get_yInPos_data.size();
			for (int i = 0; i < VecDataSize; i++)
			{
				YInposSum += Vec_Get_yInPos_data.at(i);
			}
			YInposAvg = (YInposSum / VecDataSize);

			//for (int i = 0; i < STAGE_CHECK_DATA_SIZE; i++)
			//{
			//	XInposSum += get_xInPos_data[i];
			//	YInposSum += get_yInPos_data[i];
			//}

			//XInposAvg = (XInposSum / STAGE_CHECK_DATA_SIZE);
			//YInposAvg = (YInposSum / STAGE_CHECK_DATA_SIZE);

			XInposSum = 0.0;
			YInposSum = 0.0;

			x_inpos_str_avg.Format("%3.4e", XInposAvg);
			y_inpos_str_avg.Format("%3.4e", YInposAvg);

			VecDataSize = Vec_Get_xInPos_data.size();
			for (int i = 0; i < VecDataSize; i++)
			{
				X_value += pow((Vec_Get_xInPos_data.at(i) - XInposAvg), 2);
			}
			XInposVariance = (X_value / VecDataSize);

			VecDataSize = Vec_Get_yInPos_data.size();
			for (int i = 0; i < VecDataSize; i++)
			{
				Y_value += pow((Vec_Get_yInPos_data.at(i) - YInposAvg), 2);
			}
			YInposVariance = (Y_value / VecDataSize);

			//
			//for (int i = 0; i < STAGE_CHECK_DATA_SIZE; i++)
			//{
			//	X_value += pow((get_xInPos_data[i] - XInposAvg), 2);
			//	Y_value += pow((get_yInPos_data[i] - YInposAvg), 2);
			//}
			//
			//XInposVariance = (X_value / STAGE_CHECK_DATA_SIZE);
			//YInposVariance = (Y_value / STAGE_CHECK_DATA_SIZE);

			X_value = 0.0;
			Y_value = 0.0;

			XInposSTD = sqrt(XInposVariance);
			YInposSTD = sqrt(YInposVariance);

			x_pos_str.Format("%3.7f", x_pos);
			y_pos_str.Format("%3.7f", y_pos);

			x_max_load_str.Format("%3.7f", Max_XLoad);
			x_min_load_str.Format("%3.7f", Min_XLoad);
			y_max_load_str.Format("%3.7f", Max_YLoad);
			y_min_load_str.Format("%3.7f", Min_YLoad);
			x_inpos_str.Format("%3.4e", XInposSTD);
			y_inpos_str.Format("%3.4e", YInposSTD);

			sh_stage_measurement_datafile << std::setprecision(3) << "\t\t" << x_pos_str << "\t\t" << y_pos_str << "\t\t" << x_max_load_str << "\t\t" << x_min_load_str << "\t\t" << y_max_load_str << "\t\t" << y_min_load_str << "\t\t" << x_inpos_str << "\t\t" << y_inpos_str << "\n";
			sh_stage_measurement_load_value << std::setprecision(3) << "\t\t" << x_pos_str << "\t\t" << y_pos_str << "\t\t" << x_max_load_str << "\t\t" << x_min_load_str << "\t\t" << y_max_load_str << "\t\t" << y_min_load_str << "\n";
			sh_stage_measurement_inpos_value << std::setprecision(3) << "\t\t" << x_pos_str << "\t\t" << y_pos_str << "\t\t" << x_inpos_str_avg << "\t\t" << y_inpos_str_avg << "\t\t" << x_inpos_str << "\t\t" << y_inpos_str << "\n";

			// X = 0
			if (abs(x_pos) < 2.0)
			{
				//Y = 0 (0,0)
				if (abs(y_pos) <= 2.0)
				{
					SetDlgItemTextA(IDC_EDIT_POS_0_0_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_0_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_0_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_0_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(3, 5, x_inpos_str);
					XL.SetCellValue(2, 5, y_inpos_str);
					XL.SetCellValue(3, 6, x_max_load_str);
					XL.SetCellValue(2, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_0_0_POS))->SetIcon(m_LedIcon[2]);
				}
				// Y = 50 (0,50)
				else if ((48.0 < y_pos) && (y_pos < 52.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_0_50_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_50_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_50_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_50_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(5, 5, x_inpos_str);
					XL.SetCellValue(4, 5, y_inpos_str);
					XL.SetCellValue(5, 6, x_max_load_str);
					XL.SetCellValue(4, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_0_50_POS))->SetIcon(m_LedIcon[2]);
				}
				// Y = 100 (0,100)
				else if ((98.0 < y_pos) && (y_pos < 102.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_0_100_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_100_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_100_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_100_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(7, 5, x_inpos_str);
					XL.SetCellValue(6, 5, y_inpos_str);
					XL.SetCellValue(7, 6, x_max_load_str);
					XL.SetCellValue(6, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_0_100_POS))->SetIcon(m_LedIcon[2]);
				}
				// Y = 150 (0,150)
				else if ((148.0 < y_pos) && (y_pos < 152.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_0_150_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_150_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_150_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_150_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(9, 5, x_inpos_str);
					XL.SetCellValue(8, 5, y_inpos_str);
					XL.SetCellValue(9, 6, x_max_load_str);
					XL.SetCellValue(8, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_0_150_POS))->SetIcon(m_LedIcon[2]);
				}
			}
			else if ((98.0 < x_pos) && (x_pos < 102.0))
			{
				//Y = 0 (100,0)
				if (y_pos <= 2.0)
				{
					SetDlgItemTextA(IDC_EDIT_POS_100_0_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_0_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_0_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_0_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(11, 5, x_inpos_str);
					XL.SetCellValue(10, 5, y_inpos_str);
					XL.SetCellValue(11, 6, x_max_load_str);
					XL.SetCellValue(10, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_100_0_POS))->SetIcon(m_LedIcon[2]);
				}
				// Y = 50 (100,50)
				else if ((48.0 < y_pos) && (y_pos < 52.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_100_50_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_50_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_50_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_50_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(13, 5, x_inpos_str);
					XL.SetCellValue(12, 5, y_inpos_str);
					XL.SetCellValue(13, 6, x_max_load_str);
					XL.SetCellValue(12, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_100_50_POS))->SetIcon(m_LedIcon[2]);
				}
				// Y = 100 (100,100)
				else if ((98.0 < y_pos) && (y_pos < 101.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_100_100_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_100_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_100_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_100_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(15, 5, x_inpos_str);
					XL.SetCellValue(14, 5, y_inpos_str);
					XL.SetCellValue(15, 6, x_max_load_str);
					XL.SetCellValue(14, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_100_100_POS))->SetIcon(m_LedIcon[2]);
				}
				// Y = 150 (100,150)
				else if ((148.0 < y_pos) && (y_pos < 152.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_100_150_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_150_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_150_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_150_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(17, 5, x_inpos_str);
					XL.SetCellValue(16, 5, y_inpos_str);
					XL.SetCellValue(17, 6, x_max_load_str);
					XL.SetCellValue(16, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_100_150_POS))->SetIcon(m_LedIcon[2]);
				}
			}
			else if ((198.0 < x_pos) && (x_pos < 202.0))
			{
				//Y = 0 (200,0)
				if (y_pos < 2.0)
				{
					SetDlgItemTextA(IDC_EDIT_POS_200_0_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_0_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_0_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_0_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(19, 5, x_inpos_str);
					XL.SetCellValue(18, 5, y_inpos_str);
					XL.SetCellValue(19, 6, x_max_load_str);
					XL.SetCellValue(18, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_200_0_POS))->SetIcon(m_LedIcon[2]);
				}
				// Y = 50 (200,50)
				else if ((48.0 < y_pos) && (y_pos < 52.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_200_50_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_50_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_50_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_50_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(21, 5, x_inpos_str);
					XL.SetCellValue(20, 5, y_inpos_str);
					XL.SetCellValue(21, 6, x_max_load_str);
					XL.SetCellValue(20, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_200_50_POS))->SetIcon(m_LedIcon[2]);
				}
				// Y = 100 (200,100)
				else if ((98.0 < y_pos) && (y_pos < 102.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_200_100_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_100_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_100_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_100_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(23, 5, x_inpos_str);
					XL.SetCellValue(22, 5, y_inpos_str);
					XL.SetCellValue(23, 6, x_max_load_str);
					XL.SetCellValue(22, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_200_100_POS))->SetIcon(m_LedIcon[2]);
				}
				// Y = 150 (200,150)
				else if ((148.0 < y_pos) && (y_pos < 152.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_200_150_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_150_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_150_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_150_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(25, 5, x_inpos_str);
					XL.SetCellValue(24, 5, y_inpos_str);
					XL.SetCellValue(25, 6, x_max_load_str);
					XL.SetCellValue(24, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_200_150_POS))->SetIcon(m_LedIcon[2]);
				}
			}
			else if ((298.0 < x_pos) && (x_pos < 302.0))
			{	//Y = 0 (300,0)
				if (y_pos < 2.0)
				{
					SetDlgItemTextA(IDC_EDIT_POS_300_0_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_0_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_0_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_0_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(27, 5, x_inpos_str);
					XL.SetCellValue(26, 5, y_inpos_str);
					XL.SetCellValue(27, 6, x_max_load_str);
					XL.SetCellValue(26, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_300_0_POS))->SetIcon(m_LedIcon[2]);
				}
				// Y = 50 (300,50)
				else if ((48.0 < y_pos) && (y_pos < 52.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_300_50_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_50_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_50_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_50_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(29, 5, x_inpos_str);
					XL.SetCellValue(28, 5, y_inpos_str);
					XL.SetCellValue(29, 6, x_max_load_str);
					XL.SetCellValue(28, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_300_50_POS))->SetIcon(m_LedIcon[2]);
				}
				// Y = 100 (300,100)
				else if ((98.0 < y_pos) && (y_pos < 102.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_300_100_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_100_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_100_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_100_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(31, 5, x_inpos_str);
					XL.SetCellValue(30, 5, y_inpos_str);
					XL.SetCellValue(31, 6, x_max_load_str);
					XL.SetCellValue(30, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_300_100_POS))->SetIcon(m_LedIcon[2]);
				}
				// Y = 150 (300,150)
				else if ((148.0 < y_pos) && (y_pos < 152.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_300_150_X_LOAD_CTL, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_150_Y_LOAD_CTL, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_150_X_INPOS_CTL, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_150_Y_INPOS_CTL, y_inpos_str);
					XL.SetCellValue(33, 5, x_inpos_str);
					XL.SetCellValue(32, 5, y_inpos_str);
					XL.SetCellValue(33, 6, x_max_load_str);
					XL.SetCellValue(32, 6, y_max_load_str);
					((CStatic*)GetDlgItem(IDC_ICON_300_150_POS))->SetIcon(m_LedIcon[2]);
				}
			}
			//CString cont_str;
			//cont_str.Format("%d", cont);
			//GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM)->SetWindowTextA(cont_str);

			if (!m_bNavigation_Stage_Test_Start)
			{

				::AfxMessageBox(" Navigation Stage 측정 STOP 버튼 누름 ! ");
				nRet = -2;

				XL.SaveFileAs(strThisPath);
				XL.ReleaseExcel();
				sh_stage_measurement_datafile << std::endl;
				if (sh_stage_measurement_datafile.is_open() == true)
				{
					sh_stage_measurement_datafile.close();
				}

				sh_stage_measurement_load_value << std::endl;
				if (sh_stage_measurement_load_value.is_open() == true)
				{
					sh_stage_measurement_load_value.close();
				}

				sh_stage_measurement_inpos_value << std::endl;
				if (sh_stage_measurement_inpos_value.is_open() == true)
				{
					sh_stage_measurement_inpos_value.close();
				}

				sh_stage_measurement_load_Inpos_datafile_sub << std::endl;
				if (sh_stage_measurement_load_Inpos_datafile_sub.is_open() == true)
				{
					sh_stage_measurement_load_Inpos_datafile_sub.close();
				}

				break;
			}

			Max_XLoad = 0.0;
			Max_YLoad = 0.0;
			Min_XLoad = 0.0;
			Min_YLoad = 0.0;

			///////////////////////////////////////////////
			//Y stroke 170, X stroke 350 측정을 위함.
			///////////////////////////////////////////////
			move_x_pos = x_pos + x_step_move;
			move_y_pos = y_pos;

			// Z축 충돌 방지를 위한 0 재 확인 후 이동.
			//g_pScanStage->MoveZAbsolute_SlowInterlock(0.0);
			//WaitSec(2);
			//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
			//{
			//	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
			//}
			//else
			//{
			//	::AfxMessageBox(" Z AXIS 0.0 아님.!");
			//	nRet = -1;
			//	break;
			//}

			x = move_x_pos;
			if (x >= X_AXIS_1_SIZE) break;

			//g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
			MsgBoxAuto.DoModal(_T(" Z 축 home Moving ! "), 4);

			//asdf
			//if ((int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) >= (Z_INITIAL_POS_UM - 2.0)) && (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= (Z_INITIAL_POS_UM + 2.0)))
			//{
				g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
				MsgBoxAuto.DoModal(_T(" Navigation Stage Moving ! "), 10); 
				//WaitSec(10);
			//}
			//else
			//{
			//	::AfxMessageBox(" Z AXIS 원점이 아님.!");
			//	nRet = -1;
			//	break;
			//}
			cont++;

		}

		if (!m_bNavigation_Stage_Test_Start)
		{
			::AfxMessageBox(" Navigation Stage 측정 STOP 버튼 누름 ! ");
			nRet = -2;
			XL.SaveFileAs(strThisPath);
			XL.ReleaseExcel();
			sh_stage_measurement_datafile << std::endl;
			if (sh_stage_measurement_datafile.is_open() == true)
			{
				sh_stage_measurement_datafile.close();
			}

			sh_stage_measurement_load_value << std::endl;
			if (sh_stage_measurement_load_value.is_open() == true)
			{
				sh_stage_measurement_load_value.close();
			}

			sh_stage_measurement_inpos_value << std::endl;
			if (sh_stage_measurement_inpos_value.is_open() == true)
			{
				sh_stage_measurement_inpos_value.close();
			}

			sh_stage_measurement_load_Inpos_datafile_sub << std::endl;
			if (sh_stage_measurement_load_Inpos_datafile_sub.is_open() == true)
			{
				sh_stage_measurement_load_Inpos_datafile_sub.close();
			}
			break;
		}

		if (nRet != RUN) break; // x 방향 Error 발생.

		move_x_pos = orgin_x_pos;		  // X 축 초기 값으로 이동.
		move_y_pos = y_pos + y_step_move; // Y 축 50 mm 씩 이동.

		x = 0;
		y = move_y_pos;
		if (y >= Y_AXIS_0_SIZE) break;


		//g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		MsgBoxAuto.DoModal(_T(" Z 축 home Moving ! "), 4);
		//asdf
		//if ((int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) >= (Z_INITIAL_POS_UM - 2.0)) && (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= (Z_INITIAL_POS_UM + 2.0)))
		//{
		//	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
		//	MsgBoxAuto.DoModal(_T(" Navigation Stage Moving ! "), 10);
		//}
		//else
		//{
		//	::AfxMessageBox(" Z AXIS 원점이 아님.!");
		//	 nRet = -1;
		//	 break;
		//}
		//// Z축 충돌 방지를 위한 0 재 확인 후 이동.
		//g_pScanStage->MoveZAbsolute_SlowInterlock(0.0);
		//WaitSec(2);
		//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
		//{
		//	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
		//}
		//else
		//{
		//	::AfxMessageBox(" Z AXIS 0.0 아님.!");
		//	nRet = -1;
		//	break;
		//}
		//
		//WaitSec(2);

		if (nRet != RUN) break; // y 방향 Error 발생.
	}

	XL.SaveFileAs(strThisPath);
	XL.ReleaseExcel();

	sh_stage_measurement_datafile << std::endl;
	if (sh_stage_measurement_datafile.is_open() == true)
	{
		sh_stage_measurement_datafile.close();
	}

	sh_stage_measurement_load_value << std::endl;
	if (sh_stage_measurement_load_value.is_open() == true)
	{
		sh_stage_measurement_load_value.close();
	}

	sh_stage_measurement_inpos_value << std::endl;
	if (sh_stage_measurement_inpos_value.is_open() == true)
	{
		sh_stage_measurement_inpos_value.close();
	}

	sh_stage_measurement_load_Inpos_datafile_sub << std::endl;
	if (sh_stage_measurement_load_Inpos_datafile_sub.is_open() == true)
	{
		sh_stage_measurement_load_Inpos_datafile_sub.close();
	}


	//GetDlgItem(IDC_MASK_PRO)->SetWindowTextA(_T("측정 완료"));
	if (nRet != RUN)
	{
		Vec_Get_xLoad_data.clear();
		Vec_Get_yLoad_data.clear();
		Vec_Get_xInPos_data.clear();
		Vec_Get_yInPos_data.clear();
		return nRet; // Error 발생 시
	}

	// 측정 완료 후 다시 OM 영역으로 복귀
	//g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
	//SetDlgItemTextA(IDC_OM_EUV, "OM");
	//
	//GetDlgItem(IDC_CHECK_MASK_FLAT_SET)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_MASK_FLAT)->EnableWindow(true);
	//GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM)->SetWindowTextA(_T("0"));
	
	Vec_Get_xLoad_data.clear();
	Vec_Get_yLoad_data.clear();
	Vec_Get_xInPos_data.clear();
	Vec_Get_yInPos_data.clear();

	cont = 0;
	m_bNavigation_Stage_Test_Start = FALSE;
	
	return nRet;

}

void CNavigationStageTestDlg::OnBnClickedStageMeasurementStopCtl()
{
	m_bNavigation_Stage_Test_Start = FALSE;
}

void CNavigationStageTestDlg::OnCbnSelendokComboVel()
{
	int nGetcur;

	nGetcur = m_comboxvelo.GetCurSel();

	switch (nGetcur)
	{
		case 0:
			m_nSetVelocity = 2;
		break;
		case 1:
			m_nSetVelocity = 5;
		break;
		case 2:
			m_nSetVelocity = 10;
		break;
		case 3:
			m_nSetVelocity = 20;
		break;
	default:
		break;
	}

	//g_pNavigationStage->SetVelocity(ACSC_AXIS_1, m_nSetVelocity);
}

void CNavigationStageTestDlg::StageVelocityCheck()
{
	double target_vel_x = 0.0, target_vel_y = 0.0;
	double current_vel_x = 0.0, current_vel_y = 0.0;
	int target_x = 0, target_y = 0, current_x = 0,  current_y = 0;
	
	g_pNavigationStage->GetVelocity(STAGE_X_AXIS, target_vel_x);
	g_pNavigationStage->GetFeedbackVelocity(STAGE_X_AXIS, current_vel_x);

	g_pNavigationStage->GetVelocity(STAGE_Y_AXIS, target_vel_y);
	g_pNavigationStage->GetFeedbackVelocity(STAGE_Y_AXIS, current_vel_y);

	target_x = (int)target_vel_x;
	current_x = (int)(abs(current_vel_x));

	target_y = (int)target_vel_y;
	current_y = (int)(abs(current_vel_y));
	
	//current = abs((int)current_vel);

	if (target_x == current_x)
	{
		//g_pNavigationStage->GetGlobalRealVariable("PE0");
		//trackingErrorTest_P1.insert(trackingErrorTest_P1.begin() + m_nTestCnt, g_pNavigationStage->GetGlobalRealVariable("PE1"));
		m_VectrackingErrorTestPoint.push_back(g_pNavigationStage->GetGlobalRealVariable("PE1"));
		m_nTestCnt++;
	}
	else if (target_y == current_y)
	{
		//g_pNavigationStage->GetGlobalRealVariable("PE0");
		//trackingErrorTest_P1.insert(trackingErrorTest_P1.begin() + m_nTestCnt, g_pNavigationStage->GetGlobalRealVariable("PE1"));
		m_VectrackingErrorTestPoint.push_back(g_pNavigationStage->GetGlobalRealVariable("PE0"));
		m_nTestCnt++;
	}
}

UINT CNavigationStageTestDlg::FollowingErrorCheckThread(LPVOID pParam)
{
	int ret = 0;

	//StageErrorAxis *StageTestAxis = (StageErrorAxis*)pParam;
	CNavigationStageTestDlg*  FollowingErrorCheckThread = (CNavigationStageTestDlg*)pParam;

	FollowingErrorCheckThread->m_nTestCnt = 0;
	FollowingErrorCheckThread->m_bFollowingTestThreadStopFlag = FALSE;

	if (!FollowingErrorCheckThread->m_VectrackingErrorTestPoint.empty())
		FollowingErrorCheckThread->m_VectrackingErrorTestPoint.clear();

	while (!FollowingErrorCheckThread->m_bFollowingTestThreadStopFlag)
	{
		FollowingErrorCheckThread->StageVelocityCheck();
		//FollowingErrorCheckThread->StageCheck();
		Sleep(50);
	}

	//FollowingErrorCheckThread->m_nTestCnt = 0;
	FollowingErrorCheckThread->m_pFollowingErrorCheckThread = NULL;


	return ret;
}

UINT CNavigationStageTestDlg::FollowingErrorStageMoveThread(LPVOID pParam)
{
	int ret = 0;

	StageTrakingErrorInf *StageTestParam = (StageTrakingErrorInf*)pParam;

	CNavigationStageTestDlg	*StageMoveThread = StageTestParam->pNavigationStageTest;
	//CNavigationStageTestDlg	*StageMoveThread = (CNavigationStageTestDlg*)pParam;
	StageMoveThread->m_bFollowingTestStageMoveThreadStopFlag = FALSE;

	while (!StageMoveThread->m_bFollowingTestStageMoveThreadStopFlag)
	{
		if (!StageMoveThread->StageMove(StageTestParam->xpos, StageTestParam->ypos, StageTestParam->TestNo, StageTestParam->TestAxis, FALSE))
			StageMoveThread->m_bFollowingTestStageMoveThreadStopFlag = TRUE;

	}
	//SetEvent(StageMoveThread->StageTestEvent);
	//StageMoveThread->m_bFollowingTestThreadStopFlag = TRUE;
	StageMoveThread->m_pFollowingErrorStageMoveThread = NULL;
	
	delete StageTestParam;

	return ret;
}

void CNavigationStageTestDlg::StageCheck()
{
	double pos_mm[2];
	CString tmp;

	double posx_mm = 0.0, posy_mm = 0.0;
	double aa = 0.0, bb = 0.0, cc = 0.0, dd = 0.0;

	//////////////////////////////////////////////////// STAGE POSITION READ ////////////////////////////////////////////////////////////////////////////////////////////////////////
	for (int i = 0; i < STAGE_AXIS_NUMBER; i++) {
		pos_mm[i] = g_pNavigationStage->GetPosmm(i);
	}

	tmp.Format("%3.6f", pos_mm[STAGE_X_AXIS]);
	if (m_stageencoderx_test.m_hWnd != NULL)
		m_stageencoderx_test.SetWindowText(tmp);

	tmp.Format("%3.6f", pos_mm[STAGE_Y_AXIS]);
	if (m_stageencodery_test.m_hWnd != NULL)
		m_stageencodery_test.SetWindowText(tmp);




	//Mask LB 좌표계 Display
	if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == SCOPE_OM4X && g_pMaskMap->m_bOMAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
	{
		aa = (g_pMaskMap->m_dOMAlignPointRT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		bb = (g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		cc = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		dd = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		posx_mm = (bb*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000.;
		posy_mm = (aa*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000.;
	}
	else if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV && g_pMaskMap->m_bEUVAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
	{
		aa = (g_pMaskMap->m_dEUVAlignPointRT_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		bb = (g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		cc = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		dd = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		posx_mm = (bb*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000.;
		posy_mm = (aa*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000.;
	}
	else
	{
		if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV)
		{
			posx_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].x - pos_mm[STAGE_X_AXIS];
			posy_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].y - pos_mm[STAGE_Y_AXIS];
		}
		else
		{
			posx_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - pos_mm[STAGE_X_AXIS];
			posy_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - pos_mm[STAGE_Y_AXIS];

		}
	}
	tmp.Format("%3.6f", posx_mm);
	if (m_Defectcoordxtest_x.m_hWnd != NULL)
		m_Defectcoordxtest_x.SetWindowText(tmp);
	tmp.Format("%3.6f", posy_mm);
	if (m_Defectcoordxtest_y.m_hWnd != NULL)
		m_Defectcoordxtest_y.SetWindowText(tmp);

	//Mask Align Point LB 좌표계 Display
	if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == SCOPE_OM4X && g_pMaskMap->m_bOMAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
	{
		aa = (g_pMaskMap->m_dOMAlignPointRT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		bb = (g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		cc = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		dd = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		posx_mm = (bb*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd);
		posy_mm = (aa*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd);
	}
	else if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV && g_pMaskMap->m_bEUVAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
	{
		aa = (g_pMaskMap->m_dEUVAlignPointRT_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		bb = (g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		cc = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		dd = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		posx_mm = (bb*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd);
		posy_mm = (aa*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd);
	}
	else
	{
		if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV)
		{
			posx_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].x - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000. - pos_mm[STAGE_X_AXIS];
			posy_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000. - pos_mm[STAGE_Y_AXIS];
		}
		else
		{
			posx_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000. - pos_mm[STAGE_X_AXIS];
			posy_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000. - pos_mm[STAGE_Y_AXIS];
		}
	}
	tmp.Format("%3.6f", posx_mm);
	if (m_masklbmcoordx_test.m_hWnd != NULL)
		m_masklbmcoordx_test.SetWindowText(tmp);
	tmp.Format("%3.6f", posy_mm);
	if (m_masklbmcoordy_test.m_hWnd != NULL)
		m_masklbmcoordy_test.SetWindowText(tmp);

	//Mask Center 좌표계 Display
	if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == SCOPE_OM4X && g_pMaskMap->m_bOMAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
	{
		aa = (g_pMaskMap->m_dOMAlignPointRT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		bb = (g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		cc = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		dd = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		posx_mm = (bb*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000. - MaskSize_mm / 2;
		posy_mm = (aa*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000. - MaskSize_mm / 2;
	}
	else if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV && g_pMaskMap->m_bEUVAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
	{
		aa = (g_pMaskMap->m_dEUVAlignPointRT_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		bb = (g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		cc = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
		dd = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
		posx_mm = (bb*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000. - MaskSize_mm / 2;
		posy_mm = (aa*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - pos_mm[STAGE_Y_AXIS]) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (pos_mm[STAGE_X_AXIS] - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000. - MaskSize_mm / 2;
	}
	else
	{
		if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV)
		{
			posx_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - pos_mm[STAGE_X_AXIS];
			posy_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - pos_mm[STAGE_Y_AXIS];
		}
		else
		{
			posx_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - pos_mm[STAGE_X_AXIS];
			posy_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - pos_mm[STAGE_Y_AXIS];
		}
	}

	tmp.Format("%3.6f", posx_mm);
	if (m_maskcentercoordx_test.m_hWnd != NULL)
		m_maskcentercoordx_test.SetWindowText(tmp);
	tmp.Format("%3.6f", posy_mm);
	if (m_maskcentercoordy_test.m_hWnd != NULL)
		m_maskcentercoordy_test.SetWindowText(tmp);


}

BOOL CNavigationStageTestDlg::StageMove(double maskCoordX_um, double maskCoordY_um, int num, int TestAxis, BOOL CoordRef)
{
	CString Avg, Max, Min, Std, TestVelo, TestCnt, logstring, teststartxpos, teststartypos, testendxpos, testendypos, testaxis;
	double currentx = 0.0, currenty = 0.0;
	double xpos_mm = 0.0, ypos_mm = 0.0, SumData = 0.0, AvgData = 0.0, MaxData = 0.0, MinData = 0.0, x_data = 0.0, XInposVariance = 0.0, XInposSTD = 0.0;

	if (g_pNavigationStage == NULL)
	{
		return FALSE;
	}
	
	/* Z 축 안전 위치 이동 */
	//g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	WaitSec(1);



	
	if (CoordRef)
	{
		/* Mask Center 기준으로 Stage position Convert */
		g_pNavigationStage->ConvertToStageFromMask(xpos_mm, ypos_mm, maskCoordX_um, maskCoordY_um);
	}
	else
	{
		/* Mask Center 기준으로 Stage position Convert */
		xpos_mm = maskCoordX_um;
		ypos_mm = maskCoordY_um;
	}
	

	/* 시작 위치로 이동 */
	if(g_pNavigationStage->MoveAbsolutePosition(xpos_mm, ypos_mm) != XY_NAVISTAGE_OK)
	{
		::AfxMessageBox(" Stage 동작 불가!");
		return FALSE;
	}
	while (TRUE)
	{
		currentx = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		currenty = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

		if (((currentx >= xpos_mm - 0.01) && (currentx <= xpos_mm + 0.01)) && ((currenty >= ypos_mm - 0.01) && (currenty <= ypos_mm + 0.01)))
			break;
	}

	//StageErrorAxis *StageTestAxis = new StageErrorAxis;
	//StageTestAxis->pNavigationStageTest = this;
	//StageTestAxis->TestAxis = TestAxis;

	m_pFollowingErrorCheckThread = AfxBeginThread(FollowingErrorCheckThread, this, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL		
	//m_pFollowingErrorCheckThread = AfxBeginThread(FollowingErrorCheckThread, (LPVOID)StageTestAxis, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL		
	
	WaitSec(2);
	
	/* X 축, Y 축 측정에 따른 Stage 이동 */

	if (TestAxis == STAGE_X_AXIS)
	{
		g_pNavigationStage->SetVelocity(STAGE_X_AXIS, m_nSetVelocity);
		if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, 350) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
			return FALSE;
		}

		while (TRUE)
		{
			currentx = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
			currenty = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

			if (((int(currentx) == (int)xpos_mm + 350.0)) && (((int)currenty == (int)ypos_mm)))
				break;
		}
	}

	else if (TestAxis == STAGE_Y_AXIS)
	{
		g_pNavigationStage->SetVelocity(STAGE_Y_AXIS, m_nSetVelocity);
		if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, 170) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
			return FALSE;
		}

		while (TRUE)
		{
			currentx = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
			currenty = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

			if (((int(currentx) == (int)xpos_mm)) && (((int)currenty == (int)ypos_mm + 170)))
				break;
		}
	}
	WaitSec(3);

	m_bFollowingTestThreadStopFlag = TRUE;
	m_pFollowingErrorCheckThread = NULL;

	SetEvent(StageTestEvent);

	MaxData = m_VectrackingErrorTestPoint.at(0);
	int size = m_VectrackingErrorTestPoint.size();

	for (int i = 0; i < size; i++)
	{
		SumData += m_VectrackingErrorTestPoint.at(i);

		if(MaxData < m_VectrackingErrorTestPoint.at(i))
			MaxData = m_VectrackingErrorTestPoint.at(i);

		if(MinData > m_VectrackingErrorTestPoint.at(i))
			MinData = m_VectrackingErrorTestPoint.at(i);
	}

	AvgData = SumData / size;

	for (int i = 0; i < size; i++)
	{
		x_data += pow((m_VectrackingErrorTestPoint.at(i) - AvgData), 2);
	}

	XInposVariance = (x_data / size);

	x_data = 0.0;

	XInposSTD = sqrt(XInposVariance);

	switch (num)
	{
	case 0 :
		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_0.m_hWnd != NULL)
			m_avg_value_0.SetWindowText(Avg);
		
		Max.Format("%3.5e", MaxData);
		if (m_max_value_0.m_hWnd != NULL)
			m_max_value_0.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_0.m_hWnd != NULL)
			m_min_value_0.SetWindowText(Min);

		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_0.m_hWnd != NULL)
			m_edit_test_cnt_0.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_0.m_hWnd != NULL)
			m_edit_test_vel_0.SetWindowText(TestVelo);
		
		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_0.m_hWnd != NULL)
			m_error_value_0.SetWindowText(Std);

		

		break;
	case 1:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_1.m_hWnd != NULL)
			m_edit_test_cnt_1.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_1.m_hWnd != NULL)
			m_edit_test_vel_1.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_1.m_hWnd != NULL)
			m_avg_value_1.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_1.m_hWnd != NULL)
			m_max_value_1.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_1.m_hWnd != NULL)
			m_min_value_1.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_1.m_hWnd != NULL)
			m_error_value_1.SetWindowText(Std);


		break;
	case 2:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_2.m_hWnd != NULL)
			m_edit_test_cnt_2.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_2.m_hWnd != NULL)
			m_edit_test_vel_2.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_2.m_hWnd != NULL)
			m_avg_value_2.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_2.m_hWnd != NULL)
			m_max_value_2.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_2.m_hWnd != NULL)
			m_min_value_2.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_2.m_hWnd != NULL)
			m_error_value_2.SetWindowText(Std);


		break;
	case 3:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_3.m_hWnd != NULL)
			m_edit_test_cnt_3.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_3.m_hWnd != NULL)
			m_edit_test_vel_3.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_3.m_hWnd != NULL)
			m_avg_value_3.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_3.m_hWnd != NULL)
			m_max_value_3.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_3.m_hWnd != NULL)
			m_min_value_3.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_3.m_hWnd != NULL)
			m_error_value_3.SetWindowText(Std);

		break;
	case 4:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_4.m_hWnd != NULL)
			m_edit_test_cnt_4.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_4.m_hWnd != NULL)
			m_edit_test_vel_4.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_4.m_hWnd != NULL)
			m_avg_value_4.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_4.m_hWnd != NULL)
			m_max_value_4.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_4.m_hWnd != NULL)
			m_min_value_4.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_4.m_hWnd != NULL)
			m_error_value_4.SetWindowText(Std);


		break;
	case 5:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_5.m_hWnd != NULL)
			m_edit_test_cnt_5.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_5.m_hWnd != NULL)
			m_edit_test_vel_5.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_5.m_hWnd != NULL)
			m_avg_value_5.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_5.m_hWnd != NULL)
			m_max_value_5.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_5.m_hWnd != NULL)
			m_min_value_5.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_5.m_hWnd != NULL)
			m_error_value_5.SetWindowText(Std);


		break;
	case 6:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_6.m_hWnd != NULL)
			m_edit_test_cnt_6.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_6.m_hWnd != NULL)
			m_edit_test_vel_6.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_6.m_hWnd != NULL)
			m_avg_value_6.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_6.m_hWnd != NULL)
			m_max_value_6.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_6.m_hWnd != NULL)
			m_min_value_6.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_6.m_hWnd != NULL)
			m_error_value_6.SetWindowText(Std);


		break;
	case 7:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_7.m_hWnd != NULL)
			m_edit_test_cnt_7.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_7.m_hWnd != NULL)
			m_edit_test_vel_7.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_7.m_hWnd != NULL)
			m_avg_value_7.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_7.m_hWnd != NULL)
			m_max_value_7.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_7.m_hWnd != NULL)
			m_min_value_7.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_7.m_hWnd != NULL)
			m_error_value_7.SetWindowText(Std);


		break;
	case 8:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_8.m_hWnd != NULL)
			m_edit_test_cnt_8.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_8.m_hWnd != NULL)
			m_edit_test_vel_8.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_8.m_hWnd != NULL)
			m_avg_value_8.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_8.m_hWnd != NULL)
			m_max_value_8.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_8.m_hWnd != NULL)
			m_min_value_8.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_8.m_hWnd != NULL)
			m_error_value_8.SetWindowText(Std);

		break;
	case 9:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_9.m_hWnd != NULL)
			m_edit_test_cnt_9.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_9.m_hWnd != NULL)
			m_edit_test_vel_9.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_9.m_hWnd != NULL)
			m_avg_value_9.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_9.m_hWnd != NULL)
			m_max_value_9.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_9.m_hWnd != NULL)
			m_min_value_9.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_9.m_hWnd != NULL)
			m_error_value_9.SetWindowText(Std);

		break;
	case 10:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_10.m_hWnd != NULL)
			m_edit_test_cnt_10.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_10.m_hWnd != NULL)
			m_edit_test_vel_10.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_10.m_hWnd != NULL)
			m_avg_value_10.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_10.m_hWnd != NULL)
			m_max_value_10.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_10.m_hWnd != NULL)
			m_min_value_10.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_10.m_hWnd != NULL)
			m_error_value_10.SetWindowText(Std);

		break;
	case 11:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_11.m_hWnd != NULL)
			m_edit_test_cnt_11.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_11.m_hWnd != NULL)
			m_edit_test_vel_11.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_11.m_hWnd != NULL)
			m_avg_value_11.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_11.m_hWnd != NULL)
			m_max_value_11.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_11.m_hWnd != NULL)
			m_min_value_11.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_11.m_hWnd != NULL)
			m_error_value_11.SetWindowText(Std);

		break;
	case 12:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_12.m_hWnd != NULL)
			m_edit_test_cnt_12.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_12.m_hWnd != NULL)
			m_edit_test_vel_12.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_12.m_hWnd != NULL)
			m_avg_value_12.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_12.m_hWnd != NULL)
			m_max_value_12.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_12.m_hWnd != NULL)
			m_min_value_12.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_12.m_hWnd != NULL)
			m_error_value_12.SetWindowText(Std);

		break;
	case 13:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_13.m_hWnd != NULL)
			m_edit_test_cnt_13.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_13.m_hWnd != NULL)
			m_edit_test_vel_13.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_13.m_hWnd != NULL)
			m_avg_value_13.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_13.m_hWnd != NULL)
			m_max_value_13.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_13.m_hWnd != NULL)
			m_min_value_13.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_13.m_hWnd != NULL)
			m_error_value_13.SetWindowText(Std);

		break;
	case 14:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_14.m_hWnd != NULL)
			m_edit_test_cnt_14.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_14.m_hWnd != NULL)
			m_edit_test_vel_14.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_14.m_hWnd != NULL)
			m_avg_value_14.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_14.m_hWnd != NULL)
			m_max_value_14.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_14.m_hWnd != NULL)
			m_min_value_14.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_14.m_hWnd != NULL)
			m_error_value_14.SetWindowText(Std);

		break;
	case 15:
		TestCnt.Format("%d", m_nTestCnt);
		if (m_edit_test_cnt_15.m_hWnd != NULL)
			m_edit_test_cnt_15.SetWindowText(TestCnt);

		TestVelo.Format("%d", m_nSetVelocity);
		if (m_edit_test_vel_15.m_hWnd != NULL)
			m_edit_test_vel_15.SetWindowText(TestVelo);

		Avg.Format("%3.5e", AvgData);
		if (m_avg_value_15.m_hWnd != NULL)
			m_avg_value_15.SetWindowText(Avg);

		Max.Format("%3.5e", MaxData);
		if (m_max_value_15.m_hWnd != NULL)
			m_max_value_15.SetWindowText(Max);

		Min.Format("%3.5e", MinData);
		if (m_min_value_15.m_hWnd != NULL)
			m_min_value_15.SetWindowText(Min);

		Std.Format("%3.5e", XInposSTD);
		if (m_error_value_15.m_hWnd != NULL)
			m_error_value_15.SetWindowText(Std);

		break;

	default:
		break;
	}

	teststartxpos.Format("%f", maskCoordX_um);
	teststartypos.Format("%f", maskCoordY_um);
	testaxis.Format("%d", TestAxis);
	
	testendxpos.Format("%f", currentx = g_pNavigationStage->GetPosmm(STAGE_X_AXIS));
	testendypos.Format("%f", currenty = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS));

	logstring = _T("[Stage Start Coordinate] X(mm) : ") + teststartxpos + _T(" , Y(mm) : ") + teststartypos +
		_T(" ,  [Stage End Coordinate] X(mm) : ") + testendxpos + _T(" , Y(mm) : ") + testendypos +
		_T(" ,  [Position Start] Test Axis : ") + testaxis + _T("Axis  , Velocity : ") + TestVelo + _T(" , Test Count : ") + TestCnt +
		_T(" ,  Traking Error Average : ") + Avg + _T(" , Traking Error Max : ") + Max +
		_T(" ,  Traking Error Min : ") + Min + _T(" , Traking Error Std : ") + Std;

	//logstring = _T("[Coordinate LB] X(um) : ") + teststartxpos + _T(" , Y(um) : ") + teststartypos +
	//	_T(" Position Start] Test Axis : ") + testaxis + _T("Axis  , Velocity : ") + TestVelo + _T(" , Test Count : ") + TestCnt +
	//	_T(" , Traking Error Average : ") + Avg + _T(" , Traking Error Max : ") + Max +
	//	_T(" , Traking Error Min : ") + Min + _T(" , Traking Error Std : ") + Std;

	SaveLogFile("TrakingErrorReport_Log", _T((LPSTR)(LPCTSTR)(logstring)));

	g_pNavigationStage->SetVelocity(STAGE_Y_AXIS, 20);
	g_pNavigationStage->SetVelocity(STAGE_X_AXIS, 20);
	
	
	//SetEvent(StageTestEvent);
	m_bFollowingTestStageMoveThreadStopFlag = TRUE;
	//delete StageTestAxis;

	Avg.Empty();
	Max.Empty();
	Min.Empty();
	Std.Empty();
	TestVelo.Empty();
	TestCnt.Empty();
	logstring.Empty();
	teststartxpos.Empty();
	teststartypos.Empty();
	testaxis.Empty();
	xpos_mm = 0.0;
	ypos_mm = 0.0;
	SumData = 0.0;
	AvgData = 0.0;
	MaxData = 0.0;
	MinData = 0.0;
	x_data = 0.0;
	XInposVariance = 0.0;
	XInposSTD = 0.0;

	return TRUE;

}

/*
	STAGE ENCODER CENTER 기준
*/

void CNavigationStageTestDlg::StageFollowingErrorCheck(int TestPointNum, int TestAxis)
{

	StageTrakingErrorInf *StageTestParam = new StageTrakingErrorInf;
	StageTestParam->pNavigationStageTest = this;

	g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
	WaitSec(4);

	/* EncoderMode 로 측정*/
	//g_pNavigationStage->SetLaserSwitchingFunction(FALSE);
	g_pNavigationStage->SetEncoderMode();

	StageTestParam->CoordRef = FALSE;

	switch (TestPointNum)
	{
	case 0:
		StageTestParam->xpos = 0.0;
		StageTestParam->ypos = 0.0;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		//m_pFollowingErrorStageMoveThread = AfxBeginThread(FollowingErrorStageMoveThread, this, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL		
		//m_pFollowingErrorStageMoveThread = AfxBeginThread(FollowingErrorStageMoveThread, (LPVOID)StageTestParam, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL		
		//StageMove(0.0 , 0.0, 0);
		break;
	case 1:
		StageTestParam->xpos = 0.0;
		StageTestParam->ypos = 24.5;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 2:
		StageTestParam->xpos = 0.0;
		StageTestParam->ypos = 49.0;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 3:
		StageTestParam->xpos = 0.0;
		StageTestParam->ypos = 73.5;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 4:
		StageTestParam->xpos = 0.0;
		StageTestParam->ypos = 98.0;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 5:
		StageTestParam->xpos = 0.0;
		StageTestParam->ypos = 122.5;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 6:
		StageTestParam->xpos = 0.0;
		StageTestParam->ypos = 147.0;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 7:
		StageTestParam->xpos = 0.0;
		StageTestParam->ypos = 171.5;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 8:
		StageTestParam->xpos = 0.0;
		StageTestParam->ypos = 0.0;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 9:
		StageTestParam->xpos = 50.0;
		StageTestParam->ypos = 0.0;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 10:
		StageTestParam->xpos = 100.0;
		StageTestParam->ypos = 0.0;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 11:
		StageTestParam->xpos = 150.0;
		StageTestParam->ypos = 0.0;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 12:
		StageTestParam->xpos = 200.0;
		StageTestParam->ypos = 0.0;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 13:
		StageTestParam->xpos = 250.0;
		StageTestParam->ypos = 0.0;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 14:
		StageTestParam->xpos = 300.0;
		StageTestParam->ypos = 0.0;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	case 15:
		StageTestParam->xpos = 350.0;
		StageTestParam->ypos = 0.0;
		StageTestParam->TestNo = TestPointNum;
		StageTestParam->TestAxis = TestAxis;
		break;
	default:
		break;
	}
	m_pFollowingErrorStageMoveThread = AfxBeginThread(FollowingErrorStageMoveThread, (LPVOID)StageTestParam, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL		
}



/*
	MASK CENTER 기준 
*/
//void CNavigationStageTestDlg::StageFollowingErrorCheck(int TestPointNum, int TestAxis)
//{
//
//	StageTrakingErrorInf *StageTestParam = new StageTrakingErrorInf;
//	StageTestParam->pNavigationStageTest = this;
//	
//	g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
//	WaitSec(4);
//
//	/* EncoderMode 로 측정*/
//	//g_pNavigationStage->SetLaserSwitchingFunction(FALSE);
//	g_pNavigationStage->SetEncoderMode();
//
//	switch (TestPointNum)
//	{
//	case 0 :
//		StageTestParam->xpos = -75000.0;
//		StageTestParam->ypos = -75000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		//m_pFollowingErrorStageMoveThread = AfxBeginThread(FollowingErrorStageMoveThread, this, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL		
//		//m_pFollowingErrorStageMoveThread = AfxBeginThread(FollowingErrorStageMoveThread, (LPVOID)StageTestParam, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL		
//		//StageMove(0.0 , 0.0, 0);
//		break;
//	case 1:
//		StageTestParam->xpos = -75000.0;
//		StageTestParam->ypos = -55000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 2:
//		StageTestParam->xpos = -75000.0;
//		StageTestParam->ypos = -35000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 3:
//		StageTestParam->xpos = -75000.0;
//		StageTestParam->ypos = -15000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 4:
//		StageTestParam->xpos = -75000.0;
//		StageTestParam->ypos = 5000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 5:
//		StageTestParam->xpos = -75000.0;
//		StageTestParam->ypos = 25000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 6:
//		StageTestParam->xpos = -75000.0;
//		StageTestParam->ypos = 45000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 7:
//		StageTestParam->xpos = -75000.0;
//		StageTestParam->ypos = 65000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 8:
//		StageTestParam->xpos = -75000.0;
//		StageTestParam->ypos = -75000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 9:
//		StageTestParam->xpos = -55000.0;
//		StageTestParam->ypos = -75000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 10:
//		StageTestParam->xpos = -35000.0;
//		StageTestParam->ypos = -75000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 11:
//		StageTestParam->xpos = -15000.0;
//		StageTestParam->ypos = -75000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 12:
//		StageTestParam->xpos = 5000.0;
//		StageTestParam->ypos = -75000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 13:
//		StageTestParam->xpos = 25000.0;
//		StageTestParam->ypos = -75000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 14:
//		StageTestParam->xpos = 45000.0;
//		StageTestParam->ypos = -75000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	case 15:
//		StageTestParam->xpos = 65000.0;
//		StageTestParam->ypos = -75000.0;
//		StageTestParam->TestNo = TestPointNum;
//		StageTestParam->TestAxis = TestAxis;
//		break;
//	default:
//		break;
//	}
//	m_pFollowingErrorStageMoveThread = AfxBeginThread(FollowingErrorStageMoveThread, (LPVOID)StageTestParam, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL		
//}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet0()
{
	//g_pMaskMap->ChangeOMEUVSet(OMTOEUV);
	//WaitSec(4);
	StageFollowingErrorCheck(0, STAGE_X_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet1()
{
	StageFollowingErrorCheck(1, STAGE_X_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet2()
{
	StageFollowingErrorCheck(2, STAGE_X_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet3()
{
	StageFollowingErrorCheck(3, STAGE_X_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet4()
{
	StageFollowingErrorCheck(4, STAGE_X_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet5()
{
	StageFollowingErrorCheck(5, STAGE_X_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet6()
{
	StageFollowingErrorCheck(6, STAGE_X_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet7()
{
	StageFollowingErrorCheck(7, STAGE_X_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet8()
{
	StageFollowingErrorCheck(8, STAGE_Y_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet9()
{
	StageFollowingErrorCheck(9, STAGE_Y_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet10()
{
	StageFollowingErrorCheck(10, STAGE_Y_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet11()
{
	StageFollowingErrorCheck(11, STAGE_Y_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet12()
{
	StageFollowingErrorCheck(12, STAGE_Y_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet13()
{
	StageFollowingErrorCheck(13, STAGE_Y_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet14()
{
	StageFollowingErrorCheck(14, STAGE_Y_AXIS);
}

void CNavigationStageTestDlg::OnBnClickedBtnTestSet15()
{
	StageFollowingErrorCheck(15, STAGE_Y_AXIS);
}


void CNavigationStageTestDlg::OnBnClickedBtnTestSetAll()
{
	for(int i = 0; i < 8; i++ )
	{
		ResetEvent(StageTestEvent);
		StageFollowingErrorCheck(i, STAGE_X_AXIS);
		if (WaitForSingleObject(StageTestEvent, 300000) != WAIT_OBJECT_0)
		{
			AfxMessageBox("Stage Test Loop Error");
			return;
		}
		WaitSec(2);
	}

	//int i = 0;
	//ResetEvent(StageTestEvent);
	//StageFollowingErrorCheck(i, STAGE_X_AXIS);
	//if (WaitForSingleObject(StageTestEvent, 120000) != WAIT_OBJECT_0)
	//	{
	//		AfxMessageBox("Stage 0 Test Loop Error");
	//		return;
	//	}
	//WaitSec(2);
	//i++;
	//ResetEvent(StageTestEvent);
	//StageFollowingErrorCheck(i, STAGE_X_AXIS);
	//if (WaitForSingleObject(StageTestEvent, 120000) != WAIT_OBJECT_0)
	//{
	//	AfxMessageBox("Stage 0 Test Loop Error");
	//	return;
	//}
	//WaitSec(2);
	//i++;
	//StageFollowingErrorCheck(i, STAGE_X_AXIS);
	//WaitSec(2);
	//i++;
	//StageFollowingErrorCheck(i, STAGE_X_AXIS);
	//WaitSec(2);
	//i++;
	//StageFollowingErrorCheck(i, STAGE_X_AXIS);
	//WaitSec(2);
	//i++;
	//StageFollowingErrorCheck(i, STAGE_X_AXIS);
	//WaitSec(2);
	//i++;
	//StageFollowingErrorCheck(i, STAGE_X_AXIS);
	//WaitSec(2);
	//i++;
	//StageFollowingErrorCheck(i, STAGE_X_AXIS);


}


void CNavigationStageTestDlg::OnBnClickedBtnTestSetAll2()
{
	for (int i = 8; i < 16; i++)
	{
		ResetEvent(StageTestEvent);
		StageFollowingErrorCheck(i, STAGE_Y_AXIS);
		if (WaitForSingleObject(StageTestEvent, 300000) != WAIT_OBJECT_0)
		{
			AfxMessageBox("Stage Test Loop Error");
			return;
		}
		WaitSec(2);
	}
}
