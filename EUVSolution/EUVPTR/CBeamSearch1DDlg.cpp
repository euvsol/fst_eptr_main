﻿// CBeam1DScanDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CBeam1DScanDlg 대화 상자

IMPLEMENT_DYNAMIC(CBeamSearch1DDlg, CDialogEx)

CBeamSearch1DDlg::CBeamSearch1DDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_BEAMSEARCH1D_DIALOG, pParent)
{

}

CBeamSearch1DDlg::~CBeamSearch1DDlg()
{
}

void CBeamSearch1DDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CBeamSearch1DDlg, CDialogEx)
END_MESSAGE_MAP()


// CBeam1DScanDlg 메시지 처리기
