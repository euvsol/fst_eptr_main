/**
 * GEM Automation Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once


// CGemAutomaionDlg 대화 상자

class CGemAutomaionDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CGemAutomaionDlg)

public:
	CGemAutomaionDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CGemAutomaionDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_GEMAUTOMATION_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
};
