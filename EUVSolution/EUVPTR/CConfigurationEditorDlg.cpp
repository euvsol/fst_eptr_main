﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CConfigurationEditorDlg 대화 상자

IMPLEMENT_DYNAMIC(CConfigurationEditorDlg, CDialogEx)

CConfigurationEditorDlg::CConfigurationEditorDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CONFIG_DIALOG, pParent)
	, m_dConfigCalTx_urad(0)
	, m_dConfigCalTy_urad(0)
	, m_dGlobalOffsetX_mm(0)
	, m_dGlobalOffsetY_mm(0)
	, m_dLBOffsetX_mm(0)
	, m_dLBOffsetY_mm(0)
	, m_dLTOffsetX_mm(0)
	, m_dLTOffsetY_mm(0)
	, m_dRTOffsetX_mm(0)
	, m_dRTOffsetY_mm(0)
	, m_dZdistanceCap2nStage_um(0)
	, m_dInspectorOffsetX_um(0)
	, m_dInspectorOffsetY_um(0)
	, m_dZdistanceCap1nStage_um(0)
	, m_dZdistanceCap3nStage_um(0)
	, m_dZdistanceCap4nStage_um(0)
	, m_dDownZHeightWhenMove_um(100)
	, m_nSourceAutoOffTime_min(30)	//30*60sec = 30 min --> Thrufocus 시간때문에 늘림 kyd
	, m_dLB_LaserSwitching_OffsetX_mm(0)
	, m_dLB_LaserSwitching_OffsetY_mm(0)
	, m_dLT_LaserSwitching_OffsetX_mm(0)
	, m_dLT_LaserSwitching_OffsetY_mm(0)
	, m_dRT_LaserSwitching_OffsetX_mm(0)
	, m_dRT_LaserSwitching_OffsetY_mm(0)
	, m_dOM_PixelSizeX_um(0.86)
	, m_dOM_PixelSizeY_um(0.86)
	, m_nOMStageAreaLB_X(172)
	, m_nOMStageAreaLB_Y(10)
	, m_nOMStageAreaRT_X(320)
	, m_nOMStageAreaRT_Y(170)
	, m_nEUVStageAreaLB_X(25)
	, m_nEUVStageAreaLB_Y(10)
	, m_nEUVStageAreaRT_X(172)
	, m_nEUVStageAreaRT_Y(170)
{
	m_dPressure_ChangeToFast_MC_Rough_ini = 50.0;	// MC Slow rough -> Fast Rough
	m_dPressure_ChangeToFast_Rough_ini = 50.0;		// LLC Slow rough -> Fast Rough

	m_dPressure_ChangeToFast_MC_Rough = 50.0;		// MC Slow rough -> Fast Rough
	m_dPressure_ChangeToFast_Rough = 50.0;			// LLC Slow rough -> Fast Rough

	m_dPressure_ChangeToTMP_Rough = 0.02;			// [2 * 10^-2] 	Fast Rougn -> Tmp pumping
	m_dPressure_ChangeToTMP_Rough_ini = 0.02;		// [2 * 10^-2] 	Fast Rougn -> Tmp pumping

	m_dPressure_ChangeToMCTMP_Rough = 0.035;		// [35 * 10^-2]  MC Fast Rough -> TMP pumping 	
	m_dPressure_ChangeToMCTMP_Rough_ini = 0.035;	// [35 * 10^-2]  MC Fast Rough -> TMP pumping 	
	
	m_dPressure_Rough_End = 0.000009;				// Tmp pumping 조건 값	m_dPressure_ChangeToSlow2_Vent = 0.1;
	m_dPressure_Rough_End_ini = 0.000009;			// Tmp pumping 조건 값	m_dPressure_ChangeToSlow2_Vent = 0.1;

	//m_dPressure_Rough_End = 0.00003;				// Tmp pumping 조건 값	m_dPressure_ChangeToSlow2_Vent = 0.1;
	//m_dPressure_Rough_End_ini = 0.00003;			// Tmp pumping 조건 값	m_dPressure_ChangeToSlow2_Vent = 0.1;

	m_dPressure_ChangeToVent = 0.003;				// MC 대기 Venting
	m_dPressure_ChangeToVent_ini = 0.003;			// MC 대기 Venting

	// 초기 Setting 값.
	//m_dPressure_ChangeToVent_LLC = 0.002; // 2 x 10^-3
	//m_dPressure_ChangeToVent_LLC_ini = 0.002;

	// Venting 속도 절감을 위해 값 변경 (대기 venting 범위)
	//m_dPressure_ChangeToVent_LLC = 0.00005;	// 5 x 10^-5
	//m_dPressure_ChangeToVent_LLC_ini = 0.00005;

	m_dPressure_ChangeToVent_LLC = 0.00002;	// 2 x 10^-5
	m_dPressure_ChangeToVent_LLC_ini = 0.00002;

	m_dPressure_ChangeToSlow2_Vent = 1;		//LLC slow vent -> Fast vent
	m_dPressure_ChangeToSlow2_Vent_ini = 1;			//LLC slow vent -> Fast vent

	m_dPressure_ChangeToSlow_Vent = 0.1;			// MC Vent 전 SLOW VENT 가능 조건 검사 압력 기준, MC slow vent -> mc fast vent 
	m_dPressure_ChangeToSlow_Vent_ini = 0.1;			// MC Vent 전 SLOW VENT 가능 조건 검사 압력 기준, MC slow vent -> mc fast vent 

	m_dPressure_ChangeToFast_Vent = 10.0; 
	
	m_dPressure_Vent_End = 760.0;
	m_dPressure_Vent_End_ini = 760.0;

	m_dPressure_Vent_Tolerance = 10.0;
	m_nTimeout_sec_LLKSlowRough = 90;				//LLC Slow Rough 시간
	m_nTimeout_sec_LLKFastRough = 300;
	m_nTimeout_sec_LLKRoughEnd = 1200;
	m_nTimeout_sec_LLKStandbyVent = 600;			// LLC 대기 시간
	//m_nTimeout_sec_LLKSlow1Vent = 120;				// LLC Slow Vent 시간
	m_nTimeout_sec_LLKSlow1Vent = 300;				// LLC Slow Vent 시간
	m_nTimeout_sec_LLKFastVent = 120;				// LLC Fast Vent 시간
	m_nTimeout_sec_LLKVentEnd = 1200;				// LLC Venting 시간.
	m_nTimeout_sec_ValveOperation = 10;
	m_nTimeout_sec_MCSlowRough = 600;
	m_nTimeout_sec_MCFastRough = 600;
	m_nTimeout_sec_MCTmpEnd = 3600;
	m_nTimeout_sec_MCLLCVent = 1800;				// MC Vent 시 TMP Gate Close 후 대기 venting 시간.(30분)
	m_nTimeout_sec_MCRoughEnd = 1000;
	m_nTimeout_sec_MCSlow1Vent = 1800;				// MC Vent 시 Slow Vent 시간 (30분)
	m_nTimeout_sec_MCFastVent = 1800;				// MC Vent 시 Fast Vent 시간 (30분)
	m_nTimeout_sec_MCVentEnd = 300;

	m_nTimeout_sec_LLKSlowRough_ini = m_nTimeout_sec_LLKSlowRough;
	m_nTimeout_sec_LLKFastRough_ini = m_nTimeout_sec_LLKFastRough;
	m_nTimeout_sec_LLKRoughEnd_ini = m_nTimeout_sec_LLKRoughEnd;
	m_nTimeout_sec_LLKSlow1Vent_ini = m_nTimeout_sec_LLKSlow1Vent;
	m_nTimeout_sec_LLKStandbyVent_ini = m_nTimeout_sec_LLKStandbyVent;
	m_nTimeout_sec_LLKFastVent_ini = m_nTimeout_sec_LLKFastVent;
	m_nTimeout_sec_LLKVentEnd_ini = m_nTimeout_sec_LLKVentEnd;
	m_nTimeout_sec_ValveOperation_ini = m_nTimeout_sec_ValveOperation;
	m_nTimeout_sec_MCSlowRough_ini = m_nTimeout_sec_MCSlowRough;
	m_nTimeout_sec_MCFastRough_ini = m_nTimeout_sec_MCFastRough;
	m_nTimeout_sec_MCRoughEnd_ini = m_nTimeout_sec_MCRoughEnd;
	m_nTimeout_sec_MCTmpEnd_ini = m_nTimeout_sec_MCTmpEnd;
	m_nTimeout_sec_MCLLCVent_ini = m_nTimeout_sec_MCLLCVent;
	m_nTimeout_sec_MCSlow1Vent_ini = m_nTimeout_sec_MCSlow1Vent; 
	m_nTimeout_sec_MCFastVent_ini = m_nTimeout_sec_MCFastVent;
	m_nTimeout_sec_MCVentEnd_ini = m_nTimeout_sec_MCVentEnd;

	m_dPressure_SlowMFC_Inlet_Valve_Open_Value = 0.1;	// Slow MFC Inlet Open 시점 값


	int i = 0;
	for (i = 0; i < MAX_STAGE_POSITION; i++)
	{
		m_stStagePos[i].x = m_stStagePos[i].y = 0.0;
		sprintf(m_stStagePos[i].chStagePositionString, "");
	}
	m_bNaviStageMoveAtZorigin = FALSE;

	//m_dLB_LaserSwitching_OffsetX_mm = 0.0011;
	//m_dLB_LaserSwitching_OffsetY_mm = -0.0011;
	//m_dLT_LaserSwitching_OffsetX_mm = 0.012;
	//m_dLT_LaserSwitching_OffsetY_mm = 0.015;
	//m_dRT_LaserSwitching_OffsetX_mm = 0.0400;
	//m_dRT_LaserSwitching_OffsetY_mm = 0.0105;
	m_dLB_LaserSwitching_OffsetX_mm = 0.0;
	m_dLB_LaserSwitching_OffsetY_mm = 0.0;
	m_dLT_LaserSwitching_OffsetX_mm = 0.0;
	m_dLT_LaserSwitching_OffsetY_mm = 0.0;
	m_dRT_LaserSwitching_OffsetX_mm = 0.0;
	m_dRT_LaserSwitching_OffsetY_mm = 0.0;

	m_dOMAlignPointLB_X_mm = 0.0;
	m_dOMAlignPointLB_Y_mm = 0.0;
	m_dOMAlignPointLT_X_mm = 0.0;
	m_dOMAlignPointLT_Y_mm = 0.0;
	m_dOMAlignPointRT_X_mm = 0.0;
	m_dOMAlignPointRT_Y_mm = 0.0;
	m_dEUVAlignPointLB_X_mm = 0.0;
	m_dEUVAlignPointLB_Y_mm = 0.0;
	m_dEUVAlignPointLT_X_mm = 0.0;
	m_dEUVAlignPointLT_Y_mm = 0.0;
	m_dEUVAlignPointRT_X_mm = 0.0;
	m_dEUVAlignPointRT_Y_mm = 0.0;
	m_dNotchAlignPoint1_X_mm = 0.0; 
	m_dNotchAlignPoint1_Y_mm = 0.0;
	m_dNotchAlignPoint2_X_mm = 0.0; 
	m_dNotchAlignPoint2_Y_mm = 0.0;
	m_dEUVNotchAlignPoint1_X_mm = 0.0;
	m_dEUVNotchAlignPoint1_Y_mm = 0.0;
	m_dEUVNotchAlignPoint2_X_mm = 0.0;
	m_dEUVNotchAlignPoint2_Y_mm = 0.0;
	m_bOMAlignCompleteFlag = FALSE;
	m_bEUVAlignCompleteFlag = FALSE;
	//m_bLaserFeedbackAvailable_Flag = FALSE;
	m_bLaserFeedback_Flag = FALSE;

	m_nEUVAlignFOV = 10000;	//nm
	m_nEUVAlignGrid = 80;

	m_bMtsRotateDone_Flag	= FALSE;
	m_bMtsFlipDone_Flag		= FALSE;
	m_nMaterialLocation		= -1;

	m_dZInterlock_um = 120.0;
	m_dZFocusPos_um = 100.0;
	m_dZCapStageOffset_um = 314.0;
	m_dZThroughFocusUp_um = 0.0;

	//m_rcOMStageAreaRect.SetRect(175, 10, 330, 165);	//Stage의 OM 영역(RB_x, RB_y, LT_x, LT_y, )  -> SREM Chuck
	//m_rcEUVStageAreaRect.SetRect(30, 10, 175, 165);	//Stage의 EUV 영역(RB_x, RB_y, LT_x, LT_y, ) -> SREM Chuck
	//m_rcOMStageAreaRect.SetRect(190, 10, 340, 178);	//Stage의 OM 영역(RB_x, RB_y, LT_x, LT_y, )  -> PTR Chuck
	//m_rcEUVStageAreaRect.SetRect(30, 10, 190, 178);	//Stage의 EUV 영역(RB_x, RB_y, LT_x, LT_y, ) -> PTR Chuck
	m_rcOMStageAreaRect.SetRect(m_nOMStageAreaLB_X, m_nOMStageAreaLB_Y, m_nOMStageAreaRT_X, m_nOMStageAreaRT_Y);	//Stage의 OM 영역(LB_x, LB_y, RT_x, RT_y, )      -> Zerodur Chuck , config에 생성필요
	m_rcEUVStageAreaRect.SetRect(m_nEUVStageAreaLB_X, m_nEUVStageAreaLB_Y, m_nEUVStageAreaRT_X, m_nEUVStageAreaRT_Y);	//Stage의 EUV 영역(LB_x, LB_y, RT_x,RT_y, )     -> Zerodur Chuck , config에 생성필요

	m_bBidirection = FALSE;

	// Phase 측정관련 data/////////////
	m_nAlignCcdX = -1;
	m_nAlignCcdY = -1;
	m_nAlignCcdWidth = -1;
	m_nAlignCcdHeight = -1;
	m_nAlignCcdBinning = -1;	
	m_nAlignImageX = -1;
	m_nAlignImageY = -1;
	m_nAlignImageWidth = -1;
	m_nAlignImageHeight = -1;	
	
	m_nMeasureCcdX = -1;
	m_nMeasureCcdY = -1;
	m_nMeasureCcdWidth = -1;
	m_nMeasureCcdHeight = -1;
	m_nMeasureCcdBinning = -1;	
	m_nMeasureImageX = -1;
	m_nMeasureImageY = -1;
	m_nMeasureImageWidth = -1;
	m_nMeasureImageHeight = -1;
	m_dMeasureExposureTime = -1;
	
	m_dHorizentalMargin_um = -1;
	m_dVerticalMargin_um = -1;
	m_nMaximumTryCount = -1;
	
	m_dStepWidthX_um = -1;
	m_nHalfNumberOfStepX = -1;
	m_dStepWidthY_um = -1;
	m_nHalfNumberOfStepY = -1;
	m_nNumOfStdPoint = -1;
	
	m_dSlitWidth_um = -1;
	m_dSlitPitch_um = -1;
	m_dSlitLength_um = -1;
	
	//m_dInterlockMargin_um = -1;
	//m_dZControlTolerance_um = -1;
	//
	//m_dCap1PosLimitMin_um = -1;
	//m_dCap1PosLimitMax_um = -1;
	//m_dCap2PosLimitMin_um = -1;
	//m_dCap2PosLimitMax_um = -1;
	//m_dCap3PosLimitMin_um = -1;
	//m_dCap3PosLimitMax_um = -1;
	//m_dCap4PosLimitMin_um = -1;
	//m_dCap4PosLimitMax_um = -1;


	//m_dCap1MeasurePos_um = -1;
	//m_dCap2MeasurePos_um = -1;
	//m_dCap3MeasurePos_um = -1;
	//m_dCap4MeasurePos_um = -1;

	double m_dZStageLimitMin_um =-1;
	double m_dZStageLimitMax_um =-1;
	//////////////////////////////////

	m_bUseRotate		= TRUE;
	m_bUseFlip			= FALSE;
	m_nRotateAngle		= 90;

	m_dEUVVacuumRate = 0.0000007;
}

CConfigurationEditorDlg::~CConfigurationEditorDlg()
{
}

void CConfigurationEditorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_TX, m_dConfigCalTx_urad);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_TY, m_dConfigCalTy_urad);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_GLOBAL_OFFSETX, m_dGlobalOffsetX_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_GLOBAL_OFFSETY, m_dGlobalOffsetY_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_LB_OFFSETX, m_dLBOffsetX_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_LB_OFFSETY, m_dLBOffsetY_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_LT_OFFSETX, m_dLTOffsetX_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_LT_OFFSETY, m_dLTOffsetY_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_RT_OFFSETX, m_dRTOffsetX_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_RT_OFFSETY, m_dRTOffsetY_mm);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_FOCUS_CAP2, m_dZdistanceCap2nStage_um);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_INSPECTOR_OFFSETX, m_dInspectorOffsetX_um);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_INSPECTOR_OFFSETY, m_dInspectorOffsetY_um);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_FOCUS_CAP1, m_dZdistanceCap1nStage_um);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_FOCUS_CAP3, m_dZdistanceCap3nStage_um);
	DDX_Text(pDX, IDC_EDIT_CONFIG_CAL_FOCUS_CAP4, m_dZdistanceCap4nStage_um);
	DDX_Text(pDX, IDC_EDIT_CONFIG_SOURCE_AUTOOFF_TIME, m_nSourceAutoOffTime_min);
	DDX_Text(pDX, IDC_EDIT_LASER_LB_OFFSET_X, m_dLB_LaserSwitching_OffsetX_mm);
	DDX_Text(pDX, IDC_EDIT_LASER_LB_OFFSET_Y, m_dLB_LaserSwitching_OffsetY_mm);
	DDX_Text(pDX, IDC_EDIT_LASER_LT_OFFSET_X, m_dLT_LaserSwitching_OffsetX_mm);
	DDX_Text(pDX, IDC_EDIT_LASER_LT_OFFSET_Y, m_dLT_LaserSwitching_OffsetY_mm);
	DDX_Text(pDX, IDC_EDIT_LASER_RT_OFFSET_X, m_dRT_LaserSwitching_OffsetX_mm);
	DDX_Text(pDX, IDC_EDIT_LASER_RT_OFFSET_Y, m_dRT_LaserSwitching_OffsetY_mm);
	DDX_Control(pDX, IDC_CHECK_CONFIG_USE_ROTATE, m_UseMtsRotateCtrl);
	DDX_Control(pDX, IDC_COMBO_CONFIG_ROTATE_ANGLE, m_MtsRotateAngleCtrl);
	DDX_Text(pDX, IDC_EDIT_CONFIG_OM_PIXEL_POSX, m_dOM_PixelSizeX_um);
	DDX_Text(pDX, IDC_EDIT_CONFIG_OM_PIXEL_POSY, m_dOM_PixelSizeY_um);
	DDX_Text(pDX, IDC_EDIT_CONFIG_OM_STAGE_LB_POSX, m_nOMStageAreaLB_X);
	DDX_Text(pDX, IDC_EDIT_CONFIG_OM_STAGE_LB_POSY, m_nOMStageAreaLB_Y);
	DDX_Text(pDX, IDC_EDIT_CONFIG_OM_STAGE_RT_POSX, m_nOMStageAreaRT_X);
	DDX_Text(pDX, IDC_EDIT_CONFIG_OM_STAGE_RT_POSY, m_nOMStageAreaRT_Y);
	DDX_Text(pDX, IDC_EDIT_CONFIG_EUV_STAGE_LB_POSX, m_nEUVStageAreaLB_X);
	DDX_Text(pDX, IDC_EDIT_CONFIG_EUV_STAGE_LB_POSY, m_nEUVStageAreaLB_Y);
	DDX_Text(pDX, IDC_EDIT_CONFIG_RT_STAGE_RT_POSX, m_nEUVStageAreaRT_X);
	DDX_Text(pDX, IDC_EDIT_CONFIG_RT_STAGE_RT_POSY, m_nEUVStageAreaRT_Y);
}


BEGIN_MESSAGE_MAP(CConfigurationEditorDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_IP_LOAD, &CConfigurationEditorDlg::OnBnClickedButtonConfigIpLoad)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_IP_EDIT, &CConfigurationEditorDlg::OnBnClickedButtonConfigIpEdit)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_IP_SAVE, &CConfigurationEditorDlg::OnBnClickedButtonConfigIpSave)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_COM_LOAD, &CConfigurationEditorDlg::OnBnClickedButtonConfigComLoad)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_COM_EDIT, &CConfigurationEditorDlg::OnBnClickedButtonConfigComEdit)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_COM_SAVE, &CConfigurationEditorDlg::OnBnClickedButtonConfigComSave)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_TX, &CConfigurationEditorDlg::OnEnChangeEditConfigCalTx)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_TY, &CConfigurationEditorDlg::OnEnChangeEditConfigCalTy)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_CAL_LOAD, &CConfigurationEditorDlg::OnBnClickedButtonConfigCalLoad)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_CAL_SAVE, &CConfigurationEditorDlg::OnBnClickedButtonConfigCalSave)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_GLOBAL_OFFSETX, &CConfigurationEditorDlg::OnEnChangeEditConfigCalGlobalOffsetx)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_GLOBAL_OFFSETY, &CConfigurationEditorDlg::OnEnChangeEditConfigCalGlobalOffsety)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_LB_OFFSETX, &CConfigurationEditorDlg::OnEnChangeEditConfigCalLbOffsetx)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_LB_OFFSETY, &CConfigurationEditorDlg::OnEnChangeEditConfigCalLbOffsety)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_LT_OFFSETX, &CConfigurationEditorDlg::OnEnChangeEditConfigCalLtOffsetx)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_LT_OFFSETY, &CConfigurationEditorDlg::OnEnChangeEditConfigCalLtOffsety)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_RT_OFFSETX, &CConfigurationEditorDlg::OnEnChangeEditConfigCalRtOffsetx)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_RT_OFFSETY, &CConfigurationEditorDlg::OnEnChangeEditConfigCalRtOffsety)
	ON_BN_CLICKED(IDC_BUTTON_SET_AUTO_OFFSET, &CConfigurationEditorDlg::OnBnClickedButtonSetAutoOffset)
	ON_BN_CLICKED(IDC_BUTTON_SET_LB_OFFSET, &CConfigurationEditorDlg::OnBnClickedButtonSetLbOffset)
	ON_BN_CLICKED(IDC_BUTTON_SET_LT_OFFSET, &CConfigurationEditorDlg::OnBnClickedButtonSetLtOffset)
	ON_BN_CLICKED(IDC_BUTTON_SET_RT_OFFSET, &CConfigurationEditorDlg::OnBnClickedButtonSetRtOffset)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_FOCUS_CAP2, &CConfigurationEditorDlg::OnEnChangeEditConfigCalFocusCap2)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_FOCUS_CAP1, &CConfigurationEditorDlg::OnEnChangeEditConfigCalFocusCap1)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_FOCUS_CAP3, &CConfigurationEditorDlg::OnEnChangeEditConfigCalFocusCap3)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_FOCUS_CAP4, &CConfigurationEditorDlg::OnEnChangeEditConfigCalFocusCap4)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_INSPECTOR_OFFSETX, &CConfigurationEditorDlg::OnEnChangeEditConfigCalInspectorOffsetx)
	ON_EN_CHANGE(IDC_EDIT_CONFIG_CAL_INSPECTOR_OFFSETY, &CConfigurationEditorDlg::OnEnChangeEditConfigCalInspectorOffsety)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_PHASE_LOAD, &CConfigurationEditorDlg::OnBnClickedButtonConfigPhaseLoad)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_PHASE_SAVE, &CConfigurationEditorDlg::OnBnClickedButtonConfigPhaseSave)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_ETC_LOAD, &CConfigurationEditorDlg::OnBnClickedButtonConfigEtcLoad)
	ON_BN_CLICKED(IDC_BUTTON_CONFIG_ETC_SAVE, &CConfigurationEditorDlg::OnBnClickedButtonConfigEtcSave)
	ON_BN_CLICKED(IDC_BTN_IO_CON, &CConfigurationEditorDlg::OnBnClickedBtnIoCon)
	ON_WM_TIMER()
//	ON_BN_CLICKED(IDC_BTN_NAVI_DISCON, &CConfigurationEditorDlg::OnBnClickedBtnNaviDiscon)
	ON_BN_CLICKED(IDC_BTN_ADAM_CON, &CConfigurationEditorDlg::OnBnClickedBtnAdamCon)
	ON_BN_CLICKED(IDC_BTN_AF_CON, &CConfigurationEditorDlg::OnBnClickedBtnAfCon)
	ON_BN_CLICKED(IDC_BTN_ZP_CON, &CConfigurationEditorDlg::OnBnClickedBtnZpCon)
	ON_BN_CLICKED(IDC_BTN_MIRROR_CON, &CConfigurationEditorDlg::OnBnClickedBtnMirrorCon)
	ON_BN_CLICKED(IDC_BTN_SOURCE_CON, &CConfigurationEditorDlg::OnBnClickedBtnSourceCon)
	ON_BN_CLICKED(IDC_BTN_MTS_CON, &CConfigurationEditorDlg::OnBnClickedBtnMtsCon)
	ON_BN_CLICKED(IDC_BTN_VMTR_CON, &CConfigurationEditorDlg::OnBnClickedBtnVmtrCon)
	ON_BN_CLICKED(IDC_BTN_NAVI_CON, &CConfigurationEditorDlg::OnBnClickedBtnNaviCon)
	ON_BN_CLICKED(IDC_BTN_SCAN_CON, &CConfigurationEditorDlg::OnBnClickedBtnScanCon)
	ON_BN_CLICKED(IDC_BTN_BAEM_CON, &CConfigurationEditorDlg::OnBnClickedBtnBaemCon)
	ON_BN_CLICKED(IDC_BTN_IO_DISCON, &CConfigurationEditorDlg::OnBnClickedBtnIoDiscon)
	ON_BN_CLICKED(IDC_BTN_ADAM_DISCON, &CConfigurationEditorDlg::OnBnClickedBtnAdamDiscon)
	ON_BN_CLICKED(IDC_BTN_AF_DISCON, &CConfigurationEditorDlg::OnBnClickedBtnAfDiscon)
	ON_BN_CLICKED(IDC_BTN_ZP_DISCON, &CConfigurationEditorDlg::OnBnClickedBtnZpDiscon)
	ON_BN_CLICKED(IDC_BTN_MIRROR_DISCON, &CConfigurationEditorDlg::OnBnClickedBtnMirrorDiscon)
	ON_BN_CLICKED(IDC_BTN_SOURCE_DISCON, &CConfigurationEditorDlg::OnBnClickedBtnSourceDiscon)
	ON_BN_CLICKED(IDC_BTN_MTS_DISCON, &CConfigurationEditorDlg::OnBnClickedBtnMtsDiscon)
	ON_BN_CLICKED(IDC_BTN_VMTR_DISCON, &CConfigurationEditorDlg::OnBnClickedBtnVmtrDiscon)
	ON_BN_CLICKED(IDC_BTN_SCAN_DISCON, &CConfigurationEditorDlg::OnBnClickedBtnScanDiscon)
	ON_BN_CLICKED(IDC_BTN_NAVI_DISCON, &CConfigurationEditorDlg::OnBnClickedBtnNaviDiscon)
	ON_BN_CLICKED(IDC_BTN_BAEM_DISCON, &CConfigurationEditorDlg::OnBnClickedBtnBaemDiscon)
	ON_BN_CLICKED(IDC_BTN_NAVI_CON2, &CConfigurationEditorDlg::OnBnClickedBtnNaviCon2)
	ON_STN_CLICKED(IDC_ICON_SCAN_CON_STATE, &CConfigurationEditorDlg::OnStnClickedIconScanConState)
END_MESSAGE_MAP()


// CConfigurationEditorDlg 메시지 처리기


BOOL CConfigurationEditorDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CConfigurationEditorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);

	InitialICon();
	ReadFile();

	//m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));
	m_font.CreateFont(20, 8, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));
	int i = 0;
	if (g_pLog->IsFileExist(PISTAGE_TEST_POSITION_FILE_FULLPATH))
	{
		//ReadPIStageTestPositionFromDB();
		ReadPIStageTestPositionFromDBum();
	}

	if (g_pLog->IsFileExist(STAGEPOSITION_FILE_FULLPATH))
	{
		ReadStagePositionFromDB();
	}
	else
	{
		sprintf(m_stStagePos[99].chStagePositionString, "SYSTEM_LOADING_POSITION");
		sprintf(m_stStagePos[98].chStagePositionString, "SYSTEM_EUV_MASKCENTER_POSITION");
		sprintf(m_stStagePos[97].chStagePositionString, "SYSTEM_OM_MASKCENTER_POSITION");
		sprintf(m_stStagePos[96].chStagePositionString, "SYSTEM_STAGE_ORIGIN_POSITION");
		sprintf(m_stStagePos[95].chStagePositionString, "SYSTEM_EUV_LWS_POSITION");
		sprintf(m_stStagePos[94].chStagePositionString, "SYSTEM_OM_LWS_POSITION");
		sprintf(m_stStagePos[93].chStagePositionString, "SYSTEM_TBD14_POSITION");
		sprintf(m_stStagePos[92].chStagePositionString, "SYSTEM_TBD13_POSITION");
		sprintf(m_stStagePos[91].chStagePositionString, "SYSTEM_TBD12_POSITION");
		sprintf(m_stStagePos[90].chStagePositionString, "SYSTEM_TBD11_POSITION");
		sprintf(m_stStagePos[89].chStagePositionString, "SYSTEM_TBD10_POSITION");
		sprintf(m_stStagePos[88].chStagePositionString, "SYSTEM_TBD9_POSITION");
		sprintf(m_stStagePos[87].chStagePositionString, "SYSTEM_TBD8_POSITION");
		sprintf(m_stStagePos[86].chStagePositionString, "SYSTEM_TBD7_POSITION");
		sprintf(m_stStagePos[85].chStagePositionString, "SYSTEM_TBD6_POSITION");
		sprintf(m_stStagePos[84].chStagePositionString, "SYSTEM_TBD5_POSITION");
		sprintf(m_stStagePos[83].chStagePositionString, "SYSTEM_TBD4_POSITION");
		sprintf(m_stStagePos[82].chStagePositionString, "SYSTEM_TBD3_POSITION");
		sprintf(m_stStagePos[81].chStagePositionString, "SYSTEM_TBD2_POSITION");
		sprintf(m_stStagePos[80].chStagePositionString, "SYSTEM_TBD1_POSITION");
		for (i = MAX_STAGE_POSITION - 21; i >= 0; i--)
		{
			sprintf(m_stStagePos[i].chStagePositionString, "USER_TBD%02d_POSITION", i);
		}
		SaveStagePositionToDB();
	}

	m_MtsRotateAngleCtrl.AddString(_T("90"));
	m_MtsRotateAngleCtrl.AddString(_T("180"));
	m_MtsRotateAngleCtrl.AddString(_T("270"));
	m_MtsRotateAngleCtrl.SetCurSel(0);
	m_UseMtsRotateCtrl.SetCheck(m_bUseRotate);

		ReadETCInfo();
		ReadCalibrationInfo();
		ReadRecoveryData();
		ReadVacuumSeqData();
		ReadPhaseInfo();
		ReadPTRInfo();

	ReadAdamInfo();
	ReadFilterInfo();

	UpdateData(FALSE);

	SetTimer(PORT_CHECK_TIMER, 1000, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CConfigurationEditorDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	KillTimer(PORT_CHECK_TIMER);

}


void CConfigurationEditorDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case PORT_CHECK_TIMER:
		PortCheck();
		SetTimer(nIDEvent, 1000, NULL);
		break;
	default:
		break;
	}

	__super::OnTimer(nIDEvent);
}

int CConfigurationEditorDlg::SaveFile()
{
	int nRet = 0, i = 0;
	char chTmp[125];
	CString strSector, strKey;

	return nRet;
}

int CConfigurationEditorDlg::ReadFile()
{
	int nRet = 0, i = 0;
	char chTmp[125];
	CString strSector,strKey;
	HWND hwnd_com_port, hwnd_rate, hwnd_bit, hwnd_stop, hwnd_parity, hwnd_ip, hwnd_ip_port;
	

	strSector.Format(_T("GENERAL_CONFIG"));
	strKey.Format(_T("EQUIPMENT_MODE"));
	nRet = GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
	if (nRet == 0)
	{
		nRet = -1;
		return nRet;
	}
	m_nEquipmentMode = atoi(chTmp);

	strKey.Format(_T("EQUIPMENT_TYPE"));
	nRet = GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
	if (nRet == 0)
	{
		nRet = -1;
		return nRet;
	}
	m_nEquipmentType = atoi(chTmp);

	strSector.Format(_T("COMMUNICATION_CONFIG"));
	for (i = 0; i < ETHERNET_COM_NUMBER; i++)
	{
		strKey.Format(_T("ETHERNET_NAME_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chNAME[i], chTmp);
		strKey.Format(_T("ETHERNET_IP_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chIP[i], chTmp);
		strKey.Format(_T("ETHERNET_PORT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nPORT[i] = atoi(chTmp);
		sprintf(m_chIPPORT[i], chTmp);
	}
	
	for (int nIdx = 0; nIdx < ETHERNET_COM_NUMBER; nIdx++)
	{
		GetDlgItem(IDC_STATIC_IP_NAME_0 + nIdx)->SetWindowText(m_chNAME[nIdx]);
		GetDlgItem(IDC_EDIT_CREVIS + nIdx)->SetWindowText(m_chIP[nIdx]);
		GetDlgItem(IDC_EDIT_CREVIS_PORT + nIdx)->SetWindowText(m_chIPPORT[nIdx]);
		GetDlgItem(IDC_STATIC_IP_NAME_0 + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_CREVIS + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_CREVIS_PORT + nIdx)->SetFont(&m_font);
	}

	for (int nIdx = 0; nIdx < ETHERNET_COM_NUMBER; nIdx++)
	{
		hwnd_ip = ::GetDlgItem(m_hWnd, IDC_EDIT_CREVIS + nIdx);
		hwnd_ip_port = ::GetDlgItem(m_hWnd, IDC_EDIT_CREVIS_PORT + nIdx);
		SendMessageW(hwnd_ip, EM_SETREADONLY, true, 0);
		SendMessageW(hwnd_ip_port, EM_SETREADONLY, true, 0);
	}


	for (i = 0; i < SERIAL_COM_NUMBER; i++)
	{
		strKey.Format(_T("SERIAL_PORT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chPORT[i], chTmp);
		strKey.Format(_T("SERIAL_BAUD_RATE_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nBAUD_RATE[i] = atoi(chTmp);
		sprintf(m_ch_nBAUD_RATE[i], chTmp);
		strKey.Format(_T("SERIAL_USE_BIT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nUSE_BIT[i] = atoi(chTmp);
		sprintf(m_ch_nUSE_BIT[i], chTmp);
		strKey.Format(_T("SERIAL_STOP_BIT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nSTOP_BIT[i] = atoi(chTmp);
		sprintf(m_ch_nSTOP_BIT[i], chTmp);
		strKey.Format(_T("SERIAL_PARITY_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nPARITY[i] = atoi(chTmp);
		sprintf(m_ch_nPARITY[i], chTmp);
	}

	for (int nIdx = 0; nIdx < SERIAL_COM_NUMBER; nIdx++)
	{
		GetDlgItem(IDC_EDIT_ISOLATOR_PORT + nIdx)->SetWindowText(m_chPORT[nIdx]);
		GetDlgItem(IDC_EDIT_ISOLATOR_BAUD_RATE + nIdx)->SetWindowText(m_ch_nBAUD_RATE[nIdx]);
		GetDlgItem(IDC_EDIT_ISOLATOR_USE_BIT + nIdx)->SetWindowText(m_ch_nUSE_BIT[nIdx]);
		GetDlgItem(IDC_EDIT_ISOLATOR_STOP_BIT + nIdx)->SetWindowText(m_ch_nSTOP_BIT[nIdx]);
		GetDlgItem(IDC_EDIT_ISOLATOR_PARITY + nIdx)->SetWindowText(m_ch_nPARITY[nIdx]);
		GetDlgItem(IDC_EDIT_ISOLATOR_PORT + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_ISOLATOR_BAUD_RATE + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_ISOLATOR_USE_BIT + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_ISOLATOR_STOP_BIT + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_ISOLATOR_PARITY + nIdx)->SetFont(&m_font);
		hwnd_com_port = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_PORT + nIdx);
		hwnd_rate = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_BAUD_RATE + nIdx);
		hwnd_bit = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_USE_BIT + nIdx);
		hwnd_stop = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_STOP_BIT + nIdx);
		hwnd_parity = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_PARITY + nIdx);
		SendMessageW(hwnd_com_port, EM_SETREADONLY, true, 0);
		SendMessageW(hwnd_rate, EM_SETREADONLY, true, 0);
		SendMessageW(hwnd_bit, EM_SETREADONLY, true, 0);
		SendMessageW(hwnd_stop, EM_SETREADONLY, true, 0);
		SendMessageW(hwnd_parity, EM_SETREADONLY, true, 0);
	}

	//20190627 jkseo, Read IO map list from config file.
	//20190902 jhkim, Read IO Map list Modify.

	strSector.Format(_T("DIGITAL_INPUT"));
	for (i = 0; i < DIGITAL_INPUT_NUMBER; i++)
	{
		strKey.Format(_T("%03d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, IOMAP_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chDi[i], chTmp);
	}

	strSector.Format(_T("DIGITAL_OUTPUT"));
	for (i = 0; i < DIGITAL_OUTPUT_NUMBER; i++)
	{
		strKey.Format(_T("%03d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, IOMAP_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chDo[i], chTmp);
	}

	strSector.Format(_T("ANALOG_INPUT"));
	for (int nIdx = 0; nIdx < ANALOG_INPUT_NUMBER; nIdx++)
	{
		strKey.Format(_T("%03d"), nIdx);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, IOMAP_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chAi[nIdx], chTmp);
	}

	strSector.Format(_T("ANALOG_OUTPUT"));
	for (int nIdx = 0; nIdx < ANALOG_OUTPUT_NUMBER; nIdx++)
	{
		strKey.Format(_T("%03d"), nIdx);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, IOMAP_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chAo[nIdx], chTmp);
	}

	return nRet;
}


void CConfigurationEditorDlg::SaveStagePositionToDB()
{
	CString strAppName, strString;

	for (int i = 0; i < MAX_STAGE_POSITION; i++)
	{
		strAppName.Format("STAGE_POSITION_%02d", i);
		strString.Format("%s", m_stStagePos[i].chStagePositionString);
		WritePrivateProfileString(strAppName, "NAME", strString, STAGEPOSITION_FILE_FULLPATH);
		strString.Format("%4.6lf", m_stStagePos[i].x);
		WritePrivateProfileString(strAppName, "X(mm)", strString, STAGEPOSITION_FILE_FULLPATH);
		strString.Format("%4.6lf", m_stStagePos[i].y);
		WritePrivateProfileString(strAppName, "Y(mm)", strString, STAGEPOSITION_FILE_FULLPATH);
	}
}

void CConfigurationEditorDlg::BackupStagePositionToDB(CString sPath)
{
	CString strAppName, strString;

	for (int i = 0; i < MAX_STAGE_POSITION; i++)
	{
		strAppName.Format("STAGE_POSITION_%02d", i);
		strString.Format("%s", m_stStagePos[i].chStagePositionString);
		WritePrivateProfileString(strAppName, "NAME", strString, sPath);
		strString.Format("%4.6lf", m_stStagePos[i].x);
		WritePrivateProfileString(strAppName, "X(mm)", strString, sPath);
		strString.Format("%4.6lf", m_stStagePos[i].y);
		WritePrivateProfileString(strAppName, "Y(mm)", strString, sPath);
	}
}

void CConfigurationEditorDlg::ReadVacuumSeqData()
{
		char chTmp[125];
		CString strSector, strKey;

		strSector.Format(_T("VACUUM_SEQUENCE_INFO"));

		//Mc Data
		strKey.Format(_T("Mc_StandBy_Vent_Value"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToVent);
		strKey.Format(_T("Mc_Slow_Vent_Value"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToSlow_Vent);
		strKey.Format(_T("Mc_Fast_Vent_Value"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_Vent_End);

		strKey.Format(_T("Mc_Slow_Rough_Value"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToFast_MC_Rough);
		strKey.Format(_T("Mc_Fast_Rough_Value"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToMCTMP_Rough);
		strKey.Format(_T("Mc_Tmp_Rough_Value"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_Rough_End);

		strKey.Format(_T("Mc_StandBy_Vent_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_MCLLCVent);
		strKey.Format(_T("Mc_Slow_Vent_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_MCSlow1Vent);
		strKey.Format(_T("Mc_Fast_Vent_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_MCFastVent);

		strKey.Format(_T("Mc_Slow_Rough_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_MCSlowRough);
		strKey.Format(_T("Mc_Fast_Rough_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_MCFastRough);
		strKey.Format(_T("Mc_Tmp_Rough_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_MCTmpEnd);


		//Llc Data
		strKey.Format(_T("Llc_StandBy_Vent_Value"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToVent_LLC);
		strKey.Format(_T("Llc_Slow_Vent_Value"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToSlow2_Vent);
		strKey.Format(_T("Llc_Fast_Vent_Value"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_Vent_End);

		strKey.Format(_T("Llc_Slow_Rough_Value"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToFast_Rough);
		strKey.Format(_T("Llc_Fast_Rough_Value"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_ChangeToTMP_Rough);
		strKey.Format(_T("Llc_Tmp_Rough_Value"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%lf", &m_dPressure_Rough_End);

		strKey.Format(_T("Llc_StandBy_Vent_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_LLKStandbyVent);
		strKey.Format(_T("Llc_Slow_Vent_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_LLKSlow1Vent);
		strKey.Format(_T("Llc_Fast_Vent_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_LLKFastVent);

		strKey.Format(_T("Llc_Slow_Rough_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_LLKSlowRough);
		strKey.Format(_T("Llc_Fast_Rough_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_LLKFastRough);
		strKey.Format(_T("Llc_Tmp_Rough_Time"));
		GetPrivateProfileString(strSector, strKey, _T("0"), chTmp, 125, CONFIG_FILE_FULLPATH);
		sscanf(chTmp, "%d", &m_nTimeout_sec_LLKRoughEnd);
}

void CConfigurationEditorDlg::SaveVacuumSeqData()
{
		CString strSector, strKey, strTemp;

		strSector.Format(_T("VACUUM_SEQUENCE_INFO"));

		strKey.Format(_T("Mc_StandBy_Vent_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToVent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Slow_Vent_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToSlow_Vent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Fast_Vent_Value"));
		strTemp.Format("%f", m_dPressure_Vent_End);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);

		strKey.Format(_T("Mc_Slow_Rough_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToFast_MC_Rough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Fast_Rough_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToMCTMP_Rough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Tmp_Rough_Value"));
		strTemp.Format("%f", m_dPressure_Rough_End);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);

		strKey.Format(_T("Mc_StandBy_Vent_Time"));
		strTemp.Format("%d", m_nTimeout_sec_MCLLCVent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Slow_Vent_Time"));
		strTemp.Format("%d", m_nTimeout_sec_MCSlow1Vent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Fast_Vent_Time"));
		strTemp.Format("%d", m_nTimeout_sec_MCFastVent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);

		strKey.Format(_T("Mc_Slow_Rough_Time"));
		strTemp.Format("%d", m_nTimeout_sec_MCSlowRough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Fast_Rough_Time"));
		strTemp.Format("%d", m_nTimeout_sec_MCFastRough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Mc_Tmp_Rough_Time"));
		strTemp.Format("%d", m_nTimeout_sec_MCTmpEnd);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);


		strKey.Format(_T("Llc_StandBy_Vent_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToVent_LLC);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Slow_Vent_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToSlow2_Vent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Fast_Vent_Value"));
		strTemp.Format("%f", m_dPressure_Vent_End);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);

		strKey.Format(_T("Llc_Slow_Rough_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToFast_Rough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Fast_Rough_Value"));
		strTemp.Format("%f", m_dPressure_ChangeToTMP_Rough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Tmp_Rough_Value"));
		strTemp.Format("%f", m_dPressure_Rough_End);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);

		strKey.Format(_T("Llc_StandBy_Vent_Time"));
		strTemp.Format("%d", m_nTimeout_sec_LLKStandbyVent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Slow_Vent_Time"));
		strTemp.Format("%d", m_nTimeout_sec_LLKSlow1Vent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Fast_Vent_Time"));
		strTemp.Format("%d", m_nTimeout_sec_LLKFastVent);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);

		strKey.Format(_T("Llc_Slow_Rough_Time"));
		strTemp.Format("%d", m_nTimeout_sec_LLKSlowRough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Fast_Rough_Time"));
		strTemp.Format("%d", m_nTimeout_sec_LLKFastRough);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
		strKey.Format(_T("Llc_Tmp_Rough_Time"));
		strTemp.Format("%d", m_nTimeout_sec_LLKRoughEnd);
		WritePrivateProfileString(strSector, strKey, strTemp, CONFIG_FILE_FULLPATH);
}

void CConfigurationEditorDlg::ReadStagePositionFromDB()
{
	CString strAppName;
	char chGetString[128];
	int i = 0;

	for (i = 0; i < MAX_STAGE_POSITION; i++)
	{
		m_stStagePos[i].x = m_stStagePos[i].y = 0.0;
		sprintf(m_stStagePos[i].chStagePositionString, "");

		strAppName.Format("STAGE_POSITION_%02d", i);
		GetPrivateProfileString(strAppName, "NAME", "", chGetString, DATA_STRING_SIZE, STAGEPOSITION_FILE_FULLPATH);
		strcpy(m_stStagePos[i].chStagePositionString, chGetString);
		GetPrivateProfileString(strAppName, "X(mm)", "0.0", chGetString, DATA_STRING_SIZE, STAGEPOSITION_FILE_FULLPATH);
		m_stStagePos[i].x = atof(chGetString);
		GetPrivateProfileString(strAppName, "Y(mm)", "0.0", chGetString, DATA_STRING_SIZE, STAGEPOSITION_FILE_FULLPATH);
		m_stStagePos[i].y = atof(chGetString);
	}
}

void CConfigurationEditorDlg::ReadPIStageTestPositionFromDB()
{
	CString strAppName;
	char chGetString[128];
	int i = 0;

	for (i = 0; i < 4; i++)
	{
		m_stStagePos_PI[i].x = m_stStagePos_PI[i].y = 0.0;
		sprintf(m_stStagePos_PI[i].chStagePositionString, "");

		strAppName.Format("POINT_%02d", i+1);
		GetPrivateProfileString(strAppName, "NO", "", chGetString, DATA_STRING_SIZE, PISTAGE_TEST_POSITION_FILE_FULLPATH);
		strcpy(m_stStagePos_PI[i].chStagePositionString, chGetString);
		GetPrivateProfileString(strAppName, "X(mm)", "0.0", chGetString, DATA_STRING_SIZE, PISTAGE_TEST_POSITION_FILE_FULLPATH);
		m_stStagePos_PI[i].x = atof(chGetString);
		GetPrivateProfileString(strAppName, "Y(mm)", "0.0", chGetString, DATA_STRING_SIZE, PISTAGE_TEST_POSITION_FILE_FULLPATH);
		m_stStagePos_PI[i].y = atof(chGetString);
	}
}

void CConfigurationEditorDlg::ReadPIStageTestPositionFromDBum()
{
	CString strAppName;
	char chGetString[128];
	int i = 0;

	for (i = 0; i < 20; i++)
	{
		m_stStageumPos_PI[i].x = m_stStageumPos_PI[i].y = 0.0;
		sprintf(m_stStageumPos_PI[i].chStageumPositionString, "");

		strAppName.Format("POINT_%02d", i + 1);
		GetPrivateProfileString(strAppName, "NO", "", chGetString, DATA_STRING_SIZE, PISTAGE_TEST_POSITION_FILE_FULLPATH);
		strcpy(m_stStageumPos_PI[i].chStageumPositionString, chGetString);
		GetPrivateProfileString(strAppName, "X(um)", "0.0", chGetString, DATA_STRING_SIZE, PISTAGE_TEST_POSITION_FILE_FULLPATH);
		m_stStageumPos_PI[i].x = atof(chGetString);
		GetPrivateProfileString(strAppName, "Y(um)", "0.0", chGetString, DATA_STRING_SIZE, PISTAGE_TEST_POSITION_FILE_FULLPATH);
		m_stStageumPos_PI[i].y = atof(chGetString);
	}
}

void CConfigurationEditorDlg::ReadCalibrationInfo()
{
		CString strAppName, strKeyName;
		char chGetString[DATA_STRING_SIZE];

		strAppName.Format(_T("CALIBRATION_INFO"));

		strKeyName.Format(_T("Scan_Stage_Tx"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dConfigCalTx_urad);
		strKeyName.Format(_T("Scan_Stage_Ty"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dConfigCalTy_urad);

		strKeyName.Format(_T("Global_Offset_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dGlobalOffsetX_mm);
		strKeyName.Format(_T("Global_Offset_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dGlobalOffsetY_mm);
		strKeyName.Format(_T("LB_Align_Offset_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dLBOffsetX_mm);
		strKeyName.Format(_T("LB_Align_Offset_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dLBOffsetY_mm);
		strKeyName.Format(_T("LT_Align_Offset_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dLTOffsetX_mm);
		strKeyName.Format(_T("LT_Align_Offset_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dLTOffsetY_mm);
		strKeyName.Format(_T("RT_Align_Offset_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dRTOffsetX_mm);
		strKeyName.Format(_T("RT_Align_Offset_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dRTOffsetY_mm);

		strKeyName.Format(_T("LB_LaserSwitching_OffsetX"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dLB_LaserSwitching_OffsetX_mm);
		strKeyName.Format(_T("LB_LaserSwitching_OffsetY"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dLB_LaserSwitching_OffsetY_mm);
		strKeyName.Format(_T("LT_LaserSwitching_OffsetX"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dLT_LaserSwitching_OffsetX_mm);
		strKeyName.Format(_T("LT_LaserSwitching_OffsetY"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dLT_LaserSwitching_OffsetY_mm);
		strKeyName.Format(_T("RT_LaserSwitching_OffsetX"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dRT_LaserSwitching_OffsetX_mm);
		strKeyName.Format(_T("RT_LaserSwitching_OffsetY"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dRT_LaserSwitching_OffsetY_mm);

		strKeyName.Format(_T("Cap1_Focus_DistanceZ"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dZdistanceCap1nStage_um);
		strKeyName.Format(_T("Cap2_Focus_DistanceZ"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dZdistanceCap2nStage_um);
		strKeyName.Format(_T("Cap3_Focus_DistanceZ"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dZdistanceCap3nStage_um);
		strKeyName.Format(_T("Cap4_Focus_DistanceZ"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dZdistanceCap4nStage_um);

		strKeyName.Format(_T("InspectorOffsetX"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dInspectorOffsetX_um);
		strKeyName.Format(_T("InspectorOffsetY"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dInspectorOffsetY_um);

	UpdateData(FALSE);
}

void CConfigurationEditorDlg::SaveCalibrationInfo()
{
		CString strAppName, strKeyName, strValue;

		strAppName.Format(_T("CALIBRATION_INFO"));

		strKeyName.Format(_T("Scan_Stage_Tx"));
		strValue.Format("%.1f", m_dConfigCalTx_urad);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("Scan_Stage_Ty"));
		strValue.Format("%.1f", m_dConfigCalTy_urad);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("Global_Offset_X"));
		strValue.Format("%.4f", m_dGlobalOffsetX_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("Global_Offset_Y"));
		strValue.Format("%.4f", m_dGlobalOffsetY_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("LB_Align_Offset_X"));
		strValue.Format("%.4f", m_dLBOffsetX_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("LB_Align_Offset_Y"));
		strValue.Format("%.4f", m_dLBOffsetY_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("LT_Align_Offset_X"));
		strValue.Format("%.4f", m_dLTOffsetX_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("LT_Align_Offset_Y"));
		strValue.Format("%.4f", m_dLTOffsetY_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("RT_Align_Offset_X"));
		strValue.Format("%.4f", m_dRTOffsetX_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("RT_Align_Offset_Y"));
		strValue.Format("%.4f", m_dRTOffsetY_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("LB_LaserSwitching_OffsetX"));
		strValue.Format("%.4f", m_dLB_LaserSwitching_OffsetX_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("LB_LaserSwitching_OffsetY"));
		strValue.Format("%.4f", m_dLB_LaserSwitching_OffsetY_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("LT_LaserSwitching_OffsetX"));
		strValue.Format("%.4f", m_dLT_LaserSwitching_OffsetX_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("LT_LaserSwitching_OffsetY"));
		strValue.Format("%.4f", m_dLT_LaserSwitching_OffsetY_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("RT_LaserSwitching_OffsetX"));
		strValue.Format("%.4f", m_dRT_LaserSwitching_OffsetX_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("RT_LaserSwitching_OffsetY"));
		strValue.Format("%.4f", m_dRT_LaserSwitching_OffsetY_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("Cap1_Focus_DistanceZ"));
		strValue.Format("%.1f", m_dZdistanceCap1nStage_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("Cap2_Focus_DistanceZ"));
		strValue.Format("%.1f", m_dZdistanceCap2nStage_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("Cap3_Focus_DistanceZ"));
		strValue.Format("%.1f", m_dZdistanceCap3nStage_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("Cap4_Focus_DistanceZ"));
		strValue.Format("%.1f", m_dZdistanceCap4nStage_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("InspectorOffsetX"));
		strValue.Format("%.1f", m_dInspectorOffsetX_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("InspectorOffsetY"));
		strValue.Format("%.1f", m_dInspectorOffsetY_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
}

void CConfigurationEditorDlg::ReadRecoveryData()
{
		CString strAppName, strKeyName;
		char chGetString[DATA_STRING_SIZE];

		strAppName.Format(_T("RECOVERY_INFO"));
		strKeyName.Format(_T("OMAlignPointLB_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dOMAlignPointLB_X_mm);
		strKeyName.Format(_T("OMAlignPointLB_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dOMAlignPointLB_Y_mm);
		strKeyName.Format(_T("OMAlignPointLT_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dOMAlignPointLT_X_mm);
		strKeyName.Format(_T("OMAlignPointLT_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dOMAlignPointLT_Y_mm);
		strKeyName.Format(_T("OMAlignPointRT_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dOMAlignPointRT_X_mm);
		strKeyName.Format(_T("OMAlignPointRT_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dOMAlignPointRT_Y_mm);

		strKeyName.Format(_T("EUVAlignPointLB_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVAlignPointLB_X_mm);
		strKeyName.Format(_T("EUVAlignPointLB_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVAlignPointLB_Y_mm);
		strKeyName.Format(_T("EUVAlignPointLT_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVAlignPointLT_X_mm);
		strKeyName.Format(_T("EUVAlignPointLT_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVAlignPointLT_Y_mm);
		strKeyName.Format(_T("EUVAlignPointRT_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVAlignPointRT_X_mm);
		strKeyName.Format(_T("EUVAlignPointRT_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVAlignPointRT_Y_mm);

		strKeyName.Format(_T("NotchAlignPoint1_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dNotchAlignPoint1_X_mm);
		strKeyName.Format(_T("NotchAlignPoint1_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dNotchAlignPoint1_Y_mm);
		strKeyName.Format(_T("NotchAlignPoint2_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dNotchAlignPoint2_X_mm);
		strKeyName.Format(_T("NotchAlignPoint2_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dNotchAlignPoint2_Y_mm);

		strKeyName.Format(_T("EUVNotchAlignPoint1_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVNotchAlignPoint1_X_mm);
		strKeyName.Format(_T("EUVNotchAlignPoint1_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVNotchAlignPoint1_Y_mm);
		strKeyName.Format(_T("EUVNotchAlignPoint2_X"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVNotchAlignPoint2_X_mm);
		strKeyName.Format(_T("EUVNotchAlignPoint2_Y"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dEUVNotchAlignPoint2_Y_mm);

		strKeyName.Format(_T("OMAlignComplete_Flag"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_bOMAlignCompleteFlag);
		strKeyName.Format(_T("EUVAlignComplete_Flag"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_bEUVAlignCompleteFlag);

		//strKeyName.Format(_T("LaserFeedbackAvailable_Flag"));
		//GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		//sscanf(chGetString, "%d", &m_bLaserFeedbackAvailable_Flag);
		strKeyName.Format(_T("LaserFeedback_Flag"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_bLaserFeedback_Flag);

		strKeyName.Format(_T("EUVAlignFOV"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nEUVAlignFOV);
		strKeyName.Format(_T("EUVAlignGrid"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nEUVAlignGrid);

		strKeyName.Format(_T("ZInterlockPosition"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dZInterlock_um);
		strKeyName.Format(_T("ZFocusPosition"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dZFocusPos_um);

		strAppName.Format(_T("MTS_DATA"));
		strKeyName.Format(_T("RotateDone"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_bMtsRotateDone_Flag);
		strKeyName.Format(_T("FlipDone"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_bMtsFlipDone_Flag);

		strAppName.Format(_T("TRANSFER_INFO"));
		strKeyName.Format(_T("CurrentProcess"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nPreviousProcess);
		strKeyName.Format(_T("MaterialLocation"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, RECOVERY_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nMaterialLocation);
}

void CConfigurationEditorDlg::SaveRecoveryData()
{
		CString strAppName, strKeyName, strValue;

		strAppName.Format(_T("RECOVERY_INFO"));
		strKeyName.Format(_T("OMAlignPointLB_X"));
		strValue.Format("%.6f", m_dOMAlignPointLB_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("OMAlignPointLB_Y"));
		strValue.Format("%.6f", m_dOMAlignPointLB_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("OMAlignPointLT_X"));
		strValue.Format("%.6f", m_dOMAlignPointLT_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("OMAlignPointLT_Y"));
		strValue.Format("%.6f", m_dOMAlignPointLT_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("OMAlignPointRT_X"));
		strValue.Format("%.6f", m_dOMAlignPointRT_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("OMAlignPointRT_Y"));
		strValue.Format("%.6f", m_dOMAlignPointRT_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);

		strKeyName.Format(_T("EUVAlignPointLB_X"));
		strValue.Format("%.6f", m_dEUVAlignPointLB_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVAlignPointLB_Y"));
		strValue.Format("%.6f", m_dEUVAlignPointLB_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVAlignPointLT_X"));
		strValue.Format("%.6f", m_dEUVAlignPointLT_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVAlignPointLT_Y"));
		strValue.Format("%.6f", m_dEUVAlignPointLT_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVAlignPointRT_X"));
		strValue.Format("%.6f", m_dEUVAlignPointRT_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVAlignPointRT_Y"));
		strValue.Format("%.6f", m_dEUVAlignPointRT_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);

		strKeyName.Format(_T("NotchAlignPoint1_X"));
		strValue.Format("%.6f", m_dNotchAlignPoint1_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("NotchAlignPoint1_Y"));
		strValue.Format("%.6f", m_dNotchAlignPoint1_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("NotchAlignPoint2_X"));
		strValue.Format("%.6f", m_dNotchAlignPoint2_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("NotchAlignPoint2_Y"));
		strValue.Format("%.6f", m_dNotchAlignPoint2_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);

		strKeyName.Format(_T("EUVNotchAlignPoint1_X"));
		strValue.Format("%.6f", m_dEUVNotchAlignPoint1_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVNotchAlignPoint1_Y"));
		strValue.Format("%.6f", m_dEUVNotchAlignPoint1_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVNotchAlignPoint2_X"));
		strValue.Format("%.6f", m_dEUVNotchAlignPoint2_X_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVNotchAlignPoint2_Y"));
		strValue.Format("%.6f", m_dEUVNotchAlignPoint2_Y_mm);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);

		strKeyName.Format(_T("OMAlignComplete_Flag"));
		strValue.Format("%d", m_bOMAlignCompleteFlag);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVAlignComplete_Flag"));
		strValue.Format("%d", m_bEUVAlignCompleteFlag);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);

		//strKeyName.Format(_T("LaserFeedbackAvailable_Flag"));
		//strValue.Format("%d", m_bLaserFeedbackAvailable_Flag);
		//WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("LaserFeedback_Flag"));
		strValue.Format("%d", m_bLaserFeedback_Flag);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);

		strKeyName.Format(_T("EUVAlignFOV"));
		strValue.Format("%d", m_nEUVAlignFOV);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("EUVAlignGrid"));
		strValue.Format("%d", m_nEUVAlignGrid);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);

		strKeyName.Format(_T("ZInterlockPosition"));
		strValue.Format("%lf", m_dZInterlock_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("ZFocusPosition"));
		strValue.Format("%lf", m_dZFocusPos_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);

		strAppName.Format(_T("MTS_DATA"));
		strKeyName.Format(_T("RotateDone"));
		strValue.Format("%d", m_bMtsRotateDone_Flag);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("FlipDone"));
		strValue.Format("%d", m_bMtsFlipDone_Flag);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);

		strAppName.Format(_T("TRANSFER_INFO"));
		strKeyName.Format(_T("CurrentProcess"));
		strValue.Format("%d", m_nPreviousProcess);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
		strKeyName.Format(_T("MaterialLocation"));
		strValue.Format("%d", m_nMaterialLocation);
		WritePrivateProfileString(strAppName, strKeyName, strValue, RECOVERY_FILE_FULLPATH);
}

void CConfigurationEditorDlg::SaveCurrentProcess(int nProcess)
{
	m_nPreviousProcess = nProcess;

		CString strSector, strKey, strTemp;
		strSector.Format(_T("TRANSFER_INFO"));
		strKey.Format(_T("CurrentProcess"));
		strTemp.Format("%d", m_nPreviousProcess);
		WritePrivateProfileString(strSector, strKey, strTemp, RECOVERY_FILE_FULLPATH);
}

void CConfigurationEditorDlg::SaveMaterialLocation(int nLoc)
{
	m_nMaterialLocation = nLoc;

		CString strSector, strKey, strTemp;
		strSector.Format(_T("TRANSFER_INFO"));
		strKey.Format(_T("MaterialLocation"));
		strTemp.Format("%d", m_nMaterialLocation);
		WritePrivateProfileString(strSector, strKey, strTemp, RECOVERY_FILE_FULLPATH);
}

void CConfigurationEditorDlg::SaveMtsRecoveryData()
{
		CString strSector, strKey, strTemp;
		strSector.Format(_T("MTS_DATA"));
		strKey.Format(_T("RotateDone"));
		strTemp.Format("%d", m_bMtsRotateDone_Flag);
		WritePrivateProfileString(strSector, strKey, strTemp, RECOVERY_FILE_FULLPATH);
}

int CConfigurationEditorDlg::EthernetRead()
{
	CString strKey, strSector;
	char chTmp[125];
	int nRet = 0;

	strSector.Format(_T("COMMUNICATION_CONFIG"));
	for (int i = 0; i < ETHERNET_COM_NUMBER; i++)
	{
		strKey.Format(_T("ETHERNET_NAME_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chNAME[i], chTmp);
		strKey.Format(_T("ETHERNET_IP_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chIP[i], chTmp);
		strKey.Format(_T("ETHERNET_PORT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		//m_nPORT[i] = atoi(chTmp);
		sprintf(m_chIPPORT[i], chTmp);
	}

	for (int nIdx = 0; nIdx < ETHERNET_COM_NUMBER; nIdx++)
	{
		GetDlgItem(IDC_STATIC_IP_NAME_0 + nIdx)->SetWindowText(m_chNAME[nIdx]);
		GetDlgItem(IDC_EDIT_CREVIS + nIdx)->SetWindowText(m_chIP[nIdx]);
		GetDlgItem(IDC_EDIT_CREVIS_PORT + nIdx)->SetWindowText(m_chIPPORT[nIdx]);
		GetDlgItem(IDC_STATIC_IP_NAME_0 + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_CREVIS + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_CREVIS_PORT + nIdx)->SetFont(&m_font);
	}

	return nRet;
}

int CConfigurationEditorDlg::EthernetSave()
{
	int nRet = 0, i = 0;
	char chTmp[125];
	CString strSector, strKey, str;

	strSector.Format(_T("COMMUNICATION_CONFIG"));

	for (i = 0; i < ETHERNET_COM_NUMBER; i++)
	{
		strKey.Format(_T("ETHERNET_NAME_%d"), i);
		GetDlgItem(IDC_STATIC_IP_NAME_0 + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		strKey.Format(_T("ETHERNET_IP_%d"), i);
		GetDlgItem(IDC_EDIT_CREVIS + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		strKey.Format(_T("ETHERNET_PORT_%d"), i);
		GetDlgItem(IDC_EDIT_CREVIS_PORT + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
	}
	
	return nRet;
}

int CConfigurationEditorDlg::SerialRead()
{
	CString strKey, strSector;
	char chTmp[125];
	int nRet = 0;

	strSector.Format(_T("COMMUNICATION_CONFIG"));
	for (int i = 0; i < SERIAL_COM_NUMBER; i++)
	{
		strKey.Format(_T("SERIAL_PORT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		sprintf(m_chPORT[i], chTmp);
		strKey.Format(_T("SERIAL_BAUD_RATE_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nBAUD_RATE[i] = atoi(chTmp);
		sprintf(m_ch_nBAUD_RATE[i], chTmp);
		strKey.Format(_T("SERIAL_USE_BIT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nUSE_BIT[i] = atoi(chTmp);
		sprintf(m_ch_nUSE_BIT[i], chTmp);
		strKey.Format(_T("SERIAL_STOP_BIT_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nSTOP_BIT[i] = atoi(chTmp);
		sprintf(m_ch_nSTOP_BIT[i], chTmp);
		strKey.Format(_T("SERIAL_PARITY_%d"), i);
		nRet = GetPrivateProfileString(strSector, strKey, _T(""), chTmp, 125, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
		m_nPARITY[i] = atoi(chTmp);
		sprintf(m_ch_nPARITY[i], chTmp);
	}

	for (int nIdx = 0; nIdx < SERIAL_COM_NUMBER; nIdx++)
	{
		GetDlgItem(IDC_EDIT_ISOLATOR_PORT + nIdx)->SetWindowText(m_chPORT[nIdx]);
		GetDlgItem(IDC_EDIT_ISOLATOR_BAUD_RATE + nIdx)->SetWindowText(m_ch_nBAUD_RATE[nIdx]);
		GetDlgItem(IDC_EDIT_ISOLATOR_USE_BIT + nIdx)->SetWindowText(m_ch_nUSE_BIT[nIdx]);
		GetDlgItem(IDC_EDIT_ISOLATOR_STOP_BIT + nIdx)->SetWindowText(m_ch_nSTOP_BIT[nIdx]);
		GetDlgItem(IDC_EDIT_ISOLATOR_PARITY + nIdx)->SetWindowText(m_ch_nPARITY[nIdx]);

		GetDlgItem(IDC_EDIT_ISOLATOR_PORT + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_ISOLATOR_BAUD_RATE + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_ISOLATOR_USE_BIT + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_ISOLATOR_STOP_BIT + nIdx)->SetFont(&m_font);
		GetDlgItem(IDC_EDIT_ISOLATOR_PARITY + nIdx)->SetFont(&m_font);
	}

	return nRet;
}

int CConfigurationEditorDlg::SerialSave()
{
	int nRet = 0, i = 0;
	char chTmp[125];
	CString strSector, strKey, str;

	strSector.Format(_T("COMMUNICATION_CONFIG"));
	for (int i = 0; i < SERIAL_COM_NUMBER; i++)
	{
		strKey.Format(_T("SERIAL_PORT_%d"), i);
		GetDlgItem(IDC_EDIT_ISOLATOR_PORT + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }

		strKey.Format(_T("SERIAL_BAUD_RATE_%d"), i);
		GetDlgItem(IDC_EDIT_ISOLATOR_BAUD_RATE + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }

		strKey.Format(_T("SERIAL_USE_BIT_%d"), i);
		GetDlgItem(IDC_EDIT_ISOLATOR_USE_BIT + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }

		strKey.Format(_T("SERIAL_STOP_BIT_%d"), i);
		GetDlgItem(IDC_EDIT_ISOLATOR_STOP_BIT + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }

		strKey.Format(_T("SERIAL_PARITY_%d"), i);
		GetDlgItem(IDC_EDIT_ISOLATOR_PARITY + i)->GetWindowTextA(str);
		nRet = WritePrivateProfileString(strSector, strKey, str, CONFIG_FILE_FULLPATH);
		if (nRet == 0) { nRet = -1;	return nRet; }
	}

	return nRet;

}


void CConfigurationEditorDlg::OnBnClickedButtonConfigIpLoad()
{
	HWND hwnd, hwnd_port;

	EthernetRead();
	for (int nIdx = 0; nIdx < ETHERNET_COM_NUMBER; nIdx++)
	{
		hwnd = ::GetDlgItem(m_hWnd, IDC_EDIT_CREVIS + nIdx);
		hwnd_port = ::GetDlgItem(m_hWnd, IDC_EDIT_CREVIS_PORT + nIdx);
		SendMessageW(hwnd, EM_SETREADONLY, true, 0);
		SendMessageW(hwnd_port, EM_SETREADONLY, true, 0);
	}

	ip_config_mode = 0; // LOAD MODE
}


void CConfigurationEditorDlg::OnBnClickedButtonConfigIpEdit()
{
	HWND hwnd, hwnd_port;

	for (int nIdx = 0; nIdx < ETHERNET_COM_NUMBER; nIdx++)
	{

		hwnd = ::GetDlgItem(m_hWnd, IDC_EDIT_CREVIS + nIdx);
		hwnd_port = ::GetDlgItem(m_hWnd, IDC_EDIT_CREVIS_PORT + nIdx);
		SendMessageW(hwnd, EM_SETREADONLY, false, 0);
		SendMessageW(hwnd_port, EM_SETREADONLY, false, 0);
	}

	ip_config_mode = 1; // EDIT MODE
}


void CConfigurationEditorDlg::OnBnClickedButtonConfigIpSave()
{
	CPasswordDlg pwdlg(g_pConfig);
	CAutoMessageDlg MsgBoxAuto(g_pConfig);

	int Ret = 0;

	if (ip_config_mode)
	{
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "1234")
		{
			::AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
			return;
		}
		else if (pwdlg.m_strTxt == "1234")
		{
			if (EthernetSave() != 1)
			{
				::AfxMessageBox(" Ethernet Config Save Fail ", MB_ICONSTOP);
				return;
			}
			MsgBoxAuto.DoModal(_T(" Ethernet Config Save 완료 "), 2);
		}

	}
	else
	{
		::AfxMessageBox(" Ethernet Config Load mode 이므로 Save Fail ", MB_ICONSTOP);
		return;
	}
}


void CConfigurationEditorDlg::OnBnClickedButtonConfigComLoad()
{
	HWND hwnd, hwnd_rate, hwnd_bit, hwnd_stop, hwnd_parity;


	SerialRead();

	for (int nIdx = 0; nIdx < SERIAL_COM_NUMBER; nIdx++)
	{
		hwnd = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_PORT + nIdx);
		hwnd_rate = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_BAUD_RATE + nIdx);
		hwnd_bit = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_USE_BIT + nIdx);
		hwnd_stop = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_STOP_BIT + nIdx);
		hwnd_parity = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_PARITY + nIdx);
		SendMessageW(hwnd, EM_SETREADONLY, true, 0);
		SendMessageW(hwnd_rate, EM_SETREADONLY, true, 0);
		SendMessageW(hwnd_bit, EM_SETREADONLY, true, 0);
		SendMessageW(hwnd_stop, EM_SETREADONLY, true, 0);
		SendMessageW(hwnd_parity, EM_SETREADONLY, true, 0);
	}
	serial_config_mode = 0; // LOAD MODE

}


void CConfigurationEditorDlg::OnBnClickedButtonConfigComEdit()
{
	HWND hwnd, hwnd_rate, hwnd_bit, hwnd_stop, hwnd_parity;

	for (int nIdx = 0; nIdx < SERIAL_COM_NUMBER; nIdx++)
	{
		hwnd = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_PORT + nIdx);
		hwnd_rate = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_BAUD_RATE + nIdx);
		hwnd_bit = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_USE_BIT + nIdx);
		hwnd_stop = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_STOP_BIT + nIdx);
		hwnd_parity = ::GetDlgItem(m_hWnd, IDC_EDIT_ISOLATOR_PARITY + nIdx);
		SendMessageW(hwnd, EM_SETREADONLY, false, 0);
		SendMessageW(hwnd_rate, EM_SETREADONLY, false, 0);
		SendMessageW(hwnd_bit, EM_SETREADONLY, false, 0);
		SendMessageW(hwnd_stop, EM_SETREADONLY, false, 0);
		SendMessageW(hwnd_parity, EM_SETREADONLY, false, 0);
	}
	serial_config_mode = 1; // EDIT MODE
}


void CConfigurationEditorDlg::OnBnClickedButtonConfigComSave()
{
	CPasswordDlg pwdlg(g_pConfig);
	CAutoMessageDlg MsgBoxAuto(g_pConfig);

	int Ret = 0;

	if (serial_config_mode)
	{
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "1234")
		{
			::AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
			return;
		}
		else if (pwdlg.m_strTxt == "1234")
		{
			if (SerialSave() != 1)
			{
				::AfxMessageBox(" Serial Config Save Fail ", MB_ICONSTOP);
				return;
			}
			MsgBoxAuto.DoModal(_T(" Serial Config Save 완료 "), 2);
		}

	}
	else
	{
		::AfxMessageBox(" Serial Config Load mode 이므로 Save Fail ", MB_ICONSTOP);
		return;
	}
}

void CConfigurationEditorDlg::OnBnClickedButtonConfigCalLoad()
{
	g_pLog->Display(0, _T("CConfigurationEditorDlg::OnBnClickedButtonConfigCalLoad() 버튼 클릭!"));
	
	ReadCalibrationInfo();
	//UpdateData(FALSE);
}


void CConfigurationEditorDlg::OnBnClickedButtonConfigCalSave()
{
	g_pLog->Display(0, _T("CConfigurationEditorDlg::OnBnClickedButtonConfigCalSave() 버튼 클릭!"));
	
	CPasswordDlg pwdlg(this);
	pwdlg.DoModal();
	if (pwdlg.m_strTxt != "1234")
	{
		::AfxMessageBox("Password가 일치 하지 않습니다.");
		return;
	}

	UpdateData(TRUE);

	SaveCalibrationInfo();
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalTx()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalTy()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalGlobalOffsetx()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalGlobalOffsety()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalLbOffsetx()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalLbOffsety()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalLtOffsetx()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalLtOffsety()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalRtOffsetx()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalRtOffsety()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnBnClickedButtonSetAutoOffset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CConfigurationEditorDlg::OnBnClickedButtonSetLbOffset()
{
	g_pLog->Display(0, _T("CConfigurationEditorDlg::OnBnClickedButtonSetLbOffset() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}

	//if (IDYES != AfxMessageBox("Mask Offset 설정 작업을 시작 하시겠습니까?", MB_YESNO)) return;

	//if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	//{
	//	MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
	//	return ;
	//}

	//AfxMessageBox(" OM화면에서 Align Point를 Center에 두고 확인버튼 눌러주세요.");

	//double posX1 = 0.0, posY1 = 0.0;
	//posX1 = GetPosmm(STAGE_X_AXIS);
	//posY1 = GetPosmm(STAGE_Y_AXIS);

	// EUV로 이동

	//AfxMessageBox(" EUV 화면에서 Align Point를 Center에 두고 확인버튼 눌러주세요.");
	//double posX2 = 0.0, posY2 = 0.0;
	//posX2 = GetPosmm(STAGE_X_AXIS);
	//posY2 = GetPosmm(STAGE_Y_AXIS);

	//m_dLBOffsetX_mm = posX2 - posX1;
	//m_dLBOffsetY_mm = posY2 - posY1;
	UpdateData(FALSE);
}

void CConfigurationEditorDlg::OnBnClickedButtonSetLtOffset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CConfigurationEditorDlg::OnBnClickedButtonSetRtOffset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalFocusCap2()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalFocusCap1()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalFocusCap3()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::OnEnChangeEditConfigCalFocusCap4()
{
	UpdateData(TRUE);
}


void CConfigurationEditorDlg::OnEnChangeEditConfigCalInspectorOffsetx()
{
	UpdateData(TRUE);
}


void CConfigurationEditorDlg::OnEnChangeEditConfigCalInspectorOffsety()
{
	UpdateData(TRUE);
}

void CConfigurationEditorDlg::ReadPhaseInfo()
{
		CString strAppName, strKeyName;
		char chGetString[64];

		strAppName.Format(_T("PHASE_INFO"));

		strKeyName.Format(_T("AlignCcdX"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nAlignCcdX);
		strKeyName.Format(_T("AlignCcdY"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nAlignCcdY);
		strKeyName.Format(_T("AlignCcdWidth"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nAlignCcdWidth);
		strKeyName.Format(_T("AlignCcdHeight"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nAlignCcdHeight);
		strKeyName.Format(_T("AlignCcdBinnig"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nAlignCcdBinning);
		strKeyName.Format(_T("AlignImageX"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nAlignImageX);
		strKeyName.Format(_T("AlignImageY"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nAlignImageY);
		strKeyName.Format(_T("AlignImageWidth"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nAlignImageWidth);
		strKeyName.Format(_T("AlignImageHeight"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nAlignImageHeight);
		strKeyName.Format(_T("AlignExposureTime"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dAlignExposureTime);

		strKeyName.Format(_T("MeasureCcdX"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nMeasureCcdX);
		strKeyName.Format(_T("MeasureCcdY"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nMeasureCcdY);
		strKeyName.Format(_T("MeasureCcdWidth"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nMeasureCcdWidth);
		strKeyName.Format(_T("MeasureCcdHeight"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nMeasureCcdHeight);
		strKeyName.Format(_T("MeasureCcdBinnig"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nMeasureCcdBinning);
		strKeyName.Format(_T("MeasureImageX"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nMeasureImageX);
		strKeyName.Format(_T("MeasureImageY"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nMeasureImageY);
		strKeyName.Format(_T("MeasureImageWidth"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nMeasureImageWidth);
		strKeyName.Format(_T("MeasureImageHeight"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nMeasureImageHeight);
		strKeyName.Format(_T("MeasureExposureTime"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dMeasureExposureTime);


		strKeyName.Format(_T("HorizentalMargin"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dHorizentalMargin_um);
		strKeyName.Format(_T("VerticalMargin"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dVerticalMargin_um);
		strKeyName.Format(_T("MaximumTryCount"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nMaximumTryCount);

		strKeyName.Format(_T("CoarseAlignIntensityTolerancePercent"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dCoarseAlignIntensityTolerancePercent);

		strKeyName.Format(_T("StepWidthX"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dStepWidthX_um);
		strKeyName.Format(_T("HalfNumberOfStepX"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nHalfNumberOfStepX);

		strKeyName.Format(_T("StepWidthY"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dStepWidthY_um);
		strKeyName.Format(_T("HalfNumberOfStepY"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nHalfNumberOfStepY);

		strKeyName.Format(_T("NumOfStdPoint"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nNumOfStdPoint);

		strKeyName.Format(_T("SlitWidth"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dSlitWidth_um);
		strKeyName.Format(_T("SlitPitch"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dSlitPitch_um);
		strKeyName.Format(_T("SlitLength"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dSlitLength_um);

/*
		strKeyName.Format(_T("InterlockMargin_um"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dInterlockMargin_um);
		strKeyName.Format(_T("ZControlTolerance_um"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dZControlTolerance_um);

		strKeyName.Format(_T("Cap1PosLimitMin_um"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dCap1PosLimitMin_um);
		strKeyName.Format(_T("Cap1PosLimitMax_um"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dCap1PosLimitMax_um);

		strKeyName.Format(_T("Cap2PosLimitMin_um"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dCap2PosLimitMin_um);
		strKeyName.Format(_T("Cap2PosLimitMax_um"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dCap2PosLimitMax_um);

		strKeyName.Format(_T("Cap3PosLimitMin_um"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dCap3PosLimitMin_um);
		strKeyName.Format(_T("Cap3PosLimitMax_um"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dCap3PosLimitMax_um);

		strKeyName.Format(_T("Cap4PosLimitMin_um"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dCap4PosLimitMin_um);
		strKeyName.Format(_T("Cap4PosLimitMax_um"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dCap4PosLimitMax_um);

		strKeyName.Format(_T("Cap1Measure"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dCap1MeasurePos_um);
		strKeyName.Format(_T("Cap2Measure"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dCap2MeasurePos_um);
		strKeyName.Format(_T("Cap3Measure"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dCap3MeasurePos_um);
		strKeyName.Format(_T("Cap4Measure"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dCap4MeasurePos_um);

		strKeyName.Format(_T("ZStageLimitMin_um"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dZStageLimitMin_um);
		strKeyName.Format(_T("ZStageLimitMax_um"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dZStageLimitMax_um);*/

		strKeyName.Format(_T("ImageRotaionAngle"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dImageRotationAngle_deg);

		strKeyName.Format(_T("XrayCameraReadTimeout"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dXrayCameraReadTimeout_ms);

		strKeyName.Format(_T("DownZHeightWhenMove"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dDownZHeightWhenMove_um);

		strKeyName.Format(_T("Nfft"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nNfft);

		strKeyName.Format(_T("ContinuousWave"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dContinuousWave);

		strKeyName.Format(_T("ContinuousWaveDate"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nContinuousWaveDate);

		strKeyName.Format(_T("BackGroundHeight"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_nBackGroundHeight);
}


void CConfigurationEditorDlg::SavePhaseInfo()
{
		CString strAppName, strKeyName, strValue;

		strAppName.Format(_T("PHASE_INFO"));

		strKeyName.Format(_T("AlignCcdX"));
		strValue.Format("%d", m_nAlignCcdX);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("AlignCcdY"));
		strValue.Format("%d", m_nAlignCcdY);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("AlignCcdWidth"));
		strValue.Format("%d", m_nAlignCcdWidth);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("AlignCcdHeight"));
		strValue.Format("%d", m_nAlignCcdHeight);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("AlignCcdBinnig"));
		strValue.Format("%d", m_nAlignCcdBinning);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("AlignImageX"));
		strValue.Format("%d", m_nAlignImageX);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("AlignImageY"));
		strValue.Format("%d", m_nAlignImageY);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("AlignImageWidth"));
		strValue.Format("%d", m_nAlignImageWidth);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("AlignImageHeight"));
		strValue.Format("%d", m_nAlignImageHeight);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("AlignExposureTime"));
		strValue.Format("%.6f", m_dAlignExposureTime);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("MeasureCcdX"));
		strValue.Format("%d", m_nMeasureCcdX);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("MeasureCcdY"));
		strValue.Format("%d", m_nMeasureCcdY);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("MeasureCcdWidth"));
		strValue.Format("%d", m_nMeasureCcdWidth);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("MeasureCcdHeight"));
		strValue.Format("%d", m_nMeasureCcdHeight);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("MeasureCcdBinnig"));
		strValue.Format("%d", m_nMeasureCcdBinning);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("MeasureImageX"));
		strValue.Format("%d", m_nMeasureImageX);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("MeasureImageY"));
		strValue.Format("%d", m_nMeasureImageY);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("MeasureImageWidth"));
		strValue.Format("%d", m_nMeasureImageWidth);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("MeasureImageHeight"));
		strValue.Format("%d", m_nMeasureImageHeight);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("MeasureExposureTime"));
		strValue.Format("%.6f", m_dMeasureExposureTime);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("HorizentalMargin"));
		strValue.Format("%.6f", m_dHorizentalMargin_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("VerticalMargin"));
		strValue.Format("%.6f", m_dVerticalMargin_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("MaximumTryCount"));
		strValue.Format("%d", m_nMaximumTryCount);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("CoarseAlignIntensityTolerancePercent"));
		strValue.Format("%.6f", m_dCoarseAlignIntensityTolerancePercent);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("StepWidthX"));
		strValue.Format("%.6f", m_dStepWidthX_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("HalfNumberOfStepX"));
		strValue.Format("%d", m_nHalfNumberOfStepX);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("StepWidthY"));
		strValue.Format("%.6f", m_dStepWidthY_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("HalfNumberOfStepY"));
		strValue.Format("%d", m_nHalfNumberOfStepY);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("NumOfStdPoint"));
		strValue.Format("%d", m_nNumOfStdPoint);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);


		strKeyName.Format(_T("SlitWidth"));
		strValue.Format("%.6f", m_dSlitWidth_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("SlitPitch"));
		strValue.Format("%.6f", m_dSlitPitch_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("SlitLength"));
		strValue.Format("%.6f", m_dSlitLength_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

/*
		strKeyName.Format(_T("InterlockMargin_um"));
		strValue.Format("%.6f", m_dInterlockMargin_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("ZControlTolerance_um"));
		strValue.Format("%.6f", m_dZControlTolerance_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);


		strKeyName.Format(_T("Cap1PosLimitMin_um"));
		strValue.Format("%.6f", m_dCap1PosLimitMin_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("Cap1PosLimitMax_um"));
		strValue.Format("%.6f", m_dCap1PosLimitMax_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("Cap2PosLimitMin_um"));
		strValue.Format("%.6f", m_dCap2PosLimitMin_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("Cap2PosLimitMax_um"));
		strValue.Format("%.6f", m_dCap2PosLimitMax_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("Cap3PosLimitMin_um"));
		strValue.Format("%.6f", m_dCap3PosLimitMin_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("Cap3PosLimitMax_um"));
		strValue.Format("%.6f", m_dCap3PosLimitMax_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("Cap4PosLimitMin_um"));
		strValue.Format("%.6f", m_dCap4PosLimitMin_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("Cap4PosLimitMax_um"));
		strValue.Format("%.6f", m_dCap4PosLimitMax_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);


		strKeyName.Format(_T("Cap1Measure"));
		strValue.Format("%.6f", m_dCap1MeasurePos_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("Cap2Measure"));
		strValue.Format("%.6f", m_dCap2MeasurePos_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("Cap3Measure"));
		strValue.Format("%.6f", m_dCap3MeasurePos_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("Cap4Measure"));
		strValue.Format("%.6f", m_dCap4MeasurePos_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("ZStageLimitMin_um"));
		strValue.Format("%.6f", m_dZStageLimitMin_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
		strKeyName.Format(_T("ZStageLimitMax_um"));
		strValue.Format("%.6f", m_dZStageLimitMax_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
*/
		strKeyName.Format(_T("ImageRotaionAngle"));
		strValue.Format("%.6f", m_dImageRotationAngle_deg);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);


		strKeyName.Format(_T("XrayCameraReadTimeout"));
		strValue.Format("%.6f", m_dXrayCameraReadTimeout_ms);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("DownZHeightWhenMove"));
		strValue.Format("%.6f", m_dDownZHeightWhenMove_um);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("Nfft"));
		strValue.Format("%d", m_nNfft);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("ContinuousWave"));
		strValue.Format("%.6f", m_dContinuousWave);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("ContinuousWaveDate"));
		strValue.Format("%d", m_nContinuousWaveDate);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("BackGroundHeight"));
		strValue.Format("%d", m_nBackGroundHeight);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
}

void CConfigurationEditorDlg::ReadPTRInfo()
{
		CString strAppName, strKeyName;
		char chGetString[DATA_STRING_SIZE];

		strAppName.Format(_T("PTR_INFO"));

		strKeyName.Format(_T("D1_Reference"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dD1Ref);

		strKeyName.Format(_T("D2_Reference"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dD2Ref);

		strKeyName.Format(_T("D3_Reference"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dD3Ref);


		strKeyName.Format(_T("D1_Reference_Std"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dD1RefStd);

		strKeyName.Format(_T("D2_Reference_Std"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dD2RefStd);

		strKeyName.Format(_T("D3_Reference_Std"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_dD3RefStd);

		strKeyName.Format(_T("Reference_Reflectance"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_RefReflectance);
}

void CConfigurationEditorDlg::SavePTRInfo()
{
		CString strAppName, strKeyName, strValue;

		strAppName.Format(_T("PTR_INFO"));

		strKeyName.Format(_T("D1_Reference"));
		strValue.Format("%.6f", m_dD1Ref);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("D2_Reference"));
		strValue.Format("%.6f", m_dD2Ref);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("D3_Reference"));
		strValue.Format("%.6f", m_dD3Ref);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("D1_Reference_Std"));
		strValue.Format("%.6f", m_dD1RefStd);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("D2_Reference_Std"));
		strValue.Format("%.6f", m_dD2RefStd);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("D3_Reference_Std"));
		strValue.Format("%.6f", m_dD3RefStd);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("Reference_Reflectance"));
		strValue.Format("%.6f", m_RefReflectance);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
}

void CConfigurationEditorDlg::OnBnClickedButtonConfigPhaseLoad()
{
	g_pLog->Display(0, _T("CConfigurationEditorDlg::OnBnClickedButtonConfigPhaseLoad() 버튼 클릭!"));

	ReadPhaseInfo();

	UpdateData(FALSE);
}

void CConfigurationEditorDlg::OnBnClickedButtonConfigPhaseSave()
{
	g_pLog->Display(0, _T("CConfigurationEditorDlg::OnBnClickedButtonConfigPhaseSave() 버튼 클릭!"));

	CPasswordDlg pwdlg(this);
	pwdlg.DoModal();
	if (pwdlg.m_strTxt != "1234")
	{
		::AfxMessageBox("Password가 일치 하지 않습니다.");
		return;
	}

	SavePhaseInfo();
}

void CConfigurationEditorDlg::ReadETCInfo()
{
	//Get data from config file
	CString strAppName, strKeyName;
	char chGetString[DATA_STRING_SIZE];

	strAppName.Format(_T("ETC_INFO"));
	strKeyName.Format(_T("UseMtsRotate"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_bUseRotate);
	strKeyName.Format(_T("MtsRotateAngle"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nRotateAngle);
	strKeyName.Format(_T("Auto_Source_Off_Time_sec"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nSourceAutoOffTime_min);
	strKeyName.Format(_T("EUVVacuumRate"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dEUVVacuumRate);

	strAppName.Format(_T("OM_INFO"));
	strKeyName.Format(_T("PixelResolutionX"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dOM_PixelSizeX_um);
	strKeyName.Format(_T("PixelResolutionY"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%lf", &m_dOM_PixelSizeY_um);

	strAppName.Format(_T("OM_STAGE_AREA"));
	strKeyName.Format(_T("LeftBottomPosX"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nOMStageAreaLB_X);
	strKeyName.Format(_T("LeftBottomPosY"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nOMStageAreaLB_Y);
	strKeyName.Format(_T("RightTopPosX"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nOMStageAreaRT_X);
	strKeyName.Format(_T("RightTopPosY"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nOMStageAreaRT_Y);

	strAppName.Format(_T("EUV_STAGE_AREA"));
	strKeyName.Format(_T("LeftBottomPosX"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nEUVStageAreaLB_X);
	strKeyName.Format(_T("LeftBottomPosY"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nEUVStageAreaLB_Y);
	strKeyName.Format(_T("RightTopPosX"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nEUVStageAreaRT_X);
	strKeyName.Format(_T("RightTopPosY"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	sscanf(chGetString, "%d", &m_nEUVStageAreaRT_Y);

	m_rcOMStageAreaRect.SetRect(m_nOMStageAreaLB_X, m_nOMStageAreaLB_Y, m_nOMStageAreaRT_X, m_nOMStageAreaRT_Y);
	m_rcEUVStageAreaRect.SetRect(m_nEUVStageAreaLB_X, m_nEUVStageAreaLB_Y, m_nEUVStageAreaRT_X, m_nEUVStageAreaRT_Y);

	//Set value to controls
	CString sAngle;
	sAngle.Format("%d", m_nRotateAngle);
	int nIdx = m_MtsRotateAngleCtrl.FindStringExact(0, sAngle);
	m_MtsRotateAngleCtrl.SetCurSel(nIdx);
	m_UseMtsRotateCtrl.SetCheck(m_bUseRotate);

	UpdateData(FALSE);
}

void CConfigurationEditorDlg::SaveETCInfo()
{
	UpdateData(TRUE);

	//Get value from controls
	CString sAngle;
	int nIdx = m_MtsRotateAngleCtrl.GetCurSel();
	m_MtsRotateAngleCtrl.GetLBText(nIdx, sAngle);
	m_nRotateAngle = atoi(sAngle);
	m_bUseRotate = m_UseMtsRotateCtrl.GetCheck();

	//Save config data
	CString strAppName, strKeyName, strValue;
	strAppName.Format(_T("ETC_INFO"));
	strKeyName.Format(_T("UseMtsRotate"));
	strValue.Format("%d", m_bUseRotate);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("MtsRotateAngle"));
	strValue.Format("%d", m_nRotateAngle);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("Auto_Source_Off_Time_sec"));
	strValue.Format("%d", m_nSourceAutoOffTime_min);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	strAppName.Format(_T("OM_INFO"));
	strKeyName.Format(_T("PixelResolutionX"));
	strValue.Format("%lf", m_dOM_PixelSizeX_um);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("PixelResolutionY"));
	strValue.Format("%lf", m_dOM_PixelSizeY_um);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	strAppName.Format(_T("OM_STAGE_AREA"));
	strKeyName.Format(_T("LeftBottomPosX"));
	strValue.Format("%d", m_nOMStageAreaLB_X);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("LeftBottomPosY"));
	strValue.Format("%d", m_nOMStageAreaLB_Y);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("RightTopPosX"));
	strValue.Format("%d", m_nOMStageAreaRT_X);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("RightTopPosY"));
	strValue.Format("%d", m_nOMStageAreaRT_Y);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	strAppName.Format(_T("EUV_STAGE_AREA"));
	strKeyName.Format(_T("LeftBottomPosX"));
	strValue.Format("%d", m_nEUVStageAreaLB_X);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("LeftBottomPosY"));
	strValue.Format("%d", m_nEUVStageAreaLB_Y);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("RightTopPosX"));
	strValue.Format("%d", m_nEUVStageAreaRT_X);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("RightTopPosY"));
	strValue.Format("%d", m_nEUVStageAreaRT_Y);
	WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

	m_rcOMStageAreaRect.SetRect(m_nOMStageAreaLB_X, m_nOMStageAreaLB_Y, m_nOMStageAreaRT_X, m_nOMStageAreaRT_Y);
	m_rcEUVStageAreaRect.SetRect(m_nEUVStageAreaLB_X, m_nEUVStageAreaLB_Y, m_nEUVStageAreaRT_X, m_nEUVStageAreaRT_Y);
}

void CConfigurationEditorDlg::ReadAdamInfo()
{
		CString strAppName, strKeyName;
		char chGetString[64];

		strAppName.Format(_T("ADAM_INFO"));

		strKeyName.Format(_T("I0_Reference"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_I0Reference);

		strKeyName.Format(_T("I0_Filter_Window_Size"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_I0FilterWindowSize);

		strKeyName.Format(_T("LOW_PASS_FILTER_SIGMA"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%lf", &m_LowPassFilterSigma);


		strKeyName.Format(_T("ADAM_MAX_WIDTH"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_AdamMaxWidth);

		strKeyName.Format(_T("ADAM_MAX_HEIGHT"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_AdamMaxHeight);

		strKeyName.Format(_T("ADAM_MAX_WIDTH_REGRID"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_AdamMaxWidthRegrid);

		strKeyName.Format(_T("ADAM_MAX_HEIGHT_REGRID"));
		GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
		sscanf(chGetString, "%d", &m_AdamMaxHeightRegrid);
}

void CConfigurationEditorDlg::SaveAdamInfo()
{
		CString strAppName, strKeyName, strValue;

		strAppName.Format(_T("ADAM_INFO"));

		strKeyName.Format(_T("I0_Reference"));
		strValue.Format("%.6f", m_I0Reference);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("I0_Filter_Window_Size"));
		strValue.Format("%d", m_I0FilterWindowSize);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("LOW_PASS_FILTER_SIGMA"));
		strValue.Format("%.6f", m_LowPassFilterSigma);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);


		strKeyName.Format(_T("ADAM_MAX_WIDTH"));
		strValue.Format("%d", m_AdamMaxWidth);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("ADAM_MAX_HEIGHT"));
		strValue.Format("%d", m_AdamMaxHeight);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("ADAM_MAX_WIDTH_REGRID"));
		strValue.Format("%d", m_AdamMaxWidthRegrid);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);

		strKeyName.Format(_T("ADAM_MAX_HEIGHT_REGRID"));
		strValue.Format("%d", m_AdamMaxHeightRegrid);
		WritePrivateProfileString(strAppName, strKeyName, strValue, CONFIG_FILE_FULLPATH);
}

void CConfigurationEditorDlg::OnBnClickedButtonConfigEtcLoad()
{
	ReadETCInfo();
}


void CConfigurationEditorDlg::OnBnClickedButtonConfigEtcSave()
{
	SaveETCInfo();
}

void CConfigurationEditorDlg::ReadFilterInfo()
{
	CString strAppName, strKeyName;
	char chGetString[DATA_STRING_SIZE];

	strAppName.Format(_T("FILTER_INFO"));
	strKeyName.Format(_T("Pos1"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	m_sFilterPos1 = (LPSTR)chGetString;
	strKeyName.Format(_T("Pos2"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	m_sFilterPos2 = (LPSTR)chGetString;
	strKeyName.Format(_T("Pos3"));
	GetPrivateProfileString(strAppName, strKeyName, "", chGetString, DATA_STRING_SIZE, CONFIG_FILE_FULLPATH);
	m_sFilterPos3 = (LPSTR)chGetString;
}

void CConfigurationEditorDlg::SaveFilterInfo()
{
	//Save config data
	CString strAppName, strKeyName;
	strAppName.Format(_T("FILTER_INFO"));
	strKeyName.Format(_T("Pos1"));
	WritePrivateProfileString(strAppName, strKeyName, m_sFilterPos1, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("Pos2"));
	WritePrivateProfileString(strAppName, strKeyName, m_sFilterPos2, CONFIG_FILE_FULLPATH);
	strKeyName.Format(_T("Pos3"));
	WritePrivateProfileString(strAppName, strKeyName, m_sFilterPos3, CONFIG_FILE_FULLPATH);
}



void CConfigurationEditorDlg::InitialICon()
{
	((CStatic*)GetDlgItem(IDC_ICON_IO_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_ADAM_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_AF_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_ZP_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MIRROR_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SOURCE_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_VMTR_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_NAVI_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_BEAM_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SCAN_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_ISO_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_GAUGE_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LIGHT_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_LLCTMP_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FILTER_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_AF2_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_REVOL_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MCTMP_CON_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPARE_CON_STATE))->SetIcon(m_LedIcon[0]);
}

void CConfigurationEditorDlg::PortCheck()
{
	if (g_pAdam != NULL)
	{
		if (g_pAdam->Is_ADAM_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_ADAM_CON_STATE))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_ADAM_CON_STATE))->SetIcon(m_LedIcon[2]);
	}

	if (g_pCamera != NULL)
	{
		//if (g_pCamera->Is_CAM_Connected() == TRUE)
	}


	if (g_pGauge_IO != NULL)
	{
		if (g_pGauge_IO->Is_GAUGE_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_GAUGE_CON_STATE))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_GAUGE_CON_STATE))->SetIcon(m_LedIcon[2]);
	}

	if (g_pIO != NULL)
	{
		if (g_pIO->Is_CREVIS_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_IO_CON_STATE))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_IO_CON_STATE))->SetIcon(m_LedIcon[2]);
	}

	if (g_pEfem != NULL)
	{
		if (g_pEfem->Is_MTS_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_LOCAL_CON_STATE))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_LOCAL_CON_STATE))->SetIcon(m_LedIcon[2]);

	}

	if (g_pNavigationStage != NULL)
	{
		if (g_pNavigationStage->Is_NAVI_Stage_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SOURCE_CON_STATE))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SOURCE_CON_STATE))->SetIcon(m_LedIcon[2]);
	}

	if (g_pFilterStage != NULL)
	{
		if (g_pFilterStage->Is_FILTER_Stage_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_FILTER_CON_STATE))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_FILTER_CON_STATE))->SetIcon(m_LedIcon[2]);
	}

	if (g_pMCTmp_IO != NULL)
	{
		if (g_pMCTmp_IO->Is_MC_Tmp_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_MCTMP_CON_STATE))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MCTMP_CON_STATE))->SetIcon(m_LedIcon[2]);
	}

	if (g_pLLCTmp_IO != NULL)
	{
		if (g_pLLCTmp_IO->Is_LLC_Tmp_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_LLCTMP_CON_STATE))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_LLCTMP_CON_STATE))->SetIcon(m_LedIcon[2]);
	}

	if (g_pVacuumRobot != NULL)
	{
		if (g_pVacuumRobot->Is_VMTR_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_NAVI_CON_STATE))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_NAVI_CON_STATE))->SetIcon(m_LedIcon[2]);

	}

	if (g_pEUVSource != NULL)
	{
		if (g_pEUVSource->Is_SRC_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_VMTR_CON_STATE))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_VMTR_CON_STATE))->SetIcon(m_LedIcon[2]);
	}

	if (g_pLightCtrl != NULL)
	{
		if (g_pLightCtrl->Is_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_LIGHT_CON_STATE))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_LIGHT_CON_STATE))->SetIcon(m_LedIcon[2]);
	}

}


void CConfigurationEditorDlg::PortOpen(int OpenModuleNumber)
{
	int nRet = -1;
	CString str;

	switch (OpenModuleNumber)
	{
	case ETHERNET_CREVIS:
		if (g_pIO != NULL)
		{
			if (g_pConfig->m_nEquipmentMode != OFFLINE)
			{
				if (!g_pIO->m_bCrevis_Open_Port)
				{
					//g_pIO->Closedevice_IO();
					nRet = g_pIO->OpenDevice(g_pConfig->m_chIP[ETHERNET_CREVIS]);
					if (nRet != 0)
					{
						nRet = -87001;
						g_pAlarm->SetAlarm((nRet));
					}
				}
				else
					AfxMessageBox(_T("이미 연결 되어 있습니다"));
			}
		}
		else
		{
			AfxMessageBox(_T("g_pIO == NULL Error"));
		}
		break;
	case ETHERNET_ADAM:
		if (g_pAdam != NULL)
		{

		}
		else
		{

		}
		break;
	case ETHERNET_ZP_CAMERA:
		break;
	case ETHERNET_MTS:
		if (g_pEfem != NULL)
		{
			if (!g_pEfem->Is_MTS_Connected())
			{
				nRet = g_pEfem->OpenDevice();
				if (nRet != 0)
				{
					str.Format(_T("MTS Connection Fail Error : %d"), nRet);
					AfxMessageBox(str);
				}
			}
			else
				AfxMessageBox(_T("이미 연결 되어 있습니다"));
		}
		break;
	case ETHERNET_VMTR:
		if (g_pVacuumRobot != NULL)
		{

		}
		else
		{

		}
		break;
	case ETHERNET_SCAN_STAGE:
		break;
	case ETHERNET_NAVI_STAGE:
		if (g_pNavigationStage != NULL)
		{
			if (!g_pNavigationStage->Is_NAVI_Stage_Connected())
			{
				if (g_pConfig->m_nEquipmentMode != OFFLINE)
				{
					if (!g_pNavigationStage->ConnectACSController(ETHERNET, g_pConfig->m_chIP[ETHERNET_NAVI_STAGE], g_pConfig->m_nPORT[ETHERNET_NAVI_STAGE]))
					{
						AfxMessageBox(_T("Navigation Stage Connection Fail!"), MB_ICONERROR);
						WaitSec(3);
					}
					else
					{
						AfxMessageBox(_T("Navigation Stage Connection Success!"), MB_ICONINFORMATION);
					}
				}
			}
			else 
			{
				AfxMessageBox(_T("Navigation Stage already Connected l!"));
			}
		}
		else
		{
			AfxMessageBox(_T("g_pNavigationStage == NULL Error"));
		}
		break;
	case ETHERNET_MIRROR_SHIFT:
		break;
	case ETHERNET_SRC_PC:
		if (g_pEUVSource != NULL)
		{

		}
		else
		{

		}
		break;
	
	case ETHERNET_LOCAL:
		break;
	case ETHERNET_BEAM:
		break;
	case MODULE_SERIAL_ISOLATOR:
		break;
	case MODULE_SERIAL_VACUUMGAUGE:
		if (g_pGauge_IO != NULL)
		{

		}
		else
		{

		}
		break;
	case MODULE_SERIAL_LIGHT_CTRL:
		if (g_pLightCtrl != NULL)
		{

		}
		else
		{

		}
		break;
	case MODULE_SERIAL_AFMODULE:
		break;
	case MODULE_SERIAL_REVOLVER:
		break;
	case MODULE_SERIAL_MC_TMP:
		if (g_pMCTmp_IO != NULL)
		{

		}
		else
		{

		}
		break;
	case MODULE_SERIAL_LLC_TMP:
		if (g_pLLCTmp_IO != NULL)
		{

		}
		else
		{

		}
		break;
	case MODULE_SERIAL_FILTER_STAGE:
		if (g_pFilterStage != NULL)
		{

		}
		else
		{

		}
		break;
	default:
		break;
	}


}

void CConfigurationEditorDlg::PortClose(int OpenModuleNumber)
{

	switch (OpenModuleNumber)
	{
	case MODULE_ETHERNET_CREVIS:
		if (g_pIO != NULL)
		{

		}
		else
		{

		}
		break;
	case MODULE_ETHERNET_ADAM:
		if (g_pAdam != NULL)
		{

		}
		else
		{

		}
		break;
	case MODULE_ETHERNET_MONITOR_STAGE:
		break;
	case MODULE_ETHERNET_ZP_CAMERA:
		break;
	
	case MODULE_ETHERNET_SCAN_STAGE:
		break;
	case MODULE_ETHERNET_NAVI_STAGE:
		if (g_pNavigationStage != NULL)
		{
			if (g_pNavigationStage->Is_NAVI_Stage_Connected())
				g_pNavigationStage->DisconnectComm();
			else
				AfxMessageBox(_T("Navigation Stage already Disconnected l!"));
		}
		else
		{
			AfxMessageBox(_T("g_pNavigationStage == NULL Error"));
		}
		break;
	case MODULE_ETHERNET_MIRROR_SHIFT:
		break;
	case MODULE_ETHERNET_SRC_PC:
		if (g_pEUVSource != NULL)
		{

		}
		else
		{

		}
		break;
	case MODULE_ETHERNET_VMTR:
		if (g_pVacuumRobot != NULL)
		{

		}
		else
		{

		}
		break;
	case MODULE_ETHERNET_LOCAL:
		break;
	case MODULE_ETHERNET_BEAM:
		break;
	case MODULE_SERIAL_ISOLATOR:
		break;
	case MODULE_SERIAL_VACUUMGAUGE:
		if (g_pGauge_IO != NULL)
		{

		}
		else
		{

		}
		break;
	case MODULE_SERIAL_LIGHT_CTRL:
		if (g_pLightCtrl != NULL)
		{

		}
		else
		{

		}
		break;
	case MODULE_SERIAL_AFMODULE:
		break;
	case MODULE_SERIAL_REVOLVER:
		break;
	case MODULE_SERIAL_MC_TMP:
		if (g_pMCTmp_IO != NULL)
		{

		}
		else
		{

		}
		break;
	case MODULE_SERIAL_LLC_TMP:
		if (g_pLLCTmp_IO != NULL)
		{

		}
		else
		{

		}
		break;
	case MODULE_SERIAL_FILTER_STAGE:
		if (g_pFilterStage != NULL)
		{

		}
		else
		{

		}
		break;
	default:
		break;
	}


}


void CConfigurationEditorDlg::OnBnClickedBtnIoCon()
{
	PortOpen(ETHERNET_CREVIS);
}


void CConfigurationEditorDlg::OnBnClickedBtnAdamCon()
{
	PortOpen(ETHERNET_ADAM);
}


void CConfigurationEditorDlg::OnBnClickedBtnAfCon()
{
	PortOpen(ETHERNET_MONITOR_STAGE);
}


void CConfigurationEditorDlg::OnBnClickedBtnZpCon()
{
	//PortOpen(MODULE_ETHERNET_ZP_CAMERA);
}


void CConfigurationEditorDlg::OnBnClickedBtnMirrorCon()
{
	//PortOpen(MODULE_ETHERNET_MIRROR_SHIFT);
}


void CConfigurationEditorDlg::OnBnClickedBtnSourceCon()
{
	PortOpen(ETHERNET_NAVI_STAGE);
}


void CConfigurationEditorDlg::OnBnClickedBtnMtsCon()
{
	//PortOpen(MODULE_ETHERNET_MTS);
}


void CConfigurationEditorDlg::OnBnClickedBtnVmtrCon()
{
	PortOpen(ETHERNET_SRC_PC);
}


void CConfigurationEditorDlg::OnBnClickedBtnNaviCon()
{
	PortOpen(ETHERNET_VMTR);
}


void CConfigurationEditorDlg::OnBnClickedBtnScanCon()
{
	PortOpen(ETHERNET_LOCAL);
}


void CConfigurationEditorDlg::OnBnClickedBtnBaemCon()
{
	PortOpen(ETHERNET_BEAM);
}


void CConfigurationEditorDlg::OnBnClickedBtnIoDiscon()
{
	PortClose(MODULE_ETHERNET_CREVIS);
}


void CConfigurationEditorDlg::OnBnClickedBtnAdamDiscon()
{
	PortClose(MODULE_ETHERNET_ADAM);
}


void CConfigurationEditorDlg::OnBnClickedBtnAfDiscon()
{
	PortClose(MODULE_ETHERNET_MONITOR_STAGE);
}


void CConfigurationEditorDlg::OnBnClickedBtnZpDiscon()
{
	PortClose(MODULE_ETHERNET_ZP_CAMERA);
}


void CConfigurationEditorDlg::OnBnClickedBtnMirrorDiscon()
{
	PortClose(MODULE_ETHERNET_MIRROR_SHIFT);
}


void CConfigurationEditorDlg::OnBnClickedBtnSourceDiscon()
{
	PortClose(MODULE_ETHERNET_SRC_PC);
}



void CConfigurationEditorDlg::OnBnClickedBtnMtsDiscon()
{
	PortClose(MODULE_ETHERNET_MTS);
}


void CConfigurationEditorDlg::OnBnClickedBtnVmtrDiscon()
{
	PortClose(MODULE_ETHERNET_VMTR);
}


void CConfigurationEditorDlg::OnBnClickedBtnScanDiscon()
{
	PortClose(MODULE_ETHERNET_SCAN_STAGE);
}


void CConfigurationEditorDlg::OnBnClickedBtnNaviDiscon()
{
	PortClose(MODULE_ETHERNET_NAVI_STAGE);
}


void CConfigurationEditorDlg::OnBnClickedBtnBaemDiscon()
{
	PortClose(MODULE_ETHERNET_BEAM);
}


void CConfigurationEditorDlg::OnBnClickedBtnNaviCon2()
{
	PortOpen(ETHERNET_MTS);
}


void CConfigurationEditorDlg::OnStnClickedIconScanConState()
{

}
