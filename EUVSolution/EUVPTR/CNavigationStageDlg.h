﻿/**
 * Navigation Stage Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once

 // CNavigationStageDlg 대화 상자

class CNavigationStageDlg : public CDialogEx, public CACSMC4UCtrl, public IObserver
{
	DECLARE_DYNAMIC(CNavigationStageDlg)

public:
	CNavigationStageDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CNavigationStageDlg();

	// 대화 상자 데이터입니다.
	//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_NAVIGATION_STAGE_DIALOG };
	//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	HICON m_LedIcon[2];

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonZeroreturn();
	afx_msg void OnBnClickedButtonZeroset();
	afx_msg void OnBnClickedButtonXenable();
	afx_msg void OnBnClickedButtonYenable();
	afx_msg void OnBnClickedButtonStop();
	afx_msg void OnBnClickedButtonEstop();
	afx_msg void OnBnClickedButtonXminus();
	afx_msg void OnBnClickedButtonXplus();
	afx_msg void OnBnClickedButtonYplus();
	afx_msg void OnBnClickedButtonYminus();
	afx_msg void OnBnClickedButtonStageready();
	afx_msg void OnEnChangeEditIncstep();
	afx_msg void OnEnChangeEditPosnum();
	afx_msg void OnEnChangeEditGetxpos();
	afx_msg void OnEnChangeEditGetypos();
	afx_msg void OnBnClickedButtonSavepos();
	afx_msg void OnBnClickedButtonMovepos();
	afx_msg void OnBnClickedButtonMoveorigin();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDeltaposIncstepspin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposPosnumspin(NMHDR *pNMHDR, LRESULT *pResult);
	CString m_strMovingDistance;
	CEdit m_XPosCtrl;
	CEdit m_YPosCtrl;
	int m_nStagePosNumber;
	double	m_GetYPos;
	double	m_GetXPos;
	double m_GetZOldPos;
	double	m_dMovingDistance;
	CString m_strGetXPos;
	CString m_strGetYPos;
	CString m_strStagePosition;
	CButton m_chkXEnableCtrl;
	CButton m_chkYEnableCtrl;
	CButton m_chkXInpositionCtrl;
	CButton m_chkYInpositionCtrl;
	CButton m_chkXMovingCtrl;
	CButton m_chkYMovingCtrl;
	CButton m_chkXAccelCtrl;
	CButton m_chkYAccelCtrl;
	CButton m_chkXLoadingPos;
	CButton m_chkYLoadingPos;
	CButton m_chkXPlusLimitCtrl;
	CButton m_chkYPlusLimitCtrl;
	CButton m_chkXMinusLimitCtrl;
	CButton m_chkYMinusLimitCtrl;
	CButton m_chkXSoftPlusLimitCtrl;
	CButton m_chkYSoftPlusLimitCtrl;
	CButton m_chkXSoftMinusLimitCtrl;
	CButton m_chkYSoftMinusLimitCtrl;
	CButton m_chkXMotorFaultCtrl;
	CButton m_chkYMotorFaultCtrl;
	CFont Font;

	void StagePeriodicCheck();
	void ConvertToMaskFromStage(double stageX_mm, double stageY_mm, double& maskCoordX_mm, double& maskCoordY_mm);	
	void ConvertToStageFromMask(double &stageX_mm, double &stageY_mm, double maskCoordX_um, double maskCoordY_um);
	void ConvertToStageFromWafer(double &stageX_mm, double &stageY_mm, double maskCoordX_um, double maskCoordY_um);

	void ConvertToStageFromWaferELitho(double &stageX_mm, double &stageY_mm, double maskCoordX_um, double maskCoordY_um);
	void ConvertToWaferFromStageELitho(double stageX_mm, double stageY_mm, double& maskCoordX_um, double& maskCoordY_um);
	void GetWaferCenterPosAndRotationELitho(double& StageCenterPosX, double& StageCenterPosY, double& RotateCos, double& RotateSin);

	int m_nLoadingPosSensorX;
	int m_nLoadingPosSensorY;


	int MoveToSelectedPointNo(int nSelectedID, int nPointType = FIRSRT_POINT, BOOL bWithoutZdown =FALSE);						//측정할 Index로 이동
	int MoveToSelectedPointNo(double dMaskPixelPosX, double dMaskPixelPosY);
	int MoveToMaskCenterCoordinate(double dMaskX_um, double dMaskY_um, BOOL bWithoutZdown = FALSE);	//측정할 Mask 위치로 이동(Mask 좌표계)

	int MoveToMaskCoordinateWithoutInpositionForPTR(double dMaskX_um, double dMaskY_um, double *target_xpos_out_mm, double *target_ypos_out_mm);	//측정할 Mask 위치로 이동(Mask 좌표계)


	int MoveStageDBPositionNo(int nMesureNo, BOOL bZReturnAfterMove = TRUE);					//Navigation Stage의 저장된 DB Position(0~99번)으로 이동
	int MoveRelativePosition(int nAxis, double dPosmm);			//X,Y 상대위치로 이동(단위 mm)
	int MoveAbsolutePosition(double dXmm, double dYmm, BOOL bZReturnAfterMove = TRUE);			//X,Y 절대위치로 이동(단위 mm)

	//물류 Sequence에서 필요로하는 함수
	int	Move_Loading_Position();							//항상 Not Onthefly, 자동 Sequence에서만 호출된다.
	BOOL		Is_Loading_Positioin();				//Navigation Stage가 Loading Position이면 1, 아니면 0 

	CSpinButtonCtrl m_PosNumSpin;
	CButton m_XEnableCtrl;
	CButton m_YEnableCtrl;

	CGridCtrl m_StagePosGrid2;
	void InitStagePosGrid2();
	afx_msg void OnStagePos_GridClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnStagePos_GridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnStagePos_GridRClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnStagePos_GridStartEdit(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnStagePos_GridEndEdit(NMHDR *pNotifyStruct, LRESULT* pResult);

	CButton m_btn_X_Minus_Ctrl;
	CButton m_btn_X_Plus_Ctrl;
	CButton m_btn_Y_Minus_Ctrl;
	CButton m_btn_Y_Plus_Ctrl;
	afx_msg void OnBnClickedCheckStagemoveAtZorigin();
	CButton m_CheckStageMoveAtZOCtrl;

	////Switching시 Drift 보상용
	//double m_bLaserValueBeforeSwitchingX; //사용안하는 변수임 ihlee 2020.12.24
	//double m_bLaserValueBeforeSwitchingY; //사용안하는 변수임 ihlee 2020.12.24
	//double m_bLaserValueAfterSwitchingX; //사용안하는 변수임 ihlee 2020.12.24
	//double m_bLaserValueAfterSwitchingY; //사용안하는 변수임 ihlee 2020.12.24
	//double m_bDeltaValueAfterSwitchingX; //사용안하는 변수임 ihlee 2020.12.24
	//double m_bDeltaValueAfterSwitchingY; //사용안하는 변수임 ihlee 2020.12.24

	/**
	 * Stage의 각 축의 Laser - Encoder feedback 선택
	 * @param	bOnOff	: On/Off 여부
	 */
	//int SetFeedbackTypeDlg(int nFeedbackType);

	//For Animation
	double	GetCurrentPosX()	 { return GetPosmm(STAGE_X_AXIS); }
	double	GetCurrentPosY()	 { return GetPosmm(STAGE_Y_AXIS); }
	BOOL	Is_NAVI_Stage_Connected()	 { return m_bConnect; }

	/////////////////////////////////
	// Simulator Mode On/OFF 
	/////////////////////////////////
	BOOL	Simulator_Mode;
	
	afx_msg void OnBnClickedButtonFaultclear();

	int Simulator_Start();
	int StageSimulatorSetup();
	void EquipmentMode_Check();

	afx_msg void OnBnClickedButtonStageSimulatorMode();
	afx_msg void OnBnClickedButtonStageNormalMode();
	afx_msg void OnBnClickedButtonStageSimulatorXHome();
	afx_msg void OnBnClickedButtonStageSimulatorYHome();
	afx_msg void OnBnClickedButtonConnect();
	afx_msg void OnBnClickedButtonDisconnect();

	void Navi_Stage_Connected_Check();

	struct _Coordinate
	{
		double X;
		double Y;
	};
	_Coordinate m_WaferRefCoordinate;

	int Start_Scan(double FovX_um, double FovY_um, double StepDistance_um, int nRepeatNo);
	int Stop_Scan();

	void	EmergencyStop();
};
