﻿// CSqOneConfig.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include "CSqOneConfigDlg.h"

// CSqOneConfig 대화 상자

IMPLEMENT_DYNAMIC(CSqOneConfigDlg, CDialogEx)

CSqOneConfigDlg::CSqOneConfigDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SQONECONFIG_DIALOG, pParent)
{
	m_dBeamInitialSetHorizontal = 0.0;
	m_dBeamInitialSetVertical = 0.0;
	m_dBeamInitialSetPitch = 0.0;
	m_dBeamInitialSetYaw = 0.0;
}

CSqOneConfigDlg::~CSqOneConfigDlg()
{
}

void CSqOneConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSqOneConfigDlg, CDialogEx)
	ON_BN_CLICKED(IDC_SQONECONFIG_BUTTON_SETINITIALPARAMETER, &CSqOneConfigDlg::OnBnClickedButtonSqconfigSetinitialparameter)
END_MESSAGE_MAP()


// CSqOneConfig 메시지 처리기


void CSqOneConfigDlg::OnBnClickedButtonSqconfigSetinitialparameter()
{
	CString temp = "";
	GetDlgItemTextA(IDC_SQONECONFIG_EDIT_HORIZONTAL, temp);
	m_dBeamInitialSetHorizontal = atof(temp);
	GetDlgItemTextA(IDC_SQONECONFIG_EDIT_VERTICAL, temp);
	m_dBeamInitialSetVertical = atof(temp);
	GetDlgItemTextA(IDC_SQONECONFIG_EDIT_PITCH, temp);
	m_dBeamInitialSetPitch = atof(temp);
	GetDlgItemTextA(IDC_SQONECONFIG_EDIT_YAW, temp);
	m_dBeamInitialSetYaw = atof(temp);
	g_pBeamMain->setBeamInitail();
	g_pSqOneControl->Display(0, "Beam Initial Parameter Set Button Clicked");
}


BOOL CSqOneConfigDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CSqOneConfigDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CSqOneConfigDlg::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::DestroyWindow();
}

void CSqOneConfigDlg::initializePositionDB()
{
	

}
