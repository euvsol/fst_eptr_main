﻿// CSourceTestDlg.cpp: 구현 파일


#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

// CSourceTestDlg 대화 상자

IMPLEMENT_DYNAMIC(CSourceTestDlg, CDialogEx)

CSourceTestDlg::CSourceTestDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SOURCE_TEST_DIALOG, pParent)
{

}

CSourceTestDlg::~CSourceTestDlg()
{
}

void CSourceTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSourceTestDlg, CDialogEx)
END_MESSAGE_MAP()


// CSourceTestDlg 메시지 처리기
