
// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.





#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC의 리본 및 컨트롤 막대 지원

#include "..\..\CommonModule\DllECommon\CECommon.h"
#include "..\..\CommonModule\DllSerialCom\CSerialCom.h"
#include "..\..\CommonModule\DllEthernetCom\CEthernetCom.h"
#include "..\..\CommonModule\DllACSMC4U\CACSMC4UCtrl.h"
#include "..\..\CommonModule\DllADAM\CADAMAlgorithm.h"
#include "..\..\CommonModule\DllADAM\CADAMCtrl.h"
#include "..\..\CommonModule\DllADAM\AdamPtr.h"
#include "..\..\CommonModule\DllAgilentTwisTorr304TMP\CAgilentTwisTorr304TMPCtrl.h"
#include "..\..\CommonModule\DllAnimationGUI\CAnimationGUICom.h"
#include "..\..\CommonModule\DllCrevisIO\CCrevisIOCtrl.h"
#include "..\..\CommonModule\DllCymechsMTS\CCymechsMTSCtrl.h"
#include "..\..\CommonModule\DllFSTEuvSource\CFSTEuvSourceCtrl.h"
#include "..\..\CommonModule\DllMKS390Gauge\CMKS390Gauge.h"
#include "..\..\CommonModule\DllPfeifferHiPace2300TMP\CPfeifferHiPace2300TMPCtrl.h"
#include "..\..\CommonModule\DllPIE712\CPIE712Ctrl.h"
#include "..\..\CommonModule\DllPIE873\CPIE873Ctrl.h"
#include "..\..\CommonModule\DllRaonVactraVMTR\CRaonVactraVMTRCtrl.h"
#include "..\..\CommonModule\DllAnimationGUI\CAnimationGUICom.h"
#include "..\..\CommonModule\DllMfcUtility\DllMfcUtility.h"
#include "..\..\CommonModule\DllXRayCamera\CXRayCameraCtrl.h"
#include "..\..\CommonModule\DllPhase\CPhaseAlgorithm.h"
#include "..\..\CommonModule\DllLightController\CLightCtrl.h"
#include "..\..\CommonModule\DllSqareOneStageControl\CSqareOneStageControl.h"
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>
#include <afxcontrolbars.h>



#ifdef _DEBUG
#pragma comment(lib, "..\\x64\\Debug\\DllECommon.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllSerialCom.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllEthernetCom.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllACSMC4U.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllADAM.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllAgilentTwisTorr304TMP.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllAnimationGUI.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllCrevisIO.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllCymechsMTS.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllFSTEuvSource.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllMKS390Gauge.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllPfeifferHiPace2300TMP.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllPIE712.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllPIE873.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllRaonVactraVMTR.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllMfcUtility.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllAnimationGUI.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllXRayCamera.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllPhase.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllLightController.lib")
#pragma comment(lib, "..\\x64\\Debug\\DllSqareOneStageControl.lib")
#else
#pragma comment(lib, "..\\x64\\Release\\DllECommon.lib")
#pragma comment(lib, "..\\x64\\Release\\DllSerialCom.lib")
#pragma comment(lib, "..\\x64\\Release\\DllEthernetCom.lib")
#pragma comment(lib, "..\\x64\\Release\\DllACSMC4U.lib")
#pragma comment(lib, "..\\x64\\Release\\DllADAM.lib")
#pragma comment(lib, "..\\x64\\Release\\DllAgilentTwisTorr304TMP.lib")
#pragma comment(lib, "..\\x64\\Release\\DllAnimationGUI.lib")
#pragma comment(lib, "..\\x64\\Release\\DllCrevisIO.lib")
#pragma comment(lib, "..\\x64\\Release\\DllCymechsMTS.lib")
#pragma comment(lib, "..\\x64\\Release\\DllFSTEuvSource.lib")
#pragma comment(lib, "..\\x64\\Release\\DllMKS390Gauge.lib")
#pragma comment(lib, "..\\x64\\Release\\DllPfeifferHiPace2300TMP.lib")
#pragma comment(lib, "..\\x64\\Release\\DllPIE712.lib")
#pragma comment(lib, "..\\x64\\Release\\DllPIE873.lib")
#pragma comment(lib, "..\\x64\\Release\\DllRaonVactraVMTR.lib")
#pragma comment(lib, "..\\x64\\Release\\DllMfcUtility.lib")
#pragma comment(lib, "..\\x64\\Release\\DllAnimationGUI.lib")
#pragma comment(lib, "..\\x64\\Release\\DllXRayCamera.lib")
#pragma comment(lib, "..\\x64\\Release\\DllPhase.lib")
#pragma comment(lib, "..\\x64\\Release\\DllLightController.lib")
#pragma comment(lib, "..\\x64\\Release\\DllSqareOneStageControl.lib")
#endif

#pragma comment(lib,"version.lib")






