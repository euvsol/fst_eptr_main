﻿/**
 * PTR Control Dialog Class
 *
 * Copyright 2020 by E-SOL, Inc.,
 *
 */
#pragma once

// CPTRDlg 대화 상자

class CPTRDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CPTRDlg)

	typedef struct _MOUSEPOSITION
	{
		void Set(MIL_INT DisplayPositionX, MIL_INT DisplayPositionY, MIL_DOUBLE BufferPositionX, MIL_DOUBLE BufferPositionY)
		{
			m_DisplayPositionX = DisplayPositionX;
			m_DisplayPositionY = DisplayPositionY;
			m_BufferPositionX = BufferPositionX;
			m_BufferPositionY = BufferPositionY;
		}
		_MOUSEPOSITION()
		{
			Set(M_INVALID, M_INVALID, M_INVALID, M_INVALID);
		}
		_MOUSEPOSITION& operator=(const _MOUSEPOSITION& MousePosition)
		{
			Set(MousePosition.m_DisplayPositionX,
				MousePosition.m_DisplayPositionY,
				MousePosition.m_BufferPositionX,
				MousePosition.m_BufferPositionY);

			return *this;
		}
		MIL_INT     m_DisplayPositionX;
		MIL_INT     m_DisplayPositionY;
		MIL_DOUBLE  m_BufferPositionX;
		MIL_DOUBLE  m_BufferPositionY;
	} MOUSEPOSITION;

public:
	CPTRDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CPTRDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PTR_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	CGridCtrl m_PTRResultGrid;
	void InitResultGrid();
	void addResultGrid(int nCurrentCoupon, int nCurrentMeasure);

	CGridCtrl m_StageGrid;
	void InitStageGrid();
	void PTRPositionUpdate(double x, double y);

	CGridCtrl m_SensorGrid;
	void InitSensorGrid();
	void SensorGridUpdate();

	int m_PTRResultGridRowCnt = 0;


	afx_msg void OnBnClickedButtonPtrTest();
	afx_msg void OnBnClickedButtonStageBufferRun();
	//afx_msg void OnEnChangeEditDatanum();	
	
	int ReadSensor();
	int ReadSensor(int nDataNum);

	int m_nPTRDataNum;	
	afx_msg void OnBnClickedButtonPtrGetBackground();
	afx_msg void OnBnClickedButtonPtrReadSensor();
	afx_msg void OnBnClickedButtonPtrReadRefReflectance();
	
	int mBackGroundN;	
	int mReadRefReflectanceN;	
	int mMeasureWithOutPellicleN;
	int mMeasureWithPellicleN;

	CString mSaveFilenameDate;

	afx_msg void OnBnClickedButtonPtrMeasureWithoutPellicle();
	afx_msg void OnBnClickedButtonPtrMeasureWithPellicle();
	afx_msg void OnBnClickedButtonCalculatePtr();

	void CalculatePTR(double x_um, double y_um);

	void CalculatePtrForScan(double x_um, double y_um);

	void AllocForScan(int NumX, int NumY);
	unsigned long m_saveCount = 0;
	

	void DeleteForScan();
	void SaveFileForScan();

	void MakeSmallDataForScan(int row);


	void FileStartEnd(BOOL flag);

	void WriteGrid( double x, double y, double T, double R);
	void ClearGrid();

	int mNumOfGridCol = 5;
	int mNumOfGridRow = 300000;
	
	afx_msg void OnBnClickedButtonPtrClearGrid();
		
	void EnableButton(BOOL isEnable);

	double m_dRefPosX = 0;
	double m_dRefPosY = 0;
	double m_dAbsPosX = 0;
	double m_dAbsPosY = 0;
	double m_dRelPosX = 0;
	double m_dRelPosY = 0;
	double m_dMaskPosX = 0;
	double m_dMaskPosY = 0;
	

	afx_msg void OnBnClickedButtonPtrGetRefPos();

	MIL_ID m_MilSystem = M_NULL;
	MIL_ID m_MilDisplay = M_NULL;	
	MIL_ID m_MilColorLut = M_NULL;
	MIL_ID m_MilGraphList = M_NULL;
	MIL_ID m_MilGraphContext = M_NULL;
	MIL_ID m_MilImageUpscale = M_NULL;	


	CWnd *m_pWndDisplay = NULL;
	MOUSEPOSITION m_ImageMousePosition;
	
	
	int m_TotalCouponNum =0;
	int m_CurrenCouponIndex;
	int m_SelectedCounponIndex;

	MIL_INT m_MilImageWidthCurrent = 3;
	MIL_INT m_MilImageHeightCurrent = 3;
	double m_LutPercentMinCurrent;
	double m_LutPercentMaxCurrent;

	// Array
	MIL_ID* m_MilImageArray = M_NULL;
	int* m_MilImageWidthArray = NULL;
	int* m_MilImageHeightArray = NULL;

	double* m_DataMinArray = NULL;
	double* m_DataMaxArray = NULL;


	int* m_CurrentDataNum = NULL;
	
	//삭제예정
	double* m_DataMeanArray = NULL;
	double* m_DataSumArray = NULL;

	int* m_LutMinArray = NULL;
	int* m_LutMaxArray = NULL;

	double** m_ImageDataDoubleArray = NULL;
	UINT16** m_ImageDataUint16Array = NULL;
	
	void DeleteArray();
	void AllocArray(int NumCoupon, int* widthArray, int* heightArray);

	void SelectDisp(int indexDisp);
	void PushDispData(int indexDisp, int indexX, int indexY,double value);
	void UpdateDisplayLut(int low, int high);
	void UpdateLut();

	UINT16 ScaleDoubleToU16bit(double data);
	//double Scale16bitToDoulble(int data);
	double Scale16bitToDoulble(UINT16 data);

	
	int m_dispSelected = -1;	


	const int m_constLutMax = 65536 - 1;
	int m_LutLow =0;
	int m_LutHigh = 65535;
	double m_AutoScaleWidth = 0.5;


	void MilInitialize();
	void MilDestroy();			
		
	//double GetValueFromLut(UINT16 uintValue);
	void ResetPtrDisplay(CProcessData *ProcessData);

	//void UpdateDisplayLut();
	static MIL_INT MFTYPE MouseMoveFct(MIL_INT HookType, MIL_ID EventID, void* UserDataPtr);
	void SetMousePosition(MOUSEPOSITION MousePosition);
	CString m_strMousePosition;
	
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedButtonReadReference();	

	void ReadRefDataFromConfig();

	afx_msg void OnBnClickedButtonSaveReference();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedButtonFit();
	afx_msg void OnBnClickedButtonPtrSetScale();
	afx_msg void OnBnClickedCheckPtrAutoScale();
	BOOL m_bAutoScaleImage;
	afx_msg void OnBnClickedButtonPtrAlgorithmTest();
	CComboBox m_comboCouponSelect;
	afx_msg void OnBnClickedButtonPtrDispTest();	
	afx_msg void OnBnClickedButton3();
	afx_msg void OnSelchangeComboSelectCoupon();
	afx_msg void OnBnClickedButtonPtrScanStart();

	
	double m_XStart_um = -40000;
	double m_XEnd_um = -10000;

	double m_YStart_um = -30000;	
	double m_YEnd_um = 20000;

	double m_XDistance_um = 30000;
	double m_YDistance_um = 10000;

	double m_XInterval_um =20;
	double m_YInterval_um =1000;

	int m_NumOfXGrid =1501;
	int m_NumOfYGrid =51;

	int m_NumOfXGridwithMargin = 0;
	int m_NumOfYGridwithMargin = 0;

	double *m_saveTransmittance = NULL;
	double *m_saveReflectance = NULL;

	double *m_saveTransmittanceSmall = NULL;
	double *m_saveReflectanceSmall = NULL;
	int m_NumOfXGridwithMarginSmall = 0;
	int m_NumOfYGridwithMarginSmall = 0;
	int m_saveWindow;
	unsigned long m_saveCountSmall = 0;
	double m_XIntervalSmall_um = 20;
	double m_YIntervalSmall_um = 1000;
	
	int testCount = 0;
	int testCount_i = 0;
	int testCount_j = 0;
	int poitIndex = 0;
	int IndexCoupon = 0;
	int indexY = 0;
	int indexX = 0;
	
	int IndexCoupon_pre = 0;



	void SetSmallSaveGrid();

	afx_msg void OnBnClickedButtonPtrScanSet();
	afx_msg void OnBnClickedButtonPtrScanSetFromRecipe();
	afx_msg void OnBnClickedButtonPtrTest3();
	
	afx_msg void OnEnChangeEditAutoScaleWidth();
	double m_CurrentMean;
	afx_msg void OnBnClickedButton1();
	int m_UpscaleRatio;
	afx_msg void OnBnClickedButtonUpscale();	
	BOOL m_IsUpScale;
	afx_msg void OnBnClickedCheckUpscale();	


};
