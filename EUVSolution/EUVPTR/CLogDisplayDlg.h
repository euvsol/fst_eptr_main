/**
 * Log Display Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once


// CLogDisplayDlg 대화 상자

class CLogDisplayDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CLogDisplayDlg)

public:
	CLogDisplayDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CLogDisplayDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LOG_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	/**
	* Definition:		로그 디스플레이 및 저장
	* Parameter:		nModule:모듈번호(0:System,1:Vacuum,2:MTS,3:VMTR,4:NavigationStage,5:ScanStage,...,-1:Error), strLogMsg[in] : 로그, bDisplay: 디스플레이여부, bSaveFile 파일저장여부
	* Return:			BOOL 성공여부
	**/
	BOOL Display(int nModule=0, CString strLogMsg=_T(""), BOOL bDisplay=TRUE, BOOL bSaveFile=TRUE);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();

};
