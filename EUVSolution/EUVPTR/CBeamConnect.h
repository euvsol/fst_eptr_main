#pragma once





class CBeamConnect : public CEthernetCom
{
	
public:
	CBeamConnect();
	~CBeamConnect();


protected:

	BOOL				m_bThreadExitFlag;
	BOOL				m_bConnectThreadExitFlag;
	CWinThread*			m_pStatusThread;
	CWinThread*			m_pConnectThread;

	//static UINT	SendDataThread(LPVOID pParam);
	//static UINT	TryConnectThread(LPVOID pParam);

public:
	struct  GetAdam
	{
		double m_Detector1;
		double m_Detector2;
		double m_Detector3;
		double m_Detector4;
		double m_Capsensor1;
		double m_Capsensor2;
		double m_Capsensor3;
		double m_Capsensor4;

	};

	/* Server Connect Flag*/
	BOOL				m_beam_bConnected;


	/* Data On Flag*/
	BOOL				DataReceiveOnFlagForBeam;


	int		OpenDevice();
	void	CloseDevice();

	void	InitializeData();

	int GetReadAdamData();
	int SetSendData();
	
	virtual int On_Accept();
	virtual int	WaitReceiveEventThread();
	virtual	int	ReceiveData(char *lParam, int nLength);
	virtual	int	SendData(int nSize, char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	
	GetAdam GetAdamData;

	CString m_strReceiveEndOfStreamSymbol;

	BOOL m_Adam_Data_On;

	char*	m_BeamConnect_IpAddr;
	int		m_BeamConnect_nPort;

};

