﻿/**
 * Input/Output Dialog Class
 *
 * Copyright 2021 by E-SOL, Inc.,
 *
 */
#pragma once
#include "CIOInputDlg.h"
#include "CIOOutputDlg.h"
#include "CIOInputAnalogDlg.h"
#include "CIOOutputAnalogDlg.h"
#include "CIOPartOutputDlg.h"
#include "CIOPartInputDlg.h"
#include "CIOLinePartInputDlg.h"
#include "CIOLinePartOutputDlg.h"

// CCrevisIODlg 대화 상자

class CIODlg : public CDialogEx , public CCrevisIOCtrl, public CECommon
{
	DECLARE_DYNAMIC(CIODlg)

public:
	CIODlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CIODlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IO_DIALOG };
//#endif
	

protected:
	BOOL						m_bThreadExitFlag;
	CWinThread*					m_pThread;

	BYTE*						m_pInputData;
	BYTE*						m_pOutputData;

	CDigitalInput*				m_pDlgDi;
	CDigitalOutput*				m_pDlgDo;

	CAnalogInput*				m_pDlgAi;
	CAnalogOutput*				m_pDlgAo;

	CDigitalOutputPart*			m_pDlgDoPart;
	CDigitalInputPart*			m_pDlgDiPart;

	CDigitalInputLinePart*		m_pDlgDiLinePart;
	CDigitalOutputLinePart*		m_pDlgDoLinePart;

	CTabCtrl					m_TabCtrlDi;
	CTabCtrl					m_TabCtrlDo;

	CBrush						m_brush;
	CFont						m_font;

	static UINT	IoCheckThread(LPVOID pParam);
	
	void					CreateInputControl(int main_tab);
	void					CreateOutputControl(int main_tab);
	void					GetInputIoData();
	void					GetOutputIoData();
	void					Check_IO_Status();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()

public:
	struct IoMap
	{
		int addr;
		int bit;
	};

	HICON	m_LedIcon[3];

	char* Log_str;						// LOG 기록.
	CString m_StrReturnValue;			// Write Return 기록.

	int m_nIOMode;						// OPER, MAINT MODE 선택 변수
	int m_nERROR_MODE;					// ERROR MODE ON, OFF 선택 변수

	void CreateEventHandle();
	void CloseEventHandle();
	void DigitalInputSetHandle();
	void DigitalOutputSetHandle();

	bool m_bDigitalIn[DIGITAL_INPUT_NUMBER];
	bool m_bDigitalOut[DIGITAL_OUTPUT_NUMBER];
	int m_nDecimal_Addr_In[ANALOG_INPUT_SIZE + DIGITAL_INPUT_SIZE] = { 0 };
	int m_nDecimal_Addr_Out[ANALOG_OUTPUT_SIZE + DIGITAL_OUTPUT_SIZE] = { 0 };

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// IO Digital Input 
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	unsigned int m_nDecimal_Digital_in_Module_1 = 0;
	unsigned int m_nDecimal_Digital_in_Module_2 = 0;
	unsigned int m_nDecimal_Digital_in_Module_3 = 0;
	unsigned int m_nDecimal_Digital_in_Module_4 = 0;
	unsigned int m_nDecimal_Digital_in_Module_5 = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// IO Analog Input 
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	int m_nDecimal_Analog_in_ch_1 = 0;
	int m_nDecimal_Analog_in_ch_2 = 0;
	int	m_nDecimal_Analog_in_ch_3 = 0;
	int	m_nDecimal_Analog_in_ch_4 = 0;
	int	m_nDecimal_Analog_in_ch_5 = 0;
	int	m_nDecimal_Analog_in_ch_6 = 0;
	int	m_nDecimal_Analog_in_ch_7 = 0;
	int	m_nDecimal_Analog_in_ch_8 = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// IO Temperature Monitoring  
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	int	m_nDecimal_Analog_in_ch_9 = 0;
	int	m_nDecimal_Analog_in_ch_10 = 0;
	int	m_nDecimal_Analog_in_ch_11 = 0;
	int	m_nDecimal_Analog_in_ch_12 = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// IO Analog Output 
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	int m_nDecimal_Analog_out_ch_1 = 0;
	int m_nDecimal_Analog_out_ch_2 = 0;
	int	m_nDecimal_Analog_out_ch_3 = 0;
	int	m_nDecimal_Analog_out_ch_4 = 0;
	int m_nDecimal_Analog_out_ch_5 = 0;
	int m_nDecimal_Analog_out_ch_6 = 0;
	int	m_nDecimal_Analog_out_ch_7 = 0;
	int	m_nDecimal_Analog_out_ch_8 = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// IO Digital Output 
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	int m_nDecimal_Digital_out_Module_1 = 0;
	int m_nDecimal_Digital_out_Module_2 = 0;
	int m_nDecimal_Digital_out_Module_3 = 0;
	int m_nDecimal_Digital_out_Module_4 = 0;



	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnSelchangeTabDigitalIn(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnSelchangingTabDigitalIn(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnSelchangeTabDigitalOut(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnSelchangingTabDigitalOut(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnAnalog();
	afx_msg void OnBnClickedBtnDigital();
	afx_msg void OnBnClickedBtnMaint();
	afx_msg void OnBnClickedBtnOper();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedBtnLine();

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// IO Connection Function
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	int OpenDevice(CString strIpAddr);
	BOOL m_bCrevis_Open_Port;	// CREVI IO 연결 성공 여부 변수
	BOOL Is_CREVIS_Connected() { return m_bCrevis_Open_Port; }
	void SupplyLine();

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// IO DisConnection Function
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	void Closedevice_IO();

	clock_t	m_start_time, m_finish_time;

	int DigitalWriteIO(int IODATA, int OnOffFlag);

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// IO On / Open Control Function
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	int On_SignalTowerRed();		//Crevis IO로 정상 명령전달 되었으면 TRUE, 아니면 FALSE 반환
	int On_SignalTowerYellow();
	int On_SignalTowerGreen();
	int On_SignalTowerBuzzer_1();
	int On_SignalTowerBuzzer_2();
	int On_LLCSetPoint_ATM();
	int On_LLCSetPoint_VAC();
	int On_MCSetPoint_ATM();
	int On_MCSetPoint_VAC();
	int Open_WaterSupplyValve();
	int Open_WaterReturnValve();
	int Open_DVR_Valve(int DVRNum);
	int On_LlcDryPump_Valve();
	int On_McDryPump_Valve();
	int Open_CoverDoor();
	int Open_LLCGateValve();				
	int Open_TRGateValve();
	int Open_SlowVent_Inlet_Valve();
	int Open_SlowVent_Outlet_Valve();
	int Open_SlowVentValve();
	int Open_FastVent_Inlet_Valve();
	int Open_FastVent_Outlet_Valve();
	int Open_FastVentValve();
	int Open_MC_SlowRoughValve();
	int Open_MC_FastRoughValve();
	int Open_MC_TMP_GateValve();
	int Open_MC_TMP_ForelineValve();
	int Open_LLC_SlowRoughValve();
	int Open_LLC_FastRoughValve();
	int Open_LLC_TMP_GateValve();
	int Open_LLC_TMP_ForelineValve();
	int Open_SourceGate_Check();		// Source Gate On 확인. ( output )

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// IO Off / Close Control Function
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	int Off_SignalTowerRed();			//Crevis IO로 정상 명령전달 되었으면 TRUE, 아니면 FALSE 반환
	int Off_SignalTowerYellow();
	int Off_SignalTowerGreen();
	int Off_SignalTowerBuzzer_1();
	int Off_SignalTowerBuzzer_2();
	int Off_LLCSetPoint_ATM();
	int Off_LLCSetPoint_VAC();
	int Off_MCSetPoint_ATM();
	int Off_MCSetPoint_VAC();
	int Off_SlowMfc();
	int Off_FastMfc();
	int Close_WaterSupplyValve();
	int Close_WaterReturnValve();
	int Close_DVR_Valve(int DVRNum);
	int Off_LlcDryPump_Valve();
	int Off_McDryPump_Valve();
	int Close_CoverDoor();
	int Close_LLCGateValve();			
	int Close_TRGateValve();
	int Close_SlowVent_Inlet_Valve();
	int Close_SlowVent_Outlet_Valve();
	int Close_SlowVentValve();
	int Close_FastVent_Inlet_Valve();
	int Close_FastVent_Outlet_Valve();
	int Close_FastVentValve();
	int Close_MC_SlowRoughValve();
	int Close_MC_FastRoughValve();
	int Close_MC_TMP_GateValve();
	int Close_MC_TMP_ForelineValve();
	int Close_LLC_SlowRoughValve();
	int Close_LLC_FastRoughValve();
	int Close_LLC_TMP_GateValve();
	int Close_LLC_TMP_ForelineValve();
	int Close_SourceGate_Check();



	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// IO Check Function
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	int Is_SiganlTowerRed();
	int Is_SiganlTowerYellow();
	int Is_SiganlTowerGreen();
	int Is_SiganlTowerBlue();
	int Is_SiganlTowerBuzzer();
	int Is_Water_Supply_Valve_Open();		// DRY , TMP 공급 Water Supply 확인.
	int Is_Water_Return_Valve_Open();		// DRY , TMP 공급 Water Return 확인.
	int Is_DVR_Valve(int DVRNum);
	int Is_LLC_DryPump_Status();			//LLC 상태를 General_define.h에 정의하고, 현재 상태를 return: ex) DRYPUMP_RUN,DRYPUMP_STOP,DRYPUMP_ERROR,DRYPUMP_WARNING,...
	int Is_MC_DryPump_Status();				//MC 상태를 General_define.h에 정의하고, 현재 상태를 return: ex) DRYPUMP_RUN,DRYPUMP_STOP,DRYPUMP_ERROR,DRYPUMP_WARNING,...
	
	//int Is_CoverDoor();
	
	int Is_Water_Supply_Status();			// Main  Water Supply 상태 확인.
	int Is_Water_Supply_Valve_Status();		// Main  Water Supply Valve 상태 확인.
	int Is_Water_Temp_Alarm_Status();		// Main  Water Temp 상태 확인.
	int Is_Air_Supply_Status();				// Main Air Supply 상태 확인.
	int Is_LLCGateValve_Open();				//Valve가 정상 Open되었으면 1, 아니면 0, 잘 모르겠으면 -1 반환 
	int Is_TRGateValve_Open();				//Valve가 정상 Open되었으면 1, 아니면 0, 잘 모르겠으면 -1 반환 
	int Is_SlowVent_Inlet_Valve_Open();
	int Is_SlowVent_Outlet_Valve_Open();
	int Is_SlowVentValve_Open();
	int Is_FastVentValve_Open();
	int Is_FastVent_Inlet_Valve_Open();
	int Is_FastVent_Outlet_Valve_Open();
	int Is_MC_SlowRoughValve_Open();
	int Is_MC_FastRoughValve_Open();
	int Is_MC_TMP_GateValve_Open();
	int Is_MC_TMP_ForelineValve_Open();
	int Is_LLC_SlowRoughValve_Open();
	int Is_LLC_FastRoughValve_Open();
	int Is_LLC_TMP_GateValve_Open();
	int Is_LLC_TMP_ForelineValve_Open();
	int Is_LaserSource_Open();
	int Is_Water_Valve_Open();				// Water Supply , Return 상태 확인. ( 상태 Valve Close 시, Dry, tmp 바로 OFF)
	int Is_Mask_OnChuck();					// Chuck위에 Mask가 정상적으로 있으면 TRUE, 기울었거나, 아니면 FALSE 반환
	int Is_Mask_OnLLC();					// LLC에 Mask가 정상적으로 있으면 TRUE, 기울었거나, 아니면 FALSE 반환
				
	bool Is_LLC_Mask_Slant_Check();			// LLC MASK 기울기 센서 감지 확인(#1, #2 통합)
	bool Is_MC_Mask_Slant_Check();			// MC MASK 기울기 센서 감지 확인(#1, #2 통합)
	bool Is_LLC_Mask_Slant1_Check();		// LLC MASK1 기울기 센서 감지 확인(개별)
	bool Is_LLC_Mask_Slant2_Check();		// LLC MASK2 기울기 센서 감지 확인(개별)
	bool Is_MC_Mask_Slant1_Check();			// MC MASK1 기울기 센서 감지 확인(개별)
	bool Is_MC_Mask_Slant2_Check();			// MC MASK2 기울기 센서 감지 확인(개별)
	int Is_Mask_Check_Only_OnChuck();		// Chuck위에 Mask가 있으면 TRUE, 아니면 FALSE 반환  (기울기 무시, 오로지 마스크만 Check)
	int Is_Mask_Check_Only_OnLLC();			// LLC에 Mask가 있으면 TRUE, 아니면 FALSE 반환 (기울기 무시, 오로지 마스크만 Check)
	int Is_Tmp_Leak_Status();				// Water leak Check (LLC, MC 통합)
	int Is_Tmp_LLC_Leak_Status();			// LLC Water leak Check
	int ls_Tmp_MC_Leak_Status();			// MC Water leak Check
	int Is_detect_Status();					// Smoke detact Check
	int Is_Ac_Rack_detect_Status();			// Smoke detact Check
	int Is_Control_Rack_detect_Status();	// Smoke detact Check
	int Is_SourceGate_OpenOn_Check();		// SOURCE GATE OPEN 가능 여부 확인
	int Is_MC_Lid_Open();					// MC LID OPEN 확인
	int Is_TR_Lid_Open();					// TR LID OPEN 확인
	int Is_LLC_Lid_Open();					// LLC LID OPEN 확인
	int Is_Llc_SetPointStatus();			// LLC Set Point 확인
	int Is_Mc_SetPointStatus();				// MC Set Point  확인
	int Is_Loading_Position_Status();		// Stage Loading Position Check
	bool Is_LLC_Atm_Check();				// LLC ATM 상태 확인
	bool Is_LLC_Vac_Check();				// LLC VAC 상태 확인 
	bool Is_MC_Atm_Check();					// MC ATM 상태 확인
	bool Is_MC_Vac_Check();					// MC VAC 상태 확인 	
	bool Is_LLC_DryLine_VAC_Check();		// LLC Dry Line 진공 상태 확인
	bool Is_MC_DryLine_VAC_Check();			// MC Dry Line 진공 상태 확인 	

	double Is_AC_Rack_TempCheck();
	double Is_Control_Bok_TempCheck();

	bool Is_VAC_Robot_LLC_Arm_Retract();
	bool Is_VAC_Robot_MC_Arm_Retract();

	int Is_Isolator_Up();					//Isolator가 작동 중이면 1, 아니면 0 반환

	/* 확인 필요 */

	int Is_Mask_OnVMTR();					// VMTR에 Mask가 있으면 TRUE, 아니면 FALSE 반환
	int Is_Mask_OnVMTR_LLC();				// VMTR에 Mask가 있으면 TRUE, 아니면 FALSE 반환


	bool Is_VAC_Robot_Arm_Retract();		// VAC ROBOT ARM 확인  (TRUE = retract)
	bool Is_VAC_Robot_Arm_Extend();			// VAC ROBOT ARM 확인 (TRUE = extend)
	bool Is_ATM_Robot_Arm_Retract();		// ARM ROBOt ARM 확인 (TRUE = retract , FALSE = extend)
	bool Is_ATM_Robot_Arm_Extend();			// ARM ROBOt ARM 확인 (TRUE = extend , FALSE = retract)
	

	double Get_MFC2_N2_10sccm_In() { return (double)(m_nDecimal_Analog_in_ch_5) / ANALOG_INPUT_UNIT_CONVERSION; }
	double Get_MFC1_N2_20000sccm_In() { return (double)(m_nDecimal_Analog_in_ch_6) / ANALOG_INPUT_UNIT_CONVERSION; }
	double Get_MFC2_N2_10sccm_Out() { return (double)(m_nDecimal_Analog_out_ch_1) / ANALOG_INPUT_UNIT_CONVERSION; }
	double Get_MFC1_N2_20000sccm_Out() { return (double)(m_nDecimal_Analog_out_ch_2) / ANALOG_INPUT_UNIT_CONVERSION; }
	double Get_AC_Rack_Temp() { return (double)(m_nDecimal_Analog_in_ch_9) / ANALOG_TEMP_UNIT_CONVERSION - 20.0; }
	double Get_ControlBox_Temp() { return (double)(m_nDecimal_Analog_in_ch_10) / ANALOG_TEMP_UNIT_CONVERSION - 20.0; }

	bool m_bAPSequenceNormalCloseFlag;
	bool m_bLLCAtmFlag;
	bool m_bMCAtmFlag;



	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// IO Afxmessage Box YES/NO Function
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	int Str_ok_Box(CString Log, CString str_nCheck);

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// IO Valve Status Return Function
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	int Valve_Check(bool OPEN, bool CLOSE);

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// IO Error Sequence Function
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	void Main_Error_Status();
	int Error_On(int ErrorCode, char* str);
	int MC_Dry_Pump_Error_Sequence(); 	// DRY PUMP ERROR 시 발생 sequence.
	int LLC_Dry_Pump_Error_Sequence();


	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// IO evenct handle
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	HANDLE m_SignalTowerRed_Off_WriteEvent;
	HANDLE m_SignalTowerYello_Off_WriteEvent;
	HANDLE m_SignalTowerGreen_Off_WriteEvent;
	HANDLE m_SignalTowerBuzzer_1_Off_WriteEvent;
	HANDLE m_SignalTowerBuzzer_2_Off_WriteEvent;
	HANDLE m_WaterSupply_Off_WriteEvent;
	HANDLE m_WaterReturn_Off_WriteEvent;
	HANDLE m_DVRCam_1_Off_WriteEvent;
	HANDLE m_DVRCam_2_Off_WriteEvent;
	HANDLE m_DVRCam_3_Off_WriteEvent;
	HANDLE m_DVRCam_4_Off_WriteEvent;
	HANDLE m_LlcDryPump_On_WriteEvent;
	HANDLE m_McDryPump_On_WriteEvent;
	HANDLE m_LlcDryPump_Off_WriteEvent;
	HANDLE m_McDryPump_Off_WriteEvent;
	HANDLE m_LLCGate_WriteEvent;
	HANDLE m_TRGate_WriteEvent;
	HANDLE m_SlowVent_WriteEvent;
	HANDLE m_LLCForelineGate_WriteEvent;
	HANDLE m_LLCTmpGate_WriteEvent;
	HANDLE m_LLCFastRough_WriteEvent;
	HANDLE m_LLCSlowRough_WriteEvent;
	HANDLE m_MCForelineGate_WriteEvent;
	HANDLE m_MCTmpGate_WriteEvent;
	HANDLE m_MCFastRough_WriteEvent;
	HANDLE m_MCSlowRough_WriteEvent;
	HANDLE m_FastVent_WriteEvent;
	HANDLE m_FastVent_Inlet_WriteEvent_Close;
	HANDLE m_SlowVent_Inlet_WriteEvent_Close;
	HANDLE m_SignalTowerRed_On_WriteEvent;
	HANDLE m_SignalTowerYello_On_WriteEvent;
	HANDLE m_SignalTowerGreen_On_WriteEvent;
	HANDLE m_SignalTowerBuzzer_1_On_WriteEvent;
	HANDLE m_SignalTowerBuzzer_2_On_WriteEvent;
	HANDLE m_WaterSupply_On_WriteEvent;
	HANDLE m_WaterReturn_On_WriteEvent;
	HANDLE m_DVRCam_1_On_WriteEvent;
	HANDLE m_DVRCam_2_On_WriteEvent;
	HANDLE m_DVRCam_3_On_WriteEvent;
	HANDLE m_DVRCam_4_On_WriteEvent;
	HANDLE m_LLCGate_WriteEvent_Open;
	HANDLE m_TRGate_WriteEvent_Open;
	HANDLE m_SlowVent_WriteEvent_Open;
	HANDLE m_LLCForelineGate_WriteEvent_Open;
	HANDLE m_LLCTmpGate_WriteEvent_Open;
	HANDLE m_LLCFastRough_WriteEvent_Open;
	HANDLE m_LLCSlowRough_WriteEvent_Open;
	HANDLE m_MCForelineGate_WriteEvent_Open;
	HANDLE m_MCTmpGate_WriteEvent_Open;
	HANDLE m_MCFastRough_WriteEvent_Open;
	HANDLE m_MCSlowRough_WriteEvent_Open;
	HANDLE m_FastVent_WriteEvent_Open;
	HANDLE m_TRGate_Vauccum_Value_On;
	HANDLE m_FastVent_Inlet_WriteEvent_Open;
	HANDLE m_SlowVent_Inlet_WriteEvent_Open;
	HANDLE m_LLCSetPointATM_WriteEvent_Open;
	HANDLE m_LLCSetPointVAC_WriteEvent_Open;
	HANDLE m_MCSetPointATM_WriteEvent_Open;
	HANDLE m_MCSetPointVAC_WriteEvent_Open;
	HANDLE m_LLCSetPointATM_WriteEvent_Close;
	HANDLE m_LLCSetPointVAC_WriteEvent_Close;
	HANDLE m_MCSetPointATM_WriteEvent_Close;
	HANDLE m_MCSetPointVAC_WriteEvent_Close;


	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// IO Digital Output enum Define
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	enum DO{
		SIGNAL_TOWER_RED = 0,
		SIGNAL_TOWER_YELLOW,
		SIGNAL_TOWER_GREEN,
		SIGNAL_TOWER_BUZZER_1,
		SIGNAL_TOWER_BUZZER_2,
		DigitalOutput_5,
		WATER_SUPPLY_VALVE,
		WATER_RETURN_VALVE,
		DigitalOutput_8,
		DVR_CAM1_SW,
		DVR_CAM2_SW,
		DVR_CAM3_SW,
		DVR_CAM4_SW,
		DigitalOutput_13,
		LLC_MASK_CHECK_TRIGGER,
		DigitalOutput_15,
		LLC_DRY_PUMP_SW,
		MC_DRY_PUMP_SW,
		DigitalOutput_18,
		LLC_SLOW_ROUGHING,
		LLC_FAST_ROUGHING,
		LLC_TMP_GATE,
		LLC_FORELINE,
		DigitalOutput_23,
		MC_SLOW_ROUGHING,
		MC_FAST_ROUGHING,
		MC_TMP_GATE,
		MC_FORELINE,
		LLC_GATE_VALVE_OPEN_STATUS,
		LLC_GATE_VALVE_CLOSE_STATUS,
		TR_GATE_VALVE_OPEN_STATUS,
		TR_GATE_VALVE_CLOSE_STATUS,
		SLOW_VENT_INLET,
		SLOW_VENT_OUTLET,
		FAST_VENT_INLET,
		FAST_VENT_OUTLET,
		DigitalOutput_36,
		FRAME_COVER_DOOR_OPEN,
		EUV_SHUTTER_CHECK,
		DigitalOutput_39,
		DigitalOutput_40,
		DigitalOutput_41,
		UTILLITY_SPARE1,
		UTILLITY_SPARE2,
		INTERFACE_BOARD_SPARE1,
		INTERFACE_BOARD_SPARE2,
		DigitalOutput_46,
		DigitalOutput_47,
		DigitalOutput_48,
		DigitalOutput_49,
		DigitalOutput_50,
		DigitalOutput_51,
		DigitalOutput_52,
		DigitalOutput_53,
		LLC_SET_POINT_OUT_1_ATM,
		LLC_SET_POINT_OUT_2_VAC,
		MC_SET_POINT_OUT_1_ATM,
		MC_SET_POINT_OUT_2_VAC,
		DigitalOutput_58,
		DigitalOutput_59,
		DigitalOutput_60,
		DigitalOutput_61,
		DigitalOutput_62,
		DigitalOutput_63,
	};


	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// IO Digital Input enum Define
	//
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	enum DI{
		EMS_ON_STATUS_1_AC_RACK = 0,
		EMS_ON_STATUS_2_MTS_FRONT,
		EMS_ON_STATUS_3_MTS_RIGHT,
		EMS_ON_STATUS_4_MTS_LEFT,
		EMS_ON_STATUS_5_FRAME_1,
		EMS_ON_STATUS_6_FRAME_2,
		EMS_ON_STATUS_7_FRAME_3,
		m_bDigitalin_7,
		m_bDigitalin_8,
		EMS_ON_STATUS_10_SOURCE_RACK,
		m_bDigitalin_10,
		FRAME_DOOR_OPEN_1,
		FRAME_DOOR_OPEN_2,
		FRAME_DOOR_OPEN_3,
		FRAME_DOOR_OPEN_4,
		FRAME_DOOR_OPEN_5,
		CP1_2AXIS_STAGE1,
		CP2_2AXIS_STAGE2,
		CP3_LLC_TMP,
		CP4_MC_TMP,
		CP5_VAC_ROBOT,
		CP6_MTS_MAIN,
		CP7_CONSENT_11A,
		CP8_CONSENT_12A,
		CP9_MC_LINE_SPARE,
		CP10_CONSENT_13A,
		CP11_CONSENT_13B,
		CP12_SAFETY_PLC,
		CP13_CONTROL_DC,
		CP14_IO_MODULE,
		CP15_SENSOR_ETC,
		m_bDigitalin_31,
		CP16_5VDC_EM1024_SMPS,
		CP17_MFC_SMPS,
		CP18_FRAME_FAN_LAMP,
		CP19_PC_MONITOR,
		CP20_ORTEC_SENSOR_AMP,
		CP21_ADAM_PC,
		CP22_MTS_FFU_PC,
		CP23_NF_CONSENT_3,
		m_bDigitalin_40,
		m_bDigitalin_41,
		m_bDigitalin_42,
		m_bDigitalin_43,
		m_bDigitalin_44,
		m_bDigitalin_45,
		m_bDigitalin_46,
		m_bDigitalin_47,
		MAIN_AIR_SW,
		LID_AIR_SW,
		ISOLATOR_AIR_SW,
		WATER_FLOW_SW,
		WATER_TEMP_ALARM,
		LLC_DRY_PUMP_ON_STATUS,
		LLC_DRY_PUMP_ALARM_STATUS,
		LLC_DRY_PUMP_WARNNING_STATUS,
		MC_DRY_PUMP_ON_STATUS,
		MC_DRY_PUMP_ALARM_STATUS,
		MC_DRY_PUMP_WARNNING_STATUS,
		m_bDigitalin_59,
		WATER_LEAK_SENSOR1_LLC_TMP,
		WATER_LEAK_SENSOR2_MC_TMP,
		AC_RACK_SMOKE_DETECTOR,
		CONTROL_RACK_SMOKE_DETECTOR,
		LLC_GATE_VALVE_OPEN,
		LLC_GATE_VALVE_CLOSE,
		TR_GATE_VALVE_OPEN,
		TR_GATE_VALVE_CLOSE,
		LLC_TMP_GATE_VALVE_OPEN,
		LLC_TMP_GATE_VALVE_CLOSE,
		MC_TMP_GATE_VALVE_OPEN,
		MC_TMP_GATE_VALVE_CLOSE,
		LLC_FAST_ROUGHING_VALVE_OPEN,
		LLC_ROUGHING_VALVE_CLOSE,
		MC_FAST_ROUGHING_VALVE_OPEN,
		MC_ROUGHING_VALVE_CLOSE,
		LLC_FORELINE_VALVE_OPEN,
		LLC_FORELINE_VALVE_CLOSE,
		MC_FORELINE_VALVE_OPEN,
		MC_FORELINE_VALVE_CLOSE,
		LLC_ATM_SW,
		LLC_LINE_PRESSURE_SW,
		MC_ATM_SW,
		MC_LINE_PRESSURE_SW,
		m_bDigitalin_84,
		MAINT_MODE,
		ATM_ROBOT_ERROR,
		MC_LID_OPEN,
		TR_LID_OPEN,
		LL_LID_OPEN,
		m_bDigitalin_90,
		m_bDigitalin_91,
		VAC_ROBOT_HAND_RE_TO_LLC,
		VAC_ROBOT_HAND_RE_TO_MC,
		ATM_ROBOT_HAND_EX,
		ATM_ROBOT_HAND_RE,
		LLC_MASK_CHECK,
		LLC_MASK_TILT1,
		LLC_MASK_TILT2,
		MC_MASK_CHECK,
		MC_MASK_TILT1,
		MC_MASK_TILT2,
		VAC_ROBOT_MASK_CHECK_TO_LLC,
		VAC_ROBOT_MASK_CHECK_TO_MC,
		m_bDigitalin_104,
		LL_MASKCHECK_VISION_SEN,
		MC_GAUGE_SET_POINT,
		LLC_GAUGE_SET_POINT,
		ISOLATOR_INTERLOCK_SENSOR1,
		ISOLATOR_INTERLOCK_SENSOR2,
		SAFETY_BAR_SENSOR1,
		SAFETY_BAR_SENSOR2,
		CONTROL_RACK_COOLING_FAN_1,
		CONTROL_RACK_COOLING_FAN_2,
		CONTROL_RACK_COOLING_FAN_3,
		CONTROL_RACK_COOLING_FAN_4,
		AC_RACK_COOLING_FAN_STATUS_1,
		AC_RACK_COOLING_FAN_STATUS_2,
		AC_RACK_COOLING_FAN_STATUS_3,
		AC_RACK_COOLING_FAN_STATUS_4,
		AC_RACK_COOLING_FAN_STATUS_5,
		AC_RACK_COOLING_FAN_STATUS_6,
		AC_RACK_COOLING_FAN_STATUS_7,
		AC_RACK_COOLING_FAN_STATUS_8,
		FRAME_COOLING_FAN_STATUS_1,
		FRAME_COOLING_FAN_STATUS_2,
		FRAME_COOLING_FAN_STATUS_3,
		FRAME_COOLING_FAN_STATUS_4,
		FRAME_COOLING_FAN_STATUS_5,
		FRAME_COOLING_FAN_STATUS_6,
		FRAME_COOLING_FAN_STATUS_7,
		FRAME_COOLING_FAN_STATUS_8,
		FRAME_COOLING_FAN_STATUS_9,
		FRAME_COOLING_FAN_STATUS_10,
		FRAME_COOLING_FAN_STATUS_11,
		FRAME_COOLING_FAN_STATUS_12,
		FRAME_COOLING_FAN_STATUS_13,
		FRAME_COOLING_FAN_STATUS_14,
		FRAME_COOLING_FAN_STATUS_15,
		FRAME_COOLING_FAN_STATUS_16,
		LASER_ALARM,
		LASER_VALVE_OPEN_STATUS,
		LASER_VALVE_CLOSE_STATUS,
		LASER_ON_STATUS,
		m_bDigitalin_144,
		m_bDigitalin_145,
		m_bDigitalin_146,
		m_bDigitalin_147,
		m_bDigitalin_148,
		m_bDigitalin_149,
		m_bDigitalin_150,
		m_bDigitalin_151,
		m_bDigitalin_152,
		m_bDigitalin_153,
		m_bDigitalin_154,
		m_bDigitalin_155,
		m_bDigitalin_156,
		m_bDigitalin_157,
		m_bDigitalin_158,
		m_bDigitalin_159,
	};




	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
