﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

// CCameraDlg 대화 상자

IMPLEMENT_DYNAMIC(CCameraDlg, CDialogEx)

#define BUFFERING_SIZE_MAX 4

CCameraDlg::CCameraDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CAMERA_DIALOG, pParent)
	, m_bCrossLineCheck(TRUE)
	, m_bMouseMoveCheck(TRUE)
	, m_nSetScaleMinValue(0)
	, m_nSetScaleMaxValue(0)
	, m_bDispScaleImage(FALSE)
{
	MilDigitizer = M_NULL;
	m_pMilProcImageBuf = M_NULL;
	m_MilProcImageBufSize = 0;

	m_bGrab		 = TRUE;
	m_bConnected = FALSE;
}

CCameraDlg::~CCameraDlg()
{
}

void CCameraDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Check(pDX, IDC_CHECK_CROSSLINE, m_bCrossLineCheck);
	DDX_Check(pDX, IDC_CHECK_MOUSEMOVE, m_bMouseMoveCheck);
	DDX_Text(pDX, IDC_CAM_SCALE_MIN_EDIT, m_nSetScaleMinValue);
	DDX_Text(pDX, IDC_CAM_SCALE_MAX_EDIT, m_nSetScaleMaxValue);
	DDX_Check(pDX, IDC_CAM_DISP_SCALED_CHECK, m_bDispScaleImage);
}

BEGIN_MESSAGE_MAP(CCameraDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CAM_START_BUTTON, &CCameraDlg::OnBnClickedCamStartButton)
	ON_BN_CLICKED(IDC_CAM_STOP_BUTTON, &CCameraDlg::OnBnClickedCamStopButton)
	ON_BN_CLICKED(IDC_CAM_SAVE_BUTTON, &CCameraDlg::OnBnClickedCamSaveButton)
	ON_BN_CLICKED(IDC_CHECK_CROSSLINE, &CCameraDlg::OnBnClickedCheckCrossline)
	ON_BN_CLICKED(IDC_CHECK_MOUSEMOVE, &CCameraDlg::OnBnClickedCheckMousemove)
	ON_BN_CLICKED(IDC_CAM_SHOW_ZP_BUTTON, &CCameraDlg::OnBnClickedCamShowZpButton)
	ON_BN_CLICKED(IDC_CAM_SET_SCALE_BUTTON, &CCameraDlg::OnBnClickedCamSetScaleButton)
	ON_BN_CLICKED(IDC_CAM_DISP_SCALED_CHECK, &CCameraDlg::OnBnClickedCamDispScaledCheck)
END_MESSAGE_MAP()


// CCameraDlg 메시지 처리기
BOOL CCameraDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CCameraDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	GrabStop();

	if (m_pMilProcImageBuf != M_NULL)
	{
		while (m_MilProcImageBufSize > 0)
			MbufFree(m_pMilProcImageBuf[--m_MilProcImageBufSize]);

		delete[] m_pMilProcImageBuf;
	}

	if (MilDigitizer != M_NULL)
		MdigHalt(MilDigitizer);

	if (MilDigitizer != M_NULL)
	{
		MdigFree(MilDigitizer);
		MilDigitizer = M_NULL;
	}
}

BOOL CCameraDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	CRect rect;
	CWnd *pWnd = GetDlgItem(IDC_OPTIC_VIEWER_STATIC);
	pWnd->GetClientRect(rect);
	m_CameraWnd.Create(WS_CHILD | WS_VISIBLE /*| WS_VSCROLL | WS_HSCROLL*/, rect, pWnd);
	m_CameraWnd.ShowWindow(SW_SHOW);

	m_CameraWnd.m_bCrossLine = m_bCrossLineCheck;
	m_CameraWnd.m_bMouseMove = m_bMouseMoveCheck;

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
	{
#if true
		MbufAllocColor(g_milSystemGigEVision, m_CameraWnd.m_nDigBand, m_CameraWnd.m_nDigSizeX, m_CameraWnd.m_nDigSizeY, 8 + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP + M_DIB + M_GDI, &m_CameraWnd.m_MilOMDisplay);
		MbufClear(m_CameraWnd.m_MilOMDisplay, M_COLOR_GRAY);

		CString str;
		str.Format(_T("%s\\OM_IMAGE.BMP"), OM_IMAGEFILE_PATH);
		if (IsFileExist(str))
		{
			MbufLoad((char*)(LPCTSTR)str, m_CameraWnd.m_MilOMDisplay);
		}
		m_CameraWnd.SetTimer(OMIMAGE_DISPLAY_TIMER, 100, 0);
#else

		///
		MIL_ID MilDisplay = 0, MilImage = 0;
		MdispAlloc(g_milSystemGigEVision, M_DEFAULT, M_DISPLAY_SETUP, M_DEFAULT, &MilDisplay);
		MdispControl(MilDisplay, M_MOUSE_USE, M_ENABLE);
		MdispControl(MilDisplay, M_MOUSE_CURSOR_CHANGE, M_ENABLE);

		MdispControl(MilDisplay, M_BACKGROUND_COLOR, M_COLOR_BLACK);
		MdispControl(MilDisplay, M_CENTER_DISPLAY, M_ENABLE);

		MdispControl(MilDisplay, M_SCALE_DISPLAY, M_ENABLE);



		MbufAllocColor(g_milSystemGigEVision, m_CameraWnd.m_nDigBand, m_CameraWnd.m_nDigSizeX, m_CameraWnd.m_nDigSizeY, 8 + M_UNSIGNED, M_IMAGE + M_DISP + M_GRAB, &MilImage);
		MbufClear(MilImage, 0x0);

		MdispSelectWindow(MilDisplay, MilImage, pWnd->m_hWnd);
		
		
		CString str;
		str.Format(_T("%s\\OM_IMAGE.BMP"), OM_IMAGEFILE_PATH);
		if (IsFileExist(str))
		{
			MbufLoad((char*)(LPCTSTR)str, MilImage);
		}
#endif
	}


	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int CCameraDlg::OpenDevice()
{
	try
	{
		MIL_INT MilBufNo, n;

		char szCCFFile[128] = { 0, };
		sprintf(szCCFFile, OM_CAMERA_CCF_FILE_FULLPATH);

		MdigAlloc(g_milSystemGigEVision, M_DEV0, (MIL_TEXT_PTR)szCCFFile, M_DEFAULT, &MilDigitizer);

		m_CameraWnd.m_nDigSizeX = MdigInquire(MilDigitizer, M_SIZE_X, M_NULL);
		m_CameraWnd.m_nDigSizeY = MdigInquire(MilDigitizer, M_SIZE_Y, M_NULL);
		m_CameraWnd.m_nDigBand  = MdigInquire(MilDigitizer, M_SIZE_BAND, M_NULL);

		//MbufAlloc2d(MilSystem, mWidth, mHeight, 8 + M_UNSIGNED, M_IMAGE + M_DISP + M_GRAB, &MilImage);
		//MbufAllocColor(MilSystem, m_CameraWnd.m_nDigBand, m_CameraWnd.m_nDigSizeX, m_CameraWnd.m_nDigSizeY, 8 + M_UNSIGNED, M_IMAGE + M_DISP + M_GRAB, &m_CameraWnd.m_MilImage);
		MbufAllocColor(g_milSystemGigEVision, m_CameraWnd.m_nDigBand, m_CameraWnd.m_nDigSizeX, m_CameraWnd.m_nDigSizeY, 8 + M_UNSIGNED, M_IMAGE + M_DISP + M_GRAB, &m_CameraWnd.m_MilImage);
		MbufClear(m_CameraWnd.m_MilImage, 0x0);

		if (m_CameraWnd.m_MilOMDisplay != M_NULL)
		{
			MbufFree(m_CameraWnd.m_MilOMDisplay);
			m_CameraWnd.m_MilOMDisplay = M_NULL;
		}
		
		MbufAllocColor(g_milSystemGigEVision, m_CameraWnd.m_nDigBand, m_CameraWnd.m_nDigSizeX, m_CameraWnd.m_nDigSizeY, 8 + M_UNSIGNED, M_IMAGE + M_PROC + M_DIB + M_GDI, &m_CameraWnd.m_MilOMDisplay);
		MbufClear(m_CameraWnd.m_MilOMDisplay, 0x0);
		
		m_pMilProcImageBuf = new MIL_ID[BUFFERING_SIZE_MAX];
		
		for (MilBufNo = 0; MilBufNo < BUFFERING_SIZE_MAX; MilBufNo++)
		{
			MbufAllocColor(g_milSystemGigEVision, m_CameraWnd.m_nDigBand, m_CameraWnd.m_nDigSizeX, m_CameraWnd.m_nDigSizeY, 8 + M_UNSIGNED, M_IMAGE + M_DISP + M_GRAB + M_PROC, &m_pMilProcImageBuf[MilBufNo]);
		
			if (m_pMilProcImageBuf[MilBufNo])
				MbufClear(m_pMilProcImageBuf[MilBufNo], 0xFF);
			else
				break;
		}
		
		for (n = 0; n < 2 && MilBufNo; n++)
		{
			MilBufNo--;
			MbufFree(m_pMilProcImageBuf[MilBufNo]);
		}
		
		m_MilProcImageBufSize = MilBufNo;
		
		m_UserHookData.MilDigitizer = MilDigitizer;
		m_UserHookData.MilBasicDispImageBuf = m_CameraWnd.m_MilImage;
		m_UserHookData.MilProcessedDispImageBuf = m_CameraWnd.m_MilImage;
		m_UserHookData.ProcessedImageCount = 0;

		m_CameraWnd.SetTimer(OMIMAGE_DISPLAY_TIMER, 100, 0);
	}
	catch (int exception)
	{
		return -1;
	}

	GrabStart();
	m_bConnected = TRUE;
	m_bGrab = TRUE;
	return 0;
}

void CCameraDlg::OnBnClickedCamStartButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CCameraDlg::OnBnClickedCamStartButton() 버튼 클릭!"));

	if (m_bGrab == FALSE)
	{
		GrabStart();
		m_bGrab = TRUE;
	}
}

void CCameraDlg::OnBnClickedCamStopButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CCameraDlg::OnBnClickedCamStopButton() 버튼 클릭!"));

	if (m_bGrab == TRUE)
	{
		GrabStop();
		m_bGrab = FALSE;
	}
}

void CCameraDlg::OnBnClickedCamSaveButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CCameraDlg::OnBnClickedCamSaveButton() 버튼 클릭!"));

	SaveOMImage();
	return;

#if FALSE
	//MbufPut2d(MilImage, 0, 0, 2064, 1544, m_Buffers);
	CLoadSaveDlg dlg(this, m_Conv->GetOutputBuffer(), FALSE);
	dlg.DoModal();
#else
	CString dir;
	dir = OM_IMAGEFILE_PATH;
	if (!IsFolderExist(dir))
	{
		CreateDirectory(dir, NULL);
	}
	CString strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	dir += "\\";
	dir += strDate;
	if (!IsFolderExist(dir))
	{
		CreateDirectory(dir, NULL);
	}

	CString strFullName;
	int year, month, day, hour, minute, second;
	year = CTime::GetCurrentTime().GetYear();
	month = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	minute = CTime::GetCurrentTime().GetMinute();
	second = CTime::GetCurrentTime().GetSecond();
	strFullName.Format(_T("%s\\OM_IMAGE_%d_%d_%d_%d_%d_%d.bmp"), dir, year, month, day, hour, minute, second);

	//dir += "\\";
	//dir += "OM_IMAGE.BMP";
	MbufExport(strFullName, M_BMP, m_CameraWnd.m_MilOMDisplay);
#endif
}

void CCameraDlg::OnBnClickedCheckCrossline()
{
	UpdateData(TRUE);
	m_CameraWnd.m_bCrossLine = m_bCrossLineCheck;
}

void CCameraDlg::OnBnClickedCheckMousemove()
{
	UpdateData(TRUE);
	m_CameraWnd.m_bMouseMove = m_bMouseMoveCheck;
}

void CCameraDlg::OnBnClickedCamShowZpButton()
{
	g_pCamera->ShowWindow(SW_HIDE);
	g_pCamZoneplate->ShowWindow(SW_SHOW);
}

void CCameraDlg::SaveOMImage()
{
	LPCTSTR filesFilter = _T("Image Files (*.bmp, *.BMP) | *.bmp;*.BMP; ||");
	CFileDialog dlg(FALSE, _T("bmp"), _T("DefaultImage"), OFN_OVERWRITEPROMPT, filesFilter, this, 0, 1);
	dlg.m_ofn.lpstrTitle = TEXT("Save Image File");
	dlg.m_ofn.lpstrInitialDir = "d:\\";

	if (dlg.DoModal() == IDOK)
	{
		CString sImagePath = dlg.GetPathName();

		//Save origin image.
		MbufExport(sImagePath, M_BMP, m_CameraWnd.m_MilOMDisplay);

		//Save scaled image.
		int iCountForFileName = sImagePath.ReverseFind('\\') + 1;	// 파일이름을 위한 위치확인
		int iCountForFileType = sImagePath.ReverseFind('.') + 1;	// 파일타입을 위한 위치확인

		CString tempFileName = sImagePath.Mid(iCountForFileName, iCountForFileType - iCountForFileName - 1);	// 파일 이름 선언, 획득 및 할당
		CString tempFolderPath = sImagePath.Mid(0, iCountForFileName);

		MbufExport(tempFolderPath + tempFileName + _T("_scaled.bmp"), M_BMP, m_CameraWnd.m_MilScaleImage);
	}
}

void CCameraDlg::GrabStart()
{
	//MIL_DOUBLE ExposureTime = 10000;
	////Control Feature Value
	//MdigControlFeature(MilDigitizer, M_FEATURE_VALUE, MIL_TEXT("ExposureTime"), M_TYPE_DOUBLE, &ExposureTime);
	////Inquire Feature Value
	//MdigInquireFeature(MilDigitizer, M_FEATURE_VALUE, MIL_TEXT("ExposureTime"), M_TYPE_DOUBLE, &ExposureTime);

	m_UserHookData.ProcessedImageCount = 0;
	MdigProcess(MilDigitizer, m_pMilProcImageBuf, m_MilProcImageBufSize, M_START, M_DEFAULT, ProcessingCallback, &m_UserHookData);
}

void CCameraDlg::GrabStop()
{
	if (MilDigitizer)
		MdigProcess(MilDigitizer, m_pMilProcImageBuf, m_MilProcImageBufSize, M_STOP, M_DEFAULT, ProcessingCallback, &m_UserHookData);
}

MIL_INT MFTYPE CCameraDlg::ProcessingCallback(MIL_INT HookType, MIL_ID HookId, void * HookDataPtr)
{
	HookDataStruct *UserHookDataPtr = (HookDataStruct *)HookDataPtr;
	MIL_ID ModifiedBufferId;

	MdigGetHookInfo(HookId, M_MODIFIED_BUFFER + M_BUFFER_ID, &ModifiedBufferId);
	MbufCopy(ModifiedBufferId, UserHookDataPtr->MilBasicDispImageBuf);

	return 0;
}


void CCameraDlg::OnBnClickedCamSetScaleButton()
{
	UpdateData(TRUE);

	m_CameraWnd.m_nSetScaleMin = m_nSetScaleMinValue;
	m_CameraWnd.m_nSetScaleMax = m_nSetScaleMaxValue;
}


void CCameraDlg::OnBnClickedCamDispScaledCheck()
{
	UpdateData(TRUE);
	m_CameraWnd.m_bScaleImage = m_bDispScaleImage;

	if (m_CameraWnd.m_bScaleImage == TRUE)
	{
		m_CameraWnd.m_nSetScaleMin = m_nSetScaleMinValue = m_CameraWnd.m_nCurrScaleMin;
		m_CameraWnd.m_nSetScaleMax = m_nSetScaleMaxValue = m_CameraWnd.m_nCurrScaleMax;
	}
	UpdateData(FALSE);
}

void CCameraDlg::GetCurrentScaleValue(int nMin, int nMax)
{
	CString strTemp;
	strTemp.Format(_T("%d"), nMin);
	SetDlgItemText(IDC_CAM_CURRENT_MIN_EDIT, strTemp);
	strTemp.Format(_T("%d"), nMax);
	SetDlgItemText(IDC_CAM_CURRENT_MAX_EDIT, strTemp);
}

void CCameraDlg::InitScaleValue(int nMin, int nMax)
{
	CString strTemp;
	strTemp.Format(_T("%d"), nMin);
	SetDlgItemText(IDC_CAM_SCALE_MIN_EDIT, strTemp);
	strTemp.Format(_T("%d"), nMax);
	SetDlgItemText(IDC_CAM_SCALE_MAX_EDIT, strTemp);
}
