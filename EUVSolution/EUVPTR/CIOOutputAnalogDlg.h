﻿#pragma once


// CAnalogOutput 대화 상자

class CAnalogOutput : public CDialogEx
{
	DECLARE_DYNAMIC(CAnalogOutput)

public:
	CAnalogOutput(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CAnalogOutput();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IO_OUTPUT_AN_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL OnInitDialog();
	HICON	m_LedIcon[2];
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	CString			strPos;
	CString			strPos2;
	CString			strPos3;
	CString			strPos4;
	CString			slow_mfc_sccm;
	CString			fast_mfc_sccm;
	CString			o2_mfc_sccm;

	int				m_nChannel;
	//int				SetOutputBitOnOff(int nIndex);
	
	void			InitControls_AO();

	bool			OnUpdateAnalogOutput();

	afx_msg void OnBnClickedCheckAnalogoutY000();
	afx_msg void OnBnClickedCheckAnalogoutY001();
	afx_msg void OnNMCustomdrawSliderMfc01(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawSliderMfc02(NMHDR *pNMHDR, LRESULT *pResult);

	CSliderCtrl m_mfc_ctrl_1;
	CSliderCtrl m_mfc_ctrl_2;
	CSliderCtrl m_mfc_ctrl_3;

	afx_msg void OnNMCustomdrawSliderCtn(NMHDR *pNMHDR, LRESULT *pResult);
	
	CSliderCtrl m_ctrl_3;
	
	afx_msg void OnBnClickedCheckAnalogoutSet0();
	afx_msg void OnBnClickedCheckAnalogoutSet1();
	afx_msg void OnBnClickedCheckAnalogoutSet2();
	afx_msg void OnBnClickedCheckAnalogoutY002();

	CBrush  m_brush;
	CFont	m_font;

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedCheckAnalogoutSet3();
	afx_msg void OnNMCustomdrawSliderMfc03(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnBnClickedCheckAnalogoutY003();
};
