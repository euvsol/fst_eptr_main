﻿// CSQOneDataBaseDlg.cpp: 구현 파일
//
#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include "CSqOneConfigDlg.h"
#include "CSQOneDataBaseDlg.h"


// CSQOneDataBaseDlg 대화 상자

IMPLEMENT_DYNAMIC(CSQOneDataBaseDlg, CDialogEx)

CSQOneDataBaseDlg::CSQOneDataBaseDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SQONEDB_DIALOG, pParent)
{
	m_nListIndex = 0;

	m_isLoaded = FALSE;
}

CSQOneDataBaseDlg::~CSQOneDataBaseDlg()
{
}

void CSQOneDataBaseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSQOneDataBaseDlg, CDialogEx)
	ON_BN_CLICKED(IDC_SQONEDB_BUTTON_SAVEPOSITION, &CSQOneDataBaseDlg::OnBnClickedSqonedbButtonAddposition)
	ON_BN_CLICKED(IDC_SQONEDB_BUTTON_LOADPOSITION, &CSQOneDataBaseDlg::OnBnClickedSqonedbButtonLoadposition)
	ON_BN_CLICKED(IDC_SQONEDB_BUTTON_DELETEPOSITION, &CSQOneDataBaseDlg::OnBnClickedSqonedbButtonDeleteposition)
	ON_NOTIFY(HDN_ITEMCLICK, 0, &CSQOneDataBaseDlg::OnHdnItemclickSqonedbList)
	ON_NOTIFY(NM_CLICK, IDC_SQONEDB_LIST, &CSQOneDataBaseDlg::OnNMClickSqonedbList)
	ON_NOTIFY(NM_DBLCLK, IDC_SQONEDB_LIST, &CSQOneDataBaseDlg::OnNMDblclkSqonedbList)
END_MESSAGE_MAP()


// CSQOneDataBaseDlg 메시지 처리기


BOOL CSQOneDataBaseDlg::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::DestroyWindow();
}


BOOL CSQOneDataBaseDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->InsertColumn(0, _T("No"));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetColumnWidth(0, 35);
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->InsertColumn(1, _T("Name"));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetColumnWidth(1, 130);
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->InsertColumn(2, _T("Horizontal X"));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetColumnWidth(2, 80);
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->InsertColumn(3, _T("Vertical Y"));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetColumnWidth(3, 80);
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->InsertColumn(4, _T("Beam Z"));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetColumnWidth(4, 80);
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->InsertColumn(5, _T("Pitch Rx"));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetColumnWidth(5, 80);
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->InsertColumn(6, _T("Yaw Ry"));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetColumnWidth(6, 80);
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->InsertColumn(7, _T("Roll Rz"));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetColumnWidth(7, 80);
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->InsertColumn(8, _T("Revision Date"));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetColumnWidth(8, 130);

	loadSQONEStagePosition();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CSQOneDataBaseDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CSQOneDataBaseDlg::initializePositionDB()
{
	//int rc = 0;
	//sqlite3 *db = NULL;
	//sqlite3_stmt *stmt = NULL;
	//char *err_msg = { 0 };
	//rc = sqlite3_open(SQONESTAGE_POSITION_FILE_FULLPATH, &db);		//DB 열기
	//if (SQLITE_OK != rc) {
	//	MessageBox("sqlite3_open error", sqlite3_errmsg(db), MB_ICONSTOP);
	//	return;
	//}
	//sqlite3_prepare_v2(db, "select * from tableSQONEPOSITION;", -1, &stmt, NULL);
	//while (sqlite3_step(stmt) != SQLITE_DONE) {
	//	int nItem = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->InsertItem(0, (char *)sqlite3_column_text(stmt, 0));
	//	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(nItem, 1, (char *)sqlite3_column_text(stmt, 1));
	//	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(nItem, 2, (char *)sqlite3_column_text(stmt, 2));
	//	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(nItem, 3, (char *)sqlite3_column_text(stmt, 3));
	//	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(nItem, 4, (char *)sqlite3_column_text(stmt, 3));
	//	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(nItem, 5, (char *)sqlite3_column_text(stmt, 3));
	//	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(nItem, 6, (char *)sqlite3_column_text(stmt, 3));
	//}
	//sqlite3_finalize(stmt);
	//
	//sqlite3_close(db);
	int row = 0, index = 0;;
	CString strTemp = "";
	index = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemCount();
	for (row = 0; row < MAX_STAGE_POSITION; row++)
	{
		if (strcmp(m_stStagePos_SQONE[row].PosName,"\0") == 0)
		{
			break;
		}

		strTemp.Format("%02d", index);
		int nItem = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->InsertItem(index, strTemp);
		//((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 0, strTemp);
		((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 1, m_stStagePos_SQONE[row].PosName);
		strTemp.Format("%4.6f", m_stStagePos_SQONE[row].Horizontal);
		((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 2, strTemp);
		strTemp.Format("%4.6f", m_stStagePos_SQONE[row].Vertical);
		((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 3, strTemp);
		strTemp.Format("%4.6f", m_stStagePos_SQONE[row].Beam);
		((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 4, strTemp);
		strTemp.Format("%4.6f", m_stStagePos_SQONE[row].Pitch);
		((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 5, strTemp);
		strTemp.Format("%4.6f", m_stStagePos_SQONE[row].Yaw);
		((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 6, strTemp);
		strTemp.Format("%4.6f", m_stStagePos_SQONE[row].Roll);
		((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 7, strTemp);
		//strTemp.Format("%s", m_stStagePos_SQONE[row].Date);
		((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 8, m_stStagePos_SQONE[row].Date);
		index++;
	}
}

void CSQOneDataBaseDlg::loadSQONEStagePosition()
{
	CString strAppName;
	char chGetString[128];
	int i = 0;
	m_isLoaded = TRUE;
	for (i = 0; i < MAX_STAGE_POSITION; i++)
	{
		m_stStagePos_SQONE[i].Horizontal = m_stStagePos_SQONE[i].Vertical = m_stStagePos_SQONE[i].Beam = m_stStagePos_SQONE[i].Pitch =
			m_stStagePos_SQONE[i].Yaw = m_stStagePos_SQONE[i].Roll = 0.0;
		sprintf(m_stStagePos_SQONE[i].PosName, "");
		sprintf(m_stStagePos_SQONE[i].Date, "");

		strAppName.Format("STAGE_POSITION_%02d", i);
		GetPrivateProfileString(strAppName, "NAME", "", chGetString, DATA_STRING_SIZE, SQONESTAGE_POSITION_FILE_FULLPATH);
		strcpy(m_stStagePos_SQONE[i].PosName,  chGetString);
		GetPrivateProfileString(strAppName, "Horizontal(mm)", "0.0", chGetString, DATA_STRING_SIZE, SQONESTAGE_POSITION_FILE_FULLPATH);
		m_stStagePos_SQONE[i].Horizontal = atof(chGetString);
		GetPrivateProfileString(strAppName, "Vertical(mm)", "0.0", chGetString, DATA_STRING_SIZE, SQONESTAGE_POSITION_FILE_FULLPATH);
		m_stStagePos_SQONE[i].Vertical = atof(chGetString);
		GetPrivateProfileString(strAppName, "Beam(mm)", "0.0", chGetString, DATA_STRING_SIZE, SQONESTAGE_POSITION_FILE_FULLPATH);
		m_stStagePos_SQONE[i].Beam = atof(chGetString);
		GetPrivateProfileString(strAppName, "Pitch(mrad)", "0.0", chGetString, DATA_STRING_SIZE, SQONESTAGE_POSITION_FILE_FULLPATH);
		m_stStagePos_SQONE[i].Pitch = atof(chGetString);
		GetPrivateProfileString(strAppName, "Yaw(mrad)", "0.0", chGetString, DATA_STRING_SIZE, SQONESTAGE_POSITION_FILE_FULLPATH);
		m_stStagePos_SQONE[i].Yaw = atof(chGetString);
		GetPrivateProfileString(strAppName, "Roll(mrad)", "0.0", chGetString, DATA_STRING_SIZE, SQONESTAGE_POSITION_FILE_FULLPATH);
		m_stStagePos_SQONE[i].Roll = atof(chGetString);
		GetPrivateProfileString(strAppName, "Date", "", chGetString, DATA_STRING_SIZE, SQONESTAGE_POSITION_FILE_FULLPATH);
		strcpy(m_stStagePos_SQONE[i].Date, chGetString);
	}

	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->DeleteAllItems();
	initializePositionDB();
}

void CSQOneDataBaseDlg::saveSQONEStagePosition()
{
	CString strAppName, strString;

	for (int i = 0; i < MAX_STAGE_POSITION; i++)
	{
		strAppName.Format("STAGE_POSITION_%02d", i);
		strString = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(i, 1);
		WritePrivateProfileString(strAppName, "NAME", strString, SQONESTAGE_POSITION_FILE_FULLPATH);
		strString = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(i, 2);
		WritePrivateProfileString(strAppName, "Horizontal(mm)", strString, SQONESTAGE_POSITION_FILE_FULLPATH);
		strString = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(i, 3);
		WritePrivateProfileString(strAppName, "Vertical(mm)", strString, SQONESTAGE_POSITION_FILE_FULLPATH);
		strString = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(i, 4);
		WritePrivateProfileString(strAppName, "Beam(mm)", strString, SQONESTAGE_POSITION_FILE_FULLPATH);
		strString = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(i, 5);
		WritePrivateProfileString(strAppName, "Pitch(mrad)", strString, SQONESTAGE_POSITION_FILE_FULLPATH);
		strString = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(i, 6);
		WritePrivateProfileString(strAppName, "Yaw(mrad)", strString, SQONESTAGE_POSITION_FILE_FULLPATH);
		strString = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(i, 7);
		WritePrivateProfileString(strAppName, "Roll(mrad)", strString, SQONESTAGE_POSITION_FILE_FULLPATH);
		strString = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(i, 8);
		WritePrivateProfileString(strAppName, "Date", strString, SQONESTAGE_POSITION_FILE_FULLPATH);
	}
}

void CSQOneDataBaseDlg::addSQONEStagePosition()
{
	int index = 0;
	if (m_nListIndex == -1)
	{
		index = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemCount();
	}
	else
	{
		((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->DeleteItem(m_nListIndex);
		index = m_nListIndex;
	}

	CString strTemp = "";
	strTemp.Format("%02d", index);
	int nItem = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->InsertItem(index, strTemp);

	GetDlgItemTextA(IDC_SQONEDB_EDIT_NAME, strTemp);
	if (strTemp == "")
	{
		CTime tempTime = CTime::GetCurrentTime();
		strTemp.Format(_T("%02d,%02d,%02d:%02d:%02d"), tempTime.GetMonth(), tempTime.GetDay(), tempTime.GetHour(), tempTime.GetMinute(), tempTime.GetSecond());
	}
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 1, strTemp);
	GetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, strTemp);
	strTemp.Format(_T("%4.6lf"), _ttof(strTemp));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 2, strTemp);
	GetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, strTemp);
	strTemp.Format(_T("%4.6lf"), _ttof(strTemp));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 3, strTemp);
	GetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, strTemp);
	strTemp.Format(_T("%4.6lf"), _ttof(strTemp));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 4, strTemp);
	GetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, strTemp);
	strTemp.Format(_T("%4.6lf"), _ttof(strTemp));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 5, strTemp);
	GetDlgItemTextA(IDC_SQONEDB_EDIT_YAW, strTemp);
	strTemp.Format(_T("%4.6lf"), _ttof(strTemp));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 6, strTemp);
	GetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, strTemp);
	strTemp.Format(_T("%4.6lf"), _ttof(strTemp));
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 7, strTemp);
	CTime tempTime = CTime::GetCurrentTime();
	strTemp.Format(_T("%02d,%02d,%02d:%02d:%02d"), tempTime.GetMonth(), tempTime.GetDay(), tempTime.GetHour(), tempTime.GetMinute(), tempTime.GetSecond());
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->SetItemText(index, 8, strTemp);

	saveSQONEStagePosition();
}

void CSQOneDataBaseDlg::deleteSQONEStagePosition()
{
	((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->DeleteItem(m_nListIndex);

	saveSQONEStagePosition();
}


void CSQOneDataBaseDlg::OnBnClickedSqonedbButtonAddposition()
{
	if (m_isLoaded == TRUE)
	{
		addSQONEStagePosition();
	}
}


void CSQOneDataBaseDlg::OnBnClickedSqonedbButtonLoadposition()
{
	if (g_pSqOneControl->IsFileExist(SQONESTAGE_POSITION_FILE_FULLPATH))
	{
		loadSQONEStagePosition();
	}
}


void CSQOneDataBaseDlg::OnBnClickedSqonedbButtonDeleteposition()
{
	if (m_isLoaded == TRUE)
	{
		deleteSQONEStagePosition();
	}
}


void CSQOneDataBaseDlg::OnHdnItemclickSqonedbList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
}


void CSQOneDataBaseDlg::OnNMClickSqonedbList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;
	m_nListIndex = pNMItemActivate->iItem;
	if (m_nListIndex == -1)
	{
		SetDlgItemTextA(IDC_SQONEDB_EDIT_NAME, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_YAW,"");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, "");
	}
	else
	{
		SetDlgItemTextA(IDC_SQONEDB_EDIT_NAME, ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(m_nListIndex, 1));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(m_nListIndex, 2));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(m_nListIndex, 3));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(m_nListIndex, 4));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(m_nListIndex, 5));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_YAW, ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(m_nListIndex, 6));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(m_nListIndex, 7));
	}
}


void CSQOneDataBaseDlg::OnNMDblclkSqonedbList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	*pResult = 0;

	CString strHorizontal = "", strVertical = "", strBeam = "", strPitch = "", strYaw = "", strRoll = "";

	strHorizontal = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(pNMItemActivate->iItem,2);
	strVertical = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(pNMItemActivate->iItem,3);
	strBeam = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(pNMItemActivate->iItem,4);
	strPitch = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(pNMItemActivate->iItem,5);
	strYaw = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(pNMItemActivate->iItem,6);
	strRoll = ((CListCtrl*)GetDlgItem(IDC_SQONEDB_LIST))->GetItemText(pNMItemActivate->iItem,7);

	if (m_isLoaded == TRUE && g_pSqOneControl->m_bConnected == TRUE)
	{
		int nRet = AfxMessageBox("SQ one stage를 움직이시겠습니까", MB_YESNO);
		if (nRet = IDYES)
		{
			g_pSqOneDB->ShowWindow(SW_HIDE);
			g_pSqOneManual->ShowWindow(SW_SHOW);
			g_pSqOneControl->moveSQ1(_ttof(strHorizontal), _ttof(strVertical), _ttof(strBeam), _ttof(strPitch), _ttof(strYaw), _ttof(strRoll));
		}
		else
		{
			return;
		}
	}
	else
	{
		AfxMessageBox("통신이 연결되지 않았습니다.");
	}
}
