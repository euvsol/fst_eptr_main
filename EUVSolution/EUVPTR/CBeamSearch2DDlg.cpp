﻿// CSqOneScanDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"



// CSqOneScanDlg 대화 상자


IMPLEMENT_DYNAMIC(CBeamSearch2DDlg, CDialogEx)

CBeamSearch2DDlg::CBeamSearch2DDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_BEAMSEARCH2D_DIALOG, pParent)
{
	m_nScanNumber = 0;
	m_bisScan = FALSE;

}

CBeamSearch2DDlg::~CBeamSearch2DDlg()
{
	delete m_XYchartViewer.getChart();
}

void CBeamSearch2DDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BEAMSEARCH2D_COMBO_PARAMETER1, m_combobox_Sqscan_parameter1);
	DDX_Control(pDX, IDC_BEAMSEARCH2D_COMBO_PARAMETER2, m_combobox_Sqscan_parameter2);
	DDX_Control(pDX, IDC_BEAMSEARCH2D_BITMAP_CHART, m_XYchartViewer);

}

// CSqOneScanDlg 메시지 처리기
BEGIN_MESSAGE_MAP(CBeamSearch2DDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BEAMSEARCH2D_BUTTON_CHECKPARAMETER, &CBeamSearch2DDlg::OnBnClickedButtonSqscanCheckparameter)
	ON_BN_CLICKED(IDC_BEAMSEARCH2D_BUTTON_CANCEL, &CBeamSearch2DDlg::OnBnClickedButtonSqscanCancel)
	ON_BN_CLICKED(IDC_BEAMSEARCH2D_BUTTON_RUNSEARCH, &CBeamSearch2DDlg::OnBnClickedButtonSqscanRunsearch)
	ON_BN_CLICKED(IDC_BEAMSEARCH2D_BUTTON_RUNMEASURE, &CBeamSearch2DDlg::OnBnClickedBeamsearch2dButtonRunmeasure)
	ON_CBN_SELCHANGE(IDC_BEAMSEARCH2D_COMBO_PARAMETER2, &CBeamSearch2DDlg::OnCbnSelchangeComboSqscanParameter2)
	ON_CBN_SELCHANGE(IDC_BEAMSEARCH2D_COMBO_PARAMETER1, &CBeamSearch2DDlg::OnCbnSelchangeComboSqscanParameter1)
	ON_CONTROL(CVN_ViewPortChanged, IDC_BEAMSEARCH2D_BITMAP_CHART, OnViewPortChangedChart)
END_MESSAGE_MAP()


BOOL CBeamSearch2DDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_RUNSEARCH)->EnableWindow(false);
	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_RUNMEASURE)->EnableWindow(false);
	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CANCEL)->EnableWindow(false);
	SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_DISPSCANNUM, "0 / 0");
	drawXYChart(&m_XYchartViewer);
	m_XYchartViewer.ShowWindow(SW_SHOW);
	m_combobox_Sqscan_parameter1.SetCurSel(0);
	m_combobox_Sqscan_parameter2.SetCurSel(1);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


int CBeamSearch2DDlg::checkScanSet()
{
	int nTempRet = 0;
	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CHECKPARAMETER)->EnableWindow(false);

	CString	parm1LL ="", parm1RL = "", parm1It = "", parm2LL = "", parm2RL = "", parm2It = "", strTotalNum = "", para1EP = "", para2EP = "";
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_STARTPOSITION, parm1LL);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_INCREMENT, parm1RL);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_ITERATION, parm1It);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_STARTPOSITION, parm2LL);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_INCREMENT, parm2RL);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_ITERATION, parm2It);

	double dendpos1 = 0, dendpos2 = 0;
	nTempRet =  checkScanParameter(parm1LL, parm1RL, parm1It, parm2LL, parm2RL, parm2It);
	if (nTempRet == 0)
	{
		int iScanNum = (_ttof(parm1It))*(_ttof(parm2It));
		strTotalNum.Format(_T("0 / %d"), iScanNum);
		SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_DISPSCANNUM, strTotalNum);

		double dEndPos1 = 0, dEndPos2 = 0;
		dEndPos1 = _ttof(parm1LL) + _ttof(parm1RL)*(_ttof(parm1It) - 1);
		dEndPos2 = _ttof(parm2LL) + _ttof(parm2RL)*(_ttof(parm2It) - 1);
		
		para1EP.Format(_T("%f"), dEndPos1);
		para2EP.Format(_T("%f"), dEndPos2);

		SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_ENDPOSITION, para1EP);
		SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_ENDPOSITION, para2EP);
		GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_RUNSEARCH)->EnableWindow(true);
		GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_RUNMEASURE)->EnableWindow(true);
		GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CANCEL)->EnableWindow(true);
		g_pBeamMain->Display(0, "Parameter Setting Sucess");
		return 0;
	}
	else if (nTempRet == -1)
	{
		AfxMessageBox(_T("파라미터가 설정이 잘못되었습니다."));
		g_pBeamMain->Display(0, "Scan Number Error");
		GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CHECKPARAMETER)->EnableWindow(true);
		return -1;
	}
	else if (nTempRet == -2)
	{
		g_pBeamMain->Display(0, "Communicaton Error");
		AfxMessageBox(_T("통신 상태 확인"));
		GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CHECKPARAMETER)->EnableWindow(true);
		return -1;
	}
	else
	{
		AfxMessageBox(_T("파라미터가 중복되었습니다."));
		g_pBeamMain->Display(0, "Parameter Duplication Error");
		GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CHECKPARAMETER)->EnableWindow(true);
		return -1;
	}
}

int CBeamSearch2DDlg::runSearch()
{
	g_pSqOneControl->m_bStopFlag = FALSE;
	m_bisScan = TRUE;

	if (g_pBeamAutoAlign->m_isAutoAlignOn == FALSE)
	{
		g_pBeamMain->resetDataset();
	}

	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CANCEL)->EnableWindow(FALSE);

	int nRet = 0;
	CString strLog = "";

	//g_pBeamMain->Display(0, "Scan Start");
	double runposX = 0, runposY = 0, runposZ = 0, runposRx = 0, runposRy = 0, runposRz = 0, runposCx = 0, runposCy = 0, runposCz = 0;
	CString	parm1LL = "", parm1RL = "", parm1It = "", parm2LL = "", parm2RL = "", parm2It = "", maxval = "", temp = "";

	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_STARTPOSITION, parm1LL);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_INCREMENT, parm1RL);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_ITERATION, parm1It);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_STARTPOSITION, parm2LL);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_INCREMENT, parm2RL);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_ITERATION, parm2It);

	int tonum = 0, nIndexIntensityNumber = 0;
	tonum = _ttof(parm1It)*_ttof(parm2It);

	int n1Iteration, n2Iteration;
	double *para1ary, *para2ary;

	n1Iteration = _ttoi(parm1It);
	para1ary = new double[_ttoi(parm1It)];
	memset(para1ary, NULL, sizeof(double) * _ttoi(parm1It));


	n2Iteration = _ttoi(parm2It);
	para2ary = new double[_ttoi(parm2It)];
	memset(para2ary, NULL, sizeof(double) * _ttoi(parm2It));

	for (int ti = 0; ti < n1Iteration; ti++)
	{
		para1ary[ti] = _ttof(parm1LL) + _ttof(parm1RL) * ti;
	}
	for (int ti = 0; ti < n2Iteration; ti++)
	{
		para2ary[ti] = _ttof(parm2LL) + _ttof(parm2RL) * ti;
	}

	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_RUNSEARCH)->EnableWindow(false);
	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_RUNMEASURE)->EnableWindow(false);

	runposX = g_pSqOneControl->m_SQOneParseData.X;
	runposY = g_pSqOneControl->m_SQOneParseData.Y;
	runposZ = g_pSqOneControl->m_SQOneParseData.Z;
	runposRx = g_pSqOneControl->m_SQOneParseData.Rx;
	runposRy = g_pSqOneControl->m_SQOneParseData.Ry;
	runposRz = g_pSqOneControl->m_SQOneParseData.Rz;
	runposCx = g_pSqOneControl->m_SQOneParseData.Cx;
	runposCy = g_pSqOneControl->m_SQOneParseData.Cy;
	runposCz = g_pSqOneControl->m_SQOneParseData.Cz;

	g_pBeamMain->SaveLogFile("SQONE 2D SEARCH PHASE RESULT", (LPSTR)(LPCTSTR)("2D SEARCH PHASE TEST START"));

	m_nScanNumber = 0;
	g_pBeamMain->m_d2DScanIntensity[0] = 0;

	if (g_pSqOneControl->m_bStopFlag == FALSE)
	{
		for (int i = 0; i < n1Iteration; i++)
		{
			switch (m_combobox_Sqscan_parameter1.GetCurSel())
			{
			case 0:
				runposRx = para1ary[i];
				break;
			case 1:
				runposRy = para1ary[i];
				break;
			case 2:
				runposRz = para1ary[i];
				break;
			case 3:
				runposX = para1ary[i];
				break;
			case 4:
				runposY = para1ary[i];
				break;
			case 5:
				runposZ = para1ary[i];
				break;
			}
			if (g_pSqOneControl->m_bStopFlag == FALSE)
			{
				nRet = g_pSqOneControl->waitUntilMoveComplete(runposX, runposY, runposZ, runposRx, runposRy, runposRz, runposCx, runposCy, runposCz); // Sq-1 Command input
				if (nRet != 0)
				{
					break;
				}
				else
				{
					strLog.Format(_T("Move to first : %02.4f,%02.4f,%02.4f,%02.4f,%02.4f,%02.4f"), runposX, runposY, runposZ, runposRx, runposRy, runposRz);
					g_pBeamMain->Display(0, strLog);
				}
			}
			else
			{
				nRet = -10;
				break;
			}

			for (int j = 0; j < n2Iteration; j++)
			{
				switch (m_combobox_Sqscan_parameter2.GetCurSel())
				{
				case 0:
					runposRx = para2ary[j];
					break;
				case 1:
					runposRy = para2ary[j];
					break;
				case 2:
					runposRz = para2ary[j];
					break;
				case 3:
					runposX = para2ary[j];
					break;
				case 4:
					runposY = para2ary[j];
					break;
				case 5:
					runposZ = para2ary[j];
					break;
				}

				strLog.Format(_T("%03d / %03d"), m_nScanNumber + 1, tonum);
				if (g_pBeamAutoAlign->m_isAutoAlignOn == TRUE)
				{
					g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_DSIPSCANNUM, strLog);
				}
				else
				{
					SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_DISPSCANNUM, strLog);
				}
				// sqr-1 stage motion 

				// sqr-1 stage motion 
				if (g_pSqOneControl->m_bStopFlag == FALSE)
				{
					nRet = g_pSqOneControl->checkLimit(runposX, runposY, runposZ, runposRx, runposRy, runposRz);
					if (nRet != 0)
					{
						nRet = -2;
						break;
					}
					else
					{

					}
				}
				else
				{
					nRet = -10;
					break;
				}

				if (g_pSqOneControl->m_bStopFlag == FALSE)
				{
					nRet = g_pSqOneControl->waitUntilMoveComplete(runposX, runposY, runposZ, runposRx, runposRy, runposRz, runposCx, runposCy, runposCz); // Sq-1 Command input
					if (nRet != 0)
					{
						nRet = -1;
						break;
					}
					else
					{
						strLog.Format(_T("Move to first : %02.4f,%02.4f,%02.4f,%02.4f,%02.4f,%02.4f"), runposX, runposY, runposZ, runposRx, runposRy, runposRz);
						g_pBeamMain->Display(0, strLog);
					}
				}
				else
				{
					nRet = -10;
					break;
				}
				if (g_pSqOneControl->m_bStopFlag == FALSE)
				{
					if (g_pBeamMain->IsDlgButtonChecked(IDC_BEAMALIGNMAIN_CHECK_SIMULATOR) == BST_CHECKED)
					{
						g_pBeamMain->caculateIntensty();
					}
					else
					{
						if (g_pConfig->m_nEquipmentMode == OFFLINE)
						{
							g_pBeamMain->caculateIntensty();
						}
						else
						{
							if (g_pConfig->m_nEquipmentType == PHASE)
							{
								int tempRet = g_pBeamMain->getMeanValuefromXrayCam();
								if (tempRet != 0)
								{
									if (tempRet == -1)
									{
										nRet = -6;
									}
									else
									{
										nRet = -5;
									}
									break;
								}
							}
							else if (g_pConfig->m_nEquipmentType == SREM033)
							{
								g_pBeamMain->getIntensityfromADAM();
							}
							else
							{

							}
						}
					}
				}
				else
				{
					nRet = -10;
					break;
				}
				
				g_pBeamMain->m_chartx[m_nScanNumber] = para1ary[i];
				g_pBeamMain->m_charty[m_nScanNumber] = para2ary[j];
				g_pBeamMain->m_chartz[m_nScanNumber] = g_pBeamMain->m_dIntensity;
				strLog.Format(_T("Log\t %d\t %f\t %f\t %f"), m_nScanNumber + 1, para1ary[i], para2ary[j], g_pBeamMain->m_dIntensity);
				g_pBeamMain->Display(0, strLog);
				g_pBeamMain->SaveLogFile("SQONE 2D SEARCH PHASE RESULT", (LPSTR)(LPCTSTR)strLog);

				if (g_pBeamAutoAlign->m_isAutoAlignOn == TRUE)
				{
					g_pBeamAutoAlign->m_ContourChartViewer.updateViewPort(true, false);

					CString temp;
					g_pBeamAutoAlign->GetDlgItemTextA(IDC_BEAMALIGN_EDIT_DSIPDETINTENSITY, temp);
					double nIntsntiy = _ttof(temp);
					if (g_pBeamMain->m_chartz[m_nScanNumber] >= nIntsntiy)
					{
						g_pBeamAutoAlign->m_isIntensityOn = TRUE;
						if (g_pBeamMain->m_chartz[m_nScanNumber] == g_pBeamMain->m_d2DScanIntensity[0])
						{
							nIndexIntensityNumber++;
							g_pBeamMain->m_d2DScanIntensity[nIndexIntensityNumber] = g_pBeamMain->m_chartz[m_nScanNumber];
							g_pBeamMain->m_d2DScanpPrameter1[nIndexIntensityNumber] = g_pBeamMain->m_chartx[m_nScanNumber];
							g_pBeamMain->m_d2DScanpPrameter2[nIndexIntensityNumber] = g_pBeamMain->m_charty[m_nScanNumber];

						}
						else if (g_pBeamMain->m_chartz[m_nScanNumber] > g_pBeamMain->m_d2DScanIntensity[nIndexIntensityNumber])
						{
							for (int i = 0; i < nIndexIntensityNumber; i++)
							{
								g_pBeamMain->m_d2DScanIntensity[i] = 0;
								g_pBeamMain->m_d2DScanpPrameter1[i] = 0;
								g_pBeamMain->m_d2DScanpPrameter2[i] = 0;
							}
							nIndexIntensityNumber = 0;
							g_pBeamMain->m_d2DScanIntensity[nIndexIntensityNumber] = g_pBeamMain->m_chartz[m_nScanNumber];
							g_pBeamMain->m_d2DScanpPrameter1[nIndexIntensityNumber] = g_pBeamMain->m_chartx[m_nScanNumber];
							g_pBeamMain->m_d2DScanpPrameter2[nIndexIntensityNumber] = g_pBeamMain->m_charty[m_nScanNumber];
						}
						if (g_pBeamAutoAlign->m_isAutoAlignOn == TRUE && g_pBeamAutoAlign->m_nAutoAlignStatus == 1)
						{
							nRet = 1;
							break;
						}
					}
				}
				else
				{
					g_pBeamSearch2D->m_XYchartViewer.updateViewPort(true, false);
				}
				m_nScanNumber++;
			}
			if (nRet != 0)
			{
				break;
			}
		}
	}
	m_bisScan = FALSE;
	delete[] para1ary;
	delete[] para2ary;
	para1ary = NULL;
	para2ary = NULL;
	strLog.Format(_T("2D SEARCH PHASE TEST END"));
	g_pBeamMain->SaveLogFile("SQONE 2D SEARCH PHASE RESULT", (LPSTR)(LPCTSTR)strLog);

	if (nRet == 0)
	{
		//strScanlog.Format(_T(""));
		g_pBeamMain->Display(0, "Scan End : Finished");
	}
	else if (nRet == -1)
	{
		g_pBeamMain->Display(0, "Scan End : Time Out Error");
	}
	else if (nRet == -2)
	{
		g_pBeamMain->Display(0, "Scan End : Divergence Error");
	}
	else if (nRet == -3)
	{
		g_pBeamMain->Display(0, "Scan End : Stage Stop");
	}
	else if (nRet == -5)
	{
		g_pBeamMain->Display(0, "Scan End : XRay Camera Timeout error");
	}
	else if (nRet == -6)
	{
		g_pBeamMain->Display(0, "Scan End : XRay Camera Not Connected");
	}
	else if (nRet == 1)
	{
		g_pBeamMain->Display(0, "Scan End : Find Beam Intensity");
	}
	else if (nRet == -10)
	{
		g_pBeamMain->Display(0, "Scan End : Stop Button Clicked");
	}
	else
	{
		nRet = -99;
		g_pBeamMain->Display(0, "Scan End : Unknown Error");

	}

	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CHECKPARAMETER)->EnableWindow(TRUE);
	return nRet;
}

int CBeamSearch2DDlg::runCirculScan()
{
	g_pSqOneControl->m_bStopFlag = FALSE;
	m_bisScan = TRUE;

	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CANCEL)->EnableWindow(FALSE);

	if (g_pBeamAutoAlign->m_isAutoAlignOn == FALSE)
	{
		g_pBeamMain->resetDataset();
	}

	int nRet = 0;
	m_nDirectionIndex = 0;
	CString strLog = "";
	g_pBeamMain->Display(0, "Direction Search Start");
	double runposX = 0, runposY = 0, runposZ = 0, runposRx = 0, runposRy = 0, runposRz = 0, runposCx = 0, runposCy = 0, runposCz = 0;
	int tonum = 0, nIndexIntensityNumber = 0;
	tonum = 12;

	runposX = g_pSqOneControl->m_SQOneParseData.X;
	runposY = g_pSqOneControl->m_SQOneParseData.Y;
	runposZ = g_pSqOneControl->m_SQOneParseData.Z;
	runposRx = g_pSqOneControl->m_SQOneParseData.Rx;
	runposRy = g_pSqOneControl->m_SQOneParseData.Ry;
	runposRz = g_pSqOneControl->m_SQOneParseData.Rz;
	runposCx = g_pSqOneControl->m_SQOneParseData.Cx;
	runposCy = g_pSqOneControl->m_SQOneParseData.Cy;
	runposCz = g_pSqOneControl->m_SQOneParseData.Cz;

	double *para1ary, *para2ary;

	tonum = 12;
	para1ary = new double[tonum];
	para2ary = new double[tonum];
	memset(para1ary, NULL, sizeof(double) * tonum);
	memset(para2ary, NULL, sizeof(double) * tonum);
	float pi = 3.14159265359;
	double weight = 0.1;
	for (int i = 0; i < tonum; i++)
	{
		para1ary[i] = g_pBeamMain->m_d2DScanpPrameter1[0] + weight * cos(2 * pi* i / tonum);
		para2ary[i] = g_pBeamMain->m_d2DScanpPrameter2[0] + weight * sin(2 * pi* i / tonum);
	}

	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_RUNSEARCH)->EnableWindow(false);
	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_RUNMEASURE)->EnableWindow(false);

	m_nScanNumber = 0;
	g_pBeamMain->m_dCurcleScanMaxIntensity[0] = 0;
	if (!g_pSqOneControl->m_bStopFlag && nRet == 0)
	{
		for (int i = 0; i < tonum; i++)
		{
			
			strLog.Format(_T("%03d / %03d"), m_nScanNumber + 1 , tonum);
			if (g_pBeamAutoAlign->m_isAutoAlignOn == TRUE)
			{
				g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_DSIPSCANNUM, strLog);
			}
			else
			{
				SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_DISPSCANNUM, strLog);
			}

			if (g_pSqOneControl->m_bStopFlag == FALSE)
			{
				nRet = g_pSqOneControl->checkLimit(runposX, runposY, runposZ, para1ary[i], para2ary[i], runposRz);
				if (nRet != 0)
				{
					nRet = -2;
					break;
				}
			}
			else
			{
				nRet = -10;
				break;
			}

			if (g_pSqOneControl->m_bStopFlag == FALSE)
			{
				nRet = g_pSqOneControl->waitUntilMoveComplete(runposX, runposY, runposZ, para1ary[i], para2ary[i], runposRz, runposCx, runposCy, runposCz); // Sq-1 Command input
				if (nRet != 0)
				{
					nRet = -1;
					break;
				}
				else
				{
					strLog.Format(_T("Move to Scan : %02.4f,%02.4f,%02.4f,%02.4f,%02.4f,%02.4f"), runposX, runposY, runposZ, para1ary[i], para2ary[i], runposRz);
					g_pBeamMain->Display(0, strLog);
				}
			}
			else
			{
				nRet = -10;
				break;
			}
			if (g_pSqOneControl->m_bStopFlag == FALSE)
			{
				if (g_pBeamMain->IsDlgButtonChecked(IDC_BEAMALIGNMAIN_CHECK_SIMULATOR) == BST_CHECKED)
				{
					g_pBeamMain->caculateIntensty();
				}
				else
				{
					if (g_pConfig->m_nEquipmentMode == OFFLINE)
					{
						g_pBeamMain->caculateIntensty();
					}
					else
					{
						if (g_pConfig->m_nEquipmentType == SREM033)
						{
							g_pBeamMain->getIntensityfromADAM();
						}
						else
						{

						}
					}
				}
			}
			else
			{
				nRet = -10;
				break;
			}
			if (g_pBeamMain->IsDlgButtonChecked(IDC_BEAMALIGNMAIN_CHECK_SIMULATOR) == BST_CHECKED)
			{
				g_pBeamMain->caculateIntensty();
			}
			else
			{
				if (g_pConfig->m_nEquipmentMode == OFFLINE)
				{
					g_pBeamMain->caculateIntensty();
				}
				else 
				{
					if (g_pConfig->m_nEquipmentType == SREM033)
					{
						g_pBeamMain->getIntensityfromADAM();
					}
					else
					{

					}
				}
			}
			g_pBeamMain->m_chartx[m_nScanNumber] = para1ary[i];
			g_pBeamMain->m_charty[m_nScanNumber] = para2ary[i];
			g_pBeamMain->m_chartz[m_nScanNumber] = g_pBeamMain->m_dIntensity;
			strLog.Format(_T("Log : %f,%f,%f"), para1ary[i], para2ary[i], g_pBeamMain->m_dIntensity);
			g_pBeamMain->Display(0, strLog);

			if (g_pBeamAutoAlign->m_isAutoAlignOn == TRUE)
			{
				g_pBeamAutoAlign->m_ContourChartViewer.updateViewPort(true, false);
			}
			else
			{
				g_pBeamSearch2D->m_XYchartViewer.updateViewPort(true, false);
			}

			CString temp;
			g_pBeamAutoAlign->GetDlgItemTextA(IDC_BEAMALIGN_EDIT_DSIPDETINTENSITY, temp);
			double nIntsntiy = _ttof(temp);

			if (g_pBeamMain->m_chartz[m_nScanNumber] > g_pBeamMain->m_dCurcleScanMaxIntensity[0])
			{
				g_pBeamMain->m_dCurcleScanMaxIntensity[0] = g_pBeamMain->m_chartz[m_nScanNumber];
				m_nDirectionIndex = m_nScanNumber;
			}

			m_nScanNumber++;

		}
	}

	m_bisScan = false;
	delete[] para1ary;
	delete[] para2ary;
	para1ary = NULL;
	para2ary = NULL;

	if (nRet == 0)
	{
		//strScanlog.Format(_T(""));
		g_pBeamMain->Display(0, "Scan End : Finished");
	}
	else if (nRet == -1)
	{
		g_pBeamMain->Display(0, "Scan End : Time Out Error");
	}
	else if (nRet == -2)
	{
		g_pBeamMain->Display(0, "Scan End : Divergence Error");
	}
	else if (nRet == -3)
	{
		g_pBeamMain->Display(0, "Scan End : Stage Stop");
	}
	else
	{
		nRet == -99;
		g_pBeamMain->Display(0, "Scan End : Unknown Error");
	}

	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CHECKPARAMETER)->EnableWindow(true);
	return nRet;
}



void CBeamSearch2DDlg::OnBnClickedButtonSqscanCheckparameter()
{
	g_pBeamMain->Display(0, "Check Parameter Button Clicked");
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		checkScanSet();
	}
	else
	{
		AfxMessageBox("SQ One Stage 통신 상태 확인");
	}
}


void CBeamSearch2DDlg::OnBnClickedButtonSqscanCancel()
{
	g_pBeamMain->Display(0, "Setting Cancel Button Clicked");
	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CHECKPARAMETER)->EnableWindow(true);
	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_RUNSEARCH)->EnableWindow(false);
	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_RUNMEASURE)->EnableWindow(false);
	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CANCEL)->EnableWindow(false);
}


void CBeamSearch2DDlg::OnBnClickedButtonSqscanRunsearch()
{
	g_pBeamMain->Display(0, "Run Scan Button Clicked");
	if (g_pConfig->m_nEquipmentType == PHASE)
	{
		int nRet = AfxMessageBox("SQ One Stage Network 설정을 확인하셨나요", MB_YESNO);
		if (nRet == IDNO)
		{
			return;
		}
		else
		{
			runSearch();
		}
	}
	else
	{
		g_pBeamSearch2D->m_pThread = AfxBeginThread(g_pBeamSearch2D->runSCAN, this);
	}
	
}

void CBeamSearch2DDlg::OnCbnSelchangeComboSqscanParameter1()
{
	if (m_combobox_Sqscan_parameter1.GetCurSel() <= 2)
	{
		SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_UNIT, "mrad");
	}
	else
	{
		SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_UNIT, "mm");
	}
}


void CBeamSearch2DDlg::OnCbnSelchangeComboSqscanParameter2()
{
	if (m_combobox_Sqscan_parameter2.GetCurSel() <= 2)
	{
		SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_UNIT, "mrad");
	}
	else
	{
		SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_UNIT, "mm");
	}
}


int CBeamSearch2DDlg::checkScanParameter(CString str1LeftLimit, CString str1RightLimit, CString str1Iteration, CString str2LeftLimit, CString str2RightLimit, CString str2Iteration)
{
	if (g_pSqOneControl->m_bConnected != 1)
	{
		return -2;
	}

	if (m_combobox_Sqscan_parameter1.GetCurSel() == m_combobox_Sqscan_parameter2.GetCurSel())
	{
		return -3;
	}
	int iScanNum = (_ttof(str1Iteration))*(_ttof(str2Iteration));
	if (iScanNum > GatherNum || iScanNum == 0)
	{
		return -1;
	}

	switch (m_combobox_Sqscan_parameter1.GetCurSel() + 1)
	{
	case 1: //X
		if (_ttof(str1LeftLimit) < Xleftlimit || _ttof(str1LeftLimit) > Xrightlimit || (_ttof(str1RightLimit) < 0 && _ttof(str1LeftLimit) + _ttof(str1RightLimit)*(_ttof(str1Iteration) - 1) < Xleftlimit) || (_ttof(str1Iteration) > 0 && _ttof(str1LeftLimit) + _ttof(str1RightLimit)*(_ttof(str1Iteration) - 1) > Xrightlimit))
		{
			return -1;
		}
		break;
	case 2:
		if (_ttof(str1LeftLimit) < Yleftlimit || _ttof(str1LeftLimit) > Yrightlimit || (_ttof(str1RightLimit) < 0 && _ttof(str1LeftLimit) + _ttof(str1RightLimit)*(_ttof(str1Iteration) - 1) < Yleftlimit) || (_ttof(str1Iteration) > 0 && _ttof(str1LeftLimit) + _ttof(str1RightLimit)*(_ttof(str1Iteration) - 1) > Yrightlimit))
		{
			return -1;
		}
		break;
	case 3:
		if (_ttof(str1LeftLimit) < Zleftlimit || _ttof(str1LeftLimit) > Zrightlimit || (_ttof(str1RightLimit) < 0 && _ttof(str1LeftLimit) + _ttof(str1RightLimit)*(_ttof(str1Iteration) - 1) < Zleftlimit) || (_ttof(str1Iteration) > 0 && _ttof(str1LeftLimit) + _ttof(str1RightLimit)*(_ttof(str1Iteration) - 1) > Zrightlimit))
		{
			return -1;
		}
		break;
	case 4:
		if (_ttof(str1LeftLimit) < Rxleftlimit || _ttof(str1LeftLimit) > Rxrightlimit || (_ttof(str1RightLimit) < 0 && _ttof(str1LeftLimit) + _ttof(str1RightLimit)*(_ttof(str1Iteration) - 1) < Rxleftlimit) || (_ttof(str1Iteration) > 0 && _ttof(str1LeftLimit) + _ttof(str1RightLimit)*(_ttof(str1Iteration) - 1) > Rxrightlimit))
		{
			return -1;
		}
		break;
	case 5:
		if (_ttof(str1LeftLimit) < Ryleftlimit || _ttof(str1LeftLimit) > Ryrightlimit || (_ttof(str1RightLimit) < 0 && _ttof(str1LeftLimit) + _ttof(str1RightLimit)*(_ttof(str1Iteration) - 1) < Ryleftlimit) || (_ttof(str1Iteration) > 0 && _ttof(str1LeftLimit) + _ttof(str1RightLimit)*(_ttof(str1Iteration) - 1) > Ryrightlimit))
		{
			return -1;
		}
		break;
	case 6:
		if (_ttof(str1LeftLimit) < Rzleftlimit || _ttof(str1LeftLimit) > Rzrightlimit || (_ttof(str1RightLimit) < 0 && _ttof(str1LeftLimit) + _ttof(str1RightLimit)*(_ttof(str1Iteration) - 1) < Rzleftlimit) || (_ttof(str1Iteration) > 0 && _ttof(str1LeftLimit) + _ttof(str1RightLimit)*(_ttof(str1Iteration) - 1) > Rzrightlimit))
		{
			return -1;
		}
		break;
		//default:
	}
	switch (m_combobox_Sqscan_parameter2.GetCurSel() + 1)
	{
	case 1: //X
		if (_ttof(str2LeftLimit) < Xleftlimit || _ttof(str2LeftLimit) > Xrightlimit || (_ttof(str2RightLimit) < 0 && _ttof(str2LeftLimit) + _ttof(str2RightLimit)*(_ttof(str2Iteration) - 1) < Xleftlimit) || (_ttof(str2Iteration) > 0 && _ttof(str2LeftLimit) + _ttof(str2RightLimit)*(_ttof(str2Iteration) - 1) > Xrightlimit))
		{
			return -1;
		}
		break;
	case 2:
		if (_ttof(str2LeftLimit) < Yleftlimit || _ttof(str2LeftLimit) > Yrightlimit || (_ttof(str2RightLimit) < 0 && _ttof(str2LeftLimit) + _ttof(str2RightLimit)*(_ttof(str2Iteration) - 1) < Yleftlimit) || (_ttof(str2Iteration) > 0 && _ttof(str2LeftLimit) + _ttof(str2RightLimit)*(_ttof(str2Iteration) - 1) > Yrightlimit))
		{
			return  -1;
		}
		break;
	case 3:
		if (_ttof(str2LeftLimit) < Zleftlimit || _ttof(str2LeftLimit) > Zrightlimit || (_ttof(str2RightLimit) < 0 && _ttof(str2LeftLimit) + _ttof(str2RightLimit)*(_ttof(str2Iteration) - 1) < Zleftlimit) || (_ttof(str2Iteration) > 0 && _ttof(str2LeftLimit) + _ttof(str2RightLimit)*(_ttof(str2Iteration) - 1) > Zrightlimit))
		{
			return -1;
		}
		break;
	case 4:
		if (_ttof(str2LeftLimit) < Rxleftlimit || _ttof(str2LeftLimit) > Rxrightlimit || (_ttof(str2RightLimit) < 0 && _ttof(str2LeftLimit) + _ttof(str2RightLimit)*(_ttof(str2Iteration) - 1) < Rxleftlimit) || (_ttof(str2Iteration) > 0 && _ttof(str2LeftLimit) + _ttof(str2RightLimit)*(_ttof(str2Iteration) - 1) > Rxrightlimit))
		{
			return -1;
		}
		break;
	case 5:
		if (_ttof(str2LeftLimit) < Ryleftlimit || _ttof(str2LeftLimit) > Ryrightlimit || (_ttof(str2RightLimit) < 0 && _ttof(str2LeftLimit) + _ttof(str2RightLimit)*(_ttof(str2Iteration) - 1) < Ryleftlimit) || (_ttof(str2Iteration) > 0 && _ttof(str2LeftLimit) + _ttof(str2RightLimit)*(_ttof(str2Iteration) - 1) > Ryrightlimit))
		{
			return -1;
		}
		break;
	case 6:
		if (_ttof(str2LeftLimit) < Rzleftlimit || _ttof(str2LeftLimit) > Rzrightlimit || (_ttof(str2RightLimit) < 0 && _ttof(str2LeftLimit) + _ttof(str2RightLimit)*(_ttof(str2Iteration) - 1) < Rzleftlimit) || (_ttof(str2Iteration) > 0 && _ttof(str2LeftLimit) + _ttof(str2RightLimit)*(_ttof(str2Iteration) - 1) > Rzrightlimit))
		{
			return -1;
		}
		break;
		//default:
	}


	return 0; 
}


BOOL CBeamSearch2DDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}

void CBeamSearch2DDlg::drawXYChart(CChartViewer * viewer)
{
	ptrXYChart = new XYChart(550, 500);

	//cartesian->setRoundedFrame(m_extBgColor);
	ptrXYChart->addTitle("Intensity Contour Chart", "arial.ttf", 14
	)->setBackground(0xdddddd, 0x000000, Chart::glassEffect());

	//ptrTempChart->setPlotArea(80, 60, 350, 350, 0xffffff, -1, -1, 0xcccccc, 0xcccccc);
	ptrXYChart->setPlotArea(80, 60, 350, 350, 0xffffff, -1, -1, ptrXYChart->dashLineColor(0x80000000, Chart::DotLine), -1);
	ptrXYChart->setBackground(LIGHT_GRAY, Transparent, 0);

	//double dataX[] = { -2, -2, 2, 2 };
	//double dataY[] = { -2, 2, 2, -2 };
	//double dataZ[];

	ContourLayer *layer = ptrXYChart->addContourLayer(DoubleArray(g_pBeamMain->m_chartx, g_pBeamSearch2D->m_nScanNumber +1),
		DoubleArray(g_pBeamMain->m_charty, g_pBeamSearch2D->m_nScanNumber +1), DoubleArray(g_pBeamMain->m_chartz, g_pBeamSearch2D->m_nScanNumber +1));

	layer->setContourColor(Chart::Transparent);

	ptrXYChart->getPlotArea()->moveGridBefore(layer);


	ptrXYChart->xAxis()->setLabelStyle("arial.ttf", 12);
	ptrXYChart->yAxis()->setLabelStyle("arial.ttf", 12);

	ptrXYChart->xAxis()->setColors(Chart::Transparent, Chart::TextColor, Chart::TextColor, 0xaaaaaa);
	ptrXYChart->yAxis()->setColors(Chart::Transparent);

	//ptrTempChart->xAxis()->setTickLength(0, 0);

	ptrXYChart->xAxis()->setTickDensity(40);
	ptrXYChart->yAxis()->setTickDensity(40);

	//ptrTempChart->xAxis()->setWidth(2);
	//ptrTempChart->yAxis()->setWidth(2); 
	CString strAxis1 = "", strAxis2 = "";
	switch (m_combobox_Sqscan_parameter1.GetCurSel())
	{
	case 0:
		strAxis1 = "Pitch (mrad)";
		break;
	case 1:
		strAxis1 = "Yaw (mrad)";
		break;
	case 2:
		strAxis1 = "Roll (mrad)";
		break;
	case 3:
		strAxis1 = "Horizontal (mm)";
		break;
	case 4:
		strAxis1 = "Vertical (mm)";
		break;
	case 5:
		strAxis1 = "Beam (mm)";
		break;
	}
	switch (m_combobox_Sqscan_parameter2.GetCurSel())
	{
	case 0:
		strAxis2 = "Pitch (mrad)";
		break;
	case 1:
		strAxis2 = "Yaw (mrad)";
		break;
	case 2:
		strAxis2 = "Roll (mrad)";
		break;
	case 3:
		strAxis2 = "Horizontal (mm)";
		break;
	case 4:
		strAxis2 = "Vertical (mm)";
		break;
	case 5:
		strAxis2 = "Beam (mm)";
		break;
	}
	ptrXYChart->xAxis()->setTitle(strAxis1, "arial.ttf", 14, 0x555555);
	ptrXYChart->yAxis()->setTitle(strAxis2, "arial.ttf", 14, 0x555555);

	ColorAxis *cAxis = layer->setColorAxis(470, 250, Chart::Left, 350, Chart::Right);
	cAxis->setBoundingBox(0xeeeeee, 0x444444);
	//cAxis->setTitle("Intensity Value (  )", "arialbi.ttf", 12);

	//cAxis->setLabelStyle("arialbd.ttf");

	cAxis->setColorGradient(true);

	delete viewer->getChart();
	viewer->setChart(ptrXYChart);
}

void CBeamSearch2DDlg::OnViewPortChangedChart()
{
	if (m_XYchartViewer.needUpdateChart())
	drawXYChart(&m_XYchartViewer);
}

UINT CBeamSearch2DDlg::runSCAN(LPVOID lParam)
{
	g_pBeamSearch2D->runSearch();

	return 0;
}

UINT CBeamSearch2DDlg::runMeasure(LPVOID lParam)
{
	g_pBeamSearch2D->runSearchMeasure();
	return 0;
}


void CBeamSearch2DDlg::OnBnClickedBeamsearch2dButtonRunmeasure()
{
	g_pBeamMain->Display(0, "Run Scan Button Clicked");
	if (g_pConfig->m_nEquipmentType = PHASE)
	{
		int nRet = AfxMessageBox("SQ One Stage Network 설정을 확인하셨나요", MB_YESNO);
		if (nRet == IDNO)
		{
			return;
		}
		else
		{
			runSearchMeasure();
		}
	}
	else
	{
		g_pBeamSearch2D->m_pThread = AfxBeginThread(g_pBeamSearch2D->runMeasure, this);
	}
}

int CBeamSearch2DDlg::runSearchMeasure()
{
	g_pSqOneControl->m_bStopFlag = FALSE;
	m_bisScan = TRUE;

	if (g_pBeamAutoAlign->m_isAutoAlignOn == FALSE)
	{
		g_pBeamMain->resetDataset();
	}

	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CANCEL)->EnableWindow(FALSE);

	int nRet = 0;
	CString strLog = "";

	//g_pBeamMain->Display(0, "Scan Start");
	double runposX = 0, runposY = 0, runposZ = 0, runposRx = 0, runposRy = 0, runposRz = 0, runposCx = 0, runposCy = 0, runposCz = 0;
	CString	parm1LL = "", parm1RL = "", parm1It = "", parm2LL = "", parm2RL = "", parm2It = "", maxval = "", temp = "";

	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_STARTPOSITION, parm1LL);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_INCREMENT, parm1RL);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER1_ITERATION, parm1It);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_STARTPOSITION, parm2LL);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_INCREMENT, parm2RL);
	GetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_PARAMETER2_ITERATION, parm2It);

	int tonum = 0, nIndexIntensityNumber = 0;
	tonum = _ttof(parm1It)*_ttof(parm2It);

	int n1Iteration, n2Iteration;
	double *para1ary, *para2ary;

	n1Iteration = _ttoi(parm1It);
	para1ary = new double[_ttoi(parm1It)];
	memset(para1ary, NULL, sizeof(double) * _ttoi(parm1It));


	n2Iteration = _ttoi(parm2It);
	para2ary = new double[_ttoi(parm2It)];
	memset(para2ary, NULL, sizeof(double) * _ttoi(parm2It));

	for (int ti = 0; ti < n1Iteration; ti++)
	{
		para1ary[ti] = _ttof(parm1LL) + _ttof(parm1RL) * ti;
	}
	for (int ti = 0; ti < n2Iteration; ti++)
	{
		para2ary[ti] = _ttof(parm2LL) + _ttof(parm2RL) * ti;
	}

	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_RUNSEARCH)->EnableWindow(false);
	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_RUNMEASURE)->EnableWindow(false);

	runposX = g_pSqOneControl->m_SQOneParseData.X;
	runposY = g_pSqOneControl->m_SQOneParseData.Y;
	runposZ = g_pSqOneControl->m_SQOneParseData.Z;
	runposRx = g_pSqOneControl->m_SQOneParseData.Rx;
	runposRy = g_pSqOneControl->m_SQOneParseData.Ry;
	runposRz = g_pSqOneControl->m_SQOneParseData.Rz;
	runposCx = g_pSqOneControl->m_SQOneParseData.Cx;
	runposCy = g_pSqOneControl->m_SQOneParseData.Cy;
	runposCz = g_pSqOneControl->m_SQOneParseData.Cz;

	g_pBeamMain->SaveLogFile("SQONE 2D SEARCH PHASE RESULT", (LPSTR)(LPCTSTR)("2D SEARCH PHASE TEST START"));

	m_nScanNumber = 0;
	g_pBeamMain->m_d2DScanIntensity[0] = 0;

	if (g_pSqOneControl->m_bStopFlag == FALSE)
	{
		for (int i = 0; i < n1Iteration; i++)
		{
			switch (m_combobox_Sqscan_parameter1.GetCurSel())
			{
			case 0:
				runposRx = para1ary[i];
				break;
			case 1:
				runposRy = para1ary[i];
				break;
			case 2:
				runposRz = para1ary[i];
				break;
			case 3:
				runposX = para1ary[i];
				break;
			case 4:
				runposY = para1ary[i];
				break;
			case 5:
				runposZ = para1ary[i];
				break;
			}
			if (g_pSqOneControl->m_bStopFlag == FALSE)
			{
				nRet = g_pSqOneControl->waitUntilMoveComplete(runposX, runposY, runposZ, runposRx, runposRy, runposRz, runposCx, runposCy, runposCz); // Sq-1 Command input
				if (nRet != 0)
				{
					break;
				}
				else
				{
					strLog.Format(_T("Move to first : %02.4f,%02.4f,%02.4f,%02.4f,%02.4f,%02.4f"), runposX, runposY, runposZ, runposRx, runposRy, runposRz);
					g_pBeamMain->Display(0, strLog);
				}
			}
			else
			{
				nRet = -10;
				break;
			}

			for (int j = 0; j < n2Iteration; j++)
			{
				switch (m_combobox_Sqscan_parameter2.GetCurSel())
				{
				case 0:
					runposRx = para2ary[j];
					break;
				case 1:
					runposRy = para2ary[j];
					break;
				case 2:
					runposRz = para2ary[j];
					break;
				case 3:
					runposX = para2ary[j];
					break;
				case 4:
					runposY = para2ary[j];
					break;
				case 5:
					runposZ = para2ary[j];
					break;
				}

				strLog.Format(_T("%03d / %03d"), m_nScanNumber + 1, tonum);
				if (g_pBeamAutoAlign->m_isAutoAlignOn == TRUE)
				{
					g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_DSIPSCANNUM, strLog);
				}
				else
				{
					SetDlgItemTextA(IDC_BEAMSEARCH2D_EDIT_DISPSCANNUM, strLog);
				}

				// sqr-1 stage motion 
				if (g_pSqOneControl->m_bStopFlag == FALSE)
				{
					nRet = g_pSqOneControl->checkLimit(runposX, runposY, runposZ, runposRx, runposRy, runposRz);
					if (nRet != 0)
					{
						nRet = -2;
						break;
					}
					else
					{

					}
				}
				else
				{
					nRet = -10;
					break;
				}

				if (g_pSqOneControl->m_bStopFlag == FALSE)
				{
					nRet = g_pSqOneControl->waitUntilMoveComplete(runposX, runposY, runposZ, runposRx, runposRy, runposRz, runposCx, runposCy, runposCz); // Sq-1 Command input
					if (nRet != 0)
					{
						break;
					}
					else
					{
						strLog.Format(_T("Move to first : %02.4f,%02.4f,%02.4f,%02.4f,%02.4f,%02.4f"), runposX, runposY, runposZ, runposRx, runposRy, runposRz);
						g_pBeamMain->Display(0, strLog);
					}
				}
				else
				{
					nRet = -10;
					break;
				}
				if (g_pSqOneControl->m_bStopFlag == FALSE)
				{
					if (g_pBeamMain->IsDlgButtonChecked(IDC_BEAMALIGNMAIN_CHECK_SIMULATOR) == BST_CHECKED)
					{
						g_pBeamMain->caculateIntensty();
					}
					else
					{
						if (g_pConfig->m_nEquipmentMode == OFFLINE)
						{
							g_pBeamMain->caculateIntensty();
						}
						else
						{
							if (g_pConfig->m_nEquipmentType == PHASE)
							{

				

								int tempRet = g_pBeamMain->executeMeasureformPhase();
								if (tempRet != 0)
								{
									nRet = -7;
									break;
								}
							}
							else if (g_pConfig->m_nEquipmentType == SREM033)
							{
								g_pBeamMain->getIntensityfromADAM();
							}
							else
							{

							}
						}
					}
				}
				else
				{
					nRet = -10;
					break;
				}

				g_pBeamMain->m_chartx[m_nScanNumber] = para1ary[i];
				g_pBeamMain->m_charty[m_nScanNumber] = para2ary[j];
				g_pBeamMain->m_chartz[m_nScanNumber] = g_pBeamMain->m_dIntensity;
				strLog.Format(_T("Log\t %d\t %f\t %f\t %f"), m_nScanNumber + 1, para1ary[i], para2ary[j], g_pBeamMain->m_dIntensity);
				g_pBeamMain->Display(0, strLog);
				g_pBeamMain->SaveLogFile("SQONE 2D SEARCH PHASE RESULT", (LPSTR)(LPCTSTR)strLog);

				g_pBeamSearch2D->m_XYchartViewer.updateViewPort(true, false);
				m_nScanNumber++;
			}
			if (nRet != 0)
			{
				break;
			}
		}
	}
	m_bisScan = FALSE;
	delete[] para1ary;
	delete[] para2ary;
	para1ary = NULL;
	para2ary = NULL;
	strLog.Format(_T("2D SEARCH PHASE TEST END"));
	g_pBeamMain->SaveLogFile("SQONE 2D SEARCH PHASE RESULT", (LPSTR)(LPCTSTR)strLog);

	if (nRet == 0)
	{
		//strScanlog.Format(_T(""));
		g_pBeamMain->Display(0, "Scan End : Finished");
	}
	else if (nRet == -1)
	{
		g_pBeamMain->Display(0, "Scan End : Time Out Error");
	}
	else if (nRet == -2)
	{
		g_pBeamMain->Display(0, "Scan End : Divergence Error");
	}
	else if (nRet == -3)
	{
		g_pBeamMain->Display(0, "Scan End : Stage Stop");
	}
	else if (nRet == -5)
	{
		g_pBeamMain->Display(0, "Scan End : XRay Camera Timeout error");
	}
	else if (nRet == -6)
	{
		g_pBeamMain->Display(0, "Scan End : XRay Camera Not Connected");
	}
	else if (nRet == -7)
	{
		g_pBeamMain->Display(0, "Scan End : Phase Measure Fail");
	}
	else if (nRet == 1)
	{
		g_pBeamMain->Display(0, "Scan End : Find Beam Intensity");
	}
	else if (nRet == -10)
	{
		g_pBeamMain->Display(0, "Scan End : Stop Button Clicked");
	}
	else
	{
		nRet = -99;
		g_pBeamMain->Display(0, "Scan End : Unknown Error");

	}

	GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CHECKPARAMETER)->EnableWindow(TRUE);
	return nRet;
}
