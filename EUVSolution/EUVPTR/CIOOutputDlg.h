﻿#pragma once


// CDigitalOutput 대화 상자

class CDigitalOutput : public CDialogEx , public CECommon
{
	DECLARE_DYNAMIC(CDigitalOutput)

public:
	CDigitalOutput(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CDigitalOutput();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IO_OUTPUT_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	int		m_nChannel;
	HICON	m_LedIcon[3];

	int  SetOutputBitOnOff(int nIndex);
	void InitControls_DO(int nChannel);
	bool OnUpdateDigitalOutput();

	int WriteOutputBitOnOff(int SEL, int nCheck);

	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedCheckDigitaloutY000();
	afx_msg void OnBnClickedCheckDigitaloutY001();
	afx_msg void OnBnClickedCheckDigitaloutY002();
	afx_msg void OnBnClickedCheckDigitaloutY003();
	afx_msg void OnBnClickedCheckDigitaloutY004();
	afx_msg void OnBnClickedCheckDigitaloutY005();
	afx_msg void OnBnClickedCheckDigitaloutY006();
	afx_msg void OnBnClickedCheckDigitaloutY007();
	afx_msg void OnBnClickedCheckDigitaloutY008();
	afx_msg void OnBnClickedCheckDigitaloutY009();
	afx_msg void OnBnClickedCheckDigitaloutY010();
	afx_msg void OnBnClickedCheckDigitaloutY011();
	afx_msg void OnBnClickedCheckDigitaloutY012();
	afx_msg void OnBnClickedCheckDigitaloutY013();
	afx_msg void OnBnClickedCheckDigitaloutY014();
	afx_msg void OnBnClickedCheckDigitaloutY015();
	afx_msg void OnDestroy();

	CBrush  m_brush;
	CFont	m_font;

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
