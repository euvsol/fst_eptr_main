﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

// CMaskMapDlg 대화 상자

IMPLEMENT_DYNAMIC(CMaskMapDlg, CDialogEx)

CMaskMapDlg::CMaskMapDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MASK_MAP_DIALOG, pParent)
	, m_DateTimeStart(COleDateTime::GetCurrentTime())
	, m_DateTimeEnd(COleDateTime::GetCurrentTime())
	, m_bShowcode(FALSE)
	, m_bSelZoomCheck(FALSE)
	, m_bMaskMap(TRUE)
	, m_strStagePosName(_T("TBD"))
	, m_strStageStatus(_T("Stage Not Connected"))
	, m_strStageMovingDistance(_T("1.000000"))
	, m_strXMovePos(_T("0.0"))
	, m_strYMovePos(_T("0.0"))
	, m_bOMDisplay(FALSE)
	, m_nAutoRunRepeatSetNo(1)
	, m_nAutoRunRepeatResultNo(0)
	, m_nMeasurementPointNo(1)
	, m_dMaskCoordinateX_mm(0.0)
	, m_dMaskCoordinateY_mm(0.0)
	, m_bBiDirectionFlag(TRUE)
	, m_bOMAlignOnly(FALSE)
	, m_bManualAlign(FALSE)
	, m_bReverseCoordinate(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	bFinding = false;

	m_nMapWidthPixelNo = 0;
	m_nMapHeightPixelNo = 0;

	m_dStageMovingDistance = 1.000000;
	m_bOMAlignComplete = FALSE;
	m_bEUVAlignComplete = FALSE;
	m_bAutoSequenceProcessing = FALSE;

	m_dOMAlignPointLB_posx_mm = 0.0, m_dOMAlignPointLB_posy_mm = 0.0;
	m_dOMAlignPointLT_posx_mm = 0.0, m_dOMAlignPointLT_posy_mm = 0.0;
	m_dOMAlignPointRT_posx_mm = 0.0, m_dOMAlignPointRT_posy_mm = 0.0;
	m_dEUVAlignPointLB_posx_mm = 0.0, m_dEUVAlignPointLB_posy_mm = 0.0;
	m_dEUVAlignPointLT_posx_mm = 0.0, m_dEUVAlignPointLT_posy_mm = 0.0;
	m_dEUVAlignPointRT_posx_mm = 0.0, m_dEUVAlignPointRT_posy_mm = 0.0;

	m_dNotchAlignPoint1_posx_mm = 0.0, m_dNotchAlignPoint1_posy_mm = 0.0;
	m_dNotchAlignPoint2_posx_mm = 0.0, m_dNotchAlignPoint2_posy_mm = 0.0;

	m_bMacroSystemRunFlag = FALSE;

}

CMaskMapDlg::~CMaskMapDlg()
{
}


// CMaskMapDlg 메시지 처리기



void CMaskMapDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DATETIME_START, m_DateTimeStartCtrl);
	DDX_DateTimeCtrl(pDX, IDC_DATETIME_START, m_DateTimeStart);
	DDX_Control(pDX, IDC_DATETIME_END, m_DateTimeEndCtrl);
	DDX_DateTimeCtrl(pDX, IDC_DATETIME_END, m_DateTimeEnd);
	DDX_Control(pDX, IDC_LIST_FILELIST, m_ListCtrlRecipeFile);
	DDX_Control(pDX, IDC_BUTTON_SEARCHFILE, m_BtnSearchFileCtrl);
	DDX_Check(pDX, IDC_CHECK_DEFECTCODEDISPLAY, m_bShowcode);
	DDX_Control(pDX, IDC_BUTTON_X_MINUS, m_btnXMinusCtrl);
	DDX_Control(pDX, IDC_BUTTON_X_PLUS, m_btnXPlusCtrl);
	DDX_Control(pDX, IDC_BUTTON_Y_PLUS, m_btnYPlusCtrl);
	DDX_Control(pDX, IDC_BUTTON_Y_MINUS, m_btnYMinusCtrl);
	DDX_Text(pDX, IDC_EDIT_STAGE_POSNAME, m_strStagePosName);
	DDX_Text(pDX, IDC_EDIT_STAGE_STATUS, m_strStageStatus);
	DDX_Text(pDX, IDC_EDIT_INCREMENT_STEPSIZE, m_strStageMovingDistance);
	DDX_Text(pDX, IDC_EDIT_X_MOVEPOS, m_strXMovePos);
	DDX_Text(pDX, IDC_EDIT_Y_MOVEPOS, m_strYMovePos);
	DDX_Control(pDX, IDC_EDIT_X_FPOS, m_EditStagePosXCtrl);
	DDX_Control(pDX, IDC_EDIT_Y_FPOS, m_EditStagePosYCtrl);
	DDX_Control(pDX, IDC_CHECK_OMADAM, m_CheckOMAdamCtrl);
	DDX_Check(pDX, IDC_CHECK_OMADAM, m_bOMDisplay);
	DDX_Text(pDX, IDC_EDIT_SCAN_REPEAT_NUMBER, m_nAutoRunRepeatSetNo);
	DDX_Control(pDX, IDC_CHECK_DEFECTCODEDISPLAY, m_bCheckCommentDisplayCtrl);
	DDX_Control(pDX, IDC_EDIT_DEFECTCOORDX, m_EditLBAlignPointCoordXCtrl);
	DDX_Control(pDX, IDC_EDIT_DEFECTCOORDY, m_EditLBAlignPointCoordYCtrl);
	DDX_Control(pDX, IDC_EDIT_STAGECOORDX, m_EditMaskCenterCoordXCtrl);
	DDX_Control(pDX, IDC_EDIT_STAGECOORDY, m_EditMaskCenterCoordYCtrl);
	DDX_Control(pDX, IDC_EDIT_ORIGINCOORDX, m_EditMaskLBCoordXCtrl);
	DDX_Control(pDX, IDC_EDIT_ORIGINCOORDY, m_EditMaskLBCoordYCtrl);
	DDX_Text(pDX, IDC_EDIT_SCAN_REPEAT_NUMBER2, m_nAutoRunRepeatResultNo);
	DDX_Text(pDX, IDC_EDIT_POINTNO, m_nMeasurementPointNo);
	DDX_Text(pDX, IDC_EDIT_MASKCODX, m_dMaskCoordinateX_mm);
	DDX_Text(pDX, IDC_EDIT_MASKCODY, m_dMaskCoordinateY_mm);
	DDX_Control(pDX, IDC_CHECK_OMALIGNONLY, m_CheckOMAlignOnlyCtrl);
	DDX_Check(pDX, IDC_CHECK_OMALIGNONLY, m_bOMAlignOnly);
	DDX_Control(pDX, IDC_CHECK_ALIGN_AUTOMANUAL, m_CheckManualAlignCtrl);
	DDX_Check(pDX, IDC_CHECK_ALIGN_AUTOMANUAL, m_bManualAlign);
	DDX_Control(pDX, IDC_CHECK_OM_ADAM_UI, m_CheckOMAdamUi);
	DDX_Check(pDX, IDC_CHECK_MASKMAP_REVERSE_COORDINATE, m_bReverseCoordinate);
	DDX_Control(pDX, IDC_ICON_NAVIGATIONSTAGE_CONNECT_CHECK, m_Icon_NaviStageConnectState);
}

BEGIN_MESSAGE_MAP(CMaskMapDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_MAPFILELOAD, &CMaskMapDlg::OnBnClickedButtonRecipeFileLoad)
	ON_BN_CLICKED(IDC_BUTTON_MAPSAVE, &CMaskMapDlg::OnBnClickedButtonMapSave)
	ON_BN_CLICKED(IDC_CHECK_DEFECTCODEDISPLAY, &CMaskMapDlg::OnBnClickedCheckCodeShow)
	ON_NOTIFY(NM_CLICK, IDC_LIST_FILELIST, &CMaskMapDlg::OnNMClickListFilelist)
	ON_BN_CLICKED(IDC_BUTTON_SEARCHFILE, &CMaskMapDlg::OnBnClickedButtonRecipeFileSearch)
	ON_BN_CLICKED(IDC_BUTTON_MESUREPOINTMOVE, &CMaskMapDlg::OnBnClickedButtonMesurepointmove)
	ON_BN_CLICKED(IDC_BUTTON_MASKCOORDMOVE, &CMaskMapDlg::OnBnClickedButtonMaskcoordmove)
	ON_EN_CHANGE(IDC_EDIT_POINTNO, &CMaskMapDlg::OnEnChangeEditPointno)
	ON_EN_CHANGE(IDC_EDIT_MASKCODX, &CMaskMapDlg::OnEnChangeEditMaskcodx)
	ON_EN_CHANGE(IDC_EDIT_MASKCODY, &CMaskMapDlg::OnEnChangeEditMaskcody)
	ON_BN_CLICKED(IDC_CHECK_OMADAM, &CMaskMapDlg::OnBnClickedCheckOmadam)
	ON_BN_CLICKED(IDC_BUTTON_GET_POSITION, &CMaskMapDlg::OnBnClickedButtonGetPosition)
	ON_EN_CHANGE(IDC_EDIT_INCREMENT_STEPSIZE, &CMaskMapDlg::OnEnChangeEditIncrementStepsize)
	ON_BN_CLICKED(IDC_BUTTON_X_MINUS, &CMaskMapDlg::OnBnClickedButtonXMinus)
	ON_BN_CLICKED(IDC_BUTTON_X_PLUS, &CMaskMapDlg::OnBnClickedButtonXPlus)
	ON_BN_CLICKED(IDC_BUTTON_Y_PLUS, &CMaskMapDlg::OnBnClickedButtonYPlus)
	ON_BN_CLICKED(IDC_BUTTON_Y_MINUS, &CMaskMapDlg::OnBnClickedButtonYMinus)
	ON_WM_UNICHAR()
	ON_WM_TIMER()
	ON_NOTIFY(NM_CLICK, IDC_TEXT_STAGEPOSGRID, OnStagePosGridClick)
	ON_NOTIFY(NM_DBLCLK, IDC_TEXT_STAGEPOSGRID, OnStagePosGridDblClick)
	ON_NOTIFY(NM_RCLICK, IDC_TEXT_STAGEPOSGRID, OnStagePosGridRClick)
	ON_NOTIFY(GVN_BEGINLABELEDIT, IDC_TEXT_STAGEPOSGRID, OnStagePosGridStartEdit)
	ON_NOTIFY(GVN_ENDLABELEDIT, IDC_TEXT_STAGEPOSGRID, OnStagePosGridEndEdit)
	ON_NOTIFY(UDN_DELTAPOS, IDC_INCREMENT_STEPSPIN, &CMaskMapDlg::OnDeltaposIncrementStepspin)
	ON_BN_CLICKED(IDC_BUTTON_MASKLOAD, &CMaskMapDlg::OnBnClickedButtonMaskload)
	ON_BN_CLICKED(IDC_BUTTON_MASKALIGN, &CMaskMapDlg::OnBnClickedButtonMaskalign)
	ON_BN_CLICKED(IDC_BUTTON_MASKMEASURE_START, &CMaskMapDlg::OnBnClickedButtonMaskmeasureStart)
	ON_BN_CLICKED(IDC_BUTTON_MASKUNLOAD, &CMaskMapDlg::OnBnClickedButtonMaskunload)
	ON_EN_CHANGE(IDC_EDIT_SCAN_REPEAT_NUMBER, &CMaskMapDlg::OnEnChangeEditScanRepeatNumber)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_POSITION, &CMaskMapDlg::OnBnClickedButtonMovePosition)
	ON_BN_CLICKED(IDC_BUTTON_XY_MOVE, &CMaskMapDlg::OnBnClickedButtonXyMove)
	ON_EN_CHANGE(IDC_EDIT_X_MOVEPOS, &CMaskMapDlg::OnEnChangeEditXMovepos)
	ON_EN_CHANGE(IDC_EDIT_Y_MOVEPOS, &CMaskMapDlg::OnEnChangeEditYMovepos)
	ON_BN_CLICKED(IDC_BUTTON_ZOOMRESET, &CMaskMapDlg::OnBnClickedButtonZoomreset)
	ON_BN_CLICKED(IDC_BUTTON_MASKMEASURE_AUTOSTART, &CMaskMapDlg::OnBnClickedButtonMaskmeasureAutostart)
	ON_BN_CLICKED(IDC_BUTTON_STOP_AUTOSEQUENCE, &CMaskMapDlg::OnBnClickedButtonStopAutosequence)
	ON_BN_CLICKED(IDC_CHECK_MAGNIFICATION, &CMaskMapDlg::OnBnClickedCheckMagnification)
	ON_BN_CLICKED(IDC_CHECK_OMALIGNONLY, &CMaskMapDlg::OnBnClickedCheckOmalignonly)
	ON_BN_CLICKED(IDC_CHECK_ALIGN_AUTOMANUAL, &CMaskMapDlg::OnBnClickedCheckAlignAutomanual)
	ON_BN_CLICKED(IDC_CHECK_OM_ADAM_UI, &CMaskMapDlg::OnBnClickedCheckOmAdamUi)
	ON_BN_CLICKED(IDC_CHECK_MASKMAP_REVERSE_COORDINATE, &CMaskMapDlg::OnBnClickedCheckMaskmapReverseCoordinate)
	ON_BN_CLICKED(IDC_BUTTON_NAVIGATIONSTAGE_CONNECT, &CMaskMapDlg::OnBnClickedButtonNavigationstageConnect)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR_POSITION, &CMaskMapDlg::OnBnClickedButtonClearPosition)
	ON_BN_CLICKED(IDC_BUTTON_BACKUP_POSITION, &CMaskMapDlg::OnBnClickedButtonBackupPosition)
	ON_BN_CLICKED(IDC_MASKMAP_CHECK_SETCURRENTPOSITION, &CMaskMapDlg::OnBnClickedMaskmapCheckSetcurrentposition)
	ON_BN_CLICKED(IDC_MASKMAP_CHECK_SETSELECTPOSITION, &CMaskMapDlg::OnBnClickedMaskmapCheckSetselectposition)
	ON_CBN_EDITCHANGE(IDC_MASKMAP_COMBO_COUPONNUM, &CMaskMapDlg::OnCbnEditchangeMaskmapComboCouponnum)
	ON_CBN_SELCHANGE(IDC_MASKMAP_COMBO_COUPONNUM, &CMaskMapDlg::OnCbnSelchangeMaskmapComboCouponnum)
	ON_CBN_SELCHANGE(IDC_MASKMAP_COMBO_MEASURENUM, &CMaskMapDlg::OnCbnSelchangeMaskmapComboMeasurenum)
	ON_BN_CLICKED(IDC_MASKMAP_BUTTON_MEASUREBACKGROUND, &CMaskMapDlg::OnBnClickedMaskmapButtonMeasurebackground)
	ON_BN_CLICKED(IDC_MASKMAP_BUTTON_MEASUREWITHOUTPELLICLE, &CMaskMapDlg::OnBnClickedMaskmapButtonMeasurewithoutpellicle)
	ON_BN_CLICKED(IDC_MASKMAP_BUTTON_MEASUREWITHPELLICLE, &CMaskMapDlg::OnBnClickedMaskmapButtonMeasurewithpellicle)
	ON_BN_CLICKED(IDC_MASKMAP_BUTTON_MOVEPOSITION, &CMaskMapDlg::OnBnClickedMaskmapButtonMoveposition)
END_MESSAGE_MAP()


// CMaskMapDlg 메시지 처리기

BOOL CMaskMapDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);

	m_Icon_NaviStageConnectState.SetIcon((m_LedIcon[0]));

	// List initialization
	m_ListCtrlRecipeFile.InsertColumn(0, "File Name", LVCFMT_LEFT, 330, 0);
	m_ListCtrlRecipeFile.InsertColumn(1, "Size", LVCFMT_LEFT, 120, 3);
	//m_ListCtrlRecipeFile.InsertColumn(0, "File Name", LVCFMT_LEFT, 180, 0);
	//m_ListCtrlRecipeFile.InsertColumn(1, "Date", LVCFMT_LEFT, 140, 1);
	//m_ListCtrlRecipeFile.InsertColumn(2, "Size", LVCFMT_LEFT, 80, 3);

	// Set callback function
	m_filefinder.SetCallback(FileSearching, this);

	RecipeHeaderDisplay();

	CRect rect;
	CWnd *pWnd = GetDlgItem(IDC_PICTURE_MAP);
	pWnd->GetClientRect(rect);
	m_MaskMapWnd.Create(WS_CHILD | WS_VISIBLE /*| WS_VSCROLL | WS_HSCROLL*/, rect, pWnd);
	m_MaskMapWnd.ShowWindow(SW_SHOW);
	m_nMapWidthPixelNo = rect.Width();
	m_nMapHeightPixelNo = m_nMapWidthPixelNo;

	InitButtonSkin();
	InitStagePosGrid();

	m_strStagePosName = g_pConfig->m_stStagePos[0].chStagePositionString;
	m_strXMovePos.Format(_T("%3.3f"), g_pConfig->m_stStagePos[0].x);
	m_strYMovePos.Format(_T("%3.3f"), g_pConfig->m_stStagePos[0].y);


	if (g_pConfig != NULL)
	{
		g_pConfig->ReadRecoveryData();
		m_dOMAlignPointLB_posx_mm = g_pConfig->m_dOMAlignPointLB_X_mm;
		m_dOMAlignPointLB_posy_mm = g_pConfig->m_dOMAlignPointLB_Y_mm;
		m_dOMAlignPointLT_posx_mm = g_pConfig->m_dOMAlignPointLT_X_mm;
		m_dOMAlignPointLT_posy_mm = g_pConfig->m_dOMAlignPointLT_Y_mm;
		m_dOMAlignPointRT_posx_mm = g_pConfig->m_dOMAlignPointRT_X_mm;
		m_dOMAlignPointRT_posy_mm = g_pConfig->m_dOMAlignPointRT_Y_mm;
		m_dEUVAlignPointLB_posx_mm = g_pConfig->m_dEUVAlignPointLB_X_mm;
		m_dEUVAlignPointLB_posy_mm = g_pConfig->m_dEUVAlignPointLB_Y_mm;
		m_dEUVAlignPointLT_posx_mm = g_pConfig->m_dEUVAlignPointLT_X_mm;
		m_dEUVAlignPointLT_posy_mm = g_pConfig->m_dEUVAlignPointLT_Y_mm;
		m_dEUVAlignPointRT_posx_mm = g_pConfig->m_dEUVAlignPointRT_X_mm;
		m_dEUVAlignPointRT_posy_mm = g_pConfig->m_dEUVAlignPointRT_Y_mm;
		m_dNotchAlignPoint1_posx_mm = g_pConfig->m_dNotchAlignPoint1_X_mm;
		m_dNotchAlignPoint1_posy_mm = g_pConfig->m_dNotchAlignPoint1_Y_mm;
		m_dNotchAlignPoint2_posx_mm = g_pConfig->m_dNotchAlignPoint2_X_mm;
		m_dNotchAlignPoint2_posy_mm = g_pConfig->m_dNotchAlignPoint2_X_mm;
		m_bOMAlignComplete = g_pConfig->m_bOMAlignCompleteFlag;
		m_bEUVAlignComplete = g_pConfig->m_bEUVAlignCompleteFlag;
	}

	UpdateData(FALSE);
	m_MaskMapWnd.Invalidate(FALSE);

	m_CheckOMAdamUi.SetCheck(TRUE);

	switch (g_pConfig->m_nEquipmentType)
	{
	case EUVPTR:
		g_pPTR->ShowWindow(SW_HIDE);
		g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("PTR"));
		break;
	default:
		break;
	}

	//switch (m_CheckLaserFrequency.GetCheck())
	//{
	//case BST_CHECKED:
	//	m_CheckLaserFrequency.SetWindowText(_T("5 Khz"));
	//	break;
	//case BST_UNCHECKED:
	//	m_CheckLaserFrequency.SetWindowText(_T("1 Khz"));
	//	break;
	//default:
	//	break;
	//}

	m_AdamOmtiltSwitch = 0;


	//switch (m_CheckBidirectionCtrl.GetCheck())
	//{
	//case BST_CHECKED:
	//	m_CheckBidirectionCtrl.SetWindowText(_T("BI-DIRECTION SCAN"));
	//	g_pAdam->m_nScanType = BI_DIRECTION;
	//	break;
	//case BST_UNCHECKED:
	//	m_CheckBidirectionCtrl.SetWindowText(_T("UNI-DIRECTION SCAN"));
	//	g_pAdam->m_nScanType = UNI_DIRECTION;
	//	break;
	//default:
	//	break;
	//}

	GetDlgItem(IDC_BUTTON_MASKMEASURE_MACRO_START)->EnableWindow(FALSE);

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}
void CMaskMapDlg::InitButtonSkin()
{
	m_btnXMinusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2LT));
	m_btnYMinusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2DN));
	m_btnXPlusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2RT));
	m_btnYPlusCtrl.SetIcon(AfxGetApp()->LoadIcon(IDI_ARW2UP));
}

void CMaskMapDlg::InitStagePosGrid()
{
	CRect rect;
	GetDlgItem(IDC_TEXT_STAGEPOSGRID)->GetWindowRect(rect);
	ScreenToClient(&rect);
	m_StagePosGrid.Create(rect, this, IDC_TEXT_STAGEPOSGRID);

	int nGap, col, row;
	col = 0;
	row = 0;
	nGap = 8;

	m_StagePosGrid.SetEditable(false);
	m_StagePosGrid.SetListMode(true);
	m_StagePosGrid.SetSingleRowSelection(false);
	m_StagePosGrid.EnableDragAndDrop(false);
	m_StagePosGrid.SetFixedRowCount(1);
	m_StagePosGrid.SetFixedColumnCount(1);
	m_StagePosGrid.SetColumnCount(4);
	m_StagePosGrid.SetRowCount(MAX_STAGE_POSITION + 1);
	//m_StagePosGrid.SetBkColor(RGB(192, 192, 192));
	m_StagePosGrid.GetDefaultCell(FALSE, FALSE)->SetBackClr(WHITE);

	LOGFONT lf;
	CFont Font;
	m_StagePosGrid.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	//lf.lfHeight=10;
	//strcpy(lf.lfFaceName,"Verdana");
	Font.CreateFontIndirect(&lf);
	m_StagePosGrid.SetFont(&Font);
	Font.DeleteObject();

	CString str;
	str.Format("%s", "No");
	m_StagePosGrid.SetItemText(0, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Stage Position Name");
	m_StagePosGrid.SetItemText(0, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "X(mm)");
	m_StagePosGrid.SetItemText(0, 2, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Y(mm)");
	m_StagePosGrid.SetItemText(0, 3, (LPSTR)(LPCTSTR)str);

	m_StagePosGrid.SetRowHeight(0, 35);
	m_StagePosGrid.SetColumnWidth(0, 10);
	m_StagePosGrid.SetColumnWidth(1, 400);
	m_StagePosGrid.SetColumnWidth(2, 70);
	m_StagePosGrid.SetColumnWidth(3, 70);
	m_StagePosGrid.ExpandColumnsToFit();

	CString strNo, strX, strY;
	for (row = 1; row < MAX_STAGE_POSITION + 1; row++)
	{
		strNo.Format("%d", row - 1);
		m_StagePosGrid.SetItemText(row, 0, strNo);
		m_StagePosGrid.SetItemText(row, 1, g_pConfig->m_stStagePos[row - 1].chStagePositionString);
		strX.Format("%4.6f", g_pConfig->m_stStagePos[row - 1].x);
		m_StagePosGrid.SetItemText(row, 2, strX);
		strY.Format("%4.6f", g_pConfig->m_stStagePos[row - 1].y);
		m_StagePosGrid.SetItemText(row, 3, strY);
	}
	//m_StagePosGrid.SetSelectedRow(1);
}

void CMaskMapDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CMaskMapDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMaskMapDlg::RecipeHeaderDisplay()
{
	CRect rect;
	GetDlgItem(IDC_TEXT_INFORMATION)->GetWindowRect(&rect);
	ScreenToClient(&rect);
	m_reCtrl.Create(ES_READONLY | ES_LEFT | ES_MULTILINE, rect, this, 1);
	CHARFORMAT charformat;
	m_reCtrl.GetDefaultCharFormat(charformat);
	if (charformat.dwEffects & CFE_AUTOCOLOR) charformat.dwEffects -= CFE_AUTOCOLOR;
	charformat.crTextColor = WHITE;
	m_reCtrl.SetDefaultCharFormat(charformat);
	m_reCtrl.SetBackgroundColor(false, DARK_GRAY);
	m_reCtrl.ShowWindow(SW_SHOW);
}

void CMaskMapDlg::OnBnClickedButtonRecipeFileLoad()
{
	CString sLog;

	GetDlgItem(IDC_BUTTON_MASKMEASURE_MACRO_START)->EnableWindow(FALSE);
	if (LoadProcessData() == TRUE)
	{
		sLog.Format(_T("Success to load [ %s ] recipe."), m_MaskMapWnd.m_ProcessData.curFilePath.GetLongPath());
	}
	else
	{
		sLog.Format(_T("Fail to load recipe."));
	}

	g_pLog->Display(0, sLog);
}

int CMaskMapDlg::LoadProcessData()
{
	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (m_MaskMapWnd.m_ProcessData.curFilePath.ExistFile() == FALSE)
	{
		return FALSE;
	}

	CString	buf, loadname;
	buf = m_MaskMapWnd.m_ProcessData.curFilePath.GetExtName();
	buf.MakeLower();
	loadname = m_MaskMapWnd.m_ProcessData.curFilePath.GetLongPath();
	int ret = 0;

	m_MaskMapWnd.m_bDrawMeasurePoint = FALSE;
	m_MaskMapWnd.m_bDrawSelectedIDRect = FALSE;
	m_MaskMapWnd.m_ProcessData.ResetData();
	ret = m_MaskMapWnd.m_ProcessData.LoadHeaderInfo((char*)(LPCTSTR)loadname);
	if (ret == TRUE)
	{
		if (g_pConfig->m_nEquipmentType == EUVPTR && m_MaskMapWnd.m_ProcessData.EquipmentModelName == "E-SOL_EUVPTR")
		{
			ret &= m_MaskMapWnd.m_ProcessData.LoadPTRMeasureData((char*)(LPCTSTR)loadname);
		}
		else
		{
			MsgBoxAuto.DoModal(_T(" 본 설비와 호환되지 않는 Recipe 입니다 ! "), 2);
			return FALSE;
		}

		if (ret == TRUE)
		{
			m_MaskMapWnd.m_bShowcode = FALSE;
			m_bCheckCommentDisplayCtrl.SetCheck(TRUE);
			m_MaskMapWnd.m_bDrawMeasurePoint = TRUE;
			m_MaskMapWnd.m_bDrawSelectedIDRect = TRUE;
			m_MaskMapWnd.Invalidate(FALSE);
		}
		else
			return FALSE;
	}

	return TRUE;
}

void CMaskMapDlg::FileSearchInFolder(CString strSearchFile, CString strBaseFolder)
{
	if (strSearchFile.IsEmpty())
	{
		strSearchFile = _T("*.*");
	}

	if (bFinding)
	{
		m_filefinder.StopSearch();
		return;
	}

	m_ListCtrlRecipeFile.DeleteAllItems();

	SYSTEMTIME buftime;
	m_DateTimeStartCtrl.GetTime(&buftime);
	buftime.wHour = 0;
	buftime.wMinute = 0;
	buftime.wSecond = 0;
	m_DateTimeStart = buftime;

	m_DateTimeEndCtrl.GetTime(&buftime);
	buftime.wHour = 23;
	buftime.wMinute = 59;
	buftime.wSecond = 59;
	m_DateTimeEnd = buftime;

	BOOL ret = SetCurrentDirectory(strBaseFolder);

	CFileFinder::CFindOpts	opts;

	// Set CFindOpts object
	opts.strBaseFolder = strBaseFolder;
	opts.sFileMask.Format("*%s*", strSearchFile);
	opts.bSubfolders = 0;
	opts.FindNormalFiles();
	opts.tMinModified = m_DateTimeStart;
	opts.tMaxModified = m_DateTimeEnd;
	opts.dwOptionsFlags |= FIND_DATEMODIFIED;

	if (ret > 0)
	{
		BeginWaitCursor();
		bFinding = true;
		m_filefinder.RemoveAll();
		m_filefinder.Find(opts);
		bFinding = false;
		SetStatus(m_filefinder.GetFileCount());
		EndWaitCursor();
	}
}

void CMaskMapDlg::FileSearching(CFileFinder *pfilefinder, DWORD dwCode, void *pCustomParam)
{
	CString			sText, sNewFile;
	CMaskMapDlg	*pDlg = (CMaskMapDlg *)pCustomParam;
	int				nListIndex;

	switch (dwCode)
	{
	case FF_FOUND:
		// Update list		
		sNewFile = pfilefinder->GetFilePath(pfilefinder->GetFileCount() - 1);
		nListIndex = pDlg->FindInList(sNewFile);
		if (nListIndex == -1) pDlg->AddFileToList(sNewFile);
		break;
	case FF_FOLDER:
		pDlg->SetStatus(pfilefinder->GetFileCount(), pfilefinder->GetSearchingFolder());
		break;
	default:
		break;
	}
}

int	CMaskMapDlg::FindInList(LPCTSTR szFilename)
{
	int		nIndex = 0;
	bool	bFound = false;

	RestoreWaitCursor();

	int count = m_ListCtrlRecipeFile.GetItemCount();
	for (nIndex = 0; nIndex < m_ListCtrlRecipeFile.GetItemCount(); nIndex++)
	{
		bFound = (GetListFilename(nIndex).CompareNoCase(szFilename) == 0);
		if (bFound) break;
	}

	return (bFound ? nIndex : -1);
}

void CMaskMapDlg::AddFileToList(LPCTSTR szFilename)
{
	int		nIndex;
	CPath	path(szFilename);
	__int64	nSize64;
	long	nSize;
	CString	sText;
	CTime	tModified;

	// File name
	if (path.IsFilePath() == TRUE)
	{
		nIndex = m_ListCtrlRecipeFile.InsertItem(m_ListCtrlRecipeFile.GetItemCount(), path.GetFileName(), 0);
	}
	else
	{
		nIndex = m_ListCtrlRecipeFile.InsertItem(m_ListCtrlRecipeFile.GetItemCount(), path.GetRelativePath(_T("C:\\EUVPTR\\Recipe")), 0);
	}

	// File modified date
	path.GetFileTime(tModified);
	//m_ListCtrlRecipeFile.SetItemText(nIndex, 1, tModified.FormatGmt("%y:%m:%d-%I:%M %p"));

	// File size
	path.GetFileSize(nSize64);
	nSize = (long)(nSize64 / (__int64)1024);
	if (nSize < 10)
		sText.Format("%ld Byte", nSize64);
	else
		sText.Format("%ld KB", nSize);

	m_ListCtrlRecipeFile.SetItemText(nIndex, 1, sText);

}

void CMaskMapDlg::SetStatus(int nCount, LPCTSTR szFolder)
{
	CString sStatus;

	if (szFolder != NULL)
		sStatus.Format("(%d) - %s", nCount, szFolder);
	else
		sStatus.Format("%d ea Recipe Files", nCount);

	//m_TxtSearchResultCtrl.SetWindowText(sStatus);
}

CString	CMaskMapDlg::GetListFilename(int nIndex)
{
	return m_ListCtrlRecipeFile.GetItemText(nIndex, 1) + m_ListCtrlRecipeFile.GetItemText(nIndex, 0);
}

CPath CMaskMapDlg::GetCurSelListCtrl()
{
	int nSelected = -1;
	// Get the selected items in the control
	POSITION p = m_ListCtrlRecipeFile.GetFirstSelectedItemPosition();
	while (p)
	{
		nSelected = m_ListCtrlRecipeFile.GetNextSelectedItem(p);
		// Do something with item nSelected
	}
	TCHAR szBuffer[1024];
	DWORD cchBuf(1024);
	LVITEM lvi;
	if (nSelected >= 0)
	{
		lvi.iItem = nSelected;
		lvi.iSubItem = 0;
		lvi.mask = LVIF_TEXT;
		lvi.pszText = szBuffer;
		lvi.cchTextMax = cchBuf;
		m_ListCtrlRecipeFile.GetItem(&lvi);
		return lvi.pszText;
	}
	return "";
}

void CMaskMapDlg::OnBnClickedButtonMapSave()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMaskMapDlg::OnBnClickedCheckCodeShow()
{
	UpdateData(TRUE);
	m_MaskMapWnd.m_bShowcode = m_bShowcode;
	m_MaskMapWnd.Invalidate(FALSE);
}

void CMaskMapDlg::OnBnClickedButtonRecipeFileSearch()
{
	FileSearchInFolder("*.rcp", _T("C:\\EUVPTR\\Recipe"));
}

void CMaskMapDlg::OnNMClickListFilelist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	GetDlgItem(IDC_BUTTON_MASKMEASURE_MACRO_START)->EnableWindow(FALSE);

	int col, row;
	row = pNMItemActivate->iItem;
	col = pNMItemActivate->iSubItem;
	if (row == -1 || col != 0)
		return;

	int ret = 0;
	m_MaskMapWnd.m_ProcessData.curFilePath = GetCurSelListCtrl();
	if (m_MaskMapWnd.m_ProcessData.curFilePath.ExistFile() == FALSE)
	{
		FileSearchInFolder("", m_MaskMapWnd.m_ProcessData.curFilePath.GetLongPath());
		return;
	}

	m_MaskMapWnd.m_ProcessData.orgFilePath = m_MaskMapWnd.m_ProcessData.curFilePath;

	CString str = m_MaskMapWnd.m_ProcessData.curFilePath.GetLongPath();

	CString strExt = m_MaskMapWnd.m_ProcessData.curFilePath.GetExtension();
	strExt.MakeUpper();
	m_MaskMapWnd.m_bDrawAlignPoint = FALSE;
	m_MaskMapWnd.m_bDrawMeasurePoint = FALSE;
	m_MaskMapWnd.m_bDrawSelectedIDRect = FALSE;
	m_MaskMapWnd.m_ProcessData.ResetData();

	CTime	FileGenTime;
	m_MaskMapWnd.m_ProcessData.curFilePath.GetFileTime(FileGenTime);
	m_MaskMapWnd.m_ProcessData.RecipeGenDateTime = FileGenTime.FormatGmt("%y:%m:%d-%I:%M %p");

	char fpath[256];
	strcpy(fpath, (char*)(LPCTSTR)(m_MaskMapWnd.m_ProcessData.curFilePath.GetLongPath()));
	ret = OpenFile(fpath);
	if (ret == TRUE)
	{
		m_MaskMapWnd.m_bDrawAlignPoint = TRUE;
		m_MaskMapWnd.Invalidate(FALSE);
	}

	CString sLog;
	sLog.Format(_T("Clicked [ %s ] recipe file."), str);
	g_pLog->Display(0, sLog);

	*pResult = 0;
}

BOOL CMaskMapDlg::OpenFile(char *fpath)
{
	CString recipe_header_contents;
	CString str, temp, commnd;
	int ret = 0;

	BeginWaitCursor();
	ret = m_MaskMapWnd.m_ProcessData.LoadHeaderInfo(fpath);
	EndWaitCursor();

	m_reCtrl.Clear();
	recipe_header_contents.Empty();
	recipe_header_contents = "=> Recipe Generation Date :  " + m_MaskMapWnd.m_ProcessData.RecipeGenDateTime + "\n";
	recipe_header_contents += "=> Equipment :  " + m_MaskMapWnd.m_ProcessData.EquipmentModelName + "\n";
	recipe_header_contents += "=> Substrate :  " + m_MaskMapWnd.m_ProcessData.Substrate + "\n";
	recipe_header_contents += "=> Substrate ID :  " + m_MaskMapWnd.m_ProcessData.SubstrateID + "\n";
	str.Format("%.0f %.0f", m_MaskMapWnd.m_ProcessData.SubstrateWidth, m_MaskMapWnd.m_ProcessData.SubstrateHeight);
	recipe_header_contents += "=> Substrate Size(um) :  " + str + "\n";
	recipe_header_contents += "=> Lot :  " + m_MaskMapWnd.m_ProcessData.Lot + "\n";
	recipe_header_contents += "=> Step :  " + m_MaskMapWnd.m_ProcessData.Step + "\n";
	recipe_header_contents += "=> Device :  " + m_MaskMapWnd.m_ProcessData.Device + "\n";
	str.Format("%d", m_MaskMapWnd.m_ProcessData.TotalAlignNum);
	recipe_header_contents += "=> Alignment Points No :  " + str + " ea \n";
	str.Format("%.3f um  %.3f um", m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X, m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y);
	recipe_header_contents += "       1  :  " + str + "\n";
	str.Format("%.3f um  %.3f um", m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X, m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y);
	recipe_header_contents += "       2  :  " + str + "\n";
	str.Format("%.3f um  %.3f um", m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X, m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y);
	recipe_header_contents += "       3  :  " + str + "\n";

	if (m_MaskMapWnd.m_ProcessData.TotalAlignNum == 4)
	{
		str.Format("%.3f um  %.3f um", m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_BOTTOM].X, m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_BOTTOM].Y);
		recipe_header_contents += "       4  :  " + str + "\n";
	}

	str.Format("%.3f   %.3f", m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.X, m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.Y);
	recipe_header_contents += "=> Beam Diagnosis Pos(um) :  " + str + "\n";
	str.Format("%d", m_MaskMapWnd.m_ProcessData.m_nBeamDiagPosExposureTime_ms);
	recipe_header_contents += "=> Beam Diagnosis Exposure Time(msec) :  " + str + "\n";
	
	str.Format("%d", m_MaskMapWnd.m_ProcessData.TotalCouponNum);
	temp += "=> Total Coupon No :  " + str + " ea \n";
	str.Format("%d", m_MaskMapWnd.m_ProcessData.MeasurePointNum);
	temp += "=> Measurement Point No :  " + str + " ea \n";
	str.Format("%d", m_MaskMapWnd.m_ProcessData.MeasureScanNum);
	temp += "=> Measurement Scan No :  " + str + " ea \n";
	recipe_header_contents += temp;
	
	m_reCtrl.SetWindowText(recipe_header_contents);
	m_reCtrl.ShowWindow(SW_SHOW);

	return TRUE;
}

BOOL CMaskMapDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		case VK_LBUTTON:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CMaskMapDlg::OnBnClickedCheckOmadam()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedCheckOmadam() Check Button Click!"));
	UpdateData(TRUE);

	g_pChart->ShowWindow(SW_HIDE);

	switch (m_CheckOMAdamCtrl.GetCheck())
	{
	case BST_CHECKED:
		ChangeOMEUVSet(OMTOEUV);
		break;
	case BST_UNCHECKED:
		ChangeOMEUVSet(EUVTOOM);
		break;
	default:
		break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////// Stage Moving /////////////////////////////////////////////////////////////////////////////////////////////////
void CMaskMapDlg::OnEnChangeEditPointno()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMaskMapDlg::OnBnClickedButtonMesurepointmove()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMesurepointmove() Check Button Click!"));
	UpdateData(TRUE);
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (m_nMeasurementPointNo < 1)
	{
		MsgBoxAuto.DoModal(_T(" 측정 불가능한 번호입니다. 1번부터 진행 가능 ! "), 2);
		return;
	}

	if (m_bAutoSequenceProcessing == FALSE)
		g_pNavigationStage->MoveToSelectedPointNo(m_nMeasurementPointNo - 1);

}

void CMaskMapDlg::OnEnChangeEditMaskcodx()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMaskMapDlg::OnEnChangeEditMaskcody()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CMaskMapDlg::OnBnClickedButtonMaskcoordmove()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMaskcoordmove() Check Button Click!"));
	UpdateData(TRUE);
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (m_bAutoSequenceProcessing == FALSE)
		g_pNavigationStage->MoveToMaskCenterCoordinate(m_dMaskCoordinateX_mm * 1000. + g_pConfig->m_dInspectorOffsetX_um, m_dMaskCoordinateY_mm * 1000. + g_pConfig->m_dInspectorOffsetY_um);
}


void CMaskMapDlg::OnBnClickedButtonGetPosition()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonGetPosition() Button Click!"));
	UpdateData(true);

	CString strX, strY;
	int row = 0;
	row = m_StagePosGrid.GetSelectedRow();

	if (row > MAX_STAGE_POSITION - 20)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "srem")
		{
			::AfxMessageBox("Password가 일치 하지 않습니다.");
			return;
		}
	}

	sprintf(g_pConfig->m_stStagePos[row - 1].chStagePositionString, "%s", m_strStagePosName);
	g_pConfig->m_stStagePos[row - 1].x = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	g_pConfig->m_stStagePos[row - 1].y = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

	m_StagePosGrid.SetItemText(row, 1, m_strStagePosName);
	strX.Format(_T("%4.6f"), g_pConfig->m_stStagePos[row - 1].x);
	m_StagePosGrid.SetItemText(row, 2, strX);
	strY.Format(_T("%4.6f"), g_pConfig->m_stStagePos[row - 1].y);
	m_StagePosGrid.SetItemText(row, 3, strY);

	m_StagePosGrid.Invalidate(FALSE);

	g_pConfig->SaveStagePositionToDB();
}


void CMaskMapDlg::OnEnChangeEditIncrementStepsize()
{
	UpdateData(TRUE);
	m_dStageMovingDistance = atof(m_strStageMovingDistance);
}


void CMaskMapDlg::OnBnClickedButtonXMinus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonXMinus() Button Click!"));

	if (g_pNavigationStage->Simulator_Mode)
	{
		if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, -m_dStageMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
		}
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (!m_bReverseCoordinate)
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, -m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}
		else
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}
	}
}


void CMaskMapDlg::OnBnClickedButtonXPlus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonXPlus() Button Click!"));


	if (g_pNavigationStage->Simulator_Mode)
	{
		if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, m_dStageMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
		}
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (!m_bReverseCoordinate)
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}
		else
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, -m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}

	}
}


void CMaskMapDlg::OnBnClickedButtonYPlus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonYPlus() Button Click!"));

	if (g_pNavigationStage->Simulator_Mode)
	{
		if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, m_dStageMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
		}
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (!m_bReverseCoordinate)
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}
		else
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, -m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}

	}
}


void CMaskMapDlg::OnBnClickedButtonYMinus()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonYMinus() Button Click!"));

	if (g_pNavigationStage->Simulator_Mode)
	{
		if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, -m_dStageMovingDistance) != XY_NAVISTAGE_OK)
		{
			::AfxMessageBox(" Stage 동작 불가!");
		}
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (!m_bReverseCoordinate)
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, -m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}
		else
		{
			if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, m_dStageMovingDistance) != XY_NAVISTAGE_OK)
			{
				::AfxMessageBox(" Stage 동작 불가!");
			}
		}


	}
}

void CMaskMapDlg::OnEnChangeEditZThroughfocusHeight()
{
	UpdateData(TRUE);
}


void CMaskMapDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	default:
		break;
	}
	CDialogEx::OnTimer(nIDEvent);
}

void CMaskMapDlg::OnStagePosGridClick(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;

	m_strStagePosName = g_pConfig->m_stStagePos[row - 1].chStagePositionString;
	m_strXMovePos.Format(_T("%3.7f"), g_pConfig->m_stStagePos[row - 1].x);
	m_strYMovePos.Format(_T("%3.7f"), g_pConfig->m_stStagePos[row - 1].y);
	//m_StagePosGrid.SetEditable(false);

	UpdateData(false);

}

void CMaskMapDlg::OnStagePosGridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;

	if (g_pLog == NULL || g_pConfig == NULL || g_pNavigationStage == NULL)
		return;

	g_pLog->Display(0, _T("CMaskMapDlg::OnStagePosGridDblClick !"));

	if (g_pNavigationStage->Simulator_Mode)
	{
		g_pNavigationStage->MoveStageDBPositionNo(row - 1);
	}
	else
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return;

		if (g_pConfig->m_nEquipmentType == PHASE)
			g_pNavigationStage->MoveStageDBPositionNo(row - 1, FALSE);
		else
			g_pNavigationStage->MoveStageDBPositionNo(row - 1);
	}
}

void CMaskMapDlg::OnStagePosGridRClick(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;
	//if (col == 1)
	//{
	//	if (row > MAX_STAGE_POSITION - 20)
	//	{
	//		CPasswordDlg pwdlg(this);
	//		pwdlg.DoModal();
	//		if (pwdlg.m_strTxt != "srem")
	//		{
	//			::AfxMessageBox("Password가 일치 하지 않습니다.");
	//			return;
	//		}
	//	}

	//	m_StagePosGrid.SetEditable(true);
	//}
}

void CMaskMapDlg::OnStagePosGridStartEdit(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;
}

void CMaskMapDlg::OnStagePosGridEndEdit(NMHDR *pNotifyStruct, LRESULT* pResult)
{
	NM_GRIDVIEW* pItem = (NM_GRIDVIEW*)pNotifyStruct;
	int col, row;
	row = pItem->iRow;
	col = pItem->iColumn;

	if (row < 0 || col < 0)
		return;

	//row = m_StagePosGrid.GetSelectedRow();
	sprintf(g_pConfig->m_stStagePos[row - 1].chStagePositionString, "%s", m_StagePosGrid.GetItemText(row, col));
	m_StagePosGrid.SetEditable(false);
}

void CMaskMapDlg::OnDeltaposIncrementStepspin(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	UpdateData(true);

	if (pNMUpDown->iDelta < 0)
	{
		if (m_dStageMovingDistance >= 0.000001 && m_dStageMovingDistance < 1) m_dStageMovingDistance *= 10;
		else if (m_dStageMovingDistance >= 1 && m_dStageMovingDistance < 10) m_dStageMovingDistance += 1;
		else if (m_dStageMovingDistance >= 10 && m_dStageMovingDistance < SOFT_LIMIT_PLUS_X)	m_dStageMovingDistance += 10;

		if (m_dStageMovingDistance > SOFT_LIMIT_PLUS_X)
			m_dStageMovingDistance = SOFT_LIMIT_PLUS_X;
	}
	else
	{
		if (m_dStageMovingDistance > 0.000001 && m_dStageMovingDistance <= 1) m_dStageMovingDistance /= 10;
		else if (m_dStageMovingDistance > 1 && m_dStageMovingDistance <= 10) m_dStageMovingDistance -= 1;
		else if (m_dStageMovingDistance > 10 && m_dStageMovingDistance <= SOFT_LIMIT_PLUS_X) m_dStageMovingDistance -= 10;

		if (m_dStageMovingDistance < 0.000001)
			m_dStageMovingDistance = 0.000001;
	}
	m_strStageMovingDistance.Format("%0.6f", m_dStageMovingDistance);

	UpdateData(false);

	*pResult = 0;
}

void CMaskMapDlg::OnBnClickedButtonMaskmeasureAutostart()
{
	CString strLog;

	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMaskmeasureAutostart() Button Click!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}

	if (IDYES != AfxMessageBox("자동 Sequence를 진행하시겠습니까?", MB_YESNO)) return;

	strLog.Format(_T("AutoRun LongRun Test Start Button Click"));
	SaveLogFile("AutoRun_LongRunTestLog", strLog);
	AutoRun(m_nAutoRunRepeatSetNo);
}

void CMaskMapDlg::OnBnClickedButtonMaskload()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMaskload() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 3);
		return;
	}

	if (IDYES != AfxMessageBox("Mask를 Loading 하시겠습니까?", MB_YESNO)) return;

	MaskLoad();
}

void CMaskMapDlg::OnBnClickedButtonMaskalign()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMaskalign() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}

	if (IDYES != AfxMessageBox("Mask Align을 진행 하시겠습니까?", MB_YESNO)) return;

	int ret = 0;
	ret = MaskAlign_OM(m_bManualAlign);
	if (ret == 0)//EUV 영역은 OM-EUV Offset을 이용해서 Align 완료
	{
		m_dEUVAlignPointRT_posx_mm = m_dOMAlignPointRT_posx_mm - g_pConfig->m_dGlobalOffsetX_mm;
		m_dEUVAlignPointRT_posy_mm = m_dOMAlignPointRT_posy_mm + g_pConfig->m_dGlobalOffsetY_mm;
		g_pConfig->m_dEUVAlignPointRT_X_mm = m_dEUVAlignPointRT_posx_mm;
		g_pConfig->m_dEUVAlignPointRT_Y_mm = m_dEUVAlignPointRT_posy_mm;
		m_dEUVAlignPointLT_posx_mm = m_dOMAlignPointLT_posx_mm - g_pConfig->m_dGlobalOffsetX_mm;
		m_dEUVAlignPointLT_posy_mm = m_dOMAlignPointLT_posy_mm + g_pConfig->m_dGlobalOffsetY_mm;
		g_pConfig->m_dEUVAlignPointLT_X_mm = m_dEUVAlignPointLT_posx_mm;
		g_pConfig->m_dEUVAlignPointLT_Y_mm = m_dEUVAlignPointLT_posy_mm;
		m_dEUVAlignPointLB_posx_mm = m_dOMAlignPointLB_posx_mm - g_pConfig->m_dGlobalOffsetX_mm;
		m_dEUVAlignPointLB_posy_mm = m_dOMAlignPointLB_posy_mm + g_pConfig->m_dGlobalOffsetY_mm;
		g_pConfig->m_dEUVAlignPointLB_X_mm = m_dEUVAlignPointLB_posx_mm;
		g_pConfig->m_dEUVAlignPointLB_Y_mm = m_dEUVAlignPointLB_posy_mm;

		m_bEUVAlignComplete = TRUE;
		g_pConfig->m_bEUVAlignCompleteFlag = m_bEUVAlignComplete;
		g_pConfig->SaveRecoveryData();
		m_bAutoSequenceProcessing = FALSE;
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Mask Align이 완료되었습니다! "), 2);
		m_strStageStatus.Format(_T("Mask Align(EUV) 완료!"));
		m_MaskMapWnd.Invalidate(FALSE);
		UpdateData(FALSE);
		g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_EUV_SREM() End!"));
	}

	CString str;
	str.Format(_T("MaskAlign return: %d"), ret);
	g_pLog->Display(0, str);
}

void CMaskMapDlg::OnBnClickedButtonMaskmeasureStart()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMaskmeasureStart() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}

	int ret = 0;

	switch (g_pConfig->m_nEquipmentType)
	{
	
	case EUVPTR:
		if (IDYES != AfxMessageBox("EUV PTR 측정 Process를 진행 하시겠습니까?", MB_YESNO)) return;
		ret = PtrProcess();
		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("MaskImagingProcess return: %d"), ret);
	g_pLog->Display(0, str);
}

void CMaskMapDlg::OnBnClickedButtonMaskunload()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMaskunload() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 3);
		return;
	}

	if (IDYES != AfxMessageBox("Mask를 Unloading 하시겠습니까?", MB_YESNO)) return;

	MaskUnload();
}


void CMaskMapDlg::OnBnClickedButtonStopAutosequence()
{
	m_bMacroSystemRunFlag = FALSE;
	//vector<double> a = { 3, 2,0 };
	//vector<double> b = { 1, 4,0 };
	//vector<double> c = { 5, 4,0 };
	//CenterOfCircumCircle(a, b, c);

	g_pAP->StopSequence();
	m_bAutoSequenceProcessing = FALSE;

	//g_pPhase->m_IsStop = TRUE;

	g_pNavigationStage->Stop_Scan();
	//g_pAdam->ADAMAbort();
	//g_pAdam->KillTimer(ADAMDATA_DISPLAY_TIMER);
}

void CMaskMapDlg::OnEnChangeEditScanRepeatNumber()
{
	UpdateData(TRUE);
}


void CMaskMapDlg::OnBnClickedButtonMovePosition()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonMovePosition() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;
}







void CMaskMapDlg::OnBnClickedButtonXyMove()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedButtonXyMove() Button Click!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	UpdateData(true);

	if (g_pNavigationStage != NULL)
	{
		if (g_pConfig->m_nEquipmentType == PHASE)
		{
			g_pNavigationStage->MoveAbsolutePosition(atof(m_strXMovePos), atof(m_strYMovePos), FALSE);
		}
		else
		{
			g_pNavigationStage->MoveAbsolutePosition(atof(m_strXMovePos), atof(m_strYMovePos));
		}
	}
}

void CMaskMapDlg::OnEnChangeEditXMovepos()
{
	UpdateData(true);
}


void CMaskMapDlg::OnEnChangeEditYMovepos()
{
	UpdateData(true);
}


void CMaskMapDlg::OnBnClickedButtonZoomreset()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMaskMapDlg::ChangeOMEUVSet(int direction)
{
	if (g_pNavigationStage == NULL || g_pAdam == NULL || g_pCamera == NULL || g_pRecipe == NULL || g_pConfig == NULL || g_pAP == NULL)
		return;

	//Global Offset만을 적용시켜서 이동시키기 위해//
	double target_posx_mm = 0.0, target_posy_mm = 0.0;
	double current_posx_mm = 0.0, current_posy_mm = 0.0;
	current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	CPoint pt;
	pt.x = current_posx_mm;
	pt.y = current_posy_mm;

	switch (direction)
	{
	case OMTOEUV:
		g_pCamera->ShowWindow(SW_HIDE);
		g_pRecipe->ShowWindow(SW_HIDE);
		//20201221 jkseo, Om->Euv 시 조명 Off
		g_pLightCtrl->LightOff();

		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			//g_pAdam->ShowWindow(SW_SHOW);
			break;
		case EUVPTR:
			g_pPTR->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}

		m_MaskMapWnd.m_nMicroscopyType = EUV;
		m_AdamOmtiltSwitch = 1;
		OnBnClickedCheckOmAdamUi();

		m_CheckOMAdamCtrl.SetWindowText(_T("To Optic Microscopy"));
		target_posx_mm = current_posx_mm - g_pConfig->m_dGlobalOffsetX_mm;
		target_posy_mm = current_posy_mm + g_pConfig->m_dGlobalOffsetY_mm;

		if (g_pConfig->m_rcOMStageAreaRect.PtInRect(pt) == TRUE && g_pAP->Check_STAGE_MovePossible() == XY_NAVISTAGE_OK)
		{
			if (g_pWarning != NULL)
			{
				g_pWarning->m_strWarningMessageVal = "Stage 이동 중입니다. 잠시 기다려 주세요!";
				g_pWarning->UpdateData(FALSE);
				g_pWarning->ShowWindow(SW_SHOW);
			}
			m_bAutoSequenceProcessing = TRUE;
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//EUV 영역으로 이동
			//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
			//	g_pNavigationStage->SetLaserMode();
			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
			WaitSec(1);
		
			m_bAutoSequenceProcessing = FALSE;
			if (g_pWarning != NULL)
				g_pWarning->ShowWindow(SW_HIDE);
		}
		break;
	case EUVTOOM:
		g_pCamera->ShowWindow(SW_SHOW);
		//20201221 jkseo, Euv->Om 시 조명 On
		g_pLightCtrl->LightOn();

		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			//g_pAdam->ShowWindow(SW_HIDE);
			break;
		case EUVPTR:
			g_pPTR->ShowWindow(SW_HIDE);
			break;
		default:
			break;
		}


		m_MaskMapWnd.m_nMicroscopyType = SCOPE_OM4X;
		g_pRecipe->ShowWindow(SW_SHOW);
		g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("OM"));
		m_AdamOmtiltSwitch = 0;
		OnBnClickedCheckOmAdamUi();

		m_CheckOMAdamCtrl.SetWindowText(_T("To EUV Microscopy"));
		target_posx_mm = current_posx_mm + g_pConfig->m_dGlobalOffsetX_mm;
		target_posy_mm = current_posy_mm - g_pConfig->m_dGlobalOffsetY_mm;

		if (g_pConfig->m_rcEUVStageAreaRect.PtInRect(pt) == TRUE && g_pAP->Check_STAGE_MovePossible() == XY_NAVISTAGE_OK)
		{
			if (g_pWarning != NULL)
			{
				g_pWarning->m_strWarningMessageVal = "Stage 이동 중입니다. 잠시 기다려 주세요!";
				g_pWarning->UpdateData(FALSE);
				g_pWarning->ShowWindow(SW_SHOW);
			}
			m_bAutoSequenceProcessing = TRUE;

			//g_pNavigationStage->SetEncoderMode();
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//OM 영역으로 이동
			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);
			m_bAutoSequenceProcessing = FALSE;
			g_pWarning->ShowWindow(SW_HIDE);
		}
		break;
	default:
		break;
	}
	m_MaskMapWnd.Invalidate(FALSE);
}

int CMaskMapDlg::AutoRun(int nRepeatNo)
{
	CString strLog;

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL)
		return -1;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.TotalMeasureNum == 0)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -1;
	}

	m_strStageStatus.Format(_T("Auto Sequence 가동 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " Auto Sequence 가동 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	m_nAutoRunRepeatResultNo = 0;
	m_bAutoSequenceProcessing = TRUE;

	while (m_bAutoSequenceProcessing == TRUE && m_nAutoRunRepeatResultNo < nRepeatNo)
	{
		strLog.Format(("AutoRun LongRun Test %d Start"), m_nAutoRunRepeatResultNo);
		SaveLogFile("AutoRun_LongRunTestLog", strLog);

		WaitSec(3);	//혹시 가동 취소 버튼 누를 시간 확보
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			strLog.Format(_T("AutoRun LongRun Test Stop Button Click "));
			SaveLogFile("AutoRun_LongRunTestLog", strLog);
			return -1;
		}

		//if (g_pConfig->m_nMaterialLocation == MTS_POD)
		//{
			m_strStageStatus.Format(_T("Mask Loading 시작!"));
			SaveLogFile("AutoRun_LongRunTestLog", m_strStageStatus);
			UpdateData(FALSE);
			g_pWarning->m_strWarningMessageVal = " Mask Loading 중입니다! ";
			g_pWarning->UpdateData(FALSE);
			if (g_pAP->MaskLoadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				strLog.Format(_T("AutoRun LongRun Test 중 Mask Loading Error 발생으로 Sequence 중지 "));
				SaveLogFile("AutoRun_LongRunTestLog", strLog);
				return -1;
			}
			while (TRUE)
			{
				//ProcessMessages();
				//Sleep(10);
				WaitSec(1);
				if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->LOADING_COMPLETE)
					break;

				if (m_bAutoSequenceProcessing == FALSE)
				{
					g_pWarning->ShowWindow(SW_HIDE);
					MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
					return -1;
				}

				if (g_pAP->m_nProcessErrorCode != 0)
				{
					g_pWarning->ShowWindow(SW_HIDE);
					MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
					m_bAutoSequenceProcessing = FALSE;
					return -1;
				}
			}
			m_strStageStatus.Format(_T("Mask Loading 완료!"));
			SaveLogFile("AutoRun_LongRunTestLog", m_strStageStatus);
			UpdateData(FALSE);
			WaitSec(3);
			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				return -1;
			}
		//}

		int ret = 0;
		if (g_pConfig->m_nMaterialLocation == CHUCK)
		{
			g_pWarning->m_strWarningMessageVal = " Mask Align 중입니다! ";
			strLog.Format(_T("Mask Align 중입니다!!"));
			SaveLogFile("AutoRun_LongRunTestLog", strLog);
			g_pWarning->UpdateData(FALSE);
			//ret = MaskZInterlock();

			ret = MaskAlign_OM(m_bManualAlign);
			

			g_pWarning->ShowWindow(SW_SHOW);
			m_bAutoSequenceProcessing = TRUE;	//MaskAlign() 내부에서 끝날때 FALSE를 만들기때문에 여기서 다시 TRUE로 변경
			WaitSec(3);
			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				return -1;
			}

			g_pWarning->m_strWarningMessageVal = " Image 자동 측정 중입니다! ";
			g_pWarning->UpdateData(FALSE);
			strLog.Format(_T("Image 자동 측정 중입니다!!!"));
			SaveLogFile("AutoRun_LongRunTestLog", strLog);
			//////////////////////////////////////////////////////////////////
			// 2021.06.16
			// Euv 없이 Scan 진행 Test (jhkim)
			//
			strLog.Format(_T("Euv 없이 Test 위한 Scan 진행!!"));
			SaveLogFile("AutoRun_LongRunTestLog", strLog);
			//MaskImagingProcess();
			//////////////////////////////////////////////////////////////////
			g_pWarning->ShowWindow(SW_SHOW);
			m_bAutoSequenceProcessing = TRUE;	//MaskImagingProcess() 내부에서 끝날때 FALSE를 만들기때문에 여기서 다시 TRUE로 변경
			WaitSec(3);
			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				return -1;
			}

			g_pEUVSource->SetMechShutterOpen(FALSE);
			g_pEUVSource->SetEUVSourceOn(FALSE);
			WaitSec(3);



			g_pWarning->m_strWarningMessageVal = " Mask Unloading 중입니다! ";
			g_pWarning->UpdateData(FALSE);

			strLog.Format(_T("Mask Unloading 중입니다!!"));
			SaveLogFile("AutoRun_LongRunTestLog", strLog);

			if (g_pAP->MaskUnloadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				return -1;
			}
			while (TRUE)
			{
				//ProcessMessages();
				//Sleep(10);
				WaitSec(1);
				if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->UNLOADING_COMPLETE)
					break;

				if (m_bAutoSequenceProcessing == FALSE)
				{
					g_pWarning->ShowWindow(SW_HIDE);
					MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
					return -1;
				}

				if (g_pAP->m_nProcessErrorCode != 0)
				{
					g_pWarning->ShowWindow(SW_HIDE);
					MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
					m_bAutoSequenceProcessing = FALSE;
					return -1;
				}
			}
			WaitSec(3);

			strLog.Format(_T("Mask Unloading 완료 되었습니다!!"));
			SaveLogFile("AutoRun_LongRunTestLog", strLog);
		}



		m_nAutoRunRepeatResultNo++;
		UpdateData(FALSE);
	}

	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	m_strStageStatus.Format(_T("Auto Sequence 완료!"));
	strLog.Format(_T("Auto Sequence 완료!!"));
	SaveLogFile("AutoRun_LongRunTestLog", strLog);
	UpdateData(FALSE);
}

int CMaskMapDlg::MaskLoad()
{
	if (g_pAP == NULL || g_pLog == NULL || g_pWarning == NULL)
		return -1;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);

	g_pWarning->m_strWarningMessageVal = " Auto Sequence 가동 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	m_bAutoSequenceProcessing = TRUE;

	WaitSec(3);	//혹시 가동 취소 버튼 누를 시간 확보
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		AfxMessageBox(_T(" Auto Sequence가 중지되었습니다!"), MB_ICONSTOP);
		return -1;
	}

	g_pWarning->m_strWarningMessageVal = " Mask Loading 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	if (g_pAP->MaskLoadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		AfxMessageBox(_T(" Auto Sequence가 중지되었습니다!"), MB_ICONSTOP);
		m_bAutoSequenceProcessing = FALSE;
		return -1;
	}

	while (TRUE)
	{
		//ProcessMessages();
		//Sleep(10);
		WaitSec(1);
		if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->LOADING_COMPLETE)
			break;

		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			AfxMessageBox(_T(" Auto Sequence가 중지되었습니다!"), MB_ICONSTOP);
			return -1;
		}

		if (g_pAP->m_nProcessErrorCode != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			g_pAlarm->SetAlarm(g_pAP->m_nProcessErrorCode);
			m_bAutoSequenceProcessing = FALSE;
			return -1;
		}
	}
	WaitSec(3);

	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Mask Loading이 완료되었습니다! "), 3);
	return 0;
}

int CMaskMapDlg::MaskUnload()
{
	if (g_pAP == NULL || g_pLog == NULL || g_pWarning == NULL)
		return -1;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);

	g_pWarning->m_strWarningMessageVal = " Auto Sequence 가동 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	m_bAutoSequenceProcessing = TRUE;

	WaitSec(3);
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		AfxMessageBox(_T(" Auto Sequence가 중지되었습니다!"), MB_ICONSTOP);
		return -1;
	}

	g_pWarning->m_strWarningMessageVal = " Mask Unloading 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	if (g_pAP->MaskUnloadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		AfxMessageBox(_T(" Auto Sequence가 중지되었습니다!"), MB_ICONSTOP);
		m_bAutoSequenceProcessing = FALSE;
		return -1;
	}
	while (TRUE)
	{
		//ProcessMessages();
		//Sleep(10);
		WaitSec(1);
		if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->UNLOADING_COMPLETE)
			break;

		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			AfxMessageBox(_T(" Auto Sequence가 중지되었습니다!"), MB_ICONSTOP);
			return -1;
		}

		if (g_pAP->m_nProcessErrorCode != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			g_pAlarm->SetAlarm(g_pAP->m_nProcessErrorCode);
			m_bAutoSequenceProcessing = FALSE;
			return -1;
		}
	}
	WaitSec(3);

	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Mask Unloading이 완료되었습니다! "), 3);
	return 0;
}

int CMaskMapDlg::PtrProcess()
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pNavigationStage == NULL || g_pRecipe == NULL || g_pWarning == NULL || g_pPTR == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.TotalMeasureNum == 0)
	{
		MsgBoxAuto.DoModal(_T(" Pellicle 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::PtrProcess() Start!"));

	ChangeOMEUVSet(OMTOEUV);	//EUV 영역이 아니면 EUV영역으로 이동

	m_strStageStatus.Format(_T("PTR Auto Process 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " PTR 자동 측정 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;


	// EUV 연결 체크
	if (g_pEUVSource == NULL) return -4;

	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return -5;
	}

	if (g_pIO->Is_SourceGate_OpenOn_Check() != TRUE)
	{
		AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
		return -6;
	}

	//////////////////////////////// 측정 Process Start ////////////////////////////////////////

	////UI reset
	g_pPTR->ClearGrid();
	g_pPTR->ResetPtrDisplay(&m_MaskMapWnd.m_ProcessData);
	//1. EUV On
	////1) EUV shutter close
	g_pEUVSource->SetMechShutterOpen(FALSE);
	WaitSec(1);
	////2) EUV on:  
	if (g_pEUVSource->SetEUVSourceOn(TRUE) != 0)
	{
		AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
		return -999;
	}
	WaitSec(10);



	for (int CouponIdx = 0; CouponIdx < m_MaskMapWnd.m_ProcessData.GetCoupons().size(); CouponIdx++)
	{

		for (int PointIdx = 0; PointIdx < m_MaskMapWnd.m_ProcessData.GetCoupons()[CouponIdx].GetPoints().size(); PointIdx++)
		{
			
		}

		//for (int ScanIdx = 0; ScanIdx < m_MaskMapWnd.m_ProcessData.GetCoupons()[CouponIdx].GetScans().size(); ScanIdx++)
		//{
		//
		//}
	}






	//int poitIndex = 0;
	//
	//for (int IndexCoupon = 0; IndexCoupon < m_MaskMapWnd.m_ProcessData.TotalMeasureNum; IndexCoupon++)
	//{
	//	// Coupon 마다 EUV calibration 수행
	//	//2. Background 측정
	//	g_pEUVSource->SetMechShutterOpen(FALSE);
	//	WaitSec(5);
	//	// 버튼 정지
	//	if (m_bAutoSequenceProcessing == FALSE)
	//	{
	//		g_pWarning->ShowWindow(SW_HIDE);
	//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
	//		m_bAutoSequenceProcessing = FALSE;
	//		g_pEUVSource->SetMechShutterOpen(FALSE);
	//		g_pEUVSource->SetEUVSourceOn(FALSE);
	//		return -5;
	//	}
	//	////3) Background 측정위치 이동
	//	g_pNavigationStage->MoveToMaskCenterCoordinate(m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.X, m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.Y, TRUE); //+ 방향
	//	////4) 측정	
	//	g_pPTR->m_nPTRDataNum = m_MaskMapWnd.m_ProcessData.m_nBeamDiagPosExposureTime_ms;
	//	g_pPTR->ReadSensor();
	//
	//	g_pAdam->mD1BackGround = g_pAdam->mD1;
	//	g_pAdam->mD2BackGround = g_pAdam->mD2;
	//	g_pAdam->mD3BackGround = g_pAdam->mD3;
	//
	//	g_pAdam->mD1BackGraoundStd = g_pAdam->mD1Std;
	//	g_pAdam->mD2BackGraoundStd = g_pAdam->mD2Std;
	//	g_pAdam->mD3BackGraoundStd = g_pAdam->mD3Std;
	//
	//	g_pPTR->SensorGridUpdate();
	//	g_pPTR->UpdateData(FALSE);
	//
	//
	//	// 버튼 정지
	//	if (m_bAutoSequenceProcessing == FALSE)
	//	{
	//		g_pWarning->ShowWindow(SW_HIDE);
	//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
	//		m_bAutoSequenceProcessing = FALSE;
	//		g_pEUVSource->SetMechShutterOpen(FALSE);
	//		g_pEUVSource->SetEUVSourceOn(FALSE);
	//		return -5;
	//	}
	//
	//	//3. Through Pellicle 측정
	//	////1) Through Pellicle 측정위치 이동
	//	g_pNavigationStage->MoveToMaskCenterCoordinate(m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.X, m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.Y, TRUE); //+ 방향
	//	////2) EUV shutter open
	//	g_pEUVSource->SetMechShutterOpen(TRUE);
	//	WaitSec(5);
	//	////3) 측정
	//	g_pPTR->m_nPTRDataNum = m_MaskMapWnd.m_ProcessData.m_nBeamDiagPosExposureTime_ms;
	//	g_pPTR->ReadSensor();
	//
	//	g_pAdam->mD1WithoutPellicle = g_pAdam->mD1;
	//	g_pAdam->mD2WithoutPellicle = g_pAdam->mD2;
	//	g_pAdam->mD3WithoutPellicle = g_pAdam->mD3;
	//
	//	g_pAdam->mD1WithoutPellicleStd = g_pAdam->mD1Std;
	//	g_pAdam->mD2WithoutPellicleStd = g_pAdam->mD2Std;
	//	g_pAdam->mD3WithoutPellicleStd = g_pAdam->mD3Std;
	//
	//	g_pPTR->SensorGridUpdate();
	//	g_pPTR->UpdateData(FALSE);
	//
	//
	//	// 결과 화면 선택
	//	g_pPTR->SelectDisp(IndexCoupon);
	//
	//	//int couponNum = IndexCoupon;
	//	int totalPointInCoupon = m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementXNo *m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementYNo;
	//	int currentPointInCoupon = 0;
	//
	//
	//	// 이동시 Shutter close
	//	g_pEUVSource->SetMechShutterOpen(FALSE);
	//	WaitSec(2);
	//	g_pNavigationStage->MoveToMaskCenterCoordinate(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodY_um, TRUE); //+ 방향
	//	g_pEUVSource->SetMechShutterOpen(TRUE);
	//	WaitSec(5);
	//	//
	//
	//	for (int indexY = 0; indexY < m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementYNo; indexY++)
	//	{
	//		for (int indexX = 0; indexX < m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementXNo; indexX++)
	//		{
	//			// 3. Pellicle 측정
	//			////1) 측정위치 이동
	//			g_pNavigationStage->MoveToMaskCenterCoordinate(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodY_um, TRUE); //+ 방향
	//
	//			for (int repeatIndex = 0; repeatIndex < m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nRepeatNo; repeatIndex++)
	//			{
	//				m_strStageStatus.Format(_T("PTR Auto Process: Coupon: %d  Point: %d/%d 측정 시작"), IndexCoupon + 1, currentPointInCoupon + 1, totalPointInCoupon);
	//
	//				UpdateData(FALSE);
	//				////2) EUV shutter open
	//				//g_pEUVSource->SetMechShutterOpen(TRUE);
	//				//WaitSec(1);
	//				////3) 측정			
	//				g_pPTR->m_nPTRDataNum = m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].dExposureTime_msec;
	//
	//				g_pPTR->ReadSensor();
	//
	//				g_pAdam->mD1WithPellicle = g_pAdam->mD1;
	//				g_pAdam->mD2WithPellicle = g_pAdam->mD2;
	//				g_pAdam->mD3WithPellicle = g_pAdam->mD3;
	//
	//				g_pAdam->mD1WithPellicleStd = g_pAdam->mD1Std;
	//				g_pAdam->mD2WithPellicleStd = g_pAdam->mD2Std;
	//				g_pAdam->mD3WithPellicleStd = g_pAdam->mD3Std;
	//
	//				g_pPTR->SensorGridUpdate();
	//				g_pPTR->UpdateData(FALSE);
	//				////4) EUV shutter off
	//				//g_pEUVSource->SetMechShutterOpen(FALSE);
	//				////5) 계산
	//				g_pPTR->CalculatePTR(g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_pstPTRMeasureList[poitIndex].dReferenceCoodY_um);
	//
	//				// 6)Display Data updata
	//				g_pPTR->PushDispData(IndexCoupon, indexX, indexY, g_pAdam->mT);
	//				g_pPTR->UpdateLut();
	//
	//				m_strStageStatus.Format(_T("PTR Auto Process: Coupon: %d  Point: %d/%d 측정 완료"), IndexCoupon + 1, currentPointInCoupon + 1, totalPointInCoupon);
	//				UpdateData(FALSE);
	//			}
	//
	//			poitIndex++;
	//			currentPointInCoupon++;
	//
	//			// 버튼 정지
	//			if (m_bAutoSequenceProcessing == FALSE)
	//			{
	//				g_pWarning->ShowWindow(SW_HIDE);
	//				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
	//				m_bAutoSequenceProcessing = FALSE;
	//				g_pEUVSource->SetMechShutterOpen(FALSE);
	//				g_pEUVSource->SetEUVSourceOn(FALSE);
	//				return -5;
	//			}
	//
	//		}
	//	}
	//}

	UpdateData(FALSE);
	g_pPTR->UpdateData(FALSE);

	//////////////////////////// 측정 종료 ///////////////////////////////////////////////////		
	//EUV off
	g_pEUVSource->SetMechShutterOpen(FALSE);
	g_pEUVSource->SetEUVSourceOn(FALSE);
	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Auto PTR 측정이 완료되었습니다! "), 2);
	m_strStageStatus.Format(_T("PTR Auto Process 완료!"));
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::PtrProcess() End!"));

	return 0;
}

int CMaskMapDlg::PtrScanProcess()
{
	BOOL isSimulation = FALSE;

	if (!isSimulation)
	{
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			return -1;
	}

	if (g_pNavigationStage == NULL || g_pRecipe == NULL || g_pWarning == NULL || g_pPTR == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);

	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.TotalMeasureNum == 0)
	{
		MsgBoxAuto.DoModal(_T(" Pellicle 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::PtrProcess() Start!"));

	ChangeOMEUVSet(OMTOEUV);	//EUV 영역이 아니면 EUV영역으로 이동

	m_strStageStatus.Format(_T("PTR Auto Process 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = " PTR 자동 측정 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;

	// EUV 연결 체크
	if (!isSimulation)
	{
		if (g_pEUVSource == NULL) return -4;

		if (g_pEUVSource->Is_SRC_Connected() != TRUE)
		{
			AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
			return -5;
		}

		if (g_pIO->Is_SourceGate_OpenOn_Check() != TRUE)
		{
			AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
			return -6;
		}
	}
	//////////////////////////////// 측정 Process Start ////////////////////////////////////////

	////UI reset
	g_pPTR->ClearGrid();

	//g_pPTR->ResetPtrDisplay(&m_MaskMapWnd.m_ProcessData);  수정필요


	//1. Background 측정
	////1) EUV shutter close
	g_pEUVSource->SetMechShutterOpen(FALSE);
	////2) EUV on:  
	if (g_pEUVSource->SetEUVSourceOn(TRUE) != 0)
	{
		AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
		return -999;
	}
	if (!isSimulation)
	{
		WaitSec(10);
	}

	// 버튼 정지
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pEUVSource->SetMechShutterOpen(FALSE);
		g_pEUVSource->SetEUVSourceOn(FALSE);
		return -5;
	}
	////3) Background 측정위치 이동
	g_pNavigationStage->MoveToMaskCenterCoordinate(m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.X, m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.Y, TRUE); //+ 방향
	////4) 측정	
	g_pPTR->m_nPTRDataNum = m_MaskMapWnd.m_ProcessData.m_nBeamDiagPosExposureTime_ms;

	if (!isSimulation)
	{
		g_pPTR->ReadSensor();
	}

	g_pAdam->mD1BackGround = g_pAdam->mD1;
	g_pAdam->mD2BackGround = g_pAdam->mD2;
	g_pAdam->mD3BackGround = g_pAdam->mD3;

	g_pAdam->mD1BackGraoundStd = g_pAdam->mD1Std;
	g_pAdam->mD2BackGraoundStd = g_pAdam->mD2Std;
	g_pAdam->mD3BackGraoundStd = g_pAdam->mD3Std;

	g_pPTR->SensorGridUpdate();
	g_pPTR->UpdateData(FALSE);


	// 버튼 정지
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		g_pEUVSource->SetMechShutterOpen(FALSE);
		g_pEUVSource->SetEUVSourceOn(FALSE);
		return -5;
	}

	//2. Through Pellicle 측정
	////1) Through Pellicle 측정위치 이동
	g_pNavigationStage->MoveToMaskCenterCoordinate(m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.X, m_MaskMapWnd.m_ProcessData.m_stBeamDiagnosisPos_um.Y, TRUE); //+ 방향
	////2) EUV shutter open
	g_pEUVSource->SetMechShutterOpen(TRUE);
	WaitSec(2);
	////3) 측정
	g_pPTR->m_nPTRDataNum = m_MaskMapWnd.m_ProcessData.m_nBeamDiagPosExposureTime_ms;
	if (!isSimulation)
	{
		g_pPTR->ReadSensor();
	}

	g_pAdam->mD1WithoutPellicle = g_pAdam->mD1;
	g_pAdam->mD2WithoutPellicle = g_pAdam->mD2;
	g_pAdam->mD3WithoutPellicle = g_pAdam->mD3;

	g_pAdam->mD1WithoutPellicleStd = g_pAdam->mD1Std;
	g_pAdam->mD2WithoutPellicleStd = g_pAdam->mD2Std;
	g_pAdam->mD3WithoutPellicleStd = g_pAdam->mD3Std;

	g_pPTR->SensorGridUpdate();
	g_pPTR->UpdateData(FALSE);

	////4) EUV shutter close
	//g_pEUVSource->SetMechShutterOpen(FALSE);	
	//WaitSec(1);

	int LineNum = g_pPTR->m_NumOfYGrid;
	double x_Margin_um = 100;
	int x_GridMargin = x_Margin_um / g_pPTR->m_XInterval_um;
	int PointNum = g_pPTR->m_NumOfXGrid + x_GridMargin;


	g_pPTR->m_NumOfXGridwithMargin = PointNum;
	g_pPTR->m_NumOfYGridwithMargin = LineNum;

	g_pPTR->AllocForScan(g_pPTR->m_NumOfXGridwithMargin, g_pPTR->m_NumOfYGridwithMargin);


	g_pLog->Display(0, _T("CMaskMapDlg::PtrProcess() Scan START!"));

	//g_pPTR->SelectDisp(IndexCoupon);   수정필요
	for (int IndexLine = 0; IndexLine < LineNum; IndexLine++)
	{
		////1) 측정위치 이동
		g_pNavigationStage->MoveToMaskCenterCoordinate(g_pPTR->m_XStart_um - x_Margin_um, g_pPTR->m_YStart_um + IndexLine * g_pPTR->m_YInterval_um, TRUE); //+ 방향
		////2) ScanStart, AdamStart
		m_strStageStatus.Format(_T("PTR Auto Process:  Scanline: %d/%d 측정 시작"), IndexLine + 1, LineNum);
		UpdateData(FALSE);

		//3)Stage Start
		double xpos_tartget, ypos_target;
		double currentStartX_um, currentStartY_um;

		currentStartX_um = g_pPTR->m_XEnd_um + x_Margin_um;
		currentStartY_um = g_pPTR->m_YStart_um + IndexLine * g_pPTR->m_YInterval_um;

		g_pNavigationStage->MoveToMaskCoordinateWithoutInpositionForPTR(currentStartX_um, currentStartY_um, &xpos_tartget, &ypos_target); //+ 방향

		//4)Adam Set and Start
		g_pPTR->m_nPTRDataNum = PointNum;
		if (!isSimulation)
		{
			g_pPTR->ReadSensor();
		}

		//5)Stage Wait
		g_pNavigationStage->WaitInPosition(STAGE_X_AXIS, xpos_tartget);
		g_pNavigationStage->WaitInPosition(STAGE_Y_AXIS, ypos_target);

		m_MaskMapWnd.Invalidate(FALSE);

		for (int IndexPoint = 0; IndexPoint < PointNum; IndexPoint++)
		{
			//5) 결과 update
			/*g_pAdam->mD1WithPellicle = g_pAdam->m_dRawImageForDisplay[Detector1][IndexPoint];
			g_pAdam->mD2WithPellicle = g_pAdam->m_dRawImageForDisplay[Detector2][IndexPoint];
			g_pAdam->mD3WithPellicle = g_pAdam->m_dRawImageForDisplay[Detector3][IndexPoint];
*/
			g_pAdam->mD1WithPellicleStd = g_pAdam->mD1Std;
			g_pAdam->mD2WithPellicleStd = g_pAdam->mD2Std;
			g_pAdam->mD3WithPellicleStd = g_pAdam->mD3Std;

			g_pPTR->SensorGridUpdate();

			////6) 계산
			double x = currentStartX_um + g_pPTR->m_XInterval_um;
			double y = currentStartY_um;
			//g_pPTR->CalculatePTR(x, y);

			g_pPTR->CalculatePtrForScan(x, y);
		}


		//g_pPTR->MakeSmallDataForScan(IndexLine);

		g_pPTR->UpdateData(FALSE);

		// 7)Display Data updata
		//g_pPTR->PushDispData(IndexCoupon, currentPointInCoupon, g_pAdam->mT);

		m_strStageStatus.Format(_T("PTR Auto Process:  Scanline: %d/%d 측정 종료"), IndexLine + 1, LineNum);
		UpdateData(FALSE);

		// 버튼 정지
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			g_pEUVSource->SetMechShutterOpen(FALSE);
			g_pEUVSource->SetEUVSourceOn(FALSE);

			g_pPTR->DeleteForScan();
			return -5;
		}
	}

	g_pLog->Display(0, _T("CMaskMapDlg::PtrProcess() Scan END!"));

	//g_pPTR->MakeSmallDataForScan();

	g_pPTR->SaveFileForScan();
	//if (g_pPTR->m_bAutoScaleImage)
	//{
	//	g_pPTR->UpdateDataMinMaxFromData(IndexCoupon);
	//}
	//g_pPTR->UpdateU16Image(g_pPTR->m_DataMinArray[IndexCoupon], g_pPTR->m_DataMaxArray[IndexCoupon], IndexCoupon);
	//MdispControl(g_pPTR->m_MilDisplay, M_UPDATE, M_NOW);


	UpdateData(FALSE);
	g_pPTR->UpdateData(FALSE);

	//////////////////////////// 측정 종료 ///////////////////////////////////////////////////		
	//EUV off
	g_pEUVSource->SetMechShutterOpen(FALSE);
	g_pEUVSource->SetEUVSourceOn(FALSE);
	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Auto PTR 측정이 완료되었습니다! "), 2);
	m_strStageStatus.Format(_T("PTR Auto Process 완료!"));
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::PtrProcess() End!"));

	g_pPTR->DeleteForScan();
	return 0;
}



int CMaskMapDlg::MaskAlign_OM(BOOL bManual)
{
	int nRet = 0;

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return -1;

	if (g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		return -2;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.TotalMeasureNum == 0)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		return -3;
	}
	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 2);
		return -4;
	}

	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_OM() Start!"));

	m_strStageStatus.Format(_T("Mask Align(OM) 시작!"));
	UpdateData(FALSE);
	g_pWarning->m_strWarningMessageVal = "Mask Align(OM) 중입니다. 잠시 기다려 주세요!";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bOMAlignComplete = FALSE;
	m_bAutoSequenceProcessing = TRUE;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -8;
	}
	WaitSec(1);

	//2. OM 영역이 아니면 OM영역으로 이동
	g_pWarning->m_strWarningMessageVal = "Mask Align 중입니다. 잠시 기다려 주세요!";
	g_pWarning->UpdateData(FALSE);
	ChangeOMEUVSet(EUVTOOM);
	g_pWarning->ShowWindow(SW_SHOW);
	m_bAutoSequenceProcessing = TRUE;

	CMessageDlg MsgBox;
	double target_posx_mm = 0.0, target_posy_mm = 0.0;
	double current_posx_mm = 0.0, current_posy_mm = 0.0;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -9;
	}

	//1. OM RT Align Start
	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X / 1000.),
		g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y / 1000.));

	if (bManual)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		if (MsgBox.DoModal(" OM 1st Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
		{
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -10;
		}
		g_pWarning->ShowWindow(SW_SHOW);
	}
	else
	{
		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(FALSE);
		nRet = g_pRecipe->SearchPMModel(PM_3RDALING_POSITION);
		g_pRecipe->WaitSec(0.5);
		nRet = g_pRecipe->SearchPMModel(PM_3RDALING_POSITION);
		g_pRecipe->WaitSec(0.5);
		nRet = g_pRecipe->SearchPMModel(PM_3RDALING_POSITION);
		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(TRUE);
		if (nRet != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -15; // 매칭 Fail
		}
	}

	m_dOMAlignPointRT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dOMAlignPointRT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dOMAlignPointRT_X_mm = m_dOMAlignPointRT_posx_mm;
	g_pConfig->m_dOMAlignPointRT_Y_mm = m_dOMAlignPointRT_posy_mm;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -10;
	}

	CString str;
	str.Format(_T("OM_RT_X = %.6f, OM_RT_Y = %.6f"), m_dOMAlignPointRT_posx_mm, m_dOMAlignPointRT_posy_mm);
	SaveLogFile("Acuuracy", _T((LPSTR)(LPCTSTR)(str)));

	//2. OM LT Align Start
	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X / 1000.),
		g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y / 1000.));

	if (bManual)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		if (MsgBox.DoModal(" OM 2nd Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
		{
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -10;
		}
		g_pWarning->ShowWindow(SW_SHOW);
	}
	else
	{
		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(FALSE);
		nRet = g_pRecipe->SearchPMModel(PM_2NDALIGN_POSITION);
		g_pRecipe->WaitSec(0.5);
		nRet = g_pRecipe->SearchPMModel(PM_2NDALIGN_POSITION);
		g_pRecipe->WaitSec(0.5);
		nRet = g_pRecipe->SearchPMModel(PM_2NDALIGN_POSITION);
		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(TRUE);
		if (nRet != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -15; // 매칭 Fail
		}
	}

	m_dOMAlignPointLT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dOMAlignPointLT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dOMAlignPointLT_X_mm = m_dOMAlignPointLT_posx_mm;
	g_pConfig->m_dOMAlignPointLT_Y_mm = m_dOMAlignPointLT_posy_mm;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -11;
	}


	//3. OM LB Align Start
	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000.),
		g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - (g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000.));

	if (bManual)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		if (MsgBox.DoModal(" OM 3rd Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
		{
			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -10;
		}
		g_pWarning->ShowWindow(SW_SHOW);
	}
	else
	{
		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(FALSE);
		nRet = g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
		g_pRecipe->WaitSec(0.5);
		nRet = g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
		g_pRecipe->WaitSec(0.5);
		nRet = g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
		g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(TRUE);
		if (nRet != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -15; // 매칭 Fail
		}
	}

	m_dOMAlignPointLB_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	m_dOMAlignPointLB_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	g_pConfig->m_dOMAlignPointLB_X_mm = m_dOMAlignPointLB_posx_mm;
	g_pConfig->m_dOMAlignPointLB_Y_mm = m_dOMAlignPointLB_posy_mm;

	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -12;
	}

	m_bOMAlignComplete = TRUE;
	g_pConfig->m_bOMAlignCompleteFlag = m_bOMAlignComplete;
	g_pConfig->SaveRecoveryData();

	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" OM Align이 완료되었습니다! "), 2);
	m_bAutoSequenceProcessing = FALSE;
	m_strStageStatus.Format(_T("Mask OM Align 완료!"));
	m_MaskMapWnd.Invalidate(FALSE);
	UpdateData(FALSE);
	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_OM() End!"));
	return 0;
}

int CMaskMapDlg::MaskAlign_EUV_SREM(BOOL bManual)
{
//	if (g_pConfig->m_nEquipmentMode == OFFLINE)
//		return -1;
//
//	if (g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
//		return -2;
//
//	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
//	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.TotalMeasureNum == 0)
//	{
//		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
//		return -3;
//	}
//	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
//	{
//		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 2);
//		return -4;
//	}
//
//	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_EUV_SREM() Start!"));
//
//	CMessageDlg MsgBox;
//	double target_posx_mm = 0.0, target_posy_mm = 0.0;
//	double current_posx_mm = 0.0, current_posy_mm = 0.0;
//
//	m_strStageStatus.Format(_T("Mask Align(EUV) 시작!"));
//	UpdateData(FALSE);
//	g_pWarning->m_strWarningMessageVal = "Mask Align(EUV) 중입니다. 잠시 기다려 주세요!";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//	m_bOMAlignComplete = FALSE;
//	m_bAutoSequenceProcessing = TRUE;
//
//	// 삭제 ihlee
//	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
//	//{
//	//	g_pNavigationStage->SetEncoderMode();
//	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);
//	//	//WaitSec(1);
//	//}
//
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		return -8;
//	}
//	WaitSec(1);
//
//	m_bEUVAlignComplete = FALSE;
//
//	g_pCamera->ShowWindow(SW_HIDE);
//	g_pRecipe->ShowWindow(SW_HIDE);
//	g_pAdam->ShowWindow(SW_SHOW);
//	m_MaskMapWnd.m_nMicroscopyType = EUV;
//	m_CheckOMAdamCtrl.SetWindowText(_T("To Optic Microscopy"));
//	g_pAdam->m_bEUVCrossLineDisplay = TRUE;
//	g_pAdam->m_comboFovSelect.SetCurSel(5); // 10000 nm
//	g_pAdam->m_nEuvImage_Fov = g_pConfig->m_nEUVAlignFOV; // 10000 nm
//	//g_pAdam->m_nEuvImage_Old_Fov = g_pAdam->m_nEuvImage_Fov;
//	g_pAdam->m_nEuvImage_ScanGrid = g_pConfig->m_nEUVAlignGrid;
//	if (g_pConfig->m_nEUVAlignGrid == 40)
//		g_pAdam->m_comboScanGridSelect.SetCurSel(3);
//	else if (g_pConfig->m_nEUVAlignGrid == 80)
//		g_pAdam->m_comboScanGridSelect.SetCurSel(4);
//	g_pAdam->m_comboScanNumber.SetCurSel(0);
//	g_pAdam->m_nEuvImage_ScanNumber = 1;
//
//	g_pAdam->m_bIsUseHighFovInterpolationOld = g_pAdam->m_bIsUseHighFovInterpolation;
//	g_pAdam->m_bIsUseHighFovInterpolation = FALSE;
//	g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//	g_pAdam->Invalidate(FALSE);
//
//	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//		return -13;
//	}
//
//	if (g_pEUVSource->Is_EUV_On() == FALSE)
//	{
//		g_pWarning->m_strWarningMessageVal = " EUV On 중입니다. 잠시 기다려 주세요! ";
//		g_pWarning->UpdateData(FALSE);
//		g_pEUVSource->SetMechShutterOpen(TRUE);
//		if (g_pEUVSource->SetEUVSourceOn(TRUE) != 0)
//		{
//			return -999;
//		}
//	}
//
//	g_pWarning->m_strWarningMessageVal = " Mask Align(EUV) 중입니다. 잠시 기다려 주세요! ";
//	g_pWarning->UpdateData(FALSE);
//
//
//	//4. EUV LB Align Start
//	current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//	current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//
//	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE)
//	{
//		target_posx_mm = m_dOMAlignPointLB_posx_mm - g_pConfig->m_dLBOffsetX_mm + g_pConfig->m_dLB_LaserSwitching_OffsetX_mm;
//		target_posy_mm = m_dOMAlignPointLB_posy_mm + g_pConfig->m_dLBOffsetY_mm + g_pConfig->m_dLB_LaserSwitching_OffsetY_mm;
//	}
//	else
//	{
//		target_posx_mm = m_dOMAlignPointLB_posx_mm - g_pConfig->m_dLBOffsetX_mm;
//		target_posy_mm = m_dOMAlignPointLB_posy_mm + g_pConfig->m_dLBOffsetY_mm;
//	}
//
//	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= 0)//이부분 확인 필요. IHLEE//asdf
//	//if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= Z_INITIAL_POS_UM) // ihlee 변경
//	//{
//	//	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//EUV 영역으로 이동
//	//	WaitSec(1);
//	//	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//정확히 못간 경우 있을 수 있으므로 한번 더 가자
//	//	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//	//	//{
//	//	//	g_pNavigationStage->SetLaserMode();
//	//	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//	//	//	WaitSec(1);
//	//	//}
//	//	g_pScanStage->MoveZAbsolute_SlowInterlock(m_dZFocusPos);
//	//	WaitSec(2);
//	//
//	//
//	//	//// 하이닉스 마스크용 임시코드: Cap2번으로 Focus Setting //////////////////////////////////////////////////
//	//	//double dDifferencez = 0.0;
//	//	//if (g_pAdam->Command_CapReadTimeout() != 0)
//	//	//{
//	//	//	return -10;
//	//	//}
//	//	////WaitSec(1);
//	//	////dDifferencez = g_pConfig->m_dZCapStageOffset_um - g_pAdam->AdamData.m_dCapsensor2;
//	//	//dDifferencez = g_pConfig->m_dZdistanceCap2nStage_um - g_pAdam->AdamData.m_dCapsensor2;
//	//	////g_pAdam->Command_ADAMSimpleRun();
//	//	//if (g_pScanStage->m_dPIStage_GetPos[Z_AXIS] + dDifferencez < g_pConfig->m_dZInterlock_um + 10)
//	//	//{
//	//	//	m_dZFocusPos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS] + dDifferencez;
//	//	//	UpdateData(FALSE);
//	//	//}
//	//	//g_pScanStage->MoveZAbsolute_SlowInterlock(m_dZFocusPos);
//	//	//WaitSec(2);
//	//	////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	//
//	//	// 21.01.17 Z축 제어 변경, 인터락 세팅 안함. 
//	//	int nRetFocus = g_pAdam->MoveZCapsensorFocusPosition(TRUE);// Interlock은 세팅 안함..
//	//	WaitSec(2);
//	//	if (nRetFocus != 0)
//	//	{
//	//		g_pWarning->ShowWindow(SW_HIDE);
//	//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다!(Z축 제어 에러) "), 2);
//	//		m_bAutoSequenceProcessing = FALSE;
//	//		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//	//		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//	//		return -17;
//	//	}
//	//}
//	//
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//		return -14;
//	}
//
//	WaitSec(1);
//
//	MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//	g_pAdam->ADAMRunStart();
//	g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
////	g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, 1);
//	WaitSec(1);
//
//	while (g_pAdam->m_bIsScaningOn)
//	{
//		//ProcessMessages();
//		//Sleep(10);
//		WaitSec(1);
//	}
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//		return -15;
//	}
//	WaitSec(1);
//	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//	//{
//	//	g_pNavigationStage->SetLaserMode();
//	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//	//	WaitSec(1);
//	//}
//
//	if (bManual)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		if (MsgBox.DoModal(" EUV First Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
//		{
//			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//			g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//			MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//			return -16;
//		}
//		g_pWarning->ShowWindow(SW_SHOW);
//	}
//	else
//	{
//		g_pAdam->GetCornerPixel(&g_pAdam->m_nEdgeFindResultX, &g_pAdam->m_nEdgeFindResultY);
//		g_pAdam->Invalidate(FALSE);
//		if (g_pAdam->m_bEUVEdgeFindSuccess == TRUE)
//		{
//			int center_pixel_x = (int)(g_pAdam->m_nEdgeFindResultX - g_pAdam->m_nRawImage_PixelWidth / 2);
//			int center_pixel_y = (int)(g_pAdam->m_nEdgeFindResultY - g_pAdam->m_nRawImage_PixelHeight / 2);
//			double nCenter_DistanceX_mm ;//= (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_x);
//			double nCenter_DistanceY_mm ;//= (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_y);
//
//			g_pAdam->EUVMoveLineDraw(center_pixel_x, center_pixel_y);
//
//			current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//			current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//			target_posx_mm = current_posx_mm - nCenter_DistanceX_mm;
//			target_posy_mm = current_posy_mm + nCenter_DistanceY_mm;
//			//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//			//{
//			//	g_pNavigationStage->SetLaserMode();
//			//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//			//	WaitSec(1);
//			//}
//			g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//			WaitSec(1);
//			g_pAdam->m_bEUVEdgeFindSuccess = FALSE;
//		}
//	}
//
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//		return -16;
//	}
//	WaitSec(1);
//
//	m_dEUVAlignPointLB_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//	m_dEUVAlignPointLB_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//	g_pConfig->m_dEUVAlignPointLB_X_mm = m_dEUVAlignPointLB_posx_mm;
//	g_pConfig->m_dEUVAlignPointLB_Y_mm = m_dEUVAlignPointLB_posy_mm;
//
//	//5. EUV LT Align Start
//
//	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
//	{
//		target_posx_mm = m_dOMAlignPointLT_posx_mm - g_pConfig->m_dLTOffsetX_mm + g_pConfig->m_dLT_LaserSwitching_OffsetX_mm;
//		target_posy_mm = m_dOMAlignPointLT_posy_mm + g_pConfig->m_dLTOffsetY_mm + g_pConfig->m_dLT_LaserSwitching_OffsetY_mm;
//	}
//	else
//	{
//		target_posx_mm = m_dOMAlignPointLT_posx_mm - g_pConfig->m_dLTOffsetX_mm;
//		target_posy_mm = m_dOMAlignPointLT_posy_mm + g_pConfig->m_dLTOffsetY_mm;
//	}
//	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//	//{
//	//	g_pNavigationStage->SetLaserMode();
//	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//	//	WaitSec(1);
//	//}
//	g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//	WaitSec(1);
//	g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//	WaitSec(1);
//
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//		return -17;
//	}
//
//	WaitSec(1);
//
//	MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//	g_pAdam->ADAMRunStart();
//	g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
//	//g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, 1);
//	WaitSec(1);
//
//	while (g_pAdam->m_bIsScaningOn)
//	{
//		//ProcessMessages();
//		//Sleep(10);
//		WaitSec(1);
//	}
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//		return -18;
//	}
//	WaitSec(1);
//	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//	//{
//	//	g_pNavigationStage->SetLaserMode();
//	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//	//	WaitSec(1);
//	//}
//
//	if (bManual)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		if (MsgBox.DoModal(" EUV 2nd Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
//		{
//			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//			g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//			MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//			return -16;
//		}
//		g_pWarning->ShowWindow(SW_SHOW);
//	}
//	else
//	{
//		g_pAdam->GetCornerPixel(&g_pAdam->m_nEdgeFindResultX, &g_pAdam->m_nEdgeFindResultY);
//		g_pAdam->Invalidate(FALSE);
//		if (g_pAdam->m_bEUVEdgeFindSuccess == TRUE)
//		{
//			int center_pixel_x = (int)(g_pAdam->m_nEdgeFindResultX - g_pAdam->m_nRawImage_PixelWidth / 2);
//			int center_pixel_y = (int)(g_pAdam->m_nEdgeFindResultY - g_pAdam->m_nRawImage_PixelHeight / 2);
//			double nCenter_DistanceX_mm;// = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_x);
//			double nCenter_DistanceY_mm;// = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_y);
//
//			g_pAdam->EUVMoveLineDraw(center_pixel_x, center_pixel_y);
//
//			current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//			current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//			target_posx_mm = current_posx_mm - nCenter_DistanceX_mm;
//			target_posy_mm = current_posy_mm + nCenter_DistanceY_mm;
//			//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//			//{
//			//	g_pNavigationStage->SetLaserMode();
//			//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//			//	WaitSec(1);
//			//}
//			g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//			WaitSec(1);
//			g_pAdam->m_bEUVEdgeFindSuccess = FALSE;
//		}
//	}
//
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//		return -19;
//	}
//	WaitSec(1);
//
//	m_dEUVAlignPointLT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//	m_dEUVAlignPointLT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//	g_pConfig->m_dEUVAlignPointLT_X_mm = m_dEUVAlignPointLT_posx_mm;
//	g_pConfig->m_dEUVAlignPointLT_Y_mm = m_dEUVAlignPointLT_posy_mm;
//
//	//6. EUV RT Align Start
//	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
//	{
//		target_posx_mm = m_dOMAlignPointRT_posx_mm - g_pConfig->m_dRTOffsetX_mm + g_pConfig->m_dRT_LaserSwitching_OffsetX_mm;
//		target_posy_mm = m_dOMAlignPointRT_posy_mm + g_pConfig->m_dRTOffsetY_mm + g_pConfig->m_dRT_LaserSwitching_OffsetY_mm;
//	}
//	else
//	{
//		target_posx_mm = m_dOMAlignPointRT_posx_mm - g_pConfig->m_dRTOffsetX_mm;
//		target_posy_mm = m_dOMAlignPointRT_posy_mm + g_pConfig->m_dRTOffsetY_mm;
//	}
//	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//	//{
//	//	g_pNavigationStage->SetLaserMode();
//	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//	//	WaitSec(1);
//	//}
//	g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//	WaitSec(1);
//	g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//	WaitSec(1);
//
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_TRUE);
//		return -20;
//	}
//
//	WaitSec(1);
//
//	MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//	g_pAdam->ADAMRunStart();
//	g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
//	//g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, 1);
//	WaitSec(1);
//
//	while (g_pAdam->m_bIsScaningOn)
//	{
//		//ProcessMessages();
//		//Sleep(10);
//		WaitSec(1);
//	}
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//		return -21;
//	}
//	WaitSec(1);
//	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//	//{
//	//	g_pNavigationStage->SetLaserMode();
//	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//	//	WaitSec(1);
//	//}
//
//	if (bManual)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		if (MsgBox.DoModal(" EUV 3rd Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
//		{
//			MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
//			m_bAutoSequenceProcessing = FALSE;
//			g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//			g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//			MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//			return -16;
//		}
//		g_pWarning->ShowWindow(SW_SHOW);
//	}
//	else
//	{
//		g_pAdam->GetCornerPixel(&g_pAdam->m_nEdgeFindResultX, &g_pAdam->m_nEdgeFindResultY);
//		g_pAdam->Invalidate(FALSE);
//		if (g_pAdam->m_bEUVEdgeFindSuccess == TRUE)
//		{
//			int center_pixel_x = (int)(g_pAdam->m_nEdgeFindResultX - g_pAdam->m_nRawImage_PixelWidth / 2);
//			int center_pixel_y = (int)(g_pAdam->m_nEdgeFindResultY - g_pAdam->m_nRawImage_PixelHeight / 2);
//			double nCenter_DistanceX_mm ;//= (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_x);
//			double nCenter_DistanceY_mm ;//= (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_y);
//
//			g_pAdam->EUVMoveLineDraw(center_pixel_x, center_pixel_y);
//
//			current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//			current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//			target_posx_mm = current_posx_mm - nCenter_DistanceX_mm;
//			target_posy_mm = current_posy_mm + nCenter_DistanceY_mm;
//			//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//			//{
//			//	g_pNavigationStage->SetLaserMode();
//			//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//			//	WaitSec(1);
//			//}
//			g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
//			WaitSec(1);
//			g_pAdam->m_bEUVEdgeFindSuccess = FALSE;
//		}
//	}
//
//	if (m_bAutoSequenceProcessing == FALSE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
//		m_bAutoSequenceProcessing = FALSE;
//		g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//		g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//		MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//		return -22;
//	}
//	WaitSec(1);
//
//	m_dEUVAlignPointRT_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
//	m_dEUVAlignPointRT_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
//	g_pConfig->m_dEUVAlignPointRT_X_mm = m_dEUVAlignPointRT_posx_mm;
//	g_pConfig->m_dEUVAlignPointRT_Y_mm = m_dEUVAlignPointRT_posy_mm;
//
//
//
//
//	g_pEUVSource->SetMechShutterOpen(FALSE);
//	//g_pEUVSource->SetEUVSourceOn(FALSE); //ihlee 임시
//
//	m_bEUVAlignComplete = TRUE;
//	g_pConfig->m_bEUVAlignCompleteFlag = m_bEUVAlignComplete;
//	g_pConfig->SaveRecoveryData();
//	m_bAutoSequenceProcessing = FALSE;
//	g_pWarning->ShowWindow(SW_HIDE);
//	MsgBoxAuto.DoModal(_T(" Mask Align이 완료되었습니다! "), 2);
//	m_strStageStatus.Format(_T("Mask Align(EUV) 완료!"));
//	m_MaskMapWnd.Invalidate(FALSE);
//	UpdateData(FALSE);
//	g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_EUV_SREM() End!"));
//
//	g_pAdam->m_bIsUseHighFovInterpolation = g_pAdam->m_bIsUseHighFovInterpolationOld;
//	g_pAdam->m_checkIsUseHighFovInterpolation.SetCheck(g_pAdam->m_bIsUseHighFovInterpolation);
//	MgraControlList(g_pAdam->m_MilGraphListCross, M_GRAPHIC_LABEL(g_pAdam->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
//
	return 0;
}






void CMaskMapDlg::OnBnClickedCheckMagnification()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}







//g_pNavigationStage->RunBuffer(6);
//double realvalue1 = 0.0, realvalue2 = 0.0;
//realvalue1 = g_pNavigationStage->GetGlobalRealVariable("DOUT_0");
//realvalue2 = g_pNavigationStage->GetGlobalRealVariable("DOUT_1");
//g_pNavigationStage->StopBuffer(6);


void CMaskMapDlg::OnBnClickedCheckOmalignonly()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedCheckOmalignonly() 버튼 클릭!"));

	UpdateData(TRUE);

	switch (m_CheckOMAlignOnlyCtrl.GetCheck())
	{
	case BST_CHECKED:
		m_CheckOMAlignOnlyCtrl.SetWindowText(_T("OM ALIGN"));
		break;
	case BST_UNCHECKED:
		m_CheckOMAlignOnlyCtrl.SetWindowText(_T("OM-EUV ALIGN"));
		break;
	default:
		break;
	}
}


void CMaskMapDlg::OnBnClickedCheckAlignAutomanual()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedCheckAlignAutomanual() 버튼 클릭!"));

	UpdateData(TRUE);

	switch (m_CheckManualAlignCtrl.GetCheck())
	{
	case BST_CHECKED:
		m_CheckManualAlignCtrl.SetWindowText(_T("MANUAL"));
		break;
	case BST_UNCHECKED:
		m_CheckManualAlignCtrl.SetWindowText(_T("AUTO"));
		break;
	default:
		break;
	}
}


void CMaskMapDlg::OnStnClickedTextStageposgrid()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void CMaskMapDlg::OnBnClickedCheckOmAdamUi()
{
	g_pLog->Display(0, _T("CMaskMapDlg::OnBnClickedCheckOmAdamUi() 버튼 클릭!"));
	UpdateData(TRUE);

	//	jhkim -2021.01.25
	//	Scan Stage Roll, Pitch 보정 UI 임시 작업으로 수정.
	switch (m_AdamOmtiltSwitch)
	{
	case 0:
		// OM UI		
		g_pCamera->ShowWindow(SW_SHOW);
		g_pRecipe->ShowWindow(SW_SHOW);
		//g_pScanStageTest->ShowWindow(SW_HIDE);
		g_pChart->ShowWindow(SW_HIDE);
		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			//if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_HIDE);
			g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("OM"));
			break;
		case EUVPTR:
			g_pPTR->ShowWindow(SW_HIDE);
			g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("PTR"));
			break;
		}
		m_AdamOmtiltSwitch++;
		break;
	case 1:
		// ADAM
		g_pCamera->ShowWindow(SW_HIDE);
		g_pRecipe->ShowWindow(SW_HIDE);
		//g_pScanStageTest->ShowWindow(SW_HIDE);
		g_pChart->ShowWindow(SW_HIDE);

		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			//if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_SHOW);
			break;
		case EUVPTR:
			if (g_pPTR != NULL) g_pPTR->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}
		m_AdamOmtiltSwitch++;
		g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("Adam"));
		break;
	case 2:
		//TILT
		g_pCamera->ShowWindow(SW_HIDE);
		g_pRecipe->ShowWindow(SW_HIDE);
		//g_pAdam->ShowWindow(SW_HIDE);
		g_pPTR->ShowWindow(SW_HIDE);

		//g_pScanStageTest->ShowWindow(SW_SHOW);
		g_pChart->ShowWindow(SW_SHOW);

		m_AdamOmtiltSwitch = 0;
		g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("TILT"));

		break;
	default:
		break;
	}


	/*

	jhkim -2021.01.25
	Scan Stage Roll, Pitch 보정 UI 임시 작업으로 수정.


	switch (m_CheckOMAdamUi.GetCheck())
	{
	case BST_CHECKED:
		// OM UI
		g_pCamera->ShowWindow(SW_SHOW);
		g_pRecipe->ShowWindow(SW_SHOW);


		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_HIDE);
			g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("Adam"));
			break;
		case PHASE:
			g_pXrayCamera->ShowWindow(SW_HIDE);
			g_pPhase->ShowWindow(SW_HIDE);
			g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("Phase"));
			break;
		case EUVPTR:
			g_pPTR->ShowWindow(SW_HIDE);
			g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("PTR"));
			break;
		default:
			break;
		}

		break;
	case BST_UNCHECKED:
		g_pCamera->ShowWindow(SW_HIDE);
		g_pRecipe->ShowWindow(SW_HIDE);

		switch (g_pConfig->m_nEquipmentType)
		{
		case SREM033:
			if (g_pAdam != NULL) g_pAdam->ShowWindow(SW_SHOW);
			break;
		case PHASE:
			g_pXrayCamera->ShowWindow(SW_SHOW);
			g_pPhase->ShowWindow(SW_SHOW);
			break;
		case EUVPTR:
			if (g_pPTR != NULL) g_pPTR->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}
		g_pMaskMap->m_CheckOMAdamUi.SetWindowText(_T("OM"));

		break;
	default:
		break;
	}
	*/



	/*switch (m_CheckOMAdamCtrl.GetCheck())
	{
	case BST_CHECKED:
		ChangeOMEUVSet(OMTOEUV);
		break;
	case BST_UNCHECKED:
		ChangeOMEUVSet(EUVTOOM);
		break;
	default:
		break;
	}*/

}

void CMaskMapDlg::OnBnClickedCheckMaskmapReverseCoordinate()
{
	//m_bReverseCoordinate;
	UpdateData(TRUE);
}

double Norm(vector<double> const& u) {
	double accum = 0.;
	for (double x : u) {
		accum += x * x;
	}
	return sqrt(accum);
}

vector<double> Cross(vector<double> const &a, vector<double> const &b)
{
	vector<double> r(a.size());

	r[0] = a[1] * b[2] - a[2] * b[1];
	r[1] = a[2] * b[0] - a[0] * b[2];
	r[2] = a[0] * b[1] - a[1] * b[0];
	return r;
}

double Dot(vector<double> const &a, vector<double> const &b)
{
	double sum = 0;

	for (int i = 0; i < a.size(); i++)
	{
		sum = sum + a[i] * b[i];
	}

	return sum;
}

vector<double> Subtract(vector<double> const &a, vector<double> const &b)
{
	vector<double> r(a.size());
	r[0] = a[0] - b[0];
	r[1] = a[1] - b[1];
	r[2] = a[2] - b[2];
	return r;
}

vector<double> CMaskMapDlg::CenterOfCircumCircle(vector<double> P1, vector<double> P2, vector<double> P3)
{
	double r = Norm(Subtract(P1, P2))*Norm(Subtract(P2, P3))*Norm(Subtract(P3, P1)) / 2 / Norm(Cross(Subtract(P2, P1), Subtract(P2, P3)));


	vector<double> center(P1.size());

	double alpha = Norm(Subtract(P2, P3)) * Norm(Subtract(P2, P3)) * Dot(Subtract(P1, P2), Subtract(P1, P3)) / 2 / (Norm(Cross(Subtract(P1, P2), Subtract(P2, P3)))*Norm(Cross(Subtract(P1, P2), Subtract(P2, P3))));
	double beta = Norm(Subtract(P1, P3)) * Norm(Subtract(P1, P3)) * Dot(Subtract(P2, P1), Subtract(P2, P3)) / 2 / (Norm(Cross(Subtract(P1, P2), Subtract(P2, P3)))*Norm(Cross(Subtract(P1, P2), Subtract(P2, P3))));
	double gama = Norm(Subtract(P1, P2)) * Norm(Subtract(P1, P2)) * Dot(Subtract(P3, P1), Subtract(P3, P2)) / 2 / (Norm(Cross(Subtract(P1, P2), Subtract(P2, P3)))*Norm(Cross(Subtract(P1, P2), Subtract(P2, P3))));

	//double  beta = Norm(Subtract(P1 , P3)) ^ 2 * Dot(P2 - P1, P2 - P3) / 2 / Norm(Cross(P1 - P2, P2 - P3)) ^ 2;
	//double  gama = Norm(Subtract(P1 , P2)) ^ 2 * Dot(P3 - P1, P3 - P2) / 2 / Norm(Cross(P1 - P2, P2 - P3)) ^ 2;

	center[0] = alpha * P1[0] + beta * P2[0] + gama * P3[0];
	center[1] = alpha * P1[1] + beta * P2[1] + gama * P3[1];
	center[2] = alpha * P1[2] + beta * P2[2] + gama * P3[2];

	double x = center[0];
	double y = center[1];

	return center;
}

double CMaskMapDlg::FindRotation(double x1, double y1, double x2, double y2)
{
	double pi = 3.14159265358979323846;
	//double rotation_rad = atan2(y2 - y1, x2 - x1);
	double rotation_rad = atan((y2 - y1) / (x2 - x1));

	return rotation_rad;
}

void CMaskMapDlg::OnBnClickedButtonNavigationstageConnect()
{
	if (g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		if (g_pNavigationStage->m_hComm != ACSC_INVALID)
			g_pNavigationStage->DisconnectComm();
	}

	if (!g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		if (g_pConfig->m_nEquipmentMode != OFFLINE)
		{
			if (!g_pNavigationStage->ConnectACSController(ETHERNET, g_pConfig->m_chIP[ETHERNET_NAVI_STAGE], g_pConfig->m_nPORT[ETHERNET_NAVI_STAGE]))
			{
				g_pCommStat->NavigationStageCommStatus(FALSE);
				g_pLoadingScreen.SetTextMessage(_T("Navigation Stage Connection Fail!"));
				AfxMessageBox(_T("Navigation Stage Connection Fail!"), MB_ICONERROR);
				WaitSec(3);
			}
			else
			{
				g_pLoadingScreen.SetTextMessage(_T("Navigation Stage Connection Success!"));
				AfxMessageBox(_T("Navigation Stage Connection Success!"), MB_ICONINFORMATION);
			}
		}
	}
}


void CMaskMapDlg::OnBnClickedButtonClearPosition()
{
	CString strX, strY;

	int nSelectedRowCount = m_StagePosGrid.GetSelectedCount() / 3;

	if (nSelectedRowCount == 0)
	{
		return;
	}
	else if (nSelectedRowCount == 1)
	{
		int row = 0;
		row = m_StagePosGrid.GetSelectedRow();

		if (row > MAX_STAGE_POSITION - 20)
		{
			CPasswordDlg pwdlg(this);
			pwdlg.DoModal();
			if (pwdlg.m_strTxt != "srem")
			{
				::AfxMessageBox("Password가 일치 하지 않습니다.");
				return;
			}
		}

		sprintf(g_pConfig->m_stStagePos[row - 1].chStagePositionString, "%s", "EMPTY");
		g_pConfig->m_stStagePos[row - 1].x = 0.0;
		g_pConfig->m_stStagePos[row - 1].y = 0.0;

		m_StagePosGrid.SetItemText(row, 1, "EMPTY");
		strX.Format(_T("%4.6f"), g_pConfig->m_stStagePos[row - 1].x);
		m_StagePosGrid.SetItemText(row, 2, strX);
		strY.Format(_T("%4.6f"), g_pConfig->m_stStagePos[row - 1].y);
		m_StagePosGrid.SetItemText(row, 3, strY);
	}
	else //Multiple selection
	{
		BOOL bCheck = FALSE;
		for (int nIdx = 81; nIdx < MAX_STAGE_POSITION + 1; nIdx++)	//SYSTEM POSITION이 선택되었는지 확인.
		{
			bCheck = m_StagePosGrid.IsCellSelected(nIdx, 1);
			if (bCheck == TRUE)
				break;
		}

		if (bCheck == TRUE)	//SYSTEM POSITION이 선택되어 있다면 암호 물어보기.
		{
			CPasswordDlg pwdlg(this);
			pwdlg.DoModal();
			if (pwdlg.m_strTxt != "srem")
			{
				::AfxMessageBox("Password가 일치 하지 않습니다.");
				return;
			}
		}

		for (int nIdx = 1; nIdx < MAX_STAGE_POSITION + 1; nIdx++)
		{
			bCheck = m_StagePosGrid.IsCellSelected(nIdx, 1);
			if (bCheck == TRUE)
			{
				sprintf(g_pConfig->m_stStagePos[nIdx - 1].chStagePositionString, "%s", "EMPTY");
				g_pConfig->m_stStagePos[nIdx - 1].x = 0.0;
				g_pConfig->m_stStagePos[nIdx - 1].y = 0.0;

				m_StagePosGrid.SetItemText(nIdx, 1, "EMPTY");
				strX.Format(_T("%4.6f"), g_pConfig->m_stStagePos[nIdx - 1].x);
				m_StagePosGrid.SetItemText(nIdx, 2, strX);
				strY.Format(_T("%4.6f"), g_pConfig->m_stStagePos[nIdx - 1].y);
				m_StagePosGrid.SetItemText(nIdx, 3, strY);
			}
		}
	}

	m_StagePosGrid.Invalidate(FALSE);
	g_pConfig->SaveStagePositionToDB();
}


void CMaskMapDlg::OnBnClickedButtonBackupPosition()
{
	CString sPath;
	sPath = CONFIG_BACKUP_PATH;
	if (!IsFolderExist(sPath))
	{
		CreateDirectory(sPath, NULL);
	}

	SYSTEMTIME	systemtime;
	::GetSystemTime(&systemtime);
	int msec = systemtime.wMilliseconds;
	CTime ctime = CTime::GetCurrentTime();
	ctime.GetAsSystemTime(systemtime);

	CString strDate, strTime;
	strDate.Format(_T("%04d%02d%02d"), ctime.GetYear(), ctime.GetMonth(), ctime.GetDay());
	strTime.Format(_T("%02d%02d%02d.%03d"), ctime.GetHour(), ctime.GetMinute(), ctime.GetSecond(), msec);
	sPath += "\\" + strDate + "_" + strTime + "_Stage_Position_DB.cdb";
	g_pConfig->BackupStagePositionToDB(sPath);
}


void CMaskMapDlg::OnBnClickedMaskmapCheckSetcurrentposition()
{
	((CButton*)GetDlgItem(IDC_MASKMAP_CHECK_SETCURRENTPOSITION))->SetCheck(TRUE);
	((CButton*)GetDlgItem(IDC_MASKMAP_CHECK_SETSELECTPOSITION))->SetCheck(FALSE);
	GetDlgItem(IDC_MASKMAP_COMBO_COUPONNUM)->EnableWindow(FALSE);
	GetDlgItem(IDC_MASKMAP_COMBO_MEASURENUM)->EnableWindow(FALSE);
	GetDlgItem(IDC_MASKMAP_BUTTON_MOVEPOSITION)->EnableWindow(FALSE);
}


void CMaskMapDlg::OnBnClickedMaskmapCheckSetselectposition()
{
	((CButton*)GetDlgItem(IDC_MASKMAP_CHECK_SETCURRENTPOSITION))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_MASKMAP_CHECK_SETSELECTPOSITION))->SetCheck(TRUE);
	GetDlgItem(IDC_MASKMAP_COMBO_COUPONNUM)->EnableWindow(TRUE);
	GetDlgItem(IDC_MASKMAP_COMBO_MEASURENUM)->EnableWindow(FALSE);
	GetDlgItem(IDC_MASKMAP_BUTTON_MOVEPOSITION)->EnableWindow(FALSE);

	//((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_COUPONNUM))->InsertString(1,_T("1"));
	//((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_COUPONNUM))->AddString(_T("2"));
	int nCount = ((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_COUPONNUM))->GetCount();
	for (size_t i = nCount; i > 0; i--)
	{
		((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_COUPONNUM))->DeleteString(i - 1);
	}
	//
	((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_COUPONNUM))->AddString("");
	for (int CouponIdx = 0; CouponIdx < m_MaskMapWnd.m_ProcessData.GetCoupons().size(); CouponIdx++)
	{
		//CString strTemp = m_MaskMapWnd.m_ProcessData.GetCoupons()[CouponIdx].CouponNo;
		//((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_COUPONNUM))->AddString(strTemp);
	}
}


void CMaskMapDlg::OnCbnEditchangeMaskmapComboCouponnum()
{
	int test = 0;
}


void CMaskMapDlg::OnCbnSelchangeMaskmapComboCouponnum()
{
	CString strTemp = "";
	((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_COUPONNUM))->GetLBText(((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_COUPONNUM))->GetCurSel(), strTemp);
	if (strTemp == "")
	{
		((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_MEASURENUM))->EnableWindow(FALSE);
	}
	else
	{
		((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_MEASURENUM))->EnableWindow(TRUE);
		for (int CouponIdx = 0; CouponIdx < m_MaskMapWnd.m_ProcessData.GetCoupons().size(); CouponIdx++)
		{
			//CString strCurCoupon = m_MaskMapWnd.m_ProcessData.GetCoupons()[CouponIdx].CouponNo;
			//if (strcmp(strTemp, strCurCoupon) == FALSE)
			//{
			//	int nCount = ((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_MEASURENUM))->GetCount();
			//	for (size_t i = nCount; i > 0; i--)
			//	{
			//		((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_MEASURENUM))->DeleteString(i - 1);
			//	}
			//	//
			//	((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_MEASURENUM))->AddString("");
			//	//((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_MEASURENUM))->SetCurSel(0);
			//
			//	for (int PointIdx = 0; PointIdx < m_MaskMapWnd.m_ProcessData.GetCoupons()[CouponIdx].GetPoints().size(); PointIdx++)
			//	{
			//		int nTemp = m_MaskMapWnd.m_ProcessData.GetCoupons()[CouponIdx].GetPoints()[PointIdx].No;
			//		CString strTemp = "";
			//		strTemp.Format(_T("Point %d"), PointIdx);
			//		((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_MEASURENUM))->AddString(strTemp);
			//	}
			//
			//	for (int ScanIdx = 0; ScanIdx < m_MaskMapWnd.m_ProcessData.GetCoupons()[CouponIdx].GetScans().size(); ScanIdx++)
			//	{
			//		int nTemp = m_MaskMapWnd.m_ProcessData.GetCoupons()[CouponIdx].GetScans()[ScanIdx].No;
			//		CString strTemp = "";
			//		strTemp.Format(_T("Scan %d"), ScanIdx);
			//		((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_MEASURENUM))->AddString(strTemp);
			//	}
			//}
		}
	}
}


void CMaskMapDlg::OnCbnSelchangeMaskmapComboMeasurenum()
{
	CString strTemp = "";
	((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_MEASURENUM))->GetLBText(((CComboBox*)GetDlgItem(IDC_MASKMAP_COMBO_MEASURENUM))->GetCurSel(), strTemp);
	if (strTemp == "")
	{
		GetDlgItem(IDC_MASKMAP_BUTTON_MOVEPOSITION)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_MASKMAP_BUTTON_MOVEPOSITION)->EnableWindow(TRUE);
	}
}

void CMaskMapDlg::OnBnClickedMaskmapButtonMeasurebackground()
{
	CString strTemp = "";
	GetDlgItemTextA(IDC_MASKMAP_EDIT_DATANUMBACKGROUND,strTemp);
	g_pPTR->ReadSensor(_ttoi(strTemp));

	//g_pAdam->mD1BackGround = g_pAdam->mD1;
	//g_pAdam->mD2BackGround = g_pAdam->mD2;
	//g_pAdam->mD3BackGround = g_pAdam->mD3;
	//
	//g_pAdam->mD1BackGraoundStd = g_pAdam->mD1Std;
	//g_pAdam->mD2BackGraoundStd = g_pAdam->mD2Std;
	//g_pAdam->mD3BackGraoundStd = g_pAdam->mD3Std;
}


void CMaskMapDlg::OnBnClickedMaskmapButtonMeasurewithoutpellicle()
{
	CString strTemp = "";
	GetDlgItemTextA(IDC_MASKMAP_EDIT_DATANUMWITHOUTPELLICLE, strTemp);
	g_pPTR->ReadSensor(_ttoi(strTemp));

	//g_pAdam->mD1WithoutPellicle = g_pAdam->mD1;
	//g_pAdam->mD2WithoutPellicle = g_pAdam->mD2;
	//g_pAdam->mD3WithoutPellicle = g_pAdam->mD3;
	//
	//g_pAdam->mD1WithoutPellicleStd = g_pAdam->mD1Std;
	//g_pAdam->mD2WithoutPellicleStd = g_pAdam->mD2Std;
	//g_pAdam->mD3WithoutPellicleStd = g_pAdam->mD3Std;
}


void CMaskMapDlg::OnBnClickedMaskmapButtonMeasurewithpellicle()
{
	CString strTemp = "";
	GetDlgItemTextA(IDC_MASKMAP_EDIT_DATANUMWITHPELLICLE, strTemp);
	g_pPTR->ReadSensor(_ttoi(strTemp));

	//g_pAdam->mD1WithPellicle = g_pAdam->mD1;
	//g_pAdam->mD2WithPellicle = g_pAdam->mD2;
	//g_pAdam->mD3WithPellicle = g_pAdam->mD3;
	//
	//g_pAdam->mD1WithPellicleStd = g_pAdam->mD1Std;
	//g_pAdam->mD2WithPellicleStd = g_pAdam->mD2Std;
	//g_pAdam->mD3WithPellicleStd = g_pAdam->mD3Std;
}


void CMaskMapDlg::OnBnClickedMaskmapButtonMoveposition()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}
