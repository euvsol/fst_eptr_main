﻿/**
 * Mask Map Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once
#include	"MaskMapWnd.h"
#include <tuple>

 // CMaskMapDlg 대화 상자

#define		OMTOEUV		1
#define		EUVTOOM		2

class CMaskMapDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CMaskMapDlg)

public:
	CMaskMapDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CMaskMapDlg();

	// 대화 상자 데이터입니다.
	//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MASK_MAP_DIALOG };
	//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	HICON m_hIcon;

	HICON m_LedIcon[3];
	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	afx_msg void OnBnClickedButtonRecipeFileSearch();
	afx_msg void OnBnClickedButtonRecipeFileLoad();
	afx_msg void OnBnClickedButtonMapSave();
	afx_msg void OnBnClickedCheckCodeShow();
	afx_msg void OnNMClickListFilelist(NMHDR *pNMHDR, LRESULT *pResult);

	CDateTimeCtrl m_DateTimeStartCtrl;
	COleDateTime m_DateTimeStart;
	CDateTimeCtrl m_DateTimeEndCtrl;
	COleDateTime m_DateTimeEnd;
	CListCtrl m_ListCtrlRecipeFile;

	CButton m_BtnSearchFileCtrl;
	CRichEditCtrl m_reCtrl;

	bool		bFinding;
	CFileFinder	m_filefinder;
	void FileSearchInFolder(CString strSearchFile, CString strBaseFolder);
	static void FileSearching(CFileFinder *pfilefinder, DWORD dwCode, void *pCustomParam);
	int		FindInList(LPCTSTR szFilename);
	void	AddFileToList(LPCTSTR szFilename);
	void	SetStatus(int nCount = 0, LPCTSTR szFolder = NULL);
	CString	GetListFilename(int nIndex);
	CPath GetCurSelListCtrl();
	void RecipeHeaderDisplay();
	BOOL OpenFile(char *fpath);


public:
	CMaskMapWnd	m_MaskMapWnd;
	int		m_nMapWidthPixelNo;
	int		m_nMapHeightPixelNo;
	BOOL m_bShowcode;
	BOOL m_bSelZoomCheck;
	BOOL m_bMaskMap;

	int LoadProcessData();

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtonMesurepointmove();
	afx_msg void OnBnClickedButtonMaskcoordmove();
	afx_msg void OnEnChangeEditPointno();
	afx_msg void OnEnChangeEditMaskcodx();
	afx_msg void OnEnChangeEditMaskcody();
	afx_msg void OnBnClickedCheckOmadam();
	afx_msg void OnBnClickedButtonGetPosition();
	afx_msg void OnEnChangeEditIncrementStepsize();
	afx_msg void OnBnClickedButtonXMinus();
	afx_msg void OnBnClickedButtonXPlus();
	afx_msg void OnBnClickedButtonYPlus();
	afx_msg void OnBnClickedButtonYMinus();
	afx_msg void OnTimer(UINT_PTR nIDEvent);


	CEdit m_CtrlStepSizeum;
	CButton m_btnXMinusCtrl;
	CButton m_btnXPlusCtrl;
	CButton m_btnYPlusCtrl;
	CButton m_btnYMinusCtrl;

	void InitButtonSkin();


	CGridCtrl m_StagePosGrid;
	void InitStagePosGrid();

	afx_msg void OnStagePosGridClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnStagePosGridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnStagePosGridRClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnStagePosGridStartEdit(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnStagePosGridEndEdit(NMHDR *pNotifyStruct, LRESULT* pResult);
	CString m_strStagePosName;
	CString m_strStageStatus;
	CString m_strStageMovingDistance;
	double	m_dStageMovingDistance;

	afx_msg void OnDeltaposIncrementStepspin(NMHDR *pNMHDR, LRESULT *pResult);	
	afx_msg void OnBnClickedButtonMaskload();
	afx_msg void OnBnClickedButtonMaskalign();
	afx_msg void OnBnClickedButtonMaskmeasureStart();
	afx_msg void OnBnClickedButtonMaskunload();
	afx_msg void OnEnChangeEditScanRepeatNumber();
	afx_msg void OnBnClickedButtonMovePosition();
	CEdit m_EditStepSizeCtrl;
	afx_msg void OnBnClickedButtonXyMove();
	CString m_strXMovePos;
	CString m_strYMovePos;
	afx_msg void OnEnChangeEditXMovepos();
	afx_msg void OnEnChangeEditYMovepos();
	afx_msg void OnBnClickedButtonZoomreset();
	CEdit m_EditStagePosXCtrl;
	CEdit m_EditStagePosYCtrl;
	
	CButton m_CheckOMAdamCtrl;
	BOOL m_bOMDisplay;
	void ChangeOMEUVSet(int direction);

	BOOL m_bAutoSequenceProcessing;
	BOOL m_bOMAlignComplete;
	BOOL m_bEUVAlignComplete;

	int MaskAlign_OM(BOOL bManual);
	int MaskAlign_EUV_SREM(BOOL bAuto);	
		
	int AutoRun(int nRepeatNo);
	
	int PtrProcess();	
	int PtrScanProcess();
	int MaskLoad();
	int MaskUnload();

	BOOL Is_SEQ_Working() { return m_bAutoSequenceProcessing; }

	double m_dOMAlignPointLB_posx_mm, m_dOMAlignPointLB_posy_mm;
	double m_dOMAlignPointLT_posx_mm, m_dOMAlignPointLT_posy_mm;
	double m_dOMAlignPointRT_posx_mm, m_dOMAlignPointRT_posy_mm;
	double m_dEUVAlignPointLB_posx_mm, m_dEUVAlignPointLB_posy_mm;
	double m_dEUVAlignPointLT_posx_mm, m_dEUVAlignPointLT_posy_mm;
	double m_dEUVAlignPointRT_posx_mm, m_dEUVAlignPointRT_posy_mm;
	double m_dNotchAlignPoint1_posx_mm, m_dNotchAlignPoint1_posy_mm;
	double m_dNotchAlignPoint2_posx_mm, m_dNotchAlignPoint2_posy_mm;
	double m_dEUVNotchAlignPoint1_posx_mm, m_dEUVNotchAlignPoint1_posy_mm;
	double m_dEUVNotchAlignPoint2_posx_mm, m_dEUVNotchAlignPoint2_posy_mm;


	afx_msg void OnBnClickedButtonMaskmeasureAutostart();
	CButton m_bCheckCommentDisplayCtrl;
	afx_msg void OnBnClickedButtonStopAutosequence();
	CEdit m_EditLBAlignPointCoordXCtrl;
	CEdit m_EditLBAlignPointCoordYCtrl;
	CEdit m_EditMaskCenterCoordXCtrl;
	CEdit m_EditMaskCenterCoordYCtrl;
	CEdit m_EditMaskLBCoordXCtrl;
	CEdit m_EditMaskLBCoordYCtrl;
	afx_msg void OnBnClickedCheckMagnification();
	int m_nAutoRunRepeatSetNo;
	int m_nAutoRunRepeatResultNo;

	int m_nMeasurementPointNo;
	double m_dMaskCoordinateX_mm;
	double m_dMaskCoordinateY_mm;
	afx_msg void OnEnChangeEditZThroughfocusHeight();

	BOOL m_bBiDirectionFlag;
	afx_msg void OnBnClickedCheckOmalignonly();
	CButton m_CheckOMAlignOnlyCtrl;
	BOOL m_bOMAlignOnly;
	CButton m_CheckManualAlignCtrl;
	BOOL m_bManualAlign;
	afx_msg void OnBnClickedCheckAlignAutomanual();
	afx_msg void OnStnClickedTextStageposgrid();
	afx_msg void OnBnClickedCheckOmAdamUi();
	CButton m_CheckOMAdamUi;


	/* ADAM - OM - SCAN STAGE CAL  Switch 변수*/
	int m_AdamOmtiltSwitch;
	BOOL m_bMacroSystemRunFlag;
	afx_msg void OnBnClickedCheckMaskmapReverseCoordinate();
	BOOL m_bReverseCoordinate;
	
	vector<double> CenterOfCircumCircle(vector<double> P1, vector<double> P2, vector<double> P3);

	double FindRotation(double x1, double y1, double x2, double y2);

	CStatic m_Icon_NaviStageConnectState;
	afx_msg void OnBnClickedButtonNavigationstageConnect();

	int			m_nTimeSecCount;
	int			m_nTimeMinCount;

	afx_msg void OnBnClickedButtonClearPosition();
	afx_msg void OnBnClickedButtonBackupPosition();
	afx_msg void OnBnClickedMaskmapCheckSetcurrentposition();
	afx_msg void OnBnClickedMaskmapCheckSetselectposition();
	afx_msg void OnCbnEditchangeMaskmapComboCouponnum();
	afx_msg void OnCbnSelchangeMaskmapComboCouponnum();
	afx_msg void OnCbnSelchangeMaskmapComboMeasurenum();
	afx_msg void OnBnClickedMaskmapButtonMeasurebackground();
	afx_msg void OnBnClickedMaskmapButtonMeasurewithoutpellicle();
	afx_msg void OnBnClickedMaskmapButtonMeasurewithpellicle();
	afx_msg void OnBnClickedMaskmapButtonMoveposition();
};
