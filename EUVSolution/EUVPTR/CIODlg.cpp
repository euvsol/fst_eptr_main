﻿// CCrevisIODlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

#define IO_INPUT_PART_DIALOG_WIDTH		750
#define IO_INPUT_PART_DIALOG_HEIGHT		1200
#define IO_OUTPUT_PART_DIALOG_WIDTH		800
#define IO_OUTPUT_PART_DIALOG_HEIGHT	1200

// CCrevisIODlg 대화 상자

IMPLEMENT_DYNAMIC(CIODlg, CDialogEx)

CIODlg::CIODlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IO_DIALOG, pParent)
{
	m_pInputData  = NULL;
	m_pOutputData = NULL;
	m_pDlgDi	  = NULL;
	m_pDlgDo	  = NULL;
	m_pDlgAi	  = NULL;
	m_pDlgAo	  = NULL;
	m_pThread	  = NULL;

	m_bThreadExitFlag = FALSE;

	CreateEventHandle();

}

CIODlg::~CIODlg()
{
	m_StrReturnValue.Empty();
	m_bThreadExitFlag = TRUE;
	
	m_brush.DeleteObject();
	m_font.DeleteObject();


	CloseEventHandle();

	if (m_pThread != NULL)
	{
		HANDLE threadHandle = m_pThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/3000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pThread = NULL;
	}


	if (m_pInputData)
		free(m_pInputData);
	if (m_pOutputData)
		free(m_pOutputData);


	if (m_pDlgDi != NULL)
	{
		m_pDlgDi->DestroyWindow();
		delete[] m_pDlgDi;
		m_pDlgDi = NULL;
	}

	if (m_pDlgDo != NULL)
	{
		m_pDlgDo->DestroyWindow();
		delete[] m_pDlgDo;
		m_pDlgDo = NULL;
	}

	if (m_pDlgAi != NULL)
	{
		m_pDlgAi->DestroyWindow();
		delete[] m_pDlgAi;
		m_pDlgAi = NULL;
	}

	if (m_pDlgAo != NULL)
	{
		m_pDlgAo->DestroyWindow();
		delete[] m_pDlgAo;
		m_pDlgAo = NULL;
	}

	if (m_pDlgDoPart != NULL)
	{
		m_pDlgDoPart->DestroyWindow();
		delete[] m_pDlgDoPart;
		m_pDlgDoPart = NULL;
	}

	if (m_pDlgDiPart != NULL)
	{
		m_pDlgDiPart->DestroyWindow();
		delete[] m_pDlgDiPart;
		m_pDlgDiPart = NULL;
	}

	if (m_pDlgDiLinePart != NULL)
	{
		m_pDlgDiLinePart->DestroyWindow();
		delete[] m_pDlgDiLinePart;
		m_pDlgDiLinePart = NULL;
	}

	if (m_pDlgDoLinePart != NULL)
	{
		m_pDlgDoLinePart->DestroyWindow();
		delete[] m_pDlgDoLinePart;
		m_pDlgDoLinePart = NULL;
	}

	StopIoUpdate();
	CloseDevice();
}

void CIODlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_DIGITAL_IN, m_TabCtrlDi);
	DDX_Control(pDX, IDC_TAB_DIGITAL_OUT, m_TabCtrlDo);
}


BEGIN_MESSAGE_MAP(CIODlg, CDialogEx)
	ON_WM_DESTROY()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_DIGITAL_IN, &CIODlg::OnSelchangeTabDigitalIn)
	ON_NOTIFY(TCN_SELCHANGING, IDC_TAB_DIGITAL_IN, &CIODlg::OnSelchangingTabDigitalIn)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_DIGITAL_OUT, &CIODlg::OnSelchangeTabDigitalOut)
	ON_NOTIFY(TCN_SELCHANGING, IDC_TAB_DIGITAL_OUT, &CIODlg::OnSelchangingTabDigitalOut)
	ON_BN_CLICKED(IDC_BTN_ANALOG, &CIODlg::OnBnClickedBtnAnalog)
	ON_BN_CLICKED(IDC_BTN_DIGITAL, &CIODlg::OnBnClickedBtnDigital)
	ON_BN_CLICKED(IDC_BTN_MAINT, &CIODlg::OnBnClickedBtnMaint)
	ON_BN_CLICKED(IDC_BTN_OPER, &CIODlg::OnBnClickedBtnOper)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_LINE, &CIODlg::OnBnClickedBtnLine)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CCrevisIODlg 메시지 처리기


BOOL CIODlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CIODlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_bLLCAtmFlag = false;
	m_bMCAtmFlag = false;

	for (int nIdx = 0; nIdx < DIGITAL_INPUT_NUMBER; nIdx++)
	{
		m_bDigitalIn[nIdx] = false;
	}

	for (int nIdx = 0; nIdx < DIGITAL_OUTPUT_NUMBER; nIdx++)
	{
		m_bDigitalOut[nIdx] = false;
	}

	m_brush.CreateSolidBrush((RGB(255, 234, 234))); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));

	m_TabCtrlDi.SetItemSize(CSize(0, 40));
	m_TabCtrlDo.SetItemSize(CSize(0, 40));

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	this->SetWindowPos(NULL, 0, 0, 1550, 1900, SWP_NOZORDER);

	GetDlgItem(IDC_BTN_ANALOG)->EnableActiveAccessibility();
	GetDlgItem(IDC_BTN_ANALOG)->EnableWindow(false);
	GetDlgItem(IDC_BTN_DIGITAL)->EnableWindow(true);

	GetDlgItem(IDC_BTN_OPER)->EnableActiveAccessibility();
	GetDlgItem(IDC_BTN_OPER)->EnableWindow(false);
	GetDlgItem(IDC_BTN_MAINT)->EnableWindow(true);

	GetDlgItem(IDC_STATIC_TEMP_MONITOR_AC)->SetFont(&m_font);
	GetDlgItem(IDC_STATIC_TEMP_MONITOR_CONTROL)->SetFont(&m_font);


	m_nIOMode = OPER_MODE_ON;
	m_nERROR_MODE = RUN_OFF;

	m_bCrevis_Open_Port = FALSE;

	CreateInputControl(ANALOG_MAIN_TAB);
	CreateOutputControl(ANALOG_MAIN_TAB);

	((CStatic*)GetDlgItem(IDC_CREVIS_OFF))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_CREVIS_ON))->SetIcon(m_LedIcon[0]);

	m_StrReturnValue.Empty();

	m_bAPSequenceNormalCloseFlag = FALSE;
	//SupplyLine();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CIODlg::SupplyLine()
{
	if (Is_Water_Supply_Valve_Open() != VALVE_OPEN)
	{
		if (Open_WaterSupplyValve() != OPERATION_COMPLETED)
		{
			g_pAlarm->SetAlarm((IO_WATER_SUPPLY_WRITE_ERROR));
		}
	}

	if (Is_Water_Return_Valve_Open() != VALVE_OPEN)
	{
		if (Open_WaterReturnValve() != OPERATION_COMPLETED)
		{
			g_pAlarm->SetAlarm((IO_WATER_RETURN_WRITE_ERROR));
		}
	}
}
int CIODlg::OpenDevice(CString strIpAddr)
{
	int nRet = -1;
		
	nRet = CCrevisIOCtrl::OpenDevice(LPSTR(LPCTSTR(strIpAddr)));
	
	if (nRet == 0)
	{
		m_pInputData  = (BYTE*)malloc(GetInputSize());
		m_pOutputData = (BYTE*)malloc(GetOutputSize());

		m_bCrevis_Open_Port = TRUE;

		((CStatic*)GetDlgItem(IDC_CREVIS_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_CREVIS_OFF))->SetIcon(m_LedIcon[0]);
	
		StartIoUpdate();
		
		CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: SREM IO Successfully Connected")));	//통신 상태 기록.
		CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: SREM OPER Mode On ")));				// IO Mode 기록.

		m_pThread = AfxBeginThread(IoCheckThread, (LPVOID)this, THREAD_PRIORITY_NORMAL, 0, 0);
		
		SetTimer(IO_UPDATE_TIMER, 100, NULL);
	}
	else
	{
		m_bCrevis_Open_Port = FALSE;
		
		CECommon::SaveLogFile("SREM__IO__", _T((LPSTR)(LPCTSTR)("IO Connection Failed")));							//통신 상태 기록.
		
		((CStatic*)GetDlgItem(IDC_CREVIS_OFF))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_CREVIS_ON))->SetIcon(m_LedIcon[0]);
		
		nRet = -87001;
		g_pAlarm->SetAlarm((nRet));
	}

	return nRet;
}

void CIODlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == IO_UPDATE_TIMER)
	{
		if (!m_bThreadExitFlag)
		{
			//Open_SourceGate_Check();
			Check_IO_Status();
			SetTimer(IO_UPDATE_TIMER, 500, NULL);
		}
	}


	__super::OnTimer(nIDEvent);
}

void CIODlg::CreateEventHandle()
{
	m_SignalTowerRed_On_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SignalTowerYello_On_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SignalTowerGreen_On_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SignalTowerBuzzer_1_On_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SignalTowerBuzzer_2_On_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCSetPointATM_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCSetPointVAC_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCSetPointATM_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCSetPointVAC_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCSetPointATM_WriteEvent_Close = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCSetPointVAC_WriteEvent_Close = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCSetPointATM_WriteEvent_Close = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCSetPointVAC_WriteEvent_Close = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_FastVent_Inlet_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	//m_Source_Gate_Open_On = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_TRGate_Vauccum_Value_On = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCGate_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_TRGate_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SlowVent_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCForelineGate_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCTmpGate_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCFastRough_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCSlowRough_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCForelineGate_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCTmpGate_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCFastRough_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCSlowRough_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_FastVent_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_FastVent_Inlet_WriteEvent_Close = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SlowVent_Inlet_WriteEvent_Close = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SlowVent_Inlet_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCGate_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_TRGate_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SlowVent_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCForelineGate_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCTmpGate_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCFastRough_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LLCSlowRough_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCForelineGate_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCTmpGate_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCFastRough_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_MCSlowRough_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_FastVent_WriteEvent_Open = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_WaterSupply_On_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_WaterReturn_On_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_DVRCam_1_On_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_DVRCam_3_On_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_DVRCam_2_On_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_DVRCam_4_On_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LlcDryPump_On_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_McDryPump_On_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SignalTowerRed_Off_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SignalTowerYello_Off_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SignalTowerGreen_Off_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SignalTowerBuzzer_1_Off_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_SignalTowerBuzzer_2_Off_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_WaterSupply_Off_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_WaterReturn_Off_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_DVRCam_1_Off_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_DVRCam_2_Off_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_DVRCam_3_Off_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_DVRCam_4_Off_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_LlcDryPump_Off_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_McDryPump_Off_WriteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
}
void CIODlg::CloseEventHandle()
{
	CloseHandle(m_SignalTowerRed_Off_WriteEvent);
	CloseHandle(m_SignalTowerYello_Off_WriteEvent);
	CloseHandle(m_SignalTowerGreen_Off_WriteEvent);
	CloseHandle(m_SignalTowerBuzzer_1_Off_WriteEvent);
	CloseHandle(m_SignalTowerBuzzer_2_Off_WriteEvent);
	CloseHandle(m_WaterSupply_Off_WriteEvent);
	CloseHandle(m_WaterReturn_Off_WriteEvent);
	CloseHandle(m_DVRCam_1_Off_WriteEvent);
	CloseHandle(m_DVRCam_2_Off_WriteEvent);
	CloseHandle(m_DVRCam_3_Off_WriteEvent);
	CloseHandle(m_DVRCam_4_Off_WriteEvent);
	CloseHandle(m_LlcDryPump_Off_WriteEvent);
	CloseHandle(m_McDryPump_Off_WriteEvent);
	CloseHandle(m_FastVent_Inlet_WriteEvent_Open);
	CloseHandle(m_TRGate_Vauccum_Value_On);
	CloseHandle(m_LLCGate_WriteEvent);
	CloseHandle(m_TRGate_WriteEvent);
	CloseHandle(m_SlowVent_WriteEvent);
	CloseHandle(m_LLCForelineGate_WriteEvent);
	CloseHandle(m_LLCTmpGate_WriteEvent);
	CloseHandle(m_LLCFastRough_WriteEvent);
	CloseHandle(m_LLCSlowRough_WriteEvent);
	CloseHandle(m_MCForelineGate_WriteEvent);
	CloseHandle(m_MCTmpGate_WriteEvent);
	CloseHandle(m_MCFastRough_WriteEvent);
	CloseHandle(m_MCSlowRough_WriteEvent);
	CloseHandle(m_FastVent_WriteEvent);
	CloseHandle(m_FastVent_Inlet_WriteEvent_Close);
	CloseHandle(m_SlowVent_Inlet_WriteEvent_Close);
	CloseHandle(m_SlowVent_Inlet_WriteEvent_Open);
	//CloseHandle(m_Source_Gate_Open_On);
	CloseHandle(m_LLCGate_WriteEvent_Open);
	CloseHandle(m_TRGate_WriteEvent_Open);
	CloseHandle(m_SlowVent_WriteEvent_Open);
	CloseHandle(m_LLCForelineGate_WriteEvent_Open);
	CloseHandle(m_LLCTmpGate_WriteEvent_Open);
	CloseHandle(m_LLCFastRough_WriteEvent_Open);
	CloseHandle(m_LLCSlowRough_WriteEvent_Open);
	CloseHandle(m_MCForelineGate_WriteEvent_Open);
	CloseHandle(m_MCTmpGate_WriteEvent_Open);
	CloseHandle(m_MCFastRough_WriteEvent_Open);
	CloseHandle(m_MCSlowRough_WriteEvent_Open);
	CloseHandle(m_FastVent_WriteEvent_Open);
	CloseHandle(m_SignalTowerRed_On_WriteEvent);
	CloseHandle(m_SignalTowerYello_On_WriteEvent);
	CloseHandle(m_SignalTowerGreen_On_WriteEvent);
	CloseHandle(m_SignalTowerBuzzer_1_On_WriteEvent);
	CloseHandle(m_SignalTowerBuzzer_2_On_WriteEvent);
	CloseHandle(m_WaterSupply_On_WriteEvent);
	CloseHandle(m_WaterReturn_On_WriteEvent);
	CloseHandle(m_DVRCam_1_On_WriteEvent);
	CloseHandle(m_DVRCam_2_On_WriteEvent);
	CloseHandle(m_DVRCam_3_On_WriteEvent);
	CloseHandle(m_DVRCam_4_On_WriteEvent);
	CloseHandle(m_LlcDryPump_On_WriteEvent);
	CloseHandle(m_McDryPump_On_WriteEvent);
	CloseHandle(m_LLCSetPointATM_WriteEvent_Open);
	CloseHandle(m_LLCSetPointVAC_WriteEvent_Open);
	CloseHandle(m_MCSetPointATM_WriteEvent_Open);
	CloseHandle(m_MCSetPointVAC_WriteEvent_Open);
	CloseHandle(m_LLCSetPointATM_WriteEvent_Close);
	CloseHandle(m_LLCSetPointVAC_WriteEvent_Close);
	CloseHandle(m_MCSetPointATM_WriteEvent_Close);
	CloseHandle(m_MCSetPointVAC_WriteEvent_Close);
}

UINT CIODlg::IoCheckThread(LPVOID pParam)
{
	CIODlg*  pIoThread = (CIODlg*)pParam;

	while (!pIoThread->m_bThreadExitFlag)
	{
		pIoThread->GetInputIoData();
		pIoThread->GetOutputIoData();
		//pIoThread->Main_Error_Status();
		Sleep(500);
	}

	return 0;
}

void CIODlg::GetInputIoData()
{
	if (GetInputSize() < 0) return;


	int nDecimal = 0;
	  	 
	int nPos_ch1 = 0;
	int nPos_ch2 = 32;
	int nPos_ch3 = 64;
	int nPos_ch4 = 96;
	int nPos_ch5 = 128;


	ReadInputData(m_pInputData);

	int a = GetInputSize();

	for (int nIdx = 0; nIdx < GetInputSize(); nIdx++)
	{
		m_nDecimal_Addr_In[nIdx] = m_pInputData[nIdx];
	}

	// 
	//ANALOG INPUT Module #1
	m_nDecimal_Analog_in_ch_1 = (m_nDecimal_Addr_In[1] << 8) | m_nDecimal_Addr_In[0];
	m_nDecimal_Analog_in_ch_2 = (m_nDecimal_Addr_In[3] << 8) | m_nDecimal_Addr_In[2];
	m_nDecimal_Analog_in_ch_3 = (m_nDecimal_Addr_In[5] << 8) | m_nDecimal_Addr_In[4];
	m_nDecimal_Analog_in_ch_4 = (m_nDecimal_Addr_In[7] << 8) | m_nDecimal_Addr_In[6];
	
	// ANALOG INPUT Module #2
	m_nDecimal_Analog_in_ch_5 = (m_nDecimal_Addr_In[9] << 8) | m_nDecimal_Addr_In[8];
	m_nDecimal_Analog_in_ch_6 = (m_nDecimal_Addr_In[11] << 8) | m_nDecimal_Addr_In[10];
	m_nDecimal_Analog_in_ch_7 = (m_nDecimal_Addr_In[13] << 8) | m_nDecimal_Addr_In[12];
	m_nDecimal_Analog_in_ch_8 = (m_nDecimal_Addr_In[15] << 8) | m_nDecimal_Addr_In[14];
	
	// ANALOG INPUT 온도센서모듈
	m_nDecimal_Analog_in_ch_9 = (m_nDecimal_Addr_In[17] << 8) | m_nDecimal_Addr_In[16];
	m_nDecimal_Analog_in_ch_10 = (m_nDecimal_Addr_In[19] << 8) | m_nDecimal_Addr_In[18];
	m_nDecimal_Analog_in_ch_11 = (m_nDecimal_Addr_In[21] << 8) | m_nDecimal_Addr_In[20];
	m_nDecimal_Analog_in_ch_12 = (m_nDecimal_Addr_In[23] << 8) | m_nDecimal_Addr_In[22];


	//	DIGITAL INPUT Module #1 #2
	m_nDecimal_Digital_in_Module_1 = (m_nDecimal_Addr_In[27] << 24) | (m_nDecimal_Addr_In[26] << 16) | (m_nDecimal_Addr_In[25] << 8) | (m_nDecimal_Addr_In[24]);
	
	//	DIGITAL INPUT Module #3 #4
	m_nDecimal_Digital_in_Module_2 = (m_nDecimal_Addr_In[31] << 24) | (m_nDecimal_Addr_In[30] << 16) | (m_nDecimal_Addr_In[29] << 8) | (m_nDecimal_Addr_In[28]);
	
	//	DIGITAL INPUT Module #5 #6
	m_nDecimal_Digital_in_Module_3 = (m_nDecimal_Addr_In[35] << 24) | (m_nDecimal_Addr_In[34] << 16) | (m_nDecimal_Addr_In[33] << 8) | (m_nDecimal_Addr_In[32]);
	
	//	DIGITAL INPUT Module #7 #8
	m_nDecimal_Digital_in_Module_4 = (m_nDecimal_Addr_In[39] << 24) | (m_nDecimal_Addr_In[38] << 16) | (m_nDecimal_Addr_In[37] << 8) | (m_nDecimal_Addr_In[36]);

	//	DIGITAL INPUT Module #9 #10
	m_nDecimal_Digital_in_Module_5 = (m_nDecimal_Addr_In[43] << 24) | (m_nDecimal_Addr_In[42] << 16) | (m_nDecimal_Addr_In[41] << 8) | (m_nDecimal_Addr_In[40]);


	while (1)
	{
		m_bDigitalIn[nPos_ch1] = m_nDecimal_Digital_in_Module_1 % 2;
		m_bDigitalIn[nPos_ch2] = m_nDecimal_Digital_in_Module_2 % 2;
		m_bDigitalIn[nPos_ch3] = m_nDecimal_Digital_in_Module_3 % 2;
		m_bDigitalIn[nPos_ch4] = m_nDecimal_Digital_in_Module_4 % 2;
		m_bDigitalIn[nPos_ch5] = m_nDecimal_Digital_in_Module_5 % 2;

		m_nDecimal_Digital_in_Module_1 = m_nDecimal_Digital_in_Module_1 / 2;
		m_nDecimal_Digital_in_Module_2 = m_nDecimal_Digital_in_Module_2 / 2;
		m_nDecimal_Digital_in_Module_3 = m_nDecimal_Digital_in_Module_3 / 2;
		m_nDecimal_Digital_in_Module_4 = m_nDecimal_Digital_in_Module_4 / 2;
		m_nDecimal_Digital_in_Module_5 = m_nDecimal_Digital_in_Module_5 / 2;

		nPos_ch1++;
		nPos_ch2++;
		nPos_ch3++;
		nPos_ch4++;
		nPos_ch5++;

		if (nPos_ch1 == 32 && nPos_ch2 == 64 && nPos_ch3 == 96 && nPos_ch4 == 128 && nPos_ch5 == 160) {
		//if (m_nDecimal_Digital_in_Module_1 == 0 && m_nDecimal_Digital_in_Module_2 == 0 && m_nDecimal_Digital_in_Module_3 == 0 && m_nDecimal_Digital_in_Module_4 == 0 && m_nDecimal_Digital_in_Module_5 == 0) {
			break;
		}
	}

	DigitalInputSetHandle();

	// MAIN VALVE CHECK
	//Main_Error_Status();
}

void CIODlg::DigitalInputSetHandle()
{
	//LLC GATE OPEN STATE
	if (m_bDigitalIn[DI::LLC_GATE_VALVE_CLOSE] == false && m_bDigitalIn[DI::LLC_GATE_VALVE_OPEN] == true)	SetEvent(m_LLCGate_WriteEvent_Open);
	// TR GATE OPEN STATE
	if (m_bDigitalIn[DI::TR_GATE_VALVE_CLOSE] == false && m_bDigitalIn[DI::TR_GATE_VALVE_OPEN] == true)	SetEvent(m_TRGate_WriteEvent_Open);
	//// MC FAST ROUGH OPEN STATE
	if (m_bDigitalIn[DI::MC_FAST_ROUGHING_VALVE_OPEN] == true && m_bDigitalIn[DI::MC_ROUGHING_VALVE_CLOSE] == false) SetEvent(m_MCFastRough_WriteEvent_Open);
	//// MC TMP GATE OPEN STATE
	if (m_bDigitalIn[DI::MC_TMP_GATE_VALVE_OPEN] == true && m_bDigitalIn[MC_TMP_GATE_VALVE_CLOSE] == false) SetEvent(m_MCTmpGate_WriteEvent_Open);
	//// MC TMP FORELINE OPEN STATE
	if (m_bDigitalIn[DI::MC_FORELINE_VALVE_OPEN] == true && m_bDigitalIn[MC_FORELINE_VALVE_CLOSE] == false) SetEvent(m_MCForelineGate_WriteEvent_Open);
	//// LLC FAST ROUGH CLOSE STATE
	if (m_bDigitalIn[DI::LLC_FAST_ROUGHING_VALVE_OPEN] == true && m_bDigitalIn[DI::LLC_ROUGHING_VALVE_CLOSE] == false) SetEvent(m_LLCFastRough_WriteEvent_Open);
	////LLC TMP GATE CLOSE STATE
	if (m_bDigitalIn[DI::LLC_TMP_GATE_VALVE_OPEN] == true && m_bDigitalIn[DI::LLC_TMP_GATE_VALVE_CLOSE] == false) SetEvent(m_LLCTmpGate_WriteEvent_Open);
	////LLC FORELINE CLOSE STATE
	if (m_bDigitalIn[DI::LLC_FORELINE_VALVE_OPEN] == true && m_bDigitalIn[DI::LLC_FORELINE_VALVE_CLOSE] == false) SetEvent(m_LLCForelineGate_WriteEvent_Open);
	//LLC GATE CLOSE STATE
	if (m_bDigitalIn[DI::LLC_GATE_VALVE_CLOSE] == true && m_bDigitalIn[DI::LLC_GATE_VALVE_OPEN] == false) SetEvent(m_LLCGate_WriteEvent);
	// TR GATE CLOSE STATE
	if (m_bDigitalIn[DI::TR_GATE_VALVE_CLOSE] == true && m_bDigitalIn[DI::TR_GATE_VALVE_OPEN] == false) SetEvent(m_TRGate_WriteEvent);
	//LLC FORELINE CLOSE STATE
	if (m_bDigitalIn[DI::LLC_FORELINE_VALVE_OPEN] == false && m_bDigitalIn[DI::LLC_FORELINE_VALVE_CLOSE] == true) SetEvent(m_LLCForelineGate_WriteEvent);
	//LLC TMP GATE CLOSE STATE
	if (m_bDigitalIn[DI::LLC_TMP_GATE_VALVE_OPEN] == false && m_bDigitalIn[DI::LLC_TMP_GATE_VALVE_CLOSE] == true) SetEvent(m_LLCTmpGate_WriteEvent);
	// LLC FAST ROUGH CLOSE STATE
	if (m_bDigitalIn[DI::LLC_FAST_ROUGHING_VALVE_OPEN] == false && m_bDigitalIn[DI::LLC_ROUGHING_VALVE_CLOSE] == true) SetEvent(m_LLCFastRough_WriteEvent);
	// MC TMP FORELINE CLOSE STATE
	if (m_bDigitalIn[DI::MC_FORELINE_VALVE_OPEN] == false && m_bDigitalIn[DI::MC_FORELINE_VALVE_CLOSE] == true) SetEvent(m_MCForelineGate_WriteEvent);
	// MC TMP GATE CLOSE STATE
	if (m_bDigitalIn[DI::MC_TMP_GATE_VALVE_OPEN] == false && m_bDigitalIn[DI::MC_TMP_GATE_VALVE_CLOSE] == true) SetEvent(m_MCTmpGate_WriteEvent);
	// MC FAST ROUGH CLOSE STATE
	if (m_bDigitalIn[DI::MC_FAST_ROUGHING_VALVE_OPEN] == false && m_bDigitalIn[DI::MC_ROUGHING_VALVE_CLOSE] == true) SetEvent(m_MCFastRough_WriteEvent);

	if (Is_LLC_Atm_Check())
	{
		if (m_bDigitalOut[DO::LLC_SET_POINT_OUT_1_ATM] != true)
			On_LLCSetPoint_ATM();
	}
	else 
	{
		if (m_bDigitalOut[DO::LLC_SET_POINT_OUT_1_ATM] != false)
			Off_LLCSetPoint_ATM();
	}
	
	if (Is_LLC_Vac_Check())
	{
		if (m_bDigitalOut[DO::LLC_SET_POINT_OUT_2_VAC] != true)
			On_LLCSetPoint_VAC();
	}
	else 
	{
		if (m_bDigitalOut[DO::LLC_SET_POINT_OUT_2_VAC] != false)
			Off_LLCSetPoint_VAC();
	}


	if (Is_MC_Atm_Check())
	{
		if (m_bDigitalOut[DO::MC_SET_POINT_OUT_1_ATM] != true)
			On_MCSetPoint_ATM();
	}
	else
	{
		if (m_bDigitalOut[DO::MC_SET_POINT_OUT_1_ATM] != false)
			Off_MCSetPoint_ATM();

	}
	
	if (Is_MC_Vac_Check())
	{
		if (m_bDigitalOut[DO::MC_SET_POINT_OUT_2_VAC] != true)
			On_MCSetPoint_VAC();
	}
	else
	{
		if (m_bDigitalOut[DO::MC_SET_POINT_OUT_2_VAC] != false)
			Off_MCSetPoint_VAC();
	}

}

void CIODlg::GetOutputIoData()
{
	if (GetOutputSize() < 0) return;

	int nDecimal = 0;
	int nPos = 0;

	int nPos_ch1 = 0;
	int nPos_ch2 = 16;
	int nPos_ch3 = 32;
	int nPos_ch4 = 48;

	int a = ReadOutputData(m_pOutputData);

	for (int nIdx = 0; nIdx < GetOutputSize(); nIdx++)
	{
		m_nDecimal_Addr_Out[nIdx] = m_pOutputData[nIdx];
	}

	//ANALOG_OUTPUT Module #1
	m_nDecimal_Analog_out_ch_1 = (m_nDecimal_Addr_Out[1] << 8) | m_nDecimal_Addr_Out[0];
	m_nDecimal_Analog_out_ch_2 = (m_nDecimal_Addr_Out[3] << 8) | m_nDecimal_Addr_Out[2];
	m_nDecimal_Analog_out_ch_3 = (m_nDecimal_Addr_Out[5] << 8) | m_nDecimal_Addr_Out[4];
	m_nDecimal_Analog_out_ch_4 = (m_nDecimal_Addr_Out[7] << 8) | m_nDecimal_Addr_Out[6];

	//ANALOG_OUTPUT Module #2
	m_nDecimal_Analog_out_ch_5 = (m_nDecimal_Addr_Out[9] << 8) | m_nDecimal_Addr_Out[8];
	m_nDecimal_Analog_out_ch_6 = (m_nDecimal_Addr_Out[11] << 8) | m_nDecimal_Addr_Out[10];
	m_nDecimal_Analog_out_ch_7 = (m_nDecimal_Addr_Out[13] << 8) | m_nDecimal_Addr_Out[12];
	m_nDecimal_Analog_out_ch_8 = (m_nDecimal_Addr_Out[15] << 8) | m_nDecimal_Addr_Out[14];

	//DIGITAL_OUTPUT
	m_nDecimal_Digital_out_Module_1 = (m_nDecimal_Addr_Out[17] << 8) | (m_nDecimal_Addr_Out[16]);
	m_nDecimal_Digital_out_Module_2 = (m_nDecimal_Addr_Out[19] << 8) | (m_nDecimal_Addr_Out[18]);
	m_nDecimal_Digital_out_Module_3 = (m_nDecimal_Addr_Out[21] << 8) | (m_nDecimal_Addr_Out[20]);
	m_nDecimal_Digital_out_Module_4 = (m_nDecimal_Addr_Out[23] << 8) | (m_nDecimal_Addr_Out[22]);
	//nDecimal_Digital_in_Module_4 = (nDecimal_Addr_In[36] << 24) | (nDecimal_Addr_In[37] << 16) | (nDecimal_Addr_In[38] << 8) | (nDecimal_Addr_In[39]);



	while (1)
	{
		
		m_bDigitalOut[nPos_ch1] = m_nDecimal_Digital_out_Module_1 % 2;
		m_bDigitalOut[nPos_ch2] = m_nDecimal_Digital_out_Module_2 % 2;
		m_bDigitalOut[nPos_ch3] = m_nDecimal_Digital_out_Module_3 % 2;
		m_bDigitalOut[nPos_ch4] = m_nDecimal_Digital_out_Module_4 % 2;

		m_nDecimal_Digital_out_Module_1 = m_nDecimal_Digital_out_Module_1 / 2;
		m_nDecimal_Digital_out_Module_2 = m_nDecimal_Digital_out_Module_2 / 2;
		m_nDecimal_Digital_out_Module_3 = m_nDecimal_Digital_out_Module_3 / 2;
		m_nDecimal_Digital_out_Module_4 = m_nDecimal_Digital_out_Module_4 / 2;

		nPos_ch1++;
		nPos_ch2++;
		nPos_ch3++;
		nPos_ch4++;
	
		//if (nDecimal_Digital_out_Module_1 == 0 && nDecimal_Digital_out_Module_2 == 0 && nDecimal_Digital_out_Module_3 == 0) {
		//	break;
		//}

		if (nPos_ch1 == 16 && nPos_ch2 == 32 && nPos_ch3 == 48 && nPos_ch4 == 64)
			break;

	}

	DigitalOutputSetHandle();
	
	///////////////////////////////////////////////////
	//CAMSENSOR Trigger On/Off Force input
	//////////////////////////////////////////////////
	//if(m_bCamsensor_Trigger_On) 
	//	m_bDigitalOut[CAM_SENSOR_TRIGGER] = true;
	//else 
	//	m_bDigitalOut[CAM_SENSOR_TRIGGER] = false;


}

void CIODlg::DigitalOutputSetHandle()
{

	/* TOWER RED */
	if (m_bDigitalOut[DO::SIGNAL_TOWER_RED] == true) SetEvent(m_SignalTowerRed_On_WriteEvent);
	else if (m_bDigitalOut[DO::SIGNAL_TOWER_RED] == false) SetEvent(m_SignalTowerRed_Off_WriteEvent);
	/* TOWER YELLO */
	if (m_bDigitalOut[DO::SIGNAL_TOWER_YELLOW] == true) SetEvent(m_SignalTowerYello_On_WriteEvent);
	else if (m_bDigitalOut[DO::SIGNAL_TOWER_YELLOW] == false) SetEvent(m_SignalTowerYello_Off_WriteEvent);
	/* TOWER GREEN */
	if (m_bDigitalOut[DO::SIGNAL_TOWER_GREEN] == true) SetEvent(m_SignalTowerGreen_On_WriteEvent);
	else if (m_bDigitalOut[DO::SIGNAL_TOWER_GREEN] == false) SetEvent(m_SignalTowerGreen_Off_WriteEvent);
	/* TOWER BUZZER #1 */
	if (m_bDigitalOut[DO::SIGNAL_TOWER_BUZZER_1] == true) SetEvent(m_SignalTowerBuzzer_1_On_WriteEvent);
	else if (m_bDigitalOut[DO::SIGNAL_TOWER_BUZZER_1] == false) SetEvent(m_SignalTowerBuzzer_1_Off_WriteEvent);
	/* TOWER BUZZER #2 */
	if (m_bDigitalOut[DO::SIGNAL_TOWER_BUZZER_2] == true) SetEvent(m_SignalTowerBuzzer_2_On_WriteEvent);
	else if (m_bDigitalOut[DO::SIGNAL_TOWER_BUZZER_2] == false) SetEvent(m_SignalTowerBuzzer_2_Off_WriteEvent);
	/* WATER SUPPLY */
	if (m_bDigitalOut[DO::WATER_SUPPLY_VALVE] == true) SetEvent(m_WaterSupply_On_WriteEvent);
	else if (m_bDigitalOut[DO::WATER_SUPPLY_VALVE] == false) SetEvent(m_WaterSupply_Off_WriteEvent);
	/* WATER RETURN */
	if (m_bDigitalOut[DO::WATER_RETURN_VALVE] == true) SetEvent(m_WaterReturn_On_WriteEvent);
	else if (m_bDigitalOut[DO::WATER_RETURN_VALVE] == false) SetEvent(m_WaterReturn_Off_WriteEvent);
	/* DVR_1 */
	if (m_bDigitalOut[DO::DVR_CAM1_SW] == true) SetEvent(m_DVRCam_1_On_WriteEvent);
	else if (m_bDigitalOut[DO::DVR_CAM1_SW] == false) SetEvent(m_DVRCam_1_Off_WriteEvent);
	/* DVR_2 */
	if (m_bDigitalOut[DO::DVR_CAM2_SW] == true) SetEvent(m_DVRCam_2_On_WriteEvent);
	else if (m_bDigitalOut[DO::DVR_CAM2_SW] == false) SetEvent(m_DVRCam_2_Off_WriteEvent);
	/* DVR_3 */
	if (m_bDigitalOut[DO::DVR_CAM3_SW] == true) SetEvent(m_DVRCam_3_On_WriteEvent);
	else if (m_bDigitalOut[DO::DVR_CAM3_SW] == false) SetEvent(m_DVRCam_3_Off_WriteEvent);
	/* DVR_4 */
	if (m_bDigitalOut[DO::DVR_CAM4_SW] == true) SetEvent(m_DVRCam_4_On_WriteEvent);
	else if (m_bDigitalOut[DO::DVR_CAM4_SW] == false) SetEvent(m_DVRCam_4_Off_WriteEvent);
	/* LLC DRY PUMP */
	if (m_bDigitalOut[DO::LLC_DRY_PUMP_SW] == true) SetEvent(m_LlcDryPump_On_WriteEvent);
	else if (m_bDigitalOut[DO::LLC_DRY_PUMP_SW] == false) SetEvent(m_LlcDryPump_Off_WriteEvent);
	/* MC DRY PUMP */
	if (m_bDigitalOut[DO::MC_DRY_PUMP_SW] == true) SetEvent(m_McDryPump_On_WriteEvent);
	else if (m_bDigitalOut[DO::MC_DRY_PUMP_SW] == false) SetEvent(m_McDryPump_Off_WriteEvent);
	/* LLC SET POINT ATM */
	if (m_bDigitalOut[DO::LLC_SET_POINT_OUT_1_ATM] == true) SetEvent(m_LLCSetPointATM_WriteEvent_Open);
	else if (m_bDigitalOut[DO::LLC_SET_POINT_OUT_1_ATM] == false) SetEvent(m_LLCSetPointATM_WriteEvent_Close);
	/* LLC SET POINT VAC */
	if (m_bDigitalOut[DO::LLC_SET_POINT_OUT_2_VAC] == true) SetEvent(m_LLCSetPointVAC_WriteEvent_Open);
	else if (m_bDigitalOut[DO::LLC_SET_POINT_OUT_2_VAC] == false) SetEvent(m_LLCSetPointVAC_WriteEvent_Close);
	/* MC SET POINT ATM */
	if (m_bDigitalOut[DO::MC_SET_POINT_OUT_1_ATM] == true) SetEvent(m_MCSetPointATM_WriteEvent_Open);
	else if (m_bDigitalOut[DO::MC_SET_POINT_OUT_1_ATM] == false) SetEvent(m_MCSetPointATM_WriteEvent_Close);
	/* MC SET POINT VAC */
	if (m_bDigitalOut[DO::MC_SET_POINT_OUT_2_VAC] == true) SetEvent(m_MCSetPointVAC_WriteEvent_Open);
	else if (m_bDigitalOut[DO::MC_SET_POINT_OUT_2_VAC] == false) SetEvent(m_MCSetPointVAC_WriteEvent_Close);

	//// FAST VENT OPEN STATE
	if (m_bDigitalOut[DO::FAST_VENT_INLET] == true && m_bDigitalOut[DO::FAST_VENT_OUTLET] == true) SetEvent(m_FastVent_WriteEvent_Open);
	//// SLOWVENT OPEN STATE
	if (m_bDigitalOut[DO::SLOW_VENT_INLET] == true && m_bDigitalOut[DO::SLOW_VENT_OUTLET] == true) SetEvent(m_SlowVent_WriteEvent_Open);
	//// MC SLOW ROUGH OPEN STATE
	if (m_bDigitalOut[DO::MC_SLOW_ROUGHING] == true) SetEvent(m_MCSlowRough_WriteEvent_Open);
	//// LLC SLOW ROUGH OPEN STATE
	if (m_bDigitalOut[DO::LLC_SLOW_ROUGHING] == true) SetEvent(m_LLCSlowRough_WriteEvent_Open);
	// MC SLOW ROUGH CLOSE STATE
	if (m_bDigitalOut[DO::MC_SLOW_ROUGHING] == false) SetEvent(m_MCSlowRough_WriteEvent);
	// FAST VENT CLOSE STATE
	if (m_bDigitalOut[DO::FAST_VENT_INLET] == false && m_bDigitalOut[DO::FAST_VENT_OUTLET] == false) SetEvent(m_FastVent_WriteEvent);
	// LLC SLOW ROUGH CLOSE STATE
	if (m_bDigitalOut[DO::LLC_SLOW_ROUGHING] == false) SetEvent(m_LLCSlowRough_WriteEvent);
	//	SLOWVENT CLOSE STATE
	if (m_bDigitalOut[DO::SLOW_VENT_INLET] == false && m_bDigitalOut[DO::SLOW_VENT_OUTLET] == false)	SetEvent(m_SlowVent_WriteEvent);
	// FAST VENT INLET OPEN STATE
	if (m_bDigitalOut[DO::FAST_VENT_INLET] == true) SetEvent(m_FastVent_Inlet_WriteEvent_Open);
	// FAST VENT INLET CLOSE STATE
	if (m_bDigitalOut[DO::FAST_VENT_INLET] == false) SetEvent(m_FastVent_Inlet_WriteEvent_Close);
	// SLOW VENT INLET OPEN STATE
	if (m_bDigitalOut[DO::SLOW_VENT_INLET] == true) SetEvent(m_SlowVent_Inlet_WriteEvent_Open);
	// SLOW VENT INLET CLOSE STATE
	if (m_bDigitalOut[DO::SLOW_VENT_INLET] == false) SetEvent(m_SlowVent_Inlet_WriteEvent_Close);
}

void CIODlg::OnDestroy()
{
	CDialogEx::OnDestroy();

}

void CIODlg::CreateInputControl(int main_tab)
{
	CString sIndex;


	int nChCount = 0;// (GetInputSize() / 2) + (GetInputSize() % 2);
	m_TabCtrlDi.SetItemSize(CSize(400, 50));
	m_TabCtrlDi.SetWindowPos(NULL, 20, 110, 730, 1750, SWP_SHOWWINDOW);
	CRect TabRect;
	m_TabCtrlDi.GetClientRect(&TabRect);


	nChCount = ANALOG_SUB_TAB;
	m_pDlgAi = new CAnalogInput;
	
	m_pDlgAi->Create(IDD_IO_INPUT_AN_DIALOG, &m_TabCtrlDi);
	m_pDlgAi->SetWindowPos(NULL, 10, 1050, TabRect.Width() - 20, TabRect.Height() - 1090, SWP_SHOWWINDOW);
	m_pDlgAi->InitControls_AI();
	m_pDlgAi->ShowWindow(SW_SHOW);

	nChCount = DIGITAL_SUB_IN_TAB;
	m_pDlgDi = new CDigitalInput[nChCount];
	for (int nIdx = 0; nIdx < nChCount; nIdx++)
	{
		sIndex.Format("%d", nIdx);
		m_TabCtrlDi.InsertItem(nIdx + 1, _T("CH") + sIndex);
		m_pDlgDi[nIdx].Create(IDD_IO_INPUT_DIALOG, &m_TabCtrlDi);
		m_pDlgDi[nIdx].SetWindowPos(NULL, 10, 70, TabRect.Width() - 20, TabRect.Height() - 790, SWP_SHOWWINDOW);
		m_pDlgDi[nIdx].ShowWindow(SW_HIDE);
		m_pDlgDi[nIdx].InitControls_DI(nIdx);
	}

	m_pDlgDi[0].ShowWindow(SW_SHOW);

	m_pDlgDiPart = new CDigitalInputPart;
	m_pDlgDiPart->Create(IDD_IO_PART_INPUT_DIALOG, this);
	m_pDlgDiPart->SetWindowPos(NULL, 10, 150, IO_INPUT_PART_DIALOG_WIDTH, IO_INPUT_PART_DIALOG_HEIGHT, SWP_SHOWWINDOW);
	m_pDlgDiPart->InitControls_DI_PART();
	m_pDlgDiPart->ShowWindow(SW_HIDE);

	m_pDlgDiLinePart = new CDigitalInputLinePart;
	m_pDlgDiLinePart->Create(IDD_IO_LINE_PART_INPUT_DIALOG, this);
	m_pDlgDiLinePart->SetWindowPos(NULL, 10, 150, IO_INPUT_PART_DIALOG_WIDTH, IO_INPUT_PART_DIALOG_HEIGHT + 300, SWP_SHOWWINDOW);
	m_pDlgDiLinePart->InitControls_DI_LINE_PART();
	m_pDlgDiLinePart->ShowWindow(SW_HIDE);

}

void CIODlg::CreateOutputControl(int main_tab)
{
	CString sIndex;
	int nChCount = 0; // = (GetOutputSize() / 2) + (GetOutputSize() % 2);
	m_TabCtrlDo.SetItemSize(CSize(400, 50));
	m_TabCtrlDo.SetWindowPos(NULL, 770, 110, 750, 1750, SWP_SHOWWINDOW);
	CRect TabRect;
	m_TabCtrlDo.GetClientRect(&TabRect);

	m_pDlgAo = new CAnalogOutput;
	m_pDlgAo->Create(IDD_IO_OUTPUT_AN_DIALOG, &m_TabCtrlDo);
	m_pDlgAo->SetWindowPos(NULL, 10, 1050, TabRect.Width() - 20, TabRect.Height() - 1090, SWP_SHOWWINDOW);
	m_pDlgAo->InitControls_AO();
	m_pDlgAo->ShowWindow(SW_SHOW);

	nChCount = DIGITAL_SUB_OUT_TAB;
	m_pDlgDo = new CDigitalOutput[nChCount];
	for (int nIdx = 0; nIdx < nChCount; nIdx++)
	{
		sIndex.Format("%d", nIdx);
		m_TabCtrlDo.InsertItem(nIdx + 1, _T("CH") + sIndex);
		m_pDlgDo[nIdx].Create(IDD_IO_OUTPUT_DIALOG, &m_TabCtrlDo);
		m_pDlgDo[nIdx].SetWindowPos(NULL, 10, 70, TabRect.Width() - 20, TabRect.Height() - 790, SWP_SHOWWINDOW);
		m_pDlgDo[nIdx].ShowWindow(SW_HIDE);
		m_pDlgDo[nIdx].InitControls_DO(nIdx);
	}
	m_pDlgDo[0].ShowWindow(SW_SHOW);

	m_pDlgDoPart = new CDigitalOutputPart;
	m_pDlgDoPart->Create(IDD_IO_PART_OUTPUT_DIALOG, this);
	m_pDlgDoPart->SetWindowPos(NULL, 750, 150, IO_OUTPUT_PART_DIALOG_WIDTH, IO_OUTPUT_PART_DIALOG_HEIGHT, SWP_SHOWWINDOW);
	m_pDlgDoPart->InitControls_DO_PART();
	m_pDlgDoPart->ShowWindow(SW_HIDE);

	m_pDlgDoLinePart = new CDigitalOutputLinePart;
	m_pDlgDoLinePart->Create(IDD_IO_LINE_PART_OUTPUT_DIALOG, this);
	m_pDlgDoLinePart->SetWindowPos(NULL, 750, 150, IO_OUTPUT_PART_DIALOG_WIDTH, IO_OUTPUT_PART_DIALOG_HEIGHT + 100, SWP_SHOWWINDOW);
	m_pDlgDoLinePart->InitControls_DO_LINE_PART();
	m_pDlgDoLinePart->ShowWindow(SW_HIDE);
}

void CIODlg::OnBnClickedBtnAnalog()
{
	CString sIndex;


	GetDlgItem(IDC_BTN_ANALOG)->EnableActiveAccessibility();
	GetDlgItem(IDC_BTN_ANALOG)->EnableWindow(false);
	GetDlgItem(IDC_BTN_DIGITAL)->EnableWindow(true);
	GetDlgItem(IDC_BTN_LINE)->EnableWindow(true);

	m_pDlgDoPart->ShowWindow(SW_HIDE);
	m_pDlgDiPart->ShowWindow(SW_HIDE);
	m_pDlgDiLinePart->ShowWindow(SW_HIDE);
	m_pDlgDoLinePart->ShowWindow(SW_HIDE);
	m_TabCtrlDi.ShowWindow(SW_SHOW);
	m_TabCtrlDo.ShowWindow(SW_SHOW);


}

void CIODlg::OnBnClickedBtnDigital()
{
	CString sIndex;

	GetDlgItem(IDC_BTN_DIGITAL)->EnableActiveAccessibility();
	GetDlgItem(IDC_BTN_DIGITAL)->EnableWindow(false);
	GetDlgItem(IDC_BTN_ANALOG)->EnableWindow(true);
	GetDlgItem(IDC_BTN_LINE)->EnableWindow(true);

	m_TabCtrlDi.ShowWindow(SW_HIDE);
	m_TabCtrlDo.ShowWindow(SW_HIDE);
	m_pDlgDiLinePart->ShowWindow(SW_HIDE);
	m_pDlgDoLinePart->ShowWindow(SW_HIDE);
	m_pDlgDoPart->ShowWindow(SW_SHOW);
	m_pDlgDiPart->ShowWindow(SW_SHOW);

}

void CIODlg::OnBnClickedBtnLine()
{
	GetDlgItem(IDC_BTN_DIGITAL)->EnableActiveAccessibility();
	GetDlgItem(IDC_BTN_DIGITAL)->EnableWindow(true);
	GetDlgItem(IDC_BTN_ANALOG)->EnableWindow(true);
	GetDlgItem(IDC_BTN_LINE)->EnableWindow(false);

	m_TabCtrlDi.ShowWindow(SW_HIDE);
	m_TabCtrlDo.ShowWindow(SW_HIDE);
	m_pDlgDoPart->ShowWindow(SW_HIDE);
	m_pDlgDiPart->ShowWindow(SW_HIDE);
	m_pDlgDiLinePart->ShowWindow(SW_SHOW);
	m_pDlgDoLinePart->ShowWindow(SW_SHOW);
}

void CIODlg::OnSelchangeTabDigitalIn(NMHDR *pNMHDR, LRESULT *pResult)
{
	int nIdx = m_TabCtrlDi.GetCurSel();

	m_pDlgAi->ShowWindow(SW_SHOW);
	m_pDlgDi[nIdx].ShowWindow(SW_SHOW);

	*pResult = 0;
}

void CIODlg::OnSelchangingTabDigitalIn(NMHDR *pNMHDR, LRESULT *pResult)
{

	int nIdx = m_TabCtrlDi.GetCurSel();

	m_pDlgAi->ShowWindow(SW_SHOW);
	m_pDlgDi[nIdx].ShowWindow(SW_HIDE);
	*pResult = 0;
}

void CIODlg::OnSelchangeTabDigitalOut(NMHDR *pNMHDR, LRESULT *pResult)
{
	int nIdx = m_TabCtrlDo.GetCurSel();

	m_pDlgAo->ShowWindow(SW_SHOW);
	m_pDlgDo[nIdx].ShowWindow(SW_SHOW);
	*pResult = 0;
}

void CIODlg::OnSelchangingTabDigitalOut(NMHDR *pNMHDR, LRESULT *pResult)
{
	int nIdx = m_TabCtrlDo.GetCurSel();
	m_pDlgAo->ShowWindow(SW_SHOW);
	m_pDlgDo[nIdx].ShowWindow(SW_HIDE);

	*pResult = 0;
}

int CIODlg::DigitalWriteIO(int IODATA, int OnOffFlag)
{
	int nRet = -1;
	int ADDR, bitdex, data;

	if (IODATA <= DO::WATER_RETURN_VALVE)
	{
		ADDR = 16;
		bitdex = IODATA;
	}
	else if ((IODATA >= DO::DigitalOutput_8) && (IODATA <= DO::DigitalOutput_15))
	{
		ADDR = 17;
		bitdex = IODATA - 8;
	}
	else if ((IODATA >= DO::LLC_DRY_PUMP_SW) && (IODATA <= DO::DigitalOutput_23))
	{
		ADDR = 18;
		bitdex = IODATA - 16;
	}
	else if ((IODATA >= DO::MC_SLOW_ROUGHING) && (IODATA <= DO::TR_GATE_VALVE_CLOSE_STATUS))
	{
		ADDR = 19;
		bitdex = IODATA - 24;
	}
	else if ((IODATA >= DO::SLOW_VENT_INLET) && (IODATA <= DO::DigitalOutput_39))
	{
		ADDR = 20;
		bitdex = IODATA - 32;

	}
	else if ((IODATA >= DO::DigitalOutput_40) && (IODATA <= DO::DigitalOutput_47))
	{
		ADDR = 21;
		bitdex = IODATA - 40;
	}
	else if ((IODATA >= DO::DigitalOutput_48) && (IODATA <= DO::LLC_SET_POINT_OUT_2_VAC))
	{
		ADDR = 22;
		bitdex = IODATA - 48;

	}
	else if ((IODATA >= DO::MC_SET_POINT_OUT_1_ATM) && (IODATA <= DO::DigitalOutput_63))
	{
		ADDR = 23;
		bitdex = IODATA - 56;

	}
	else
	{
		return -999;
	}

	nRet = WriteOutputDataBit(ADDR, bitdex, OnOffFlag);
	return nRet;

}





///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// IO Control Function ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

int CIODlg::Close_SourceGate_Check()
{
	int nAddr  = 13;
	int nIndex = 2;
	int nData  = 0;

	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_SourceGate_Check() : SOURCE GATE OPEN OFF TIME OUT ERROR 발생";
		return CLOSE_SOURCEGATE_CHECK_ERROR;
	}
	
	Log_str = "Open_SourceGate_Check() : SOURCE GATE OPEN OFF 완료";
	return OPERATION_COMPLETED;
}

int CIODlg::Off_SlowMfc()
{
	int nRet = -1;
	unsigned long long pBuffer = 0x00;

	nRet = g_pIO->WriteOutputData(0, &pBuffer, 2);

	return nRet;
}


int CIODlg::Off_FastMfc()
{
	int nRet = -1;
	unsigned long long pBuffer = 0x00;

	nRet = g_pIO->WriteOutputData(2, &pBuffer, 2);

	return nRet;
}

int CIODlg::Open_SourceGate_Check()
{
	int nRet = -1;

	int nAddr  = 0;
	int nIndex = 0;
	int nData  = 0;

	if (Is_SourceGate_OpenOn_Check() == VALVE_OPEN)
	{
		nAddr  = 12;
		nIndex = 6;
		nData  = 1;

		//nRet = WriteOutputDataBit(nAddr, nIndex, nData);
		nRet = DigitalWriteIO(DO::EUV_SHUTTER_CHECK, VALVE_OPEN);
		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Open_SourceGate_Check() : SOURCE GATE OPEN ON TIME OUT ERROR 발생";
			return OPEN_SOURCEGATE_CHECK_ERROR;
			
		}

		Log_str = "Open_SourceGate_Check() : SOURCE GATE OPEN ON 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		nAddr  = 12;
		nIndex = 6;
		nData  = 0;

		nRet = DigitalWriteIO(DO::EUV_SHUTTER_CHECK, VALVE_CLOSE);
		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Open_SourceGate_Check() : SOURCE GATE OPEN OFF TIME OUT ERROR 발생";
			return CLOSE_SOURCEGATE_CHECK_ERROR;
		}

		Log_str = "Open_SourceGate_Check() : SOURCE GATE OPEN OFF 완료";
		return OPERATION_COMPLETED;
	}
}

int CIODlg::On_SignalTowerRed()
{
	// IO MAP : Y00.00
	// Open Set 1 m_bDigitalOut[0] 
	// Crevis Addr 8 , Crevis Bit 0
	
	int nRet = -1;

	ResetEvent(m_SignalTowerRed_On_WriteEvent);
	nRet = DigitalWriteIO(DO::SIGNAL_TOWER_RED, VALVE_OPEN);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;
	
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower Red On 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_SignalTowerRed_On_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower Red On Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower Red On Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}


int CIODlg::Off_SignalTowerRed()
{
	// IO MAP : Y00.00
	// Close Set 0 m_bDigitalOut[0] 
	// Crevis Addr 8 , Crevis Bit 0

	int nRet = -1;

	ResetEvent(m_SignalTowerRed_Off_WriteEvent);
	nRet = DigitalWriteIO(DO::SIGNAL_TOWER_RED, VALVE_CLOSE);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Off_SiganlTowerRed() : Signal Tower Red Off 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_SignalTowerRed_Off_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Off_SiganlTowerRed() : Signal Tower Red Off Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Off_SiganlTowerRed() : Signal Tower Red Off Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}

int CIODlg::Open_CoverDoor()
{
	// IO MAP : Y00.00
	// Open Set 1 m_bDigitalOut[0] 
	// Crevis Addr 8 , Crevis Bit 0

	int nRet = -1;

	int nAddr = 12;
	int nIndex = 5;
	int nData = 1;

	nRet = DigitalWriteIO(DO::FRAME_COVER_DOOR_OPEN, VALVE_OPEN);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_CoverDoor() : Cover Door Open Signal Write Error 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

}


int CIODlg::Close_CoverDoor()
{
	// IO MAP : Y00.00
	// Open Set 1 m_bDigitalOut[0] 
	// Crevis Addr 8 , Crevis Bit 0

	int nRet = -1;

	int nAddr = 12;
	int nIndex = 5;
	int nData = 0;

	nRet = DigitalWriteIO(DO::FRAME_COVER_DOOR_OPEN, VALVE_CLOSE);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_CoverDoor() : Cover Door Open Signal Write Error 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

}

int CIODlg::On_SignalTowerYellow()
{
	// IO MAP : Y00.01
	// Open Set 1 m_bDigitalOut[1] 
	// Crevis Addr 8 , Crevis Bit 1

	int nRet = -1;

	int nAddr = 16;
	int nIndex = 1;
	int nData = 1;

	ResetEvent(m_SignalTowerYello_On_WriteEvent);
	nRet = DigitalWriteIO(DO::SIGNAL_TOWER_YELLOW, VALVE_OPEN);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower Yello On 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_SignalTowerYello_On_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower Yello On Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower Yello On Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}


int CIODlg::Off_SignalTowerYellow()
{
	// IO MAP : Y00.01
	// Close Set 1 m_bDigitalOut[1] 
	// Crevis Addr 8 , Crevis Bit 1

	int nRet = -1;

	int nAddr = 16;
	int nIndex = 1;
	int nData = 0;

	ResetEvent(m_SignalTowerYello_Off_WriteEvent);

	nRet = DigitalWriteIO(DO::SIGNAL_TOWER_YELLOW, VALVE_CLOSE);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Off_SiganlTowerYellow() : Signal Tower Yello Off 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_SignalTowerYello_Off_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Off_SiganlTowerYellow() : Signal Tower Yello Off Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Off_SiganlTowerYellow() : Signal Tower Yello Off Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}


int CIODlg::On_SignalTowerGreen()
{
	// IO MAP : Y00.02
	// Open Set 1 m_bDigitalOut[2] 
	// Crevis Addr 8 , Crevis Bit 2

	int nRet = -1;

	int nAddr = 16;
	int nIndex = 2;
	int nData = 1;

	ResetEvent(m_SignalTowerGreen_On_WriteEvent);
	nRet = DigitalWriteIO(DO::SIGNAL_TOWER_GREEN, VALVE_OPEN);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower Green On 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_SignalTowerGreen_On_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower Green On Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower Green On Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}


int CIODlg::Off_SignalTowerGreen()
{
	// IO MAP : Y00.02
	// clsoe Set 0 m_bDigitalOut[2] 
	// Crevis Addr 8 , Crevis Bit 2

	int nRet = -1;

	int nAddr = 16;
	int nIndex = 2;
	int nData = 0;

	ResetEvent(m_SignalTowerGreen_Off_WriteEvent);
	nRet = DigitalWriteIO(DO::SIGNAL_TOWER_GREEN, VALVE_CLOSE);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Off_SignalTowerGreen() : Signal Tower Green Off 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_SignalTowerGreen_Off_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Off_SignalTowerGreen() : Signal Tower Green Off Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Off_SignalTowerGreen() : Signal Tower Green Off Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}

int CIODlg::On_SignalTowerBuzzer_1()
{
	// IO MAP : Y00.03
	// Open Set 1 m_bDigitalOut[3] 
	// Crevis Addr 8 , Crevis Bit 3

	int nRet = -1;

	ResetEvent(m_SignalTowerBuzzer_1_On_WriteEvent);
	nRet = DigitalWriteIO(DO::SIGNAL_TOWER_BUZZER_1, VALVE_OPEN);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower Blue On 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_SignalTowerBuzzer_1_On_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower Blue On Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower Blue On Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}


int CIODlg::Off_SignalTowerBuzzer_1()
{
	// IO MAP : Y00.03
	// Close Set 0 m_bDigitalOut[3] 
	// Crevis Addr 8 , Crevis Bit 3

	int nRet = -1;

	int nAddr = 16;
	int nIndex = 3;
	int nData = 0;

	ResetEvent(m_SignalTowerBuzzer_1_Off_WriteEvent);
	nRet = DigitalWriteIO(DO::SIGNAL_TOWER_BUZZER_1, VALVE_CLOSE);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Off_SignalTowerBlue() : Signal Tower Blue Off 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_SignalTowerBuzzer_1_Off_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Off_SignalTowerBlue() : Signal Tower Blue Off Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Off_SignalTowerBlue() : Signal Tower Blue Off Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}

int CIODlg::On_SignalTowerBuzzer_2()
{
	// IO MAP : Y00.04
	// Open Set 1 m_bDigitalOut[4] 
	// Crevis Addr 8 , Crevis Bit 4

	int nRet = -1;

	ResetEvent(m_SignalTowerBuzzer_2_On_WriteEvent);
	nRet = DigitalWriteIO(DO::SIGNAL_TOWER_BUZZER_2, VALVE_OPEN);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower buzzer On 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_SignalTowerBuzzer_2_On_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower buzzer On Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "On_SiganlTowerRed() : Signal Tower buzzer On Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}

int CIODlg::On_LLCSetPoint_VAC()
{
	// IO MAP : Y03.07
	// Open Set 1 m_bDigitalOut[55] 
	// Crevis Addr 14 , Crevis Bit 7

	int nRet = -1;

	int nAddr = 14;
	int nIndex = 7;
	int nData = 1;
	double LLC_VacuumValue = g_pGauge_IO->GetLlcVacuumRate();
	//m_dPressure_Rough_End = 0.000009;
	if (LLC_VacuumValue <= g_pConfig->m_dPressure_Rough_End)
	{
		//ResetEvent(m_LLCSetPointVAC_WriteEvent_Open);
		//nRet = WriteOutputDataBit(nAddr, nIndex, nData);
		nRet = DigitalWriteIO(DO::LLC_SET_POINT_OUT_2_VAC, VALVE_OPEN);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = _T("On_LLCSetPoint_VAC() : LLC Set Point (VAC) On 에러 발생");
			return LLCGATEOPEN_WRITEERROR;
		}

		//if (WaitForSingleObject(m_LLCSetPointVAC_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
		//{
		//	Log_str = _T("On_LLCSetPoint_VAC() : LLC Set Point (VAC) On Write 완료");
		//	return OPERATION_COMPLETED;
		//}
		else
		{
			Log_str = _T("On_LLCSetPoint_VAC() : LLC Set Point (VAC) On Error 발생 (TimeOut)");
			return LLCGATEOPEN_TIMEOUTERROR;
		}
	}
}
int CIODlg::On_LLCSetPoint_ATM()
{
	// IO MAP : Y03.06
	// Open Set 1 m_bDigitalOut[54] 
	// Crevis Addr 14 , Crevis Bit 6

	int nRet = -1;

	int nAddr = 14;
	int nIndex = 6;
	int nData = 1;

	double LLC_VacuumValue = g_pGauge_IO->GetLlcVacuumRate();
	//m_dPressure_Rough_End = 0.000009;
	if (LLC_VacuumValue >= 750.0)
	{
		//ResetEvent(m_LLCSetPointATM_WriteEvent_Open);
		//nRet = WriteOutputDataBit(nAddr, nIndex, nData);
		nRet = DigitalWriteIO(DO::LLC_SET_POINT_OUT_1_ATM, VALVE_OPEN);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "On_LLCSetPoint_ATM() : LLC Set Point (ATM) On 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		//if (WaitForSingleObject(m_LLCSetPointATM_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
		//{
		//	Log_str = "On_LLCSetPoint_ATM() :  LLC Set Point (ATM) On Write 완료";
		//	return OPERATION_COMPLETED;
		//}
		else
		{
			Log_str = "On_LLCSetPoint_ATM() :  LLC Set Point (ATM) On Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}
	}
}
int CIODlg::On_MCSetPoint_ATM()
{
	// IO MAP : Y03.08
	// Open Set 1 m_bDigitalOut[56] 
	// Crevis Addr 15 , Crevis Bit 0

	int nRet = -1;

	int nAddr = 15;
	int nIndex = 0;
	int nData = 1;

	double MC_VacuumValue = g_pGauge_IO->GetMcVacuumRate();
	//m_dPressure_Rough_End = 0.000009;
	if (MC_VacuumValue >= 750.0)
	{
		ResetEvent(m_MCSetPointATM_WriteEvent_Open);
		//nRet = WriteOutputDataBit(nAddr, nIndex, nData);
		nRet = DigitalWriteIO(DO::MC_SET_POINT_OUT_1_ATM, VALVE_OPEN);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "On_MCSetPoint_ATM() : MC Set Point (ATM) On 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		if (WaitForSingleObject(m_MCSetPointATM_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
		{
			Log_str = "On_MCSetPoint_ATM() : MC Set Point (ATM) On Write 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "On_MCSetPoint_ATM() :  MC Set Point (ATM) On Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}
	}
}
int CIODlg::On_MCSetPoint_VAC()
{
	// IO MAP : Y03.09
	// Open Set 1 m_bDigitalOut[57] 
	// Crevis Addr 15 , Crevis Bit 1

	int nRet = -1;

	int nAddr = 15;
	int nIndex = 1;
	int nData = 1;

	double MC_VacuumValue = g_pGauge_IO->GetMcVacuumRate();
	//m_dPressure_Rough_End = 0.000009;
	if (MC_VacuumValue <= g_pConfig->m_dPressure_Rough_End)
	{
		//ResetEvent(m_MCSetPointVAC_WriteEvent_Open);
		//nRet = WriteOutputDataBit(nAddr, nIndex, nData);
		nRet = DigitalWriteIO(DO::MC_SET_POINT_OUT_2_VAC, VALVE_OPEN);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "On_MCSetPoint_VAC() : MC Set Point (VAC) On 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		//if (WaitForSingleObject(m_MCSetPointVAC_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
		//{
		//	Log_str = "On_MCSetPoint_VAC() : MC Set Point (VAC) On Write 완료";
		//	return OPERATION_COMPLETED;
		//}
		else
		{
			Log_str = "On_MCSetPoint_VAC() :  MC Set Point (VAC) On Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}
	}
}

int CIODlg::Off_LLCSetPoint_VAC()
{
	// IO MAP : Y03.07
	// Open Set 1 m_bDigitalOut[55] 
	// Crevis Addr 14 , Crevis Bit 7

	int nRet = -1;

	int nAddr = 14;
	int nIndex = 7;
	int nData = 0;
	double LLC_VacuumValue = g_pGauge_IO->GetLlcVacuumRate();
	//m_dPressure_Rough_End = 0.000009;
	if (LLC_VacuumValue >= g_pConfig->m_dPressure_Rough_End)
	{
		//ResetEvent(m_LLCSetPointVAC_WriteEvent_Close);
		//nRet = WriteOutputDataBit(nAddr, nIndex, nData);
		nRet = DigitalWriteIO(DO::LLC_SET_POINT_OUT_2_VAC, VALVE_CLOSE);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = _T("Off_LLCSetPoint_VAC() : LLC Set Point (VAC) Off 에러 발생");
			return LLCGATEOPEN_WRITEERROR;
		}

		//if (WaitForSingleObject(m_LLCSetPointVAC_WriteEvent_Close, 3000) == WAIT_OBJECT_0)
		//{
		//	Log_str = _T("Off_LLCSetPoint_VAC() : LLC Set Point (VAC) Off Write 완료");
		//	return OPERATION_COMPLETED;
		//}
		else
		{
			Log_str = _T("Off_LLCSetPoint_VAC() : LLC Set Point (VAC) Off Error 발생 (TimeOut)");
			return LLCGATEOPEN_TIMEOUTERROR;
		}
	}
}
int CIODlg::Off_LLCSetPoint_ATM()
{
	// IO MAP : Y03.06
	// Open Set 1 m_bDigitalOut[54] 
	// Crevis Addr 14 , Crevis Bit 6

	int nRet = -1;

	int nAddr = 14;
	int nIndex = 6;
	int nData = 0;

	double LLC_VacuumValue = g_pGauge_IO->GetLlcVacuumRate();
	//m_dPressure_Rough_End = 0.000009;
	if (LLC_VacuumValue <= 750.0)
	{
		ResetEvent(m_LLCSetPointATM_WriteEvent_Close);
		//nRet = WriteOutputDataBit(nAddr, nIndex, nData);
		nRet = DigitalWriteIO(DO::LLC_SET_POINT_OUT_1_ATM, VALVE_CLOSE);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Off_LLCSetPoint_ATM() : LLC Set Point (ATM) Off 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		if (WaitForSingleObject(m_LLCSetPointATM_WriteEvent_Close, 3000) == WAIT_OBJECT_0)
		{
			Log_str = "Off_LLCSetPoint_ATM() :  LLC Set Point (ATM) Off Write 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "Off_LLCSetPoint_ATM() :  LLC Set Point (ATM) Off Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}
	}
}
int CIODlg::Off_MCSetPoint_ATM()
{
	// IO MAP : Y03.08
	// Open Set 1 m_bDigitalOut[56] 
	// Crevis Addr 15 , Crevis Bit 0

	int nRet = -1;

	int nAddr = 15;
	int nIndex = 0;
	int nData = 0;

	double MC_VacuumValue = g_pGauge_IO->GetMcVacuumRate();
	//m_dPressure_Rough_End = 0.000009;
	if (MC_VacuumValue <= 750.0)
	{
		//ResetEvent(m_MCSetPointATM_WriteEvent_Close);
		//nRet = WriteOutputDataBit(nAddr, nIndex, nData);
		nRet = DigitalWriteIO(DO::MC_SET_POINT_OUT_1_ATM, VALVE_CLOSE);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Off_MCSetPoint_ATM() : MC Set Point (ATM) Off 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		//if (WaitForSingleObject(m_MCSetPointATM_WriteEvent_Close, 3000) == WAIT_OBJECT_0)
		//{
		//	Log_str = "Off_MCSetPoint_ATM() : MC Set Point (ATM) Off Write 완료";
		//	return OPERATION_COMPLETED;
		//}
		else
		{
			Log_str = "Off_MCSetPoint_ATM() :  MC Set Point (ATM) Off Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}
	}
}
int CIODlg::Off_MCSetPoint_VAC()
{
	// IO MAP : Y03.09
	// Open Set 1 m_bDigitalOut[57] 
	// Crevis Addr 15 , Crevis Bit 1

	int nRet = -1;

	int nAddr = 15;
	int nIndex = 1;
	int nData = 0;

	double MC_VacuumValue = g_pGauge_IO->GetMcVacuumRate();
	//m_dPressure_Rough_End = 0.000009;
	if (MC_VacuumValue >= g_pConfig->m_dPressure_Rough_End)
	{
		//ResetEvent(m_MCSetPointVAC_WriteEvent_Close);
		//nRet = WriteOutputDataBit(nAddr, nIndex, nData);
		nRet = DigitalWriteIO(DO::MC_SET_POINT_OUT_2_VAC, VALVE_CLOSE);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Off_MCSetPoint_VAC() : MC Set Point (VAC) off 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		//if (WaitForSingleObject(m_MCSetPointVAC_WriteEvent_Close, 3000) == WAIT_OBJECT_0)
		//{
		//	Log_str = "Off_MCSetPoint_VAC() : MC Set Point (VAC) off Write 완료";
		//	return OPERATION_COMPLETED;
		//}
		else
		{
			Log_str = "Off_MCSetPoint_VAC() :  MC Set Point (VAC) off Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}
	}
}
int CIODlg::Off_SignalTowerBuzzer_2()
{
	// IO MAP : Y00.04
	// Close Set 1 m_bDigitalOut[4] 
	// Crevis Addr 8 , Crevis Bit 4

	int nRet = -1;

	ResetEvent(m_SignalTowerBuzzer_2_Off_WriteEvent);
	nRet = DigitalWriteIO(DO::SIGNAL_TOWER_BUZZER_2, VALVE_CLOSE);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Off_SignalTowerBuzzer() : Signal Tower buzzer Off 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_SignalTowerBuzzer_2_Off_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Off_SignalTowerBuzzer() : Signal Tower buzzer Off Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Off_SignalTowerBuzzer() : Signal Tower buzzer Off Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}


int CIODlg::Open_WaterSupplyValve()
{
	// IO MAP : Y00.06
	// Open Set 1 m_bDigitalOut[6] 
	// Crevis Addr 8 , Crevis Bit 6

	int nRet = -1;

	int nAddr = 16;
	int nIndex = 6;
	int nData = 1;

	ResetEvent(m_WaterSupply_On_WriteEvent);

	nRet = DigitalWriteIO(DO::WATER_SUPPLY_VALVE, VALVE_OPEN);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_WaterSupplyValve() : Water Supply On 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_WaterSupply_On_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_WaterSupplyValve() : Water Supply On Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_WaterSupplyValve() : Water Supply On Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}

int CIODlg::Close_WaterSupplyValve()
{
	// IO MAP : Y00.06
	// Close Set 0 m_bDigitalOut[6] 
	// Crevis Addr 8 , Crevis Bit 6

	int nRet = -1;

	int nAddr = 16;
	int nIndex = 6;
	int nData = 0;

	ResetEvent(m_WaterSupply_Off_WriteEvent);

	nRet = DigitalWriteIO(DO::WATER_SUPPLY_VALVE, VALVE_CLOSE);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_WaterSupplyValve() : Water Supply Off 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_WaterSupply_Off_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_WaterSupplyValve() : Water Supply Off Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_WaterSupplyValve() : Water Supply Off Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}

int CIODlg::Open_WaterReturnValve()
{
	// IO MAP : Y00.07
	// Open Set 1 m_bDigitalOut[6] 
	// Crevis Addr 8 , Crevis Bit 6

	int nRet = -1;

	int nAddr = 16;
	int nIndex = 7;
	int nData = 1;

	ResetEvent(m_WaterReturn_On_WriteEvent);

	nRet = DigitalWriteIO(DO::WATER_RETURN_VALVE, VALVE_OPEN);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_WaterReturnValve() : Water Return On 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_WaterReturn_On_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_WaterReturnValve() : Water  Return On Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_WaterReturnValve() : Water Return On Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}


int CIODlg::Close_WaterReturnValve()
{
	// IO MAP : Y00.07
	// Open Set 1 m_bDigitalOut[6] 
	// Crevis Addr 8 , Crevis Bit 6

	int nRet = -1;

	int nAddr = 16;
	int nIndex = 7;
	int nData = 0;

	ResetEvent(m_WaterReturn_Off_WriteEvent);

	nRet = DigitalWriteIO(DO::WATER_RETURN_VALVE, VALVE_CLOSE);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = _T("Close_WaterReturnValve() : Water Return Off 에러 발생");
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_WaterReturn_Off_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = _T("Close_WaterReturnValve() : Water  Return Off Write 완료");
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = _T("Close_WaterReturnValve() : Water Return Off Error 발생 (TimeOut)");
		return LLCGATEOPEN_TIMEOUTERROR;
	}

}

int CIODlg::Open_DVR_Valve(int DVRNum)
{
	// IO MAP : Y00.09
	// Open Set 1 m_bDigitalOut[9] 
	// Crevis Addr 9 , Crevis Bit 1

	int nRet = -1;

	int nAddr = 0;
	int nIndex = 0;
	int nData = 1;

	nAddr = 17;

	switch (DVRNum)
	{
	case 1:

		nIndex = 1;
		ResetEvent(m_DVRCam_1_On_WriteEvent);
		nRet = DigitalWriteIO(DO::DVR_CAM1_SW, VALVE_OPEN);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Open_DVR_1_Valve() : DVAR CAM #1 On 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		if (WaitForSingleObject(m_DVRCam_1_On_WriteEvent, 3000) == WAIT_OBJECT_0)
		{
			Log_str = "Open_DVR_1_Valve() : DVAR CAM #1 On Write 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "Open_DVR_1_Valve() : DVAR CAM #1 On Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}
		break;
	case 2:
		nIndex = 2;
		ResetEvent(m_DVRCam_2_On_WriteEvent);
		nRet = DigitalWriteIO(DO::DVR_CAM2_SW, VALVE_OPEN);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Open_DVR_2_Valve() : DVAR CAM #2 On 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		if (WaitForSingleObject(m_DVRCam_2_On_WriteEvent, 3000) == WAIT_OBJECT_0)
		{
			Log_str = "Open_DVR_2_Valve() : DVAR CAM #2 On Write 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "Open_DVR_2_Valve() : DVAR CAM #2 On Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}

		break;
	case 3:
		nIndex = 3;
		ResetEvent(m_DVRCam_3_On_WriteEvent);
		nRet = DigitalWriteIO(DO::DVR_CAM3_SW, VALVE_OPEN);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Open_DVR_3_Valve() : DVAR CAM #3 On 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		if (WaitForSingleObject(m_DVRCam_3_On_WriteEvent, 3000) == WAIT_OBJECT_0)
		{
			Log_str = "Open_DVR_3_Valve() : DVAR CAM #3 On Write 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "Open_DVR_3_Valve() : DVAR CAM #3 On Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}
		break;
	case 4:
		nIndex = 4;
		ResetEvent(m_DVRCam_4_On_WriteEvent);
		nRet = DigitalWriteIO(DO::DVR_CAM4_SW, VALVE_OPEN);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Open_DVR_4_Valve() : DVAR CAM #4 On 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		if (WaitForSingleObject(m_DVRCam_4_On_WriteEvent, 3000) == WAIT_OBJECT_0)
		{
			Log_str = "Open_DVR_4_Valve() : DVAR CAM #4 On Write 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "Open_DVR_4_Valve() : DVAR CAM #4 On Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}
		break;
	default:
		break;
	}

}

int CIODlg::Close_DVR_Valve(int DVRNum)
{
	// IO MAP : Y00.09
	// Open Set 1 m_bDigitalOut[9] 
	// Crevis Addr 9 , Crevis Bit 1

	int nRet = -1;

	int nAddr = 0;
	int nIndex = 0;
	int nData = 0;

	nAddr = 17;

	switch (DVRNum)
	{
	case 1:

		nIndex = 1;
		ResetEvent(m_DVRCam_1_Off_WriteEvent);
		nRet = DigitalWriteIO(DO::DVR_CAM1_SW, VALVE_CLOSE);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Close_DVR_1_Valve() : DVAR CAM #1 Off 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		if (WaitForSingleObject(m_DVRCam_1_Off_WriteEvent, 3000) == WAIT_OBJECT_0)
		{
			Log_str = "Close_DVR_1_Valve() : DVAR CAM #1 Off Write 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "Close_DVR_1_Valve() : DVAR CAM #1 Off Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}
		break;
	case 2:
		nIndex = 2;
		ResetEvent(m_DVRCam_2_Off_WriteEvent);
		nRet = DigitalWriteIO(DO::DVR_CAM2_SW, VALVE_CLOSE);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Close_DVR_2_Valve() : DVAR CAM #2 Off 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		if (WaitForSingleObject(m_DVRCam_2_Off_WriteEvent, 3000) == WAIT_OBJECT_0)
		{
			Log_str = "Close_DVR_2_Valve() : DVAR CAM #2 Off Write 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "Close_DVR_2_Valve() : DVAR CAM #2 Off Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}

		break;
	case 3:
		nIndex = 3;
		ResetEvent(m_DVRCam_3_Off_WriteEvent);
		nRet = DigitalWriteIO(DO::DVR_CAM3_SW, VALVE_CLOSE);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Close_DVR_3_Valve() : DVAR CAM #3 Off 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		if (WaitForSingleObject(m_DVRCam_3_Off_WriteEvent, 3000) == WAIT_OBJECT_0)
		{
			Log_str = "Close_DVR_3_Valve() : DVAR CAM #3 Off Write 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "Close_DVR_3_Valve() : DVAR CAM #3 Off Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}
		break;
	case 4:
		nIndex = 4;
		ResetEvent(m_DVRCam_4_Off_WriteEvent);
		nRet = DigitalWriteIO(DO::DVR_CAM4_SW, VALVE_CLOSE);
		if (!nRet)
			nRet = 0;
		else
			nRet = -1;

		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Close_DVR_4_Valve() : DVAR CAM #4 Off 에러 발생";
			return LLCGATEOPEN_WRITEERROR;
		}

		if (WaitForSingleObject(m_DVRCam_4_Off_WriteEvent, 3000) == WAIT_OBJECT_0)
		{
			Log_str = "Close_DVR_4_Valve() : DVAR CAM #4 Off Write 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "Close_DVR_4_Valve() : DVAR CAM #4 Off Error 발생 (TimeOut)";
			return LLCGATEOPEN_TIMEOUTERROR;
		}
		break;
	default:
		break;
	}

}

int CIODlg::On_LlcDryPump_Valve()
{
	// IO MAP : Y00.16
	// Open Set 1 m_bDigitalOut[16] 
	// Crevis Addr 10 , Crevis Bit 0

	int nRet = -1;

	int nAddr = 18;
	int nIndex = 0;
	int nData = 1;

	ResetEvent(m_LlcDryPump_On_WriteEvent);
	nRet = DigitalWriteIO(DO::LLC_DRY_PUMP_SW, VALVE_OPEN);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "On_LlcDryPump_Valve() : LLC Dry Pump On 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_LlcDryPump_On_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "On_LlcDryPump_Valve() :  LLC Dry Pump On Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "On_LlcDryPump_Valve() :  LLC Dry Pump On Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}
}


int CIODlg::Off_LlcDryPump_Valve()
{
	// IO MAP : Y00.16
	// Open Set 1 m_bDigitalOut[16] 
	// Crevis Addr 10 , Crevis Bit 0

	int nRet = -1;

	int nAddr = 18;
	int nIndex = 0;
	int nData = 0;

	ResetEvent(m_LlcDryPump_Off_WriteEvent);

	nRet = DigitalWriteIO(DO::LLC_DRY_PUMP_SW, VALVE_CLOSE);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Off_LlcDryPump_Valve() : LLC Dry Pump Off 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_LlcDryPump_Off_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Off_LlcDryPump_Valve() :  LLC Dry Pump Off Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Off_LlcDryPump_Valve() :  LLC Dry Pump Off Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}
}

int CIODlg::On_McDryPump_Valve()
{
	// IO MAP : Y00.17
	// Open Set 1 m_bDigitalOut[17] 
	// Crevis Addr 10 , Crevis Bit 1

	int nRet = -1;

	int nAddr = 18;
	int nIndex = 1;
	int nData = 1;

	ResetEvent(m_McDryPump_On_WriteEvent);

	nRet = DigitalWriteIO(DO::MC_DRY_PUMP_SW, VALVE_OPEN);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "On_McDryPump_Valve() : MC Dry Pump On 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_McDryPump_On_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "On_McDryPump_Valve() :  MC Dry Pump On Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "On_McDryPump_Valve() :  MC Dry Pump On Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}
}


int CIODlg::Off_McDryPump_Valve()
{
	// IO MAP : Y00.17
	// Open Set 1 m_bDigitalOut[17] 
	// Crevis Addr 10 , Crevis Bit 1

	int nRet = -1;

	int nAddr = 18;
	int nIndex = 1;
	int nData = 0;

	ResetEvent(m_McDryPump_Off_WriteEvent);

	nRet = DigitalWriteIO(DO::MC_DRY_PUMP_SW, VALVE_CLOSE);
	if (!nRet)
		nRet = 0;
	else
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Off_McDryPump_Valve() : MC Dry Pump Off 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_McDryPump_Off_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Off_McDryPump_Valve() :  MC Dry Pump Off Write 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Off_McDryPump_Valve() :  MC Dry Pump Off Error 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_LLCGateValve() 
{
	//	IO MAP : Y01.12  
	//	OPEN SET 1 m_bDigitalOut[28]

	////////////////////////////////////////
	//    LLC GATE OPEN INTERLOCK        ///
	////////////////////////////////////////
		// @ LLC 대기상태 확인 (41B)
		// @ MFC#1 OFF 확인 
		// @ MFC#2 OFF 확인 
		// @ MFC FAST VENT INLET CLOSE 확인 
		// @ MFC FAST VENT OUTLET CLOSE 확인 
		// @ MFC SLOW VENT INLET CLOSE 확인  
		// @ MFC SLOW VENT OUTLET CLOSE 확인  
		// @ LLC SLOW ROUGHING V/V CLOSE 확인
		// @ LLC FAST ROUGHING V/V CLOSE 확인
		// @ LLC TMP GATE CLOSE 확인 
		// @ LID CLOSE 확인.
		// @ TR GATE CLOSE 확인.

	int nRet = -1;
	int nAddr = 19;
	int nIndex = 5;
	int nData = 0;
	if (Is_LLC_Atm_Check() != TRUE)
	{
		Log_str = " Open_LLCGateValve(): Is_LLC_Atm_Check() = LLC GATE OPEN 시 LLC ATM SENSOR STATUS 에러 발생!";
		return  LLCGATEOPEN_LLCLINEATMSTATUSERROR;
	}
	if (Is_FastVentValve_Open() != VALVE_CLOSE) // MFC FAST VENT CLOSE 확인 
	{
		Log_str = "Open_LLCGateValve() :: Is_FastVentValve_Open() = LLC GATE OPEN 시 FAST VENT CLOSE INTERLOCK 에러";
		return LLCGATEOPEN_FASTVENTCLOSEERROR;
	}
	if (Is_SlowVentValve_Open() != VALVE_CLOSE) //  MFC SLOW VENT CLOSE 확인 
	{
		Log_str = "Open_LLCGateValve() :: Is_SlowVentValve1_Open() = LLC GATE OPEN 시 SLOW VENT CLOSE INTERLOCK 에러";
		return LLCGATEOPEN_SLOWVENTCLOSEERROR;
	}
	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE) // LLC FAST ROUGHING V/V CLOSE 확인
	{
		Log_str = "Open_LLCGateValve() :: Is_LLC_FastRoughValve_Open() = LLC GATE OPEN 시 LLC FAST ROUGH CLOSE INTERLOCK 에러";
		return LLCGATEOPEN_LLCFASTROUGHCLOSEERROR;
	}
	if (Is_LLC_SlowRoughValve_Open() != VALVE_CLOSE) // LLC SLOW ROUGHING V/V CLOSE 확인
	{
		Log_str = "Open_LLCGateValve() :: Is_LLC_SlowRoughValve_Open() = LLC GATE OPEN 시 LLC SLOW ROUGH CLOSE INTERLOCK 에러";
		return LLCGATEOPEN_LLCSLOWROUGHCLOSEERROR;
	}
	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSE) // LLC TMP GATE CLOSE 확인 
	{
		Log_str = "Open_LLCGateValve() :: Is_LLC_TMP_GateValve_Open() = LLC GATE OPEN 시 LLC TMP GATE CLOSE INTERLOCK 에러";
		return LLCGATEOPEN_LLCTMPGATECLOSEERROR;
	}
	if (Is_TRGateValve_Open() != VALVE_CLOSE) // TR GATE CLOSE 확인
	{
		Log_str = "Open_LLCGateValve() :: Is_TRGateValve_Open() = LLC GATE OPEN 시 TR GATE CLOSE INTERLOCK 에러";
		return LLCGATEOPEN_TRGATECLOSEERROR;
	}
	if (Is_LLC_Lid_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLCGateValve() :: Is_LLC_Lid_Open() = MC TMP OPEN OPEN 시 LLC LID CLOSE INTERLOCK 에러";
		return LLCGATEOPEN_LLCLIDCLOSEERROR;
	}
	if (Off_SlowMfc() != 0)
	{
		Log_str = "Open_LLCGateValve() :: Off_SlowMfc() = MC TMP OPEN OPEN 시 Slow MFC 0ff INTERLOCK 에러";
		return LLCGATEOPEN_SLOWMFCCLOSEERROR;
	}
	if (Off_FastMfc() != 0)
	{
		Log_str = "Open_LLCGateValve() :: Off_FastMfc() = MC TMP OPEN OPEN 시 Fast MFC 0ff INTERLOCK 에러";
		return LLCGATEOPEN_FASTMFCCLOSEERROR;
	}
	ResetEvent(m_LLCGate_WriteEvent_Open);

	/* LLC GATE CLOSE OFF */
	nAddr = 19;
	nIndex = 5;
	nData = 0;

	int nRet_1 = DigitalWriteIO(DO::LLC_GATE_VALVE_CLOSE_STATUS, VALVE_CLOSE);

	/* LLC GATE OPEN ON */
	nIndex = 4;
	nData  = 1;

	int nRet_2 = DigitalWriteIO(DO::LLC_GATE_VALVE_OPEN_STATUS, VALVE_OPEN);
		
	if (!nRet_1 && !nRet_2) 
		nRet = 0;
	else 
		nRet = -1;
	m_StrReturnValue.Format(_T("%d"), nRet);

	if (nRet != 0)
	{
		Log_str = "Open_LLCGateValve() : LLC GATE OPEN WRITE 에러 발생";
		return LLCGATEOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCGate_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_LLCGateValve() : LLC GATE OPEN WRITE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_LLCGateValve() : LLC GATE OPEN 에러 발생 (TimeOut)";
		return LLCGATEOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_TRGateValve()  
{
	// IO MAP Y01.14  
	// OPEN SET 1 m_bDigitalOut[30]
	//
	//  진공상태 , 대기상태 TR GATE OPEN 인터락 조건 상이. (2가지 상태에 따른 인터락 별도 구성 필요)
	//
	//	진공상태 인터락 (LLC -> MC)
	//
	//	대기상태 인터락 (메인터넌스 모드일경우에만 해당)
	//

	int nRet   = -1;
	int nAddr  = 0;
	int nIndex = 0;
	int nData  = 0;


	double m_mcPressure = 0.0;
	double m_mcPressureRef = 0.0;
	double m_llcPressure = 0.0;
	
	
	
	
	/* TR Gate 조건

	MC 와 LLC 10^-2 차이 이상 -> FALSE
	MC 와 LLC 10^-2 차이 이하 -> TRUE

	*/
	
	m_llcPressure = (g_pGauge_IO->GetLlcVacuumRate());
	m_mcPressure = (g_pGauge_IO->GetMcVacuumRate());
	m_mcPressureRef = m_mcPressure * 100;




	if ((m_llcPressure < m_mcPressureRef) || (g_pGauge_IO->GetMcVacuumRate() < g_pConfig->m_dPressure_Rough_End) && (g_pGauge_IO->GetLlcVacuumRate() < g_pConfig->m_dPressure_Rough_End))
	{
	
		ResetEvent(m_TRGate_WriteEvent_Open);

		/* TR GATE CLOSE OFF */
		nIndex = 7;
		nData = 0;
		int nRet_OFF = DigitalWriteIO(DO::TR_GATE_VALVE_CLOSE_STATUS, VALVE_CLOSE);

		/* TR GATE OPEN ON */
		nAddr  = 19;
		nIndex = 6;
		nData  = 1;
		int nRet_ON = DigitalWriteIO(DO::TR_GATE_VALVE_OPEN_STATUS, VALVE_OPEN);
		

		if (!nRet_ON && !nRet_OFF) 
			nRet = 0;
		else 
			nRet = -1;
		m_StrReturnValue.Format(_T("%d"), nRet);

		if (nRet != 0)
		{
			Log_str = "Open_TRGateValve() : TR GATE OPEN WRITE 에러 발생";
			return TRGATEOPEN_WRITEERROR;
		}
		
		if (WaitForSingleObject(m_TRGate_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
		{
			Log_str = "Open_TRGateValve() : TR GATE OPEN 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "Open_TRGateValve() : TR GATE OPEN 에러 발생 (TimeOut)";
			return TRGATEOPEN_TIMEOUTERROR;
		}
	}
	else
	{
		Log_str = "Open_TRGateValve() : TR GATE OPEN 에러 발생 ! 가능 진공 범위가 아님 !";
		return TRGATEOPEN_VACUUMRANGEERROR;
	}
}

int CIODlg::Open_SlowVent_Inlet_Valve()
{

	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_LLCGateValve_Open() =  SLOW VENT INLET VALVE OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return SLOWVENTINLETOPEN_LLCGATECLOSEERROR;
	}
	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_LLC_TMP_GateValve_Open() =  SLOW VENT INLET VALVE OPEN 시 LLC TMP GATE CLOSE INTERLOCK 에러";
		return SLOWVENTINLETOPEN_LLCTMPGATECLOSEERROR;
	}
	//if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
	//{
	//	Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_LLC_FastRoughValve_Open() = SLOW VENT INLET VALVE OPEN 시 LLC FAST ROUGH VALVE CLOSE INTERLOCK 에러";
	//	return SLOWVENTINLETOPEN_LLCFASTROUGHCLOSEERROR;
	//}
	if (Is_LLC_SlowRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_LLC_SlowRoughValve_Open() = SLOW VENT INLET VALVE OPEN 시 LLC SLOW ROUGH VALVE CLOSE INTERLOCK 에러";
		return SLOWVENTINLETOPEN_LLCSLOWROUGHCLOSEERROR;
	}
	if (Is_FastVent_Inlet_Valve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_FastVent_Inlet_Valve1_Open() = SLOW VENT INLET VALVE OPEN 시 FAST VENT INLET VALVE CLOSE INTERLOCK 에러";
		return SLOWVENTINLETOPEN_FASTVENTINLETCLOSEERROR;
	}
	if (Is_FastVent_Outlet_Valve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_FastVent_Outlet_Valve1_Open() = SLOW VENT INLET VALVE OPEN 시 FAST VENT OUTLET VALVE CLOSE INTERLOCK 에러";
		return SLOWVENTINLETOPEN_FASTVENTOUTLETCLOSEERROR;
	}
	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		if (Is_MC_TMP_GateValve_Open() != VALVE_CLOSED)
		{
			Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_MC_TMP_GateValve_Open() =  SLOW VENT INLET VALVE OPEN 시 (TR GATE OPEN) LLC TMP GATE CLOSE INTERLOCK 에러";
			return SLOWVENTINLETOPEN__TROPEN_MCTMPGATECLOSEERRO;
		}
		if (Is_MC_FastRoughValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_MC_FastRoughValve_Open() = SLOW VENT INLET VALVE OPEN 시  (TR GATE OPEN) LLC FAST ROUGH VALVE CLOSE INTERLOCK 에러";
			return SLOWVENTINLETOPEN__TROPEN_MCROUGHCLOSEERROR;
		}
		if (Is_MC_SlowRoughValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_MC_SlowRoughValve_Open() = SLOW VENT INLET VALVE OPEN 시 (TR GATE OPEN) LLC SLOW ROUGH VALVE CLOSE INTERLOCK 에러";
			return SLOWVENTINLETOPEN__TROPEN_MCROUGHCLOSEERROR;
		}
		if (Is_LaserSource_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_SlowVent_Inlet_Valve1() :: Is_LaserSource_Open() = SLOW VENT INLET VALVE OPEN 시 (TR GATE OPEN) LLC SLOW ROUGH VALVE CLOSE INTERLOCK 에러";
			return SLOWVENTINLETOPEN__TROPEN_LASERCLOSEERROR;
		}
	}

	ResetEvent(m_SlowVent_Inlet_WriteEvent_Open);

	int nAddr  = 20;
	int nIndex = 0;
	int nData  = 1;


	int nRet = WriteOutputDataBit(nAddr, nIndex, nData);
	DigitalWriteIO(DO::SLOW_VENT_INLET, VALVE_OPEN);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() : SLOW VENT INLET OPEN WRITE 에러 발생";
		return SLOWVENTINLETOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_SlowVent_Inlet_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() : SLOW VENT INLET OPEN WRITE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() : SLOW VENT INLET OPEN 에러 발생 (TimeOut)";
		return SLOWVENTINLETOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_SlowVent_Outlet_Valve()
{
	int nRet = -1;
	int nAddr  = 20;
	int nIndex = 1;
	int nData  = 1;

	nRet = DigitalWriteIO(DO::SLOW_VENT_OUTLET, VALVE_OPEN);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_SlowVent_Outlet_Valve1() : SLOW VENT OUTLET OPEN WRITE 에러 발생";
		return SLOWVENTOUTLETOPEN_WRITEERROR;
	}
	
	Log_str = "Open_SlowVent_Outlet_Valve1() : SLOW VENT OUTLET OPEN WRITE 완료";
	return OPERATION_COMPLETED;
}

int CIODlg::Open_SlowVentValve()  // IO MAP : Y01.04  OPEN SET 1 m_bDigitalOut[20] --> OUTLET
									//        : Y00.10  OPEN SET 1 m_bDigitalOut[10] --> INLET
{
	////////////////////////////////////////
		//    SLOW VENT OPEN INTERLOCK       ///
		////////////////////////////////////////
		// 1. LLC GATE CLOSE 확인
		// 2. TR GATE CLOSE 확인
		// 3. LLC TMP GATE V/V CLOSE 확인
		// 4. LLC ROUGHING V/V CLOSE 확인
		// 5. LLC FORELINE V/V CLOSE 확인

	
	int nRet = -1;

	int nAddr  = 0;
	int nIndex = 0;
	int nData  = 0;


	// MC & LLC 함께 Venting 시는 TR GATE = OPEN   ---> MAINT MODE
	// LLC 혼자 벤팅시는 TR GATE = CLOSE		   ---> OPER MODE
	//                |
	//                |
	//                |
	//                |
	//                |
	//                V
	// MC & LLC 함께 Venting 시는 TR GATE = OPEN   ---> mc_venting_sequence_state = true
	// LLC 혼자 벤팅시는 TR GATE = CLOSE		   ---> mc_venting_sequence_state = false

	//if (IO_MODE == OPER_MODE_ON)

	if (!g_pVP->m_bMcVentingState)
	{
		if (Is_TRGateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_SlowVentValve1() :: Is_TRGateValve_Open() = SLOW VENT OPEN 시 TR GATE CLOSE INTERLOCK 에러";
			return SLOWVENTOPEN_TRGATEOPENERROR;
		}
	}

	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_SlowVentValve1() :: Is_LLCGateValve_Open() = SLOW VENT OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return SLOWVENTOPEN_LLCGATEOPENERROR;
	}
	
	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_SlowVentValve1() :: Is_LLC_FastRoughValve_Open() = SLOW VENT OPEN 시 LLC FAST ROUGH GATE CLOSE INTERLOCK 에러";
		return SLOWVENTOPEN_LLCFASTROUGHOPENERROR;
	}


	// VENTING 시, VACUUM 은 계속 진행 함. (FORELINE OPEN 되어 있어야하며, Pumping Rough 시 Close 됨)
	//
	//	if (Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSE)
	//	{
	//		INTERLOCK_STATE = FALSE;
	//		Log_str = "Open_SlowVentValve1() :: Is_LLC_TMP_ForelineValve_Open() = SLOW VENT OPEN 시 LLC TMP FORELINE V/V CLOSE INTERLOCK 에러";
	//		return INTERLOCK_STATE;
	//	}
	//if (Get_LLC_DryPump_Status() != DRYPUMP_RUN)
	//{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_SlowVentValve1() :: Get_LLC_DryPump_Status() = SLOW VENT OPEN 시 LLC DRY PUMP OFF INTERLOCK 에러";
	//	return INTERLOCK_STATE;
	//}


	ResetEvent(m_SlowVent_WriteEvent_Open);

	// INLET OPEN WRITE
	nAddr  = 20;
	nIndex = 0;
	nData  = 1;
	int nRet1 = DigitalWriteIO(DO::SLOW_VENT_OUTLET, VALVE_OPEN);

	// OUTLET OPEN WRITE
	nIndex = 1;
	int nRet2 = DigitalWriteIO(DO::SLOW_VENT_INLET, VALVE_OPEN);

	if (!nRet1 && !nRet2) 
		nRet = 0;
	else 
		nRet = -1;
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_SlowVentValve() : SLOW VENT VALVE OPEN WRITE 에러 발생";
		return SLOWVENTOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_SlowVent_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_SlowVentValve() : SLOW VENT VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_SlowVentValve() : SLOW VENT VALVE OPEN 에러 발생 (TimeOut)";
		return SLOWVENTOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_FastVent_Inlet_Valve()
{
	int nRet = 0;

	int nAddr  = 0;
	int nIndex = 0;
	int nData  = 0;

	if (Is_TRGateValve_Open() != VALVE_CLOSE) // OPEN 상태
	{
		if (Is_MC_FastRoughValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_FastVent_Inlet_Valve1() :: Is_MC_FastRoughValve_Open() = FAST VENT INLET VALVE OPEN 시 MC FAST ROUGH VALVE CLOSE INTERLOCK 에러 (TR OPEN 되어 있음)";
			return FASTVENTINLETOPEN_LLCFASTROUGHCLOSEERROR;
		}
		if (Is_MC_SlowRoughValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_FastVent_Inlet_Valve1() :: Is_MC_SlowRoughValve_Open() = FAST VENT INLET VALVE OPEN 시 MC SLOW ROUGH VALVE CLOSE INTERLOCK 에러 (TR OPEN 되어 있음)";
			return FASTVENTINLETOPEN_LLCSLOWROUGHCLOSEERROR;
		}
		if (Is_MC_TMP_GateValve_Open() != VALVE_CLOSED)
		{
			Log_str = "Open_FastVent_Inlet_Valve1() :: Is_MC_TMP1_GateValve_Open() = FAST VENT INLET VALVE OPEN 시 MC TMP CLOSE INTERLOCK 에러 (TR OPEN 되어 있음)";
			return FASTVENTINLETOPEN_MCTMPGATECLOSEERROR;
		}
		if (Is_MC_TMP_ForelineValve_Open() != VALVE_OPENED)
		{
			Log_str = "Open_FastVent_Inlet_Valve1() :: Is_MC_TMP1_ForelineValve_Open() = FAST VENT INLET VALVE OPEN 시 MC TMP FORELINE OPEN INTERLOCK 에러 (TR OPEN 되어 있음)";
			return FASTVENTINLETOPEN_MCFORELINEGATEOPENERROR;
		}
	}
	
	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_LLCGateValve_Open() =  FAST VENT INLET VALVE OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return FASTVENTINLETOPEN_LLCGATECLOSEERROR;
	}
	if (Is_LLC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_LLC_TMP_ForelineValve_Open() = FAST VENT INLET VALVE OPEN 시 LLC TMP FORELINE OPEN INTERLOCK 에러";
		return FASTVENTINLETOPEN_LLCFORELINEGATEOPENERROR;
	}
	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_LLC_TMP_GateValve_Open() = FAST VENT INLET VALVE OPEN 시 LLC TMP GATE CLOSE INTERLOCK 에러";
		return FASTVENTINLETOPEN_LLCTMPGATECLOSEERROR;
	}

	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_LLC_FastRoughValve_Open() = FAST VENT INLET VALVE OPEN 시 LLC FAST ROUGH VALVE CLOSE INTERLOCK 에러";
		return FASTVENTINLETOPEN_LLCFASTROUGHCLOSEERROR;
	}
	if (Is_LLC_SlowRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_LLC_SlowRoughValve_Open() = FAST VENT INLET VALVE OPEN 시 LLC SLOW ROUGH VALVE CLOSE INTERLOCK 에러";
		return FASTVENTINLETOPEN_LLCSLOWROUGHCLOSEERROR;
	}

	if (Is_SlowVent_Outlet_Valve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_SlowVent_Outlet_Valve1_Open() = FAST VENT INLET VALVE OPEN 시 SLOW VENT OUTLET OPEN INTERLOCK 에러";
		return FASTVENTINLETOPEN_SLOWVENTOUTLETCLOSEERROR;
	}
	if (Is_SlowVent_Inlet_Valve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVent_Inlet_Valve1() :: Is_SlowVent_Outlet_Valve1_Open() = FAST VENT INLET VALVE OPEN 시 SLOW VENT INLET OPEN INTERLOCK 에러";
		return FASTVENTINLETOPEN_SLOWVENTINLETCLOSEERROR;
	}

	ResetEvent(m_FastVent_Inlet_WriteEvent_Open);

	nAddr  = 20;
	nIndex = 2;
	nData  = 1;

	nRet = DigitalWriteIO(DO::FAST_VENT_INLET, VALVE_OPEN);
	m_StrReturnValue.Format(_T("%d"), nRet);  //??????
	if (nRet != 0)
	{
		Log_str = "Open_SlowVent_Inlet_Valve1() : FAST VENT INLET OPEN WRITE 에러 발생";
		return FASTVENTINLETOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_FastVent_Inlet_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_FastVentValve() : FAST VENT INLET VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_FastVentValve() : FAST VENT VALVE INLET OPEN 에러 발생 (TimeOut)";
		return FASTVENTINLETOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_FastVent_Outlet_Valve()
{
	int nRet = 0;

	int nAddr = 0;
	int nIndex = 0;
	int nData = 0;

	//if (Is_FastVent_Inlet_Valve1_Open() != VALVE_OPEN)
	//{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_FastVent_Outlet_Valve1() :: Is_FastVent_Inlet_Valve1_Open() = FAST VENT OUTLET VALVE OPEN 시 FAST VENT INLET OPEN INTERLOCK 에러";
	//	return FASTVENTOUTLETOPEN_FASTVENTINLETOPENERROR;
	//}
	
	//if (INTERLOCK_STATE)
	//{
		nAddr  = 20;
		nIndex = 3;
		nData  = 1;
		nRet = DigitalWriteIO(DO::FAST_VENT_OUTLET, VALVE_OPEN);
		m_StrReturnValue.Format(_T("%d"), nRet);
		if (nRet != 0)
		{
			Log_str = "Open_FastVent_Outlet_Valve1() : FAST VENT OUTLET OPEN WRITE 에러 발생";
			return FASTVENTOUTLETOPEN_WRITEERROR;
		}

		Log_str = "Open_FastVent_Outlet_Valve1() : FAST VENT OUTLET OPEN WRITE 완료";
		return OPERATION_COMPLETED;
	//}
	//else return FASTVENTOUTLETOPENU_NKNOWNERROR;
}

int CIODlg::Open_FastVentValve()// IO MAP : Y01.03  OPEN SET 1 m_bDigitalOut[19] -> OUTLET
								  //        : Y00.09  OPEN SET 1 m_bDigitalOut[9]  -> INLET
{
		////////////////////////////////////////
		//    SLOW VENT OPEN INTERLOCK       ///
		////////////////////////////////////////
		// 1. LLC GATE CLOSE 확인
		// 2. TR GATE CLOSE 확인
		// 3. LLC TMP GATE V/V CLOSE 확인 (OPER MODE)
		// 4. LLC ROUGHING V/V CLOSE 확인
		// 5. LLC FORELINE V/V OPEN 확인
	
	int nRet = 0;

	int nAddr  = 0;
	int nIndex = 0;
	int nData  = 0;

	// MC & LLC 함께 Venting 시는 TR GATE = OPEN   ---> MAINT MODE
	// LLC 혼자 벤팅시는 TR GATE = CLOSE		   ---> OPER MODE
	//                |
	//                |
	//                |
	//                |
	//                |
	//                V
	// MC & LLC 함께 Venting 시는 TR GATE = OPEN   ---> mc_venting_sequence_state = true
	// LLC 혼자 벤팅시는 TR GATE = CLOSE		   ---> mc_venting_sequence_state = false

	//if (IO_MODE == OPER_MODE_ON)
	if (!g_pVP->m_bMcVentingState)
	{
		if (Is_TRGateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_FastVentValve() :: Is_TRGateValve_Open() = FAST VENT OPEN 시 TR GATE CLOSE INTERLOCK 에러";
			return FASTVENTOPEN_TRGATECLOSEERROR;
		}
	}
	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVentValve() :: Is_LLCGateValve_Open() = FAST VENT OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return FASTVENTOPEN_LLCGATECLOSEERROR;
	}

	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_FastVentValve() :: Is_LLC_FastRoughValve_Open() = FAST VENT OPEN 시 LLC FAST ROUGH GATE CLOSE INTERLOCK 에러";
		return FASTVENTOPEN_LLCFASTROUGHCLOSEERROR;
	}

	// VENTING 시, VACUUM 은 계속 진행 함. (FORELINE OPEN 되어 있어야하며, Pumping Rough 시 Close 됨)
	//
	//if (Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSE)
	//{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_FastVentValve() :: Is_LLC_TMP_ForelineValve_Open() = FAST VENT OPEN 시 LLC TMP FORELINE V/V CLOSE INTERLOCK 에러";
	//	return INTERLOCK_STATE;
	//}
	//if (Get_LLC_DryPump_Status() != DRYPUMP_RUN)
	//{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_FastVentValve() :: Get_LLC_DryPump_Status() = FAST VENT OPEN 시 LLC DRY PUMP OFF INTERLOCK 에러";
	//	return INTERLOCK_STATE;
	//}

	ResetEvent(m_FastVent_WriteEvent_Open);

	// INLET OPEN
	nAddr  = 20;
	nIndex = 2;
	nData  = 1;
	int nRet1 = DigitalWriteIO(DO::FAST_VENT_INLET, VALVE_OPEN);

	// OUTLET OPEN
	nIndex = 3;
	int nRet2 = DigitalWriteIO(DO::FAST_VENT_OUTLET, VALVE_OPEN);

	if (!nRet1 && !nRet2) 
		nRet = 0;
	else 
		nRet = -1;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_FastVentValve() : FAST VENT OPEN WRITE 에러 발생";
		return FASTVENTOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_FastVent_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_FastVentValve() : FAST VENT VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_FastVentValve() : FAST VENT VALVE OPEN 에러 발생 (Time Out)";
		return FASTVENTOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_MC_SlowRoughValve()
{
	//	IO MAP : Y01.08
	//	OPEN SET 1 m_bDigitalOut[24]

	int nRet = 0;

	int nAddr  = 0;
	int nIndex = 0;
	int nData  = 0;
	//  게이지 모니터로 변경.

		///////////////////////////////////////////
		//   MC SLOW ROUGH V/V OPEN INTERLOCK    ///
		///////////////////////////////////////////

		// @ MC DRY PUMP ON  확인					
		// @ MC FORELINE CLSOE 확인					
		// @ MC TMP GATE CLOSE 확인			
		// @ SOURCE GATE CLOSE 확인				
		// @ MC FAST ROUGHING VALVE CLOSE 확안.
		// @ MC SLOW ROUGHING VALVE CLOSE 확인.
		// @ TR LID CLOSE 확인. 
		// @ MC LID CLOSE 확인. 
		// @ PUMPING LINE 압력 확인 확인.
		// @ PUMPING LINE2 압력 확인 확인.


		/////TR GATE OPEN 시 INTERLOCK 확인 필요 ///////////
		// @ FAST INLET VENT VALVE CLOSE 확인.
		// @ FAST OUTLET VENT VALVE CLOSE 확인.
		// @ SLOW INLET VENT VALVE CLOSE 확인.
		// @ SLOW OUTLET VENT VALVE CLOSE 확인.
		// @ SLOW MFC CLOSE 확인.
		// @ FAST MFC CLOSE 확인.
		// @ LLC TMP GATE CLOSE  확인.
		// @ LLC GATE CLOSE 확인.
		// @ LLC FORELINE V/V CLOSE 확인.
		// @ LLC LID CLSOSE 확인.
		///////////////////////////////////////////


	if (Is_MC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_MC_SlowRoughValve() :: Get_MC_DryPump_Status() = MC SLOW ROUGH OPEN 시 MC DRY PUMP ON INTERLOCK 에러";
		return MCSLOWROUGHOPEN_DRYPUMPERROR;
	}
	if (Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_SlowRoughValve() :: Is_MC_TMP1_ForelineValve_Open() = MC SLOW ROUGH OPEN 시 MC TMP FORELINE VALVE CLOSE INTERLOCK 에러";
		return MCSLOWROUGHOPEN_MCFORELINECLOSEERROR;
	}
	if (Is_MC_TMP_GateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_SlowRoughValve() :: Is_MC_TMP1_GateValve_Open() = MC SLOW ROUGH OPEN 시 MC TMP GATE CLOSE INTERLOCK 에러";
		return MCSLOWROUGHOPEN_MCTMPGATECLOSEERROR;
	}
	if (Is_LaserSource_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_SlowRoughValve() :: Is_LaserSource_Open() = MC SLOW ROUGH OPEN 시 SOURCE GATE CLOSE INTERLOCK 에러";
		return MCSLOWROUGHOPEN_LASERSOURCECLOSEERROR;
	}
	if (Is_MC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_SlowRoughValve() :: Is_MC_FastRoughValve_Open() = MC SLOW ROUGH OPEN 시 MC FAST ROUGH VALVE CLOSE INTERLOCK 에러";
		return MCSLOWROUGHOPEN_MCFASTROUGHCLOSEERROR;
	}
	if (Is_TR_Lid_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_SlowRoughValve() :: Is_TR_Lid_Open() = MC SLOW ROUGH OPEN 시 TR LID CLOSE INTERLOCK 에러";
		return MCSLOWROUGHOPEN_TRLIDCLOSEERROR;
	}
	if (Is_MC_Lid_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_SlowRoughValve() :: Is_MC_Lid_Open() = MC SLOW ROUGH OPEN 시 MC LID CLOSE INTERLOCK 에러";
		return MCSLOWROUGHOPEN_MCLIDCLOSEERROR;
	}
	
	
	
	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		if (Is_SlowVentValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_SlowRoughValve() :: Is_SlowVentValve1_Open() =  MC SLOW ROUGH OPEN 시 (TR GATE OPEN) SLOW VENT VALVE CLOSE INTERLOCK 에러";
			return MCSLOWROUGHOPEN__TROPEN_SLOWVENTCLOSEERROR;
		}
		if (Is_FastVentValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_SlowRoughValve() :: Is_FastVentValve_Open() = MC SLOW ROUGH OPEN 시 (TR GATE OPEN) FAST VENT VALVE CLOSE INTERLOCK 에러";
			return MCSLOWROUGHOPEN__TROPEN_FASTVENTCLOSEERROR;
		}
		if (Is_LLC_Lid_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_SlowRoughValve() :: Is_LLC_Lid_Open() = MC SLOW ROUGH OPEN 시 (TR GATE OPEN)  LLC LID CLOSE INTERLOCK 에러";
			return MCSLOWROUGHOPEN__TROPEN_LLCLIDCLOSEERROR;
		}
		if (Off_SlowMfc() != 0)
		{
			Log_str = "Open_MC_SlowRoughValve() :: Off_SlowMfc() = MC SLOW ROUGH OPEN 시 (TR GATE OPEN)  Slow MFC 0ff INTERLOCK 에러";
			return MCSLOWROUGHOPEN__TROPEN_SLOWMFCLOSEERROR;
		}
		if (Off_FastMfc() != 0)
		{
			Log_str = "Open_MC_SlowRoughValve() :: Off_FastMfc() = MC SLOW ROUGH OPEN 시 (TR GATE OPEN)  Fast MFC 0ff INTERLOCK 에러";
			return MCSLOWROUGHOPEN__TROPEN_FASTMFCLOSEERROR;
		}
		if (Is_LLCGateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_SlowRoughValve() :: Is_LLCGateValve_Open() =MC SLOW ROUGH OPEN 시 (TR GATE OPEN)  LLC GATE CLOSE INTERLOCK 에러";
			return MCSLOWROUGHOPEN__TROPEN_LLCGATECLOSEERROR;
		}
		if (Is_LLC_DryPump_Status() != DRYPUMP_RUN)
		{
			Log_str = "Open_MC_SlowRoughValve() :: Get_LLC_DryPump_Status() = MC SLOW ROUGH OPEN 시 (TR GATE OPEN) LLC DRY PUMP ON INTERLOCK 에러";
			return MCSLOWROUGHOPEN__TROPEN_LLCDRYERROR;
		}
		if (Is_LLC_TMP_ForelineValve_Open() != VALVE_OPEN)
		{
			Log_str = "Open_MC_SlowRoughValve() :: Is_LLC_TMP_ForelineValve_Open() = MC SLOW ROUGH OPEN 시 (TR GATE OPEN)  LLC TMP FORELINE OPEN INTERLOCK 에러";
			return MCSLOWROUGHOPEN__TROPEN_LLCTMPFORELINECLOSEERROR;
		}
		if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_SlowRoughValve() :: Is_LLC_TMP_GateValve_Open() = MC SLOW ROUGH OPEN 시 (TR GATE OPEN) LLC TMP GATE CLOSE INTERLOCK 에러";
			return MCSLOWROUGHOPEN__TROPEN_LLCTMPGATECLOSEERROR;
		}

	}
	////if() PUMPING_LINE_VALUE = TRUE;
	////if() PUMPING_LINE_VALUE2 = TRUE;

	ResetEvent(m_MCSlowRough_WriteEvent_Open);

	nAddr  = 19;
	nIndex = 0;
	nData  = 1;

	nRet =  DigitalWriteIO(DO::MC_SLOW_ROUGHING, VALVE_OPEN);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_MC_SlowRoughValve() :  MC SLOW ROUGH VALVE OPEN WRITE 에러 발생";
		return MCSLOWROUGHOPEN_WRITEERROR;
	}

	if (WaitForSingleObject(m_MCSlowRough_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_MC_SlowRoughValve() : MC SLOW ROUGH VALVE OPEN WRITE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_MC_SlowRoughValve() :  MC SLOW ROUGH VALVE OPEN 에러 발생 (TimeOut)";
		return MCSLOWROUGHOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_MC_FastRoughValve() 
{
	// IO MAP : Y01.09 
	//	OPEN SET 1 m_bDigitalOut[25]

		///////////////////////////////////////////
		//   MC SLOW ROUGH V/V OPEN INTERLOCK    ///
		///////////////////////////////////////////

		// @ MC DRY PUMP ON  확인					
		// @ MC FORELINE CLSOE 확인					
		// @ MC TMP GATE CLOSE 확인			
		// @ SOURCE GATE CLOSE 확인				
		// @ MC FAST ROUGHING VALVE CLOSE 확안.
		// @ MC SLOW ROUGHING VALVE CLOSE 확인.
		// @ TR LID CLOSE 확인. 
		// @ MC LID CLOSE 확인. 
		// @ PUMPING LINE 압력 확인 확인.
		// @ PUMPING LINE2 압력 확인 확인.


		/////TR GATE OPEN 시 INTERLOCK 확인 필요 ///////////
		// @ FAST INLET VENT VALVE CLOSE 확인.
		// @ FAST OUTLET VENT VALVE CLOSE 확인.
		// @ SLOW INLET VENT VALVE CLOSE 확인.
		// @ SLOW OUTLET VENT VALVE CLOSE 확인.
		// @ SLOW MFC CLOSE 확인.
		// @ FAST MFC CLOSE 확인.
		// @ LLC TMP GATE CLOSE  확인.
		// @ LLC GATE CLOSE 확인.
		// @ LLC FORELINE V/V CLOSE 확인.
		// @ LLC LID CLSOSE 확인.
		///////////////////////////////////////////

	if (Is_MC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_MC_FastRoughValve() :: Get_MC_DryPump_Status() = MC FAST ROUGH OPEN 시 MC DRY PUMP ON INTERLOCK 에러";
		return MCFASTROUGHOPEN_DRYPUMPERROR;
	}
	if (Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_FastRoughValve() :: Is_MC_TMP1_ForelineValve_Open() = MC FAST ROUGH OPEN 시 MC TMP FORELINE VALVE CLOSE INTERLOCK 에러";
		return MCFASTROUGHOPEN_MCFORELINECLOSEERROR;
	}
	if (Is_MC_TMP_GateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_FastRoughValve() :: Is_MC_TMP1_GateValve_Open() = MC FAST ROUGH OPEN 시 MC TMP GATE CLOSE INTERLOCK 에러";
		return MCFASTROUGHOPEN_MCTMPGATECLOSEERROR;
	}
	if (Is_LaserSource_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_FastRoughValve() :: Is_LaserSource_Open() = MC FAST ROUGH OPEN 시 SOURCE GATE CLOSE INTERLOCK 에러";
		return MCFASTROUGHOPEN_LASERSOURCECLOSEERROR;
	}
	if (Is_MC_SlowRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_FastRoughValve() :: Is_MC_SlowRoughValve_Open() = MC FAST ROUGH OPEN 시 MC SLOW ROUGH VALVE CLOSE INTERLOCK 에러";
		return MCFASTROUGHOPEN_MCSLOWROUGHCLOSEERROR;
	}
	if (Is_TR_Lid_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_FastRoughValve() :: Is_TR_Lid_Open() = MC FAST ROUGH OPEN 시 TR LID CLOSE INTERLOCK 에러";
		return MCFASTROUGHOPEN_TRLIDCLOSEERROR;
	}
	if (Is_MC_Lid_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_FastRoughValve() :: Is_MC_Lid_Open() = MC FAST ROUGH OPEN 시 MC LID CLOSE INTERLOCK 에러";
		return MCFASTROUGHOPEN_MCLIDCLOSEERROR;
	}

	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		if (Is_SlowVentValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_FastRoughValve() :: Is_SlowVentValve1_Open() =  MC FAST ROUGH OPEN 시 (TR GATE OPEN) SLOW VENT VALVE CLOSE INTERLOCK 에러";
			return MCFASTROUGHOPEN__TROPEN_SLOWVENTCLOSEERROR;
		}
		if (Is_FastVentValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_FastRoughValve() :: Is_FastVentValve_Open() = MC FAST ROUGH OPEN 시 (TR GATE OPEN) FAST VENT VALVE CLOSE INTERLOCK 에러";
			return MCFASTROUGHOPEN__TROPEN_FASTVENTCLOSEERROR;
		}
		if (Is_LLC_Lid_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_FastRoughValve() :: Is_LLC_Lid_Open() = MC FAST ROUGH OPEN 시 (TR GATE OPEN)  LLC LID CLOSE INTERLOCK 에러";
			return MCFASTROUGHOPEN__TROPEN_LLCLIDCLOSEERROR;
		}
		if (Off_SlowMfc() != 0)
		{
			Log_str = "Open_MC_FastRoughValve() :: Off_SlowMfc() = MC FAST ROUGH OPEN 시 (TR GATE OPEN)  Slow MFC 0ff INTERLOCK 에러";
			return MCFASTROUGHOPEN__TROPEN_SLOWMFCLOSEERROR;
		}
		if (Off_FastMfc() != 0)
		{
			Log_str = "Open_MC_FastRoughValve() :: Off_FastMfc() = MC FAST ROUGH OPEN 시 (TR GATE OPEN)  Fast MFC 0ff INTERLOCK 에러";
			return MCFASTROUGHOPEN__TROPEN_FASTMFCLOSEERROR;
		}
		if (Is_LLCGateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_FastRoughValve() :: Is_LLCGateValve_Open() =MC FAST ROUGH OPEN 시 (TR GATE OPEN)  LLC GATE CLOSE INTERLOCK 에러";
			return MCFASTROUGHOPEN__TROPEN_LLCGATECLOSEERROR;
		}
		if (Is_LLC_DryPump_Status() != DRYPUMP_RUN)
		{
			Log_str = "Open_MC_FastRoughValve() :: Get_LLC_DryPump_Status() = MC FAST ROUGH OPEN 시 (TR GATE OPEN) LLC DRY PUMP ON INTERLOCK 에러";
			return MCFASTROUGHOPEN__TROPEN_LLCDRYERROR;
		}
		if (Is_LLC_TMP_ForelineValve_Open() != VALVE_OPEN)
		{
			Log_str = "Open_MC_FastRoughValve() :: Is_LLC_TMP_ForelineValve_Open() = MC FAST ROUGH OPEN 시 (TR GATE OPEN)  LLC TMP FORELINE OPEN INTERLOCK 에러";
			return MCFASTROUGHOPEN__TROPEN_LLCTMPFORELINECLOSEERROR;
		}
		if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_FastRoughValve() :: Is_LLC_TMP_GateValve_Open() = MC FAST ROUGH OPEN 시 (TR GATE OPEN) LLC TMP GATE CLOSE INTERLOCK 에러";
			return MCFASTROUGHOPEN__TROPEN_LLCTMPGATECLOSEERROR;
		}
	}
	////if() PUMPING_LINE_VALUE = TRUE;
	////if() PUMPING_LINE_VALUE2 = TRUE;

	ResetEvent(m_MCFastRough_WriteEvent_Open);

	int nAddr  = 19;
	int nIndex = 1;
	int nData  = 1;

	int nRet = DigitalWriteIO(DO::MC_FAST_ROUGHING, VALVE_OPEN);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_MC_FastRoughValve() :  MC FAST ROUGH VALVE OPEN WRITE 에러 발생";
		return MCFASTROUGHOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_MCFastRough_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_MC_FastRoughValve() : MC FAST ROUGH VALVE OPEN WRITE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_MC_FastRoughValve() :  MC FAST ROUGH VALVE OPEN 에러 발생 (TimeOut)";
		return MCFASTROUGHOPEN_TIMEOUTERROR;
	}
}


int CIODlg::Open_MC_TMP_GateValve() 
{
	// IO MAP : Y01.10
	//	OPEN SET 1 m_bDigitalOut[26]
	////////////////////////////////////////
	//    MC TMP GATE OPEN INTERLOCK    ///
	////////////////////////////////////////
	
	// @ MC R.V CLOSE 확인  
	// @ SOURCE GATE CLOSE 확인  
	// @ MC 진공도 10^2 TORR 이하 확인 
	// @ MC DRY PUMP RUN (ON) 확인 
	// @ TR LID 확인   
	// @ MC LID 확인   
	// @ TMP 상태 확인   
	// @ MC Foreline Open 확인   
	
	///// TR GATE OPEN 시 INTERLOCK 확인 필요 
	// @ LLC R.V CLOSE 확인			  
	// @ LLC GATE CLOSE 확인			  
	// @ LLC LID CLOSE 확인			  
	// @ FAST VENT INLET V/V CLOSE 확인  
	// @ SLOW VENT INLET V/V CLOSE 확인  
	// @ SLOW MFC OFF 확인  
	// @ FAST MFC OFF 확인  
	////////////////////////////////////////


	if (Is_MC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_TMP1_GateValve() :: Is_MC_FastRoughValve_Open() = MC TMP GATE OPEN 시 MC FAST ROUGH VALVE CLOSE INTERLOCK 에러";
		return MCTMPGATEOPEN_MCFASTROUGHCLOSEERROR;
	}
	if (Is_LaserSource_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_TMP1_GateValve() :: Is_LaserSource_Open() = MC TMP OPEN OPEN 시 SOURCE GATE CLOSE INTERLOCK 에러";
		return MCTMPGATEOPEN_LASERSOURCECLOSEERROR;
	}
	if (Is_MC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_MC_TMP1_GateValve() :: Get_MC_DryPump_Status() = MC TMP OPEN OPEN 시 MC DRY PUMP INTERLOCK 에러";
		return MCTMPGATEOPEN_DRYPUMPERROR;
	}
	if (Is_MC_TMP_ForelineValve_Open() != VALVE_OPEN)
	{
		Log_str = "Open_MC_TMP1_GateValve() :: Is_MC_TMP1_ForelineValve_Open() = MC TMP OPEN OPEN 시 MC FORELINE OPEN INTERLOCK 에러";
		return MCTMPGATEOPEN_MCFORELINEOPENERROR;
	}
	if (Is_TR_Lid_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_TMP1_GateValve() :: Is_TR_Lid_Open() = MC TMP OPEN OPEN 시 TR LID CLOSE INTERLOCK 에러";
		return MCTMPGATEOPEN_TRLIDCLOSEERROR;
	}
	if (Is_MC_Lid_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_TMP1_GateValve() :: Is_MC_Lid_Open() = MC TMP OPEN OPEN 시 MC LID CLOSE INTERLOCK 에러";
		return MCTMPGATEOPEN_MCLIDCLOSEERROR;
	}

	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		if (Is_SlowVentValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_TMP1_GateValve() :: Is_SlowVentValve1_Open() =  MC TMP OPEN OPEN 시 (TR GATE OPEN) SLOW VENT VALVE CLOSE INTERLOCK 에러";
			return MCTMPGATEOPEN__TROPEN_SLOWVENTCLOSEERROR;
		}
		if (Is_FastVentValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_TMP1_GateValve() :: Is_FastVentValve_Open() = MC TMP OPEN OPEN 시 (TR GATE OPEN) FAST VENT VALVE CLOSE INTERLOCK 에러";
			return MCTMPGATEOPEN__TROPEN_FASTVENTCLOSEERROR;
		}
		if (Is_LLC_Lid_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_TMP1_GateValve() :: Is_LLC_Lid_Open() = MC TMP OPEN OPEN 시 (TR GATE OPEN)  LLC LID CLOSE INTERLOCK 에러";
			return MCTMPGATEOPEN__TROPEN_LLCLIDCLOSEERROR;
		}
		if (Off_SlowMfc() != 0)
		{
			Log_str = "Open_MC_TMP1_GateValve() :: Off_SlowMfc() = MC TMP OPEN OPEN 시 (TR GATE OPEN)  Slow MFC 0ff INTERLOCK 에러";
			return MCTMPGATEOPEN__TROPEN_SLOWMFCLOSEERROR;
		}
		if (Off_FastMfc() != 0)
		{
			Log_str = "Open_MC_TMP1_GateValve() :: Off_FastMfc() = MC TMP OPEN OPEN 시 (TR GATE OPEN)  Fast MFC 0ff INTERLOCK 에러";
			return MCTMPGATEOPEN__TROPEN_FASTMFCLOSEERROR;
		}
		if (Is_LLCGateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_TMP1_GateValve() :: Is_LLCGateValve_Open() = MC TMP OPEN OPEN 시 (TR GATE OPEN)  LLC GATE CLOSE INTERLOCK 에러";
			return MCTMPGATEOPEN__TROPEN_LLCGATECLOSEERROR;
		}
		if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_TMP1_GateValve() :: Is_LLC_FastRoughValve_Open() = MC TMP OPEN OPEN 시 (TR GATE OPEN)  LLC Rough CLOSE INTERLOCK 에러";
			return MCTMPGATEOPEN__TROPEN_LLCROUGHCLOSEERROR;
		}
		if (Is_LLC_DryPump_Status() != DRYPUMP_RUN)
		{
			Log_str = "Open_MC_TMP1_GateValve() :: Get_LLC_DryPump_Status() = MC TMP OPEN OPEN 시 (TR GATE OPEN) LLC DRY PUMP ON INTERLOCK 에러";
			return MCTMPGATEOPEN__TROPEN_LLCDRYERROR;
		}
	}

	ResetEvent(m_MCTmpGate_WriteEvent_Open);

	int nAddr  = 19;
	int nIndex = 2;
	int nData  = 1;

	int nRet = DigitalWriteIO(DO::MC_TMP_GATE, VALVE_OPEN);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_MC_TMP1_GateValve() :  MC TMP GATE VALVE OPEN WRIET 에러 발생";
		return MCTMPGATEOPEN_WRITEERROR;
		
	}
	
	if (WaitForSingleObject(m_MCTmpGate_WriteEvent_Open, 5000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_MC_TMP1_GateValve(): MC TMP GATE VALVE OPEN WRITE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_MC_TMP1_GateValve() :  MC TMP GATE VALVE OPEN 에러 발생 (TimeOut)";
		return MCTMPGATEOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_MC_TMP_ForelineValve() 
{
	// IO MAP : Y01.11 
	//	OPEN SET 1 m_bDigitalOut[27]
	///////////////////////////////////////////
	//    MC FORELINE V/V OPEN INTERLOCK    ///
	///////////////////////////////////////////

	// @ MC DRY PUMP ON  확인	
	// @ MC TMP 상태 확인
	// @ SOURCE GATE CLOSE 확인
	// @ MC ROUGHING CLSOE 확인									
	// @ MC TMP GATE CLSOE 확인									
	// @ MC FORELINE 진공상태 확인 (51B, 722B 확인)			

	//bool MC_LINE_901P_SET, MC_LINE_722B_SET, MC_ROUGHING_CLOSE, MC_DRY_ON = FALSE;


	// MC & LLC 함께 Venting 시는 TR GATE = OPEN   ---> mc_venting_sequence_state = true
	// LLC 혼자 벤팅시는 TR GATE = CLOSE		   ---> mc_venting_sequence_state = false
	//if (IO_MODE == OPER_MODE_ON)
	if (!g_pVP->m_bMcVentingState)
	{
		if (Is_TRGateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_TMP1_ForelineValve() :: Is_TRGateValve_Open() = MC TMP FORELINE VALVE OPEN 시 TR GATE CLOSE INTERLOCK 에러";
			return MCFORELINEOPEN_TRGATECLOSEERROR;
		}
	}

	if (Is_MC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_MC_TMP1_ForelineValve() :: Get_MC_DryPump_Status() = MC TMP FORELINE VALVE OPEN 시 MC DRY PUMP ON INTERLOCK 에러";
		return MCFORELINEOPEN_DRYPUMPERROR;
	}
	if (Is_MC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_TMP1_ForelineValve() :: Is_MC_FastRoughValve_Open() = MC TMP FORELINE VALVE OPEN 시 MC FAST ROUGHING VALVE CLOSE INTERLOCK 에러";
		return MCFORELINEOPEN_MCFASTROUGHCLOSEERROR;
	}
	if (Is_MC_SlowRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Is_MC_SlowRoughValve_Open() :: Is_MC_SlowRoughValve_Open() = MC TMP FORELINE VALVE OPEN 시 MC SLOW ROUGHING VALVE CLOSE INTERLOCK 에러";
		return MCFORELINEOPEN_MCSLOWROUGHCLOSEERROR;
	}
	if (Is_LaserSource_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_TMP1_ForelineValve() :: Is_LaserSource_Open() = MC TMP FORELINE VALVE OPEN 시 MC SOURCE GATE CLOSE INTERLOCK 에러";
		return MCFORELINEOPEN_LASERCLOSEERROR;
	}
	if (Is_MC_TMP_GateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_MC_TMP1_ForelineValve() :: Is_MC_TMP_GateValve_Open() = MC TMP FORELINE VALVE OPEN 시 MC TMP GATE CLOSE INTERLOCK 에러";
		return MCFORELINEOPEN_MCTMPGATECLOSEERROR;
	}

	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_TMP1_ForelineValve() :: Is_LLC_FastRoughValve_Open() = MC TMP FORELINE VALVE OPEN 시 (TR GATE OPEN) LLC Fast Rough Valve close INTERLOCK 에러";
			return MCFORELINEOPEN__TROPEN_LLCROUGHCLOSEERROR;
		}
		if (Is_LLCGateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_MC_TMP1_ForelineValve() :: Is_LLCGateValve_Open() = MC TMP FORELINE VALVE OPEN 시 (TR GATE OPEN) LLC Gate Valve close INTERLOCK 에러";
			return MCFORELINEOPEN__TROPEN_LLCGATECLOSEERROR;
		}

	}

	//if (Is_MC_TMP1_GateValve_Open() != VALVE_OPEN)
	//	{
	//	INTERLOCK_STATE = FALSE;
	//	Log_str = "Open_MC_TMP1_ForelineValve() :: Is_MC_TMP1_GateValve_Open() = MC TMP FORELINE VALVE OPEN  시 MC TMPE GATE OPEN INTERLOCK 에러";
	//	return INTERLOCK_STATE;
	//}
	//if () MC_LINE_901P_SET = TRUE;
	//if () MC_LINE_722B_SET = TRUE;


	ResetEvent(m_MCForelineGate_WriteEvent_Open);

	int nAddr  = 19;
	int nIndex = 3;
	int nData  = 1;

	int nRet = DigitalWriteIO(DO::MC_FORELINE, VALVE_OPEN);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_MC_TMP1_ForelineValve() :  MC TMP FORELINE VALVE OPEN WRITE 에러 발생";
		return MCFORELINEOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_MCForelineGate_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_MC_TMP1_ForelineValve(): MC TMP FORELINE VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_MC_TMP1_ForelineValve() :  MC TMP FORELINE VALVE OPEN 에러 발생 (TimeOut)";
		return MCFORELINEOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_LLC_SlowRoughValve() 
{
	//	IO MAP : Y01.03 
	//	OPEN SET 1 m_bDigitalOut[19]

	/////////////////////////////////////////////
	//   LLC SLOW ROUGH V/V OPEN INTERLOCK    ///
	/////////////////////////////////////////////

	// @ LLC DRY PUMP ON  확인					
	// @ LLC GATE CLSOE  확인					
	// @ FAST VENT INLET CLOSE 확인				
	// @ FAST VENT OUTLET CLOSE 확인				
	// @ SLOW VENT INLET CLOSE 확인				
	// @ SLOW VENT OUTLET CLOSE 확인	
	// @ SLOW MFC OFF
	// @ FAST MFC OFF
	// @ LLC TMP GATE CLOSE  확인
	// @ LLC FORLINE V/V CLOSE 확인.
	// @ LLC LID CLOSE 확인.
	// @ PUMPING LINE 압력 확인. 901P
	// @ PUMPING LINE 압력 확인. 722B
	// @ TR GATE CLOSE 확인.
	

	///// TR GATE OPEN 시 INTERLOCK 확인 필요 ///////////
	// @ MC DRY PUMP 확인.
	// @ TR LID CLOSE  확인.
	// @ MC LID CLOSE  확인.
	// @ MC TMP GATE CLOSE 확인.
	// @ MC FORELINE V/V CLOSE 확인.
	// @ SOURCE GATE CLOSE 확인.
	/////////////////////////////////////////


	if (Is_LLC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_LLC_SlowRoughValve() :: Open_LLC_FastRoughValve() = LLC SLOW ROUGH VALVE OPEN 시 LLC DRY PUMP ON INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_DRYPUMPERROR;
	}
	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_SlowRoughValve() :: Is_LLCGateValve_Open() = LLC SLOW ROUGH VALVE OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_LLCGATECLOSEERROR;
	}
	if (Is_FastVentValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_SlowRoughValve() :: Is_FastVentValve_Open() = LLC SLOW ROUGH VALVE OPEN 시 FAST VENT CLOSE INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_FASTVENTCLOSEERROR;
	}
	if (Is_SlowVentValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_SlowRoughValve() :: Is_SlowVentValve1_Open() = LLC SLOW ROUGH VALVE OPEN 시 SLOW VENT CLOSE INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_SLOWVENTCLOSEERROR;
	}
	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_SlowRoughValve() :: Is_LLC_TMP_GateValve_Open() = LLC SLOW ROUGH VALVE OPEN 시 LLC TMP GATE CLOSE INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_LLCTMPGATECLOSEERROR;
	}
	if (Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_SlowRoughValve() :: Is_LLC_TMP_ForelineValve_Open() = LLC SLOW ROUGH VALVE OPEN 시 LLC TMP FORELINE VALVE CLOSE INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_LLCFORELINECLOSEERROR;
	}
	if (Is_LLC_Lid_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_SlowRoughValve() :: Is_LLC_Lid_Open() = LLC SLOW ROUGH VALVE OPEN 시 LLC LID CLOSE INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_LLCLIDCLOSEERROR;
	}
	if (Off_SlowMfc() != 0)
	{
		Log_str = "Open_LLC_SlowRoughValve() :: Off_SlowMfc() = LLC SLOW ROUGH VALVE OPEN 시 Slow MFC 0ff INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_SLOWMFCOFFERROR;
	}
	if (Off_FastMfc() != 0)
	{
		Log_str = "Open_LLC_SlowRoughValve() :: Off_FastMfc() = LLC SLOW ROUGH VALVE OPEN 시 Fast MFC 0ff INTERLOCK 에러";
		return LLCSLOWROUGHOPEN_FASTMFCOFFERROR;
	}

	/* TR GATE OPEN 시 적용 ? */
	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		if (Is_MC_DryPump_Status() != DRYPUMP_RUN)
		{
			Log_str = "Open_LLC_SlowRoughValve() :: Is_MC_DryPump_Status() = LLC SLOW ROUGH VALVE OPEN 시 (TR OPEN) MC DRY PUMP ON INTERLOCK 에러";
			return LLCSLOWROUGHOPEN__TRGATEOPEN_MCDRYPUMPERROR;
		}
		if (Is_LaserSource_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_SlowRoughValve() :: Is_LaserSource_Open() = LLC SLOW ROUGH VALVE OPEN 시 (TR OPEN) Laser Source Gate Close INTERLOCK 에러";
			return LLCSLOWROUGHOPEN__TRGATEOPEN_LASERCLOSEERROR;
		}
		if (Is_MC_TMP_GateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_SlowRoughValve() :: Is_MC_TMP_GateValve_Open() = LLC SLOW ROUGH VALVE OPEN 시 (TR OPEN) MC TMP GATE CLOSE INTERLOCK 에러";
			return LLCSLOWROUGHOPEN__TRGATEOPEN_MCTMPGATECLOSEERROR;
		}
		if (Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_SlowRoughValve() :: Is_MC_TMP_ForelineValve_Open() = LLC SLOW ROUGH VALVE OPEN 시 (TR OPEN) MC FORELINE VALVE CLOSE INTERLOCK 에러";
			return LLCSLOWROUGHOPEN__TRGATEOPEN_MCFORELINECLOSEERRO;
		}
		if (Is_TR_Lid_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_SlowRoughValve() :: Is_TR_Lid_Open() = LLC SLOW ROUGH VALVE OPEN 시 (TR OPEN) TR LID CLOSE INTERLOCK 에러";
			return LLCSLOWROUGHOPEN__TRGATEOPEN_TRLIDCLOSEERROR;
		}
		if (Is_MC_Lid_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_SlowRoughValve() :: Is_MC_Lid_Open() = LLC SLOW ROUGH VALVE OPEN 시 (TR OPEN) MC LID CLOSE INTERLOCK 에러";
			return LLCSLOWROUGHOPEN__TRGATEOPEN_MCLIDCLOSEERROR;
		}
	}
	
	
	int nAddr = 18;
	int nIndex = 3;
	int nData = 1;
	int nRet = -1;

	ResetEvent(m_LLCSlowRough_WriteEvent_Open);
	nRet = DigitalWriteIO(DO::LLC_SLOW_ROUGHING, VALVE_OPEN);
	
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_LLC_SlowRoughValve() :  LLC SLOW ROUGH VALVE OPEN WRITE 에러 발생";
		return LLCSLOWROUGHOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCSlowRough_WriteEvent_Open, 5000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_LLC_SlowRoughValve(): LLC SLOW ROUGH VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_LLC_SlowRoughValve() :  LLC SLOW ROUGH VALVE OPEN 에러 발생 (TimeOut)";
		return LLCSLOWROUGHOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_LLC_FastRoughValve() 
{

	//	IO MAP : Y01.04 
	//	OPEN SET 1 mbDigitalOut[20]

	/////////////////////////////////////////////
	//   LLC FAST ROUGH V/V OPEN INTERLOCK    ///
	/////////////////////////////////////////////

		// @ LLC DRY PUMP ON  확인					
	// @ LLC GATE CLSOE  확인					
	// @ FAST VENT INLET CLOSE 확인				
	// @ FAST VENT OUTLET CLOSE 확인				
	// @ SLOW VENT INLET CLOSE 확인				
	// @ SLOW VENT OUTLET CLOSE 확인	
	// @ SLOW MFC OFF
	// @ FAST MFC OFF
	// @ LLC TMP GATE CLOSE  확인
	// @ LLC FORLINE V/V CLOSE 확인.
	// @ LLC LID CLOSE 확인.
	// @ PUMPING LINE 압력 확인. 901P
	// @ PUMPING LINE 압력 확인. 722B
	// @ TR GATE CLOSE 확인.


	///// TR GATE OPEN 시 INTERLOCK 확인 필요 ///////////
	// @ MC DRY PUMP 확인.
	// @ TR LID CLOSE  확인.
	// @ MC LID CLOSE  확인.
	// @ MC TMP GATE CLOSE 확인.
	// @ MC FORELINE V/V CLOSE 확인.
	// @ SOURCE GATE CLOSE 확인.
	/////////////////////////////////////////
	
	if (Is_LLC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Open_LLC_FastRoughValve() = LLC FAST ROUGH VALVE OPEN 시 LLC DRY PUMP ON INTERLOCK 에러";
		return LLCFASTROUGHOPEN_DRYPUMPERROR;
	}
	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_LLCGateValve_Open() = LLC FAST ROUGH VALVE OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return LLCFASTROUGHOPEN_LLCGATECLOSEERROR;
	}
	if (Is_FastVentValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_FastVentValve_Open() = LLC FAST ROUGH VALVE OPEN 시 FAST VENT CLOSE INTERLOCK 에러";
		return LLCFASTROUGHOPEN_FASTVENTCLOSEERROR;
	}
	if (Is_SlowVentValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_SlowVentValve1_Open() = LLC FAST ROUGH VALVE OPEN 시 SLOW VENT CLOSE INTERLOCK 에러";
		return LLCFASTROUGHOPEN_SLOWVENTCLOSEERROR;
	}
	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_LLC_TMP_GateValve_Open() = LLC FAST ROUGH VALVE OPEN 시 LLC TMP GATE CLOSE INTERLOCK 에러";
		return LLCFASTROUGHOPEN_LLCTMPGATECLOSEERROR;
	}
	if (Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_LLC_TMP_ForelineValve_Open() = LLC FAST ROUGH VALVE OPEN 시 LLC TMP FORELINE VALVE CLOSE INTERLOCK 에러";
		return LLCFASTROUGHOPEN_LLCFORELINECLOSEERROR;
	}
	if (Is_LLC_Lid_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Is_LLC_Lid_Open() = LLC FAST ROUGH VALVE OPEN 시 LLC LID CLOSE INTERLOCK 에러";
		return LLCFASTROUGHOPEN_LLCLIDCLOSEERROR;
	}
	if (Off_SlowMfc() != 0)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Off_SlowMfc() = LLC FAST ROUGH VALVE OPEN 시 Slow MFC 0ff INTERLOCK 에러";
		return LLCFASTROUGHOPEN_SLOWMFCOFFERROR;
	}
	if (Off_FastMfc() != 0)
	{
		Log_str = "Open_LLC_FastRoughValve() :: Off_FastMfc() = LLC FAST ROUGH VALVE OPEN 시 Fast MFC 0ff INTERLOCK 에러";
		return LLCFASTROUGHOPEN_FASTMFCOFFERROR;
	}

	/* TR GATE OPEN 시 적용 */
	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		if (Is_MC_DryPump_Status() != DRYPUMP_RUN)
		{
			Log_str = "Open_LLC_FastRoughValve() :: Is_MC_DryPump_Status() = LLC FAST ROUGH VALVE OPEN 시 (TR OPEN) MC DRY PUMP ON INTERLOCK 에러";
			return LLCFASTROUGHOPEN__TRGATEOPEN_MCDRYPUMPERROR;
		}
		if (Is_LaserSource_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_FastRoughValve() :: Is_LaserSource_Open() = LLC FAST ROUGH VALVE OPEN 시 (TR OPEN) Laser Source Gate Close INTERLOCK 에러";
			return LLCFASTROUGHOPEN__TRGATEOPEN_LASERCLOSEERROR;
		}
		if (Is_MC_TMP_GateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_FastRoughValve() :: Is_MC_TMP_GateValve_Open() = LLC FAST ROUGH VALVE OPEN 시 (TR OPEN) MC TMP GATE CLOSE INTERLOCK 에러";
			return LLCFASTROUGHOPEN__TRGATEOPEN_MCTMPGATECLOSEERROR;
		}
		if (Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_FastRoughValve() :: Is_MC_TMP_ForelineValve_Open() = LLC FAST ROUGH VALVE OPEN 시 (TR OPEN) MC FORELINE VALVE CLOSE INTERLOCK 에러";
			return LLCFASTROUGHOPEN__TRGATEOPEN_MCFORELINECLOSEERRO;
		}
		if (Is_TR_Lid_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_FastRoughValve() :: Is_TR_Lid_Open() = LLC FAST ROUGH VALVE OPEN 시 (TR OPEN) TR LID CLOSE INTERLOCK 에러";
			return LLCFASTROUGHOPEN__TRGATEOPEN_TRLIDCLOSEERROR;
		}
		if (Is_MC_Lid_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_FastRoughValve() :: Is_MC_Lid_Open() = LLC FAST ROUGH VALVE OPEN 시 (TR OPEN) MC LID CLOSE INTERLOCK 에러";
			return LLCFASTROUGHOPEN__TRGATEOPEN_MCLIDCLOSEERROR;
		}
	}

	ResetEvent(m_LLCFastRough_WriteEvent_Open);

	int nAddr  = 18;
	int nIndex = 4;
	int nData  = 1;

	int nRet = DigitalWriteIO(DO::LLC_FAST_ROUGHING, VALVE_OPEN);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_LLC_FastRoughValve() :  LLC FAST ROUGH VALVE OPEN WRITE 에러 발생";
		return LLCFASTROUGHOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCFastRough_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_LLC_FastRoughValve(): LLC FAST ROUGH VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_LLC_FastRoughValve() :  LLC FAST ROUGH VALVE OPEN 에러 발생";
		return LLCFASTROUGHOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_LLC_TMP_GateValve() 
{
	//  IO MAP Y01.05 
	//	OPEN SET 1 m_bDigitalOut[21]

	////////////////////////////////////////
	//    LLC TMP GATE OPEN INTERLOCK    ///
	////////////////////////////////////////

	// @ LLC R.V CLOSE  확인 
	// @ FAST VENT INLET V/V CLOSE 확인  
	// @ FAST VENT OUTLET V/V CLOSE 확인  
	// @ SLOW VENT INLET V/V CLOSE 확인  
	// @ SLOW VENT OUTLET V/V CLOSE 확인  
	// @ SLOW MFC OFF 확인  
	// @ FAST MFC OFF 확인  
	// @ LLC GATE CLOSE 확인  
	// @ LLC 진공도 10^2 TORR 이하 확인  
	// @ LLC DRY PUMP RUN (ON) 확인  
	// @ TR GATE CLOSE 확인   
	// @ LLC TMP 상태 확인.
	// @ LLC FORELINE OPEN 상태 확인.
	// @ LLC LID CLOSE 상태 확인.


	///////  TR GATE OPEN 시 INTERLOCK 확인 필요 ///////////
	// @ MC R.V CLOSE  확인  
	// @ SOURCE GATE CLOSE 확인 
	// @ TR LID CLOSE 확인 
	// @ MC LID CLOSE 확인 
	/////////////////////////////////////////////


	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Is_LLC_FastRoughValve_Open() = LLC TMP GATE OPEN 시 LLC ROUGH VALVE CLOSE INTERLOCK 에러";
		return LLCTMPGATEOPEN_LLCFASTROUGHCLOSEERROR;
	}
	if (Is_SlowVentValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Is_SlowVentValve1_Open() = LLC TMP GATE OPEN 시 SLOW VENT VALVE CLOSE INTERLOCK 에러";
		return LLCTMPGATEOPEN_SLOWVENTCLOSEERROR;
	}
	if (Is_FastVentValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Is_FastVentValve_Open() = LLC TMP GATE OPEN 시 FAST VENT VALVE CLOSE INTERLOCK 에러";
		return LLCTMPGATEOPEN_FASTVENTCLOSEERROR;
	}
	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Is_LLCGateValve_Open() = LLC TMP GATE OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return LLCTMPGATEOPEN_LLCGATECLOSEERROR;
	}
	if (Is_LLC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Get_LLC_DryPump_Status() = LLC TMP GATE OPEN 시 LLC DRY PUMP ON INTERLOCK 에러";
		return LLCTMPGATEOPEN_DRYPUMPERROR;
	}
	if (Is_LLC_TMP_ForelineValve_Open() != VALVE_OPEN)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Is_LLC_TMP_ForelineValve_Open() = LLC TMP GATE OPEN 시 LLC TMP FORELINE OPEN INTERLOCK 에러";
		return LLCTMPGATEOPEN_LLCFORELINEOPENERROR;
	}
	if (Is_LLC_Lid_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Is_LLC_Lid_Open() = LLC TMP GATE OPEN 시 LLC LID CLOSE INTERLOCK 에러";
		return LLCTMPGATEOPEN_LLCLIDCLOSEERROR;
	}
	if (Off_SlowMfc() != 0)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Off_SlowMfc() = LLC TMP GATE OPEN  시 Slow MFC 0ff INTERLOCK 에러";
		return LLCTMPGATEOPEN_SLOWMFCOFFERROR;
	}
	if (Off_FastMfc() != 0)
	{
		Log_str = "Open_LLC_TMP_GateValve() :: Off_FastMfc() = LLC TMP GATE OPEN 시 Fast MFC 0ff INTERLOCK 에러";
		return LLCTMPGATEOPEN_FASTMFCOFFERROR;
	}
	
	//jhkim
	/* LLC SET POINT OUT (VAC) 필요 */
	/* LLC STAGE 확인 필요 */


	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		if (Is_LaserSource_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_TMP_GateValve() :: Is_LaserSource_Open() =  LLC TMP GATE OPEN 시 (TR OPEN) Laser Source Gate Close INTERLOCK 에러";
			return LLCTMPGATEOPEN__TRGATEOPEN_LASERCLOSEERROR;
		}
		if (Is_MC_FastRoughValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_TMP_GateValve() :: Is_MC_FastRoughValve_Open() = LLC TMP GATE OPEN 시 (TR GATE OPEN) MC ROUGH VALVE CLOSE INTERLOCK 에러";
			return LLCTMPGATEOPEN_LLCFASTROUGHCLOSEERROR;
		}
		if (Is_TR_Lid_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_TMP_GateValve() :: Is_TR_Lid_Open() = LLC TMP GATE OPEN 시  (TR OPEN) TR LID CLOSE INTERLOCK 에러";
			return LLCTMPGATEOPEN__TRGATEOPEN_TRLIDCLOSEERROR;
		}
		if (Is_MC_Lid_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_TMP_GateValve() :: Is_MC_Lid_Open() = LLC TMP GATE OPEN 시  (TR OPEN) MC LID CLOSE INTERLOCK 에러";
			return LLCTMPGATEOPEN__TRGATEOPEN_MCLIDCLOSEERROR;
		}
	}
	ResetEvent(m_LLCTmpGate_WriteEvent_Open);

	int nAddr  = 18;
	int nIndex = 5;
	int nData  = 1;

	int nRet = DigitalWriteIO(DO::LLC_TMP_GATE, VALVE_OPEN);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_LLC_TMP_GateValve() :  LLC TMP GATE VALVE OPEN WRITE 에러 발생";
		return LLCTMPGATEOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCTmpGate_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_LLC_TMP_GateValve(): LLC TMP GATE VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_LLC_TMP_GateValve() :  LLC TMP GATE VALVE OPEN 에러 발생 (TimeOut)";
		return LLCTMPGATEOPEN_TIMEOUTERROR;
	}
}

int CIODlg::Open_LLC_TMP_ForelineValve() 
{
	//	IO MAP Y01.06 
	//	OPEN SET 1 m_bDigitalOut[22]

	///////////////////////////////////////////
	//   LLC FORELINE V/V OPEN INTERLOCK    ///
	///////////////////////////////////////////

	// @ LLC DRY PUMP ON  확인										
	// @ LLC ROUGHING CLOSE 확인.
	// @ LLC GATE CLOSE 확인.
	// @ LLC TMP 확인.
	// @ LLC TMP GATE CLOSE 확인.
	// @ SLOW VENT CLOSE 확인.
	// @ FAST VENT CLOSE 확인.
	// @ LLC FORELINE 진공상태 확인 (51B, 722B 확인)				>>>>>>  LLC_LINE_901P_SET , LLC_LINE_722B_SET



	//if (IO_MODE == OPER_MODE_ON)
	if (!g_pVP->m_bMcVentingState)
	{
		if (Is_TRGateValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_TMP_ForelineValve() :: Is_TRGateValve_Open() = LLC TMP FORELINE V/V OPEN 시 TR GATE CLOSE INTERLOCK 에러";
			return LLCFORELINEOPEN_TRGATECLOSEERROR;
		}
	}

	if (Is_LLC_DryPump_Status() != DRYPUMP_RUN)
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :: Get_LLC_DryPump_Status() = LLC TMP FORELINE V/V OPEN 시 LLC DRY PUMP ON INTERLOCK 에러";
		return LLCFORELINEOPEN_DRYPUMPERROR;
	}
	if (Is_LLCGateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :: Is_LLCGateValve_Open() = LLC TMP FORELINE V/V OPEN 시 LLC GATE CLOSE INTERLOCK 에러";
		return LLCFORELINEOPEN_LLCGATECLOSEERROR;
	}

	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :: Open_LLC_TMP_GateValve() = LLC TMP FORELINE V/V OPEN 시 LLC TMP GATE CLOSE INTERLOCK 에러";
		return LLCFORELINEOPEN_LLCTMPGATECLOSEERROR;
	}
	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSE)
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :: Is_LLC_FastRoughValve_Open() = LLC TMP FORELINE V/V OPEN 시 LLC FAST ROUGH VALVE CLOSE INTERLOCK 에러";
		return LLCFORELINEOPEN_LLCFASTROUGHCLOSEERROR;
	}

	//jhkim 
	/* LLC TMP 상태 */
	
	if (Is_TRGateValve_Open() != VALVE_CLOSE)
	{
		if (Is_MC_FastRoughValve_Open() != VALVE_CLOSE)
		{
			Log_str = "Open_LLC_TMP_ForelineValve() :: Is_MC_FastRoughValve_Open() = LLC TMP FORELINE V/V OPEN시 (TR GATE OPEN) MC ROUGH VALVE CLOSE INTERLOCK 에러";
			return LLCFORELINEOPEN__TRGATEOPEN_MCROUGHCLOSEERROR;
		}

	}
	

	ResetEvent(m_LLCForelineGate_WriteEvent_Open);

	int nAddr  = 18;
	int nIndex = 6;
	int nData  = 1;

	int nRet = DigitalWriteIO(DO::LLC_FORELINE, VALVE_OPEN);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :  LLC TMP FORELINE VALVE OPEN WRITE 에러 발생";
		return LLCFORELINEOPEN_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCForelineGate_WriteEvent_Open, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Open_LLC_TMP_ForelineValve(): LLC TMP FORELINE VALVE OPEN 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Open_LLC_TMP_ForelineValve() :  LLC TMP FORELINE VALVE OPEN 에러 발생 (TimeOut)";
		return LLCFORELINEOPEN_TIMEOUTERROR;
	}
}



int CIODlg::Close_LLCGateValve() 
{
	// IO MAP Y01.13 
	//	CLOSE SET 1 m_bDigitalOut[29]
	int nRet = 0;
	int nAddr = 0;
	int nIndex = 0;
	int nData = 0;

	ResetEvent(m_LLCGate_WriteEvent);

	//jhkim
	/*MTS ROBOT RETRACT 확인 해야함*/

	/* LLC GATE OPEN OFF */
	nAddr  = 19;
	nIndex = 4;
	nData  = 0;
	int nRet1  = DigitalWriteIO(DO::LLC_GATE_VALVE_OPEN_STATUS, VALVE_CLOSE);

	/* LLC GATE CLOSE ON */
	nIndex = 5;
	nData  = 1;
	int nRet2 = DigitalWriteIO(DO::LLC_GATE_VALVE_CLOSE_STATUS, VALVE_OPEN);
	
	if (!nRet1 && !nRet2) 
		nRet = IO_WRITE_COMPLETE;
	else 
		nRet = LLCGATECLOSE_WRITEERROR;

	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_LLCGateValve(): LLC GATE VALVE CLOSE WRITE 에러 발생";
		return  LLCGATECLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCGate_WriteEvent, 5000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_LLCGateValve() : LLC GATE VALVE CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_LLCGateValve(): LLC GATE VALVE CLOSE 에러 발생 (TimeOut)";
		return  LLCGATECLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_TRGateValve() 
{
	// IO MAP Y01.15 
	//	CLOSE SET 1 m_bDIgitalOut[31]

	//jhkim
	//interface 함수로 동기화 할것

	/*Vac Robot 팔 위치 확인*/
	bool bVacRobotRetracted = (m_bDigitalIn[DI::VAC_ROBOT_HAND_RE_TO_LLC] && m_bDigitalIn[DI::VAC_ROBOT_HAND_RE_TO_MC]);


	int nRet = 0;
	int nAddr = 19;
	int nIndex = 6;
	int nData = 0;

	if (bVacRobotRetracted == true)
	{
		ResetEvent(m_TRGate_WriteEvent);

		/* TR GATE OPEN OFF */
		nAddr = 19;
		nIndex = 6;
		nData = 0;
		int nRet_OFF = DigitalWriteIO(DO::TR_GATE_VALVE_OPEN_STATUS, VALVE_CLOSE);

		/* TR GATE CLOSE ON */
		nIndex = 7;
		nData  = 1;
		int nRet_ON = DigitalWriteIO(DO::TR_GATE_VALVE_CLOSE_STATUS, VALVE_OPEN);

		if (!nRet_ON && !nRet_OFF) 
			nRet = IO_WRITE_COMPLETE;
		else 
			nRet = TRGATECLOSE_WRITEERROR;
		m_StrReturnValue.Format(_T("%d"), nRet);

		if (nRet != 0)
		{
			Log_str = "Close_TRGateValve() : TR GATE CLOSE WRITE 에러 발생";
			return TRGATECLOSE_WRITEERROR;
		}

		if (WaitForSingleObject(m_TRGate_WriteEvent, 8000) == WAIT_OBJECT_0)
		{
			Log_str = "Close_TRGateValve() : TR GATE VALVE CLOSE 완료";
			return OPERATION_COMPLETED;
		}
		else
		{
			Log_str = "Close_TRGateValve(): TR GATE VALVE CLOSE 에러 발생 (TimeOut)";
			return  TRGATECLOSE_TIMEOUTERROR;
		}
	}
	else
	{
		Log_str = "Close_TRGateValve() : VAC ROBOT ARM RETRACT 상태 확인 요망. Retract가 아니므로 Error발생";
		return TRGATECLOSE_VAC_ARM_RETRACT_ERROr;
	}
}

int CIODlg::Close_SlowVent_Inlet_Valve()
{
	ResetEvent(m_SlowVent_Inlet_WriteEvent_Close);

	int nRet = 0;

	int nAddr  = 20;
	int nIndex = 0;
	int nData  = 0;

	nRet = DigitalWriteIO(DO::SLOW_VENT_INLET, VALVE_CLOSE);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_SlowVent_Inlet_Valve1() :  SLOW VENT INLET CLOSE WRITE 에러 발생";
		return SLOWVENTINLETCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_SlowVent_Inlet_WriteEvent_Close, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_SlowVent_Inlet_Valve1() : SLOW VENT INLET CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_SlowVent_Inlet_Valve1() :  SLOW VENT INLET CLOSE 에러 발생(TimeOut)";
		return SLOWVENTINLETCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_SlowVent_Outlet_Valve()// IO MAP : Y00.10 CLOSE SET 0 m_bDigitalOut[10]  ---> INLET
{
	if (Is_SlowVent_Inlet_Valve_Open() != VALVE_CLOSE)
	{
		Log_str = "Close_SlowVent_Outlet_Valve1() :: Is_SlowVent_Inlet_Valve1_Open() = SLOW VENT OUTLET CLOSE 시 SLOW VENT INLET CLOSE INTERLOCK 에러";
		return SLOWVENTOUTLETCLOSE_SlOWVENTINLETCLOSEERROR;
	}

	int nRet = 0;

	int nAddr  = 20;
	int nIndex = 1;
	int nData  = 0;

	nRet = DigitalWriteIO(DO::SLOW_VENT_OUTLET, VALVE_CLOSE);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_SlowVent_Inlet_Valve1() :   SLOW VENT INLET CLOSE WRITE 에러 발생";
		return SLOWVENTOUTLETCLOSE_WRITEERROR;
	}
	
	Log_str = "Close_SlowVent_Inlet_Valve1() : SLOW VENT INLET CLOSE 완료";
	return OPERATION_COMPLETED;
}

int CIODlg::Close_SlowVentValve()// IO MAP : Y00.10 CLOSE SET 0 m_bDigitalOut[10]  ---> INLET
								  //	      : Y01.04 CLOSE SET 0 m_bDigitalOut[20]  ---> OUTLET
{
	ResetEvent(m_SlowVent_WriteEvent);

	int ADDR1, bitdex1, data1 = -1;
	int nRet = -1;

	

	//OUTLET
	int nAddr  = 20;
	int nIndex = 1;
	int nData  = 0;

	int nRet2 = DigitalWriteIO(DO::SLOW_VENT_OUTLET, VALVE_CLOSE);

	//INLET
	nIndex = 0;
	int nRet1 = DigitalWriteIO(DO::SLOW_VENT_INLET, VALVE_CLOSE);

	if (!nRet1 && !nRet2) 
		nRet = IO_WRITE_COMPLETE;
	else 
		nRet = SLOWVENTCLOSE_WRITEERROR;

	m_StrReturnValue.Format(_T("%d"), nRet);

	if (nRet != 0)
	{
		Log_str = "Close_SlowVentValve1(): SLOW VENT CLOSE WRITE 에러 발생";
		return nRet;
	}
	
	//int ret = WaitEvent(3000);
	if (WaitForSingleObject(m_SlowVent_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_SlowVentValve1() : SLOW VENT CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_SlowVentValve1() : SLOW VENT CLOSE 에러 발생 (TimeOut)";
		return SLOWVENTCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_FastVent_Inlet_Valve()// IO MAP : Y00.10 CLOSE SET 0 m_bDigitalOut[10]  ---> INLET
{
	ResetEvent(m_FastVent_Inlet_WriteEvent_Close);

	int nAddr  = 20;
	int nIndex = 2;
	int nData  = 0;

	int nRet = DigitalWriteIO(DO::FAST_VENT_INLET, VALVE_CLOSE);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_FastVent_Inlet_Valve() :   FAST VENT INLET CLOSE WRITE 에러 발생";
		return FASTVENTINLETCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_FastVent_Inlet_WriteEvent_Close, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_FastVent_Inlet_Valve() : FAST VENT Inlet CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_FastVent_Inlet_Valve() : FAST VENT Inlet CLOSE 에러 발생(TimeOut)";
		return FASTVENTINLETCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_FastVent_Outlet_Valve()// IO MAP : Y00.10 CLOSE SET 0 m_bDigitalOut[10]  ---> INLET
{
	int nAddr  = 20;
	int nIndex = 3;
	int nData  = 0;

	int nRet = DigitalWriteIO(DO::FAST_VENT_OUTLET, VALVE_CLOSE);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_SlowVent_Inlet_Valve1() :   FAST VENT OUTLET CLOSE WRITE 에러 발생";
		return FASTVENTOUTLETCLOSE_WRITEERROR;
	}
	
	Log_str = "Close_SlowVent_Inlet_Valve1() : FAST VENT OUTLET CLOSE 완료";
	return OPERATION_COMPLETED;
}

int CIODlg::Close_FastVentValve()// IO MAP : Y00.09 CLOSE SET 0 m_bDigitalOut[9]  ----> INLET
								   //        : Y01.03 CLOSE SET 0 m_bdigitalOut[19] ----> OUTLET 
{
	ResetEvent(m_FastVent_WriteEvent);

	int nRet = 0;

	//OUTLET
	int nAddr  = 20;
	int nIndex = 3;
	int nData  = 0;

	int nRet2 = DigitalWriteIO(DO::FAST_VENT_OUTLET, VALVE_CLOSE);

	//INLET
	nIndex = 2;

	int nRet1 = DigitalWriteIO(DO::FAST_VENT_INLET, VALVE_CLOSE);

	if (!nRet1 && !nRet2) 
		nRet = 0;
	else 
		nRet = -1;
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_FastVentValve1()_OUTLET : FAST VENT WRITE CLOSE 에러 발생";
		return FASTVENTCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_FastVent_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_FastVentValve() : FAST VENT CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_FastVentValve() : FAST VENT CLOSE 에러 발생 (TimeOut)";
		return FASTVENTCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_MC_SlowRoughValve()
{
	int nAddr  = 19;
    int nIndex = 0;
	int nData  = 0;

	ResetEvent(m_MCSlowRough_WriteEvent);

	int nRet = DigitalWriteIO(DO::MC_SLOW_ROUGHING, VALVE_CLOSE);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_MC_FastRoughValve() :  MC SLOW ROUGH VALVE WRITE CLOSE 에러 발생";
		return MCSLOWROUGHCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_MCSlowRough_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_MC_SlowRoughValve() :  MC SLOW ROUGH VALVE CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_MC_SlowRoughValve() : MC SLOW ROUGH VALVE CLOSE 에러 발생 (TimeOut)";
		return MCSLOWROUGHCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_MC_FastRoughValve() // IO MAP : Y00.12 CLOSE SET 0 m_bDigitalOut[12] 
{
	int nAddr  = 19;
	int nIndex = 1;
	int nData  = 0;

	ResetEvent(m_MCFastRough_WriteEvent);

	int nRet = DigitalWriteIO(DO::MC_FAST_ROUGHING, VALVE_CLOSE);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_MC_FastRoughValve() :  MC FAST ROUGH VALVE WRITE CLOSE 에러 발생";
		return MCFASTROUGHCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_MCFastRough_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_MC_FastRoughValve() :  MC FAST ROUGH VALVE CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_MC_FastRoughValve() :  MC FAST ROUGH VALVE CLOSE 에러 발생 (TimeOut)";
		return MCFASTROUGHCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_MC_TMP_GateValve() // IO MAP : Y01.07 CLOSE SET 0 m_bDigitalOut[23]
{
	int nAddr  = 19;
	int nIndex = 2;
	int nData  = 0;

	ResetEvent(m_MCTmpGate_WriteEvent);

	int nRet = DigitalWriteIO(DO::MC_TMP_GATE, VALVE_CLOSE);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_MC_TMP1_GateValve() :  MC TMP GATE VALVE WRITE CLOSE 에러 발생";
		return MCTMPGATECLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_MCTmpGate_WriteEvent, 5000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_MC_TMP1_GateValve() :  MC TMP GATE VALVE CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_MC_TMP1_GateValve() :  MC TMP GATE VALVE CLOSE 에러 발생 (TimeOut)";
		return MCTMPGATECLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_MC_TMP_ForelineValve()  // IO MAP : Y00.14 CLOSE SET 0 m_bDigitalOut[14] 
{
	int nAddr = 19;
	int nIndex = 3;
	int nData  = 0;

	ResetEvent(m_MCForelineGate_WriteEvent);

	int nRet = DigitalWriteIO(DO::MC_FORELINE, VALVE_CLOSE);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_MC_TMP1_ForelineValve() :  MC TMP FORELINE VALVE WRITE CLOSE 에러 발생";
		return MCFORELINECLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_MCForelineGate_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_MC_TMP1_ForelineValve() :  MC TMP FORELINE VALVE CLOSE 완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_MC_TMP1_ForelineValve() :  MC TMP FORELINE VALVE CLOSE 발생 (TimeOut)";
		return MCFORELINECLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_LLC_SlowRoughValve()
{
	int nAddr  = 18;
	int nIndex = 3;
	int nData  = 0;

	ResetEvent(m_LLCSlowRough_WriteEvent);

	int nRet = DigitalWriteIO(DO::LLC_SLOW_ROUGHING, VALVE_CLOSE);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_LLC_SlowRoughValve() :  LLC SLOW ROUGH VALVE WRITE CLOSE 에러 발생";
		return LLCSLOWROUGHCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCSlowRough_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_LLC_SlowRoughValve() :  LLC SLOW ROUGH VALVE CLOSE  완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_LLC_SlowRoughValve() :  LLC SLOW ROUGH VALVE CLOSE  발생 (TimeOut)";
		return LLCSLOWROUGHCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_LLC_FastRoughValve() // IO MAP : Y00.08 CLOSE SET 0 m_bDigitalOut[8]
{
	int nAddr  = 18;
	int nIndex = 4;
	int nData  = 0;
	
	ResetEvent(m_LLCFastRough_WriteEvent);

	int nRet = DigitalWriteIO(DO::LLC_FAST_ROUGHING, VALVE_CLOSE);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_LLC_FastRoughValve() :  LLC FAST ROUGH VALVE WRITE CLOSE 에러 발생";
		return LLCFASTROUGHCLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCFastRough_WriteEvent, 5000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_LLC_FastRoughValve() :  LLC FAST ROUGH VALVE CLOSE  완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_LLC_FastRoughValve() :  LLC FAST ROUGH VALVE CLOSE  에러 발생 (TimeOut)";
		return LLCFASTROUGHCLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_LLC_TMP_GateValve() // IO MAP : Y01.06 CLOSE SET 0 m_bDigitalOut[22]
{
	int nAddr  = 18;
	int nIndex = 5;
	int nData  = 0;

	ResetEvent(m_LLCTmpGate_WriteEvent);

	int nRet = DigitalWriteIO(DO::LLC_TMP_GATE, VALVE_CLOSE);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_LLC_TMP_GateValve() :  LLC TMP GATE VALVE WRITE CLOSE 에러 발생";
		return LLCTMPGATECLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCTmpGate_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_LLC_TMP_GateValve() :  LLC TMP GATE VALVE CLOSE  완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_LLC_TMP_GateValve() :  LLC TMP GATE VALVE CLOSE 에러 발생 (TimeOut)";
		return LLCTMPGATECLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Close_LLC_TMP_ForelineValve() // IO MAP : Y00.13 CLOSE SET 0 m_bDigitalOut[13]
{
	int nAddr  = 18;
	int nIndex = 6;
	int nData  = 0;

	ResetEvent(m_LLCForelineGate_WriteEvent);

	int nRet = DigitalWriteIO(DO::LLC_FORELINE, VALVE_CLOSE);
	m_StrReturnValue.Format(_T("%d"), nRet);
	if (nRet != 0)
	{
		Log_str = "Close_LLC_TMP_ForelineValve()  :  LLC TMP FORELINE VALVE WRITE CLOSE 에러 발생";
		return LLCFORELINECLOSE_WRITEERROR;
	}
	
	if (WaitForSingleObject(m_LLCForelineGate_WriteEvent, 3000) == WAIT_OBJECT_0)
	{
		Log_str = "Close_LLC_TMP_ForelineValve() : LLC TMP FORELINE VALVE CLOSE  완료";
		return OPERATION_COMPLETED;
	}
	else
	{
		Log_str = "Close_LLC_TMP_ForelineValve() :  LLC TMP FORELINE VALVE CLOSE 에러 발생(TimeOut)";
		return LLCFORELINECLOSE_TIMEOUTERROR;
	}
}

int CIODlg::Is_SiganlTowerRed()
{
	int ret = 0;

	if (m_bDigitalOut[DO::SIGNAL_TOWER_RED] == true)
		ret = VALVE_OPEN;
	else
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_SiganlTowerYellow()
{
	int ret = 0;

	if (m_bDigitalOut[DO::SIGNAL_TOWER_YELLOW] == true)
		ret = VALVE_OPEN;
	else
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_SiganlTowerGreen()
{
	int ret = 0;

	if (m_bDigitalOut[DO::SIGNAL_TOWER_GREEN] == true)
		ret = VALVE_OPEN;
	else
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_SiganlTowerBlue()
{
	int ret = 0;

	if (m_bDigitalOut[DO::SIGNAL_TOWER_BUZZER_1] == true)
		ret = VALVE_OPEN;
	else
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_SiganlTowerBuzzer()
{
	int ret = 0;

	if (m_bDigitalOut[DO::SIGNAL_TOWER_BUZZER_2] == true)
		ret = VALVE_OPEN;
	else
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_DVR_Valve(int DVRNum)
{
	int ret = 0;
	switch (DVRNum)
	{
	case 1:
		if (m_bDigitalOut[DO::DVR_CAM1_SW] == true)
			ret = VALVE_OPEN;
		else
			ret = VALVE_CLOSE;
		break;
	case 2:
		if (m_bDigitalOut[DO::DVR_CAM2_SW] == true)
			ret = VALVE_OPEN;
		else
			ret = VALVE_CLOSE;
		break;
	case 3:
		if (m_bDigitalOut[DO::DVR_CAM3_SW] == true)
			ret = VALVE_OPEN;
		else
			ret = VALVE_CLOSE;
		break;
	case 4:
		if (m_bDigitalOut[DO::DVR_CAM4_SW] == true)
			ret = VALVE_OPEN;
		else
			ret = VALVE_CLOSE;
		break;
	default:
		break;
	}

	return ret;
}


int CIODlg::Is_LLCGateValve_Open()
{
	int ret = 0;
	
	bool bGateOpen  = m_bDigitalIn[DI::LLC_GATE_VALVE_OPEN];
	bool bGateClose = m_bDigitalIn[DI::LLC_GATE_VALVE_CLOSE];

	ret = Valve_Check(bGateOpen, bGateClose);

	return ret;
}

int CIODlg::Is_TRGateValve_Open()
{
	int ret = 0;

	bool bGateOpen  = m_bDigitalIn[DI::TR_GATE_VALVE_OPEN];
	bool bGateClose = m_bDigitalIn[DI::TR_GATE_VALVE_CLOSE];

	ret = Valve_Check(bGateOpen, bGateClose);

	return ret;
}

int CIODlg::Is_SlowVent_Inlet_Valve_Open()
{
	int ret = 0;

	if (m_bDigitalOut[DO::SLOW_VENT_INLET] == true) 
		ret = VALVE_OPEN;
	else  
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_SlowVent_Outlet_Valve_Open()
{
	int ret = 0;

	if (m_bDigitalOut[DO::SLOW_VENT_OUTLET] == true) 
		ret = VALVE_OPEN;
	else  
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_SlowVentValve_Open()
{
	int ret = 0;

	bool bSlowVentInlet  = m_bDigitalOut[DO::SLOW_VENT_INLET];
	bool bSlowVentOutlet = m_bDigitalOut[DO::SLOW_VENT_OUTLET];

	if (bSlowVentInlet == false && bSlowVentOutlet == false)
		ret = VALVE_CLOSE;
	else if (bSlowVentInlet == true || bSlowVentOutlet == true)
		ret = VALVE_OPEN;
	else 
		ret = VALVE_OPEN;

	return ret;
}

int CIODlg::Is_FastVent_Inlet_Valve_Open()
{
	int ret = 0;

	bool bState = m_bDigitalOut[DO::FAST_VENT_INLET];

	if (bState) 
		ret = VALVE_OPEN;
	else  
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_FastVent_Outlet_Valve_Open()
{
	int ret = 0;

	bool bState = m_bDigitalOut[DO::FAST_VENT_OUTLET];

	if (bState) 
		ret = VALVE_OPEN;
	else  
		ret = VALVE_CLOSE;

	return ret;
}



int CIODlg::Is_FastVentValve_Open()
{
	int ret = 0;

	bool bFastVentInlet = m_bDigitalOut[DO::FAST_VENT_INLET];
	bool bFastVentOutlet = m_bDigitalOut[DO::FAST_VENT_OUTLET];

	if ((bFastVentInlet == false) && (bFastVentOutlet == false))
		ret = VALVE_CLOSE;
	else if ((bFastVentInlet == true) || (bFastVentOutlet == true))
		ret = VALVE_OPEN;
	else 
		ret = VALVE_OPEN;

	return ret;
}

int CIODlg::Is_MC_SlowRoughValve_Open()
{
	int ret = 0;
	
	bool bMcSlowRoughState = m_bDigitalOut[DO::MC_SLOW_ROUGHING]; //1 open , 0 close

	if (bMcSlowRoughState != false)
		ret = VALVE_OPEN;
	else 
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_MC_FastRoughValve_Open()
{
	int ret = 0;

	bool bMcFastRoughValveOpen = m_bDigitalIn[DI::MC_FAST_ROUGHING_VALVE_OPEN];
	bool bMcRoughValveClose = m_bDigitalIn[DI::MC_ROUGHING_VALVE_CLOSE];

	ret = Valve_Check(bMcFastRoughValveOpen, bMcRoughValveClose);

	return ret;
}

int CIODlg::Is_MC_TMP_GateValve_Open()
{
	int ret = 0;

	bool bOpened = m_bDigitalIn[DI::MC_TMP_GATE_VALVE_OPEN];
	bool bClosed = m_bDigitalIn[DI::MC_TMP_GATE_VALVE_CLOSE];

	ret = Valve_Check(bOpened, bClosed);

	return ret;
}

int CIODlg::Is_MC_TMP_ForelineValve_Open()
{
	int ret = 0;

	bool bOpened = m_bDigitalIn[DI::MC_FORELINE_VALVE_OPEN];
	bool bClosed = m_bDigitalIn[DI::MC_FORELINE_VALVE_CLOSE];

	ret = Valve_Check(bOpened, bClosed);

	return ret;
}


int CIODlg::Is_LLC_SlowRoughValve_Open()
{
	int ret = 0;

	bool bState = m_bDigitalOut[DO::LLC_SLOW_ROUGHING]; //1 open , 0 close

	if (bState != false)
		ret = VALVE_OPEN;
	else 
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_LLC_FastRoughValve_Open()
{
	int ret = 0;

	bool bOpened = m_bDigitalIn[DI::LLC_FAST_ROUGHING_VALVE_OPEN];
	bool bClosed = m_bDigitalIn[DI::LLC_ROUGHING_VALVE_CLOSE];

	ret = Valve_Check(bOpened, bClosed);

	return ret;
}

int CIODlg::Is_LLC_TMP_GateValve_Open()
{
	int ret = 0;

	bool bOpened = m_bDigitalIn[DI::LLC_TMP_GATE_VALVE_OPEN];
	bool bClosed = m_bDigitalIn[DI::LLC_TMP_GATE_VALVE_CLOSE];

	ret = Valve_Check(bOpened, bClosed);

	return ret;
}

int CIODlg::Is_LLC_TMP_ForelineValve_Open()
{
	int ret = 0;

	bool bOpened = m_bDigitalIn[DI::LLC_FORELINE_VALVE_OPEN];
	bool bClosed = m_bDigitalIn[DI::LLC_FORELINE_VALVE_CLOSE];

	ret = Valve_Check(bOpened, bClosed);

	return ret;

}

int CIODlg::Is_LaserSource_Open()
{
	int ret = 0;
	
	bool bOpened = m_bDigitalIn[DI::LASER_VALVE_OPEN_STATUS];
	bool bClosed = m_bDigitalIn[DI::LASER_VALVE_CLOSE_STATUS];

	ret = Valve_Check(bOpened, bClosed);

	return ret;
}


int CIODlg::Is_Water_Valve_Open()
{
	int ret = 0;

	bool bSupplyValve = m_bDigitalOut[DO::WATER_SUPPLY_VALVE];
	bool bReturnValve = m_bDigitalOut[DO::WATER_RETURN_VALVE];

	if (bSupplyValve && bReturnValve)
		ret = VALVE_OPEN;
	else if (!bSupplyValve || !bReturnValve)
		ret = VALVE_CLOSE;
	else
		ret = -1;

	return ret;
}

int CIODlg::Is_Mask_OnChuck()
{
	int ret = -1;

	bool bMaskOnChuck = m_bDigitalIn[DI::MC_MASK_CHECK];		// 0 이면 마스크 확인, 1 이면 마스크 없음
	bool bTiltSensor1 = m_bDigitalIn[DI::MC_MASK_TILT1];		// 0 이면 마스크 기울기 감지됨, 1 이면 마스크 기울기 감지 X
	bool bTiltSensor2 = m_bDigitalIn[DI::MC_MASK_TILT2];		// 0 이면 마스크 기울기 감지됨, 1 이면 마스크 기울기 감지 X

	if (!bMaskOnChuck) 
		ret = MASK_ON;
	if (bMaskOnChuck) 
		ret = MASK_NONE;
	if (!bTiltSensor1) 
		ret = MASK_SLANT;
	if (!bTiltSensor2) 
		ret = MASK_SLANT;

	return ret;
}

int CIODlg::Is_Mask_Check_Only_OnChuck()
{
	int ret = -1;

	bool bMaskOnChuck = m_bDigitalIn[DI::MC_MASK_CHECK];
	if (!bMaskOnChuck)
		ret = MASK_ON;
	else 
		ret = MASK_NONE;

	return ret;
}

int CIODlg::Is_Mask_OnVMTR()
{
	int ret = -1;

	// 0 이면 마스크 감지
	// 1 이면 마스크 없음
	bool bMaskOnChuck = m_bDigitalIn[DI::VAC_ROBOT_MASK_CHECK_TO_MC];
	if (!bMaskOnChuck)
		ret = MASK_ON;
	else
		ret = MASK_NONE;

	return ret;
}

int CIODlg::Is_Mask_OnVMTR_LLC()
{
	int ret = -1;
	// 0 이면 마스크 감지
	// 1 이면 마스크 없음
	bool bMaskOnChuck = m_bDigitalIn[DI::VAC_ROBOT_MASK_CHECK_TO_LLC];
	if (!bMaskOnChuck)
		ret = MASK_ON;
	else
		ret = MASK_NONE;

	return ret;
}


int CIODlg::Is_Mask_OnLLC()
{
	int ret = -1;

	bool bMaskOnChuck = m_bDigitalIn[DI::LLC_MASK_CHECK];	// 0 이면 마스크 확인, 1 이면 마스크 없음
	bool bTilt1 = m_bDigitalIn[DI::LLC_MASK_TILT1];			// 0 이면 마스크 기울기 감지됨, 1 이면 마스크 기울기 감지 X
	bool bTilt2 = m_bDigitalIn[DI::LLC_MASK_TILT2];			// 0 이면 마스크 기울기 감지됨, 1 이면 마스크 기울기 감지 X

	if (!bMaskOnChuck) 
		ret = MASK_ON;
	if (bMaskOnChuck) 
		ret = MASK_NONE;
	if (!bTilt1) 
		ret = MASK_SLANT;
	if (!bTilt2) 
		ret = MASK_SLANT;
	
	return ret;
}


int CIODlg::Is_Mask_Check_Only_OnLLC()
{
	int ret = -1;

	bool bMaskOnChuck = m_bDigitalIn[DI::LLC_MASK_CHECK];
	if (!bMaskOnChuck)
		ret = MASK_ON;
	else 
		ret = MASK_NONE;

	return ret;
}

//jhkim 
// interlock sensor 확인 필요.
int CIODlg::Is_Isolator_Up()
{
	int ret = 0;
	
	//bool bState = m_bDigitalIn[DI::ISOLATOR_INTERLOCK_SENSOR];
	bool bState = true;

	if (bState) 
		ret = 1; //ISOLATOR UP STATE
	else 
		ret = 0;

	return ret;
}

int CIODlg::Is_LLC_DryPump_Status()
{
	int ret = 0;

	//bool DRY_ON = m_bDigitalIn[16];
	//bool DRY_ALARM = m_bDigitalIn[17];
	//bool DRY_WARNING = m_bDigitalIn[18];

	bool bDryOn		 = m_bDigitalIn[DI::LLC_DRY_PUMP_ON_STATUS];
	bool bDryAlarm	 = m_bDigitalIn[DI::LLC_DRY_PUMP_ALARM_STATUS];
	bool bDryWarning = m_bDigitalIn[DI::LLC_DRY_PUMP_WARNNING_STATUS];


	// DRY PUMP ON 일시 ON signal 점등.
	// ALARM, WARNING 도 ON 과 함께 점등. -> 발생시 소등 됨.

	if (bDryOn) 
		ret = DRYPUMP_RUN;
	if (!bDryOn) 
		ret = DRYPUMP_STOP;
	if (!bDryAlarm) 
		ret = DRYPUMP_ERROR;
	if (!bDryWarning) 
		ret = DRYPUMP_WARNING;

	//KJH
	//return DRYPUMP_RUN;

	return ret;

}

int CIODlg::Is_MC_DryPump_Status()
{
	int ret = 0;

	bool bDryOn = m_bDigitalIn[DI::MC_DRY_PUMP_ON_STATUS];
	bool bDryAlarm = m_bDigitalIn[DI::MC_DRY_PUMP_ALARM_STATUS];
	bool bDryWarning = m_bDigitalIn[DI::MC_DRY_PUMP_WARNNING_STATUS];

	if (bDryOn) 
		ret = DRYPUMP_RUN;
	if (!bDryOn) 
		ret = DRYPUMP_STOP;
	if (!bDryAlarm) 
		ret = DRYPUMP_ERROR;
	if (!bDryWarning) 
		ret = DRYPUMP_WARNING;

	return ret;
}

bool CIODlg::Is_ATM_Robot_Arm_Extend()
{
	bool bState = m_bDigitalIn[DI::ATM_ROBOT_HAND_RE];

	if (bState)
		return FALSE;
	else
		return TRUE;
}


bool CIODlg::Is_ATM_Robot_Arm_Retract()
{
	bool bState = m_bDigitalIn[DI::ATM_ROBOT_HAND_RE];

	if (bState) 
		return TRUE;
	else 
		return FALSE;
}

bool CIODlg::Is_VAC_Robot_Arm_Extend()
{
	bool Retracted_Llc = m_bDigitalIn[DI::VAC_ROBOT_HAND_RE_TO_LLC];
	bool Retracted_Mc = m_bDigitalIn[DI::VAC_ROBOT_HAND_RE_TO_MC];

	if ((!Retracted_Llc || !Retracted_Mc))
		return TRUE;
	else 
		return FALSE;
}

bool CIODlg::Is_VAC_Robot_Arm_Retract()
{
	bool Retracted_Llc = m_bDigitalIn[DI::VAC_ROBOT_HAND_RE_TO_LLC];
	bool Retracted_Mc = m_bDigitalIn[DI::VAC_ROBOT_HAND_RE_TO_MC];

	if ((Retracted_Llc && Retracted_Mc))
		return TRUE;
	else 
		return FALSE;
}


bool CIODlg::Is_VAC_Robot_LLC_Arm_Retract()
{
	bool Retracted_Llc = m_bDigitalIn[DI::VAC_ROBOT_HAND_RE_TO_LLC];

	if (Retracted_Llc)
		return TRUE;
	else
		return FALSE;
}


bool CIODlg::Is_VAC_Robot_MC_Arm_Retract()
{
	bool Retracted_Mc = m_bDigitalIn[DI::VAC_ROBOT_HAND_RE_TO_MC];

	if (Retracted_Mc)
		return TRUE;
	else
		return FALSE;
}

int CIODlg::Is_SourceGate_OpenOn_Check()
{
	if (g_pGauge_IO->GetMcVacuumRate() <= g_pConfig->m_dPressure_Rough_End) 
		return VALVE_OPEN;
	else 
		return VALVE_CLOSE;
}

int CIODlg::Is_Water_Supply_Valve_Status()
{
	int ret = 0;

	bool bState = m_bDigitalOut[DO::WATER_SUPPLY_VALVE];
	if (bState)
		ret = VALVE_OPEN;
	else
		ret = VALVE_CLOSE;

	return ret;
}
int CIODlg::Is_Water_Supply_Status()
{
	int ret = 0;

	bool bState = m_bDigitalIn[DI::WATER_FLOW_SW];
	if (bState) 
		ret = RUN;
	else 
		ret = RUN_OFF;

	return ret;
}

int CIODlg::Is_Water_Temp_Alarm_Status()
{
	int ret = 0;

	bool bState = m_bDigitalIn[DI::WATER_TEMP_ALARM];
	if (bState)
		ret = RUN;
	else
		ret = RUN_OFF;

	return ret;
}

int CIODlg::Is_Air_Supply_Status()
{
	int ret = 0;

	bool bState = m_bDigitalIn[DI::MAIN_AIR_SW];
	if (bState)
		ret = RUN;
	else 
		ret = RUN_OFF;

	return ret;
}

int CIODlg::Is_Tmp_Leak_Status()
{
	int ret = 0;

	bool bLlcTmpLeakCheck = m_bDigitalIn[DI::WATER_LEAK_SENSOR1_LLC_TMP];
	bool bMcTmpLeakCheck = m_bDigitalIn[DI::WATER_LEAK_SENSOR2_MC_TMP];

	// 1 : 정상 구동 상태.
	// 0 : leak  발생

	if (!bLlcTmpLeakCheck || !bMcTmpLeakCheck) 
		ret = RUN_OFF; // LEAK 발생
	else 
		ret = RUN; // 정상 구동 상태
	   
	return ret;
}

int CIODlg::Is_Tmp_LLC_Leak_Status()
{
	int ret = 0;

	bool bLlcTmpLeakCheck = m_bDigitalIn[DI::WATER_LEAK_SENSOR1_LLC_TMP];


	// 1 : 정상 구동 상태.
	// 0 : leak  발생

	if (bLlcTmpLeakCheck) 
		ret = RUN; 
	else 
		ret = RUN_OFF; 

	return ret;
}

int CIODlg::ls_Tmp_MC_Leak_Status()
{
	int ret = 0;

	bool bMcTmpLeakCheck = m_bDigitalIn[DI::WATER_LEAK_SENSOR2_MC_TMP];

	// 1 : 정상 구동 상태.
	// 0 : leak  발생

	if (bMcTmpLeakCheck) 
		ret = RUN;
	else 
		ret = RUN_OFF; 

	return ret;
}

int CIODlg::Is_Ac_Rack_detect_Status()
{
	int ret = 0;

	bool bSmokeCheckCB = m_bDigitalIn[DI::AC_RACK_SMOKE_DETECTOR];

	// 0 : 정상 구동 상태.
	// 1 : leak  발생

	if (bSmokeCheckCB)
		ret = RUN_OFF; // DETACT 발생
	else
		ret = RUN; // 정상 구동 상태

	return ret;
}
int CIODlg::Is_Control_Rack_detect_Status()
{
	int ret = 0;

	bool bSmokeCheckControl = m_bDigitalIn[DI::CONTROL_RACK_SMOKE_DETECTOR];

	// 0 : 정상 구동 상태.
	// 1 : leak  발생

	if (bSmokeCheckControl)
		ret = RUN_OFF; // DETACT 발생
	else
		ret = RUN; // 정상 구동 상태

	return ret;
}
int CIODlg::Is_detect_Status()
{
	int ret = 0;

	bool bSmokeCheckCB  = m_bDigitalIn[DI::AC_RACK_SMOKE_DETECTOR];
	bool bSmokeCheckVAC = m_bDigitalIn[DI::CONTROL_RACK_SMOKE_DETECTOR]; 

	// 1 : 정상 구동 상태.
	// 0 : leak  발생

	if (!bSmokeCheckCB)
		ret = 0; // DETACT 발생
	else 
		ret = 1; // 정상 구동 상태

	return ret;
}

int CIODlg::Is_MC_Lid_Open()
{
	int ret = 0;

	bool bState = m_bDigitalIn[DI::MC_LID_OPEN]; //1 Close , 0 Open

	if (bState)
		ret = VALVE_CLOSE;
	else
		ret = VALVE_OPEN;

	return ret;
}

int CIODlg::Is_TR_Lid_Open()
{
	int ret = 0;

	bool bState = m_bDigitalIn[DI::TR_LID_OPEN];  //1 Close , 0 Open

	if (bState)
		ret = VALVE_CLOSE;
	else
		ret = VALVE_OPEN;

	return ret;
}

int CIODlg::Is_LLC_Lid_Open()
{
	int ret = 0;

	bool bState = m_bDigitalIn[DI::LL_LID_OPEN];  //1 Close , 0 Open

	if (bState)
		ret = VALVE_CLOSE;
	else
		ret = VALVE_OPEN;

	return ret;
}

int CIODlg::Is_Llc_SetPointStatus()
{
	int ret = 0;

	bool ATM = m_bDigitalOut[DO::LLC_SET_POINT_OUT_1_ATM]; 
	bool VAC = m_bDigitalOut[DO::LLC_SET_POINT_OUT_2_VAC]; 

	if ((ATM == true) && (VAC == false))
		ret = CHAMBER_VENTED;
	else if ((ATM == false) && (VAC == true))
		ret = CHAMBER_PUMPED;
	else
		ret = CHAMBER_MIDDLE_PUMPED;

	return ret;
}
int CIODlg::Is_Mc_SetPointStatus()
{
	int ret = 0;

	bool ATM = m_bDigitalOut[DO::MC_SET_POINT_OUT_1_ATM];
	bool VAC = m_bDigitalOut[DO::MC_SET_POINT_OUT_2_VAC];

	if ((ATM == true) && (VAC == false))
		ret = CHAMBER_VENTED;
	else if ((ATM == false) && (VAC == true))
		ret = CHAMBER_PUMPED;
	else
		ret = CHAMBER_MIDDLE_PUMPED;

	return ret;
}

double CIODlg::Is_AC_Rack_TempCheck()
{
	double Diff = 20.0;
	double nCh_9 = (double)(((g_pIO->m_nDecimal_Analog_in_ch_9) / 10.0) - Diff); // AC RACK temp
	return nCh_9;
}

double CIODlg::Is_Control_Bok_TempCheck()
{
	double Diff = 20.0;
	double nCh_10 = (double)(((g_pIO->m_nDecimal_Analog_in_ch_10) / 10.0) - Diff); // Contorl Temp
	return nCh_10;
}

// Stage loading position check
int CIODlg::Is_Loading_Position_Status()
{
	int ret = -1;

	bool bLoadingPos = g_pNavigationStage->Is_Loading_Positioin();

	// 1 : Loading Position.
	// 0 : Loading Position 아님.

	if (!bLoadingPos) 
		ret = FALSE; 
	else 
		ret = TRUE; 

	return ret;
}

void CIODlg::Main_Error_Status()
{
	// MAIN AIR , MAIN WATER 상태 확인.
	// WATER_SUPPLY_VALVE , WATER_RETURN_VALVE 상태 확인.
	// LEAK , DETACT 상태 확인.
	char* str;




	//KJH TEST 
	// TRUE 정상, 

	str = "MAIN_ERROR_ON :: Main_Error_Status()";

	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(str)));

	BOOL Air_supply_state = Is_Air_Supply_Status(); 
	BOOL Water_supply_state = Is_Water_Supply_Status(); 
	BOOL Water_Valve_state = Is_Water_Valve_Open();
	BOOL Leak_state = Is_Tmp_Leak_Status();
	BOOL Detact_state = Is_detect_Status();
	
	CString air;
	CString water_supply;
	CString water_valve;
	CString leak;
	CString Detact;

	air.Format("%d", Air_supply_state);
	water_supply.Format("%d", Water_supply_state);
	water_valve.Format("%d", Water_Valve_state);
	leak.Format("%d", Leak_state);
	Detact.Format("%d", Detact_state);

	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Air_Supply_Status-- - 상태 값::" + air)));
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Water_Supply_Status --- 상태 값 ::" + water_supply)));
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Is_Water_Valve_Open --- 상태 값 ::" + water_valve)));
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Leak_Status --- 상태 값 ::"+ leak)));
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Leak_Status --- 상태 값 ::" + Detact)));

	//////////////////
	if (Is_Air_Supply_Status() == RUN_OFF || Is_Water_Supply_Status() == RUN_OFF || Is_Water_Valve_Open() == RUN_OFF || Is_Tmp_Leak_Status() == RUN_OFF || Is_detect_Status() == RUN_OFF)
	{
		str = " MAIN_ERROR_ON() : Main Error 발생 점검 필요 !!";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Air_Supply_Status ERROR --- 상태 값 ::" + air)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Water_Supply_Status ERROR --- 상태 값 ::" + water_supply)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Is_Water_Valve_Open ERROR --- 상태 값 ::" + water_valve)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Leak_Status ERROR --- 상태 값 ::" + leak)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)("Get_Leak_Status ERROR --- 상태 값 ::" + Detact)));

		//::AfxMessageBox(str);

		//if (Error_On() != TRUE)
		//{
		//	str = " MAIN_ERROR_ON() : Main Error 발생 후 인터락 명령 실행 에러 발생 !";
		//	SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(str)));
		//	g_pLog->Display(0, str);
		//}
	}
}

int CIODlg::Is_Water_Supply_Valve_Open()
{
	int ret = 0;

	bool bSupplyValve = m_bDigitalOut[DO::WATER_SUPPLY_VALVE];
	if (bSupplyValve)
		ret = VALVE_OPEN;
	else 
		ret = VALVE_CLOSE;

	return ret;
}

int CIODlg::Is_Water_Return_Valve_Open()
{
	int ret = 0;

	bool bReturnValve = m_bDigitalOut[DO::WATER_RETURN_VALVE];
	if (bReturnValve)
		ret = VALVE_OPEN;
	else 
		ret = VALVE_CLOSE;

	return ret;
}

bool CIODlg::Is_LLC_Atm_Check()
{
	bool bLlcAtmSensorCheck = m_bDigitalIn[DI::LLC_ATM_SW];

	;
	if (bLlcAtmSensorCheck)
	{
		m_bLLCAtmFlag = true;
		return TRUE;			//ATM 상태
	}
	else
		return FALSE;			// ATM 아닌 상태
}
bool CIODlg::Is_LLC_Vac_Check()
{
	double llcVacuumRate = g_pGauge_IO->GetLlcVacuumRate();
	if (llcVacuumRate <= 0.00003)
	{
		m_bLLCAtmFlag = false;
		return TRUE;		// VAC 상태 0.000009   -> 9.0E-6
	}
	else
		return FALSE;		// VAC 아닌 상태
}

bool CIODlg::Is_MC_Atm_Check()
{
	bool bMcAtmSensorCheck = m_bDigitalIn[DI::MC_ATM_SW];

	if (bMcAtmSensorCheck)
	{
		m_bMCAtmFlag = true;
		return TRUE;			//ATM 상태
	}
	else
		return FALSE;			// ATM 아닌 상태
}

bool CIODlg::Is_MC_Vac_Check()
{
	if (g_pGauge_IO->GetMcVacuumRate() <= 0.00003)
	{
		m_bMCAtmFlag = false;
		return TRUE;				// VAC 상태 0.000009   -> 9.0E-6
	}
	else
		return FALSE;				// VAC 아닌 상태
}

bool CIODlg::Is_LLC_DryLine_VAC_Check()
{
	bool DryLine = g_pIO->m_bDigitalIn[DI::LLC_LINE_PRESSURE_SW];

	if (DryLine)
		return TRUE;	// 진공상태 1
	else
		return FALSE;	 // 대기상태 0
}

bool CIODlg::Is_MC_DryLine_VAC_Check()
{
	bool DryLine = g_pIO->m_bDigitalIn[DI::MC_LINE_PRESSURE_SW];

	if (DryLine)
		return TRUE;	// 진공상태 1
	else
		return FALSE;	 // 대기상태 0
}

bool CIODlg::Is_LLC_Mask_Slant_Check()
{
	// 감지되면 0, 감지되지 않으면 1

	bool bLlcTilt1 = g_pIO->m_bDigitalIn[DI::LLC_MASK_TILT1];
	bool bLlcTilt2 = g_pIO->m_bDigitalIn[DI::LLC_MASK_TILT2];

	if (bLlcTilt1 == true  && bLlcTilt2 == true) 
		return FALSE; // 기울기 감지 X -> 상태 FALSE
	else 
		return TRUE; // 기울기 감지 되면 TURE
}

bool CIODlg::Is_MC_Mask_Slant_Check()
{
	// 감지되면 0, 감지되지 않으면 1

	bool bMcTilt1 = g_pIO->m_bDigitalIn[DI::MC_MASK_TILT1];
	bool bMcTilt2 = g_pIO->m_bDigitalIn[DI::MC_MASK_TILT2];

	//bool bMcTilt1 = true;
	//bool bMcTilt2 = true;

	if (bMcTilt1 == true  && bMcTilt2 == true) 
		return FALSE; // 기울기 감지 X -> 상태 FALSE
	else 
		return TRUE; // 기울기 감지 되면 TURE
}

bool CIODlg::Is_LLC_Mask_Slant1_Check()
{
	bool bLlcTilt1 = g_pIO->m_bDigitalIn[DI::LLC_MASK_TILT1];

	// 감지 되면 0, 감지 되지 않으면 1

	if (bLlcTilt1 == true) 
		return FALSE; // 기울기 감지 X 상태 FALSE
	else 
		return TRUE; // 기울기 감지 되면 TURE
}

bool CIODlg::Is_LLC_Mask_Slant2_Check()
{
	bool bLlcTilt2 = g_pIO->m_bDigitalIn[DI::LLC_MASK_TILT2];

	// 감지 되면 0, 감지 되지 않으면 1

	if (bLlcTilt2 == true) 
		return FALSE; // 기울기 감지 X 상태 FALSE
	else 
		return TRUE; // 기울기 감지 되면 TURE
}

bool CIODlg::Is_MC_Mask_Slant1_Check()
{
	bool bMcTilt1 = g_pIO->m_bDigitalIn[DI::MC_MASK_TILT1];

	// 감지 되면 0, 감지 되지 않으면 1

	if (bMcTilt1 == true) 
		return FALSE; // 기울기 감지 X 상태 FALSE
	else 
		return TRUE; // 기울기 감지 되면 TURE
}

bool CIODlg::Is_MC_Mask_Slant2_Check()
{
	bool bMcTilt2 = g_pIO->m_bDigitalIn[DI::MC_MASK_TILT2];

	// 감지 되면 0, 감지 되지 않으면 1

	if (bMcTilt2 == true)
		return FALSE; // 기울기 감지 X 상태 FALSE
	else 
		return TRUE; // 기울기 감지 되면 TURE
}

int CIODlg::Valve_Check(bool bOpen, bool bClose)
{
	int ret = 0;

	if (bOpen == bClose)
	{
		if ((bOpen == 0) && (bClose == 0))
			ret = VALVE_CLOSE; // CLOSE
		else 
			ret = VALVE_NOT_OPENED_CLOSED; //VALVE ERROR
	}
	else if (bOpen != bClose)
	{
		if (bOpen) 
			ret = VALVE_OPEN; // VALVE OPEN
		else 
			ret = VALVE_CLOSE; // VALVE CLOSE
	}
	else 
		ret = VALVE_NOT_OPENED_CLOSED;// VALVE ERROR

	return ret;
}

void CIODlg::OnBnClickedBtnMaint()
{
	CPasswordDlg pwdlg(this);
	pwdlg.DoModal();
	CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: SREM MAINT Mode 로 진행 하시겠습니까? ")));	//통신 상태 기록.
	//if (pwdlg.m_strTxt != "srem")
	if (pwdlg.m_strTxt != "1234")
	{
		CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: Password 가 일치 하지 않습니다 ")));	//통신 상태 기록.
		::AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
		return;
	}
	else if (pwdlg.m_strTxt == "1234")
	{
		GetDlgItem(IDC_BTN_MAINT)->EnableActiveAccessibility();
		GetDlgItem(IDC_BTN_MAINT)->EnableWindow(false);
		GetDlgItem(IDC_BTN_OPER)->EnableWindow(true);
		m_nIOMode = MAINT_MODE_ON;
		CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: SREM MAINT Mode On ")));	//통신 상태 기록.
		::AfxMessageBox("MAINT MODE로 전환 되었습니다", MB_ICONINFORMATION);
	}

	
}


void CIODlg::OnBnClickedBtnOper()
{
	GetDlgItem(IDC_BTN_OPER)->EnableActiveAccessibility();
	GetDlgItem(IDC_BTN_OPER)->EnableWindow(false);
	GetDlgItem(IDC_BTN_MAINT)->EnableWindow(true);
	m_nIOMode = OPER_MODE_ON;
	CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: SREM OPER Mode On ")));
	::AfxMessageBox("OPER MODE로 전환 되었습니다", MB_ICONINFORMATION);
}


int CIODlg::Str_ok_Box(CString Log, CString str_nCheck)
{
	//char* str = Log + str_nCheck;

	if (AfxMessageBox(Log + str_nCheck + "\n" + "실행하시겠습니까?", MB_YESNO) == IDYES)
	{
		return RUN;
	}
	else return RUN_OFF;
}


int CIODlg::LLC_Dry_Pump_Error_Sequence()
{
	char* str;

	if ((Is_LLC_DryPump_Status() == DRYPUMP_ERROR) || (Is_LLC_DryPump_Status() == DRYPUMP_WARNING))
	{
		str = "LLC DRY PUMP ERROR";
		m_nERROR_MODE = RUN;
		//if (!Error_On(str))
		//{
		//	g_pWarning->UpdateData(FALSE);
		//	g_pWarning->ShowWindow(SW_HIDE);
		//}
	}
	
	return 0;

}


int CIODlg::MC_Dry_Pump_Error_Sequence()
{
	char* str;

	if ((Is_MC_DryPump_Status() == DRYPUMP_ERROR) || (Is_MC_DryPump_Status() == DRYPUMP_WARNING))
	{
		str = "MC DRY PUMP ERROR";
		m_nERROR_MODE = RUN;
		//if (!Error_On(str))
		//{
		//	//CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
		//	//g_pWarning->m_strWarningMessageVal = " Error Sequence 가 중지 되었습니다 ";
		//	g_pWarning->UpdateData(FALSE);
		//	g_pWarning->ShowWindow(SW_HIDE);
		//}

	}

	return 0;

}


int CIODlg::Error_On(int ErrorCode, char* str)
{
	char* log_str;

	m_nERROR_MODE = RUN;
	
	//CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
	//g_pWarning->m_strWarningMessageVal = " Error Sequence 가 작동 중입니다. ";
	//g_pWarning->UpdateData(FALSE);
	//g_pWarning->ShowWindow(SW_SHOW);
	
	g_pIO->On_SignalTowerBuzzer_1();

	g_pAlarm->SetAlarm(ErrorCode);
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(str)));

	//////////////////////////////
	// Error Sequence
	//////////////////////////////
	// 1. TR GATE CLOSE
	// 2. LLC GATE CLOSE
	// 3. LLC TMP GATE CLOSE
	// 4. MC TMP GATE CLOSE
	// 5. LLC ROUGH GATE CLOSE
	// 6. MC ROUGH GATE CLOSE
	// 7. FAST MFC GATE CLOSE
	// 8. SLOW MFC GATE CLOSE
	// 9. MC TMP OFF
	// 10. LLC TMP OFF
	///////////////////////////////


	///////////////////////
	// SOURCE Shutter Close 
	///////////////////////
	if (g_pEUVSource != NULL)
	{
		g_pEUVSource->SetMechShutterOpen(FALSE);
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Source Shutter Close!";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		///////////////////////
		// EUV SOURCE OFF 
		///////////////////////
		g_pEUVSource->SetEUVSourceOn(FALSE);
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Euv OFF!";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
	}
	/////////////////////
	// Source Gate Close Check and Write
	/////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Source Gate Close 전달!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	if (Is_SourceGate_OpenOn_Check() != VALVE_CLOSE)
	{
		if (Close_SourceGate_Check() != OPERATION_COMPLETED)
		{
			log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Source Gate Close Check 명령 에러 발생 !";
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
			CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, log_str);
			g_pLog->Display(0, g_pIO->Log_str);
			return FALSE;
		}
	}

	////////////////////////
	// LLC TMP GATE CLOSE
	////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP Gate Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_LLC_TMP_GateValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP Gate Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_LLC_TMP_GateValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	////////////////////////
	// MC TMP GATE CLOSE
	////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP Gate Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_MC_TMP_GateValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP Gate Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_MC_TMP_GateValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_MC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	////////////////////////////////////////////////////////////
	// 로딩 & 언로딩 진행 상태 확인
	// ( 동작 중인 경우, 동작의 Sequence 를 정상 종료 확인 )
	////////////////////////////////////////////////////////////
	if (g_pAP != NULL)
	{
		while (g_pAP->m_bThreadStop != TRUE)
		{
			if (g_pAP->m_bThreadStop == FALSE)
				break;
		}

		if ((g_pAP->CurrentProcess == g_pAP->LOADING_COMPLETE) || (g_pAP->CurrentProcess == g_pAP->UNLOADING_COMPLETE))
		{
			/* Error Mode 실행 중 Loading , Unloading 정상 종료 된 경우 TR Gate Close 가능 */
			m_bAPSequenceNormalCloseFlag = TRUE;
		}
		else
		{
			/* Error Mode 실행 중 Loading , Unloading 비정상 종료 된 경우 TR Gate Close 불가능 */
			m_bAPSequenceNormalCloseFlag = FALSE;
		}

	}


	///////////////////////////////
	// LLC SLOW ROUGH VALVE CLOSE
	///////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Slow Rough Valve Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_LLC_SlowRoughValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Slow Rough Valve Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);


	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_LLC_SlowRoughValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Slow Rough Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Slow Rough Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);


	///////////////////////////////
	// LLC FAST ROUGH VALVE CLOSE
	///////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Fast Rough Valve Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_LLC_FastRoughValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Fast Rough Valve Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T("return valve :: "));
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->m_StrReturnValue)));
	g_pLog->Display(0, g_pIO->Log_str);


	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_LLC_FastRoughValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Fast Rough Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Fast Rough Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	////////////////////////////
	// MC FAST ROUGH VALVE CLOSE
	////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Fast Rough Valve Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_MC_FastRoughValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Fast Rough Valve Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;

	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_MC_FastRoughValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Fast Rough Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Fast Rough Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	/////////////////////////////////
	// MC SLOW ROUGH VALVE CLOSE
	/////////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Slow Rough Valve Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_MC_SlowRoughValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Slow Rough Valve Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);


	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_MC_SlowRoughValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_MC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Slow Rough Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Slow Rough Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	
	////////////////////////////////
	// FAST VENT VALVE CLOSE
	////////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Fast Vent Valve Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_FastVentValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Fast Vent Gate Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_FastVentValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Fast Vent Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Fast Vent Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	/////////////////////////////
	// SLOW VENT VALVE CLOSE
	/////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Slow Vent Valve Close !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);
	if (Close_SlowVentValve() != OPERATION_COMPLETED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Slow Vent Gate Close 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, log_str);
		g_pLog->Display(0, g_pIO->Log_str);
		return FALSE;
	}
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (Is_SlowVentValve_Open() == VALVE_CLOSE) break;
	}

	if (Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Slow Vent Gate Close 확인 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);

		return FALSE;
	}

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 Slow Vent Gate Close 완료!";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	/////////////////////////////
	// MC TMP OFF
	/////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Tmp Off !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	if (g_pMCTmp_IO->Mc_Tmp_Off() != RUN_OFF)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP Off 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, log_str);
		return FALSE;
	}
	
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC Tmp Off 명령 완료, off 진행 중 !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	/////////////////////////////
	// LLC TMP OFF
	/////////////////////////////
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Tmp Off !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	if (g_pLLCTmp_IO->LLC_Tmp_Off() != RUN_OFF)
	{
		log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC TMP Off 명령 에러 발생 !";
		CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
		g_pLog->Display(0, log_str);
		return FALSE;
	}
	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 LLC Tmp Off 명령 완료, off 진행 중 !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	log_str = " MAIN_ERROR_ON() : Main Error 발생 후 인터락 발생 !";
	CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
	g_pLog->Display(0, log_str);

	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_HIDE);


	return TRUE;
}


//int CIODlg::Tmp_Check()
//{
//
//	int nRet = 0;
//
//	if (g_pMCTmp_IO->mc_tmp_ReState == "Stop")
//	{
//		if (ERROR_MODE == RUN)
//		{
//			if (MC_Dry_Pump_off())
//			{
//				CString log_str = " MAIN_ERROR_ON() : Error 발생 후 MC TMP STOP , MC Dry Pump Off 실행 !";
//				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
//				g_pLog->Display(0, log_str);
//
//
//				//////////////////////////////////////////////////////////////////
//				// TMP 완전 멈춤, 그후 DRY PUMP OFF 후 MC FORELINE GATE CLOSE
//				//////////////////////////////////////////////////////////////////
//
//				log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤, 그 후 MC Foreline Gate  Close !";
//				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
//				g_pLog->Display(0, log_str);
//				if (Close_MC_TMP1_ForelineValve() != TRUE)
//				{
//					log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 명령 에러 발생 !";
//					CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
//					CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
//					g_pLog->Display(0, log_str);
//					g_pLog->Display(0, g_pIO->Log_str);
//					return FALSE;
//				}
//				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
//				g_pLog->Display(0, g_pIO->Log_str);
//
//				m_start_time = clock();
//				m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
//				while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
//				{
//					if (Is_MC_TMP1_ForelineValve_Open() == VALVE_CLOSE) break;
//				}
//
//				if (Is_MC_TMP1_ForelineValve_Open() != VALVE_CLOSED)
//				{
//					log_str = " MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 확인 에러 발생 !";
//					CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
//					g_pLog->Display(0, log_str);
//
//					return FALSE;
//				}
//
//				log_str = "MAIN_ERROR_ON() : Main Error 발생 후 MC TMP 정상 멈춤. 그 후 MC Foreline Gate Close 확인 완료!";
//				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
//				g_pLog->Display(0, log_str);
//
//			}
//			else
//			{
//				CString log_str = " MAIN_ERROR_ON() : Error 발생 후 MC TMP STOP , MC Dry Pump Off 실행 Error (확인필요) !";
//				CECommon::SaveLogFile("MAIN_ERROR_ON", _T((LPSTR)(LPCTSTR)(log_str)));
//				g_pLog->Display(0, log_str);
//			}
//		}
//	}
//
//}
//void CIODlg::Tmp_Check()
//{
//	mc_tmp_check();
//	
//}


void CIODlg::Closedevice_IO()
{
	StopIoUpdate();
	CloseDevice();
	CECommon::SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: SREM IO Disconnected Click")));	//통신 상태 기록.

	m_bThreadExitFlag = TRUE;
	m_bCrevis_Open_Port = FALSE;

	KillTimer(IO_UPDATE_TIMER);

	if (m_pThread != NULL)
	{
		HANDLE threadHandle = m_pThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pThread = NULL;
	}

}


void CIODlg::Check_IO_Status()
{

	CString str;
	double diffvalue = 20.0;
	/* AC */
	str.Format("%0.2f", Get_AC_Rack_Temp());
	SetDlgItemText(IDC_STATIC_TEMP_MONITOR_AC, str + " ℃");

	/* Control Box */
	str.Format("%0.2f", Get_ControlBox_Temp());
	SetDlgItemText(IDC_STATIC_TEMP_MONITOR_CONTROL, str + " ℃");

	Open_SourceGate_Check();
}

HBRUSH CIODlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = __super::OnCtlColor(pDC, pWnd, nCtlColor);
	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if ((pWnd->GetDlgCtrlID() == IDC_STATIC_TEMP_MONITOR_AC) || (pWnd->GetDlgCtrlID() == IDC_STATIC_TEMP_MONITOR_CONTROL))
		{
			pDC->SetBkColor(RGB(255, 234, 234));
			pDC->SetTextColor(RGB(0, 0, 0));
			return m_brush;
		}

	}
	return hbr;
}

