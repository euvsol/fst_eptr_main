﻿// CIOOutputAnalogDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"



// CAnalogOutput 대화 상자

IMPLEMENT_DYNAMIC(CAnalogOutput, CDialogEx)

CAnalogOutput::CAnalogOutput(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IO_OUTPUT_AN_DIALOG, pParent)
{

}

CAnalogOutput::~CAnalogOutput()
{
	m_brush.DeleteObject();
	m_font.DeleteObject();
}

void CAnalogOutput::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER_MFC_01, m_mfc_ctrl_1);
	DDX_Control(pDX, IDC_SLIDER_MFC_02, m_mfc_ctrl_2);
	DDX_Control(pDX, IDC_SLIDER_CTN, m_ctrl_3);
	DDX_Control(pDX, IDC_SLIDER_MFC_03, m_mfc_ctrl_3);
}


BEGIN_MESSAGE_MAP(CAnalogOutput, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_ANALOGOUT_Y000, &CAnalogOutput::OnBnClickedCheckAnalogoutY000)
	ON_BN_CLICKED(IDC_CHECK_ANALOGOUT_Y001, &CAnalogOutput::OnBnClickedCheckAnalogoutY001)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_MFC_01, &CAnalogOutput::OnNMCustomdrawSliderMfc01)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_MFC_02, &CAnalogOutput::OnNMCustomdrawSliderMfc02)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_CTN, &CAnalogOutput::OnNMCustomdrawSliderCtn)
	ON_BN_CLICKED(IDC_CHECK_ANALOGOUT_SET0, &CAnalogOutput::OnBnClickedCheckAnalogoutSet0)
	ON_BN_CLICKED(IDC_CHECK_ANALOGOUT_SET1, &CAnalogOutput::OnBnClickedCheckAnalogoutSet1)
	ON_BN_CLICKED(IDC_CHECK_ANALOGOUT_SET2, &CAnalogOutput::OnBnClickedCheckAnalogoutSet2)
	ON_BN_CLICKED(IDC_CHECK_ANALOGOUT_Y002, &CAnalogOutput::OnBnClickedCheckAnalogoutY002)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_CHECK_ANALOGOUT_SET3, &CAnalogOutput::OnBnClickedCheckAnalogoutSet3)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_MFC_03, &CAnalogOutput::OnNMCustomdrawSliderMfc03)
	ON_BN_CLICKED(IDC_CHECK_ANALOGOUT_Y003, &CAnalogOutput::OnBnClickedCheckAnalogoutY003)
END_MESSAGE_MAP()


// CAnalogOutput 메시지 처리기

BOOL CAnalogOutput::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	CDialogEx::OnInitDialog();

	m_brush.CreateSolidBrush(LIGHT_GRAY); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);


	// SLOW MFC
	m_mfc_ctrl_1.SetRange(0, 50);
	m_mfc_ctrl_1.SetRangeMin(0);
	m_mfc_ctrl_1.SetRangeMax(50);
	m_mfc_ctrl_1.SetPos(0);
	m_mfc_ctrl_1.SetTicFreq(0.1);
	m_mfc_ctrl_1.SetLineSize(0.1);

	// FAST MFC
	m_mfc_ctrl_2.SetRange(0, 50);
	m_mfc_ctrl_2.SetRangeMin(0);
	m_mfc_ctrl_2.SetRangeMax(50);
	m_mfc_ctrl_2.SetPos(0);
	m_mfc_ctrl_2.SetTicFreq(0.1);
	m_mfc_ctrl_2.SetLineSize(0.1);


	// O2 MFC
	m_mfc_ctrl_3.SetRange(0, 50);
	m_mfc_ctrl_3.SetRangeMin(0);
	m_mfc_ctrl_3.SetRangeMax(50);
	m_mfc_ctrl_3.SetPos(0);
	m_mfc_ctrl_3.SetTicFreq(0.1);
	m_mfc_ctrl_3.SetLineSize(0.1);

	// 조명 컨트롤러 
	m_ctrl_3.SetRange(0, 50);
	m_ctrl_3.SetRangeMin(0);
	m_ctrl_3.SetRangeMax(50);
	m_ctrl_3.SetPos(0);
	m_ctrl_3.SetTicFreq(0.1);
	m_ctrl_3.SetLineSize(0.1);


	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

//void CAnalogOutput::InitControls_AO(int nCh)
void CAnalogOutput::InitControls_AO()
{
	m_nChannel = 1;
	CString strTemp;
	CString str;

	for (int nIdx = 0; nIdx < ANALOG_IO_VIEW_NUMBER; nIdx++)
	{

		((CStatic*)GetDlgItem(IDC_ICON_ANALOGOUT_Y000 + nIdx))->SetIcon(m_LedIcon[0]);
		strTemp.Format(_T("Y%04d"), nIdx + (nIdx * 100));
		SetDlgItemText(IDC_STATIC_ANALOGOUT_NUM0 + nIdx, strTemp);
		//SetDlgItemText(IDC_STATIC_ANALOGIN_VALUE0 + nCnt, "0.0 V");
		GetDlgItem(IDC_STATIC_ANALOGOUT_Y000 + nIdx)->SetWindowText(g_pConfig->m_chAo[nIdx]);
		GetDlgItem(IDC_STATIC_ANALOGOUT_Y000 + nIdx)->GetWindowTextA(str);
		if ((str == "EMPTY") || (str == ""))
		{
			SetDlgItemText(IDC_STATIC_ANALOGOUT_VALUE0 + nIdx, " - ");
			GetDlgItem(IDC_CHECK_ANALOGOUT_Y000 + nIdx)->EnableWindow(false);
			GetDlgItem(IDC_CHECK_ANALOGOUT_SET0 + nIdx)->EnableWindow(false);
			
		}
		else SetDlgItemText(IDC_STATIC_ANALOGOUT_VALUE0 + nIdx, "0.0 V");

		CButton* btn = (CButton*)GetDlgItem(IDC_CHECK_ANALOGOUT_Y000 + nIdx);


	}

	SetTimer(0, 500, NULL);

}


bool  CAnalogOutput::OnUpdateAnalogOutput()
{

	int ch_1_margin_p = (g_pIO->m_nDecimal_Analog_out_ch_1) + 5;
	int ch_1_margin_n = (g_pIO->m_nDecimal_Analog_out_ch_1) - 5;

	int ch_2_margin_p = (g_pIO->m_nDecimal_Analog_out_ch_2) + 5;
	int ch_2_margin_n = (g_pIO->m_nDecimal_Analog_out_ch_2) - 5;

	int ch_3_margin_p = (g_pIO->m_nDecimal_Analog_out_ch_3) + 5;
	int ch_3_margin_n = (g_pIO->m_nDecimal_Analog_out_ch_3) - 5;

	int ch_4_margin_p = (g_pIO->m_nDecimal_Analog_out_ch_4) + 5;
	int ch_4_margin_n = (g_pIO->m_nDecimal_Analog_out_ch_4) - 5;


	((CStatic*)GetDlgItem(IDC_ICON_ANALOGOUT_Y000))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_ANALOGOUT_Y001))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_ANALOGOUT_Y002))->SetIcon(m_LedIcon[0]);

	if (g_pIO->m_nDecimal_Analog_out_ch_1 != 0) {
		if ((ch_1_margin_n <= g_pIO->m_nDecimal_Analog_out_ch_1) && (g_pIO->m_nDecimal_Analog_out_ch_1 <= ch_1_margin_p)) {
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGOUT_Y000))->SetIcon(m_LedIcon[1]);
		}
	}
	if (g_pIO->m_nDecimal_Analog_out_ch_2 != 0) {
		if ((ch_2_margin_n <= g_pIO->m_nDecimal_Analog_out_ch_2) && (g_pIO->m_nDecimal_Analog_out_ch_2 <= ch_2_margin_p)) {
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGOUT_Y001))->SetIcon(m_LedIcon[1]);
		}
	}
	if (g_pIO->m_nDecimal_Analog_out_ch_3 != 0) {
		if ((ch_3_margin_n <= g_pIO->m_nDecimal_Analog_out_ch_3) && (g_pIO->m_nDecimal_Analog_out_ch_3 <= ch_3_margin_p)) {
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGOUT_Y002))->SetIcon(m_LedIcon[1]);
		}
	}
	if (g_pIO->m_nDecimal_Analog_out_ch_4 != 0) {
		if ((ch_4_margin_n <= g_pIO->m_nDecimal_Analog_out_ch_4) && (g_pIO->m_nDecimal_Analog_out_ch_4 <= ch_4_margin_p)) {
			((CStatic*)GetDlgItem(IDC_ICON_ANALOGOUT_Y003))->SetIcon(m_LedIcon[1]);
		}
	}
	return TRUE;
}

void CAnalogOutput::OnDestroy()
{
	CDialogEx::OnDestroy();
	KillTimer(0);
}


void CAnalogOutput::OnTimer(UINT_PTR nIDEvent)
{

	OnUpdateAnalogOutput();

	CDialogEx::OnTimer(nIDEvent);

}



void CAnalogOutput::OnBnClickedCheckAnalogoutY000()
{
	GetDlgItem(IDC_CHECK_ANALOGOUT_Y000)->EnableActiveAccessibility();
	GetDlgItem(IDC_CHECK_ANALOGOUT_Y000)->EnableWindow(false);

	((CStatic*)GetDlgItem(IDC_ICON_ANALOGOUT_Y000))->SetIcon(m_LedIcon[0]);
	m_mfc_ctrl_1.SetPos(0);

	int ADDR = 0;
	unsigned long long pBuffer = 0x00;

	SetDlgItemText(IDC_STATIC_ANALOGOUT_VALUE0, "0");

	g_pIO->WriteOutputData(ADDR, &pBuffer, 2);
}


void CAnalogOutput::OnBnClickedCheckAnalogoutY001()
{
	GetDlgItem(IDC_CHECK_ANALOGOUT_Y001)->EnableActiveAccessibility();
	GetDlgItem(IDC_CHECK_ANALOGOUT_Y001)->EnableWindow(false);

	((CStatic*)GetDlgItem(IDC_ICON_ANALOGOUT_Y001))->SetIcon(m_LedIcon[0]);
	m_mfc_ctrl_2.SetPos(0);

	int ADDR = 2;
	unsigned long long pBuffer = 0x00;

	SetDlgItemText(IDC_STATIC_ANALOGOUT_VALUE1, "0");

	g_pIO->WriteOutputData(ADDR, &pBuffer, 2);
}

void CAnalogOutput::OnBnClickedCheckAnalogoutY002()
{
	GetDlgItem(IDC_CHECK_ANALOGOUT_Y002)->EnableActiveAccessibility();
	GetDlgItem(IDC_CHECK_ANALOGOUT_Y002)->EnableWindow(false);

	((CStatic*)GetDlgItem(IDC_ICON_ANALOGOUT_Y002))->SetIcon(m_LedIcon[0]);
	m_ctrl_3.SetPos(0);

	int ADDR = 4;
	unsigned long long pBuffer = 0x00;

	SetDlgItemText(IDC_STATIC_ANALOGOUT_VALUE2, "0");

	g_pIO->WriteOutputData(ADDR, &pBuffer, 2);
}



void CAnalogOutput::OnBnClickedCheckAnalogoutY003()
{
	GetDlgItem(IDC_CHECK_ANALOGOUT_Y003)->EnableActiveAccessibility();
	GetDlgItem(IDC_CHECK_ANALOGOUT_Y003)->EnableWindow(false);

	((CStatic*)GetDlgItem(IDC_ICON_ANALOGOUT_Y003))->SetIcon(m_LedIcon[0]);
	m_mfc_ctrl_3.SetPos(0);

	int ADDR = 6;
	unsigned long long pBuffer = 0x00;

	SetDlgItemText(IDC_STATIC_ANALOGOUT_VALUE3, "0");

	g_pIO->WriteOutputData(ADDR, &pBuffer, 2);
}



void CAnalogOutput::OnNMCustomdrawSliderMfc01(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);

	int nPos;

	nPos = m_mfc_ctrl_1.GetPos();



	/** 전압 값으로 환산하여 모니터 */
	strPos.Format("%.1f", (nPos / 10.0f));

	/** SCCM 값으로 환산하여 모니터 */
	slow_mfc_sccm.Format("%.1f", ((nPos/10.0f) * 2));
	SetDlgItemText(IDC_STATIC_ANALOGOUT_VALUE0, slow_mfc_sccm);



	//SetDlgItemText(IDC_STATIC_ANALOGOUT_VALUE0, strPos + " V");


	*pResult = 0;
}


void CAnalogOutput::OnNMCustomdrawSliderMfc02(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);

	int nPos;

	nPos = m_mfc_ctrl_2.GetPos();
	

	/** 전압 값으로 환산하여 모니터 */
	strPos2.Format("%.1f", (nPos / 10.0f));

	/** SCCM 값으로 환산하여 모니터 */
	fast_mfc_sccm.Format("%.1f", ((nPos / 10.0f) * 4000));
	SetDlgItemText(IDC_STATIC_ANALOGOUT_VALUE1, fast_mfc_sccm);

	
	//SetDlgItemText(IDC_STATIC_ANALOGOUT_VALUE1, strPos2 + " V");


	*pResult = 0;
}

void CAnalogOutput::OnNMCustomdrawSliderMfc03(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);

	int nPos;

	nPos = m_mfc_ctrl_3.GetPos();


	/** 전압 값으로 환산하여 모니터 */
	strPos4.Format("%.1f", (nPos / 10.0f));

	/** SCCM 값으로 환산하여 모니터 */
	o2_mfc_sccm.Format("%.1f", ((nPos / 10.0f)));
	SetDlgItemText(IDC_STATIC_ANALOGOUT_VALUE3, o2_mfc_sccm);


	*pResult = 0;
}

void CAnalogOutput::OnNMCustomdrawSliderCtn(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);

	int nPos;

	nPos = m_ctrl_3.GetPos();
	strPos3.Format("%.1f", (nPos / 10.0f));

	SetDlgItemText(IDC_STATIC_ANALOGOUT_VALUE2, strPos3 + " V");
	*pResult = 0;
}


void CAnalogOutput::OnBnClickedCheckAnalogoutSet0()
{

	GetDlgItem(IDC_CHECK_ANALOGOUT_Y000)->EnableWindow(true);

	int ADDR = 0;
	unsigned long long pBuffer;
	double mfc_ch1;
	double slider_pos_mfc_ch1;

	slider_pos_mfc_ch1 = atof(strPos);
	mfc_ch1 = slider_pos_mfc_ch1 * ANALOG_INPUT_UNIT_CONVERSION;

	ADDR = 0;
	pBuffer = mfc_ch1;

	g_pIO->WriteOutputData(ADDR, &pBuffer, 2);
}


void CAnalogOutput::OnBnClickedCheckAnalogoutSet1()
{
	GetDlgItem(IDC_CHECK_ANALOGOUT_Y001)->EnableWindow(true);


	int ADDR;
	unsigned long long pBuffer;
	double mfc_ch2;
	double slider_pos_mfc_ch2;

	slider_pos_mfc_ch2 = atof(strPos2);
	mfc_ch2 = slider_pos_mfc_ch2 * ANALOG_INPUT_UNIT_CONVERSION;

	ADDR = 2;
	pBuffer = mfc_ch2;


	g_pIO->WriteOutputData(ADDR, &pBuffer, 2);
}


void CAnalogOutput::OnBnClickedCheckAnalogoutSet2()
{
	GetDlgItem(IDC_CHECK_ANALOGOUT_Y002)->EnableWindow(true);


	int ADDR;
	unsigned long long pBuffer;
	double mfc_ch3;
	double slider_pos_mfc_ch3;

	slider_pos_mfc_ch3 = atof(strPos3);
	mfc_ch3 = slider_pos_mfc_ch3 * ANALOG_INPUT_UNIT_CONVERSION;

	ADDR = 4;
	pBuffer = mfc_ch3;
	
	g_pIO->WriteOutputData(ADDR, &pBuffer, 2);
}

void CAnalogOutput::OnBnClickedCheckAnalogoutSet3()
{
	GetDlgItem(IDC_CHECK_ANALOGOUT_Y003)->EnableWindow(true);


	int ADDR;
	unsigned long long pBuffer;
	double mfc_ch4;
	double slider_pos_mfc_ch4;

	slider_pos_mfc_ch4 = atof(strPos4);
	mfc_ch4 = slider_pos_mfc_ch4 * ANALOG_INPUT_UNIT_CONVERSION;

	ADDR = 6;
	pBuffer = mfc_ch4;

	g_pIO->WriteOutputData(ADDR, &pBuffer, 2);
}



HBRUSH CAnalogOutput::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (pWnd->GetDlgCtrlID() == IDC_STATIC_ANALOGOUT_TEXT)
		{
			pDC->SetBkColor(LIGHT_GRAY);
			//pDC->SetTextColor(RGB(0, 255, 0));
			return m_brush;
		}
	}

	return hbr;
}





