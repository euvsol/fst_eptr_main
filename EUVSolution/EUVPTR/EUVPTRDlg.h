/**
 * EUV PTR Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once

// CEUVPTRDlg 대화 상자
class CEUVPTRDlg : public CDialogEx, public CECommon
{
// 생성입니다.
public:
	CEUVPTRDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_EUVPTR_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;
	HICON m_LedIcon[3];
	HBITMAP m_hLogo;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedOperatorButton();
	afx_msg void OnBnClickedEngineerButton();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedExitButton();

	/**
	* Definition:	초기화 함수
	* Parameter:
	* Return:		int	에러번호
	*/
	int CreateModules();
	
	
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	cdxCRotBevelLine m_EuvsolBevel1Ctrl;
	cdxCRotBevelLine m_EuvsolBevel2Ctrl;
	int SetDisplay(int Mode);
	CString			GetVersion(void);
	int				DisplayVersion();
	CGCClock	 m_Clock;

	void InitClock();
	void InitializeControls();
	void GetEquipmentInfo();
	void SetEquipmentStatus(CString sTemp);

	afx_msg void OnBnClickedConfigurationButton();
	afx_msg void OnBnClickedEvaluationButton();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonMainEuvOn();
	afx_msg void OnBnClickedButtonMainEuvOff();
	afx_msg void OnBnClickedButtonMainShutterOpen();
	afx_msg void OnBnClickedButtonMainShutterClose();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButtonShowUi();
	afx_msg void OnBnClickedButtonHideUi();
 };