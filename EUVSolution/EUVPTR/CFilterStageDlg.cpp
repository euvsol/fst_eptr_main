﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CFilterStageDlg 대화 상자

IMPLEMENT_DYNAMIC(CFilterStageDlg, CDialogEx)

CFilterStageDlg::CFilterStageDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_FILTER_STAGE_DIALOG, pParent)
	, m_bEngineerMode(FALSE)
{
	m_bThreadExitFlag	= FALSE;
	m_pStatusThread		= NULL;
	m_dTolerance		= 0.1;
}

CFilterStageDlg::~CFilterStageDlg()
{
	m_bThreadExitFlag = TRUE;

	if (m_pStatusThread != NULL)
	{
		HANDLE threadHandle = m_pStatusThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pStatusThread = NULL;
	}
}

void CFilterStageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FILTER_CURR_POS_EDIT, m_PosCtrl);
	DDX_Control(pDX, IDC_EDIT_FILTER_REL_MOVE, m_RelMoveCtrl);
	DDX_Control(pDX, IDC_EDIT_FILTER_ABS_MOVE, m_AbsMoveCtrl);
	DDX_Control(pDX, IDC_EDIT_FILTER_POS1, m_Pos1Ctrl);
	DDX_Control(pDX, IDC_EDIT_FILTER_POS2, m_Pos2Ctrl);
	DDX_Control(pDX, IDC_EDIT_FILTER_POS3, m_Pos3Ctrl);
	DDX_Check(pDX, IDC_FILTER_ENGINEER_MODE_CHECK, m_bEngineerMode);
}


BEGIN_MESSAGE_MAP(CFilterStageDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_PI_FILTER_POS1, &CFilterStageDlg::OnBnClickedButtonPiFilterPos1)
	ON_BN_CLICKED(IDC_BUTTON_PI_FILTER_POS2, &CFilterStageDlg::OnBnClickedButtonPiFilterPos2)
	ON_BN_CLICKED(IDC_BUTTON_PI_FILTER_POS3, &CFilterStageDlg::OnBnClickedButtonPiFilterPos3)
	ON_BN_CLICKED(IDC_BUTTON_PI_FILTER_MOVE_ORI, &CFilterStageDlg::OnBnClickedButtonPiFilterMoveOri)
	ON_BN_CLICKED(IDC_BUTTON_PI_FILTER_MOVE_N, &CFilterStageDlg::OnBnClickedButtonPiFilterMoveN)
	ON_BN_CLICKED(IDC_BUTTON_PI_FILTER_MOVE_P, &CFilterStageDlg::OnBnClickedButtonPiFilterMoveP)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_ABS_MOVE, &CFilterStageDlg::OnBnClickedButtonFilterAbsMove)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_PI_FILTER, &CFilterStageDlg::OnDeltaposSpinPiFilter)
	ON_BN_CLICKED(IDC_BUTTON_PI_FILTER_SVO_ON, &CFilterStageDlg::OnBnClickedButtonPiFilterSvoOn)
	ON_BN_CLICKED(IDC_BUTTON_PI_FILTER_SVO_OFF, &CFilterStageDlg::OnBnClickedButtonPiFilterSvoOff)
	ON_BN_CLICKED(IDC_BUTTON_FILTER_SET, &CFilterStageDlg::OnBnClickedButtonFilterSet)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_FILTER_ENGINEER_MODE_CHECK, &CFilterStageDlg::OnBnClickedFilterEngineerModeCheck)
END_MESSAGE_MAP()


// CFilterStageDlg 메시지 처리기

BOOL CFilterStageDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

BOOL CFilterStageDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_FILTER_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FILTER_POS1))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FILTER_POS2))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_FILTER_POS3))->SetIcon(m_LedIcon[0]);

	CString str;
	str.Format(_T("%10.1f"), m_dPIStage_MovePos[X_AXIS_E873]);
	m_RelMoveCtrl.SetWindowText(str);
	str.Format(_T("%10.1f"), m_dPIStage_MovePos[X_AXIS_E873]);
	m_AbsMoveCtrl.SetWindowText(str);

	//Load Filter Pos Data
	m_Pos1Ctrl.SetWindowTextA(g_pConfig->m_sFilterPos1);
	m_Pos2Ctrl.SetWindowTextA(g_pConfig->m_sFilterPos2);
	m_Pos3Ctrl.SetWindowTextA(g_pConfig->m_sFilterPos3);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int CFilterStageDlg::OpenDevice()
{
	int nRet = 0;

	CString sPort = g_pConfig->m_chPORT[SERIAL_FILTER_STAGE];
	sPort.Replace(_T("COM"), _T(""));
	int nPortNum  = atoi(sPort);
	int nBaudrate = g_pConfig->m_nBAUD_RATE[SERIAL_FILTER_STAGE];

	nRet = ConnectComm(SERIAL, nPortNum, nBaudrate);
	if (nRet == 0)
	{
		SaveLogFile("FilterStage", "Success to connect to e873.");
		m_pStatusThread = AfxBeginThread(FilterStageStatusThread, (LPVOID)this, THREAD_PRIORITY_NORMAL, 0, 0);
		
		SetTimer(FILTERSTAGE_UPDATE_TIMER, 1000, NULL);
	}
	else
		SaveLogFile("FilterStage", "Failed to connect to e873.");

	return nRet;
}

void CFilterStageDlg::Initialize()
{
	Initialize_PIStage();
}

void CFilterStageDlg::GetDeviceStatus()
{
	if(m_bConnect == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_FILTER_CONNECT))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_FILTER_CONNECT))->SetIcon(m_LedIcon[0]);

	CString tmp;
	tmp.Format("%.4f", m_dPIStage_GetPos);
	if (m_PosCtrl.m_hWnd != NULL)
		m_PosCtrl.SetWindowText(tmp);

	if((m_dPIStage_GetPos < -10 + m_dTolerance) && (m_dPIStage_GetPos > -10 - m_dTolerance))
		((CStatic*)GetDlgItem(IDC_ICON_FILTER_POS1))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_FILTER_POS1))->SetIcon(m_LedIcon[0]);

	if ((m_dPIStage_GetPos < 0 + m_dTolerance) && (m_dPIStage_GetPos > 0 - m_dTolerance))
		((CStatic*)GetDlgItem(IDC_ICON_FILTER_POS2))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_FILTER_POS2))->SetIcon(m_LedIcon[0]);

	if ((m_dPIStage_GetPos < 10 + m_dTolerance) && (m_dPIStage_GetPos > 10 - m_dTolerance))
		((CStatic*)GetDlgItem(IDC_ICON_FILTER_POS3))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_FILTER_POS3))->SetIcon(m_LedIcon[0]);

}

UINT CFilterStageDlg::FilterStageStatusThread(LPVOID pParam)
{
	CFilterStageDlg* pDlg = (CFilterStageDlg*)pParam;

	while (!pDlg->m_bThreadExitFlag)
	{
		pDlg->GetPosAxesData();
		Sleep(1000);
	}

	return 0;
}

void CFilterStageDlg::OnDestroy()
{
	CDialogEx::OnDestroy();
}


void CFilterStageDlg::OnBnClickedButtonPiFilterPos1()
{
	g_pLog->Display(0, _T("CFilterStageDlg::OnBnClickedButtonPiFilterPos1() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (m_bConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bEngineerMode != TRUE)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "1234")
		{
			AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
			return;
		}
	}

	if (IDYES != AfxMessageBox("Pos1(-10mm)으로 이동할까요?", MB_YESNO)) return;

	m_dPIStage_MovePos[X_AXIS_E873] = -10;

	CString sTemp;
	sTemp.Format(_T("Clicked move Pos1, (Move Value : %.1f)"), m_dPIStage_MovePos[X_AXIS_E873]);
	SaveLogFile("FilterStage", sTemp);

	int nRet = PI_Move_Absolute();
	if (nRet != 0)
	{
		sTemp.Format(_T("Failed to move Pos1. (Move Pos : %.1f), Error Code : %d"), m_dPIStage_MovePos[X_AXIS_E873], nRet);
		AfxMessageBox(sTemp, MB_ICONWARNING);
	}
	else
		sTemp.Format(_T("Success to move Pos1. (Move Pos : %.1f), Current Position : %.1f"), m_dPIStage_MovePos[X_AXIS_E873], m_dPIStage_GetPos);

	SaveLogFile("FilterStage", sTemp);
}


void CFilterStageDlg::OnBnClickedButtonPiFilterPos2()
{
	g_pLog->Display(0, _T("CFilterStageDlg::OnBnClickedButtonPiFilterPos2() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (m_bConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bEngineerMode != TRUE)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "1234")
		{
			AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
			return;
		}
	}

	if (IDYES != AfxMessageBox("Pos2(0mm)으로 이동할까요?", MB_YESNO)) return;

	m_dPIStage_MovePos[X_AXIS_E873] = 0;

	CString sTemp;
	sTemp.Format(_T("Clicked move Pos2, (Move Value : %.1f)"), m_dPIStage_MovePos[X_AXIS_E873]);
	SaveLogFile("FilterStage", sTemp);

	int nRet = PI_Move_Absolute();
	if(nRet != 0)
	{
		sTemp.Format(_T("Failed to move Pos2. (Move Pos : %.1f), Error Code : %d"), m_dPIStage_MovePos[X_AXIS_E873], nRet);
		AfxMessageBox(sTemp, MB_ICONWARNING);
	}
	else
		sTemp.Format(_T("Success to move Pos2. (Move Pos : %.1f), Current Position : %.1f"), m_dPIStage_MovePos[X_AXIS_E873], m_dPIStage_GetPos);

	SaveLogFile("FilterStage", sTemp);
}


void CFilterStageDlg::OnBnClickedButtonPiFilterPos3()
{
	g_pLog->Display(0, _T("CFilterStageDlg::OnBnClickedButtonPiFilterPos3() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (m_bConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bEngineerMode != TRUE)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "1234")
		{
			AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
			return;
		}
	}

	if (IDYES != AfxMessageBox("Pos3(10mm)으로 이동할까요?", MB_YESNO)) return;

	m_dPIStage_MovePos[X_AXIS_E873] = 10;

	CString sTemp;
	sTemp.Format(_T("Clicked move Pos3, (Move Value : %.1f)"), m_dPIStage_MovePos[X_AXIS_E873]);
	SaveLogFile("FilterStage", sTemp);

	int nRet = PI_Move_Absolute();
	if(nRet != 0)
	{
		sTemp.Format(_T("Failed to move Pos3. (Move Pos : %.1f), Error Code : %d"), m_dPIStage_MovePos[X_AXIS_E873], nRet);
		AfxMessageBox(sTemp, MB_ICONWARNING);
	}
	else
		sTemp.Format(_T("Success to move Pos3. (Move Pos : %.1f), Currrent Position : %.1f"), m_dPIStage_MovePos[X_AXIS_E873], m_dPIStage_GetPos);

	SaveLogFile("FilterStage", sTemp);
}

void CFilterStageDlg::OnBnClickedButtonPiFilterMoveOri()
{
	g_pLog->Display(0, _T("CFilterStageDlg::OnBnClickedButtonPiFilterMoveOri() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (m_bConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bEngineerMode != TRUE)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "1234")
		{
			AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
			return;
		}
	}

	if (IDYES != AfxMessageBox("오리진 위치로 이동할까요?", MB_YESNO)) return;

	CString sTemp;
	sTemp.Format(_T("Clicked move origin position."));
	SaveLogFile("FilterStage", sTemp);

	int nRet = Move_Origin_Position();
	if (nRet != 0)
	{
		sTemp.Format(_T("Failed to move origin pos. (Move Pos : %.1f), Error Code : %d"), m_dPIStage_MovePos[X_AXIS_E873], nRet);
		AfxMessageBox(sTemp, MB_ICONWARNING);
	}
	else
		sTemp.Format(_T("Success to move origin pos. (Move Pos : %.1f), Current Position : %.1f"), m_dPIStage_MovePos[X_AXIS_E873], m_dPIStage_GetPos);

	SaveLogFile("FilterStage", sTemp);
}



void CFilterStageDlg::OnBnClickedButtonPiFilterMoveN()
{
	g_pLog->Display(0, _T("CFilterStageDlg::OnBnClickedButtonPiFilterMoveN() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (m_bConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bEngineerMode != TRUE)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "1234")
		{
			AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
			return;
		}
	}

	CString sTemp;
	CString strStep;
	double dInc_position;
	m_RelMoveCtrl.GetWindowText(strStep);
	dInc_position = atof(strStep);

	sTemp.Format(_T("-%.1f 만큼 상대위치로 이동할까요?"), dInc_position);
	if (IDYES != AfxMessageBox(sTemp, MB_YESNO)) return;

	double absolute_pos = -dInc_position + m_dPIStage_GetPos;
	if (absolute_pos < m_dPIStage_MinusLimitPos[X_AXIS_E873] || absolute_pos > m_dPIStage_PlusLimitPos[X_AXIS_E873])
	{
		AfxMessageBox(_T("입력 범위를 초과하였습니다. (-13 ~ 13mm)"), MB_ICONWARNING);
		return;
	}

	m_dPIStage_MovePos[X_AXIS_E873] = -dInc_position;

	sTemp.Format(_T("Clicked relative move, (Move Value : %.1f)"), m_dPIStage_MovePos[X_AXIS_E873]);
	SaveLogFile("FilterStage", sTemp);

	int nRet = PI_Move_Relative();
	if (nRet != 0)
	{
		sTemp.Format(_T("Failed to move relative. (Move Value : %.1f), Error Code : %d"), m_dPIStage_MovePos[X_AXIS_E873], nRet);
		AfxMessageBox(sTemp, MB_ICONWARNING);
	}
	else
		sTemp.Format(_T("Success to move relative. (Move Value : %.1f), Current Position : %.1f"), m_dPIStage_MovePos[X_AXIS_E873], m_dPIStage_GetPos);
	
	SaveLogFile("FilterStage", sTemp);
}


void CFilterStageDlg::OnBnClickedButtonPiFilterMoveP()
{
	g_pLog->Display(0, _T("CFilterStageDlg::OnBnClickedButtonPiFilterMoveP() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (m_bConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bEngineerMode != TRUE)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "1234")
		{
			AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
			return;
		}
	}

	CString sTemp;
	CString strStep;
	double dInc_position;
	m_RelMoveCtrl.GetWindowText(strStep);
	dInc_position = atof(strStep);

	sTemp.Format(_T("%.1f 만큼 상대위치로 이동할까요?"), dInc_position);
	if (IDYES != AfxMessageBox(sTemp, MB_YESNO)) return;

	double absolute_pos = dInc_position + m_dPIStage_GetPos;
	if (absolute_pos < m_dPIStage_MinusLimitPos[X_AXIS_E873] || absolute_pos > m_dPIStage_PlusLimitPos[X_AXIS_E873])
	{
		AfxMessageBox(_T("입력 범위를 초과하였습니다. (-13 ~ 13mm)"), MB_ICONWARNING);
		return;
	}

	m_dPIStage_MovePos[X_AXIS_E873] = dInc_position;

	sTemp.Format(_T("Clicked relative move, (Move Value : %.1f)"), m_dPIStage_MovePos[X_AXIS_E873]);
	SaveLogFile("FilterStage", sTemp);

	int nRet = PI_Move_Relative();
	if (nRet != 0)
	{
		sTemp.Format(_T("Failed to move relative. (Move Value : %.1f), Error Code : %d"), m_dPIStage_MovePos[X_AXIS_E873], nRet);
		AfxMessageBox(sTemp, MB_ICONWARNING);
	}
	else
		sTemp.Format(_T("Success to move relative. (Move Value : %.1f), Current Position : %.1f"), m_dPIStage_MovePos[X_AXIS_E873], m_dPIStage_GetPos);

	SaveLogFile("FilterStage", sTemp);
}


void CFilterStageDlg::OnBnClickedButtonFilterAbsMove()
{
	g_pLog->Display(0, _T("CFilterStageDlg::OnBnClickedButtonFilterAbsMove() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (m_bConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bEngineerMode != TRUE)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "1234")
		{
			AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
			return;
		}
	}

	double absolute_pos;
	CString sTemp;
	CString str;
	m_AbsMoveCtrl.GetWindowText(str);
	absolute_pos = atof(str);

	sTemp.Format(_T("%.1f 절대위치로 이동할까요?"), absolute_pos);
	if (IDYES != AfxMessageBox(sTemp, MB_YESNO)) return;

	if (absolute_pos < m_dPIStage_MinusLimitPos[X_AXIS_E873] || absolute_pos > m_dPIStage_PlusLimitPos[X_AXIS_E873])
	{
		AfxMessageBox(_T("입력 범위를 초과하였습니다. (-13 ~ 13mm)"), MB_ICONWARNING);
		return;
	}

	m_dPIStage_MovePos[X_AXIS_E873] = absolute_pos;

	sTemp.Format(_T("Clicked absolute move, (Move Value : %.1f)"), m_dPIStage_MovePos[X_AXIS_E873]);
	SaveLogFile("FilterStage", sTemp);

	int nRet = PI_Move_Absolute();
	if (nRet != 0)
	{
		sTemp.Format(_T("Failed to move absolute. (Move Pos : %.1f), Error Code : %d"), absolute_pos, nRet);
		AfxMessageBox(sTemp, MB_ICONWARNING);
	}
	else
		sTemp.Format(_T("Success to move absolute. (Move Pos : %.1f), Current Position : %.1f"), absolute_pos, m_dPIStage_GetPos);

	SaveLogFile("FilterStage", sTemp);
}


void CFilterStageDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case FILTERSTAGE_UPDATE_TIMER:
		GetDeviceStatus();		
		SetTimer(nIDEvent, 1000, NULL);
		break;
	default:
		break;
	}

	__super::OnTimer(nIDEvent);
}

void CFilterStageDlg::OnDeltaposSpinPiFilter(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);

	double position;
	CString str;

	m_RelMoveCtrl.GetWindowText(str);
	position = atof(str);

	if (pNMUpDown->iDelta < 0)
	{
		position++;

		if (position > X_PLUSLIMIT_E873)
			position = X_PLUSLIMIT_E873;

		str.Format(_T("%10.1f"), position);
		m_RelMoveCtrl.SetWindowText(str);
	}
	else
	{
		position--;

		if (position < 0)
			position = 0;

		str.Format(_T("%10.1f"), position);
		m_RelMoveCtrl.SetWindowText(str);
	}

	*pResult = 0;
}


void CFilterStageDlg::OnBnClickedButtonPiFilterSvoOn()
{
	g_pLog->Display(0, _T("CFilterStageDlg::OnBnClickedButtonPiFilterSvoOn() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (m_bConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bEngineerMode != TRUE)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "1234")
		{
			AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
			return;
		}
	}

	if (IDYES != AfxMessageBox("서보 온할까요?", MB_YESNO)) return;

	CString sTemp;
	sTemp.Format(_T("Clicked servo on"));
	SaveLogFile("FilterStage", sTemp);

	int nRet = ServoOn();
	if (nRet != 0)
	{
		sTemp.Format(_T("Failed servo on. (Error Code : %d)"), nRet);
		AfxMessageBox(sTemp, MB_ICONWARNING);
	}
	else
		sTemp.Format(_T("Success servo on."));
		
	SaveLogFile("FilterStage", sTemp);
}


void CFilterStageDlg::OnBnClickedButtonPiFilterSvoOff()
{
	g_pLog->Display(0, _T("CFilterStageDlg::OnBnClickedButtonPiFilterSvoOff() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (m_bConnect != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (m_bEngineerMode != TRUE)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "1234")
		{
			AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
			return;
		}
	}

	if (IDYES != AfxMessageBox("서보 오프할까요?", MB_YESNO)) return;

	CString sTemp;
	sTemp.Format(_T("Clicked servo off"));
	SaveLogFile("FilterStage", sTemp);

	int nRet = ServoOff();
	if (nRet != 0)
	{
		sTemp.Format(_T("Failed servo off. (Error Code : %d)"), nRet);
		AfxMessageBox(sTemp, MB_ICONWARNING);
	}
	else
		sTemp.Format(_T("Success servo off."));

	SaveLogFile("FilterStage", sTemp);
}


void CFilterStageDlg::OnBnClickedButtonFilterSet()
{
	if (m_bEngineerMode != TRUE)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "1234")
		{
			AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
			return;
		}
	}

	if (IDYES != AfxMessageBox("필터 데이터를 저장할까요?", MB_YESNO)) return;

	CString sTemp;
	sTemp.Format(_T("Clicked save filter data."));
	SaveLogFile("FilterStage", sTemp);

	m_Pos1Ctrl.GetWindowText(sTemp);
	g_pConfig->m_sFilterPos1 = sTemp;
	m_Pos2Ctrl.GetWindowText(sTemp);
	g_pConfig->m_sFilterPos2 = sTemp;
	m_Pos3Ctrl.GetWindowText(sTemp);
	g_pConfig->m_sFilterPos3 = sTemp;

	g_pConfig->SaveFilterInfo();

	sTemp.Format(_T("Success to save a filter data. Pos1: %s, Pos2: %s, Pos3: %s"), 
		g_pConfig->m_sFilterPos1, g_pConfig->m_sFilterPos2, g_pConfig->m_sFilterPos3);
	SaveLogFile("FilterStage", sTemp);
}


void CFilterStageDlg::OnBnClickedFilterEngineerModeCheck()
{
	if (m_bEngineerMode != TRUE)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "1234")
		{
			((CButton*)GetDlgItem(IDC_FILTER_ENGINEER_MODE_CHECK))->SetCheck(FALSE);
			AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
			return;
		}

		((CButton*)GetDlgItem(IDC_FILTER_ENGINEER_MODE_CHECK))->SetWindowText(_T("OFF"));
	}
	else
	{
		((CButton*)GetDlgItem(IDC_FILTER_ENGINEER_MODE_CHECK))->SetWindowText(_T("ON"));
	}

	UpdateData(TRUE);
}
