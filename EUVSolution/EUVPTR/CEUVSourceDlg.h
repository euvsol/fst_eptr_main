﻿/**
 * EUV Source Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once


// CEUVSourceDlg 대화 상자

class CEUVSourceDlg : public CDialogEx , public CFSTEuvSourceCtrl, public IObserver
{
	DECLARE_DYNAMIC(CEUVSourceDlg)

public:
	CEUVSourceDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CEUVSourceDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_EUVSOURCE_DIALOG };
//#endif

protected:
	HICON m_LedIcon[3];
	BOOL  m_bChecked;

	BOOL m_bThreadExitFlag;
	BOOL m_bConnectThreadExitFlag;
	BOOL m_bPreviousMonitoringMode;
	CWinThread*	m_pStatusThread;
	CWinThread*	m_pConnectThread;

	static UINT EuvSourceStatusThread(LPVOID pParam);
	static UINT TryConnectThread(LPVOID pParam);

	void GetDeviceStatus();
	void InitializeControls();

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedSrcVacuumStartButton();
	afx_msg void OnBnClickedSrcVacuumStopButton2();
	afx_msg void OnBnClickedSrcEuvStartButton();
	afx_msg void OnBnClickedSrcEuvStopButton();
	afx_msg void OnBnClickedSrcShutterOpenButton();
	afx_msg void OnBnClickedSrcShutterCloseButton();

	CStatic m_MainVacuumCtrl;
	CStatic m_SubVacuumCtrl;
	CStatic m_BufVacuumCtrl;
	CStatic m_MfcNeonCtrl;
	CStatic m_NeonGasCtrl;
	CStatic m_ForelineCtrl;

public:

	int OpenDevice();

	double Get_Main_VacuumRate()	{ return m_dMainVacuumRate; }
	double Get_Sub_VacuumRate()		{ return m_dSubVacuumRate; }
	double Get_Buf_VacuumRate()		{ return m_dBufVacuumRate; }
	double Get_Stage_Position()		{ return m_dMirrorPosition; }

	BOOL Is_EUV_On()				{ return m_bEuvOnState; }
	BOOL Is_LaserShutter_Opened()	{ return m_bLaserShutterOpenState; }
	BOOL Is_SRC_Connected()			{ return m_bConnected; }
	BOOL Is_Shutter_Opened()		{ return m_bMechShutterOpenState; }
	BOOL Is_MonitoringMode()		{ return m_bMonitoringMode; }

	int SetMechShutterOpen(BOOL bState);
	int SetEUVSourceOn(BOOL bState);

	void	EmergencyStop();
};
