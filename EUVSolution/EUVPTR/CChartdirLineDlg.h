﻿#pragma once

#include "ChartViewer.h"
#include "afxwin.h"

// CChartdirLineDlg 대화 상자

class CChartdirLineDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CChartdirLineDlg)

public:
	CChartdirLineDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CChartdirLineDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LINE_CHART_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CChartViewer m_ChartViewer;

	XYChart *Multiline_chart;

	void drawChart(CChartViewer *viewer);


	CFont font;

	afx_msg void OnBnClickedCheckLine();
};
