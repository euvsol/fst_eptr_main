﻿/**
 * Process File Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once


// CProcessEditorDlg 대화 상자

class CProcessEditorDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CProcessEditorDlg)

public:
	CProcessEditorDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CProcessEditorDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PROCESS_EDITOR_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
};
