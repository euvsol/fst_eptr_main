﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CMaskEFEMDlg 대화 상자

IMPLEMENT_DYNAMIC(CMaskEFEMDlg, CDialogEx)

CMaskEFEMDlg::CMaskEFEMDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MASK_EFEM_DIALOG, pParent)
{
	m_nParamValue		=  0;
	m_pMtsThread		= NULL;
	g_pDevMgr->RegisterObserver(this);
}

CMaskEFEMDlg::~CMaskEFEMDlg()
{
}

void CMaskEFEMDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_MTS_ALIGN_ANGLE, m_comboAngleSelect);
	DDX_Control(pDX, IDC_MTS_ERRCODE_EDIT, m_ERRCodeCtrl);
}


BEGIN_MESSAGE_MAP(CMaskEFEMDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_PICK_LOADPORT_BUTTON, &CMaskEFEMDlg::OnBnClickedPickLoadportButton)
	ON_BN_CLICKED(IDC_PLACE_LOADPORT_BUTTON, &CMaskEFEMDlg::OnBnClickedPlaceLoadportButton)
	ON_BN_CLICKED(IDC_PICK_ALIGNER_BUTTON, &CMaskEFEMDlg::OnBnClickedPickAlignerButton)
	ON_BN_CLICKED(IDC_PLACE_ALIGNER_BUTTON, &CMaskEFEMDlg::OnBnClickedPlaceAlignerButton)
	ON_BN_CLICKED(IDC_PICK_STAGE_BUTTON, &CMaskEFEMDlg::OnBnClickedPickStageButton)
	ON_BN_CLICKED(IDC_PLACE_STAGE_BUTTON, &CMaskEFEMDlg::OnBnClickedPlaceStageButton)
	ON_BN_CLICKED(IDC_INIT_MTS_BUTTON, &CMaskEFEMDlg::OnBnClickedInitMtsButton)
	ON_BN_CLICKED(IDC_CLEAR_ALARM_BUTTON, &CMaskEFEMDlg::OnBnClickedClearAlarmButton)
	ON_BN_CLICKED(IDC_FOUP_LOAD_BUTTON, &CMaskEFEMDlg::OnBnClickedFoupLoadButton)
	ON_BN_CLICKED(IDC_FOUP_UNLOAD_BUTTON, &CMaskEFEMDlg::OnBnClickedFoupUnloadButton)
	ON_BN_CLICKED(IDC_FOUP_CLAMP_BUTTON, &CMaskEFEMDlg::OnBnClickedFoupClampButton)
	ON_BN_CLICKED(IDC_FOUP_UNCLAMP_BUTTON, &CMaskEFEMDlg::OnBnClickedFoupUnclampButton)
	ON_BN_CLICKED(IDC_FOUP_OPEN_BUTTON, &CMaskEFEMDlg::OnBnClickedFoupOpenButton)
	ON_BN_CLICKED(IDC_FOUP_CLOSE_BUTTON, &CMaskEFEMDlg::OnBnClickedFoupCloseButton)
	ON_BN_CLICKED(IDC_ROBOT_HOME_BUTTON, &CMaskEFEMDlg::OnBnClickedRobotHomeButton)
	ON_BN_CLICKED(IDC_ROBOT_PAUSE_CHECK, &CMaskEFEMDlg::OnBnClickedRobotPauseCheck)
	ON_BN_CLICKED(IDC_ROBOT_STOP_BUTTON, &CMaskEFEMDlg::OnBnClickedRobotStopButton)
	ON_BN_CLICKED(IDC_ROBOT_SET_SPEED_BUTTON, &CMaskEFEMDlg::OnBnClickedRobotSetSpeedButton)
	ON_BN_CLICKED(IDC_ALIGN_BUTTON, &CMaskEFEMDlg::OnBnClickedAlignButton)
	ON_BN_CLICKED(IDC_PIO_RETRY_BUTTON, &CMaskEFEMDlg::OnBnClickedPioRetryButton)
	ON_BN_CLICKED(IDC_PIO_COMPLETE_BUTTON, &CMaskEFEMDlg::OnBnClickedPioCompleteButton)
	ON_BN_CLICKED(IDC_PIO_RESET_BUTTON, &CMaskEFEMDlg::OnBnClickedPioResetButton)
	ON_BN_CLICKED(IDC_RFID_DATA_BUTTON, &CMaskEFEMDlg::OnBnClickedRfidDataButton)
	ON_BN_CLICKED(IDC_ALIGN_ORIGIN_BUTTON, &CMaskEFEMDlg::OnBnClickedAlignOriginButton)
	ON_BN_CLICKED(IDC_MTS_LPM_INIT_BUTTON, &CMaskEFEMDlg::OnBnClickedMtsLpmInitButton)
	ON_BN_CLICKED(IDC_MTS_ROBOT_INIT_BUTTON, &CMaskEFEMDlg::OnBnClickedMtsRobotInitButton)
	ON_BN_CLICKED(IDC_ROBOT_GET_SPEED_BUTTON, &CMaskEFEMDlg::OnBnClickedRobotGetSpeedButton)
	ON_BN_CLICKED(IDC_MTS_ALIGNER_INIT_BUTTON, &CMaskEFEMDlg::OnBnClickedMtsAlignerInitButton)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_PIO_MANUAL_BUTTON, &CMaskEFEMDlg::OnBnClickedPioManualButton)
	ON_BN_CLICKED(IDC_PIO_AMHS_BUTTON, &CMaskEFEMDlg::OnBnClickedPioAmhsButton)
END_MESSAGE_MAP()


// CMaskEFEMDlg 메시지 처리기


int CMaskEFEMDlg::OpenDevice()
{
	int nRet = 0;
	
	nRet = OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_MTS], g_pConfig->m_nPORT[ETHERNET_MTS], FALSE);

	if (nRet == 0)
	{
		int nRet = GetRobotSpeed();

		if (nRet == 0)
		{
			int nSpeed;
			CString strSpeed;

			nSpeed = atoi(m_strRobotSpeed.Left(2));
			strSpeed.Format("%d", nSpeed);
			SetDlgItemText(IDC_MTS_SPEED_WITHOUTMASK_EDIT, strSpeed);
			nSpeed = atoi(m_strRobotSpeed.Mid(2, 2));
			strSpeed.Format("%d", nSpeed);
			SetDlgItemText(IDC_MTS_SPEED_WITHMASK_EDIT, strSpeed);
		}

		GetMtsStatus();
		GetLoadportStatus();
		GetRobotStatus();
		GetRotatorStatus();

		SetTimer(MTS_UPDATE_TIMER, 300, NULL);
	}

	return nRet;
}


BOOL CMaskEFEMDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CMaskEFEMDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	InitializeControls();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CMaskEFEMDlg::InitializeControls()
{
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_MTS_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_EMO))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_ERROR))->SetIcon(m_LedIcon[0]);

	((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_INIT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_WORKING))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_POD_PRESENCE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_OPEN))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_CLAMP))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_MASK_PRESENCE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_ERROR))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_ERROR))->SetIcon(m_LedIcon[0]);

	((CStatic*)GetDlgItem(IDC_ICON_MTS_ROBOT_INIT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_ROBOT_WORKING))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_ROBOT_PRESENCE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_ROBOT_ERROR))->SetIcon(m_LedIcon[0]);

	((CStatic*)GetDlgItem(IDC_ICON_MTS_ALIGN_INIT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_ALIGN_WORKING))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_ALIGN_PRESENCE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_ALIGN_ERROR))->SetIcon(m_LedIcon[0]);

	((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN0))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN1))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN2))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN3))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN4))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN5))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN6))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN7))->SetIcon(m_LedIcon[0]);

	((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT0))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT1))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT2))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT3))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT4))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT5))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT6))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT7))->SetIcon(m_LedIcon[0]);

	((CStatic*)GetDlgItem(IDC_ICON_MTS_PIO_MANUAL))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MTS_PIO_AMHS))->SetIcon(m_LedIcon[0]);
	

	m_comboAngleSelect.AddString(_T("  0 ˚"));
	m_comboAngleSelect.AddString(_T(" 90 ˚"));
	m_comboAngleSelect.AddString(_T("180 ˚"));
	m_comboAngleSelect.AddString(_T("270 ˚"));
	m_comboAngleSelect.SetCurSel(0);

	SetDlgItemText(IDC_MTS_SPEED_WITHOUTMASK_EDIT, _T("5"));
	SetDlgItemText(IDC_MTS_SPEED_WITHMASK_EDIT, _T("5"));
}

void CMaskEFEMDlg::GetDeviceStatus()
{
	if(m_bConnected == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_CONNECT))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_CONNECT))->SetIcon(m_LedIcon[0]);

	if (m_bMtsEMOStatus == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_EMO))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_EMO))->SetIcon(m_LedIcon[0]);

	if (m_bMtsError == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ERROR))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ERROR))->SetIcon(m_LedIcon[0]);

	if (m_bLpmInitComp == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_INIT))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_INIT))->SetIcon(m_LedIcon[0]);

	if (m_bLpmWorking == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_WORKING))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_WORKING))->SetIcon(m_LedIcon[0]);

	if (m_bPodOnLPM == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_POD_PRESENCE))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_POD_PRESENCE))->SetIcon(m_LedIcon[0]);

	if (m_bPodOpened == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_OPEN))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_OPEN))->SetIcon(m_LedIcon[0]);

	if (m_bLoadportClamped == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_CLAMP))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_CLAMP))->SetIcon(m_LedIcon[0]);

	if (m_bMaskInPod == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_MASK_PRESENCE))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_MASK_PRESENCE))->SetIcon(m_LedIcon[0]);

	if (m_bLpmError == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_ERROR))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_LPM_ERROR))->SetIcon(m_LedIcon[0]);

	if (m_bRobotInitComp == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ROBOT_INIT))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ROBOT_INIT))->SetIcon(m_LedIcon[0]);

	if (m_bRobotWorking == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ROBOT_WORKING))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ROBOT_WORKING))->SetIcon(m_LedIcon[0]);

	if (m_bRobotWithMask == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ROBOT_PRESENCE))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ROBOT_PRESENCE))->SetIcon(m_LedIcon[0]);

	if (m_bRobotError == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ROBOT_ERROR))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ROBOT_ERROR))->SetIcon(m_LedIcon[0]);

	if (m_bRotatorInitComp == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ALIGN_INIT))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ALIGN_INIT))->SetIcon(m_LedIcon[0]);

	if (m_bRotatorWorking == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ALIGN_WORKING))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ALIGN_WORKING))->SetIcon(m_LedIcon[0]);

	if (m_bRotatorWithMask == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ALIGN_PRESENCE))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ALIGN_PRESENCE))->SetIcon(m_LedIcon[0]);

	if (m_bRotatorError == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ALIGN_ERROR))->SetIcon(m_LedIcon[2]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_MTS_ALIGN_ERROR))->SetIcon(m_LedIcon[0]);

	////////////////////////////////// PIO Status /////////////////////////////

	if (m_bPioManualMode == TRUE)
	{
		((CStatic*)GetDlgItem(IDC_ICON_MTS_PIO_MANUAL))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_PIO_AMHS))->SetIcon(m_LedIcon[0]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_MTS_PIO_MANUAL))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_PIO_AMHS))->SetIcon(m_LedIcon[1]);
	}

	if (m_strPioInBit != _T(""))
	{
		if (m_strPioInBit.Left(1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN7))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN7))->SetIcon(m_LedIcon[0]);

		if (m_strPioInBit.Mid(1, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN6))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN6))->SetIcon(m_LedIcon[0]);

		if (m_strPioInBit.Mid(2, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN5))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN5))->SetIcon(m_LedIcon[0]);

		if (m_strPioInBit.Mid(3, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN4))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN4))->SetIcon(m_LedIcon[0]);

		if (m_strPioInBit.Mid(4, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN3))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN3))->SetIcon(m_LedIcon[0]);

		if (m_strPioInBit.Mid(5, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN2))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN2))->SetIcon(m_LedIcon[0]);

		if (m_strPioInBit.Mid(6, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN1))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN1))->SetIcon(m_LedIcon[0]);

		if (m_strPioInBit.Mid(7, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN0))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN0))->SetIcon(m_LedIcon[0]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN0))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN1))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN2))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN3))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN4))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN5))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN6))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_AMHS_IN7))->SetIcon(m_LedIcon[0]);
	}
	
	if (m_strPioOutBit != _T(""))
	{
		if (m_strPioOutBit.Left(1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT7))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT7))->SetIcon(m_LedIcon[0]);

		if (m_strPioOutBit.Mid(1, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT6))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT6))->SetIcon(m_LedIcon[0]);

		if (m_strPioOutBit.Mid(2, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT5))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT5))->SetIcon(m_LedIcon[0]);

		if (m_strPioOutBit.Mid(3, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT4))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT4))->SetIcon(m_LedIcon[0]);

		if (m_strPioOutBit.Mid(4, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT3))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT3))->SetIcon(m_LedIcon[0]);

		if (m_strPioOutBit.Mid(5, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT2))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT2))->SetIcon(m_LedIcon[0]);

		if (m_strPioOutBit.Mid(6, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT1))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT1))->SetIcon(m_LedIcon[0]);

		if (m_strPioOutBit.Mid(7, 1) == '1')
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT0))->SetIcon(m_LedIcon[2]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT0))->SetIcon(m_LedIcon[0]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT0))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT1))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT2))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT3))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT4))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT5))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT6))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MTS_EQ_OUT7))->SetIcon(m_LedIcon[0]);
	}

	if (m_ERRCodeCtrl.m_hWnd != NULL)
		m_ERRCodeCtrl.SetWindowText(m_strErrorCode);
}

void CMaskEFEMDlg::OnDestroy()
{
	CDialogEx::OnDestroy();
}

//
// Mask Transfer
//
void CMaskEFEMDlg::OnBnClickedPickLoadportButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedPickLoadportButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("POD에 있는 MASK를 가져올까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = ROBOT_PICK_LPM;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}


void CMaskEFEMDlg::OnBnClickedPlaceLoadportButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedPlaceLoadportButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("POD에 MASK를 가져다 놓을까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = ROBOT_PLACE_LPM;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}


void CMaskEFEMDlg::OnBnClickedPickAlignerButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedPickAlignerButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("ROTATOR에 있는 MASK를 가져올까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = ROBOT_PICK_ROTATOR;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}


void CMaskEFEMDlg::OnBnClickedPlaceAlignerButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedPlaceAlignerButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("ROTATOR에 MASK를 가져다 놓을까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = ROBOT_PLACE_ROTATOR;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}

void CMaskEFEMDlg::OnBnClickedPickStageButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedPickStageButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("LLC에 있는 MASK를 가져올까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	int nRet = g_pIO->Is_Mask_OnLLC();
	if (nRet != MASK_ON)
	{
		AfxMessageBox(_T("LLC에 마스크가 없습니다."), MB_ICONWARNING);
		return;
	}

	nRet = g_pIO->Is_LLC_Mask_Slant_Check();
	if (nRet != FALSE)
	{
		AfxMessageBox(_T("LLC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != FALSE)
	{
		AfxMessageBox(_T("MTS Robot에 마스크가 있습니다."), MB_ICONWARNING);
		return;
	}

	if (g_pIO->Is_Isolator_Up() != TRUE)
	{
		AfxMessageBox(_T("Isolator가 동작중이 아닙니다."), MB_ICONWARNING);
		return;
	}

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_OPENED)
	{
		AfxMessageBox(_T("LLC Gate Valve가 닫혀있습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = ROBOT_PICK_STAGE;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}


void CMaskEFEMDlg::OnBnClickedPlaceStageButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedPlaceStageButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("LLC에 MASK를 가져다 놓올까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	int nRet = g_pIO->Is_Mask_OnLLC();
	if (nRet != MASK_NONE)
	{
		AfxMessageBox(_T("LLC에 마스크가 있습니다."), MB_ICONWARNING);
		return;
	}

	nRet = g_pIO->Is_LLC_Mask_Slant_Check();
	if (nRet != FALSE)
	{
		AfxMessageBox(_T("LLC에 마스크 기울기 센서가 감지되었습니다."), MB_ICONWARNING);
		return;
	}

	if (g_pEfem->Is_MASK_OnMTSRobot() != TRUE)
	{
		AfxMessageBox(_T("MTS Robot에 마스크가 없습니다."), MB_ICONWARNING);
		return;
	}

	if (g_pIO->Is_Isolator_Up() != TRUE)
	{
		AfxMessageBox(_T("Isolator가 동작중이 아닙니다."), MB_ICONWARNING);
		return;
	}

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_OPENED)
	{
		AfxMessageBox(_T("LLC Gate Valve가 닫혀있습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = ROBOT_PLACE_STAGE;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}

//
// MTS Operate
//
void CMaskEFEMDlg::OnBnClickedInitMtsButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedInitMtsButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("MTS INITIALIZE 동작을 할까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = MTS_INIT;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}


void CMaskEFEMDlg::OnBnClickedClearAlarmButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedClearAlarmButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	ClearAllAlarm();
}

//
// Loadport Operate
//
void CMaskEFEMDlg::OnBnClickedFoupLoadButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedFoupLoadButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("LPM LOAD 동작을 할까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = LPM_LOAD;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}


void CMaskEFEMDlg::OnBnClickedFoupUnloadButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedFoupUnloadButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("LPM UNLOAD 동작을 할까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = LPM_UNLOAD;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}


void CMaskEFEMDlg::OnBnClickedFoupClampButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedFoupClampButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("LPM CLAMP 동작을 할까요?", MB_YESNO)) return;

	ClampPod();
}


void CMaskEFEMDlg::OnBnClickedFoupUnclampButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedFoupUnclampButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("LPM UNCLAMP 동작을 할까요?", MB_YESNO)) return;

	UnclampPod();
}


void CMaskEFEMDlg::OnBnClickedFoupOpenButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedFoupOpenButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("POD OPEN 동작을 할까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = LPM_OPEN;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}


void CMaskEFEMDlg::OnBnClickedFoupCloseButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedFoupCloseButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("POD CLOSE 동작을 할까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = LPM_CLOSE;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}

//
// Robot Operate
//
void CMaskEFEMDlg::OnBnClickedRobotHomeButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedRobotHomeButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("ROBOT HOME 동작을 할까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = ROBOT_HOME;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}

void CMaskEFEMDlg::OnBnClickedRobotPauseCheck()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedRobotPauseCheck() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	CButton* btn = (CButton*)GetDlgItem(IDC_ROBOT_PAUSE_CHECK);

	int nCheck = btn->GetCheck();

	if (nCheck)
	{
		RobotMovePause();
		btn->SetWindowTextA(_T("Resume"));
	}
	else
	{
		RobotMoveResume();
		btn->SetWindowTextA(_T("Pause"));
	}
}


void CMaskEFEMDlg::OnBnClickedRobotStopButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedRobotStopButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	RobotMoveStop();
}


void CMaskEFEMDlg::OnBnClickedRobotSetSpeedButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedRobotSetSpeedButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("ROBOT SPEED 설정을 저장할까요?", MB_YESNO)) return;

	CString strWithoutMaskSpeed, strWithMaskSpeed;

	GetDlgItemText(IDC_MTS_SPEED_WITHOUTMASK_EDIT, strWithoutMaskSpeed);
	GetDlgItemText(IDC_MTS_SPEED_WITHMASK_EDIT, strWithMaskSpeed);

	int nWithout = atoi(strWithoutMaskSpeed);
	int nWith	 = atoi(strWithMaskSpeed);

	SetRobotSpeed(nWithout, nWith, 1, 1);
}


//
// Aligner Operate
//
void CMaskEFEMDlg::OnBnClickedAlignButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedAlignButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("MASK ROTATE 동작을 할까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	CString strAngleData;
	m_comboAngleSelect.GetLBText(m_comboAngleSelect.GetCurSel(), strAngleData);
	m_nParamValue = atoi(strAngleData);

	MtsState = ROTATE_MASK;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}



void CMaskEFEMDlg::OnBnClickedAlignOriginButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedAlignOriginButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("ALIGNER ORIGIN 동작을 할까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = ROTATOR_ORIGIN;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}

//
// PIO Communication
//
void CMaskEFEMDlg::OnBnClickedPioRetryButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedPioRetryButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	RetryE84Communication();
}


void CMaskEFEMDlg::OnBnClickedPioCompleteButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedPioCompleteButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	ConcludeE84Communication();
}


void CMaskEFEMDlg::OnBnClickedPioResetButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedPioResetButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	ResetPioError();
}


void CMaskEFEMDlg::OnBnClickedRfidDataButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedRfidDataButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	int nRet = 0;

	nRet = ReadRfidData(0);

//	if (nRet == 0)
//		SetDlgItemText(IDC_MTS_RFID_READ_DATA_EDIT, m_strRfidTagData);
//	else
//		AfxMessageBox(_T("RFID Read 동작이 실패했습니다."));
}

UINT CMaskEFEMDlg::MtsCommandThread(LPVOID pClass)
{
	CMaskEFEMDlg* pHWCntl = (CMaskEFEMDlg *)pClass;

	int nRet = 0;

	switch (pHWCntl->MtsState)
	{
	case MTS_INIT:
		g_pAnimationGUI->SendMessageData(_T("EQ_MTS_INIT"));
		nRet = pHWCntl->InitDevice();
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if(nRet == -2) nRet = MTS_COMMUNICATION_INIT_TIMEOUT;
			else nRet = MTS_INITIALIZE_ALL_MODULE_ERROR;
		}
		break;
	case LPM_INIT:
		nRet = pHWCntl->InitModule(LPM_UNIT);
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_INIT_TIMEOUT;
			else nRet = MTS_INITIALIZE_LPM_ERROR;
		}
		break;
	case ROBOT_INIT:
		g_pAnimationGUI->SendMessageData(_T("EQ_ATR_INIT"));
		nRet = pHWCntl->InitModule(ROBOT_UNIT);
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_INIT_TIMEOUT;
			else nRet = MTS_INITIALIZE_ROBOT_ERROR;
		}
		break;
	case ROTATOR_INIT:
		g_pAnimationGUI->SendMessageData(_T("EQ_ROTATOR_WORKING"));
		nRet = pHWCntl->InitModule(ALIGN_UNIT);
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_INIT_TIMEOUT;
			else nRet = MTS_INITIALIZE_ROTATOR_ERROR;
		}
		break;
	case LPM_LOAD:
		g_pAnimationGUI->SendMessageData(_T("EQ_LPM_LOAD"));
		nRet = pHWCntl->LoadPod();
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_LPM_DONE_TIMEOUT;
			else nRet = MTS_LOADING_POD_ERROR;
		}
		else
		{
			if (pHWCntl->m_bMaskInPod == TRUE)
				g_pConfig->SaveMaterialLocation(MTS_POD);
		}
		break;
	case LPM_UNLOAD:
		g_pAnimationGUI->SendMessageData(_T("EQ_LPM_UNLOAD"));
		nRet = pHWCntl->UnloadPod();
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_LPM_DONE_TIMEOUT;
			else nRet = MTS_UNLOADING_POD_ERROR;
		}
		else
		{
			if(g_pConfig->m_nMaterialLocation == MTS_POD)
				g_pConfig->SaveMaterialLocation(UNKNOWN);
		}
		break;
	case LPM_OPEN:
		nRet = pHWCntl->OpenPod();
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_LPM_DONE_TIMEOUT;
			else nRet = MTS_OPEN_POD_ERROR;
		}
		break;
	case LPM_CLOSE:
		nRet = pHWCntl->ClosePod();
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_LPM_DONE_TIMEOUT;
			else nRet = MTS_CLOSE_POD_ERROR;
		}
		break;
	case ROTATE_MASK:
		g_pAnimationGUI->SendMessageData(_T("EQ_ROTATOR_WORKING"));
		nRet = pHWCntl->RotateMask(pHWCntl->m_nParamValue);
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_ROTATOR_DONE_TIMEOUT;
			else nRet = MTS_ROTATE_MASK_ERROR;
		}
		break;
	case ROTATOR_ORIGIN:
		g_pAnimationGUI->SendMessageData(_T("EQ_ROTATOR_WORKING"));
		nRet = pHWCntl->OriginRotator();
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_ROTATOR_DONE_TIMEOUT;
			else nRet = MTS_ORIGIN_ROTATOR_ERROR;
		}
		break;
	case ROBOT_HOME:
		g_pAnimationGUI->SendMessageData(_T("EQ_ATR_INIT"));
		nRet = pHWCntl->RobotMoveHome();
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
			else nRet = MTS_ROBOT_HOME_ERROR;
		}
		break;
	case ROBOT_PICK_LPM:
		g_pAnimationGUI->SendMessageData(_T("EQ_ATR_PICK_LPM"));
		nRet = pHWCntl->PickMaskFromLoadPort();
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
			else nRet = MTS_ROBOT_PICK_LPM_ERROR;
		}
		else
		{
			if (pHWCntl->m_bRobotWithMask == TRUE)
				g_pConfig->SaveMaterialLocation(MTS_ROBOT_HAND);
		}
		break;
	case ROBOT_PLACE_LPM:
		g_pAnimationGUI->SendMessageData(_T("EQ_ATR_PLACE_LPM"));
		nRet = pHWCntl->PlaceMaskToLoadPort();
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
			else nRet = MTS_ROBOT_PLACE_LPM_ERROR;
		}
		else
			g_pConfig->SaveMaterialLocation(MTS_POD);
		break;
	case ROBOT_PICK_ROTATOR:
		g_pAnimationGUI->SendMessageData(_T("EQ_ATR_PICK_ROTATOR"));
		nRet = pHWCntl->PickMaskFromRotator();
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
			else nRet = MTS_ROBOT_PICK_ROTATOR_ERROR;
		}
		else
		{	
			if(pHWCntl->m_bRobotWithMask == TRUE)
				g_pConfig->SaveMaterialLocation(MTS_ROBOT_HAND);

			g_pAnimationGUI->SendMessageData(_T("EQ_ROTATOR_WORKING"));
			nRet = pHWCntl->OriginRotator();
			if (nRet != 0)
			{
				if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
				else if (nRet == -2) nRet = MTS_COMMUNICATION_ROTATOR_DONE_TIMEOUT;
				else nRet = MTS_ORIGIN_ROTATOR_ERROR;
			}
		}
		break;
	case ROBOT_PLACE_ROTATOR:
		g_pAnimationGUI->SendMessageData(_T("EQ_ATR_PLACE_ROTATOR"));
		nRet = pHWCntl->PlaceMaskToRotator();
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
			else nRet = MTS_ROBOT_PLACE_ROTATOR_ERROR;
		}
		else
		{
			if(pHWCntl->m_bRotatorWithMask == TRUE)
				g_pConfig->SaveMaterialLocation(MTS_ROTATOR);
		}
		break;
	case ROBOT_PICK_STAGE:
		g_pAnimationGUI->SendMessageData(_T("EQ_ATR_PICK_STAGE"));
		nRet = pHWCntl->PickMaskFromStage();
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
			else nRet = MTS_ROBOT_PICK_LLC_STAGE_ERROR;
		}
		else
		{
			if(pHWCntl->m_bRobotWithMask == TRUE)
				g_pConfig->SaveMaterialLocation(MTS_ROBOT_HAND);
		}
		break;
	case ROBOT_PLACE_STAGE:
		g_pAnimationGUI->SendMessageData(_T("EQ_ATR_PLACE_STAGE"));
		nRet = pHWCntl->PlaceMaskToStage();
		if (nRet != 0)
		{
			if (nRet == -1) nRet = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (nRet == -2) nRet = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
			else nRet = MTS_ROBOT_PLACE_LLC_STAGE_ERROR;
		}
		else
		{
			if(g_pIO->Is_Mask_Check_Only_OnLLC() == MASK_ON)
				g_pConfig->SaveMaterialLocation(LLC);
		}
		break;
	}

	pHWCntl->m_pMtsThread = NULL;

	if (nRet != 0)
		g_pAlarm->SetAlarm(nRet);

	return 0;
}

void CMaskEFEMDlg::OnBnClickedMtsLpmInitButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedMtsLpmInitButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("LPM INITIALIZE 동작을 할까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = LPM_INIT;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}


void CMaskEFEMDlg::OnBnClickedMtsRobotInitButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedMtsRobotInitButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("ROBOT INITIALIZE 동작을 할까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = ROBOT_INIT;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}

void CMaskEFEMDlg::OnBnClickedRobotGetSpeedButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedRobotGetSpeedButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	int nRet = GetRobotSpeed();

	if (nRet == 0)
	{
		CString strSpeed = m_strRobotSpeed;

		int nWithoutSpeed = atoi(strSpeed.Left(2));
		int nWithSpeed = atoi(strSpeed.Mid(2, 2));

		CString sWithoutSpeed;
		CString sWithSpeed;

		sWithoutSpeed.Format("%d", nWithoutSpeed);
		sWithSpeed.Format("%d", nWithSpeed);

		SetDlgItemText(IDC_MTS_SPEED_WITHOUTMASK_EDIT, sWithoutSpeed);
		SetDlgItemText(IDC_MTS_SPEED_WITHMASK_EDIT, sWithSpeed);
	}
}


void CMaskEFEMDlg::OnBnClickedMtsAlignerInitButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedMtsAlignerInitButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	if (IDYES != AfxMessageBox("ALIGNER INITIALIZE 동작을 할까요?", MB_YESNO)) return;

	if (m_pMtsThread != NULL)
	{
		AfxMessageBox(_T("이미 다른 작업이 수행 중이므로 동작할 수 없습니다."), MB_ICONWARNING);
		return;
	}

	MtsState = ROTATOR_INIT;
	m_pMtsThread = AfxBeginThread(CMaskEFEMDlg::MtsCommandThread, this);
}






void CMaskEFEMDlg::OnBnClickedPioManualButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedPioManualButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	SetPioModeToAMHS(FALSE);
}


void CMaskEFEMDlg::OnBnClickedPioAmhsButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CMaskEFEMDlg::OnBnClickedPioAmhsButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."), MB_ICONWARNING);
		return;
	}

	SetPioModeToAMHS(TRUE);
}

int CMaskEFEMDlg::Is_MTS_OK()
{
	int ret = 0;
	
	if (m_bOccurAlarm == TRUE)
		ret = atoi(m_strErrorCode);

	return ret;
}

int CMaskEFEMDlg::MoveReadyPos_toFrontLLC()
{
	int ret = 0;
	g_pAnimationGUI->SendMessageData(_T("EQ_ATR_READY_STAGE"));
	ret = SetRobotReadyPos(STAGE_UNIT);
	if (ret != 0)
	{
		if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
		else if (ret == -2) ret = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
		else ret = MTS_ROBOT_MOVE_READY_POS_ERROR;
	}
	return ret;
}

int CMaskEFEMDlg::Transfer_Mask(int nSource, int nTarget)
{
	int ret = 0;

	if (nSource == MTS_ROBOT_HAND) //Place Mask
	{
		switch (nTarget)
		{
			case MTS_POD :
				g_pAnimationGUI->SendMessageData(_T("EQ_ATR_PLACE_LPM"));
				ret = PlaceMaskToLoadPort();
				if (ret != 0)
				{
					if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
					else if (ret == -2) ret = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
					else ret = MTS_ROBOT_PLACE_LPM_ERROR;
				}
				else
					g_pConfig->SaveMaterialLocation(MTS_POD);
				break;
			case LLC:
				g_pAnimationGUI->SendMessageData(_T("EQ_ATR_PLACE_STAGE"));
				ret = PlaceMaskToStage();
				if (ret != 0)
				{
					if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
					else if (ret == -2) ret = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
					else ret = MTS_ROBOT_PLACE_LLC_STAGE_ERROR;
				}
				else
				{
					if(g_pIO->Is_Mask_Check_Only_OnLLC() == MASK_ON)
						g_pConfig->SaveMaterialLocation(LLC);
				}
				break;
			case MTS_ROTATOR:
				g_pAnimationGUI->SendMessageData(_T("EQ_ATR_PLACE_ROTATOR"));
				ret = PlaceMaskToRotator();
				if (ret != 0)
				{
					if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
					else if (ret == -2) ret = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
					else ret = MTS_ROBOT_PLACE_ROTATOR_ERROR;
				}
				else
				{
					if(m_bRotatorWithMask == TRUE)
						g_pConfig->SaveMaterialLocation(MTS_ROTATOR);
				}
				break;
		}
	}
	else  //Pick Mask
	{
		switch (nSource)
		{
		case MTS_POD:
			g_pAnimationGUI->SendMessageData(_T("EQ_ATR_PICK_LPM"));
			ret = PickMaskFromLoadPort();
			if (ret != 0)
			{
				if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
				else if (ret == -2) ret = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
				else ret = MTS_ROBOT_PICK_LPM_ERROR;
			}
			else
			{
				if (m_bRobotWithMask == TRUE)
					g_pConfig->SaveMaterialLocation(MTS_ROBOT_HAND);
			}
			break;
		case LLC:
			g_pAnimationGUI->SendMessageData(_T("EQ_ATR_PICK_STAGE"));
			ret = PickMaskFromStage();
			if (ret != 0)
			{
				if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
				else if (ret == -2) ret = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
				else ret = MTS_ROBOT_PICK_LLC_STAGE_ERROR;
			}
			else
			{
				if (m_bRobotWithMask == TRUE)
					g_pConfig->SaveMaterialLocation(MTS_ROBOT_HAND);
			}
			break;
		case MTS_ROTATOR:
			g_pAnimationGUI->SendMessageData(_T("EQ_ATR_PICK_ROTATOR"));
			ret = PickMaskFromRotator();
			if (ret != 0)
			{
				if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
				else if (ret == -2) ret = MTS_COMMUNICATION_ROBOT_DONE_TIMEOUT;
				else ret = MTS_ROBOT_PICK_ROTATOR_ERROR;
				return ret;
			}
			else
			{
				if (m_bRobotWithMask == TRUE)
					g_pConfig->SaveMaterialLocation(MTS_ROBOT_HAND);

				g_pAnimationGUI->SendMessageData(_T("EQ_ROTATOR_WORKING"));
				ret = OriginRotator();
				if (ret != 0)
				{
					if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
					else if (ret == -2) ret = MTS_COMMUNICATION_ROTATOR_DONE_TIMEOUT;
					else ret = MTS_ORIGIN_ROTATOR_ERROR;
				}
			}
			break;
		}
	}

	return ret;
}

int CMaskEFEMDlg::Rotate_Mask(int nAngle, BOOL bLoading)
{
	int ret = 0;

	if (bLoading == TRUE)
	{
		g_pAnimationGUI->SendMessageData(_T("EQ_ROTATOR_WORKING"));
		ret = RotateMask(nAngle);
		if (ret != 0)
		{
			if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (ret == -2) ret = MTS_COMMUNICATION_ROTATOR_DONE_TIMEOUT;
			else ret = MTS_ROTATE_MASK_ERROR;
		}
	}
	else
	{
		g_pAnimationGUI->SendMessageData(_T("EQ_ROTATOR_WORKING"));
		ret = RotateMask(360 - nAngle);
		if (ret != 0)
		{
			if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
			else if (ret == -2) ret = MTS_COMMUNICATION_ROTATOR_DONE_TIMEOUT;
			else ret = MTS_ROTATE_MASK_ERROR;
		}
	}

	return ret;
}

int CMaskEFEMDlg::Load_POD()
{
	int ret = 0;

	g_pAnimationGUI->SendMessageData(_T("EQ_LPM_LOAD"));
	ret = LoadPod();
	if (ret != 0)
	{
		if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
		else if (ret == -2) ret = MTS_COMMUNICATION_LPM_DONE_TIMEOUT;
		else ret = MTS_LOADING_POD_ERROR;
	}
	else
	{
		if (m_bMaskInPod == TRUE)
			g_pConfig->SaveMaterialLocation(MTS_POD);
	}

	return ret;
}

int CMaskEFEMDlg::Unload_POD()
{
	int ret = 0;
	
	g_pAnimationGUI->SendMessageData(_T("EQ_LPM_UNLOAD"));
	ret = UnloadPod();
	if (ret != 0)
	{
		if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
		else if (ret == -2) ret = MTS_COMMUNICATION_LPM_DONE_TIMEOUT;
		else ret = MTS_UNLOADING_POD_ERROR;
	}
	else
	{
		if(g_pConfig->m_nMaterialLocation == MTS_POD)
			g_pConfig->SaveMaterialLocation(UNKNOWN);
	}

	return ret;
}

int CMaskEFEMDlg::Open_POD()
{
	int ret = 0;

	ret = OpenPod();
	if (ret != 0)
	{
		if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
		else if (ret == -2) ret = MTS_COMMUNICATION_LPM_DONE_TIMEOUT;
		else ret = MTS_OPEN_POD_ERROR;
	}

	return ret;
}

int CMaskEFEMDlg::Close_POD()
{
	int ret = 0;

	ret = ClosePod();
	if (ret != 0)
	{
		if (ret == -1) ret = MTS_COMMUNICATION_ACK_TIMEOUT;
		else if (ret == -2) ret = MTS_COMMUNICATION_LPM_DONE_TIMEOUT;
		else ret = MTS_CLOSE_POD_ERROR;
	}

	return ret;
}

void CMaskEFEMDlg::EmergencyStop()
{
	CString str;
	str.Format(_T("CMaskEFEMDlg::EmergencyStop(), Robot Working %d"), m_bRobotWorking);
	g_pLog->Display(0, str);

	RobotMoveStop();
	str.Format(_T("CMaskEFEMDlg::EmergencyStop(), RobotMoveStop()"));
	g_pLog->Display(0, str);
}

void CMaskEFEMDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case MTS_UPDATE_TIMER:
		GetDeviceStatus();
		SetTimer(nIDEvent, 300, NULL);
		break;
	default:
		break;
	}

	__super::OnTimer(nIDEvent);
}

