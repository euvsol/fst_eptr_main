﻿// CBeamOptimizationDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#define PI 3.14159265
#define COMMUNICATIONCHECK 1
#include <vector>
using namespace std;
// CBeamOptimizationDlg 대화 상자

IMPLEMENT_DYNAMIC(CBeamAlignMainDlg, CDialogEx)

CBeamAlignMainDlg::CBeamAlignMainDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_BEAMALIGNMAIN_DIALOG, pParent)
{
	m_dBeamIniParaHorizontal = 0;
	m_dBeamIniParaVertical = 0;
	m_dBeamIniParaPitch = 0;
	m_dBeamIniParaYaw = 0;
	m_dBeamIniParaUx = 1;
	m_dBeamIniParaUy = 0;
	m_dBeamIniParaUz = 0;
	m_dBeamSetParaHorizontal = 0;
	m_dBeamSetParaVertical = 0;
	m_dBeamSetParaPitch = 0;
	m_dBeamSetParaYaw = 0;
	m_bAutoDestroyBmp = 0;

	m_dIntensity = 0.0;
}

CBeamAlignMainDlg::~CBeamAlignMainDlg()
{

}

void CBeamAlignMainDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BOOL CBeamAlignMainDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_COMMUNICATION))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_EXCEEDSYSTEMRANGE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_MOVECOMPLETE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_SYSTEMSTATUS))->SetIcon(m_LedIcon[0]);
	SetTimer(COMMUNICATIONCHECK, 100, NULL);
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR1))->SetCheck(TRUE);
	m_nDetectorIndex = 1;

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BEGIN_MESSAGE_MAP(CBeamAlignMainDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BEAMALIGNMAIN_BUTTON_SHOW2DSEARCHDLG, &CBeamAlignMainDlg::OnBnClickedButtonSqoneShow2DScanDlg)
	ON_BN_CLICKED(IDC_BEAMALIGNMAIN_BUTTON_SHOW1DSEARCHDLG, &CBeamAlignMainDlg::OnBnClickedButtonSqoneShow1DScanDlg)
	ON_BN_CLICKED(IDC_BEAMALIGNMAIN_BUTTON_SHOWAUTOALIGNDLG, &CBeamAlignMainDlg::OnBnClickedButtonSqoneShowautoalignDlg)
	ON_BN_CLICKED(IDC_BEAMALIGNMAIN_BUTTON_SHOWCONFIGDLG, &CBeamAlignMainDlg::OnBnClickedButtonBeammainShowautoaligndlg2d)
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BEAMALIGNMAIN_BUTTON_CONNECT, &CBeamAlignMainDlg::OnBnClickedButtonSqoneConnect)
	ON_BN_CLICKED(IDC_BEAMALIGNMAIN_BUTTON_DISCONNECT, &CBeamAlignMainDlg::OnBnClickedButtonSqoneDisconnect)
	ON_BN_CLICKED(IDC_BEAMALIGNMAIN_BUTTON_STOP, &CBeamAlignMainDlg::OnBnClickedButtonSqoneStop)
	ON_BN_CLICKED(IDC_BEAMALIGNMAIN_BUTTON_TEST, &CBeamAlignMainDlg::OnBnClickedButtonBeammainTest)
	ON_BN_CLICKED(IDC_BEAMALIGNMAIN_CHECK_DETECTOR1, &CBeamAlignMainDlg::OnBnClickedCheckBeammainDetector1)
	ON_BN_CLICKED(IDC_BEAMALIGNMAIN_CHECK_DETECTOR2, &CBeamAlignMainDlg::OnBnClickedCheckBeammainDetector2)
	ON_BN_CLICKED(IDC_BEAMALIGNMAIN_CHECK_DETECTOR3, &CBeamAlignMainDlg::OnBnClickedCheckBeammainDetector3)
	ON_BN_CLICKED(IDC_BEAMALIGNMAIN_CHECK_DETECTOR4, &CBeamAlignMainDlg::OnBnClickedCheckBeammainDetector4)
	ON_BN_CLICKED(IDC_BEAMALIGNMAIN_CHECK_SIMULATOR, &CBeamAlignMainDlg::OnBnClickedCheckBeammainSimulator)
END_MESSAGE_MAP()


// CBeamOptimizationDlg 메시지 처리기

void CBeamAlignMainDlg::OnBnClickedButtonSqoneShowautoalignDlg()
{
	g_pBeamSearch2D->ShowWindow(SW_HIDE);
	g_pBeamSearch1D->ShowWindow(SW_HIDE);
	g_pBeamConfig->ShowWindow(SW_HIDE);
	g_pBeamAutoAlign->ShowWindow(SW_SHOW);
	Display(0, "Auto Align Dialog Open Button Click");
}


void CBeamAlignMainDlg::OnBnClickedButtonSqoneShow2DScanDlg()
{
	g_pBeamSearch1D->ShowWindow(SW_HIDE);
	g_pBeamConfig->ShowWindow(SW_HIDE);
	g_pBeamAutoAlign->ShowWindow(SW_HIDE);
	g_pBeamSearch2D->ShowWindow(SW_SHOW);
	Display(0, "Scan Dialog Open Button Click");
}


void CBeamAlignMainDlg::OnBnClickedButtonSqoneShow1DScanDlg()
{
	g_pBeamSearch2D->ShowWindow(SW_HIDE);
	g_pBeamConfig->ShowWindow(SW_HIDE);
	g_pBeamAutoAlign->ShowWindow(SW_HIDE);
	g_pBeamSearch1D->ShowWindow(SW_SHOW);
	Display(0, "Optimization Dialog Open Button Click");
}

void CBeamAlignMainDlg::OnBnClickedButtonBeammainShowautoaligndlg2d()
{
	g_pBeamSearch2D->ShowWindow(SW_HIDE);
	g_pBeamSearch1D->ShowWindow(SW_HIDE);
	g_pBeamAutoAlign->ShowWindow(SW_HIDE);
	g_pBeamConfig->ShowWindow(SW_SHOW);
	Display(0, "Auto Align Dialog Open Button Click");
}


void CBeamAlignMainDlg::Tcip_EthernetCom_Check()
{
	if (g_pSqOneControl->m_bConnected)
	{
		((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_COMMUNICATION))->SetIcon(m_LedIcon[1]);
		if (g_pSqOneControl->m_SQOneParseData.TargetExceedRanged)
		{
			((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_EXCEEDSYSTEMRANGE))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_EXCEEDSYSTEMRANGE))->SetIcon(m_LedIcon[0]);
		}
		if (g_pSqOneControl->m_SQOneParseData.MoveComplete)
		{
			((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_MOVECOMPLETE))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_MOVECOMPLETE))->SetIcon(m_LedIcon[0]);
		}
		if (g_pSqOneControl->m_SQOneParseData.TrisphereStatus)
		{
			((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_SYSTEMSTATUS))->SetIcon(m_LedIcon[2]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_SYSTEMSTATUS))->SetIcon(m_LedIcon[1]);
		}
		CString strIntensity = "";
		strIntensity.Format(_T("%0.4f"), m_dIntensity);
		SetDlgItemTextA(IDC_BEAMALIGNMAIN_EDIT_INTENSTY, strIntensity);
		strIntensity = "";

		SetDlgItemTextA(IDC_BEAMALIGNMAIN_EDIT_HORIZONTAL, g_pSqOneManual->m_strHorizontal);
		SetDlgItemTextA(IDC_BEAMALIGNMAIN_EDIT_VERTICAL, g_pSqOneManual->m_strVertical);
		SetDlgItemTextA(IDC_BEAMALIGNMAIN_EDIT_BEAM, g_pSqOneManual->m_strBeam);
		SetDlgItemTextA(IDC_BEAMALIGNMAIN_EDIT_PITCH, g_pSqOneManual->m_strPitch);
		SetDlgItemTextA(IDC_BEAMALIGNMAIN_EDIT_YAW, g_pSqOneManual->m_strYaw);
		SetDlgItemTextA(IDC_BEAMALIGNMAIN_EDIT_ROLL, g_pSqOneManual->m_strRoll);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_COMMUNICATION))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_EXCEEDSYSTEMRANGE))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_MOVECOMPLETE))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_BEAMALIGNMAIN_ICON_SYSTEMSTATUS))->SetIcon(m_LedIcon[0]);

		SetDlgItemTextA(IDC_BEAMALIGNMAIN_EDIT_HORIZONTAL, "0.0");
		SetDlgItemTextA(IDC_BEAMALIGNMAIN_EDIT_VERTICAL, "0.0");
		SetDlgItemTextA(IDC_BEAMALIGNMAIN_EDIT_BEAM, "0.0");
		SetDlgItemTextA(IDC_BEAMALIGNMAIN_EDIT_PITCH, "0.0");
		SetDlgItemTextA(IDC_BEAMALIGNMAIN_EDIT_YAW, "0.0");
		SetDlgItemTextA(IDC_BEAMALIGNMAIN_EDIT_ROLL, "0.0");
	}	
	if (g_pBeamAutoAlign->m_isAutoAlignOn == TRUE || g_pBeamSearch2D->m_bisScan == TRUE || g_pSqOneControl->m_isMoving == TRUE)
	{
		GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR1)->EnableWindow(FALSE);
		GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR2)->EnableWindow(FALSE);
		GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR3)->EnableWindow(FALSE);
		GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR4)->EnableWindow(FALSE);
		g_pBeamAutoAlign->GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(FALSE);
		g_pBeamSearch2D->GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CHECKPARAMETER)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR1)->EnableWindow(TRUE);
		GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR2)->EnableWindow(TRUE);
		GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR3)->EnableWindow(FALSE);
		GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR4)->EnableWindow(FALSE);
		g_pBeamAutoAlign->GetDlgItem(IDC_BEAMALIGN_BUTTON_RUN)->EnableWindow(TRUE);
		g_pBeamSearch2D->GetDlgItem(IDC_BEAMSEARCH2D_BUTTON_CHECKPARAMETER)->EnableWindow(TRUE);
	}
}


void CBeamAlignMainDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch (nIDEvent)
	{
	case COMMUNICATIONCHECK:
		Tcip_EthernetCom_Check();
		break;
	default:
		break;
	}
	__super::OnTimer(nIDEvent);

}


void CBeamAlignMainDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 __super::OnPaint()을(를) 호출하지 마십시오.
}

void CBeamAlignMainDlg::OnBnClickedButtonSqoneConnect()
{
	Display(0, "Open Communication Button Clicked");
	if (g_pSqOneControl->m_bConnected == FALSE)
	{
		g_pSqOneControl->connectSQone();
	}
}


void CBeamAlignMainDlg::OnBnClickedButtonSqoneDisconnect()
{
	Display(0, "Close Communication Button Clicked");
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		GetDlgItem(IDC_BEAMALIGNMAIN_BUTTON_DISCONNECT)->EnableWindow(FALSE);
		g_pSqOneControl->disconnectSQone();
	}
	GetDlgItem(IDC_BEAMALIGNMAIN_BUTTON_DISCONNECT)->EnableWindow(TRUE);
}



void CBeamAlignMainDlg::OnDestroy()
{
	__super::OnDestroy();
	KillTimer(COMMUNICATIONCHECK);
}


void CBeamAlignMainDlg::OnBnClickedButtonSqoneStop()
{
	Display(0, "Stop Button Clicked");

	g_pSqOneControl->m_bStopFlag = TRUE;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		g_pSqOneControl->sendStopCommand();
	}
}

void CBeamAlignMainDlg::setOptimizationInitial()
{
}


void CBeamAlignMainDlg::caculateBeamReflectVec(double *pu, double *bu, double *ReflectionUnitVector)
{

	ReflectionUnitVector[0] = 2 * (pu[0] * -bu[0] + pu[1] * -bu[1] + pu[2] * -bu[2])*pu[0] + bu[0];
	ReflectionUnitVector[1] = 2 * (pu[0] * -bu[0] + pu[1] * -bu[1] + pu[2] * -bu[2])*pu[1] + bu[1];
	ReflectionUnitVector[2] = 2 * (pu[0] * -bu[0] + pu[1] * -bu[1] + pu[2] * -bu[2])*pu[2] + bu[2];
}

void CBeamAlignMainDlg::caculateBeamReflectPos(double *pp, double *pu, double *bp, double *bu, double *dTangentPoint)
{
	double ft = 0;
	ft = ((pp[0] - bp[0])*pu[0] + (pp[1] - bp[1])*pu[1] + (pp[2] - bp[2])*pu[2]) / (pu[0] * bu[0] + pu[1] * bu[1] + pu[2] * bu[2]);
	dTangentPoint[0] = bp[0] + ft * bu[0];
	dTangentPoint[1] = bp[1] + ft * bu[1];
	dTangentPoint[2] = bp[2] + ft * bu[2];
}


void CBeamAlignMainDlg::caculateBeamIntensity(double *Lb0, double *b0u, double *intensity)
{
	double m1ll = 0, m2ll = 0, m3ll = 0, dell = 0, ph1ll = 0, ph2ll = 0, ph3ll = 0, result = 0.0;
	double *pht1t, *pht2t, *pht3t, *Lb1, *b1u, *Lb2, *b2u, *Lb3, *b3u, *Lb4;

	pht1t = new double[3];
	pht2t = new double[3];
	pht3t = new double[3];
	b1u = new double[3];
	b2u = new double[3];
	b3u = new double[3];
	Lb1 = new double[3];
	Lb2 = new double[3];
	Lb3 = new double[3];
	Lb4 = new double[3];
	//	Optic specification/////////////////////////////////////////////
	double lengthm12 = 98;
	double lengthm2zp = 198;

	// 1 st mirror
	double Pm1[3] = { 200, 0, 0 };
	double m1theta = 50;
	double m1rad = m1theta * PI / 180;
	double m1u[3] = { -sin(m1rad), 0, cos(m1rad) };
	int m1l = 10;

	// 2 nd mirror
	double Pm2[3] = { 182.9825,   0,   96.5112 };
	double m2theta = 188;
	double m2rad = m2theta * PI / 180;
	double m2u[3] = { -sin(m2rad),  0, cos(m2rad) };
	int m2l = 10;

	// mask
	double Pm3[3] = { 203.6791 ,        0, -100.4042 };
	double m3theta = 0;
	double m3rad = (m3theta)* PI / 180;
	double m3u[3] = { -sin(m3rad), 0, cos(m3rad) };
	int m3l = 5;

	// Detector
	double Pd[3] = { 212.1285 ,        0, -20.0135 };
	double dtheta = 180;
	double drad = (dtheta)* PI / 180;
	double du[3] = { 0.104528463267654, 0,  0.994521895368273 };
	int dl = 5;

	// Pinhole position
	double ph1[3] = { 141, 0, 0 };
	double ph2[3] = { 160, 0 ,0 };
	double ph3[3] = { 192.2, 0, 0 };
	double phu[3] = { -1, 0 ,0 };
	double phr[3] = { 1.7, 1.7, 3.6 };
	////////////////////////////////////////////////////////////////
	double d = 0;
	/////////////////////////////////////////////////////////////
	caculateBeamReflectPos(ph1, phu, Lb0, b0u, pht1t);
	caculateBeamReflectPos(ph2, phu, Lb0, b0u, pht2t);
	caculateBeamReflectPos(ph2, phu, Lb0, b0u, pht3t);

	caculateBeamReflectPos(Pm1, m1u, Lb0, b0u, Lb1);
	caculateBeamReflectVec(m1u, b0u, b1u);
	caculateBeamReflectPos(Pm2, m2u, Lb1, b1u, Lb2);
	caculateBeamReflectVec(m2u, b1u, b2u);
	caculateBeamReflectPos(Pm3, m3u, Lb2, b2u, Lb3);
	caculateBeamReflectVec(m3u, b2u, b3u);
	caculateBeamReflectPos(Pd, du, Lb3, b3u, Lb4);

	m1ll = sqrt((Lb1[0] - Pm1[0])*(Lb1[0] - Pm1[0]) + (Lb1[1] - Pm1[1])*(Lb1[1] - Pm1[1]) + (Lb1[2] - Pm1[2]) *(Lb1[2] - Pm1[2]));
	m2ll = sqrt((Lb2[0] - Pm2[0])*(Lb2[0] - Pm2[0]) + (Lb2[1] - Pm2[1])*(Lb2[1] - Pm2[1]) + (Lb2[2] - Pm2[2])*(Lb2[2] - Pm2[2]));
	dell = sqrt((Lb4[0] - Pd[0])*(Lb4[0] - Pd[0]) + (Lb4[1] - Pd[1])*(Lb4[1] - Pd[1]) + (Lb4[2] - Pd[2])*(Lb4[2] - Pd[2]));
	ph1ll = sqrt((pht1t[0] - ph1[0])*(pht1t[0] - ph1[0]) + (pht1t[1] - ph1[1])*(pht1t[1] - ph1[1]) + (pht1t[2] - ph1[2])*(pht1t[2] - ph1[2]));
	ph2ll = sqrt((pht2t[0] - ph2[0])*(pht2t[0] - ph2[0]) + (pht2t[1] - ph2[1])*(pht2t[1] - ph2[1]) + (pht2t[2] - ph2[2])*(pht2t[2] - ph2[2]));
	ph3ll = sqrt((pht3t[0] - ph3[0])*(pht3t[0] - ph3[0]) + (pht3t[1] - ph3[1])*(pht3t[1] - ph3[1]) + (pht3t[2] - ph3[2])*(pht3t[2] - ph3[2]));


	if (m1ll < m1l && m2ll < m2l && ph1ll < phr[0] && ph1ll < phr[1] && ph1ll < phr[2] && dell < dl)
	{
		d = (-(dell)*(dell) / 25) + 1;
	}
	else
	{
		d = 0;
		//d = (-(dell)*(dell) / 25) + 1;
	}

	double b = sqrt(du[1] * du[1] + du[2] * du[2] + du[0] * du[0]);
	double c = sqrt(b3u[1] * b3u[1] + b3u[2] * b3u[2] + b3u[0] * b3u[0]);
	double a = abs((du[0] * -b3u[0] + du[1] * -b3u[1] + du[2] * -b3u[2]) / (sqrt(du[1] * du[1] + du[2] * du[2] + du[0] * du[0])*sqrt(b3u[1] * b3u[1] + b3u[2] * b3u[2] + b3u[0] * b3u[0])));
	a = pow(a, 100);
	a = pow(a, 10);
	a = pow(a, 100);
	a = pow(a, 100);
	result = d * a;
	//Intensity = d;


	delete[] pht1t;
	delete[] pht2t;
	delete[] pht3t;
	delete[] b1u;
	delete[] b2u;
	delete[] b3u;
	delete[] Lb1;
	delete[] Lb2;
	delete[] Lb3;
	delete[] Lb4;

	*intensity = result;

	return;
}


void CBeamAlignMainDlg::setBeamInitail()
{
	double Lb0[] = { 288, 0, 0 };
	double b0u[] = { 1, 0, 0 };
	//Lb00 = Lb0; %
//	d_BeamIniPara[1] = Lb0[2] - 2 + dis(gen);
//	d_BeamIniPara[2] = Lb0[3] - 2 + dis(gen);
		//	d_BeamIniPara[3] = d_BeamIniPara[3] - 0.0002 + (0.0004)*diss(gen); //yaw
	//	d_BeamIniPara[4] = d_BeamIniPara[4] - 0.007 + (0.014)*diss(gen); //pitch

	m_dBeamIniParaHorizontal = g_pSqOneConfig->m_dBeamInitialSetHorizontal;
	m_dBeamIniParaVertical = g_pSqOneConfig->m_dBeamInitialSetVertical;
	m_dBeamIniParaPitch = g_pSqOneConfig->m_dBeamInitialSetPitch / 1000;
	m_dBeamIniParaYaw = g_pSqOneConfig->m_dBeamInitialSetYaw / 1000;
	m_dBeamIniParaUx = cos(m_dBeamIniParaPitch)*cos(m_dBeamIniParaYaw);
	m_dBeamIniParaUy = cos(m_dBeamIniParaPitch)*-sin(m_dBeamIniParaYaw);
	m_dBeamIniParaUz = sin(m_dBeamIniParaPitch);
}

void CBeamAlignMainDlg::caculateIntensty()
{
	m_dBeamSetParaHorizontal = g_pSqOneControl->m_SQOneParseData.X;
	m_dBeamSetParaVertical = g_pSqOneControl->m_SQOneParseData.Y;
	m_dBeamSetParaPitch = g_pSqOneControl->m_SQOneParseData.Rx / 1000;
	m_dBeamSetParaYaw = g_pSqOneControl->m_SQOneParseData.Ry / 1000;

	double Lb0[3], b0u[3];
	b0u[0] = m_dBeamIniParaUx * (cos(m_dBeamSetParaPitch)* cos(m_dBeamSetParaYaw)) + m_dBeamIniParaUy * (sin(m_dBeamSetParaYaw)) + m_dBeamIniParaUz * (-sin(m_dBeamSetParaPitch)*cos(m_dBeamSetParaYaw));
	b0u[1] = m_dBeamIniParaUx * (cos(m_dBeamSetParaPitch)*-sin(m_dBeamSetParaYaw)) + m_dBeamIniParaUy * (cos(m_dBeamSetParaYaw)) + m_dBeamIniParaUz * ( sin(m_dBeamSetParaPitch)*sin(m_dBeamSetParaYaw));
	b0u[2] = m_dBeamIniParaUx * (sin(m_dBeamSetParaPitch)) + m_dBeamIniParaUz * (cos(m_dBeamSetParaPitch));
	Lb0[0] = 288;
	Lb0[1] = m_dBeamIniParaHorizontal + m_dBeamSetParaHorizontal;
	Lb0[2] = m_dBeamIniParaVertical + m_dBeamSetParaVertical;

	caculateBeamIntensity(Lb0, b0u, &m_dIntensity);
	
	return;
}

int CBeamAlignMainDlg::checkLimit(double dHorizontal, double dVertical, double dBeam, double dPitch, double dYaw, double dRoll)
{
	int nRet = 0;
	if (dHorizontal <Xleftlimit || dHorizontal >Xrightlimit)
	{
		nRet = -1;
		::AfxMessageBox("X parameter limit error");
	}
	else if (dVertical <Yleftlimit || dVertical >Yrightlimit)
	{
		nRet = -1;
		::AfxMessageBox("Y parameter limit error");
	}
	else if (dBeam <Zleftlimit || dBeam >Zrightlimit)
	{
		nRet = -1;
		::AfxMessageBox("Z parameter limit error");
	}
	else if (dPitch <Rxleftlimit || dPitch >Rxrightlimit)
	{
		nRet = -1;
		::AfxMessageBox("RX parameter limit error");
	}
	else if (dYaw <Ryleftlimit || dYaw >Ryrightlimit)
	{
		nRet = -1;
		::AfxMessageBox("RY parameter limit error");
	}
	else if (dRoll <Rzleftlimit || dRoll >Rzrightlimit)
	{
		nRet = -1;
		::AfxMessageBox("RZ parameter limit error");
	}
	else
	{
		nRet = 0;
	}
	return nRet;
}

BOOL CBeamAlignMainDlg::Display(int nModule, CString strLogMsg, BOOL bDisplay, BOOL bSaveFile)
{
	CString strOuptLog;
	CListBox* pBox = (CListBox*)GetDlgItem(IDC_BEAMALIGNMAIN_LIST_LOG);
	if (pBox == NULL)
	{
		return FALSE;
	}
	int numItem = pBox->GetCount();
	if (numItem >= 1000)
	{
		pBox->ResetContent();
	}

	if (bDisplay)
	{
		CString strDate, strTime;
		GetSystemDateTime(&strDate, &strTime);

		CString str;
		str.Format("%s %s\r\n", strTime, strLogMsg);

		pBox->SetCurSel(pBox->AddString(str));
		pBox->GetHorizontalExtent();
		pBox->SetHorizontalExtent(256);
	}

	if (bSaveFile)
	{
		SaveLogFile("BeamOptimizationLog", (LPSTR)(LPCTSTR)strLogMsg);
	}

	return TRUE;
}

void CBeamAlignMainDlg::OnBnClickedButtonBeammainTest()
{
#if FALSE
	//g_pBeamAutoAlign->GetDlgItem(IDC_BUTTON_BEAMAUTOALIGN_RUN)->EnableWindow(TRUE);
	//g_pBeam2DAutoAlign->GetDlgItem(IDC_EDIT_BEAMAUTOALIGN_DSIPRUNNUM)->EnableWindow(TRUE);
	//g_pBeam2DAutoAlign->GetDlgItem(IDC_EDIT_BEAMAUTOALIGN_DSIPRUNNUM)->SendMessage(EM_SETREADONLY, 0, 0);
	int nRet = 0;
	int tempRet = 0 ;
	g_pXrayCamera->getMeanValue(m_dIntensity);
	if (tempRet != 0)
	{
		if (tempRet == -1)
		{
			nRet = -6;
		}
		else
		{
			nRet = -5;
		}
	}
#endif
}


BOOL CBeamAlignMainDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}
	return __super::PreTranslateMessage(pMsg);
}

void CBeamAlignMainDlg::resetDataset()
{
	for (int i = 0; i < GatherNum; i++)
	{
		m_chartx[i] = Chart::NoValue;
		m_charty[i] = Chart::NoValue;
		m_chartz[i] = Chart::NoValue;
		m_d2DScanIntensity[i] = Chart::NoValue;
		m_d2DScanpPrameter1[i] = Chart::NoValue;
		m_d2DScanpPrameter2[i] = Chart::NoValue;
		m_dCurcleScanMaxIntensity[i] = Chart::NoValue;
		m_dCurcleScanMaxParameter1[i] = Chart::NoValue;
		m_dCurcleScanMaxParameter2[i] = Chart::NoValue;
	}
}


void CBeamAlignMainDlg::OnBnClickedCheckBeammainDetector1()
{
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR1))->SetCheck(TRUE);
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR2))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR3))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR4))->SetCheck(FALSE);
	m_nDetectorIndex = 1;
}


void CBeamAlignMainDlg::OnBnClickedCheckBeammainDetector2()
{
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR1))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR2))->SetCheck(TRUE);
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR3))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR4))->SetCheck(FALSE);
	m_nDetectorIndex = 2;
}


void CBeamAlignMainDlg::OnBnClickedCheckBeammainDetector3()
{
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR1))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR2))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR3))->SetCheck(TRUE);
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR4))->SetCheck(FALSE);
	m_nDetectorIndex = 3;
}


void CBeamAlignMainDlg::OnBnClickedCheckBeammainDetector4()
{
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR1))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR2))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR3))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_BEAMALIGNMAIN_CHECK_DETECTOR4))->SetCheck(TRUE);
	m_nDetectorIndex = 4;
}


BOOL CBeamAlignMainDlg::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	//KillTimer(1);

	return __super::DestroyWindow();
}

void CBeamAlignMainDlg::getIntensityfromADAM()
{
	g_pEUVSource->SetMechShutterOpen(TRUE);
	int test = g_pAdam->Command_MPacketRunTimeout(1000);
	g_pEUVSource->SetMechShutterOpen(FALSE);
	m_dIntensity = g_pAdam->AdamData.m_dDetector1;
}

int CBeamAlignMainDlg::getMeanValuefromXrayCam()
{
	return -1;
#if FALSE
	if (g_pXrayCamera->m_hDevice == NULL)
	{
		//AfxMessageBox(_T("Device not connected."));
		return -1;
	}

	int grabResult = g_pXrayCamera->GrabTwice(TRUE);
	if (grabResult == GRAB_OK)
	{
		//CString strTemp = "";
		//g_pXrayCamera->GetDlgItemTextA(IDC_EDIT_IMAGE_STATISTICS,strTemp);
		//
		//vector<CString> vecRecvData;
		//CString strText = _T("");
		//
		//int nIdx = 0;
		//while (FALSE != AfxExtractSubString(strText, strTemp, nIdx++, _T(' ')))
		//{
		//	vecRecvData.push_back(strText);
		//}
		//m_dIntensity = _ttof(vecRecvData[0].Right(vecRecvData[0].GetLength() - 5));
		g_pXrayCamera->getMeanValue(m_dIntensity);
	}

	return grabResult;
#endif
}

int CBeamAlignMainDlg::executeMeasureformPhase()
{
	m_dIntensity = 0;
	//g_pPhase->OnBnClickedButtonPhaseMeasure();
	// ihlee 지연 추가
	WaitSec(1);
	//g_pPhase->OnBnClickedButtonBgUpdateMeasure();
	
	return 0;
}


void CBeamAlignMainDlg::OnBnClickedCheckBeammainSimulator()
{
	if (IsDlgButtonChecked(IDC_BEAMALIGNMAIN_CHECK_SIMULATOR) == BST_CHECKED)
	{
		g_pBeamConfig->changeConfig(TRUE);
	}
	else
	{
		g_pBeamConfig->changeConfig(FALSE);
	}
}
