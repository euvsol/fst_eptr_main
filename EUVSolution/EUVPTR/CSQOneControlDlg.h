﻿#pragma once


// CSQOneControlDlg 대화 상자

class CSQOneControlDlg : public CDialogEx, public CSqareOneStageControl
{
	DECLARE_DYNAMIC(CSQOneControlDlg)

public:
	CSQOneControlDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CSQOneControlDlg();

	// 대화 상자 데이터입니다.
	//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SQONEMAIN_DIALOG};
	//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	HICON m_LedIcon[3];
	HBITMAP m_SqoneStage;

	DECLARE_MESSAGE_MAP()

public:
	bool m_isThreadExitFlag;
	void connectSQone();
	void disconnectSQone();

	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();

	afx_msg void OnBnClickedButtonSqoneConnect();
	afx_msg void OnBnClickedButtonSqoneDisconnect();
	afx_msg void OnBnClickedButtonSqoneStop();
	afx_msg void OnBnClickedButtonSqoneShowmanualdlg();
	afx_msg void OnBnClickedButtonSqoneShowconfigdlg();

	CWinThread *m_pThread;
	static UINT updataUIThread(LPVOID lParam);
	   
	int checkLimit(double dHorizontal, double dVertical, double dBeam, double dPitch, double dYaw, double dRoll);
	void updateSQOneDlg();
	BOOL Display(int nModule = 0, CString strLogMsg = _T(""), BOOL bDisplay = TRUE, BOOL bSaveFile = TRUE);

	virtual BOOL DestroyWindow();
	int moveSQ1(double dHorizontal, double dVertical, double dBeam, double dPitch, double dYaw, double dRoll);
	afx_msg void OnBnClickedButtonSqoneShowpositiondata();

	void waitSleep(int msec);
};
