﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CVacuumGaugeDlg 대화 상자

IMPLEMENT_DYNAMIC(CVacuumGaugeDlg, CDialogEx)

CVacuumGaugeDlg::CVacuumGaugeDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_VACUUM_GAUGE_DIALOG, pParent)
{
	m_Thread = NULL;
	m_bThreadExitFlag = FALSE;
	m_nGaugeState = IDLE;
}

CVacuumGaugeDlg::~CVacuumGaugeDlg()
{
	m_bThreadExitFlag = TRUE;

	if (m_Thread != NULL)
	{
		HANDLE threadHandle = m_Thread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_Thread = NULL;
	}
}

void CVacuumGaugeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CVacuumGaugeDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON1, &CVacuumGaugeDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON4, &CVacuumGaugeDlg::OnBnClickedButton4)
END_MESSAGE_MAP()


// CVacuumGaugeDlg 메시지 처리기


BOOL CVacuumGaugeDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CVacuumGaugeDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32,32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_LLC_GAUGE_STATE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_MC_GAUGE_STATE))->SetIcon(m_LedIcon[0]);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CVacuumGaugeDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	KillTimer(GAUGE_UPDATE_TIMER);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

int CVacuumGaugeDlg::OpenDevice()
{
	int nRet = OpenSerialPort(g_pConfig->m_chPORT[SERIAL_VACUUMGAUGE],
								g_pConfig->m_nBAUD_RATE[SERIAL_VACUUMGAUGE],
								g_pConfig->m_nUSE_BIT[SERIAL_VACUUMGAUGE],
								g_pConfig->m_nSTOP_BIT[SERIAL_VACUUMGAUGE],
								g_pConfig->m_nPARITY[SERIAL_VACUUMGAUGE]);

	if (nRet != 0) {
		((CStatic*)GetDlgItem(IDC_ICON_LLC_GAUGE_STATE))->SetIcon(m_LedIcon[2]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_GAUGE_STATE))->SetIcon(m_LedIcon[2]);
		SetDlgItemText(IDC_LLC_GAUGE_VIEWER, _T("Connection Fail"));
		SetDlgItemText(IDC_MC_GAUGE_VIEWER, _T("Connection Fail"));
		m_nGaugeState = NotConnected;
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_GAUGE_STATE))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_GAUGE_STATE))->SetIcon(m_LedIcon[1]);

		m_Thread = AfxBeginThread(UpdataThread, (LPVOID)this, THREAD_PRIORITY_NORMAL, 0, 0);
		
		SetTimer(GAUGE_UPDATE_TIMER, 500, NULL);
	}

	return nRet;
}


void CVacuumGaugeDlg::UpdateVacuumData()
{
	CString str;

	str.Format(_T("%.2e Torr"), m_dPressureLLC);
	SetDlgItemText(IDC_LLC_GAUGE_VIEWER, str);
	str.Format(_T("%.2e Torr"), m_dPressureMC);
	SetDlgItemText(IDC_MC_GAUGE_VIEWER, str);

	//ch3 = ch_3; //MC ---> MKS GAUGE ADDRESS 3
	//ch2 = ch_2; //LLC --> MKS GAUGE ADDRESS 2

	//ch3.Format(_T("%s"), ch_3);
	//ch2.Format(_T("%s"), ch_2);

	//m_dPressure_LLC = atof(ch_2);
	//m_dPressure_MC = atof(ch_3);

	if (m_bLlcErrorState != TRUE && m_bMcErrorState != TRUE)
		m_nGaugeState = Running;
	else
		m_nGaugeState = Error;
}

void CVacuumGaugeDlg::RequestData()
{
	if (m_bSerialConnected == TRUE)
	{
		//ch3 = ch_3; //MC ---> MKS GAUGE ADDRESS 3
		//ch2 = ch_2; //LLC --> MKS GAUGE ADDRESS 2

		ReadMcVacuumRate();
		Sleep(20);
		ReadMcGaugeState();
		Sleep(20);

		ReadLlcVacuumRate();
		Sleep(20);
		ReadLlcGaugeState();
		Sleep(20);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_LLC_GAUGE_STATE))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_MC_GAUGE_STATE))->SetIcon(m_LedIcon[0]);

		m_bThreadExitFlag = TRUE;
	}
}


UINT CVacuumGaugeDlg::UpdataThread(LPVOID pParam)
{
	CVacuumGaugeDlg*  runthread = (CVacuumGaugeDlg*)pParam;

	while (!runthread->m_bThreadExitFlag)
	{
		runthread->RequestData();
		Sleep(100);
	}

	return 0;
}

//int CVacuumGaugeDlg::GetStatus()
//{
//	int ret = GAUGE_ERROR;
//	switch (Vaccum_Gauge_State)
//	{
//	case GAUGE_ERROR :
//		ret = GAUGE_ERROR;
//		break;
//	case GAUGE_OFFLINE:
//		ret =  GAUGE_OFFLINE;
//		break;
//	case GAUGE_NORMAL :
//		ret = GAUGE_NORMAL;
//		break;
//	case GAUGE_UPDATE_TIMER:
//		ret = GAUGE_UPDATE_TIMER;
//		break;
//	default:
//		break;
//	}
//	return ret;
//}

void CVacuumGaugeDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == GAUGE_UPDATE_TIMER)
	{
		if (!m_bThreadExitFlag)
		{
			UpdateVacuumData();
			SetTimer(GAUGE_UPDATE_TIMER, 300, NULL);
		}
	}

	__super::OnTimer(nIDEvent);
}


void CVacuumGaugeDlg::OnBnClickedButton1()
{
	int ret = 1;
	char* str_ch1;

	CPasswordDlg pwdlg(this);
	pwdlg.DoModal();
	CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)(" LLC Carlibrate 를 진행 하시겠습니까 ? (대기상태확인 760Torr에서 진행요망) ")));	
	//if (pwdlg.m_strTxt != "srem")
	if (pwdlg.m_strTxt != "1234")
	{
		CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)(" Password 가 일치 하지 않습니다 ")));	//통신 상태 기록.
		::AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
		return;
	}
	else if (pwdlg.m_strTxt == "1234")
	{
		if (m_bSerialConnected == TRUE)
		{
			CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("LLC Carlibrate 를 진행 합니다.")));	//통신 상태 기록.
			::AfxMessageBox("Carlibrate를 진행 합니다.", MB_ICONINFORMATION);
			if (g_pIO->Is_LLC_Atm_Check())
			{
				CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("ATM Sensor 확인 완료")));	//통신 상태 기록
				if (m_dPressureLLC > 755)
				{
					CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("진공값 755 이상 확인 완료")));	//통신 상태 기록
					CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("LLC Carlibrate 시작")));	//통신 상태 기록
					str_ch1 = "#02TS\r";
					ret = CMKS390Gauge::SendData((LPSTR)(LPCTSTR)str_ch1, 500);
					Sleep(10);
					if (!ret)
					{
						CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("LLC Carlibrate 완료")));	//통신 상태 기록
						::AfxMessageBox("LLC Carlibrate 완료", MB_ICONINFORMATION);
					}
					else
					{
						CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("LLC Carlibrate 실패")));	//통신 상태 기록
						::AfxMessageBox("LLC Carlibrate 실패", MB_ICONINFORMATION);
					}
				}
				else
				{
					CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("LLC 진공값 755 이하 이므로 Carlibrate 진행 불가")));	//통신 상태 기록
					::AfxMessageBox("LLC 진공값 755 이하 이므로 Carlibrate 진행 불가.", MB_ICONINFORMATION);
				}
			}
			else
			{
				CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("LLC ATM Sensor 감지 Error ( 진공상태 확인요망 ) Carlibrate 진행 불가")));	//통신 상태 기록.
				::AfxMessageBox("LLC ATM Sensor 감지 Error ( 진공상태 확인요망 ) Carlibrate 진행 불가", MB_ICONINFORMATION);
			}
		}
		else
		{
			CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("Gauge Port 연결 에러발생! 연결 확인 요망")));	//통신 상태 기록.
			::AfxMessageBox("Gauge Port 연결 에러발생! 연결 확인 요망", MB_ICONINFORMATION);
		}
	}
}


void CVacuumGaugeDlg::OnBnClickedButton4()
{
	int ret = 1;
	char* str_ch1;

	CPasswordDlg pwdlg(this);
	pwdlg.DoModal();
	CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)(" MC Carlibrate 를 진행 하시겠습니까 ? (대기상태확인 760Torr에서 진행요망) ")));
	//if (pwdlg.m_strTxt != "srem")
	if (pwdlg.m_strTxt != "1234")
	{
		CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)(" Password 가 일치 하지 않습니다 ")));	//통신 상태 기록.
		::AfxMessageBox("Password가 일치 하지 않습니다.", MB_ICONSTOP);
		return;
	}
	else if (pwdlg.m_strTxt == "1234")
	{
		if (m_bSerialConnected == TRUE)
		{
			CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("MC Carlibrate 를 진행 합니다.")));	//통신 상태 기록.
			::AfxMessageBox("Carlibrate를 진행 합니다.", MB_ICONINFORMATION);
			if (g_pIO->Is_MC_Atm_Check())
			{
				CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("ATM Sensor 확인 완료")));	//통신 상태 기록
				if (m_dPressureMC > 755)
				{
					CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("진공값 755 이상 확인 완료")));	//통신 상태 기록
					CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("MC Carlibrate 시작")));	//통신 상태 기록
					str_ch1 = "#03TS\r";
					ret = CMKS390Gauge::SendData((LPSTR)(LPCTSTR)str_ch1, 500);
					Sleep(10);
					if (!ret)
					{
						CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("MC Carlibrate 완료")));	//통신 상태 기록
						::AfxMessageBox("MC Carlibrate 완료", MB_ICONINFORMATION);
					}
					else
					{
						CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("MC Carlibrate 실패")));	//통신 상태 기록
						::AfxMessageBox("MC Carlibrate 실패", MB_ICONINFORMATION);
					}
				}
				else
				{
					CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("MC 진공값 755 이하 이므로 Carlibrate 진행 불가")));	//통신 상태 기록
					::AfxMessageBox("MC 진공값 755 이하 이므로 Carlibrate 진행 불가.", MB_ICONINFORMATION);
				}
			}
			else
			{
				CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("MC ATM Sensor 감지 Error ( 진공상태 확인요망 ) Carlibrate 진행 불가")));	//통신 상태 기록.
				::AfxMessageBox("MC ATM Sensor 감지 Error ( 진공상태 확인요망 ) Carlibrate 진행 불가", MB_ICONINFORMATION);
			}
		}
		else
		{
			CECommon::SaveLogFile("Vacuum Gague Event", _T((LPSTR)(LPCTSTR)("Gauge Port 연결 에러발생! 연결 확인 요망")));	//통신 상태 기록.
			::AfxMessageBox("Gauge Port 연결 에러발생! 연결 확인 요망", MB_ICONINFORMATION);
		}
	}
}
