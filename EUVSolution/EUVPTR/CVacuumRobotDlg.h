﻿/**
 * Vacuum Robot Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once

// CVacuumRobotDlg 대화 상자

class CVacuumRobotDlg : public CDialogEx , public CRaonVactraVMTRCtrl , public IObserver
{
	DECLARE_DYNAMIC(CVacuumRobotDlg)

public:
	CVacuumRobotDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CVacuumRobotDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_VACUUM_ROBOT_DIALOG };
//#endif

	typedef enum {
		HOME,
		READY,
		LLC_PICK,
		LLC_PLACE,
		MC_PICK,
		MC_PLACE,
		GET_SPEED,
		SET_SPEED,
		LLC_PICK_READY,
		LLC_PICK_EXTEND,
		LLC_PICK_RETRACT,
		LLC_PLACE_READY,
		LLC_PLACE_EXTEND,
		LLC_PLACE_RETRACT,
		MC_PICK_READY,
		MC_PICK_EXTEND,
		MC_PICK_RETRACT,
		MC_PLACE_READY,
		MC_PLACE_EXTEND,
		MC_PLACE_RETRACT
	} VmtrCommandState;

	VmtrCommandState VmtrState;

protected:
	HICON			m_LedIcon[3];
	CEdit			m_ZPosCtrl;
	CEdit			m_TPosCtrl;
	CEdit			m_LPosCtrl;
	CEdit			m_ERRCodeCtrl;
	CWinThread*		m_pVmtrThread;

	void			GetDeviceStatus();
	void			InitializeControls();
	
	static	UINT VRobotCommandThread(LPVOID pClass);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()
public:

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();

	int OpenDevice();

	afx_msg void OnDestroy();
	afx_msg void OnBnClickedVrobotHomeButton();
	afx_msg void OnBnClickedVrobotReadyButton();
	afx_msg void OnBnClickedVrobotStopButton();
	afx_msg void OnBnClickedVrobotResetButton();
	afx_msg void OnBnClickedVrobotPauseCheck();
	afx_msg void OnBnClickedVrobotSetspeedButton();
	afx_msg void OnBnClickedPickLlkButton();
	afx_msg void OnBnClickedPlaceLlkButton();
	afx_msg void OnBnClickedPickMcButton();
	afx_msg void OnBnClickedPlaceMcButton();
	afx_msg void OnBnClickedExpickLlkButton();
	afx_msg void OnBnClickedExplaceLlkButton();
	afx_msg void OnBnClickedRepickLlkButton();
	afx_msg void OnBnClickedReplaceLlkButton();
	afx_msg void OnBnClickedGotog1LlkButton();
	afx_msg void OnBnClickedGotop1LlkButton();
	afx_msg void OnBnClickedExpickMcButton();
	afx_msg void OnBnClickedExplaceMcButton();
	afx_msg void OnBnClickedRepickMcButton();
	afx_msg void OnBnClickedReplaceMcButton();
	afx_msg void OnBnClickedGotog1McButton();
	afx_msg void OnBnClickedGotop1McButton();
	afx_msg void OnBnClickedVrobotGetspeedButton();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	BOOL	Is_VMTR_Connected()	{ return m_bConnected; }
	BOOL	Is_VMTR_ServoOn()	{ return m_bServoMotorOn; }
	BOOL	Is_VMTR_Home()		{ return m_bHomeComplete; }
	BOOL	Is_VMTR_Working()	{ return m_bRobotWorking; }
	BOOL	Is_VMTR_Error()		{ return m_bOccurredErr; }
	BOOL	Is_VMTR_EMO()		{ return m_bEMOStatus; }

	int		Transfer_Mask(int nSource, int nTarget);
	int		MovePickReadyPos(int nTarget);
	int		MovePlaceReadyPos(int nTarget);
	void	EmergencyStop();

	double	Get_Feedback_PosL()	{ return m_dPositionL; }
	double	Get_Feedback_PosT()	{ return m_dPositionT; }
	double	Get_Feedback_PosZ()	{ return m_dPositionZ; }
	
	afx_msg void OnBnClickedVrobotSrvOnButton();
	afx_msg void OnBnClickedVrobotSrvOffButton();


	void RobotStop();
};
