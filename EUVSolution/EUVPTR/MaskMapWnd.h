/**
 * Mask Map Wnd Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once

#include	"ProcessData.h"
#include	"CMacro.h"

#define	MaskSize_mm		152.4

/////////////////////////////////////////////////////////////////////////////
// CMaskMapWnd window

class CMaskMapWnd : public CWnd
{
// Construction
public:
	CMaskMapWnd();
	virtual ~CMaskMapWnd();

	CProcessData  m_ProcessData;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMaskMapWnd)
	public:
	virtual BOOL Create(DWORD dwStyle,RECT& rect, CWnd* pParentWnd);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CMaskMapWnd)
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Implementation
public:
	CRect rcMaskMap_pixel;

	int m_nPointSize;
	int m_nMicroscopyType;
	int m_mSelectedMeasureID;

	int m_nCurrentCouponIndex;
	int m_nCurrentPointIndex;
	int m_nCurrentScanIndex;

	RECT *m_pMeasurePointRect;

	BOOL m_bDrawAlignPoint;
	BOOL m_bDrawMeasurePoint;
	BOOL m_bDrawSelectedIDRect;

	void DrawMaskBackground(CDC *pDC);
	void DrawMaskAlignPoint(CDC *pDC);
	void DrawDie(CDC *pDC);
	void DrawPTRMeasurePoint(CDC *pDC);
	void DrawBeamDiagnosisPoint(CDC *pDC);
	void DrawNavigationStagePos(CDC *pDC);
	void DrawPTRSelectedIDRect(int i, CDC *pDC);
	void DrawPTRCouponJig(CDC *pDC);

	MIL_ID  m_MilMapDisplay;

	CPoint startpt,endpt;
	POINT selcalibpos;
	bool bOnCalib;

	int curviewposy;
	int curviewposx;

	int m_Gap;

	int m_nTextSize;
	BOOL m_bShowcode;
	BOOL m_bSelZoomCheck;
	
	double m_realcenterx,m_realcentery;

	int LenTwoPoint(int x1, int y1, int x2, int y2);
	double PixeltoMicrometer(int pixel, int value);
	double MicrometertoPixel(int pixel, double value);
	void ConvertToShortRainbowColor(int nMilSource, int nMilTarget);

	clock_t	m_ctmonitoring_time, m_ctsource_off_time;	//일정 시간 Stage 움직임이 없으면 Source 자동 Off하는 기능 추가
};
