/**
 * Camera View Wnd Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */

#pragma once
class CCameraWnd : public CWnd
{
public:
	CCameraWnd();
	~CCameraWnd();

	CRect rcClient;

	MIL_ID m_MilImage;
	MIL_ID m_MilOMDisplay;

	MIL_ID m_MilGrayImage;
	MIL_ID m_MilScaleImage;

	MIL_INT m_nDigSizeX;
	MIL_INT m_nDigSizeY;
	MIL_INT m_nDigBand;

	MIL_INT m_nCurrScaleMin;
	MIL_INT m_nCurrScaleMax;

	MIL_INT m_nSetScaleMin;
	MIL_INT m_nSetScaleMax;

	BOOL m_bExcuteOnce;


	MIL_ID MilExtremeResult;
	MIL_ID Lut;

	/** 현재 보이는 OM 화면 상에서 마우스 Point 위치 */
	POINT m_ptMousePoint;
	POINT m_ptLeftButtonDownPoint;

	BOOL m_bCrossLine;
	BOOL m_bMouseMove;
	BOOL m_bMouseMoveLine;

	BOOL m_bScaleImage;

	CRectTracker m_RectTrackerOM;

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CMaskMapWnd)
	virtual BOOL Create(DWORD dwStyle, RECT& rect, CWnd* pParentWnd);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);



	afx_msg void OnDestroy();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
};

