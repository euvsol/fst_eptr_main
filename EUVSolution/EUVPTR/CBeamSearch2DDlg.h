﻿#pragma once


// CSqOneScanDlg 대화 상자

class CBeamSearch2DDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CBeamSearch2DDlg)

public:
	CBeamSearch2DDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CBeamSearch2DDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_BEAMSEARCH2D_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	bool m_bisScan;
	int m_nScanNumber;
	int m_nDirectionIndex;

	int checkScanSet();
	int runSearch();
	int runCirculScan();

	CComboBox m_combobox_Sqscan_parameter1;
	CComboBox m_combobox_Sqscan_parameter2;
	
	afx_msg void OnBnClickedButtonSqscanCheckparameter();
	afx_msg void OnBnClickedButtonSqscanCancel();
	afx_msg void OnBnClickedButtonSqscanRunsearch();
	afx_msg void OnCbnSelchangeComboSqscanParameter2();
	afx_msg void OnCbnSelchangeComboSqscanParameter1();
	virtual BOOL OnInitDialog();

	int checkScanParameter(CString str1LeftLimit, CString str1RightLimit, CString str1Iteration, CString str2LeftLimit, CString str2RightLimit, CString str2Iteration);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	CChartViewer m_XYchartViewer;
	XYChart *ptrXYChart;
	void drawXYChart(CChartViewer *viewer);

	void OnViewPortChangedChart();

	CWinThread *m_pThread;
	static UINT runSCAN(LPVOID lParam);
	static UINT runMeasure(LPVOID lParam);
	afx_msg void OnBnClickedBeamsearch2dButtonRunmeasure();
	int runSearchMeasure();
};
