﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CEUVSourceDlg 대화 상자

IMPLEMENT_DYNAMIC(CEUVSourceDlg, CDialogEx)

CEUVSourceDlg::CEUVSourceDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_EUVSOURCE_DIALOG, pParent)
{
	m_bThreadExitFlag		 = FALSE;
	m_bConnectThreadExitFlag = FALSE;
	m_pStatusThread			 = NULL;
	m_pConnectThread		 = NULL;
	m_bChecked				 = FALSE;
	m_bPreviousMonitoringMode = FALSE;

	g_pDevMgr->RegisterObserver(this);
}

CEUVSourceDlg::~CEUVSourceDlg()
{
	m_bConnectThreadExitFlag = TRUE;

	if (m_pConnectThread != NULL)
	{
		HANDLE threadHandle = m_pConnectThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pConnectThread = NULL;
	}

	m_bThreadExitFlag = TRUE;

	if (m_pStatusThread != NULL)
	{
		HANDLE threadHandle = m_pStatusThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pStatusThread = NULL;
	}
}

void CEUVSourceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_MAIN_VACUUM, m_MainVacuumCtrl);
	DDX_Control(pDX, IDC_STATIC_SUB_VACUUM, m_SubVacuumCtrl);
	DDX_Control(pDX, IDC_STATIC_BUF_VACUUM, m_BufVacuumCtrl);
	DDX_Control(pDX, IDC_STATIC_MFC_NEON_GAUGE, m_MfcNeonCtrl);
	DDX_Control(pDX, IDC_STATIC_NEON_GAS_GAUGE, m_NeonGasCtrl);
	DDX_Control(pDX, IDC_STATIC_FORELINE_GAUGE, m_ForelineCtrl);
}


BEGIN_MESSAGE_MAP(CEUVSourceDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_SRC_VACUUM_START_BUTTON, &CEUVSourceDlg::OnBnClickedSrcVacuumStartButton)
	ON_BN_CLICKED(IDC_SRC_VACUUM_STOP_BUTTON2, &CEUVSourceDlg::OnBnClickedSrcVacuumStopButton2)
	ON_BN_CLICKED(IDC_SRC_EUV_START_BUTTON, &CEUVSourceDlg::OnBnClickedSrcEuvStartButton)
	ON_BN_CLICKED(IDC_SRC_EUV_STOP_BUTTON, &CEUVSourceDlg::OnBnClickedSrcEuvStopButton)
	ON_BN_CLICKED(IDC_SRC_SHUTTER_OPEN_BUTTON, &CEUVSourceDlg::OnBnClickedSrcShutterOpenButton)
	ON_BN_CLICKED(IDC_SRC_SHUTTER_CLOSE_BUTTON, &CEUVSourceDlg::OnBnClickedSrcShutterCloseButton)
END_MESSAGE_MAP()


// CEUVSourceDlg 메시지 처리기


BOOL CEUVSourceDlg::OnInitDialog()
{
	__super::OnInitDialog();

	InitializeControls();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CEUVSourceDlg::InitializeControls()
{
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_SOURCE_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SOURCE_EUV_ON))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SOURCE_EUV_OFF))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SRC_LASER_SHUTTER_OPEN))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SRC_LASER_SHUTTER_CLOSE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SRC_SHUTTER_OPEN))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SRC_SHUTTER_CLOSE))->SetIcon(m_LedIcon[0]);	
}

int CEUVSourceDlg::OpenDevice()
{
	int nRet = 0;

	nRet = OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_SRC_PC], g_pConfig->m_nPORT[ETHERNET_SRC_PC], FALSE);

	if (nRet == 0)
	{
		SRC_Status(); Sleep(100);

		if (m_bEuvOnState == FALSE)
		{
			CString log;
			log = _T("EUV OFF");
			SaveLogFile("EuvUsage", (LPSTR)(LPCTSTR)log);
		}

		m_pStatusThread = AfxBeginThread(EuvSourceStatusThread, (LPVOID)this, THREAD_PRIORITY_NORMAL, 0, 0);		
		SetTimer(EUV_SOURCE_UPDATE_TIMER, 1000, NULL);
	}

	m_pConnectThread = AfxBeginThread(TryConnectThread, (LPVOID)this, THREAD_PRIORITY_NORMAL, 0, 0);
	
	return nRet;
}

UINT CEUVSourceDlg::EuvSourceStatusThread(LPVOID pParam)
{
	CEUVSourceDlg* pDlg = (CEUVSourceDlg*)pParam;

	while (!pDlg->m_bThreadExitFlag)
	{
		pDlg->SRC_Status();
		Sleep(1000);
	}

	return 0;
}

UINT CEUVSourceDlg::TryConnectThread(LPVOID pParam)
{
	CEUVSourceDlg* pDlg = (CEUVSourceDlg*)pParam;

	while (!pDlg->m_bThreadExitFlag)
	{
		if (pDlg->m_bConnected == FALSE)
		{
			int nRet = 0;
			//20210121 jkseo, CloseTcpIpSocket() 함수 호출 시 다른 모듈 문제 발생하여 임시 주석처리
			//pDlg->CloseTcpIpSocket();
			nRet = pDlg->OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_SRC_PC], g_pConfig->m_nPORT[ETHERNET_SRC_PC], FALSE);
			if (nRet == 0)
			{
				pDlg->SRC_Status(); Sleep(100);

				if (pDlg->m_bEuvOnState == FALSE)
				{
					CString log;
					log = _T("EUV OFF");
					pDlg->SaveLogFile("EuvUsage", (LPSTR)(LPCTSTR)log);
				}

				if (pDlg->m_pStatusThread == NULL)
				{
					pDlg->m_pStatusThread = AfxBeginThread(EuvSourceStatusThread, (LPVOID)pDlg, THREAD_PRIORITY_NORMAL);
					pDlg->SetTimer(EUV_SOURCE_UPDATE_TIMER, 1000, NULL);
				}
			}
		}

		Sleep(3000);
	}

	return 0;
}

void CEUVSourceDlg::GetDeviceStatus()
{
	if(m_bConnected == TRUE)
		((CStatic*)GetDlgItem(IDC_ICON_SOURCE_CONNECT))->SetIcon(m_LedIcon[1]);
	else
		((CStatic*)GetDlgItem(IDC_ICON_SOURCE_CONNECT))->SetIcon(m_LedIcon[0]);

	if (m_bEuvOnState == TRUE)
	{
		((CStatic*)GetDlgItem(IDC_ICON_SOURCE_EUV_ON))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_SOURCE_EUV_OFF))->SetIcon(m_LedIcon[0]);

		if (m_bChecked == FALSE)
		{
			CString log;
			log = _T("EUV ON");
			SaveLogFile("EuvUsage", (LPSTR)(LPCTSTR)log);
			m_bChecked = TRUE;
		}
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_SOURCE_EUV_ON))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_SOURCE_EUV_OFF))->SetIcon(m_LedIcon[2]);

		if (m_bChecked == TRUE)
		{
			CString log;
			log = _T("EUV OFF");
			SaveLogFile("EuvUsage", (LPSTR)(LPCTSTR)log);
			m_bChecked = FALSE;
		}
	}
	
	if (m_bLaserShutterOpenState == TRUE)
	{
		((CStatic*)GetDlgItem(IDC_ICON_SRC_LASER_SHUTTER_OPEN))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_SRC_LASER_SHUTTER_CLOSE))->SetIcon(m_LedIcon[0]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_SRC_LASER_SHUTTER_OPEN))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_SRC_LASER_SHUTTER_CLOSE))->SetIcon(m_LedIcon[2]);
	}

	if (m_bMechShutterOpenState == TRUE)
	{
		((CStatic*)GetDlgItem(IDC_ICON_SRC_SHUTTER_OPEN))->SetIcon(m_LedIcon[1]);
		((CStatic*)GetDlgItem(IDC_ICON_SRC_SHUTTER_CLOSE))->SetIcon(m_LedIcon[0]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_SRC_SHUTTER_OPEN))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_ICON_SRC_SHUTTER_CLOSE))->SetIcon(m_LedIcon[2]);
	}

	if (m_bMonitoringMode != m_bPreviousMonitoringMode)
	{
		m_bPreviousMonitoringMode = m_bMonitoringMode;
		if (m_bMonitoringMode == TRUE)
		{
		//	int nRet = g_pIO->SelCcdCamera();
		//	if (nRet != 0)
		//	{
		//		g_pLog->Display(0, _T("CEUVSourceDlg::GetDeviceStatus() SelCcdCamera fail."));
		//	}
		}
		else
		{
			//int nRet = g_pIO->SelXrayCamera();
			//if (nRet != 0)
			//{
			//	g_pLog->Display(0, _T("CEUVSourceDlg::GetDeviceStatus() SelXrayCamera fail."));
			//}
		}
	}

	CString strTemp;
	strTemp.Format(_T("%.2e"), m_dMainVacuumRate);
	m_MainVacuumCtrl.SetWindowText(strTemp);

	strTemp.Format(_T("%.2e"), m_dSubVacuumRate);
	m_SubVacuumCtrl.SetWindowText(strTemp);

	strTemp.Format(_T("%.2e"), m_dBufVacuumRate);
	m_BufVacuumCtrl.SetWindowText(strTemp);

	strTemp.Format(_T("%.2e"), m_dMFCNeonGauge);
	m_MfcNeonCtrl.SetWindowText(strTemp);

	strTemp.Format(_T("%.2e"), m_dNeonGasGauge);
	m_NeonGasCtrl.SetWindowText(strTemp);

	strTemp.Format(_T("%.2e"), m_dForelineGauge);
	m_ForelineCtrl.SetWindowText(strTemp);
}

void CEUVSourceDlg::OnDestroy()
{
	__super::OnDestroy();

	if (m_bConnected == TRUE && m_bMechShutterOpenState == TRUE)
		SRC_CloseMechShutter();
}


BOOL CEUVSourceDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}
	return __super::PreTranslateMessage(pMsg);
}


void CEUVSourceDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case EUV_SOURCE_UPDATE_TIMER:
		GetDeviceStatus();
		SetTimer(nIDEvent, 1000, NULL);
		break;
	default:
		break;
	}

	__super::OnTimer(nIDEvent);
}


void CEUVSourceDlg::OnBnClickedSrcVacuumStartButton()
{
	if(g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSourceDlg::OnBnClickedSrcVacuumStartButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	if (IDYES != AfxMessageBox("DO PUMPING?", MB_YESNO)) return;

	SRC_StartVacuum();
}


void CEUVSourceDlg::OnBnClickedSrcVacuumStopButton2()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSourceDlg::OnBnClickedSrcVacuumStopButton2() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	if (IDYES != AfxMessageBox("DO VENTING?", MB_YESNO)) return;

	SRC_StopVacuum();
}


void CEUVSourceDlg::OnBnClickedSrcEuvStartButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSourceDlg::OnBnClickedSrcEuvStartButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	if (g_pIO->Is_SourceGate_OpenOn_Check() != TRUE)
	{
		AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
		return;
	}

	if (IDYES != AfxMessageBox("START EUV?", MB_YESNO)) return;

	if(SetEUVSourceOn(TRUE) != 0)
		AfxMessageBox(_T("EUV ON 동작이 실패했습니다."));
}


void CEUVSourceDlg::OnBnClickedSrcEuvStopButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSourceDlg::OnBnClickedSrcEuvStopButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	if(SetEUVSourceOn(FALSE) != 0)
		AfxMessageBox(_T("EUV Off 동작이 실패했습니다."));
}


void CEUVSourceDlg::OnBnClickedSrcShutterOpenButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSourceDlg::OnBnClickedSrcShutterOpenButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	if (IDYES != AfxMessageBox("OPEN SHUTTER?", MB_YESNO)) return;

	int nRet = SRC_OpenMechShutter();
	if(nRet != 0)
		AfxMessageBox(_T("메카니컬 셔터 Open에 실패했습니다."));
}


void CEUVSourceDlg::OnBnClickedSrcShutterCloseButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CEUVSourceDlg::OnBnClickedSrcShutterCloseButton() 버튼 클릭!"));

	if (m_bConnected != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return;
	}

	if (IDYES != AfxMessageBox("CLOSE SHUTTER?", MB_YESNO)) return;

	int nRet = SRC_CloseMechShutter();
	if(nRet != 0)
		AfxMessageBox(_T("메카니컬 셔터 Close에 실패했습니다."));
}

int CEUVSourceDlg::SetMechShutterOpen(BOOL bState)
{
	int nRet = 0;

	if (bState == TRUE)
	{
		SRC_OpenMechShutter();
	}
	else
	{
		SRC_CloseMechShutter();
	}

	return nRet;
}

int CEUVSourceDlg::SetEUVSourceOn(BOOL bState)
{
	int nRet = 0;

	if (bState == TRUE)
	{
		//jkseo 인터락 추가 필요
		if (g_pGauge_IO->GetMcVacuumRate() <= g_pConfig->m_dEUVVacuumRate)
			SRC_StartEuv();
		else
			nRet = -999;
	}			
	else
	{
		SRC_StopEuv();
	}			

	return nRet;
}

void CEUVSourceDlg::EmergencyStop()
{
	int nRet = 0;
	CString str;

	nRet = SRC_StopEuv();
	str.Format(_T("CEUVSourceDlg::EmergencyStop(), SRC_StopEuv() Return %d"), nRet);
	g_pLog->Display(0, str);
}
