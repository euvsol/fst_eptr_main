﻿// CSubMenuDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CSubMenuDlg 대화 상자

IMPLEMENT_DYNAMIC(CSubMenuDlg, CDialogEx)

CSubMenuDlg::CSubMenuDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SUB_MENU_DIALOG, pParent)
{

}

CSubMenuDlg::~CSubMenuDlg()
{
}

void CSubMenuDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSubMenuDlg, CDialogEx)
	ON_BN_CLICKED(IDC_MTS_BUTTON, &CSubMenuDlg::OnBnClickedMtsButton)
	ON_BN_CLICKED(IDC_VMTR_BUTTON, &CSubMenuDlg::OnBnClickedVmtrButton)
	ON_BN_CLICKED(IDC_OM_BUTTON, &CSubMenuDlg::OnBnClickedOmButton)
	ON_BN_CLICKED(IDC_STAGE_BUTTON, &CSubMenuDlg::OnBnClickedStageButton)
	ON_BN_CLICKED(IDC_PUMP_BUTTON, &CSubMenuDlg::OnBnClickedPumpButton)
	ON_BN_CLICKED(IDC_DIO_BUTTON, &CSubMenuDlg::OnBnClickedDioButton)
	ON_BN_CLICKED(IDC_HWSTATE_BUTTON, &CSubMenuDlg::OnBnClickedHwstateButton)
	ON_BN_CLICKED(IDC_GEMAUTO_BUTTON, &CSubMenuDlg::OnBnClickedGemautoButton)
	ON_BN_CLICKED(IDC_SOURCE_BUTTON, &CSubMenuDlg::OnBnClickedSourceButton)
	ON_BN_CLICKED(IDC_XRAY_CAMERA_BUTTON, &CSubMenuDlg::OnBnClickedXrayCameraButton)
END_MESSAGE_MAP()


// CSubMenuDlg 메시지 처리기


void CSubMenuDlg::OnBnClickedMtsButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedMtsButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_MTS);
}


void CSubMenuDlg::OnBnClickedVmtrButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedVmtrButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_VMTR);
}


void CSubMenuDlg::OnBnClickedOmButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedOmButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_OM);
}


void CSubMenuDlg::OnBnClickedStageButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedStageButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_STAGE);
}


void CSubMenuDlg::OnBnClickedPumpButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedPumpButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_TURBOPUMP);
}


void CSubMenuDlg::OnBnClickedDioButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedDioButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_DIO);
}


void CSubMenuDlg::OnBnClickedHwstateButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedSubModuleButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_HWSTATUS);
}


void CSubMenuDlg::OnBnClickedGemautoButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedGemautoButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_GEMAUTO);
}

void CSubMenuDlg::OnBnClickedXrayCameraButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedXrayCameraButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_XRAYCAMERA);
}


void CSubMenuDlg::OnBnClickedSourceButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CSubMenuDlg::OnBnClickedSourceButton() 버튼 클릭!"));

	SetDisplay(DISPLAY_SOURCE);
}


int CSubMenuDlg::SetDisplay(int Mode)
{
	g_pSubTestMenu->ShowWindow(SW_HIDE);
	g_pNavigationStageTest->ShowWindow(SW_HIDE);
	
	g_pSourceTest->ShowWindow(SW_HIDE);
	g_pIO->ShowWindow(SW_HIDE);
	g_pEfem->ShowWindow(SW_HIDE);
	g_pNavigationStage->ShowWindow(SW_HIDE);
	
	g_pMCTmp_IO->ShowWindow(SW_HIDE);
	g_pLLCTmp_IO->ShowWindow(SW_HIDE);
	g_pGauge_IO->ShowWindow(SW_HIDE);
	g_pConfig->ShowWindow(SW_HIDE);
	g_pProcessEditor->ShowWindow(SW_HIDE);
	g_pCommStat->ShowWindow(SW_HIDE);
	g_pTest->ShowWindow(SW_HIDE);
	g_pVacuumRobot->ShowWindow(SW_HIDE);
	g_pMaindialog->ShowWindow(SW_HIDE);
	g_pCamera->ShowWindow(SW_HIDE);
	g_pCamZoneplate->ShowWindow(SW_HIDE);
	g_pEUVSource->ShowWindow(SW_HIDE);
	g_pSubMenu->ShowWindow(SW_SHOW);
	g_pChart->ShowWindow(SW_HIDE);
	g_pChartline ->ShowWindow(SW_HIDE);
	g_pChartstage ->ShowWindow(SW_HIDE);
	g_pFilterStage->ShowWindow(SW_HIDE);
	g_pLog->ShowWindow(SW_HIDE);
	g_pRecipe->ShowWindow(SW_HIDE);
	//g_pXrayCamera->ShowWindow(SW_HIDE);
	//g_pPhase->ShowWindow(SW_HIDE);
	//g_pXrayCameraConfig->ShowWindow(SW_HIDE);
	g_pLightCtrl->ShowWindow(SW_HIDE);
	//g_pPlasmaCleaner->ShowWindow(SW_HIDE);
	g_pSubModule->ShowWindow(SW_HIDE);
	if (g_pPTR != NULL) g_pPTR->ShowWindow(SW_HIDE);

	switch (Mode)
	{
	case DISPLAY_MTS:
		g_pEfem->ShowWindow(SW_SHOW);
		g_pCamera->ShowWindow(SW_SHOW);
		g_pLog->ShowWindow(SW_SHOW);
		g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType = SCOPE_OM4X;
		break;
	case DISPLAY_VMTR:
		g_pVacuumRobot->ShowWindow(SW_SHOW);
		g_pCamera->ShowWindow(SW_SHOW);
		g_pLog->ShowWindow(SW_SHOW);
		g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType = SCOPE_OM4X;
		break;
	case DISPLAY_ISOLATOR:
		g_pCamera->ShowWindow(SW_SHOW);
		g_pLog->ShowWindow(SW_SHOW);
		g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType = SCOPE_OM4X;
		break;
	case DISPLAY_OM:
		g_pLightCtrl->ShowWindow(SW_SHOW);
		g_pCamera->ShowWindow(SW_SHOW);
		g_pLog->ShowWindow(SW_SHOW);
		g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType = SCOPE_OM4X;
		g_pNavigationStage->ShowWindow(SW_SHOW);
		break;
	case DISPLAY_STAGE:
		g_pNavigationStage->ShowWindow(SW_SHOW);

		g_pCamera->ShowWindow(SW_SHOW);
		g_pLog->ShowWindow(SW_SHOW);
		g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType = SCOPE_OM4X;
		g_pChartstage->ShowWindow(SW_SHOW);
		break;
	case DISPLAY_TURBOPUMP:
		g_pMCTmp_IO->ShowWindow(SW_SHOW);
		g_pLLCTmp_IO->ShowWindow(SW_SHOW);
		g_pGauge_IO->ShowWindow(SW_SHOW);
		break;
	case DISPLAY_DIO:
		g_pIO->ShowWindow(SW_SHOW);
		//g_pLLKTmp_IO->ShowWindow(SW_SHOW);
		//g_pMCTmp_IO->ShowWindow(SW_SHOW);
		//g_pGauge_IO->ShowWindow(SW_SHOW);
		g_pMaindialog->ShowWindow(SW_SHOW);		
		g_pLog->ShowWindow(SW_SHOW);
		break;
	case DISPLAY_SOURCE:
		g_pEUVSource->ShowWindow(SW_SHOW);
		g_pFilterStage->ShowWindow(SW_SHOW);
		break;
	case DISPLAY_HWSTATUS:
		g_pSubModule->ShowWindow(SW_SHOW);
		//g_pCommStat->ShowWindow(SW_SHOW);
		//g_pCamera->ShowWindow(SW_SHOW);
		g_pLog->ShowWindow(SW_SHOW);
		//g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType = SCOPE_OM4X;
		break;
	case DISPLAY_GEMAUTO:
		g_pCamera->ShowWindow(SW_SHOW);
		g_pLog->ShowWindow(SW_SHOW);
		g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType = SCOPE_OM4X;
		break;
	default:
		break;
	}

	return 0;
}



BOOL CSubMenuDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	
	if (g_pConfig->m_nEquipmentType != PHASE)
	{
		GetDlgItem(IDC_XRAY_CAMERA_BUTTON)->EnableWindow(FALSE);
		//GetDlgItem(IDC_XRAY_CAMERA_BUTTON)->ShowWindow(SW_HIDE);
	}
		

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
