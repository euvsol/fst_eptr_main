﻿// CIOOutputDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CDigitalOutput 대화 상자

IMPLEMENT_DYNAMIC(CDigitalOutput, CDialogEx)

CDigitalOutput::CDigitalOutput(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IO_OUTPUT_DIALOG, pParent)
{
}

CDigitalOutput::~CDigitalOutput()
{
	m_brush.DeleteObject();
	m_font.DeleteObject();
}		

void CDigitalOutput::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDigitalOutput, CDialogEx)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y000, &CDigitalOutput::OnBnClickedCheckDigitaloutY000)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y001, &CDigitalOutput::OnBnClickedCheckDigitaloutY001)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y002, &CDigitalOutput::OnBnClickedCheckDigitaloutY002)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y003, &CDigitalOutput::OnBnClickedCheckDigitaloutY003)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y004, &CDigitalOutput::OnBnClickedCheckDigitaloutY004)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y005, &CDigitalOutput::OnBnClickedCheckDigitaloutY005)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y006, &CDigitalOutput::OnBnClickedCheckDigitaloutY006)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y007, &CDigitalOutput::OnBnClickedCheckDigitaloutY007)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y008, &CDigitalOutput::OnBnClickedCheckDigitaloutY008)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y009, &CDigitalOutput::OnBnClickedCheckDigitaloutY009)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y010, &CDigitalOutput::OnBnClickedCheckDigitaloutY010)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y011, &CDigitalOutput::OnBnClickedCheckDigitaloutY011)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y012, &CDigitalOutput::OnBnClickedCheckDigitaloutY012)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y013, &CDigitalOutput::OnBnClickedCheckDigitaloutY013)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y014, &CDigitalOutput::OnBnClickedCheckDigitaloutY014)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_Y015, &CDigitalOutput::OnBnClickedCheckDigitaloutY015)
	ON_WM_DESTROY()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CDigitalOutput 메시지 처리기

BOOL CDigitalOutput::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_brush.CreateSolidBrush(LIGHT_GRAY); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CDigitalOutput::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CDigitalOutput::OnDestroy()
{
	CDialogEx::OnDestroy();

	KillTimer(0);
}

void CDigitalOutput::InitControls_DO(int nChannel)
{
	m_nChannel = nChannel;

	CString strTemp;
	CString Get_str;

	for (int nIdx = nChannel * DIGITAL_IO_VIEW_NUMBER, nCnt = 0; nIdx < (nChannel * DIGITAL_IO_VIEW_NUMBER) + DIGITAL_IO_VIEW_NUMBER; nIdx++, nCnt++)
	{
		strTemp.Format(_T("Y%04d"), nCnt + (nChannel * 100));
		SetDlgItemText(IDC_STATIC_DIGITALOUT_NUM0 + nCnt, strTemp);
		

		GetDlgItem(IDC_STATIC_DIGITALOUT_Y000 + nCnt)->SetWindowText(g_pConfig->m_chDo[nIdx]);
		GetDlgItem(IDC_STATIC_DIGITALOUT_Y000 + nCnt)->GetWindowText(Get_str);
		
		if ((strTemp == "Y0306") || (strTemp == "Y0307") || (strTemp == "Y0308") || (strTemp == "Y0309") || (strTemp == "Y0206"))  GetDlgItem(IDC_CHECK_DIGITALOUT_Y000 + nCnt)->EnableWindow(false);
		if ((Get_str == "EMPTY") || (Get_str == "")) GetDlgItem(IDC_CHECK_DIGITALOUT_Y000 + nCnt)->EnableWindow(false);
		
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_Y000 + nCnt))->SetIcon(m_LedIcon[0]);

		CButton* btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_Y000 + nCnt);
		
		if (g_pIO->m_bDigitalOut[nIdx] == 1)
		{
			btn->SetCheck(true);
			btn->SetWindowTextA(_T("On"));
		}
		else
		{
			btn->SetCheck(false);
			btn->SetWindowTextA(_T("Off"));
		}
	}

	SetTimer(0, 500, NULL);
}

void CDigitalOutput::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == 0)
	{
		OnUpdateDigitalOutput();
		SetTimer(0, 500, NULL);
	}
	CDialog::OnTimer(nIDEvent);
}

int CDigitalOutput::SetOutputBitOnOff(int nIndex)
{
	int IO_WriteStatus = 1;
	CString str;

	CButton* btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_Y000 + nIndex);

	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		int nCheck = btn->GetCheck();
		int SEL;

		if (nCheck)
			btn->SetWindowTextA(_T("On"));
		else
			btn->SetWindowTextA(_T("Off"));

		SEL = (m_nChannel * DIGITAL_IO_VIEW_NUMBER) + nIndex;

		IO_WriteStatus = WriteOutputBitOnOff(SEL, nCheck);
		if (IO_WriteStatus != OPERATION_COMPLETED)
		{
			g_pAlarm->SetAlarm((IO_WriteStatus));
		}

		return 0;
	}
	else
	{
		btn->SetWindowTextA(_T("Off"));
		btn->SetCheck(false);
		AfxMessageBox("IO 연결이 되어 있지 않습니다. 연결 확인 후 재 시도 해주세요");
		g_pAlarm->SetAlarm((IO_CONNECTION_ERROR));
		return IO_CONNECTION_ERROR;
	}

}

int CDigitalOutput::WriteOutputBitOnOff(int SEL, int nCheck)
{

	int ADDR =-1;
	int bitdex=-1;
	int nRet = 1;

	CString str_IOString, str_EventState, str_IOMode, str, str_nRet;

	str_IOString.Empty();
	str_EventState.Empty();
	str_IOMode.Empty();
	str.Empty();
	str_nRet.Empty();

	g_pIO->Log_str;

	str_IOString.Format(_T("%s "), g_pConfig->m_chDo[SEL]);
	if (g_pIO->m_nIOMode == MAINT_MODE_ON)
	{
		str_IOMode = _T("Maint Mode");
		if (nCheck)
			str_EventState.Format( _T("On/Open"));
		else
			str_EventState.Format( _T("Off/Close"));

		SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " Click Evnet")));	//IO 버튼 Event 상태 기록.
		g_pLog->Display(0, "EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " Click Evnet");
		
		nRet = g_pIO->DigitalWriteIO(SEL, nCheck);
		if (nRet == OPERATION_COMPLETED)
		{
			if (SEL == g_pIO->DO::LLC_GATE_VALVE_OPEN_STATUS)
			{
				if (nCheck)
					g_pIO->DigitalWriteIO(g_pIO->DO::LLC_GATE_VALVE_CLOSE_STATUS, VALVE_CLOSE);
				else
					g_pIO->DigitalWriteIO(g_pIO->DO::LLC_GATE_VALVE_CLOSE_STATUS, VALVE_OPEN);
			}
			else if (SEL == g_pIO->DO::LLC_GATE_VALVE_CLOSE_STATUS)
			{
				if (nCheck)
					g_pIO->DigitalWriteIO(g_pIO->DO::LLC_GATE_VALVE_OPEN_STATUS, VALVE_CLOSE);
				else
					g_pIO->DigitalWriteIO(g_pIO->DO::LLC_GATE_VALVE_OPEN_STATUS, VALVE_OPEN);
			}
			else if (SEL == g_pIO->DO::TR_GATE_VALVE_OPEN_STATUS)
			{
				if (nCheck)
					g_pIO->DigitalWriteIO(g_pIO->DO::TR_GATE_VALVE_CLOSE_STATUS, VALVE_CLOSE);
				else
					g_pIO->DigitalWriteIO(g_pIO->DO::TR_GATE_VALVE_CLOSE_STATUS, VALVE_OPEN);
			}
			else if (SEL == g_pIO->DO::TR_GATE_VALVE_CLOSE_STATUS)
			{
				if (nCheck)
					g_pIO->DigitalWriteIO(g_pIO->DO::TR_GATE_VALVE_OPEN_STATUS, VALVE_CLOSE);
				else
					g_pIO->DigitalWriteIO(g_pIO->DO::TR_GATE_VALVE_OPEN_STATUS, VALVE_OPEN);

			}
		}
		//if (SEL <= 7)
		//{
		//	//01 23
		//	//45
		//	ADDR = 16;
		//	bitdex = SEL;
		//}
		//else if ((SEL >= 8) && (SEL <= 15))
		//{
		//	ADDR = 17;
		//	bitdex = SEL - 8;
		//}
		//else if ((SEL >= 16) && (SEL <= 23))
		//{
		//	ADDR = 18;
		//	bitdex = SEL - 16;
		//	//if ((SEL == g_pIO->DO::LLC_SLOW_ROUGHING) || (SEL == g_pIO->DO::LLC_FAST_ROUGHING))
		//	//{
		//	//	if (nCheck)
		//	//	{
		//	//		/*TR Gate 닫혀 있을시*/
		//	//		if (g_pIO->Is_TRGateValve_Open() == VALVE_CLOSED)
		//	//		{
		//	//			if ((g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSED))
		//	//			{
		//	//				str = _T("LLC ROUGH VALVE OPEN 불가 !! LLC FORELINE VALVE OPEN 되어 있습니다");
		//	//				::AfxMessageBox(str, MB_ICONSTOP);
		//	//
		//	//				/* jhkim
		//	//				LLC Roughing Oprn 시 Foreline open 되어 있으므로 Fail
		//	//				Error Code 정의 필요
		//	//				*/
		//	//				nRet = -87044;
		//	//				SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution Error:: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " LLC_FORELINE_VALVE_OPEN 으로 Fail. Foreline Valve 확인 필요 ")));
		//	//				g_pLog->Display(0, "EuvSolution  Error :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " LLC_FORELINE_VALVE_OPEN 으로 Fail. Foreline Valve 확인 필요");
		//	//				return nRet;
		//	//			}
		//	//		}
		//	//		/*TR Gate 열려 있을시*/
		//	//		else if (g_pIO->Is_TRGateValve_Open() == VALVE_OPENED)
		//	//		{
		//	//			if ((g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSED) || ((g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSED)))
		//	//			{
		//	//				str = _T("LLC ROUGH VALVE OPEN 불가 !! LLC 혹은 MC FORELINE VALVE OPEN 되어 있습니다");
		//	//				::AfxMessageBox(str, MB_ICONSTOP);
		//	//
		//	//				/* jhkim
		//	//				LLC Roughing Oprn 시 Foreline open 되어 있으므로 Fail
		//	//				Error Code 정의 필요
		//	//				*/
		//	//				nRet = -87044;
		//	//				SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution Error:: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " LLC 혹은 MC FORELINE_VALVE_OPEN 으로 Fail. Foreline Valve 확인 필요 ")));
		//	//				g_pLog->Display(0, "EuvSolution  Error :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " LLC 혹은 MC FORELINE_VALVE_OPEN 으로 Fail. Foreline Valve 확인 필요");
		//	//				return nRet;
		//	//			}
		//	//		}
		//	//
		//	//	}
		//	//}
		//	//else if ((SEL == g_pIO->DO::LLC_FORELINE))
		//	//{
		//	//	if (nCheck)
		//	//	{
		//	//		/*TR Gate 닫혀 있을시*/
		//	//		if (g_pIO->Is_TRGateValve_Open() == VALVE_CLOSED)
		//	//		{
		//	//			if ((g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED) || (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED))
		//	//			{
		//	//				str = _T("LLC FORELINE VALVE OPEN 불가 !! LLC ROUGH VALVE OPEN 되어 있습니다");
		//	//				::AfxMessageBox(str, MB_ICONSTOP);
		//	//
		//	//				/* jhkim
		//	//				LLC Roughing Oprn 시 Foreline open 되어 있으므로 Fail
		//	//				Error Code 정의 필요
		//	//				*/
		//	//				nRet = -87044;
		//	//				SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution Error:: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " LLC ROUGH VALVE OPEN 으로 Fail. Roughing Valve 확인 필요 ")));
		//	//				g_pLog->Display(0, "EuvSolution  Error :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " LLC ROUGH VALVE OPEN 으로 Fail. Roughing Valve 확인 필요");
		//	//				return nRet;
		//	//			}
		//	//		}
		//	//		/*TR Gate 열려 있을시*/
		//	//		else if (g_pIO->Is_TRGateValve_Open() == VALVE_OPENED)
		//	//		{
		//	//			if ((g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED) || (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED) 
		//	//					|| (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSE) || (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_CLOSE))
		//	//			{
		//	//				str = _T("LLC FORELINE VALVE OPEN 불가 !! LLC 혹은 MC ROUGH VALVE OPEN 되어 있습니다");
		//	//				::AfxMessageBox(str, MB_ICONSTOP);
		//	//
		//	//				/* jhkim
		//	//				LLC Roughing Oprn 시 Foreline open 되어 있으므로 Fail
		//	//				Error Code 정의 필요
		//	//				*/
		//	//				nRet = -87044;
		//	//				SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution Error:: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " LLC 혹은 MC ROUGH VALVE OPEN 으로 Fail. Roughing Valve 확인 필요 ")));
		//	//				g_pLog->Display(0, "EuvSolution  Error :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " LLC 혹은 MC ROUGH VALVE OPEN 으로 Fail. Roughing Valve 확인 필요");
		//	//				return nRet;
		//	//			}
		//	//		}
		//	//	}
		//	//}
		//}
		//else if ((SEL >= 24) && (SEL <= 31))
		//{
		//	ADDR = 19;
		//	bitdex = SEL - 24;
		//	if (SEL == g_pIO->DO::LLC_GATE_VALVE_OPEN_STATUS)
		//	{
		//		if (nCheck)
		//			nRet = g_pIO->WriteOutputDataBit(ADDR, 5, 0);
		//		else
		//			nRet = g_pIO->WriteOutputDataBit(ADDR, 5, 1);
		//	}
		//	
		//	if (SEL == g_pIO->DO::LLC_GATE_VALVE_CLOSE_STATUS)
		//	{
		//		if (nCheck)
		//			nRet = g_pIO->WriteOutputDataBit(ADDR, 4, 0);
		//		else
		//			nRet = g_pIO->WriteOutputDataBit(ADDR, 4, 1);
		//	}
		//
		//	if (SEL == g_pIO->DO::TR_GATE_VALVE_OPEN_STATUS)
		//	{
		//		if (nCheck)
		//			nRet = g_pIO->WriteOutputDataBit(ADDR, 7, 0);
		//		else
		//			nRet = g_pIO->WriteOutputDataBit(ADDR, 7, 1);
		//	}
		//
		//	if (SEL == g_pIO->DO::TR_GATE_VALVE_CLOSE_STATUS)
		//	{
		//		if (nCheck)
		//			nRet = g_pIO->WriteOutputDataBit(ADDR, 6, 0);
		//		else
		//			nRet = g_pIO->WriteOutputDataBit(ADDR, 6, 1);
		//	}
		//
		//	
		//	//if ((SEL == g_pIO->DO::MC_SLOW_ROUGHING) || (SEL == g_pIO->DO::MC_FAST_ROUGHING))
		//	//{
		//	//	if (nCheck)
		//	//	{
		//	//		/*TR Gate 닫혀 있을시*/
		//	//		if (g_pIO->Is_TRGateValve_Open() == VALVE_CLOSED)
		//	//		{
		//	//			if ((g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSED))
		//	//			{
		//	//				str = _T("MC ROUGH VALVE OPEN 불가 !! LLC FORELINE VALVE OPEN 되어 있습니다");
		//	//				::AfxMessageBox(str, MB_ICONSTOP);
		//	//
		//	//				/* jhkim
		//	//				MC Roughing Oprn 시 Foreline open 되어 있으므로 Fail
		//	//				Error Code 정의 필요
		//	//				*/
		//	//				nRet = -87044;
		//	//				SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution Error:: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " MC_FORELINE_VALVE_OPEN 으로 Fail. Foreline Valve 확인 필요 ")));
		//	//				g_pLog->Display(0, "EuvSolution  Error :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " MC_FORELINE_VALVE_OPEN 으로 Fail. Foreline Valve 확인 필요");
		//	//				return nRet;
		//	//			}
		//	//		}
		//	//		/*TR Gate 열려 있을시*/
		//	//		else if (g_pIO->Is_TRGateValve_Open() == VALVE_OPENED)
		//	//		{
		//	//			if ((g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSED) || ((g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSED)))
		//	//			{
		//	//				str = _T("MC ROUGH VALVE OPEN 불가 !! LLC 혹은 MC FORELINE VALVE OPEN 되어 있습니다");
		//	//				::AfxMessageBox(str, MB_ICONSTOP);
		//	//
		//	//				/* jhkim
		//	//				MC Roughing Oprn 시 Foreline open 되어 있으므로 Fail
		//	//				Error Code 정의 필요
		//	//				*/
		//	//				nRet = -87044;
		//	//				SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution Error:: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " LLC 혹은 MC FORELINE_VALVE_OPEN 으로 Fail. Foreline Valve 확인 필요 ")));
		//	//				g_pLog->Display(0, "EuvSolution  Error :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " LLC 혹은 MC FORELINE_VALVE_OPEN 으로 Fail. Foreline Valve 확인 필요");
		//	//				return nRet;
		//	//			}
		//	//		}
		//	//
		//	//
		//	//	}
		//	//}
		//	//else if (SEL == g_pIO->DO::MC_FORELINE)
		//	//{
		//	//	if (nCheck)
		//	//	{
		//	//		/*TR Gate 닫혀 있을시*/
		//	//		if (g_pIO->Is_TRGateValve_Open() == VALVE_CLOSED)
		//	//		{
		//	//			if ((g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSED) || (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_CLOSED))
		//	//			{
		//	//				str = _T("MC FORELINE VALVE OPEN 불가 !! MC ROUGH VALVE OPEN 되어 있습니다");
		//	//				::AfxMessageBox(str, MB_ICONSTOP);
		//	//
		//	//				/* jhkim
		//	//				MC Foreline Open 시 roughing open 되어 있으므로 Fail
		//	//				Error Code 정의 필요
		//	//				*/
		//	//				nRet = -87044;
		//	//				SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution Error:: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " MC ROUGH VALVE OPEN 으로 Fail. Roughing Valve 확인 필요 ")));
		//	//				g_pLog->Display(0, "EuvSolution  Error :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " MC ROUGH VALVE OPEN 으로 Fail. Roughing Valve 확인 필요");
		//	//				return nRet;
		//	//			}
		//	//		}
		//	//		/*TR Gate 열려 있을시*/
		//	//		else if (g_pIO->Is_TRGateValve_Open() == VALVE_OPENED)
		//	//		{
		//	//			if ((g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED) || (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
		//	//				|| (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSE) || (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_CLOSE))
		//	//			{
		//	//				str = _T("MC FORELINE VALVE OPEN 불가 !! LLC 혹은 MC ROUGH VALVE OPEN 되어 있습니다");
		//	//				::AfxMessageBox(str, MB_ICONSTOP);
		//	//
		//	//				/* jhkim
		//	//					MC Foreline Open 시 roughing open 되어 있으므로 Fail
		//	//				Error Code 정의 필요
		//	//				*/
		//	//				nRet = -87044;
		//	//				SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution Error:: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " LLC 혹은 MC ROUGH VALVE OPEN 으로 Fail. Roughing Valve 확인 필요 ")));
		//	//				g_pLog->Display(0, "EuvSolution  Error :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " LLC 혹은 MC ROUGH VALVE OPEN 으로 Fail. Roughing Valve 확인 필요");
		//	//				return nRet;
		//	//			}
		//	//		}
		//	//	}
		//	//}
		//	//else if (SEL == g_pIO->DO::LLC_GATE_VALVE_OPEN_STATUS)
		//	//{
		//	//	nRet = g_pIO->WriteOutputDataBit(ADDR, (g_pIO->DO::LLC_GATE_VALVE_CLOSE_STATUS - 24) ,  0); // LL GATE CLOSE OFF
		//	//}
		//	//else if (SEL == g_pIO->DO::LLC_GATE_VALVE_CLOSE_STATUS)
		//	//{
		//	//	nRet = g_pIO->WriteOutputDataBit(ADDR, (g_pIO->DO::LLC_GATE_VALVE_OPEN_STATUS -24), 0); // LL GATE OPEN OFF
		//	//}
		//	//else if (SEL == g_pIO->DO::TR_GATE_VALVE_OPEN_STATUS)
		//	//{
		//	//	nRet = g_pIO->WriteOutputDataBit(ADDR, (g_pIO->DO::TR_GATE_VALVE_CLOSE_STATUS - 24), 0); // TR GATE CLOSE OFF
		//	//}
		//	//else if (SEL == g_pIO->DO::TR_GATE_VALVE_CLOSE_STATUS)
		//	//{
		//	//	nRet = g_pIO->WriteOutputDataBit(ADDR, (g_pIO->DO::TR_GATE_VALVE_OPEN_STATUS - 24), 0); // TR GATE OPEN OFF
		//	//}
		//}
		//else if ((SEL >= 32) && (SEL <= 39))
		//{
		//	ADDR = 20;
		//	bitdex = SEL - 32;
		//
		//}
		//else if ((SEL >= 40) && (SEL <= 47))
		//{
		//	ADDR = 21;
		//	bitdex = SEL - 40;
		//}
		//else if ((SEL >= 48) && (SEL <= 55))
		//{
		//	ADDR = 22;
		//	bitdex = SEL - 48;
		//
		//}
		//else if ((SEL >= 56) && (SEL <= 63))
		//{
		//	ADDR = 23;
		//	bitdex = SEL - 56;
		//
		//}
		//////////////////////////////////////////////////////////////////////////////////
		//IO WRITE ( 정상 Write 시 return 0 )
		//////////////////////////////////////////////////////////////////////////////////
		//nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck);
		//////////////////////////////////////////////////////////////////////////////////
		str_nRet.Format(_T("%d"), nRet);
		SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " Click Evnet Return Value :: " + str_nRet)));
		g_pLog->Display(0, "EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " Click Evnet Return Value :: " + str_nRet);

		if (nRet != OPERATION_COMPLETED)
		{
			::AfxMessageBox("Write 실패! ", MB_ICONSTOP);
			SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " 실행 실패 ")));	//IO 버튼 Event 결과 상태 기록.
			g_pLog->Display(0, "EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " Click Evnet ====== 실행 실패 ");
			switch (SEL)
			{
				case g_pIO->SIGNAL_TOWER_RED :
					break;
				case g_pIO->SIGNAL_TOWER_YELLOW :
					break;
				case g_pIO->SIGNAL_TOWER_GREEN :
					break;
				case g_pIO->SIGNAL_TOWER_BUZZER_1 :
					break;
				case g_pIO->SIGNAL_TOWER_BUZZER_2 :
					break;
				case g_pIO->DigitalOutput_5 :
					break;
				case g_pIO->WATER_SUPPLY_VALVE :
					break;
				case g_pIO->WATER_RETURN_VALVE :
					break;
				case g_pIO->DigitalOutput_8 :
					break;
				case g_pIO->DVR_CAM1_SW :
					break;
				case g_pIO->DVR_CAM2_SW :
					break;
				case g_pIO->DVR_CAM3_SW :
					break;
				case g_pIO->DVR_CAM4_SW :
					break;
				case g_pIO->DigitalOutput_13 :
					break;
				case g_pIO->LLC_MASK_CHECK_TRIGGER :
					break;
				case g_pIO->DigitalOutput_15 :
					break;
				case g_pIO->LLC_DRY_PUMP_SW :
					break;
				case g_pIO->MC_DRY_PUMP_SW :
					break;
				case g_pIO->DigitalOutput_18 :
					break;
				case g_pIO->LLC_SLOW_ROUGHING :
					break;
				case g_pIO->LLC_FAST_ROUGHING :
					break;
				case g_pIO->LLC_TMP_GATE :
					break;
				case g_pIO->LLC_FORELINE :
					break;
				case g_pIO->DigitalOutput_23 :
					break;
				case g_pIO->MC_SLOW_ROUGHING :
					break;
				case g_pIO->MC_FAST_ROUGHING :
					break;
				case g_pIO->MC_TMP_GATE :
					break;
				case g_pIO->MC_FORELINE :
					break;
				case g_pIO->LLC_GATE_VALVE_OPEN_STATUS :
					break;
				case g_pIO->LLC_GATE_VALVE_CLOSE_STATUS :
					break;
				case g_pIO->TR_GATE_VALVE_OPEN_STATUS :
					break;
				case g_pIO->TR_GATE_VALVE_CLOSE_STATUS :
					break;
				case g_pIO->SLOW_VENT_INLET :
					break;
				case g_pIO->SLOW_VENT_OUTLET :
					break;
				case g_pIO->FAST_VENT_INLET : 
					break;
				case g_pIO->FAST_VENT_OUTLET :
					break;
				case g_pIO->DigitalOutput_36 : 
					break;
				case g_pIO->FRAME_COVER_DOOR_OPEN :
					break;
				case g_pIO->EUV_SHUTTER_CHECK :
					break;
				case g_pIO->DigitalOutput_39 :
					break;
				case g_pIO->DigitalOutput_40 :
					break;
				case g_pIO->DigitalOutput_41 :
					break;
				case g_pIO->UTILLITY_SPARE1 :
					break;
				case g_pIO->UTILLITY_SPARE2 :
					break;
				case g_pIO->INTERFACE_BOARD_SPARE1 :
					break;
				case g_pIO->INTERFACE_BOARD_SPARE2 :
					break;
				case g_pIO->DigitalOutput_46 :
					break;
				case g_pIO->DigitalOutput_47 :
					break;
				case g_pIO->DigitalOutput_48 :
					break;
				case g_pIO->DigitalOutput_49 :
					break;
				case g_pIO->DigitalOutput_50 :
					break;
				case g_pIO->DigitalOutput_51 :
					break;
				case g_pIO->DigitalOutput_52 :
					break;
				case g_pIO->DigitalOutput_53 :
					break;
				case g_pIO->LLC_SET_POINT_OUT_1_ATM :
					break;
				case g_pIO->LLC_SET_POINT_OUT_2_VAC :
					break;
				case g_pIO->MC_SET_POINT_OUT_1_ATM :
					break;
				case g_pIO->MC_SET_POINT_OUT_2_VAC :
					break;
				case g_pIO->DigitalOutput_58 :
					break;
				case g_pIO->DigitalOutput_59 :
					break;
				case g_pIO->DigitalOutput_60 :
					break;
				case g_pIO->DigitalOutput_61 :
					break;
				case g_pIO->DigitalOutput_62 :
					break;
				case g_pIO->DigitalOutput_63 :
					break;
				default:
					break;
			}

		}
		else
		{
			SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " 실행 완료 ")));	//IO 버튼 Event 결과 상태 기록.
			g_pLog->Display(0, "EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " Click Evnet ====== 실행 완료 ");
		}


	}
	else if (g_pIO->m_nIOMode == OPER_MODE_ON)
	{
		str_IOMode = _T("Oper Mode");
		if (nCheck) str_EventState.Format(_T(" On 또는 Open"));
		else str_EventState.Format( _T(" Off 또는 Close"));

		if (g_pIO->Is_Water_Valve_Open() == VALVE_OPENED)
		{

			SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " Click Evnet")));	//IO 버튼 Event 상태 기록.
			SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " 실행 하시겠습니까 ? (Yes / No)")));	//IO 버튼 Event 상태 기록.
			g_pLog->Display(0, "EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " 실행 하시겠습니까 ? (Yes / No)");
			if (g_pIO->Str_ok_Box(str_IOString, str_EventState) == RUN)
			{
				SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " 실행 하시겠습니까 ? (Yes)")));	//IO 버튼 Event 상태 기록.
				g_pLog->Display(0, "EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " 실행 하시겠습니까 ? (Yes)");
				switch (SEL)
				{
				case g_pIO->DO::SIGNAL_TOWER_RED:
					if (nCheck)
						nRet = g_pIO->On_SignalTowerRed();
					else
						nRet = g_pIO->Off_SignalTowerRed();
					break;
				case g_pIO->DO::SIGNAL_TOWER_YELLOW:
					if (nCheck)
						nRet = g_pIO->On_SignalTowerYellow();
					else
						nRet = g_pIO->Off_SignalTowerYellow();
					break;
				case g_pIO->DO::SIGNAL_TOWER_GREEN:
					if (nCheck)
						nRet = g_pIO->On_SignalTowerGreen();
					else
						nRet = g_pIO->Off_SignalTowerGreen();
					break;
				case g_pIO->DO::SIGNAL_TOWER_BUZZER_1:
					if (nCheck)
						nRet = g_pIO->On_SignalTowerBuzzer_1();
					else
						nRet = g_pIO->Off_SignalTowerBuzzer_1();
					break;
				case g_pIO->DO::SIGNAL_TOWER_BUZZER_2:
					if (nCheck)
						nRet = g_pIO->On_SignalTowerBuzzer_2();
					else
						nRet = g_pIO->Off_SignalTowerBuzzer_2();
					break;
				case g_pIO->DO::DigitalOutput_5:
					break;
				case g_pIO->DO::WATER_SUPPLY_VALVE:
					if (nCheck)
						nRet = g_pIO->Open_WaterSupplyValve();
					else
						nRet = g_pIO->Close_WaterSupplyValve();
					break;
				case g_pIO->DO::WATER_RETURN_VALVE:
					if (nCheck)
						nRet = g_pIO->Open_WaterReturnValve();
					else
						nRet = g_pIO->Close_WaterReturnValve();
					break;
				case g_pIO->DO::DigitalOutput_8:
					break;
				case g_pIO->DO::DVR_CAM1_SW:
					if (nCheck)
						nRet = g_pIO->Open_DVR_Valve(1);
					else
						nRet = g_pIO->Close_DVR_Valve(1);
					break;
				case g_pIO->DO::DVR_CAM2_SW:
					if (nCheck)
						nRet = g_pIO->Open_DVR_Valve(2);
					else
						nRet = g_pIO->Close_DVR_Valve(2);
					break;
				case g_pIO->DO::DVR_CAM3_SW:
					if (nCheck)
						nRet = g_pIO->Open_DVR_Valve(3);
					else
						nRet = g_pIO->Close_DVR_Valve(3);
					break;
				case g_pIO->DO::DVR_CAM4_SW:
					if (nCheck)
						nRet = g_pIO->Open_DVR_Valve(4);
					else
						nRet = g_pIO->Close_DVR_Valve(4);
					break;
				case g_pIO->DO::DigitalOutput_13:
					break;
				case g_pIO->DO::LLC_MASK_CHECK_TRIGGER:
					break;
				case g_pIO->DO::DigitalOutput_15:
					break;
				case g_pIO->DO::LLC_DRY_PUMP_SW:
					if (nCheck)
						nRet = g_pIO->On_LlcDryPump_Valve();
					else
						nRet = g_pIO->Off_LlcDryPump_Valve();
					break;
				case g_pIO->DO::MC_DRY_PUMP_SW:
					if (nCheck)
						nRet = g_pIO->On_McDryPump_Valve();
					else
						nRet = g_pIO->Off_McDryPump_Valve();
					break;
				case g_pIO->DO::DigitalOutput_18:
					break;
				case g_pIO->DO::LLC_SLOW_ROUGHING:
					if (nCheck)
						nRet = g_pIO->Open_LLC_SlowRoughValve();
					else
						nRet = g_pIO->Close_LLC_SlowRoughValve();
					break;
				case g_pIO->DO::LLC_FAST_ROUGHING:
					if (nCheck)
						nRet = g_pIO->Open_LLC_FastRoughValve();
					else
						nRet = g_pIO->Close_LLC_FastRoughValve();
					break;
				case g_pIO->DO::LLC_TMP_GATE:
					if (nCheck)
						nRet = g_pIO->Open_LLC_TMP_GateValve();
					else
						nRet = g_pIO->Close_LLC_TMP_GateValve();
					break;
				case g_pIO->DO::LLC_FORELINE:
					if (nCheck)
						nRet = g_pIO->Open_LLC_TMP_ForelineValve();
					else
						nRet = g_pIO->Close_LLC_TMP_ForelineValve();
					break;
				case g_pIO->DO::DigitalOutput_23:
					break;
				case g_pIO->DO::MC_SLOW_ROUGHING:
					if (nCheck)
						nRet = g_pIO->Open_MC_SlowRoughValve();
					else
						nRet = g_pIO->Close_MC_SlowRoughValve();
					break;
				case g_pIO->DO::MC_FAST_ROUGHING:
					if (nCheck)
						nRet = g_pIO->Open_MC_FastRoughValve();
					else
						nRet = g_pIO->Close_MC_FastRoughValve();
					break;
				case g_pIO->DO::MC_TMP_GATE:
					if (nCheck)
						nRet = g_pIO->Open_MC_TMP_GateValve();
					else
						nRet = g_pIO->Close_MC_TMP_GateValve();
					break;
				case g_pIO->DO::MC_FORELINE:
					if (nCheck)
						nRet = g_pIO->Open_MC_TMP_ForelineValve();
					else
						nRet = g_pIO->Close_MC_TMP_ForelineValve();
					break;
				case g_pIO->DO::LLC_GATE_VALVE_OPEN_STATUS:
					if (nCheck)
						nRet = g_pIO->Open_LLCGateValve();
					else
						nRet = g_pIO->Close_LLCGateValve();
					break;
				case g_pIO->DO::LLC_GATE_VALVE_CLOSE_STATUS:
					if (nCheck)
						nRet = g_pIO->Close_LLCGateValve();
					else
						nRet = g_pIO->Open_LLCGateValve();
					break;
				case g_pIO->DO::TR_GATE_VALVE_OPEN_STATUS:
					if (nCheck)
						nRet = g_pIO->Open_TRGateValve();
					else
						nRet = g_pIO->Close_TRGateValve();
					break;
				case g_pIO->DO::TR_GATE_VALVE_CLOSE_STATUS:
					if (nCheck)
						nRet = g_pIO->Close_TRGateValve();
					else
						nRet = g_pIO->Open_TRGateValve();
					break;
				case g_pIO->DO::SLOW_VENT_INLET:
					if (nCheck)
						nRet = g_pIO->Open_SlowVent_Inlet_Valve();
					else
						nRet = g_pIO->Close_SlowVent_Inlet_Valve();
					break;
				case g_pIO->DO::SLOW_VENT_OUTLET:
					if (nCheck)
						nRet = g_pIO->Open_SlowVent_Outlet_Valve();
					else
						nRet = g_pIO->Close_SlowVent_Outlet_Valve();
					break;
				case g_pIO->DO::FAST_VENT_INLET:
					if (nCheck)
						nRet = g_pIO->Open_FastVent_Inlet_Valve();
					else
						nRet = g_pIO->Close_FastVent_Inlet_Valve();
					break;
				case g_pIO->DO::FAST_VENT_OUTLET:
					if (nCheck)
						nRet = g_pIO->Open_FastVent_Outlet_Valve();
					else
						nRet = g_pIO->Close_FastVent_Outlet_Valve();
					break;
				case g_pIO->DO::DigitalOutput_36:
					break;
				case g_pIO->DO::FRAME_COVER_DOOR_OPEN:
					if (nCheck)
						nRet = g_pIO->Open_CoverDoor();
					else
						nRet = g_pIO->Close_CoverDoor();
					break;
				case g_pIO->DO::EUV_SHUTTER_CHECK:
					break;
				case g_pIO->DO::DigitalOutput_39:
					break;
				case g_pIO->DO::DigitalOutput_40:
					break;
				case g_pIO->DO::DigitalOutput_41:
					break;
				case g_pIO->DO::UTILLITY_SPARE1:
					break;
				case g_pIO->DO::UTILLITY_SPARE2:
					break;
				case g_pIO->DO::INTERFACE_BOARD_SPARE1:
					break;
				case g_pIO->DO::INTERFACE_BOARD_SPARE2:
					break;
				case g_pIO->DO::DigitalOutput_46:
					break;
				case g_pIO->DO::DigitalOutput_47:
					break;
				case g_pIO->DO::DigitalOutput_48:
					break;
				case g_pIO->DO::DigitalOutput_49:
					break;
				case g_pIO->DO::DigitalOutput_50:
					break;
				case g_pIO->DO::DigitalOutput_51:
					break;
				case g_pIO->DO::DigitalOutput_52:
					break;
				case g_pIO->DO::DigitalOutput_53:
					break;
				case g_pIO->DO::LLC_SET_POINT_OUT_1_ATM:
					break;
				case g_pIO->DO::LLC_SET_POINT_OUT_2_VAC:
					break;
				case g_pIO->DO::MC_SET_POINT_OUT_1_ATM:
					break;
				case g_pIO->DO::MC_SET_POINT_OUT_2_VAC:
					break;
				case g_pIO->DO::DigitalOutput_58:
					break;
				case g_pIO->DO::DigitalOutput_59:
					break;
				case g_pIO->DO::DigitalOutput_60:
					break;
				case g_pIO->DO::DigitalOutput_61:
					break;
				case g_pIO->DO::DigitalOutput_62:
					break;
				case g_pIO->DO::DigitalOutput_63:
					break;
				default:
					break;
				}
				//////////////////////////////////////////////////////////////////////////////////
				//IO WRITE  ( 정상 WRITE 면 retrun 0 )
				//////////////////////////////////////////////////////////////////////////////////
				//nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck);
				///////////////////////////////////////////////////////////////////////////////////
				str_nRet.Format(_T("%d"), nRet);
				str.Format(_T("s"), g_pIO->Log_str);
				SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " Click Evnet Return Value :: " + str_nRet)));
				g_pLog->Display(0, "EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " Click Evnet Return Value :: " + str_nRet);
				if (nRet != OPERATION_COMPLETED)
				{
					::AfxMessageBox(str_IOString + str_EventState + "Write 실패! ", MB_ICONSTOP);
					SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " 실행 실패")));	//IO 버튼 Event 상태 기록.
					SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + str_IOMode + " ] " + str)));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " 실행 실패");
				}
				else
				{
					SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " 실행 완료")));	//IO 버튼 Event 상태 기록.
					g_pLog->Display(0, "EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " 실행 완료");
				}
			}
			else
			{
				SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + str_IOMode + " ] " + str_IOString + " 버튼 " + str_EventState + " 실행 하시겠습니까 ? (No)")));	//IO 버튼 Event 상태 기록.
			}
		}
		else
		{
			str_IOString = " Water_Valve_Open Error 발생 [ Water Supply, Return On 확인 ]";
			SaveLogFile("SREM__IO__Event", _T((LPSTR)(LPCTSTR)("EuvSolution :: [ " + str_IOMode + " ] " + str_IOString)));	//IO 버튼 Event 상태 기록.
			::AfxMessageBox(str_IOString, MB_ICONSTOP);
			return WATER_SUPPLY_ERROR;
		}
	}

	return nRet;
}




bool CDigitalOutput::OnUpdateDigitalOutput()
{

	for (int nIdx = m_nChannel * DIGITAL_IO_VIEW_NUMBER, nCnt = 0; nIdx < (m_nChannel * DIGITAL_IO_VIEW_NUMBER) + DIGITAL_IO_VIEW_NUMBER; nIdx++, nCnt++)
	{
		if (g_pIO->m_bDigitalOut[nIdx])
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_Y000 + nCnt))->SetIcon(m_LedIcon[1]);
			CButton* btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_Y000 + nCnt);
			btn->SetCheck(true);
			btn->SetWindowTextA(_T("On"));
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_Y000 + nCnt))->SetIcon(m_LedIcon[0]);
			CButton* btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_Y000 + nCnt);
			btn->SetCheck(false);
			btn->SetWindowTextA(_T("Off"));
		}
	}

	return TRUE;
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY000()
{
	SetOutputBitOnOff(0);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY001()
{
	SetOutputBitOnOff(1);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY002()
{
	SetOutputBitOnOff(2);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY003()
{
	SetOutputBitOnOff(3);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY004()
{
	SetOutputBitOnOff(4);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY005()
{
	SetOutputBitOnOff(5);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY006()
{
	SetOutputBitOnOff(6);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY007()
{
	SetOutputBitOnOff(7);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY008()
{
	SetOutputBitOnOff(8);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY009()
{
	SetOutputBitOnOff(9);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY010()
{
	SetOutputBitOnOff(10);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY011()
{
	SetOutputBitOnOff(11);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY012()
{
	SetOutputBitOnOff(12);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY013()
{
	SetOutputBitOnOff(13);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY014()
{
	SetOutputBitOnOff(14);
}


void CDigitalOutput::OnBnClickedCheckDigitaloutY015()
{
	SetOutputBitOnOff(15);
}

HBRUSH CDigitalOutput::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALOUT_TEXT)
		{
			pDC->SetBkColor(LIGHT_GRAY);
			//pDC->SetTextColor(RGB(0, 255, 0));
			return m_brush;
		}
	}

	return hbr;
}
