#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

/////////////////////////////////////////////////////////////////////////////
// CMaskMapWnd

CMaskMapWnd::CMaskMapWnd()
{
	m_nMicroscopyType = SCOPE_OM4X;
	m_bDrawAlignPoint = FALSE;
	m_bDrawMeasurePoint = FALSE;
	m_bDrawSelectedIDRect = FALSE;
	m_pMeasurePointRect=NULL;
	m_mSelectedMeasureID =-1;
	selcalibpos.x=0;
	selcalibpos.y=0;
	bOnCalib = false;
	m_realcenterx=m_realcentery=0;
	m_nPointSize=3;
	m_nTextSize = 12;
	m_bShowcode = FALSE;
	m_bSelZoomCheck = FALSE;
	m_MilMapDisplay = M_NULL;
	MbufAllocColor(g_milSystemHost, 3, 1024, 1024, 8L + M_UNSIGNED, M_IMAGE + M_PROC + M_DIB, &m_MilMapDisplay);

	m_nCurrentCouponIndex = -1;
	m_nCurrentPointIndex = -1;
}

CMaskMapWnd::~CMaskMapWnd()
{
	if(m_pMeasurePointRect!=NULL)
	{
		delete[] m_pMeasurePointRect;
		m_pMeasurePointRect=NULL;
	}
	if (m_MilMapDisplay != M_NULL)
	{
		MbufFree(m_MilMapDisplay);
		m_MilMapDisplay = M_NULL;
	}
}

BEGIN_MESSAGE_MAP(CMaskMapWnd, CWnd)
	//{{AFX_MSG_MAP(CMaskMapWnd)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMaskMapWnd message handlers

BOOL CMaskMapWnd::Create(DWORD dwStyle,RECT& rect, CWnd* pParentWnd) 
{
	// create window
	BOOL ret;
	static CString className = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW,AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	ret = CWnd::Create(className,NULL,dwStyle, rect,pParentWnd,0);

	rcMaskMap_pixel.left=0;
	rcMaskMap_pixel.top=0;
	rcMaskMap_pixel.right=rect.right-rect.left;
	rcMaskMap_pixel.bottom=rect.bottom-rect.top;

	SetTimer(MAP_UPDATE_TIMER, 500, 0);

	return ret;
}

void CMaskMapWnd::OnPaint() 
{
	CPaintDC dc(this);


	if (m_MilMapDisplay == M_NULL)
		return;

	HDC hDC;
	MbufControl(m_MilMapDisplay, M_DC_ALLOC, M_DEFAULT);
	hDC = (HDC)MbufInquire(m_MilMapDisplay, M_DC_HANDLE, M_NULL);

	DrawMaskBackground(&dc);

	//::BitBlt(dc.GetSafeHdc(), rcMaskMap_pixel.left, rcMaskMap_pixel.top, rcMaskMap_pixel.Width(), rcMaskMap_pixel.Height(), hDC, 0, 0, SRCCOPY);
	::StretchBlt(dc.GetSafeHdc(), rcMaskMap_pixel.left+45, rcMaskMap_pixel.top+18, rcMaskMap_pixel.Width()-85, rcMaskMap_pixel.Height()-33, hDC, 0, 0, 1024, 1024, SRCCOPY); //하이닉스마스크 이미지 최적화

	switch (g_pConfig->m_nEquipmentType)
	{
	case EUVPTR:
		if(m_ProcessData.GetCoupons().size() > 0)
			DrawPTRCouponJig(&dc);
		if (m_bDrawAlignPoint)
		{
			DrawMaskAlignPoint(&dc);
			//DrawDie(&dc);
		}
		break;
	default:
		break;
	}
	   
	if (m_bDrawMeasurePoint)
	{
		DrawPTRMeasurePoint(&dc);
		DrawBeamDiagnosisPoint(&dc);
	}

	if (m_bDrawSelectedIDRect)
	{
		if (g_pConfig->m_nEquipmentType == EUVPTR)
			DrawPTRSelectedIDRect(m_mSelectedMeasureID, &dc);
	}

	DrawNavigationStagePos(&dc);

	MbufControl(m_MilMapDisplay, M_DC_FREE, M_DEFAULT);
}

void CMaskMapWnd::DrawMaskBackground(CDC *pDC)
{
	SetBkMode(pDC->m_hDC, TRANSPARENT);
	CPen Pen(PS_SOLID, 1, DARK_GRAY), *pOldPen;
	pOldPen= pDC->SelectObject(&Pen);

	CRect rc = rcMaskMap_pixel;
	rc.DeflateRect(5,5,5,5);
	rc.right=(int)(rc.right);
	rc.bottom=(int)(rc.bottom);
	pDC->FillSolidRect(rc, BLACK);

	pDC->SelectObject(pOldPen);
	Pen.DeleteObject();


	CBrush Brush, *pOldBrush;
	Brush.CreateSolidBrush(BLACK);
	pOldBrush = pDC->SelectObject(&Brush);

	CPoint Point;
	Point.x=20;
	Point.y=20;
	pDC->RoundRect(rc, Point); //pDC->Ellipse(rc);

	pDC->SelectObject(pOldBrush);
	Brush.DeleteObject();
}

void CMaskMapWnd::DrawMaskAlignPoint(CDC *pDC)
{
	double centerx = 0, centery = 0;

	SetBkMode(pDC->m_hDC, TRANSPARENT);
	CPen oPen, *pOldPen;
	if(m_ProcessData.SubstrateID == "DNP_testsample")
		oPen.CreatePen(PS_SOLID, 4, BLACK);
	else
		oPen.CreatePen(PS_SOLID, 4, WHITE);
	//CPen oPen(PS_SOLID, 4, WHITE), *pOldPen;
	pOldPen = pDC->SelectObject(&oPen);

	CRect rect = rcMaskMap_pixel;
	rect.right = (int)(rect.left + rcMaskMap_pixel.Width());
	rect.bottom = (int)(rect.top + rcMaskMap_pixel.Height());

	centerx = rect.left + (rect.right - rect.left) / 2;
	centery = rect.top + (rect.bottom - rect.top) / 2;

	double realcenterx = 0, realcentery = 0;
	realcenterx = centerx;
	realcentery = centery;

	double refx = 0, refy = 0;
	refx = MicrometertoPixel(rcMaskMap_pixel.Width(), m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X);
	refy = MicrometertoPixel(rcMaskMap_pixel.Height(), m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y);

	realcenterx = refx;
	realcentery = rcMaskMap_pixel.Height() - refy;

	double spotsize = 15;
	pDC->MoveTo((int)(realcenterx), (int)(realcentery - spotsize));
	pDC->LineTo((int)(realcenterx), (int)(realcentery));
	pDC->LineTo((int)(realcenterx + spotsize), (int)(realcentery));

	double align1x = 0, align1y = 0;
	align1x = MicrometertoPixel(rcMaskMap_pixel.Width(), m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].X);
	align1y = MicrometertoPixel(rcMaskMap_pixel.Height(), m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y);

	realcenterx = align1x;
	realcentery = rcMaskMap_pixel.Height() - align1y;

	pDC->MoveTo((int)(realcenterx), (int)(realcentery + spotsize ));
	pDC->LineTo((int)(realcenterx), (int)(realcentery));
	pDC->LineTo((int)(realcenterx + spotsize ), (int)(realcentery));

	double align2x = 0, align2y = 0;
	align2x = MicrometertoPixel(rcMaskMap_pixel.Width(), m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X);
	align2y = MicrometertoPixel(rcMaskMap_pixel.Height(), m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].Y);

	realcenterx = align2x;
	realcentery = rcMaskMap_pixel.Height() - align2y;

	pDC->MoveTo((int)(realcenterx), (int)(realcentery + spotsize));
	pDC->LineTo((int)(realcenterx), (int)(realcentery));
	pDC->LineTo((int)(realcenterx - spotsize), (int)(realcentery));

	if (m_ProcessData.TotalAlignNum == 4)
	{
		double align3x = 0, align3y = 0;
		align3x = MicrometertoPixel(rcMaskMap_pixel.Width(), m_ProcessData.m_stAlignmentPoint_um[RIGHT_BOTTOM].X);
		align3y = MicrometertoPixel(rcMaskMap_pixel.Height(), m_ProcessData.m_stAlignmentPoint_um[RIGHT_BOTTOM].Y);

		realcenterx = align3x;
		realcentery = rcMaskMap_pixel.Height() - align3y;

		pDC->MoveTo((int)(realcenterx), (int)(realcentery - spotsize));
		pDC->LineTo((int)(realcenterx), (int)(realcentery));
		pDC->LineTo((int)(realcenterx - spotsize), (int)(realcentery));
	}

	pDC->SelectObject(pOldPen);
	oPen.DeleteObject();
}

void CMaskMapWnd::DrawPTRCouponJig(CDC *pDC)
{
	CBrush hBrush, *pOldbrush;
	hBrush.CreateSolidBrush(PURPLE);
	pOldbrush = pDC->SelectObject(&hBrush);

	COLORREF jigColor = DARK_GRAY;

	for (auto coupon : m_ProcessData.GetCoupons())
	{
		CRect rcJig;
		rcJig.left = MicrometertoPixel(rcMaskMap_pixel.Width(), MaskSize_mm * 1000 / 2 + coupon.CenterPosX - coupon.Width / 2);
		rcJig.top = MicrometertoPixel(rcMaskMap_pixel.Height(), MaskSize_mm * 1000 / 2 - coupon.CenterPosY - coupon.Height / 2);
		rcJig.right = MicrometertoPixel(rcMaskMap_pixel.Width(), MaskSize_mm * 1000 / 2 + coupon.CenterPosX + coupon.Width / 2);
		rcJig.bottom = MicrometertoPixel(rcMaskMap_pixel.Height(), MaskSize_mm * 1000 / 2 - coupon.CenterPosY + coupon.Height / 2);
		pDC->FillSolidRect(rcJig, jigColor);
	}

	pDC->SelectObject(pOldbrush);
	hBrush.DeleteObject();
}

//void CMaskMapWnd::DrawDie(CDC *pDC)
//{
//	CBrush hBrush, *pOldbrush;
//	SetBkMode(pDC->m_hDC, TRANSPARENT);
//	hBrush.CreateSolidBrush(GRAY);;
//	pOldbrush = pDC->SelectObject(&hBrush);
//
//	int left, top, right, bottom;
//	CRect rc;
//	CRect rect = rcMaskMap_pixel;
//	rect.right = rect.left + rcMaskMap_pixel.Width();
//	rect.bottom = rect.top + rcMaskMap_pixel.Height();
//
//	double diex, diey;
//	diex = MicrometertoPixel(rcMaskMap_pixel.Width(), m_ProcessData.m_stDiePitch.X);
//	diey = MicrometertoPixel(rcMaskMap_pixel.Height(), m_ProcessData.m_stDiePitch.Y);
//	double diesizex, diesizey;
//	diesizex = MicrometertoPixel(rcMaskMap_pixel.Width(), m_ProcessData.DieInformation.DieSizeX);
//	diesizey = MicrometertoPixel(rcMaskMap_pixel.Height(), m_ProcessData.DieInformation.DieSizeY);
//
//	if (diex <= 0)
//	{
//		return;
//		diex = 20;
//	}
//
//	if (diey <= 0)
//	{
//		return;
//		diey = 20;
//	}
//
//	double refx = 0, refy = 0;
//	refx = MicrometertoPixel(rcMaskMap_pixel.Width(), m_ProcessData.m_stOriginCoordinate_um.X);
//	refy = MicrometertoPixel(rcMaskMap_pixel.Height(), m_ProcessData.m_stOriginCoordinate_um.Y);
//
//	double realcenterx = 0, realcentery = 0;
//	realcenterx = refx + MicrometertoPixel(rcMaskMap_pixel.Width(), m_ProcessData.m_stDieOriginPos_um.X);
//	realcentery = refy + MicrometertoPixel(rcMaskMap_pixel.Height(), m_ProcessData.m_stDieOriginPos_um.Y);
//	m_realcenterx = realcenterx;
//	m_realcentery = realcentery;
//
//	int i, j;
//	for (j = 0; j < m_ProcessData.DieInformation.nDieY; j++)
//	{
//		for (i = 0; i < m_ProcessData.DieInformation.nDieX; i++)
//		{
//			left = (int)(realcenterx + diex * i);
//			top = (int)(realcentery + diey + diey * j);
//			right = (int)(realcenterx + diex + diex * i);
//			bottom = (int)(realcentery + diey * j);
//			rc = CRect(left, (rcMaskMap_pixel.Height() - top), right + 1, (rcMaskMap_pixel.Height() - bottom) + 1);
//			pDC->FrameRect(rc, &hBrush);
//		}
//	}
//
//	pDC->SelectObject(pOldbrush);
//	hBrush.DeleteObject();
//}

void CMaskMapWnd::DrawPTRMeasurePoint(CDC *pDC)
{
	double dRefOriginX_pixel = 0, dRefOriginY_pixel = 0;
	dRefOriginX_pixel = MicrometertoPixel(rcMaskMap_pixel.Width(), m_ProcessData.m_stOriginCoordinate_um.X);
	dRefOriginY_pixel = MicrometertoPixel(rcMaskMap_pixel.Height(), m_ProcessData.m_stOriginCoordinate_um.Y);

	if (m_pMeasurePointRect != NULL)
	{
		delete[] m_pMeasurePointRect;
		m_pMeasurePointRect = NULL;
	}
	if (m_ProcessData.TotalMeasureNum > 0)
		m_pMeasurePointRect = new RECT[m_ProcessData.TotalMeasureNum];

	double dRefCoordX_pixel, dRefCoordY_pixel;
	double realxdefect, realydefect;
	double dMaskCoordX_pixel, dMaskCoordY_pixel;
	int i;
	int rectsize = 4;


	if (m_ProcessData.TotalMeasureNum > 0)
	{
		i = 0;
		for (auto coupon : m_ProcessData.GetCoupons())
		{
			for (auto point : coupon.GetPoints())
			{
				dRefCoordX_pixel = MicrometertoPixel(rcMaskMap_pixel.Width(), coupon.CenterPosX + point.CenterPosX);
				dRefCoordY_pixel = MicrometertoPixel(rcMaskMap_pixel.Height(), coupon.CenterPosY + point.CenterPosY);
				realxdefect = dRefOriginX_pixel + dRefCoordX_pixel;
				realydefect = dRefOriginY_pixel + dRefCoordY_pixel;
				dMaskCoordX_pixel = realxdefect;
				dMaskCoordY_pixel = rcMaskMap_pixel.Height() - realydefect;

				CRect temprc;
				temprc.left = (long)(dMaskCoordX_pixel - 1);
				temprc.top = (long)(dMaskCoordY_pixel - 1);
				temprc.right = (long)(dMaskCoordX_pixel + 1);
				temprc.bottom = (long)(dMaskCoordY_pixel + 1);
				m_pMeasurePointRect[i] = temprc;
				m_pMeasurePointRect[i].left -= rectsize;
				m_pMeasurePointRect[i].top -= rectsize;
				m_pMeasurePointRect[i].right += rectsize;
				m_pMeasurePointRect[i].bottom += rectsize;

				CPen oPen(PS_SOLID, m_nPointSize, RED), *pOldPen;
				pOldPen = pDC->SelectObject(&oPen);

				CBrush hBrush, *pOldbrush;
				hBrush.CreateSolidBrush(RED);
				pOldbrush = pDC->SelectObject(&hBrush);

				pDC->Rectangle(temprc);

				pDC->SelectObject(pOldPen);
				oPen.DeleteObject();
				pDC->SelectObject(pOldbrush);
				hBrush.DeleteObject();
				i++;
			}
		}
	}
}

void CMaskMapWnd::DrawBeamDiagnosisPoint(CDC *pDC)
{
	double dRefOriginX_pixel = 0, dRefOriginY_pixel = 0;
	dRefOriginX_pixel = MicrometertoPixel(rcMaskMap_pixel.Width(), m_ProcessData.m_stOriginCoordinate_um.X);
	dRefOriginY_pixel = MicrometertoPixel(rcMaskMap_pixel.Height(), m_ProcessData.m_stOriginCoordinate_um.Y);

	double dPixelx = 0.0, dPixely = 0.0;
	dPixelx = MicrometertoPixel(rcMaskMap_pixel.Width(), m_ProcessData.m_stBeamDiagnosisPos_um.X);
	dPixely = MicrometertoPixel(rcMaskMap_pixel.Height(), m_ProcessData.m_stBeamDiagnosisPos_um.Y);

	double realxdefect, realydefect;
	realxdefect = dRefOriginX_pixel + dPixelx;
	realydefect = dRefOriginY_pixel + dPixely;

	double dMaskCoordX_pixel, dMaskCoordY_pixel;
	dMaskCoordX_pixel = realxdefect;
	dMaskCoordY_pixel = rcMaskMap_pixel.Height() - realydefect;

	CRect temprc;
	temprc.left = (long)(dMaskCoordX_pixel - 1);
	temprc.top = (long)(dMaskCoordY_pixel - 1);
	temprc.right = (long)(dMaskCoordX_pixel + 1);
	temprc.bottom = (long)(dMaskCoordY_pixel + 1);

	CPen oPen(PS_SOLID, m_nPointSize, RED), *pOldPen;
	pOldPen = pDC->SelectObject(&oPen);

	CBrush hBrush, *pOldbrush;
	hBrush.CreateSolidBrush(RED);
	pOldbrush = pDC->SelectObject(&hBrush);

	pDC->Rectangle(temprc);

	pDC->SelectObject(pOldPen);
	oPen.DeleteObject();
	pDC->SelectObject(pOldbrush);
	hBrush.DeleteObject();


	HFONT font, oldfont;
	font = CreateFont(m_nTextSize, 0, 0, 0, FW_BOLD, 0, 0, 0, DEFAULT_CHARSET, 0, 0, 0, 0, NULL);
	oldfont = (HFONT)pDC->SelectObject(font);
	pDC->SetTextColor(YELLOW);

	CString str;
	str.Format("(%.3f,%.3f)", m_ProcessData.m_stBeamDiagnosisPos_um.X / 1000., m_ProcessData.m_stBeamDiagnosisPos_um.Y / 1000.);
	CSize size = pDC->GetTextExtent(str);
	pDC->TextOut(temprc.left, temprc.bottom, str, str.GetLength());

	pDC->SelectObject(oldfont);
	DeleteObject(font);

}

void CMaskMapWnd::DrawPTRSelectedIDRect(int i, CDC *pDC)
{
	if (i >= 0 && i < m_ProcessData.TotalMeasureNum)
	{
		CBrush hBrush, *pOldbrush;
		hBrush.CreateSolidBrush(PURPLE);
		pOldbrush = pDC->SelectObject(&hBrush);

		CRect rc;
		rc.left = m_pMeasurePointRect[i].left;
		rc.right = m_pMeasurePointRect[i].right;
		rc.top = m_pMeasurePointRect[i].top;
		rc.bottom = m_pMeasurePointRect[i].bottom;

		pDC->FrameRect(&rc, &hBrush);
		hBrush.DeleteObject();
		pDC->SelectObject(pOldbrush);
	}
}

BOOL CMaskMapWnd::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(m_ProcessData.TotalMeasureNum > 0)
		{
			switch (pMsg->wParam)
			{
				case VK_RETURN:  
				case VK_RIGHT:
				case VK_LEFT:
				case VK_NUMPAD0:
				case VK_NUMPAD1:
				case VK_NUMPAD2:
				case VK_NUMPAD3:
				case VK_NUMPAD4:
				case VK_NUMPAD5:
				case VK_NUMPAD6:
				case VK_NUMPAD7:
				case VK_NUMPAD8:
				case VK_NUMPAD9:
				default:
					break;
			}
		}
	}	
	return CWnd::PreTranslateMessage(pMsg);
}

void CMaskMapWnd::OnTimer(UINT_PTR nIDEvent)
{
	int current_naviposx_submm = 0, current_naviposy_submm = 0;
	static int old_naviposx_submm = 0, old_naviposy_submm = 0;
	static int nstage_stop_time_sec = 0.0;
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case MAP_UPDATE_TIMER:
		if (g_pNavigationStage != NULL)
		{
			current_naviposx_submm = (int)(g_pNavigationStage->GetPosmm(STAGE_X_AXIS)*10.);
			current_naviposy_submm = (int)(g_pNavigationStage->GetPosmm(STAGE_Y_AXIS)*10.);
			if (abs(old_naviposx_submm - current_naviposx_submm) > 0 || abs(old_naviposy_submm - current_naviposy_submm) > 0)
			{
				old_naviposx_submm = current_naviposx_submm;
				old_naviposy_submm = current_naviposy_submm;
				Invalidate(FALSE);
				nstage_stop_time_sec = 0.0;
			}
			else
			{
				if (g_pEUVSource->Is_EUV_On() == TRUE)
				{
					nstage_stop_time_sec++;
					if (nstage_stop_time_sec > g_pConfig->m_nSourceAutoOffTime_min * 60 * 2)	//일정 시간 이상 stage 멈춰있으면, Min -> Sec 곱하기 60초 타이머가 0.5초마다 호출되니 곱하기 2
					{
						if (!g_pMaskMap->m_bAutoSequenceProcessing) // Autoprocess 아닐때..Autoprocess면 시간도 초기화
						{
							g_pEUVSource->SetMechShutterOpen(FALSE);
							g_pEUVSource->SetEUVSourceOn(FALSE);
							//g_pScanStage->Move_Origin();

							if (g_pLog != NULL)
								g_pLog->Display(0, _T("CMaskMapWnd::OnTimer 스테이지를 일정시간 구동하지 않아서 EUV STOP!"));

							AfxMessageBox(_T("스테이지를 일정시간 구동하지 않아서 자동으로 EUV OFF, 스캔 스테이지 오리진 위치로 이동되었습니다."));
						}
						nstage_stop_time_sec = 0.0;
					}
				}
				else
					nstage_stop_time_sec = 0.0;
			}
		}
		SetTimer(nIDEvent, 500, 0);
		break;
	default:
		break;
	}
	CWnd::OnTimer(nIDEvent);
}

void CMaskMapWnd::DrawNavigationStagePos(CDC *pDC)
{	
	CPen oPen, *pOldPen;
	oPen.CreatePen(PS_DASHDOT, 1, RED);
	//CPen oPen(PS_DASHDOT, 1, WHITE), *pOldPen;
	pOldPen=pDC->SelectObject(&oPen);

	int crosssize = 16;

	// Stage의 현재 좌표 구하기.
	double current_naviposx_mm = 0.0, current_naviposy_mm = 0.0;
	current_naviposx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	current_naviposy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

	// Mask LB 좌표로 변환
	double current_maskposx_mm = 0.0, current_maskposy_mm = 0.0;
	double aa = 0.0, bb = 0.0, cc = 0.0, dd = 0.0;

	switch (g_pConfig->m_nEquipmentType)
	{
	default:
		if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == SCOPE_OM4X && g_pMaskMap->m_bOMAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
		{
			aa = (g_pMaskMap->m_dOMAlignPointRT_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			bb = (g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			cc = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			dd = (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			current_maskposx_mm = (bb*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - current_naviposy_mm) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (current_naviposx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000.;
			current_maskposy_mm = (aa*((g_pMaskMap->m_dOMAlignPointLT_posy_mm - current_naviposy_mm) - (g_pMaskMap->m_dOMAlignPointLT_posy_mm - g_pMaskMap->m_dOMAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm) - (current_naviposx_mm - g_pMaskMap->m_dOMAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000.;
		}
		else if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV && g_pMaskMap->m_bEUVAlignComplete == TRUE && g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X != 0.0)
		{
			aa = (g_pMaskMap->m_dEUVAlignPointRT_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			bb = (g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			cc = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointRT_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[RIGHT_TOP].X - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X) / 1000.);
			dd = (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm) / ((g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[LEFT_TOP].Y - g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y) / 1000.);
			current_maskposx_mm = (bb*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - current_naviposy_mm) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + dd * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (current_naviposx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].X / 1000.;
			current_maskposy_mm = (aa*((g_pMaskMap->m_dEUVAlignPointLT_posy_mm - current_naviposy_mm) - (g_pMaskMap->m_dEUVAlignPointLT_posy_mm - g_pMaskMap->m_dEUVAlignPointLB_posy_mm)) + cc * ((g_pMaskMap->m_dEUVAlignPointLB_posx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm) - (current_naviposx_mm - g_pMaskMap->m_dEUVAlignPointLT_posx_mm))) / (bb*cc - aa * dd) + g_pMaskMap->m_MaskMapWnd.m_ProcessData.m_stAlignmentPoint_um[REFERENCE].Y / 1000.;
		}
		else
		{
			if (g_pMaskMap->m_MaskMapWnd.m_nMicroscopyType == EUV)
			{
				current_maskposx_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].x - current_naviposx_mm;
				current_maskposy_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].y - current_naviposy_mm;
			}
			else
			{
				current_maskposx_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - current_naviposx_mm;
				current_maskposy_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - current_naviposy_mm;

			}
		}

		break;
	}
	
	//double maskcenterx_pixel = 0.0, maskcentery_pixel = 0.0;
	//maskcenterx_pixel = MicrometertoPixel(rcMaskMap_pixel.Width(),m_ProcessData.m_stReferenceCoord_um.X);
	//maskcentery_pixel = MicrometertoPixel(rcMaskMap_pixel.Height(),m_ProcessData.m_stReferenceCoord_um.Y);

	// Mask 좌표 pixel 위치 구하기
	int maskx_pixel = 0, masky_pixel = 0;
	maskx_pixel = (int)MicrometertoPixel(rcMaskMap_pixel.Width(), current_maskposx_mm*1000.0);
	masky_pixel = (int)(rcMaskMap_pixel.Height() - MicrometertoPixel(rcMaskMap_pixel.Height(), current_maskposy_mm*1000.0));
	
	// Stage위치를 마스크 좌표상에 그리기
	pDC->MoveTo((int)maskx_pixel,(int)(masky_pixel - crosssize));
	pDC->LineTo((int)maskx_pixel,(int)(masky_pixel + crosssize));
	pDC->MoveTo((int)(maskx_pixel - crosssize),(int)masky_pixel);
	pDC->LineTo((int)(maskx_pixel + crosssize),(int)masky_pixel);
	//pDC->MoveTo((int)20, (int)(20 - crosssize));
	//pDC->LineTo((int)20, (int)(20 + crosssize));
	//pDC->MoveTo((int)(20 - crosssize), (int)20);
	//pDC->LineTo((int)(20 + crosssize), (int)20);

	pDC->SelectObject(pOldPen);
	oPen.DeleteObject();
}

void CMaskMapWnd::OnRButtonDown(UINT nFlags, CPoint point)
{
	//물류 동작중이거나 마스크 없을 때는 못움직이게 막자

	//1.클릭한 Pixel 좌표를 Mask 좌표로 변환
	double target_maskposx_mm = 0.0, target_maskposy_mm = 0.0;
	target_maskposx_mm = PixeltoMicrometer(rcMaskMap_pixel.Width(), point.x) / 1000.;
	target_maskposy_mm = PixeltoMicrometer(rcMaskMap_pixel.Height(), rcMaskMap_pixel.Height() - point.y) / 1000.;

	//refx = target_maskposx_mm - m_ProcessData.m_stReferenceCoord_um.X / 1000.;
	//refy = target_maskposy_mm - m_ProcessData.m_stReferenceCoord_um.Y / 1000.;

	// Stage의 현재 좌표 구하기.
	//double current_naviposx_mm = 0.0, current_naviposy_mm = 0.0;
	//current_naviposx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	//current_naviposy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

	// Mask 좌표를 스테이지 좌표로 변환
	double target_posx_mm = 0.0, target_posy_mm = 0.0;
	if (m_nMicroscopyType == EUV)
	{
		target_posx_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].x - target_maskposx_mm;
		target_posy_mm = g_pConfig->m_stStagePos[SYSTEM_EUV_MASKLEFTDOWN_POSITION].y - target_maskposy_mm;
	}
	else
	{
		target_posx_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - target_maskposx_mm;
		target_posy_mm = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - target_maskposy_mm;

	}

	if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
	{
		//g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
		if (g_pConfig->m_nEquipmentType == PHASE)
			g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm, FALSE);
		else
			g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
	}

	CWnd::OnRButtonDown(nFlags, point);
}

void CMaskMapWnd::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (m_bSelZoomCheck)
	{
		startpt = endpt = point;
		//Invalidate(false);
	}

	startpt = endpt = point;
	CRect rc;
	int i = 0;
	int index = 0;
	if (m_pMeasurePointRect != NULL)
	{
		for (i = 0; i < m_ProcessData.TotalMeasureNum; i++)
		{
			CPoint pt;
			rc = m_pMeasurePointRect[i];
			pt.x = point.x;
			pt.y = point.y;

			if (rc.PtInRect(pt))
			{
				bool bExitFlag = false;

				for (int x = 0; x < m_ProcessData.GetCoupons().size(); x++)
				{
					_CouponList coupon = m_ProcessData.GetCoupons()[x];

					for (int y = 0; y < coupon.GetPoints().size(); y++)
					{
						if (i == index)
						{
							_MeasurePointList point = coupon.GetPoints()[y];

							double dMeasurePosX_um, dMeasurePosY_um;
							dMeasurePosX_um = coupon.CenterPosX + point.CenterPosX;
							dMeasurePosY_um = coupon.CenterPosY + point.CenterPosY;

							m_mSelectedMeasureID = i;
							m_nCurrentCouponIndex = x;
							m_nCurrentPointIndex  = y;

							if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
								g_pNavigationStage->MoveToSelectedPointNo(dMeasurePosX_um, dMeasurePosY_um);
							bExitFlag = true;
							break;
						}
						index++;
					}

					if (bExitFlag == true) break;
				}
			}
		}
	}
	Invalidate(FALSE);

	CWnd::OnLButtonDown(nFlags, point);
}

void CMaskMapWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CWnd::OnLButtonUp(nFlags, point);
}

void CMaskMapWnd::OnMouseMove(UINT nFlags, CPoint point) 
{

	CWnd::OnMouseMove(nFlags, point);
}

double CMaskMapWnd::PixeltoMicrometer(int pixel, int value)
{
	if (pixel == 0)
		return 0.0f;

	return ((MaskSize_mm*1000.*value) / (double)pixel);
}

double CMaskMapWnd::MicrometertoPixel(int pixel, double value)
{
	if (MaskSize_mm == 0)
		return 0.0f;

	return (((double)pixel*value) / (MaskSize_mm*1000.));
}

int CMaskMapWnd::LenTwoPoint(int x1, int y1, int x2, int y2)
{
	return ((int)sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2)));
}


/*convert to long rainbow RGB*/
//var a = (1 - f) / 0.2;
//var X = Math.floor(a);
//var Y = Math.floor(255 * (a - X));
//switch (X)
//{
//case 0: r = 255; g = Y; b = 0; break;
//case 1: r = 255 - Y; g = 255; b = 0; break;
//case 2: r = 0; g = 255; b = Y; break;
//case 3: r = 0; g = 255 - Y; b = 255; break;
//case 4: r = Y; g = 0; b = 255; break;
//case 5: r = 255; g = 0; b = 255; break;
//}
//ctx.fillStyle = "rgb(" + r + "," + g + "," + b + ")";
//ctx.fillRect(i, 60, 1, 20);
void CMaskMapWnd::ConvertToShortRainbowColor(int nMilSource, int nMilTarget)
{
	if (nMilSource == M_NULL || nMilTarget == M_NULL)
		return;

	int nDestDepth = MbufInquire(nMilTarget, M_SIZE_BIT, M_NULL);
	int nDestMaxValue = (1 << nDestDepth) - 1;
	MIL_ID milLookup[3];	// RGB
	int i = 0, j = 0;
	for (j = 0; j < 3; j++)
	{
		milLookup[j] = MbufAlloc1d(g_milSystemHost, (int)nDestMaxValue + 1, ((nDestDepth > 8) ? 16 : 8) + M_UNSIGNED, M_LUT, M_NULL);
	}

	unsigned char nRgb[3];
	
	float dividemax = 255 / 4.;
	for (i = 0; i <= nDestMaxValue; i++)
	{
		if (i < dividemax)
		{
			nRgb[0] = 0;
			nRgb[1] = 0;
			nRgb[2] = 150 + (unsigned char)((255 - 150) * i / dividemax);
		}
		else if (i < 2 * dividemax)
		{
			nRgb[0] = 0;
			nRgb[1] = (unsigned char)(255 * (i - dividemax) / dividemax);
			nRgb[2] = 255;
		}
		else if (i < 3 * dividemax)
		{
			nRgb[0] = (unsigned char)(255 * (i - 2 * dividemax) / dividemax);
			nRgb[1] = 255;
			nRgb[2] = 255 - nRgb[0];
		}
		else if (i < 255)
		{
			nRgb[0] = 255;
			nRgb[1] = (unsigned char)(255 - 255 * (i - 3 * dividemax) / dividemax);
			nRgb[2] = 0;
		}
		else
		{
			nRgb[0] = 255;
			nRgb[1] = nRgb[2] = 0;
		}

		for (j = 0; j < 3; j++)
		{
			MgenLutRamp(milLookup[j], i, nRgb[j], i, nRgb[j]);
		}
	}

	MIL_ID milDestMono;
	for (j = 0; j < 3; j++)
	{
		milDestMono = MbufChildColor(nMilTarget, j, M_NULL);
		MimLutMap(nMilSource, milDestMono, milLookup[j]);
		MbufFree(milDestMono);
	}
	for (j = 0; j < 3; j++)
	{
		MbufFree(milLookup[j]);
	}
	return;
}

//void jetColorMap(unsigned char *rgb, float value, float min, float max)
//{
//	unsigned char c1 = 144;
//	float max4 = (max - min) / 4;
//	value -= min;
//
//	if (value == HUGE_VAL)
//	{
//		rgb[0] = rgb[1] = rgb[2] = 255;
//	}
//	else if (value < 0)
//	{
//		rgb[0] = rgb[1] = rgb[2] = 0;
//	}
//	else if (value < max4)
//	{
//		rgb[0] = 0;
//		rgb[1] = 0;
//		rgb[2] = c1 + (unsigned char)((255 - c1) * value / max4);
//	}
//	else if (value < 2 * max4)
//	{
//		rgb[0] = 0;
//		rgb[1] = (unsigned char)(255 * (value - max4) / max4);
//		rgb[2] = 255;
//	}
//	else if (value < 3 * max4)
//	{
//		rgb[0] = (unsigned char)(255 * (value - 2 * max4) / max4);
//		rgb[1] = 255;
//		rgb[2] = 255 - rgb[0];
//	}
//	else if (value < max)
//	{
//		rgb[0] = 255;
//		rgb[1] = (unsigned char)(255 - 255 * (value - 3 * max4) / max4);
//		rgb[2] = 0;
//	}
//	else
//	{
//		rgb[0] = 255;
//		rgb[1] = rgb[2] = 0;
//	}
//}
//
//void hotColorMap(unsigned char *rgb, float value, float min, float max)
//{
//	float max3 = (max - min) / 3;
//	value -= min;
//
//	if (value == HUGE_VAL)
//	{
//		rgb[0] = rgb[1] = rgb[2] = 255;
//	}
//	else if (value < 0)
//	{
//		rgb[0] = rgb[1] = rgb[2] = 0;
//	}
//	else if (value < max3)
//	{
//		rgb[0] = (unsigned char)(255 * value / max3);
//		rgb[1] = 0;
//		rgb[2] = 0;
//	}
//	else if (value < 2 * max3)
//	{
//		rgb[0] = 255;
//		rgb[1] = (unsigned char)(255 * (value - max3) / max3);
//		rgb[2] = 0;
//	}
//	else if (value < max)
//	{
//		rgb[0] = 255;
//		rgb[1] = 255;
//		rgb[2] = (unsigned char)(255 * (value - 2 * max3) / max3);
//	}
//	else
//	{
//		rgb[0] = rgb[1] = rgb[2] = 255;
//	}
//}
//
//void coldColorMap(unsigned char *rgb, float value, float min, float max)
//{
//	float max3 = (max - min) / 3;
//	value -= min;
//
//	if (value == HUGE_VAL)
//	{
//		rgb[0] = rgb[1] = rgb[2] = 255;
//	}
//	else if (value < 0)
//	{
//		rgb[0] = rgb[1] = rgb[2] = 0;
//	}
//	else if (value < max3)
//	{
//		rgb[0] = 0;
//		rgb[1] = 0;
//		rgb[2] = (unsigned char)(255 * value / max3);
//	}
//	else if (value < 2 * max3)
//	{
//		rgb[0] = 0;
//		rgb[1] = (unsigned char)(255 * (value - max3) / max3);
//		rgb[2] = 255;
//	}
//	else if (value < max)
//	{
//		rgb[0] = (unsigned char)(255 * (value - 2 * max3) / max3);
//		rgb[1] = 255;
//		rgb[2] = 255;
//	}
//	else
//	{
//		rgb[0] = rgb[1] = rgb[2] = 255;
//	}
//}