#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

IMPLEMENT_DYNAMIC(CLoadingScreenDlg, CDialogEx)

CLoadingScreenDlg::CLoadingScreenDlg(CWnd* pParent)
	:CDialogEx(IDD_LOADING_SCREEN_DIALOG, pParent)
{
	m_sMessage = _T("");
	m_bLoadingComp = FALSE;
}

CLoadingScreenDlg::~CLoadingScreenDlg()
{

}

void CLoadingScreenDlg::DoDataExchange(CDataExchange* pDX)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SPLASH, m_pic);
	//DDX_Control(pDX, IDC_LOADING_STATIC, m_TextCtrl);
	DDX_Control(pDX, IDC_LOADING_TITLE_STATIC, m_TitleCtrl);
}


BOOL CLoadingScreenDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_ADAM_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_CREVIS_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_MTS_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_VMTR_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_CAMERA_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_NAVISTAGE_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_SOURCE_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_GAUGE_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_LLCTMP_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_MCTMP_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_FLTSTAGE_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_LIGHT_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_MTS_HOME))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_VMTR_HOME))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SPLASH_NAVISTAGE_HOME))->SetIcon(m_LedIcon[0]);
	//SetDlgItemText(IDC_LOADING_TITLE_STATIC, _T("SREM Loading ...."));

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	//CRect rectPic, rectWin;
	//GetWindowRect(&rectWin);
	//m_pic.GetWindowRect(&rectPic);
	//MoveWindow(rectWin.left, rectWin.top, rectPic.Width(), rectPic.Height());

	g_pLoadingScreen.SetTimer(1, 500, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
BEGIN_MESSAGE_MAP(CLoadingScreenDlg, CDialogEx)
	ON_WM_TIMER()
END_MESSAGE_MAP()

void CLoadingScreenDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if(m_bLoadingComp == TRUE)
		CDialog::OnOK();

	if (g_pAdam != NULL)
	{
		if (g_pAdam->Is_ADAM_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_ADAM_CONNECT))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_ADAM_CONNECT))->SetIcon(m_LedIcon[2]);
	}

	if (g_pCamera != NULL)
	{
		if (g_pCamera->Is_CAM_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_CAMERA_CONNECT))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_CAMERA_CONNECT))->SetIcon(m_LedIcon[2]);
	}

	if (g_pGauge_IO != NULL)
	{
		if (g_pGauge_IO->Is_GAUGE_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_GAUGE_CONNECT))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_GAUGE_CONNECT))->SetIcon(m_LedIcon[2]);
	}

	if (g_pIO != NULL)
	{
		if (g_pIO->Is_CREVIS_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_CREVIS_CONNECT))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_CREVIS_CONNECT))->SetIcon(m_LedIcon[2]);
	}

	if (g_pEfem != NULL)
	{
		if (g_pEfem->Is_MTS_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_MTS_CONNECT))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_MTS_CONNECT))->SetIcon(m_LedIcon[2]);

		if(g_pEfem->Is_LPM_Initialized() == TRUE && g_pEfem->Is_ATR_Initialized() == TRUE 
			&& g_pEfem->Is_Rotator_Initialized() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_MTS_HOME))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_MTS_HOME))->SetIcon(m_LedIcon[2]);
	}

	if (g_pNavigationStage != NULL)
	{
		if (g_pNavigationStage->Is_NAVI_Stage_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_NAVISTAGE_CONNECT))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_NAVISTAGE_CONNECT))->SetIcon(m_LedIcon[2]);
	}

	if (g_pFilterStage != NULL)
	{
		if (g_pFilterStage->Is_FILTER_Stage_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_FLTSTAGE_CONNECT))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_FLTSTAGE_CONNECT))->SetIcon(m_LedIcon[2]);
	}

	if (g_pMCTmp_IO != NULL)
	{
		if (g_pMCTmp_IO->Is_MC_Tmp_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_MCTMP_CONNECT))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_MCTMP_CONNECT))->SetIcon(m_LedIcon[2]);
	}

	if (g_pLLCTmp_IO != NULL)
	{
		if (g_pLLCTmp_IO->Is_LLC_Tmp_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_LLCTMP_CONNECT))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_LLCTMP_CONNECT))->SetIcon(m_LedIcon[2]);
	}

	if (g_pVacuumRobot != NULL)
	{
		if (g_pVacuumRobot->Is_VMTR_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_VMTR_CONNECT))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_VMTR_CONNECT))->SetIcon(m_LedIcon[2]);

		if(g_pVacuumRobot->Is_VMTR_Home() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_VMTR_HOME))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_VMTR_HOME))->SetIcon(m_LedIcon[2]);
	}

	if (g_pEUVSource != NULL)
	{
		if (g_pEUVSource->Is_SRC_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_SOURCE_CONNECT))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_SOURCE_CONNECT))->SetIcon(m_LedIcon[2]);
	}

	if (g_pLightCtrl != NULL)
	{
		if (g_pLightCtrl->Is_Connected() == TRUE)
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_LIGHT_CONNECT))->SetIcon(m_LedIcon[1]);
		else
			((CStatic*)GetDlgItem(IDC_ICON_SPLASH_LIGHT_CONNECT))->SetIcon(m_LedIcon[2]);
	}
}

void CLoadingScreenDlg::SetTextMessage(CString sMsg)
{
	//m_sMessage = sMsg;
}

void CLoadingScreenDlg::SetTitleMessage(CString sMsg)
{
	m_TitleCtrl.SetWindowText(sMsg);
}

void CLoadingScreenDlg::SetLoadingComplete()
{
	m_bLoadingComp = TRUE;
}
