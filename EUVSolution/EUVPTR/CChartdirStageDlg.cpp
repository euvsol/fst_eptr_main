﻿// CChartdirStageDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include <math.h>
#include <iomanip>

#define GETDATA 1
#define DATACLEAR 2
#define UPDATE	3

//g_pNavigationStage->RunBuffer(6);
//double realvalue1 = 0.0, realvalue2 = 0.0;
//realvalue1 = g_pNavigationStage->GetGlobalRealVariable("DOUT_0");
//realvalue2 = g_pNavigationStage->GetGlobalRealVariable("DOUT_1");
//g_pNavigationStage->StopBuffer(6);


// CChartdirStageDlg 대화 상자

IMPLEMENT_DYNAMIC(CChartdirStageDlg, CDialogEx)

CChartdirStageDlg::CChartdirStageDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_STAGE_CHART_DIALOG, pParent)
{

}

CChartdirStageDlg::~CChartdirStageDlg()
{

	m_pStageLoadLogWriteStop = TRUE;

	

	//m_pStageLoadLogWriteStop = TRUE;
	if (m_pStageLoadLogWriteThread != NULL)
	{
		HANDLE threadHandle = m_pStageLoadLogWriteThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pStageLoadLogWriteThread = NULL;
	}
}

void CChartdirStageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STAGE_CHART_VIEW, m_ChartViewer);
	DDX_Control(pDX, IDC_USER_TIME, m_edit_user_time);
	DDX_Control(pDX, IDC_UpdatePeriod, m_UpdatePeriod);
	DDX_Control(pDX, IDC_STATE_LOG, m_stage_log);
}


BEGIN_MESSAGE_MAP(CChartdirStageDlg, CDialogEx)
	ON_WM_TIMER()
	ON_CONTROL(CVN_ViewPortChanged, IDC_STAGE_CHART_VIEW, OnViewPortChanged)
	ON_BN_CLICKED(IDC_BTN_LONGRUN, &CChartdirStageDlg::OnBnClickedBtnLongrun)
	ON_BN_CLICKED(IDC_BTN_START, &CChartdirStageDlg::OnBnClickedBtnStart)
	ON_BN_CLICKED(IDC_BTN_CLEAR, &CChartdirStageDlg::OnBnClickedBtnClear)
	ON_BN_CLICKED(IDC_BTN_STOP, &CChartdirStageDlg::OnBnClickedBtnStop)
	ON_CBN_SELCHANGE(IDC_UpdatePeriod, &CChartdirStageDlg::OnSelchangeUpdatePeriod)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CChartdirStageDlg 메시지 처리기


BOOL CChartdirStageDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);

	Clear_state = false;
	Long_Run = true;
	Buffer_Run = false;
;
	user_time_int = 0;
	num = 0;

	m_pStageLoadLogWriteStop = FALSE;
	m_pStageLoadLogWriteThread = NULL;

	m_extBgColor = getDefaultBgColor();     // Default background color
	
   // Clear data arrays to Chart::NoValue
	for (int i = 0; i < sampleSize; ++i)
		m_timeStamps[i] = m_dataSeriesA[i] = m_dataSeriesB[i] = m_dataSeriesC[i] = Chart::NoValue;

	SYSTEMTIME st;
	GetLocalTime(&st);
	m_nextDataTime = Chart::chartTime(st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute,
		st.wSecond) + st.wMilliseconds / 1000.0;

	m_UpdatePeriod.SelectString(0, _T("1000"));
	drawChart(&m_ChartViewer);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CChartdirStageDlg::OnDestroy()
{
	__super::OnDestroy();
	KillTimer(GETDATA);
	KillTimer(DATACLEAR);
	KillTimer(UPDATE);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

//
// View port changed event
//
void CChartdirStageDlg::OnViewPortChanged()
{
	drawChart(&m_ChartViewer);
}

void CChartdirStageDlg::drawChart(CChartViewer *viewer)
{
	// 수정 전 size 2020.12.14 kjh
	//Multiline_chart = new XYChart(1600, 600);
	Multiline_chart = new XYChart(1600, 400);
	Multiline_chart->setRoundedFrame(m_extBgColor);
	Multiline_chart->addTitle(" Navigation Stage Load Measurement Graph ", "arial.ttf", 20
								)->setBackground(0xdddddd, 0x000000, Chart::glassEffect());

	// 수정 전 size 2020.12.14 kjh
	//Multiline_chart->setPlotArea(70, 120, 1500, 400, 0xffffff, -1, -1, 0xcccccc, 0xcccccc);
	Multiline_chart->setPlotArea(70, 120, 1500, 200, 0xffffff, -1, -1, 0xcccccc, 0xcccccc);
	Multiline_chart->setBackground(LIGHT_GRAY, Transparent, 0);


	if (Clear_state != true)
	{
		LegendBox *title = Multiline_chart->addLegend(55, 45, false, "arial.ttf", 15);
		title->setBackground(Chart::Transparent, Chart::Transparent);
		title->setWidth(1000);
		
	}

	Clear_state = false;
	
	Multiline_chart->xAxis()->setLabelStyle("arial.ttf", 12);
	Multiline_chart->yAxis()->setLabelStyle("arial.ttf", 12);

	Multiline_chart->xAxis()->setColors(Chart::Transparent, Chart::TextColor, Chart::TextColor, 0xaaaaaa);
	Multiline_chart->yAxis()->setColors(Chart::Transparent);

	Multiline_chart->xAxis()->setTickLength(10, 0);

	Multiline_chart->xAxis()->setTickDensity(80);
	Multiline_chart->yAxis()->setTickDensity(10);

	Multiline_chart->xAxis()->setWidth(2);
	Multiline_chart->yAxis()->setWidth(2);

	Multiline_chart->yAxis()->setTitle("Stage Load Value", "arial.ttf", 14, 0x555555);
	Multiline_chart->xAxis()->setTitle("Time (s)", "arial.ttf", 14, 0x555555);

	double lastTime = m_timeStamps[sampleSize - 1];
	if (lastTime != Chart::NoValue)
	{
		Multiline_chart->xAxis()->setDateScale(lastTime - DataInterval * sampleSize / 1000, lastTime);

		Multiline_chart->xAxis()->setLabelFormat("{value|hh:nn:ss}");

		LineLayer *layer = Multiline_chart->addLineLayer();

		layer->setXData(DoubleArray(m_timeStamps, sampleSize));

		char buffer[1024];
		sprintf(buffer, "Axis 0: <*bgColor=FFCCCC*> %.2f ", m_dataSeriesA[sampleSize - 1]);
		//sprintf(buffer, "DOUT_0: <*bgColor=FFCCCC*> %.2f ", m_dataSeriesA[sampleSize - 1]);
		layer->addDataSet(DoubleArray(m_dataSeriesA, sampleSize), 0xff0000, buffer);
		
		sprintf(buffer, "Axis 1: <*bgColor=CCFFCC*> %.2f ", m_dataSeriesB[sampleSize - 1]);
		//sprintf(buffer, "DOUT_1: <*bgColor=CCFFCC*> %.2f ", m_dataSeriesB[sampleSize - 1]);
		layer->addDataSet(DoubleArray(m_dataSeriesB, sampleSize), 0x00cc00, buffer);
	
	//	sprintf(buffer, "DOUT_2: <*bgColor=CCCCFF*> %.2f ", m_dataSeriesC[sampleSize - 1]);
	//	layer->addDataSet(DoubleArray(m_dataSeriesC, sampleSize), 0x0000ff, buffer);
	}


	viewer->setChart(Multiline_chart);
	delete Multiline_chart;

}

//
// A utility to shift a new data value into a data array
//
static void shiftData(double *data, int len, double newValue)
{
	memmove(data, data + 1, sizeof(*data) * (len - 1));
	data[len - 1] = newValue;
}


void CChartdirStageDlg::DataReset()
{
	// Clear data arrays to Chart::NoValue
	for (int i = 0; i < sampleSize; ++i)
		m_timeStamps[i] = m_dataSeriesA[i] = m_dataSeriesB[i] = m_dataSeriesC[i] = Chart::NoValue;

}

//
// The data acquisition routine. In this demo, this is invoked every 250ms.
//
void CChartdirStageDlg::getData()
{
	SYSTEMTIME st;
	GetLocalTime(&st);
	double now = Chart::chartTime(st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond) + st.wMilliseconds / 1000.0;

	// This is our formula for the random number generator
	do
	{
		// Get a data sample
		double p = m_nextDataTime * 4;
		
		//////////////////////
		// TEST BENCH
		//////////////////////
		//dataA = 20 + cos(p * 129241) * 10 + 1 / (cos(p) * cos(p) + 0.01);
		//dataB = 150 + 100 * sin(p / 27.7) * sin(p / 10.1);
		//dataC = 150 + 100 * cos(p / 6.7) * cos(p / 11.9);


		//////////////////////
		// STAGE LOAD VALUE
		//////////////////////
		dataB = g_pNavigationStage->GetGlobalRealVariable("DOUT_1");
		dataA = g_pNavigationStage->GetGlobalRealVariable("DOUT_0");



		// Shift the values into the arrays
		shiftData(m_dataSeriesA, sampleSize, dataA);
		shiftData(m_dataSeriesB, sampleSize, dataB);
		//shiftData(m_dataSeriesC, sampleSize, dataC);
		shiftData(m_timeStamps, sampleSize, m_nextDataTime);

		m_nextDataTime += DataInterval / 1000.0;
	} while (m_nextDataTime < now);
}

void CChartdirStageDlg::getDataClear()
{
	SYSTEMTIME st;
	GetLocalTime(&st);
	double now = Chart::chartTime(st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond) + st.wMilliseconds / 1000.0;

	DataReset();
	do
	{
		double p = m_nextDataTime * 4;
		
		dataA = 0.0;
		dataB = 0.0;
		
		shiftData(m_dataSeriesA, sampleSize, dataA);
		shiftData(m_dataSeriesB, sampleSize, dataB);
		shiftData(m_timeStamps, sampleSize, m_nextDataTime);

		m_nextDataTime += DataInterval / 1000.0;
	} while (m_nextDataTime < now);

}

int CChartdirStageDlg::getDefaultBgColor()
{
	LOGBRUSH LogBrush;
	HBRUSH hBrush = (HBRUSH)SendMessage(WM_CTLCOLORDLG, (WPARAM)CClientDC(this).m_hDC,
		(LPARAM)m_hWnd);
	::GetObject(hBrush, sizeof(LOGBRUSH), &LogBrush);
	int ret = LogBrush.lbColor;
	return ((ret & 0xff) << 16) | (ret & 0xff00) | ((ret & 0xff0000) >> 16);
}


void CChartdirStageDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case GETDATA:
		getData();
	//	m_ChartViewer.updateViewPort(true, false);
		break;
	case DATACLEAR:
		getDataClear();
		m_ChartViewer.updateViewPort(true, false);
		break;
	case UPDATE:
		m_ChartViewer.updateViewPort(true, false);
		break;
	}
	CDialogEx::OnTimer(nIDEvent);
}


void CChartdirStageDlg::OnBnClickedBtnLongrun()
{
	if (g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		GetDlgItem(IDC_BTN_LONGRUN)->EnableWindow(false);
		GetDlgItem(IDC_BTN_START)->EnableWindow(false);

		/////////////////////////////
		// Stage Buffer 6 START
		/////////////////////////////
		CAutoMessageDlg MsgBoxAuto(this);
		if (!Buffer_Run)
		{
			//g_pNavigationStage->RunBuffer(6);
			m_stage_log.SetWindowTextA(_T("Stage Buffer(6) Start 실행 중 입니다"));
			MsgBoxAuto.DoModal(_T(" Stage Buffer Start(6) 실행 중 입니다. 잠시만 기다려주세요 "), 5);
			m_stage_log.SetWindowTextA(_T("Stage Buffer(6) Start 실행 완료 되었습니다"));
			Buffer_Run = true;


		}

		m_stage_log.SetWindowTextA(_T("Stage Long Run 실행 중 입니다"));


		Long_Run = true;
		Clear_state = false;
		KillTimer(DATACLEAR);
		//getDataClear();
		DataReset();
		Stage_Load_Log_WriteStart();
		Run();
		//SetTimer(GETDATA, DataInterval, 0);
	}
	else
	{
		AfxMessageBox(_T("Stage 가 연결되지 않았습니다"), MB_ICONERROR);
	
	}
}


void CChartdirStageDlg::OnBnClickedBtnStart()
{
	if (g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		GetDlgItem(IDC_BTN_LONGRUN)->EnableWindow(false);
		GetDlgItem(IDC_BTN_START)->EnableWindow(false);

		m_edit_user_time.GetWindowTextA(user_time);
		if (!IsNumeric(user_time))
		{
			Buffer_Run = false;
			m_pStageLoadLogWriteStop = TRUE;
			m_pStageLoadLogWriteThread = NULL;
			m_stage_log.SetWindowTextA(_T("Error :: 사용자 설정 시간에 숫자가 아닌 값이 입력 되었습니다"));
			AfxMessageBox(_T("사용자 설정 시간에 숫자가 아닌 값이 입력 되었습니다"));
			OnBnClickedBtnStop();
		}
		else
		{
			/////////////////////////////
			// Stage Buffer 6 START
			/////////////////////////////
			CAutoMessageDlg MsgBoxAuto(this);
			if (!Buffer_Run)
			{
				//g_pNavigationStage->RunBuffer(6);
				m_stage_log.SetWindowTextA(_T("Stage Buffer(6) Start 실행 중 입니다"));
				//WaitSec(5);
				MsgBoxAuto.DoModal(_T(" Stage Buffer Start(6) 실행 중 입니다. 잠시만 기다려주세요 "), 5);
				m_stage_log.SetWindowTextA(_T("Stage Buffer(6) Start 실행 완료 되었습니다"));
				Buffer_Run = true;
			}

			m_stage_log.SetWindowTextA(_T("Stage User Start 실행 중 입니다"));

			Long_Run = false;
			Clear_state = false;
			KillTimer(DATACLEAR);
			DataReset();
			//getDataClear();
			Stage_Load_Log_WriteStart();
			Run();
			//SetTimer(GETDATA, DataInterval, 0);
		}
	}
	else
	{
		AfxMessageBox(_T("Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		m_stage_log.SetWindowTextA(_T("Stage Load 측정 가능 상태 입니다."));
	}

}

void CChartdirStageDlg::OnBnClickedBtnStop()
{

	if (g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		/////////////////////////////
		// Stage Buffer 6 STOP
		/////////////////////////////
		CAutoMessageDlg MsgBoxAuto(this);
		g_pNavigationStage->StopBuffer(6);
		m_stage_log.SetWindowTextA(_T("Stage Buffer(6) Stop 실행 중 입니다."));
		//WaitSec(5);
		MsgBoxAuto.DoModal(_T(" Stage Buffer Start(6) 실행 중 입니다. 잠시만 기다려주세요 "), 3);
		m_stage_log.SetWindowTextA(_T("Stage Buffer(6) Stop 실행 완료 되었습니다"));
		//MsgBoxAuto.DoModal(_T(" Stage Buffer Stop 실행 중 ! "), 5);
		Buffer_Run = false;
	}

		num = 0;

		KillTimer(GETDATA);
		KillTimer(DATACLEAR);

		m_pStageLoadLogWriteStop = TRUE;
		m_pStageLoadLogWriteThread = NULL;

		m_stage_log.SetWindowTextA(_T("Stage Load 측정 가능 상태 입니다."));
		GetDlgItem(IDC_BTN_LONGRUN)->EnableWindow(true);
		GetDlgItem(IDC_BTN_START)->EnableWindow(true);
	
		//DataReset();
	//SetTimer(2, DataInterval, 0);
}

void CChartdirStageDlg::OnBnClickedBtnClear()
{

	Clear_state = true;
	KillTimer(GETDATA);
	DataReset();
	//getDataClear();
	//getData();
	m_ChartViewer.updateViewPort(true, false);

	if (Buffer_Run)
	{
		SetTimer(GETDATA, DataInterval, 0);
	}
	else
	{
		GetDlgItem(IDC_BTN_LONGRUN)->EnableWindow(true);
		GetDlgItem(IDC_BTN_START)->EnableWindow(true);
		getDataClear();
	}

}


void CChartdirStageDlg::Stage_Load_Start()
{
	GetDlgItem(IDC_BTN_LONGRUN)->EnableWindow(false);
	GetDlgItem(IDC_BTN_START)->EnableWindow(false);

	/////////////////////////////
	// Stage Buffer 6 START
	/////////////////////////////

	CAutoMessageDlg MsgBoxAuto(this);
	if (!Buffer_Run)
	{
		//g_pNavigationStage->RunBuffer(6);
		m_stage_log.SetWindowTextA(_T("Stage Buffer(6) Start 실행 중 입니다"));
		MsgBoxAuto.DoModal(_T(" Stage Buffer Start(6) 실행 중 입니다. 잠시만 기다려주세요 "), 5);
		m_stage_log.SetWindowTextA(_T("Stage Buffer(6) Start 실행 완료 되었습니다"));
		Buffer_Run = true;
	}

	m_stage_log.SetWindowTextA(_T("Stage Long Run 실행 중 입니다"));
	Long_Run = true;
	Clear_state = false;

	KillTimer(DATACLEAR);

	DataReset();
	Stage_Load_Log_WriteStart();
	Run();
}

void CChartdirStageDlg::Stage_Load_Stop()
{
	/////////////////////////////
	// Stage Buffer 6 STOP
	/////////////////////////////
	CAutoMessageDlg MsgBoxAuto(this);
	g_pNavigationStage->StopBuffer(6);
	m_stage_log.SetWindowTextA(_T("Stage Buffer(6) Stop 실행 중 입니다."));
	//WaitSec(5);
	MsgBoxAuto.DoModal(_T(" Stage Buffer Start(6) 실행 중 입니다. 잠시만 기다려주세요 "), 3);
	m_stage_log.SetWindowTextA(_T("Stage Buffer(6) Stop 실행 완료 되었습니다"));
	//MsgBoxAuto.DoModal(_T(" Stage Buffer Stop 실행 중 ! "), 5);
	Buffer_Run = false;

	num = 0;

	KillTimer(GETDATA);
	KillTimer(DATACLEAR);

	m_pStageLoadLogWriteStop = TRUE;
	m_pStageLoadLogWriteThread = NULL;

	m_stage_log.SetWindowTextA(_T("Stage Load 측정 가능 상태 입니다."));
	GetDlgItem(IDC_BTN_LONGRUN)->EnableWindow(true);
	GetDlgItem(IDC_BTN_START)->EnableWindow(true);
}

void CChartdirStageDlg::Stage_Load_Log_WriteStart()
{
	m_pStageLoadLogWriteStop = FALSE;
	if (m_pStageLoadLogWriteThread == NULL)
	{
		m_pStageLoadLogWriteThread = ::AfxBeginThread(StageLoadLogWrite_Thread, this, THREAD_PRIORITY_NORMAL, 0, 0);
	}
	else
	{
		AfxMessageBox(_T("이미 thread 가 진행 중입니다."));
	}
}

UINT CChartdirStageDlg::StageLoadLogWrite_Thread(LPVOID pParam)
{
	int ret = 0;


	CChartdirStageDlg*  g_pchartstage = (CChartdirStageDlg*)pParam;

	g_pchartstage->m_pStageLoadLogWriteStop = FALSE;
	while (!g_pchartstage->m_pStageLoadLogWriteStop)
	{
		g_pchartstage->Log_th__Start();
	}

	return ret;
}

BOOL CChartdirStageDlg::IsNumeric(CString& csStr)

{
	int nLength = csStr.GetLength();
	for (int iCnt = 0; iCnt < nLength; iCnt++)
	{

		char ch = csStr.GetAt(iCnt);

		if (!((ch >= '0') && (ch <= '9') || (ch =='.')))
		return FALSE;

	}
	return TRUE;
}


void CChartdirStageDlg::Log_th__Start()
{
	int cnt = 0;
	CString path = LOG_RUN_PATH;

	if (!g_pLog->IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
	}

	
	m_edit_user_time.GetWindowTextA(user_time);
	if (!IsNumeric(user_time))
	{
		m_pStageLoadLogWriteStop = TRUE;
		m_pStageLoadLogWriteThread = NULL;
		AfxMessageBox(_T("사용자 설정 시간에 숫자가 아닌 값이 입력 되었습니다"));
		OnBnClickedBtnStop();
	}
	else
	{
		//user_time_double = _ttoi(user_time);
		user_time_double = atof(user_time);
		user_time_int = user_time_double * 3600;
	}


	year = CTime::GetCurrentTime().GetYear();
	mon = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	min = CTime::GetCurrentTime().GetMinute();
	sec = CTime::GetCurrentTime().GetSecond();

	stage_data_file.Format(_T("%s\\Stage_Load_Measurement_data__%d_%d_%d_%d_start.txt"), path, mon, day, hour, min);

	std::ofstream data_write_file(stage_data_file);
	data_write_file << std::setprecision(3) << "Date" << "\t" << "Time" << "\t" << "Axis 0" << "\t" << "Axis 1" << "\n";
	//data_write_file << std::setprecision(3) << "Date" << "\t" << "Time" << "\t" << "m_dataSeriesA" << "\t" << "m_dataSeriesB" << "\n";

	while (m_pStageLoadLogWriteStop != TRUE)
	{
		cnt++;
		mon = CTime::GetCurrentTime().GetMonth();
		day = CTime::GetCurrentTime().GetDay();
		hour = CTime::GetCurrentTime().GetHour();
		min = CTime::GetCurrentTime().GetMinute();
		sec = CTime::GetCurrentTime().GetSecond();

		if (cnt == 5)
		{
			data_write_file << std::setprecision(3) << year << "." << mon << "." << day << "\t" << hour << ":" << min << ":" << sec << "\t" << dataA << "\t" << dataB << "\n";
			cnt = 0;
		}
		WaitSec(1);
	
		if (!Long_Run)
		{
			if (user_time_int == num)
			{
				num = 0;
				OnBnClickedBtnStop();
				AfxMessageBox(_T("측정이 완료 되었습니다"));
				break;
			}
			num++;
		}
		
	}

	data_write_file << std::endl;
	if (data_write_file.is_open() == true)
	{
		data_write_file.close();
	}

	m_pStageLoadLogWriteStop = TRUE;
	m_pStageLoadLogWriteThread = NULL;

}

void CChartdirStageDlg::Run()
{
	CString s;
	m_UpdatePeriod.GetLBText(m_UpdatePeriod.GetCurSel(), s);
	KillTimer(GETDATA);
	SetTimer(GETDATA, _tcstol(s, 0, 0), 0);
	SetTimer(UPDATE, _tcstol(s, 0, 0), 0);
}



void CChartdirStageDlg::OnSelchangeUpdatePeriod()
{
	Run();
}


