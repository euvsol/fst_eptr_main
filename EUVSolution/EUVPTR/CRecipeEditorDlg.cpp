﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CRecipeEditorDlg 대화 상자

IMPLEMENT_DYNAMIC(CRecipeEditorDlg, CDialogEx)

CRecipeEditorDlg::CRecipeEditorDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_RECIPE_EDIT_DIALOG, pParent)
	, m_bPMRectDisplay(FALSE)
	, m_dPMScore(0)
	, m_dPMResultPosX(0)
	, m_dPMResultPosY(0)
	, m_dPMResultAngle(0)
{
	m_nSelectedViewType = 0;

	m_MilSystem = M_NULL;
	m_MilGrayModel		= M_NULL;
	m_MilResultModel	= M_NULL;
	int i = 0;
	for (i = 0; i < TOTAL_PATTERNMATCH_NUMBER; i++)
	{
		m_MilDisplayModel[i] = M_NULL;
		m_MilImageModel[i] = NULL;
		m_MilPMModel[i] = NULL;
	}

	nSuccessScore = 30;
	m_bPMSuccess = FALSE;
}

CRecipeEditorDlg::~CRecipeEditorDlg()
{
}

void CRecipeEditorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_OM_ALIGN1ST, m_CheckOM1stAlignCtrl);
	DDX_Control(pDX, IDC_CHECK_OM_ALIGN2ND, m_CheckOM2ndAlignCtrl);
	DDX_Control(pDX, IDC_CHECK_OM_ALIGN3RD, m_CheckOM3rdAlignCtrl);
	DDX_Control(pDX, IDC_CHECK_EUV_ALIGN1ST, m_CheckEUV1stAlignCtrl);
	DDX_Control(pDX, IDC_CHECK_EUV_ALIGN2ND, m_CheckEUV2ndAlignCtrl);
	DDX_Control(pDX, IDC_CHECK_EUV_ALIGN3RD, m_CheckEUV3rdAlignCtrl);
	DDX_Control(pDX, IDC_CHECK_TEST_POINT, m_CheckTestPointCtrl);
	DDX_Control(pDX, IDC_CHECK_OM100_ALIGN1ST, m_CheckOM100X1stAlignCtrl);
	DDX_Control(pDX, IDC_CHECK_OM100_ALIGN2ND, m_CheckOM100X2ndAlignCtrl);
	DDX_Control(pDX, IDC_CHECK_OM100_ALIGN3RD, m_CheckOM100X3rdAlignCtrl);
	DDX_Check(pDX, IDC_CHECK_PM_AREA_RECT, m_bPMRectDisplay);
	DDX_Text(pDX, IDC_EDIT_MATCHSCORE, m_dPMScore);
	DDX_Text(pDX, IDC_EDIT_MATCHPOSX, m_dPMResultPosX);
	DDX_Text(pDX, IDC_EDIT_MATCHPOSY, m_dPMResultPosY);
	DDX_Text(pDX, IDC_EDIT_MATCHANGLE, m_dPMResultAngle);
	DDX_Control(pDX, IDC_BUTTON_TEST_PATTERNMATCH, m_ButtonTestModelCtrl);
}


BEGIN_MESSAGE_MAP(CRecipeEditorDlg, CDialogEx)
	ON_BN_CLICKED(IDC_CHECK_OM_ALIGN1ST, &CRecipeEditorDlg::OnBnClickedCheckOmAlign1st)
	ON_BN_CLICKED(IDC_CHECK_OM_ALIGN2ND, &CRecipeEditorDlg::OnBnClickedCheckOmAlign2nd)
	ON_BN_CLICKED(IDC_CHECK_OM_ALIGN3RD, &CRecipeEditorDlg::OnBnClickedCheckOmAlign3rd)
	ON_BN_CLICKED(IDC_CHECK_EUV_ALIGN1ST, &CRecipeEditorDlg::OnBnClickedCheckEuvAlign1st)
	ON_BN_CLICKED(IDC_CHECK_EUV_ALIGN2ND, &CRecipeEditorDlg::OnBnClickedCheckEuvAlign2nd)
	ON_BN_CLICKED(IDC_CHECK_EUV_ALIGN3RD, &CRecipeEditorDlg::OnBnClickedCheckEuvAlign3rd)
	ON_BN_CLICKED(IDC_CHECK_TEST_POINT, &CRecipeEditorDlg::OnBnClickedCheckTestPoint)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_PATTERNMATCH, &CRecipeEditorDlg::OnBnClickedButtonMovePatternmatch)
	ON_BN_CLICKED(IDC_BUTTON_MAKE_PATTERNMATCH, &CRecipeEditorDlg::OnBnClickedButtonMakePatternmatch)
	ON_BN_CLICKED(IDC_BUTTON_DELETE_PATTERNMATCH, &CRecipeEditorDlg::OnBnClickedButtonDeletePatternmatch)
	ON_BN_CLICKED(IDC_BUTTON_TEST_PATTERNMATCH, &CRecipeEditorDlg::OnBnClickedButtonTestPatternmatch)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_PATTERNMATCH, &CRecipeEditorDlg::OnBnClickedButtonSavePatternmatch)
	ON_WM_PAINT()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHECK_OM100_ALIGN1ST, &CRecipeEditorDlg::OnBnClickedCheckOm100Align1st)
	ON_BN_CLICKED(IDC_CHECK_OM100_ALIGN2ND, &CRecipeEditorDlg::OnBnClickedCheckOm100Align2nd)
	ON_BN_CLICKED(IDC_CHECK_OM100_ALIGN3RD, &CRecipeEditorDlg::OnBnClickedCheckOm100Align3rd)
	ON_BN_CLICKED(IDC_CHECK_PM_AREA_RECT, &CRecipeEditorDlg::OnBnClickedCheckPmAreaRect)
END_MESSAGE_MAP()


// CRecipeEditorDlg 메시지 처리기

void CRecipeEditorDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 __super::OnPaint()을(를) 호출하지 마십시오.
}


BOOL CRecipeEditorDlg::OnInitDialog()
{
	__super::OnInitDialog();

	CheckAlignPoint(0);

	CWnd *pWnd[TOTAL_PATTERNMATCH_NUMBER];

	pWnd[0] = (CWnd *)(GetDlgItem(IDC_RECIPE_TEST_PMIMAGE));
	pWnd[1] = (CWnd *)(GetDlgItem(IDC_RECIPE_OM_ALIGN1ST));
	pWnd[2] = (CWnd *)(GetDlgItem(IDC_RECIPE_OM_ALIGN2ND));
	pWnd[3] = (CWnd *)(GetDlgItem(IDC_RECIPE_OM_ALIGN3RD));
	pWnd[4] = (CWnd *)(GetDlgItem(IDC_RECIPE_OM100_ALIGN1ST));
	pWnd[5] = (CWnd *)(GetDlgItem(IDC_RECIPE_OM100_ALIGN2ND));
	pWnd[6] = (CWnd *)(GetDlgItem(IDC_RECIPE_OM100_ALIGN3RD));
	pWnd[7] = (CWnd *)(GetDlgItem(IDC_RECIPE_EUV_ALIGN1ST));
	pWnd[8] = (CWnd *)(GetDlgItem(IDC_RECIPE_EUV_ALIGN2ND));
	pWnd[9] = (CWnd *)(GetDlgItem(IDC_RECIPE_EUV_ALIGN3RD));

	//MsysAlloc(M_SYSTEM_HOST, M_DEF_SYSTEM_NUM, M_COMPLETE, &m_MilSystem);
	m_MilSystem = g_milSystemHost;

	CRect rc;
	int i = 0;
	for (i = 0; i < TOTAL_PATTERNMATCH_NUMBER; i++)
	{
		pWnd[i]->GetWindowRect(rc);
		rc.right = rc.Width()-1;
		rc.bottom = rc.Height()-1;
		rc.left = 0;
		rc.top = 0;

	/*	MIL_INT SizeX, SizeY;
		MbufInquire(m_MilImage, M_SIZE_X, &SizeX);
		MbufInquire(m_MilImage, M_SIZE_Y, &SizeY);*/

		MdispAlloc(m_MilSystem, M_DEFAULT, M_DISPLAY_SETUP, M_DEFAULT, &m_MilDisplayModel[i]);
		MbufAlloc2d(m_MilSystem, rc.Width(), rc.Height(), 8L + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP, &m_MilImageModel[i]);
		MbufClear(m_MilImageModel[i], 0);
		CString str;
		str.Format(_T("%s\\MapImage\\alignimage_%d.bmp"), RECIPE_PATH, i);
		if (IsFileExist(str))
		{
			
			MbufLoad((char*)(LPCTSTR)str, m_MilImageModel[i]);
			//MbufFree(m_MilPMModel[i]);
			//MbufRestore((char*)(LPCTSTR)str, m_MilSystem, &m_MilImageModel[i]);
			MpatAllocModel(m_MilSystem, m_MilImageModel[i], 0, 0, rc.Width(), rc.Height(), M_NORMALIZED, &m_MilPMModel[i]);
		}
		MdispSelectWindow(m_MilDisplayModel[i], m_MilImageModel[i], pWnd[i]->m_hWnd);
	}
	MbufAlloc2d(m_MilSystem, 1544, 1544, 8L + M_UNSIGNED, M_IMAGE + M_PROC + M_DIB, &m_MilGrayModel);
	MbufClear(m_MilGrayModel, 0);
	MpatAllocResult(m_MilSystem, 1L, &m_MilResultModel);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CRecipeEditorDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}
	return __super::PreTranslateMessage(pMsg);
}


void CRecipeEditorDlg::OnDestroy()
{
	__super::OnDestroy();

	if (m_MilGrayModel != M_NULL)
	{
		MbufFree(m_MilGrayModel);
		m_MilGrayModel = M_NULL;
	}

	if (m_MilResultModel > 0)
	{
		MpatFree(m_MilResultModel);
		m_MilResultModel = M_NULL;
	}

	for (int i = 0; i < TOTAL_PATTERNMATCH_NUMBER; i++)
	{
		if (m_MilDisplayModel[i] > 0)
		{
			MdispFree(m_MilDisplayModel[i]);
		}
		if (m_MilPMModel[i] > 0)
		{
			MpatFree(m_MilPMModel[i]);
		}
		if (m_MilImageModel[i] > 0)
		{
			MbufFree(m_MilImageModel[i]);
		}
	}

	//if (m_MilSystem != M_NULL)
	//{
	//	MsysFree(m_MilSystem);
	//	m_MilSystem = M_NULL;
	//}
}

void CRecipeEditorDlg::OnBnClickedCheckTestPoint()
{
	CheckAlignPoint(PM_TEST_POSITION);
}


void CRecipeEditorDlg::OnBnClickedCheckOmAlign1st()
{
	CheckAlignPoint(PM_1STALIGN_POSITION);
}


void CRecipeEditorDlg::OnBnClickedCheckOmAlign2nd()
{
	CheckAlignPoint(PM_2NDALIGN_POSITION);
}


void CRecipeEditorDlg::OnBnClickedCheckOmAlign3rd()
{
	CheckAlignPoint(PM_3RDALING_POSITION);
}

void CRecipeEditorDlg::OnBnClickedCheckOm100Align1st()
{
	CheckAlignPoint(4);
}


void CRecipeEditorDlg::OnBnClickedCheckOm100Align2nd()
{
	CheckAlignPoint(5);
}


void CRecipeEditorDlg::OnBnClickedCheckOm100Align3rd()
{
	CheckAlignPoint(6);
}

void CRecipeEditorDlg::OnBnClickedCheckEuvAlign1st()
{
	CheckAlignPoint(7);
}


void CRecipeEditorDlg::OnBnClickedCheckEuvAlign2nd()
{
	CheckAlignPoint(8);
}


void CRecipeEditorDlg::OnBnClickedCheckEuvAlign3rd()
{
	CheckAlignPoint(9);
}

void CRecipeEditorDlg::OnBnClickedButtonMovePatternmatch()
{
	MovePMModel(m_nSelectedViewType);
}


void CRecipeEditorDlg::OnBnClickedButtonMakePatternmatch()
{
	m_bPMSuccess = FALSE;
	MakePMModel(m_nSelectedViewType);
}


void CRecipeEditorDlg::OnBnClickedButtonDeletePatternmatch()
{
	m_bPMSuccess = FALSE;
}


void CRecipeEditorDlg::OnBnClickedButtonTestPatternmatch()
{
	m_ButtonTestModelCtrl.EnableWindow(FALSE);
	SearchPMModel(m_nSelectedViewType);
	WaitSec(0.5);
	SearchPMModel(m_nSelectedViewType);
	WaitSec(0.5);
	SearchPMModel(m_nSelectedViewType);
	WaitSec(0.5);
	SearchPMModel(m_nSelectedViewType);
	m_ButtonTestModelCtrl.EnableWindow(TRUE);

}


void CRecipeEditorDlg::OnBnClickedButtonSavePatternmatch()
{
	m_bPMSuccess = FALSE;
}

void CRecipeEditorDlg::CheckAlignPoint(int nPoint)
{
	m_CheckTestPointCtrl.SetCheck(FALSE);
	m_CheckOM1stAlignCtrl.SetCheck(FALSE);
	m_CheckOM2ndAlignCtrl.SetCheck(FALSE);
	m_CheckOM3rdAlignCtrl.SetCheck(FALSE);
	m_CheckOM100X1stAlignCtrl.SetCheck(FALSE);
	m_CheckOM100X2ndAlignCtrl.SetCheck(FALSE);
	m_CheckOM100X3rdAlignCtrl.SetCheck(FALSE);
	m_CheckEUV1stAlignCtrl.SetCheck(FALSE);
	m_CheckEUV2ndAlignCtrl.SetCheck(FALSE);
	m_CheckEUV3rdAlignCtrl.SetCheck(FALSE);

	m_bPMSuccess = FALSE;
	m_nSelectedViewType = nPoint;
	switch (nPoint)
	{
	case PM_TEST_POSITION:
		m_CheckTestPointCtrl.SetCheck(TRUE);
		break;
	case PM_1STALIGN_POSITION:
		m_CheckOM1stAlignCtrl.SetCheck(TRUE);
		break;
	case PM_2NDALIGN_POSITION:
		m_CheckOM2ndAlignCtrl.SetCheck(TRUE);
		break;
	case PM_3RDALING_POSITION:
		m_CheckOM3rdAlignCtrl.SetCheck(TRUE);
		break;
	case 4:
		m_CheckOM100X1stAlignCtrl.SetCheck(TRUE);
		break;
	case 5:
		m_CheckOM100X2ndAlignCtrl.SetCheck(TRUE);
		break;
	case 6:
		m_CheckOM100X3rdAlignCtrl.SetCheck(TRUE);
		break;
	case 7:
		m_CheckEUV1stAlignCtrl.SetCheck(TRUE);
		break;
	case 8:
		m_CheckEUV2ndAlignCtrl.SetCheck(TRUE);
		break;
	case 9:
		m_CheckEUV3rdAlignCtrl.SetCheck(TRUE);
		break;
	default:
		break;
	}

	Invalidate(FALSE);
}

void CRecipeEditorDlg::OnBnClickedCheckPmAreaRect()
{
	m_bPMSuccess = FALSE;
	UpdateData(TRUE);
}

void CRecipeEditorDlg::MovePMModel(int nViewType)
{
	//if (g_pNavigationStage != NULL)
	//	g_pNavigationStage->MoveStageDBPositionNo(nViewType);
}

void CRecipeEditorDlg::MakePMModel(int nViewType)
{
	if (m_MilPMModel[nViewType] > 0)
	{
		MpatFree(m_MilPMModel[nViewType]);
	}
	CRect rc = g_pCamera->m_CameraWnd.m_RectTrackerOM.m_rect;

	MimConvert(g_pCamera->m_CameraWnd.m_MilOMDisplay, m_MilGrayModel, M_RGB_TO_L);
	MpatAllocModel(m_MilSystem, m_MilGrayModel, rc.left, rc.top, rc.Width(), rc.Height(), M_NORMALIZED, &m_MilPMModel[nViewType]);
	MpatCopy(m_MilPMModel[nViewType], m_MilImageModel[nViewType], M_DEFAULT);

	CString str;
	str.Format(_T("%s\\MapImage\\alignimage_%d.bmp"), RECIPE_PATH, nViewType);
	MbufExport((char*)(LPCSTR)str, M_BMP, m_MilImageModel[nViewType]);
	//MbufExport((char*)(LPCSTR)str, M_BMP, m_MilGrayModel);
	
}

int CRecipeEditorDlg::SearchPMModel(int nViewType)
{
	int ret = 0;

	m_bPMRectDisplay = FALSE;
	UpdateData(false);

	if (m_MilPMModel[nViewType] > 0)
	{
		MimConvert(g_pCamera->m_CameraWnd.m_MilOMDisplay, m_MilGrayModel, M_RGB_TO_L);
		//MpatSetAcceptance(m_MilPMModel[nViewType], nSuccessScore);
		MpatSetAcceptance(m_MilPMModel[nViewType], 50L);
		MpatSetCertainty(m_MilPMModel[nViewType], 80L);
		MpatSetAccuracy(m_MilPMModel[nViewType], M_LOW);	//M_LOW: Low accuracy(typically ± 0.20 pixels),M_MEDIUM: Medium accuracy(typically ± 0.10 pixels),M_HIGH: High accuracy(typically ± 0.05 pixels)
		MpatSetSpeed(m_MilPMModel[nViewType], M_LOW);

		///* Set the search model speed. */
		//MpatSetSpeed(m_MilPMModel[nViewType], M_MEDIUM);
		///* Set the position search accuracy. */
		//MpatSetAccuracy(m_MilPMModel[nViewType], M_HIGH);
		///* Activate the search model angle mode. */
		//MpatSetAngle(m_MilPMModel[nViewType], M_SEARCH_ANGLE_MODE, M_ENABLE);
		///* Set the search model range angle. */
		//MpatSetAngle(m_MilPMModel[nViewType], M_SEARCH_ANGLE_DELTA_NEG, ROTATED_FIND_ANGLE_DELTA_NEG);
		//MpatSetAngle(m_MilPMModel[nViewType], M_SEARCH_ANGLE_DELTA_POS, ROTATED_FIND_ANGLE_DELTA_POS);
		///* Set the search model angle accuracy. */
		//MpatSetAngle(m_MilPMModel[nViewType], M_SEARCH_ANGLE_ACCURACY, ROTATED_FIND_MIN_ANGLE_ACCURACY);
		///* Set the search model angle interpolation mode to bilinear. */
		//MpatSetAngle(m_MilPMModel[nViewType], M_SEARCH_ANGLE_INTERPOLATION_MODE, M_BILINEAR);

		MpatPreprocModel(m_MilGrayModel, m_MilPMModel[nViewType], M_DEFAULT);
		MpatSetNumber(m_MilPMModel[nViewType], 1L);

		MpatFindModel(m_MilGrayModel, m_MilPMModel[nViewType], m_MilResultModel);
		double x = 0., y = 0., angle = 0., Score = 0.;
		int getnum = MpatGetNumber(m_MilResultModel, M_NULL);
		if (getnum > 0)
		{
			MpatGetResult(m_MilResultModel, M_POSITION_X, &x);
			MpatGetResult(m_MilResultModel, M_POSITION_Y, &y);
			//MpatGetResult(m_MilResultModel, M_ANGLE, &angle);
			MpatGetResult(m_MilResultModel, M_SCORE, &Score);
			m_bPMSuccess = TRUE;
		}
		else
		{
			m_bPMSuccess = FALSE;
			ret = -1;
		}
		m_dPMResultPosX = x;
		m_dPMResultPosY = y;
		m_dPMResultAngle = angle;
		m_dPMScore = Score;
		UpdateData(false);

		// 찾은 Pixel 위치 저장
		if (g_pTest != NULL)
			g_pTest->SaveTestResult(MASK_SLIP_TEST, m_dPMResultPosX, m_dPMResultPosY, m_dPMResultAngle, m_dPMScore);

		if (m_bPMSuccess && g_pAP->Check_STAGE_MovePossible() == XY_NAVISTAGE_OK)
		{
			// 찾은 위치를 화면 센터로 이동
			int centerx = g_pCamera->m_CameraWnd.m_nDigSizeX / 2;
			int centery = g_pCamera->m_CameraWnd.m_nDigSizeY / 2;
			int tarpixelx = 0;
			int tarpixely = 0;
			double current_posx_mm = 0.0, current_posy_mm = 0.0;
			double target_posx_mm = 0.0, target_posy_mm = 0.0;

			tarpixelx = g_pRecipe->m_dPMResultPosX - centerx;
			tarpixely = g_pRecipe->m_dPMResultPosY - centery;

			current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
			current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

			target_posx_mm = current_posx_mm - (tarpixelx * g_pConfig->m_dOM_PixelSizeX_um) / 1000.;
			target_posy_mm = current_posy_mm + (tarpixely * g_pConfig->m_dOM_PixelSizeY_um) / 1000.;

			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);
		}

		//CString str = "";
		//str.Format("D:\\SystemTestImage.bmp");
		//MbufSave((char*)(LPCTSTR)strTmp,m_MilGrayModel);
	}

	return ret;
}

