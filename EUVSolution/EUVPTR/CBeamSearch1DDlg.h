﻿#pragma once


// CBeam1DScanDlg 대화 상자

class CBeamSearch1DDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CBeamSearch1DDlg)

public:
	CBeamSearch1DDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CBeamSearch1DDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_BEAMSEARCH1D_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
};
