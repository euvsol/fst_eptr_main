﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

// CProcessEditorDlg 대화 상자

IMPLEMENT_DYNAMIC(CProcessEditorDlg, CDialogEx)

CProcessEditorDlg::CProcessEditorDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PROCESS_EDITOR_DIALOG, pParent)
{

}

CProcessEditorDlg::~CProcessEditorDlg()
{
}

void CProcessEditorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CProcessEditorDlg, CDialogEx)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CProcessEditorDlg 메시지 처리기

BOOL CProcessEditorDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

BOOL CProcessEditorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CProcessEditorDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}
