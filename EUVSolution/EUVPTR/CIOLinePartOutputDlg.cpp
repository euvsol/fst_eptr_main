﻿// CIOLinePartOutputDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include "CIOLinePartOutputDlg.h"


// CDigitalOutputLinePart 대화 상자

IMPLEMENT_DYNAMIC(CDigitalOutputLinePart, CDialogEx)

CDigitalOutputLinePart::CDigitalOutputLinePart(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IO_LINE_PART_OUTPUT_DIALOG, pParent)
{

}

CDigitalOutputLinePart::~CDigitalOutputLinePart()
{
	m_brush.DeleteObject();
	m_brush2.DeleteObject();
	m_font.DeleteObject();
}

void CDigitalOutputLinePart::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDigitalOutputLinePart, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y000, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY000)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y001, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY001)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y002, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY002)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y003, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY003)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y004, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY004)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y005, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY005)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y006, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY006)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y007, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY007)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y008, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY008)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y009, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY009)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y010, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY010)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y011, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY011)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y012, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY012)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y013, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY013)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y014, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY014)
	ON_BN_CLICKED(IDC_CHECK_DIGITALOUT_LINE_PART_Y015, &CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY015)
END_MESSAGE_MAP()


// CDigitalOutputLinePart 메시지 처리기


HBRUSH CDigitalOutputLinePart::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if ((pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALOUT_LINE_PART_TEXT1) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALOUT_LINE_PART_TEXT2) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALOUT_LINE_PART_TEXT3) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALOUT_LINE_PART_TEXT4))
		{
			pDC->SetBkColor(LIGHT_GRAY);
			//pDC->SetTextColor(RGB(0, 255, 0));
			return m_brush;
		}
		else if (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALOUT_LINE_PART_TEXT)
		{
			pDC->SetBkColor(GRAY);
			//pDC->SetTextColor(RGB(0, 255, 0));
			return m_brush2;
		}
	}
	return hbr;
}


void CDigitalOutputLinePart::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


void CDigitalOutputLinePart::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == OUTPUT_LINE_PART_DIALOG_TIMER)
	{
		//if (g_pIO->m_Crevis_Open_Port == TRUE)
		{
			OnUpdateDigitalOutput_Line_Part();
			SetTimer(OUTPUT_LINE_PART_DIALOG_TIMER, 100, NULL);
		}
	}

	CDialogEx::OnTimer(nIDEvent);
}


BOOL CDigitalOutputLinePart::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	m_brush.CreateSolidBrush(LIGHT_GRAY); // Gague 배경 색
	m_brush2.CreateSolidBrush(GRAY); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CDigitalOutputLinePart::InitControls_DO_LINE_PART()
{
	CString strTemp;
	CString Get_str;
	CButton* btn;


	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y000, "LC DRY PUMP #1 ON/OFF");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y001, "MC DRY PUMP #2 ON/OFF");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y002, "WATER SUPPLY VALVE ON/OFF");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y003, "WATER RETURN VALVE ON/OFF");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y004, "SHUTTER#1 OP/CL");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y005, "SHUTTER#2 OP/CL");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y006, "SHUTTER#3 OP/CL");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y007, "SHUTTER#4 OP/CL");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y008, "SHUTTER#5 OP/CL");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y009, "DVR#1 LAMP ON/OFF");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y010, "DVR#2 LAMP ON/OFF");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y011, "DVR#3 LAMP ON/OFF");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y012, "DVR#4 LAMP ON/OFF");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y013, "EMPTY");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y014, "EMPTY");
	SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y015, "EMPTY");


	for (int nIdx = 0; nIdx < DIGITAL_IO_VIEW_NUMBER; nIdx++)
	{
		strTemp.Format(_T("Y%04d"), nIdx);
		SetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_NUM0 + nIdx, strTemp);
		GetDlgItemText(IDC_STATIC_DIGITALOUT_LINE_PART_Y000 + nIdx, Get_str);
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y000 + nIdx))->SetIcon(m_LedIcon[0]);
		
		if (Get_str == "EMPTY") GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y000 + nIdx)->EnableWindow(false);
	}

	SetTimer(OUTPUT_LINE_PART_DIALOG_TIMER, 100, NULL);
	
}

void CDigitalOutputLinePart::OnUpdateDigitalOutput_Line_Part()
{
	CButton* btn;


//	if (g_pIO->m_Crevis_Open_Port == TRUE)
//	{

		btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y000);
		if (g_pIO->m_bDigitalOut[g_pIO->DO::LLC_DRY_PUMP_SW] == RUN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y000))->SetIcon(m_LedIcon[1]);
			btn->SetCheck(true);
			btn->SetWindowTextA(_T("On"));
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y000))->SetIcon(m_LedIcon[0]);
			btn->SetCheck(false);
			btn->SetWindowTextA(_T("Off"));
		}

		btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y001);
		if (g_pIO->m_bDigitalOut[g_pIO->DO::MC_DRY_PUMP_SW] == RUN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y001))->SetIcon(m_LedIcon[1]);
			btn->SetCheck(true);
			btn->SetWindowTextA(_T("On"));
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y001))->SetIcon(m_LedIcon[0]);
			btn->SetCheck(false);
			btn->SetWindowTextA(_T("Off"));
		}

		btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y002);
		if (g_pIO->m_bDigitalOut[g_pIO->DO::WATER_SUPPLY_VALVE] == RUN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y002))->SetIcon(m_LedIcon[1]);
			btn->SetCheck(true);
			btn->SetWindowTextA(_T("On"));
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y002))->SetIcon(m_LedIcon[0]);
			btn->SetCheck(false);
			btn->SetWindowTextA(_T("Off"));
		}

		btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y003);
		if (g_pIO->m_bDigitalOut[g_pIO->DO::WATER_RETURN_VALVE] == RUN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y003))->SetIcon(m_LedIcon[1]);
			btn->SetCheck(true);
			btn->SetWindowTextA(_T("On"));
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y003))->SetIcon(m_LedIcon[0]);
			btn->SetCheck(false);
			btn->SetWindowTextA(_T("Off"));
		}

		//jhkim
		// 확인 필요 

		//btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y004);
		//if (g_pIO->m_bDigitalOut[g_pIO->SHUTTER_1] == 1)
		//{
		//	((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y004))->SetIcon(m_LedIcon[1]);
		//	btn->SetCheck(true);
		//	btn->SetWindowTextA(_T("On"));
		//}
		//else
		//{
		//	((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y004))->SetIcon(m_LedIcon[0]);
		//	btn->SetCheck(false);
		//	btn->SetWindowTextA(_T("Off"));
		//}
		//
		//btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y005);
		//if (g_pIO->m_bDigitalOut[g_pIO->SHUTTER_2] == 1)
		//{
		//	((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y005))->SetIcon(m_LedIcon[1]);
		//	btn->SetCheck(true);
		//	btn->SetWindowTextA(_T("On"));
		//}
		//else
		//{
		//	((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y005))->SetIcon(m_LedIcon[0]);
		//	btn->SetCheck(false);
		//	btn->SetWindowTextA(_T("Off"));
		//}
		//btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y006);
		//if (g_pIO->m_bDigitalOut[g_pIO->SHUTTER_3] == 1)
		//{
		//	((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y006))->SetIcon(m_LedIcon[1]);
		//	btn->SetCheck(true);
		//	btn->SetWindowTextA(_T("On"));
		//}
		//else
		//{
		//	((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y006))->SetIcon(m_LedIcon[0]);
		//	btn->SetCheck(false);
		//	btn->SetWindowTextA(_T("Off"));
		//}
		//btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y007);
		//if (g_pIO->m_bDigitalOut[g_pIO->SHUTTER_4] == 1)
		//{
		//	((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y007))->SetIcon(m_LedIcon[1]);
		//	btn->SetCheck(true);
		//	btn->SetWindowTextA(_T("On"));
		//}
		//else
		//{
		//	((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y007))->SetIcon(m_LedIcon[0]);
		//	btn->SetCheck(false);
		//	btn->SetWindowTextA(_T("Off"));
		//}
		btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y008);
		if (g_pIO->m_bDigitalOut[g_pIO->DO::FRAME_COVER_DOOR_OPEN] == RUN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y008))->SetIcon(m_LedIcon[1]);
			btn->SetCheck(true);
			btn->SetWindowTextA(_T("On"));
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y008))->SetIcon(m_LedIcon[0]);
			btn->SetCheck(false);
			btn->SetWindowTextA(_T("Off"));
		}

		btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y009);
		if (g_pIO->m_bDigitalOut[g_pIO->DO::DVR_CAM1_SW] == RUN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y009))->SetIcon(m_LedIcon[1]);
			btn->SetCheck(true);
			btn->SetWindowTextA(_T("On"));
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y009))->SetIcon(m_LedIcon[0]);
			btn->SetCheck(false);
			btn->SetWindowTextA(_T("Off"));
		}

		btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y010);
		if (g_pIO->m_bDigitalOut[g_pIO->DO::DVR_CAM2_SW] == RUN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y010))->SetIcon(m_LedIcon[1]);
			btn->SetCheck(true);
			btn->SetWindowTextA(_T("On"));
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y010))->SetIcon(m_LedIcon[0]);
			btn->SetCheck(false);
			btn->SetWindowTextA(_T("Off"));
		}

		btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y011);
		if (g_pIO->m_bDigitalOut[g_pIO->DO::DVR_CAM3_SW] == RUN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y011))->SetIcon(m_LedIcon[1]);
			btn->SetCheck(true);
			btn->SetWindowTextA(_T("On"));
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y011))->SetIcon(m_LedIcon[0]);
			btn->SetCheck(false);
			btn->SetWindowTextA(_T("Off"));
		}

		btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y012);
		if (g_pIO->m_bDigitalOut[g_pIO->DO::DVR_CAM4_SW] == RUN)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y012))->SetIcon(m_LedIcon[1]);
			btn->SetCheck(true);
			btn->SetWindowTextA(_T("On"));
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALOUT_LINE_PART_Y012))->SetIcon(m_LedIcon[0]);
			btn->SetCheck(false);
			btn->SetWindowTextA(_T("Off"));
		}

}

void CDigitalOutputLinePart::SetOutputLinePartBitOnOff(int nIndex)
{
	int nRet;
	int ADDR = 0 , bitdex = 0;

	CButton* btn = (CButton*)GetDlgItem(IDC_CHECK_DIGITALOUT_LINE_PART_Y000 + nIndex);
	int nCheck = btn->GetCheck();

	if (g_pIO->m_bCrevis_Open_Port == TRUE)
	{
		switch (nIndex)
		{
		case 0: // LLC DRY PUMP ON/OFF
			ADDR = 12;
			bitdex = 0;
			break;
		case 1: // MC DRY PUMP ON/OFF
			ADDR = 12;
			bitdex = 1;
			break;
		case 2: // WATER SUPPLY VALVE ON/OFF
			ADDR = 13;
			bitdex = 4;
			break;
		case 3: // WATER RETURN VALVE ON/OFF
			ADDR = 13;
			bitdex = 5;
			break;
		case 4: //DOOR COVER #1
			ADDR = 11;
			bitdex = 4;
			break;
		case 5: //DOOR COVER #2
			ADDR = 11;
			bitdex = 5;
			break;
		case 6: //DOOR COVER #3
			ADDR = 11;
			bitdex = 6;
			break;
		case 7: //DOOR COVER #4
			ADDR = 11;
			bitdex = 7;
			break;
		case 8: //DOOR COVER #5
			ADDR = 12;
			bitdex = 3;
			break;
		case 9: // DVR#1 LAMP ON/OFF
			ADDR = 12;
			bitdex = 5;
			break;
		case 10: // DVR#2 LAMP ON/OFF
			ADDR = 12;
			bitdex = 6;
			break;
		case 11: // DVR#3 LAMP ON/OFF
			ADDR = 12;
			bitdex = 7;
			break;
		case 12: // DVR#4 LAMP ON/OFF
			ADDR = 13;
			bitdex = 0;
			break;
		case 13:
			break;
		case 14:
			break;
		case 15:
			break;
		default:
			break;
		}
	nRet = g_pIO->WriteOutputDataBit(ADDR, bitdex, nCheck);
	if (nRet != 0) ::AfxMessageBox("Write 실패! ", MB_ICONSTOP);	
	}
	else
	{
		//btn->SetWindowTextA(_T("Off"));
		btn->SetCheck(false);
		AfxMessageBox("IO 연결이 되어 있지 않습니다. 연결 확인 후 재 시도 해주세요");
	}


}

void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY000()
{
	SetOutputLinePartBitOnOff(0);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY001()
{
	SetOutputLinePartBitOnOff(1);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY002()
{
	SetOutputLinePartBitOnOff(2);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY003()
{
	SetOutputLinePartBitOnOff(3);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY004()
{
	SetOutputLinePartBitOnOff(4);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY005()
{
	SetOutputLinePartBitOnOff(5);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY006()
{
	SetOutputLinePartBitOnOff(6);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY007()
{
	SetOutputLinePartBitOnOff(7);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY008()
{
	SetOutputLinePartBitOnOff(8);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY009()
{
	SetOutputLinePartBitOnOff(9);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY010()
{
	SetOutputLinePartBitOnOff(10);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY011()
{
	SetOutputLinePartBitOnOff(11);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY012()
{
	SetOutputLinePartBitOnOff(12);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY013()
{
	SetOutputLinePartBitOnOff(13);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY014()
{
	SetOutputLinePartBitOnOff(14);
}


void CDigitalOutputLinePart::OnBnClickedCheckDigitaloutLinePartY015()
{
	SetOutputLinePartBitOnOff(15);
}
