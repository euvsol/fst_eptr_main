#pragma once

#include	 <vector>
#include "ProcessData.h"


struct MacroStruct;

//class Command
//{
//public:
//	virtual BOOL execute() {
//		BOOL ReturnFlag = TRUE;
//		return ReturnFlag;
//	};
//	virtual ~Command(void) {};
//};
//
//
//class Macro : public CECommon
//{
//private:
//
//public:
//	Macro();
//	~Macro();
//
//	vector <Command*> m_VectorMacroCommands;
//
//	BOOL MakeRecipeCommand(vector <MacroStruct> &GetVectorData); 
//	BOOL RecipeExecute();
//
//	void AddCommand(Command *com);
//	void Clear(); 
//
//	static double target_x_pos;
//	static double target_y_pos;
//	static double target_z_pos;
//
//};
//
//class StageMoveCommand : public Command , public CECommon 
//{
//private:
//
//public:
//
//	double x;
//	double y;
//
//	Macro MacroSystem ;
//	BOOL CommandState;
//	
//	StageMoveCommand(MacroStruct* GetVectorData);
//
//	BOOL StageMoveStart(double x_pos, double y_pos);
//
//	virtual BOOL execute()
//	{
//		MacroSystem.target_x_pos = x;
//		MacroSystem.target_y_pos = y;
//		if (CommandState)
//		{
//			return StageMoveStart(x, y);
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//
//
//};
//
//class StageStepMoveCommand : public Command, public CECommon
//{
//private:
//
//public:
//
//	double step_x_um;
//	double step_y_um;
//
//	BOOL CommandState;
//
//	StageStepMoveCommand(MacroStruct* GetVectorData);
//
//	BOOL StageStepMoveStart(double x_pos_um, double y_pos_um);
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return StageStepMoveStart(step_x_um, step_y_um);
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//
//
//};
//
//class EuvOnCommand : public Command, public CECommon
//{
//private:
//
//public:
//	BOOL CommandState;
//	EuvOnCommand(MacroStruct* GetVectorData);
//
//	BOOL EuvOn();
//
//	
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return EuvOn();
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//
//};
//
//class EuvOffCommand : public Command, public CECommon
//{
//private:
//
//public:
//	BOOL CommandState;
//	EuvOffCommand(MacroStruct* GetVectorData);
//
//	BOOL EuvOff();
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return EuvOff();
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//
//};
//
//class MaskLoadCommand : public Command, public CECommon
//{
//private:
//
//public:
//	BOOL CommandState;
//	MaskLoadCommand(MacroStruct* GetVectorData);
//
//	BOOL MaskLoad();
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return MaskLoad();
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//};
//
//class MaskUnLoadCommand : public Command, public CECommon
//{
//private:
//
//public:
//	BOOL CommandState;
//	MaskUnLoadCommand(MacroStruct* GetVectorData);
//
//	BOOL MaskUnLoad();
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return MaskUnLoad();
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//
//};
//
//class ScanStopCommand : public Command, public CECommon
//{
//private:
//
//public:
//
//	ScanStopCommand(MacroStruct* GetVectorData)
//	{
//
//	}
//
//
//	virtual BOOL execute()
//	{
//		return TRUE;
//	}
//
//};
//
//class StopCommand : public Command, public CECommon
//{
//private:
//
//public:
//
//	StopCommand(MacroStruct* GetVectorData)
//	{
//
//	}
//
//
//	virtual BOOL execute()
//	{
//		return TRUE;
//	}
//
//};
//
//class AlignCommand : public Command, public CECommon
//{
//private:
//
//public:
//	BOOL Align();
//
//	AlignCommand(MacroStruct* GetVectorData)
//	{
//
//	}
//
//
//	virtual BOOL execute()
//	{
//		return Align();
//	}
//
//};
//
//class ShutterOnCommand : public Command, public CECommon
//
//{
//private :
//
//public :
//	BOOL CommandState;
//	ShutterOnCommand(MacroStruct* GetVectorData);
//
//	BOOL ShutterOn();
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return ShutterOn();
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//};
//
//class ShutterOffCommand : public Command, public CECommon
//
//{
//private:
//
//public:
//	BOOL CommandState;
//	ShutterOffCommand(MacroStruct* GetVectorData);
//
//	BOOL ShutterOff();
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return ShutterOff();
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//};
//
//class EUVTOOMCommand : public Command, public CECommon
//{
//private:
//
//public:
//	BOOL CommandState;
//	EUVTOOMCommand(MacroStruct* GetVectorData);
//
//	BOOL ChangeEuvToOm();
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return ChangeEuvToOm();
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//};
//
//class OMTOEUVCommand : public Command, public CECommon
//{
//private:
//
//public:
//	BOOL CommandState;
//	OMTOEUVCommand(MacroStruct* GetVectorData);
//
//	BOOL ChangeOmToEuv();
//
//	int Flag;
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return ChangeOmToEuv();
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//};
//
//class ZAXISORIGINCommand : public Command, public CECommon
//{
//private:
//
//public:
//	BOOL CommandState;
//	ZAXISORIGINCommand(MacroStruct* GetVectorData);
//
//
//	BOOL ZMoveOrigin();
//
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return ZMoveOrigin();
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//};
//
//class WAITSECCommand : public Command, public CECommon
//{
//private:
//
//public:
//	BOOL CommandState;
//	WAITSECCommand(MacroStruct* GetVectorData);
//
//	BOOL WaitSecond(int sec);
//	int second;
//
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return WaitSecond(second);
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//};
//
//class ZFOCUSMOVECommand : public Command, public CECommon
//{
//private:
//
//public:
//
//	Macro MacroSystem;
//
//	BOOL ZMove(double delta_target);
//	BOOL CommandState;
//
//	double target;
//
//	ZFOCUSMOVECommand(MacroStruct* GetVectorData);
//
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			MacroSystem.target_z_pos = target;
//			return ZMove(target);
//		}
//		else
//		{
//			return FALSE;
//		}
//
//	}
//};
//
//class ThroughFocusScanCommand : public Command, public CECommon
//{
//private :
//
//	virtual ~ThroughFocusScanCommand()
//	{
//		if (ThroughScanCommandParameter != NULL)
//		{
//			delete[] ThroughScanCommandParameter;
//			ThroughScanCommandParameter = NULL;
//		}
//
//	};
//
//public :
//	double* ThroughScanCommandParameter;
//	
//	BOOL ThroughFocusScan(double* parameter);
//	BOOL CommandState;
//
//	ThroughFocusScanCommand(MacroStruct* GetVectorData);
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return ThroughFocusScan(ThroughScanCommandParameter);
//		}
//		else
//		{
//			ThroughScanCommandParameter = NULL;
//			return FALSE;
//		}
//	}
//};
//
//class StageZRelativeMoveCommand : public Command, public CECommon
//{
//private:
//	virtual ~StageZRelativeMoveCommand()
//	{
//		if (ZRelativeMoveParameter != NULL)
//		{
//			delete[] ZRelativeMoveParameter;
//			ZRelativeMoveParameter = NULL;
//		}
//	};
//public:
//
//	double* ZRelativeMoveParameter;
//
//	BOOL CommandState;
//	double pos_z_um;
//
//	StageZRelativeMoveCommand(MacroStruct* GetVectorData);
//
//	BOOL StageZRelativeMove(double step_z_um);
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return StageZRelativeMove(pos_z_um);
//		}
//		else
//		{
//			return FALSE;
//		}
//	}
//
//
//};
//
//class ScanStartCommand : public Command, public CECommon
//{
//private:
//	
//public:
//	virtual ~ScanStartCommand()
//	{
//		if (SanStartCommandParameter != NULL)
//		{
//			delete[] SanStartCommandParameter;
//			SanStartCommandParameter = NULL;
//		}
//
//	};
//	double*		SanStartCommandParameter;
//
//	Macro		MacroSystem;
//	BOOL		CommandState;
//	double		x = 0.0;
//	double		y = 0.0;
//	double		z = 0.0;
//
//	BOOL		ScanStart(double* parameter);
//			
//	ScanStartCommand(MacroStruct* GetVectorData);
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			x = MacroSystem.target_x_pos;
//			y = MacroSystem.target_y_pos;
//			z = MacroSystem.target_z_pos;
//
//			return ScanStart(SanStartCommandParameter);
//		}
//		else
//		{
//			SanStartCommandParameter = NULL;
//			return FALSE;
//		}
//	}
//};
//
//class SQOneStageMoveOnCommand : public Command, public CECommon
//{
//private:
//	virtual ~SQOneStageMoveOnCommand()
//	{
//		if (SQOneStageMoveCommandParameter != NULL)
//		{
//			delete[] SQOneStageMoveCommandParameter;
//			SQOneStageMoveCommandParameter = NULL;
//		}
//
//	};
//public:
//
//	double*		SQOneStageMoveCommandParameter;
//	double		dHorizontal;
//	double		dVertical;
//	double		dPitch;
//	double		dBeam;
//	double		dYaw;
//	double		dRoll;
//
//	BOOL		SQOneStageMoveConrtol(double* parameter);
//	BOOL		CommandState;
//
//	SQOneStageMoveOnCommand(MacroStruct* GetVectorData);
//
//	virtual BOOL execute()
//	{
//		if (CommandState)
//		{
//			return SQOneStageMoveConrtol(SQOneStageMoveCommandParameter);
//		}
//		else
//		{
//			SQOneStageMoveCommandParameter = NULL;
//			return FALSE;
//		}
//
//	}
//
//
//};
