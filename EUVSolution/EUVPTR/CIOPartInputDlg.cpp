﻿// CIOPartInputDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

// CDigitalInputPart 대화 상자

IMPLEMENT_DYNAMIC(CDigitalInputPart, CDialogEx)

CDigitalInputPart::CDigitalInputPart(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IO_PART_INPUT_DIALOG, pParent)
{

}

CDigitalInputPart::~CDigitalInputPart()
{
	m_brush.DeleteObject();
	m_brush2.DeleteObject();
	m_font.DeleteObject();
}

void CDigitalInputPart::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDigitalInputPart, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CDigitalInputPart 메시지 처리기


HBRUSH CDigitalInputPart::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if ((pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALIN_PART_TEXT) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALIN_PART_TEXT2))
		{
			pDC->SetBkColor(LIGHT_GRAY);
			//pDC->SetTextColor(RGB(0, 255, 0));
			return m_brush;
		}
		else if (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITAIN_TEXT)
		{
			pDC->SetBkColor(GRAY);
			return m_brush2;
		}
	}

	return hbr;
}


void CDigitalInputPart::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == INPUT_PART_DIALOG_TIMER)
	{
		//if (g_pIO->m_Crevis_Open_Port == TRUE)
		{
			OnUpdateDigitalInput_Part();
			SetTimer(INPUT_PART_DIALOG_TIMER, 100, NULL);
		}
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CDigitalInputPart::OnDestroy()
{
	CDialogEx::OnDestroy();
	KillTimer(INPUT_PART_DIALOG_TIMER);
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


BOOL CDigitalInputPart::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_brush.CreateSolidBrush(LIGHT_GRAY); // Gague 배경 색
	m_brush2.CreateSolidBrush(GRAY); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CDigitalInputPart::InitControls_DI_PART()
{

	CString strTemp;
	int Digital_In_Static_num = 18;

	for (int nIdx = 0; nIdx < Digital_In_Static_num; nIdx++)
	{
		//strTemp.Format(_T("X%04d"), nIdx);
		//SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM0 + nIdx, strTemp);
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y000 + nIdx))->SetIcon(m_LedIcon[0]);

	}

	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM0, "X0607");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM1, "X0608");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM2, "X0604");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM3, "X0012");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM4, "X0013");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM5, "X0612");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM6, "X0613");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM7, "X0004");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM8, "X0005");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM9, "X0002");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM10, "X0003");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM11, "X0604");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM12, "X0605");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM13, "X0606");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM14, "X0610");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM15, "X0611");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM16, "X0010");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_NUM17, "X0011");


	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y000, "MC SLOW ROUGHING V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y001, "MC FAST ROUGHING V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y002, "MC ROUGHING V/V CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y003, "MC#1 TMP GATE V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y004, "MC#1 TMP GATE V/V CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y005, "MC FORLINE#1 V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y006, "MC FORLINE#1 V/V CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y007, "TR GATE OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y008, "TR GATE CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y009, "LLC GATE OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y010, "LLC GATE CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y011, "LLC SLOW ROUGHING V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y012, "LLC FAST ROUGHING V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y013, "LLC ROUGHING V/V CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y014, "LLC FORLINE V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y015, "LLC FORLINE V/V CLOSE");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y016, "LLC TMP GATE V/V OPEN");
	SetDlgItemText(IDC_STATIC_DIGITALIN_PART_Y017, "LLC TMP GATE V/V CLOSE");

	SetTimer(INPUT_PART_DIALOG_TIMER, 100, NULL);
}

void CDigitalInputPart::OnUpdateDigitalInput_Part()
{
	//jhkm
	// 확인 필요
	// MC Slow Roughing valve

	//if (g_pIO->m_bDigitalIn[g_pIO->DI::MC_SLOW_ROUGHING_OPEN] == 1)
	//{
	//	((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y000))->SetIcon(m_LedIcon[1]);
	//}
	//else
	//{
	//	((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y000))->SetIcon(m_LedIcon[0]);
	//}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::MC_FAST_ROUGHING_VALVE_OPEN] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y001))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y001))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::MC_ROUGHING_VALVE_CLOSE] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y002))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y002))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::MC_TMP_GATE_VALVE_OPEN] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y003))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y003))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::MC_TMP_GATE_VALVE_CLOSE] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y004))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y004))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::MC_FORELINE_VALVE_OPEN] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y005))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y005))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::MC_FORELINE_VALVE_CLOSE] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y006))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y006))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::TR_GATE_VALVE_OPEN] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y007))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y007))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::TR_GATE_VALVE_CLOSE] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y008))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y008))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_GATE_VALVE_OPEN] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y009))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y009))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_GATE_VALVE_CLOSE] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y010))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y010))->SetIcon(m_LedIcon[0]);
	}

	//jhkim
	// llc slow roughing 확인필요

	//if (g_pIO->m_bDigitalIn[g_pIO->LL_SLOW_ROUGHING_OPEN] == 1)
	//{
	//	((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y011))->SetIcon(m_LedIcon[1]);
	//}
	//else
	//{
	//	((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y011))->SetIcon(m_LedIcon[0]);
	//}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_FAST_ROUGHING_VALVE_OPEN] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y012))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y012))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_ROUGHING_VALVE_CLOSE] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y013))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y013))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_FORELINE_VALVE_OPEN] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y014))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y014))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_FORELINE_VALVE_CLOSE] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y015))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y015))->SetIcon(m_LedIcon[0]);

	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_TMP_GATE_VALVE_OPEN] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y016))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y016))->SetIcon(m_LedIcon[0]);
	}
	if (g_pIO->m_bDigitalIn[g_pIO->DI::LLC_TMP_GATE_VALVE_CLOSE] == 1)
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y017))->SetIcon(m_LedIcon[1]);
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_PART_Y017))->SetIcon(m_LedIcon[0]);
	}
}