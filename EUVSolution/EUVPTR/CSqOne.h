#pragma once

class CSqOne : public CEthernetCom
{

public:
	CSqOne();
	~CSqOne();

protected:


	//int Set();
	//int Get();

	//��� ����
	CString	m_IpAddr;
	int		m_Port;
	BOOL m_ConnectionStatus;

	HANDLE m_hParsingEvent = NULL;

	struct IpConfig
	{
		char* ip;
		int port;
	};

	IpConfig ipconfig;
	char storedbuff[540] = "";

	typedef struct _SQ_DATA_ {
		double X = 0;
		double Y = 0;
		double Z = 0;
		double Rx = 0;
		double Ry = 0;
		double Rz = 0;
		double Cx = 0;
		double Cy = 0;
		double Cz = 0;
		int TargetExceedsRange = 0;
		int TriSphereStatus = 0;
		int MoveComplete = 0;
	} SQ_DATA;

	virtual int	ReceiveData(char *lParam, int nLength);	

	virtual int WaitReceiveEventThread();

	int ParseData(char * bufrecv, int nLen);		
	int m_DataSetIndex = 0;
	int MakeDataSet(char *lParam, int nLength);

public:

	void SendGetCommand();
	void SendSetCommand(double x, double y, double z, double rx, double ry, double rz, double cx, double cy, double cz);
	void SendStopCommand();
	int GetUntilReceiveData();
	int SetUntilInposition(double x, double y, double z, double rx, double ry, double rz, double cx, double cy, double cz);
	SQ_DATA	SqData;
	SQ_DATA InitialData;

	BOOL m_IsInitailsed = FALSE;
	int Open();
	void Close();
	void Stop();

	void EmergencyStop();

	CString m_SendString="send";
	CString m_ReceiveString="receive";

};

