#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

BEGIN_MESSAGE_MAP(CCameraWnd, CWnd)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_SETCURSOR()
END_MESSAGE_MAP()

CCameraWnd::CCameraWnd()
{
	m_nDigSizeX = 1544;
	m_nDigSizeY = 1544;
	m_nDigBand = 3;
	m_bCrossLine = FALSE;
	m_bMouseMove = FALSE;
	m_bMouseMoveLine = FALSE;
	m_bScaleImage = FALSE;
	m_MilImage = M_NULL;
	m_MilOMDisplay = M_NULL;

	m_MilGrayImage = M_NULL;
	m_MilScaleImage = M_NULL;

	m_bExcuteOnce = FALSE;

	MilExtremeResult = 0;
	Lut = 0;

	m_nCurrScaleMin = 0;
	m_nCurrScaleMax = 0;
	m_nSetScaleMin = 0;
	m_nSetScaleMax = 0;

	MbufAllocColor(g_milSystemGigEVision, 1, 1544, 1544, 8 + M_UNSIGNED, M_IMAGE + M_PROC, &m_MilGrayImage);
	MbufClear(m_MilGrayImage, M_COLOR_GRAY);
	MbufAllocColor(g_milSystemGigEVision, 1, 1544, 1544, 8 + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP + M_DIB + M_GDI, &m_MilScaleImage);
	MbufClear(m_MilScaleImage, M_COLOR_GRAY);

	MimAllocResult(g_milSystemGigEVision, 1, M_EXTREME_LIST, &MilExtremeResult);
	MbufAlloc1d(g_milSystemGigEVision, 256, 8 + M_UNSIGNED, M_LUT, &Lut);
}

CCameraWnd::~CCameraWnd()
{
}

void CCameraWnd::OnDestroy()
{
	CWnd::OnDestroy();

	if (MilExtremeResult != M_NULL)
	{
		MimFree(MilExtremeResult);
		MilExtremeResult = M_NULL;
	}

	if (Lut != M_NULL)
	{
		MbufFree(Lut);
		Lut = M_NULL;
	}

	if (m_MilOMDisplay != M_NULL)
	{
		MbufFree(m_MilOMDisplay);
		m_MilOMDisplay = M_NULL;
	}

	if (m_MilImage != M_NULL)
	{
		MbufFree(m_MilImage);
		m_MilImage = M_NULL;
	}

	if (m_MilGrayImage != M_NULL)
	{
		MbufFree(m_MilGrayImage);
		m_MilGrayImage = M_NULL;
	}

	if (m_MilScaleImage != M_NULL)
	{
		MbufFree(m_MilScaleImage);
		m_MilScaleImage = M_NULL;
	}
}

BOOL CCameraWnd::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CWnd::PreTranslateMessage(pMsg);
}

BOOL CCameraWnd::Create(DWORD dwStyle, RECT& rect, CWnd* pParentWnd)
{
	// create window
	BOOL ret;
	static CString className = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW, AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	ret = CWnd::Create(className, NULL, dwStyle, rect, pParentWnd, 0);

	rcClient.left = 0;
	rcClient.top = 0;
	rcClient.right = rect.right - rect.left;
	rcClient.bottom = rect.bottom - rect.top;

	int nPMRectSize = 130;
	m_RectTrackerOM.m_rect.left = rcClient.Width() / 2 - nPMRectSize;
	m_RectTrackerOM.m_rect.top = rcClient.Height() / 2 - nPMRectSize;
	m_RectTrackerOM.m_rect.right = rcClient.Width() / 2 + nPMRectSize;
	m_RectTrackerOM.m_rect.bottom = rcClient.Height() / 2 + nPMRectSize;
	m_RectTrackerOM.m_nStyle = CRectTracker::dottedLine;

	return ret;
}

void CCameraWnd::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (m_bMouseMove)
	{
		m_bMouseMoveLine = TRUE;
		m_ptLeftButtonDownPoint = point;
		Invalidate(FALSE);
	}

	if (g_pRecipe->m_bPMRectDisplay)
	{
		m_RectTrackerOM.Track(this, point, true);
	}

	CWnd::OnLButtonDown(nFlags, point);
}

void CCameraWnd::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		::AfxMessageBox(" Stage Moving Impossible!");
		return ;
	}

	if (g_pNavigationStage->Is_Loading_Positioin() == TRUE)
	{
		::AfxMessageBox(" Stage Moving Impossible!");
		return;
	}

	m_bMouseMoveLine = FALSE;
	if (m_bMouseMove == TRUE && g_pRecipe->m_bPMRectDisplay == FALSE /* && 물류동작 == FALSE && 마스크있음 == TRUE*/)
	{
		int centerx = m_nDigSizeX / 2, centery = m_nDigSizeY / 2;
		int tarpixelx = point.x - centerx;
		int tarpixely = point.y - centery;

		double current_posx_mm = 0.0, current_posy_mm = 0.0;
		current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

		double target_posx_mm = 0.0, target_posy_mm = 0.0;
		target_posx_mm = current_posx_mm - (tarpixelx * g_pConfig->m_dOM_PixelSizeX_um) / 1000.;
		target_posy_mm = current_posy_mm + (tarpixely * g_pConfig->m_dOM_PixelSizeY_um) / 1000.;

		g_pRecipe->m_bPMSuccess = FALSE;
		if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE || g_pMaskMap->m_bManualAlign == TRUE)
		{
			g_pNavigationStage->MoveAbsoluteXY_OnTheFly(target_posx_mm, target_posy_mm);
			CString sTemp;
			sTemp.Format(_T("CCameraWnd::OnLButtonUp() Event! (Target posx : %3.7f, posy : %3.7f)"), target_posx_mm, target_posy_mm);
			g_pLog->Display(0, sTemp);
		}
	}

	CWnd::OnLButtonUp(nFlags, point);
}

void CCameraWnd::OnMouseMove(UINT nFlags, CPoint point)
{
	if (m_bMouseMove && ((nFlags & MK_LBUTTON) == MK_LBUTTON))
	{
		m_ptLeftButtonDownPoint = point;
		Invalidate(false);
	}
	m_ptMousePoint = point;

	//double pixelvalue_x = 0.0, pixelvalue_y = 0.0;
	//pixelvalue_x = (double)m_nDigSizeX / rcClient.Width();
	//pixelvalue_y = (double)m_nDigSizeY / rcClient.Height();
	//int pixel_x = (int)((double)point.x * pixelvalue_x);
	//int pixel_y = (int)((double)point.y * pixelvalue_y);

	//CString strMousePoint;
	//strMousePoint.Format("point_x=%d,point_y=%d,distance_x=%d nm,distance_y=%d nm", pixel_x, pixel_y, 0.8 * pixel_x, 0.8 * pixel_y);
	//GetDlgItem(IDC_OM_STATIC)->SetWindowTextA(strMousePoint);

	CWnd::OnMouseMove(nFlags, point);
}


void CCameraWnd::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 CWnd::OnPaint()을(를) 호출하지 마십시오.

	SetBkMode(dc.m_hDC, TRANSPARENT);

	if (m_MilOMDisplay == M_NULL)
		return;

	//20210812 jkseo, Original image to scaled image.
	MimConvert(m_MilOMDisplay, m_MilGrayImage, M_RGB_TO_L);
	MimFindExtreme(m_MilGrayImage, MilExtremeResult, M_MIN_VALUE);
	MimGetResult(MilExtremeResult, M_VALUE, &m_nCurrScaleMin);
	MimFindExtreme(m_MilGrayImage, MilExtremeResult, M_MAX_VALUE);
	MimGetResult(MilExtremeResult, M_VALUE, &m_nCurrScaleMax);
	if (m_bExcuteOnce != TRUE)
	{
		m_nSetScaleMin = m_nCurrScaleMin;
		m_nSetScaleMax = m_nCurrScaleMax;
		g_pCamera->InitScaleValue(m_nSetScaleMin, m_nSetScaleMax);
		m_bExcuteOnce = TRUE;
	}
	MgenLutRamp(Lut, m_nSetScaleMin, 0x00, m_nSetScaleMax, 0xFF);
	MimLutMap(m_MilGrayImage, m_MilScaleImage, Lut);

	g_pCamera->GetCurrentScaleValue(m_nCurrScaleMin, m_nCurrScaleMax);

	if (m_bScaleImage != TRUE)
	{
		HDC hDC;
		MbufControl(m_MilOMDisplay, M_DC_ALLOC, M_DEFAULT);
		hDC = (HDC)MbufInquire(m_MilOMDisplay, M_DC_HANDLE, M_NULL);
		::StretchBlt(dc.GetSafeHdc(), rcClient.left, rcClient.top, rcClient.Width(), rcClient.Height(), hDC, 0, 0, m_nDigSizeX, m_nDigSizeY, SRCCOPY);
		MbufControl(m_MilOMDisplay, M_DC_FREE, M_DEFAULT);
	}
	else
	{
		HDC hDC;
		MbufControl(m_MilScaleImage, M_DC_ALLOC, M_DEFAULT);
		hDC = (HDC)MbufInquire(m_MilScaleImage, M_DC_HANDLE, M_NULL);
		::StretchBlt(dc.GetSafeHdc(), rcClient.left, rcClient.top, rcClient.Width(), rcClient.Height(), hDC, 0, 0, m_nDigSizeX, m_nDigSizeY, SRCCOPY);
		MbufControl(m_MilScaleImage, M_DC_FREE, M_DEFAULT);
	}

	if (m_bCrossLine)
	{
		CPen hpen, *hpenOld;
		hpen.CreatePen(PS_DOT, 1, BLUE);
		hpenOld = dc.SelectObject(&hpen);
		dc.MoveTo(0, m_nDigSizeY / 2);
		dc.LineTo(m_nDigSizeX, m_nDigSizeY / 2);
		dc.MoveTo(m_nDigSizeX / 2, 0);
		dc.LineTo(m_nDigSizeX / 2, m_nDigSizeY);
		dc.SelectObject(hpenOld);
		hpen.DeleteObject();
	}

	if (m_bMouseMoveLine == TRUE && g_pRecipe->m_bPMRectDisplay == FALSE)
	{
		CPen hpen, *hpenOld;
		hpen.CreatePen(PS_DOT, 1, RED);
		hpenOld = dc.SelectObject(&hpen);
		dc.MoveTo(0, m_ptLeftButtonDownPoint.y);
		dc.LineTo(m_nDigSizeX, m_ptLeftButtonDownPoint.y);
		dc.MoveTo(m_ptLeftButtonDownPoint.x, 0);
		dc.LineTo(m_ptLeftButtonDownPoint.x, m_nDigSizeY);
		dc.SelectObject(hpenOld);
		hpen.DeleteObject();
	}

	if (g_pRecipe->m_bPMRectDisplay)
	{
		CBrush hBrush, *pOldbrush;
		hBrush.CreateSolidBrush(WHITE);
		pOldbrush = dc.SelectObject(&hBrush);
		dc.FrameRect(m_RectTrackerOM.m_rect, &hBrush);
		dc.SelectObject(pOldbrush);
		hBrush.DeleteObject();
		CPen hpen, *hpenOld;
		hpen.CreatePen(PS_DOT, 1, YELLOW);
		hpenOld = dc.SelectObject(&hpen);
		dc.MoveTo(m_RectTrackerOM.m_rect.left, m_RectTrackerOM.m_rect.top + m_RectTrackerOM.m_rect.Height() / 2);
		dc.LineTo(m_RectTrackerOM.m_rect.right, m_RectTrackerOM.m_rect.top + m_RectTrackerOM.m_rect.Height() / 2);
		dc.MoveTo(m_RectTrackerOM.m_rect.left + m_RectTrackerOM.m_rect.Width() / 2, m_RectTrackerOM.m_rect.top);
		dc.LineTo(m_RectTrackerOM.m_rect.left + m_RectTrackerOM.m_rect.Width() / 2, m_RectTrackerOM.m_rect.bottom);
		dc.SelectObject(hpenOld);
		hpen.DeleteObject();
	}
	if (g_pRecipe->m_bPMSuccess)
	{
		int nCrossSize = 10;
		int nCrossRectSize = 15;
		CPen hpen, *hpenOld;
		hpen.CreatePen(PS_SOLID, 2, RED);
		hpenOld = dc.SelectObject(&hpen);
		dc.MoveTo(g_pRecipe->m_dPMResultPosX - nCrossSize, g_pRecipe->m_dPMResultPosY);
		dc.LineTo(g_pRecipe->m_dPMResultPosX + nCrossSize, g_pRecipe->m_dPMResultPosY);
		dc.MoveTo(g_pRecipe->m_dPMResultPosX, g_pRecipe->m_dPMResultPosY - nCrossSize);
		dc.LineTo(g_pRecipe->m_dPMResultPosX, g_pRecipe->m_dPMResultPosY + nCrossSize);
		dc.MoveTo(g_pRecipe->m_dPMResultPosX - nCrossRectSize, g_pRecipe->m_dPMResultPosY - nCrossRectSize);
		dc.LineTo(g_pRecipe->m_dPMResultPosX - nCrossRectSize, g_pRecipe->m_dPMResultPosY + nCrossRectSize);
		dc.LineTo(g_pRecipe->m_dPMResultPosX + nCrossRectSize, g_pRecipe->m_dPMResultPosY + nCrossRectSize);
		dc.LineTo(g_pRecipe->m_dPMResultPosX + nCrossRectSize, g_pRecipe->m_dPMResultPosY - nCrossRectSize);
		dc.LineTo(g_pRecipe->m_dPMResultPosX - nCrossRectSize, g_pRecipe->m_dPMResultPosY - nCrossRectSize);
		dc.SelectObject(hpenOld);
		hpen.DeleteObject();
	}

	//double pixelvalue_x = 0.0, pixelvalue_y = 0.0;
	//pixelvalue_x = (double)m_nDigSizeX / rcClient.Width();
	//pixelvalue_y = (double)m_nDigSizeY / rcClient.Height();
	int pixel_x = (int)((abs((double)m_ptMousePoint.x - m_nDigSizeX / 2)));
	int pixel_y = (int)((abs((double)m_ptMousePoint.y - m_nDigSizeY / 2)));
	CString str;
	str.Format("center_point_x=%d, center_point_y=%d, center_distance_x=%.3f um, center_distance_y=%.3f um", pixel_x, pixel_y, g_pConfig->m_dOM_PixelSizeX_um * pixel_x, g_pConfig->m_dOM_PixelSizeY_um * pixel_y);
	dc.SetTextColor(RED);
	dc.DrawText(str, rcClient, DT_LEFT | DT_WORDBREAK | DT_EDITCONTROL);

}
//1300 830  300.014 122.526
//902 687  299.659   122.656
//
//398 143 0.355 0.13
//
//0.00089
//0.00091
void CCameraWnd::OnTimer(UINT_PTR nIDEvent)
{
	//int current_naviposx_submm = 0, current_naviposy_submm = 0;
	//static int old_naviposx_submm = 0, old_naviposy_submm = 0;
	switch (nIDEvent)
	{
	case OMIMAGE_DISPLAY_TIMER:
		KillTimer(nIDEvent);
		if (m_MilImage != M_NULL)
			MbufCopy(m_MilImage, m_MilOMDisplay);
		//if (g_pNavigationStage != NULL)
		//{
		//	current_naviposx_submm = (int)(g_pNavigationStage->GetPosmm(STAGE_X_AXIS)*10.);
		//	current_naviposy_submm = (int)(g_pNavigationStage->GetPosmm(STAGE_Y_AXIS)*10.);
		//	if (abs(old_naviposx_submm - current_naviposx_submm) > 0 || abs(old_naviposy_submm - current_naviposy_submm) > 0)
		//	{
		//		old_naviposx_submm = current_naviposx_submm;
		//		old_naviposy_submm = current_naviposy_submm;
		//		Invalidate(FALSE);
		//	}
		//}
		Invalidate(FALSE);
		SetTimer(nIDEvent, 100, 0);
		break;
	case ZONEPLATE_DISPLAY_TIMER:
		KillTimer(nIDEvent);
		if (m_MilImage != M_NULL)
			MbufCopy(m_MilImage, m_MilOMDisplay);
		Invalidate(FALSE);
		SetTimer(nIDEvent, 100, 0);
		break;
	}

	CWnd::OnTimer(nIDEvent);
}


BOOL CCameraWnd::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if (g_pRecipe->m_bPMRectDisplay)
	{
		if (m_RectTrackerOM.SetCursor(this, nHitTest))
			return true;

	}
	return CWnd::OnSetCursor(pWnd, nHitTest, message);
}
