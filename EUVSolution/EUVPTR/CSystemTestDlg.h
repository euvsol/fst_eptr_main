﻿/**
 * System Test Module Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 * Description: 각종 Sequence Test Command, Test Data Result Display
 */
#pragma once


#define		STOP_TEST						0
#define		MASK_SLIP_TEST					1
#define		EUV_STAGE_REPEATABILITY_TEST	2
#define		OM_STAGE_REPEATABILITY_TEST		3
#define		STAGE_LONGRUN_TEST				4
#define		LLC_PUMP_VENT_LONGRUN_TEST		5

// CSystemTestDlg 대화 상자

class CSystemTestDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CSystemTestDlg)

public:
	CSystemTestDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CSystemTestDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SYSTEM_TEST_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedLoadTestButton();
	afx_msg void OnBnClickedUnloadTestButton();
	afx_msg void OnBnClickedSeqStopButton();
	afx_msg void OnEnChangeEditTestRepeatno();
	afx_msg void OnBnClickedCheckStagelongrunTest();
	afx_msg void OnBnClickedCheckOmStagerepeatTest();
	afx_msg void OnBnClickedCheckEuvStagerepeatTest();
	afx_msg void OnBnClickedCheckMasksliptest();
	afx_msg void OnBnClickedCheckTestend();

	BOOL m_bTestStop;
	int m_nRepeatNo;
	int m_nCurrentNo;
	int m_nTestType;
	void CheckTESTTYPE(int nTest);
	CButton m_CheckTestEndCtrl;
	CButton m_CheckMaskSlipTestCtrl;
	CButton m_CheckEUVStageRepeatTestCtrl;
	CButton m_CheckOMStageRepeatTestCtrl;
	CButton m_CheckStageLongRunTestCtrl;

	void RunMaskSlipTest();
	void SaveTestResult(int nTestType, double param1, double param2, double param3 = 0.0, double param4 = 0.0);

	int AutoRun(int nRepeatNo);
	int MaskLoad();
	int MaskUnload();
	int FindMark();

	void MachingimageCal(double x, double y, double Score , double& ptpX , double& ptpY, double& aveX, double& aveY, double& aveScore);
	double ptpx;
	double ptpy;
	double avex;
	double avey;
	double avescore;

	void VecAverageCal(vector<double> vec, double& average);
	void VecPtoPCal(vector<double> vec, double& PtoP);


	struct maching
	{
		vector<double> x;
		vector<double> y;
		vector<double> score;
	}; 

	maching machingimage;
	BOOL m_bAutoSequenceProcessing;

	static UINT LLC_Vacuum_Run_Thread(LPVOID pParam);


	CWinThread		*m_pVaccum_Run_Thread;
	CWinThread		*m_pLongRunThread;

	BOOL			m_bVacuum_Run_ThreadStop;
	BOOL			m_pSourceTestThreadStop;
	BOOL			m_pOffsetThreadStop;

	
	CString str;

	int LLC_Vacuum_Test_State;

	/* Long Run Test 횟수 사용자 지정 변수 */
	int num;

	int LLC_Start_Long_Run_Test();
	void LLC_long_run_test();

	int set_num ;
	
	void Long_Run_Error();
	void Long_Run_Stop();
	void LLC_Start_Long_Run_Test_View();


	clock_t	m_start_time, m_finish_time;
	clock_t	start, current_time;

	afx_msg void OnBnClickedLlcTestButton();
	afx_msg void OnBnClickedLoadTestButton3();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	HICON m_LedIcon[3];

	BOOL m_bRepeatabilityCheck;
	double m_dRepeatPosX;
	double m_dRepeatPosY;
	double m_dTestPtoPX;
	double m_dTestPtoPY;
	double m_dTestAVEX;
	double m_dTestAVEY;
	double m_dTestAVESCORE;
};
