﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include <iomanip>
#include <math.h>

#define  STAGE_CHECK_DATA_SIZE  50


// CSystemTestDlg 대화 상자

IMPLEMENT_DYNAMIC(CSystemTestDlg, CDialogEx)

CSystemTestDlg::CSystemTestDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SYSTEM_TEST_DIALOG, pParent)
	, m_bTestStop(TRUE)
	, m_nRepeatNo(0)
	, m_nCurrentNo(0)
	, m_bRepeatabilityCheck(FALSE)
	, m_dRepeatPosX(0)
	, m_dRepeatPosY(0)
	, m_dTestPtoPX(0)
	, m_dTestPtoPY(0)
	, m_dTestAVEX(0)
	, m_dTestAVEY(0)
	, m_dTestAVESCORE(0)
{
	m_nTestType = 0;

	m_bAutoSequenceProcessing = FALSE;

}

CSystemTestDlg::~CSystemTestDlg()
{

}

void CSystemTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK_TESTEND, m_bTestStop);
	DDX_Text(pDX, IDC_EDIT_TEST_REPEATNO, m_nRepeatNo);
	DDX_Text(pDX, IDC_EDIT_TEST_REPEATNO2, m_nCurrentNo);
	DDX_Control(pDX, IDC_CHECK_TESTEND, m_CheckTestEndCtrl);
	DDX_Control(pDX, IDC_CHECK_MASKSLIPTEST, m_CheckMaskSlipTestCtrl);
	DDX_Control(pDX, IDC_CHECK_EUV_STAGEREPEAT_TEST, m_CheckEUVStageRepeatTestCtrl);
	DDX_Control(pDX, IDC_CHECK_OM_STAGEREPEAT_TEST, m_CheckOMStageRepeatTestCtrl);
	DDX_Control(pDX, IDC_CHECK_STAGELONGRUN_TEST, m_CheckStageLongRunTestCtrl);
	DDX_Check(pDX, IDC_TEST_LOADING_REPEAT_CHECK, m_bRepeatabilityCheck);
	DDX_Text(pDX, IDC_TEST_NAVI_XPOS_EDIT, m_dRepeatPosX);
	DDX_Text(pDX, IDC_TEST_NAVI_YPOS_EDIT, m_dRepeatPosY);
	DDX_Text(pDX, IDC_EDIT_TEST_PTOPX, m_dTestPtoPX);
	DDX_Text(pDX, IDC_EDIT_TEST_PTOPY, m_dTestPtoPY);
	DDX_Text(pDX, IDC_EDIT_TEST_AVEX, m_dTestAVEX);
	DDX_Text(pDX, IDC_EDIT_TEST_AVEY, m_dTestAVEY);
	DDX_Text(pDX, IDC_EDIT_TEST_AVESCORE, m_dTestAVESCORE);
}


BEGIN_MESSAGE_MAP(CSystemTestDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_LOAD_TEST_BUTTON, &CSystemTestDlg::OnBnClickedLoadTestButton)
	ON_BN_CLICKED(IDC_UNLOAD_TEST_BUTTON, &CSystemTestDlg::OnBnClickedUnloadTestButton)
	ON_BN_CLICKED(IDC_SEQ_STOP_BUTTON, &CSystemTestDlg::OnBnClickedSeqStopButton)
	ON_EN_CHANGE(IDC_EDIT_TEST_REPEATNO, &CSystemTestDlg::OnEnChangeEditTestRepeatno)
	ON_BN_CLICKED(IDC_CHECK_STAGELONGRUN_TEST, &CSystemTestDlg::OnBnClickedCheckStagelongrunTest)
	ON_BN_CLICKED(IDC_CHECK_OM_STAGEREPEAT_TEST, &CSystemTestDlg::OnBnClickedCheckOmStagerepeatTest)
	ON_BN_CLICKED(IDC_CHECK_EUV_STAGEREPEAT_TEST, &CSystemTestDlg::OnBnClickedCheckEuvStagerepeatTest)
	ON_BN_CLICKED(IDC_CHECK_MASKSLIPTEST, &CSystemTestDlg::OnBnClickedCheckMasksliptest)
	ON_BN_CLICKED(IDC_CHECK_TESTEND, &CSystemTestDlg::OnBnClickedCheckTestend)
	ON_BN_CLICKED(IDC_LLC_TEST_BUTTON, &CSystemTestDlg::OnBnClickedLlcTestButton)
	ON_BN_CLICKED(IDC_LOAD_TEST_BUTTON3, &CSystemTestDlg::OnBnClickedLoadTestButton3)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CSystemTestDlg 메시지 처리기

BOOL CSystemTestDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

BOOL CSystemTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	str.Empty();

	ptpx = 0.0;
	ptpy = 0.0;
	avex = 0.0;
	avey = 0.0;
	avescore = 0.0;

	// ICON
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);

	LLC_Vacuum_Test_State = Pumping_START_State;
	
	set_num = 0;
	

	if (g_pNavigationStage != NULL && g_pCamera != NULL
		&& g_pWarning != NULL && g_pAdam != NULL && g_pConfig != NULL)
	{
		//SetTimer(MASK_FLATNESS_MEASUREMENT_TIMER, 100, NULL);
	}
	

	//테스트 플래그
	m_bTestStop = TRUE;

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSystemTestDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CSystemTestDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case REFERENCE_POS_CHECK_TIMER:
		//PI_Stage_Faltness_Monitor_Check();
		SetTimer(nIDEvent, 100, NULL);
		break;
	default:
		break;
	}
	__super::OnTimer(nIDEvent);
}

void CSystemTestDlg::OnBnClickedLoadTestButton()
{
	g_pLog->Display(0, _T("CSystemTestDlg::OnBnClickedLoadTestButton() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pTest);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}

	if (IDYES == AfxMessageBox("Mask를 Loading 하시겠습니까?", MB_YESNO)) MaskLoad();
}

void CSystemTestDlg::OnBnClickedUnloadTestButton()
{
	g_pLog->Display(0, _T("CSystemTestDlg::OnBnClickedUnloadTestButton() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pTest);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}

	if (IDYES != AfxMessageBox("Mask를 Unloading 하시겠습니까?", MB_YESNO)) return;

	MaskUnload();
}

void CSystemTestDlg::OnBnClickedSeqStopButton()
{
	g_pLog->Display(0, _T("CSystemTestDlg::OnBnClickedSeqStopButton() Button Click!"));

	g_pAP->StopSequence();
	m_bAutoSequenceProcessing = FALSE;
}

void CSystemTestDlg::OnEnChangeEditTestRepeatno()
{
	UpdateData(TRUE);
}

void CSystemTestDlg::OnBnClickedCheckStagelongrunTest()
{
	g_pLog->Display(0, _T("CSystemTestDlg::OnBnClickedCheckStagelongrunTest() Button Click!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if ( g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL 
		|| g_pAdam == NULL || g_pIO == NULL || g_pLLCTmp_IO == NULL || g_pMCTmp_IO == NULL)
		return;

	UpdateData(TRUE);
	int nRet = AutoRun(m_nRepeatNo);

	if (nRet != 0)
	{
		m_bAutoSequenceProcessing = FALSE;
		g_pWarning->ShowWindow(SW_HIDE);

		CString sTemp;
		sTemp.Format(_T("Fail to long run test (error code : %d)"), nRet);
		AfxMessageBox(sTemp);
	}
}

int CSystemTestDlg::AutoRun(int nRepeatNo)
{
	CString strLog;
	
	machingimage.x.clear();
	machingimage.y.clear();
	machingimage.score.clear();

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);

	g_pWarning->m_strWarningMessageVal = " Auto Sequence 가동 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	m_nCurrentNo = 0;
	m_bAutoSequenceProcessing = TRUE;
	
	
	while (m_bAutoSequenceProcessing == TRUE && m_nCurrentNo < nRepeatNo)
	{
		strLog.Format(_T(">>>> Long Run Test %d/%d Start"), m_nCurrentNo, nRepeatNo);
		SaveLogFile("LoadingSequence", strLog);

		WaitSec(2);	//혹시 가동 취소 버튼 누를 시간 확보
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
			return -2;
		}

		g_pWarning->m_strWarningMessageVal = " Mask Loading 중입니다! ";
		g_pWarning->UpdateData(FALSE);
		if (g_pAP->MaskLoadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
			return -3;
		}
		while (TRUE)
		{
			WaitSec(1);
			if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->LOADING_COMPLETE)
				break;

			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
				return -4;
			}

			if (g_pAP->m_nProcessErrorCode != 0)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
				return g_pAP->m_nProcessErrorCode;
			}
		}


		if (m_bRepeatabilityCheck == TRUE)
		{
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(m_dRepeatPosX, m_dRepeatPosY);
			FindMark();

			m_dTestPtoPX = ptpx;
			m_dTestPtoPY = ptpy;
			m_dTestAVEX = avex;
			m_dTestAVEY = avey;
			m_dTestAVESCORE = avescore;

			UpdateData(FALSE);
		}
		
		WaitSec(3);
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
			return -5;
		}

		/* Loading 끝난 후 5 분 휴식 */
		g_pWarning->m_strWarningMessageVal = " Mask Loading 후 5분 Break Time ! ";
		g_pWarning->UpdateData(FALSE);
		WaitSec(300);


		g_pRecipe->m_bPMSuccess = FALSE;
		g_pWarning->m_strWarningMessageVal = " Mask Unloading 중입니다! ";
		g_pWarning->UpdateData(FALSE);




		if (g_pAP->MaskUnloadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
			m_bAutoSequenceProcessing = FALSE;
			return -6;
		}
		while (TRUE)
		{
			WaitSec(1);
			if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->UNLOADING_COMPLETE)
				break;

			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
				return -7;
			}

			if (g_pAP->m_nProcessErrorCode != 0)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
				m_bAutoSequenceProcessing = FALSE;
				return g_pAP->m_nProcessErrorCode;
			}
		}
		WaitSec(3);

		m_nCurrentNo++;
		UpdateData(FALSE);


		/* UnLoading 끝난 후 5 분 휴식 */
		g_pWarning->m_strWarningMessageVal = " Mask UnLoading 후 5분 Break Time ! ";
		g_pWarning->UpdateData(FALSE);
		WaitSec(300);
	}

	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);

	return 0;
}

int CSystemTestDlg::FindMark()
{
	int ret = 0; CString sTemp;

	if (g_pRecipe->m_MilPMModel[PM_TEST_POSITION] > 0)
	{
		MimConvert(g_pCamera->m_CameraWnd.m_MilOMDisplay, g_pRecipe->m_MilGrayModel, M_RGB_TO_L);
		MpatSetAcceptance(g_pRecipe->m_MilPMModel[PM_TEST_POSITION], 50L);
		MpatSetCertainty(g_pRecipe->m_MilPMModel[PM_TEST_POSITION], 80L);
		MpatSetAccuracy(g_pRecipe->m_MilPMModel[PM_TEST_POSITION], M_LOW);	//M_LOW: Low accuracy(typically ± 0.20 pixels),M_MEDIUM: Medium accuracy(typically ± 0.10 pixels),M_HIGH: High accuracy(typically ± 0.05 pixels)
		MpatSetSpeed(g_pRecipe->m_MilPMModel[PM_TEST_POSITION], M_LOW);

		MpatPreprocModel(g_pRecipe->m_MilGrayModel, g_pRecipe->m_MilPMModel[PM_TEST_POSITION], M_DEFAULT);
		MpatSetNumber(g_pRecipe->m_MilPMModel[PM_TEST_POSITION], 1L);

		MpatFindModel(g_pRecipe->m_MilGrayModel, g_pRecipe->m_MilPMModel[PM_TEST_POSITION], g_pRecipe->m_MilResultModel);
		double x = 0., y = 0., angle = 0., Score = 0.;
		int getnum = MpatGetNumber(g_pRecipe->m_MilResultModel, M_NULL);
		if (getnum > 0)
		{
			MpatGetResult(g_pRecipe->m_MilResultModel, M_POSITION_X, &x);
			MpatGetResult(g_pRecipe->m_MilResultModel, M_POSITION_Y, &y);
			MpatGetResult(g_pRecipe->m_MilResultModel, M_SCORE, &Score);

			g_pRecipe->m_dPMResultPosX = x;
			g_pRecipe->m_dPMResultPosY = y;
			g_pRecipe->m_dPMScore = Score;
			
			g_pRecipe->m_bPMSuccess = TRUE;

			sTemp.Format(_T("Xpos:\t%lf\tYpos:\t%lf\tScore:\t%lf"), x, y, Score);
			SaveLogFile("LoadingResult", sTemp);
			MachingimageCal(x, y, Score, ptpx,ptpy,avex,avey,avescore);
			
		}
		else
		{
			g_pRecipe->m_bPMSuccess = FALSE;
			SaveLogFile("LoadingResult", _T("Fail to find the pattern."));
		}
	}
	else
	{
		SaveLogFile("LoadingResult", _T("There is no registered pattern."));
	}

	return 0;
}

void CSystemTestDlg::MachingimageCal(double x, double y, double Score, double& ptpX, double& ptpY, double& aveX, double& aveY, double& aveScore)
{
	machingimage.x.push_back(x);
	machingimage.y.push_back(y);
	machingimage.score.push_back(Score);

	
	VecPtoPCal(machingimage.x, ptpX);
	VecPtoPCal(machingimage.y, ptpY);
	VecAverageCal(machingimage.x, aveX);
	VecAverageCal(machingimage.y, aveY);
	VecAverageCal(machingimage.score, aveScore);

}

void CSystemTestDlg::VecAverageCal(vector<double> vec, double & average)
{
	double sum = 0.0;

	for (int i = 0; i < vec.size(); i++)
		sum += vec.at(i);
	
	average = sum / vec.size();
}

void CSystemTestDlg::VecPtoPCal(vector<double> vec, double & PtoP)
{
	double min = 100.0;
	double max = 0.0;

	for (int i = 0; i < vec.size(); i++)
	{
		if (vec.at(i) < min)
			min = vec.at(i);
		else if (vec.at(i) > max)
			max = vec.at(i);
	}

	PtoP = max - min;
}


void CSystemTestDlg::OnBnClickedCheckOmStagerepeatTest()
{
	CheckTESTTYPE(OM_STAGE_REPEATABILITY_TEST);
}

void CSystemTestDlg::OnBnClickedCheckEuvStagerepeatTest()
{
	CheckTESTTYPE(EUV_STAGE_REPEATABILITY_TEST);
}

void CSystemTestDlg::OnBnClickedCheckMasksliptest()
{
	CheckTESTTYPE(MASK_SLIP_TEST);
}

void CSystemTestDlg::OnBnClickedCheckTestend()
{
	UpdateData(TRUE);
	CheckTESTTYPE(STOP_TEST);
}

void CSystemTestDlg::CheckTESTTYPE(int nTest)
{
	if (m_bTestStop == FALSE)
	{
		::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
		return;
	}
	m_CheckTestEndCtrl.SetCheck(FALSE);
	m_CheckMaskSlipTestCtrl.SetCheck(FALSE);
	m_CheckEUVStageRepeatTestCtrl.SetCheck(FALSE);
	m_CheckOMStageRepeatTestCtrl.SetCheck(FALSE);
	m_CheckStageLongRunTestCtrl.SetCheck(FALSE);

	m_bTestStop = FALSE;
	m_nTestType = nTest;
	switch (nTest)
	{
	case STOP_TEST:
		m_CheckTestEndCtrl.SetCheck(TRUE);
		break;
	case MASK_SLIP_TEST:
		m_CheckMaskSlipTestCtrl.SetCheck(TRUE);
		RunMaskSlipTest();
		break;
	case EUV_STAGE_REPEATABILITY_TEST:
		m_CheckEUVStageRepeatTestCtrl.SetCheck(TRUE);
		break;
	case OM_STAGE_REPEATABILITY_TEST:
		m_CheckOMStageRepeatTestCtrl.SetCheck(TRUE);
		break;
	case STAGE_LONGRUN_TEST:
		m_CheckStageLongRunTestCtrl.SetCheck(TRUE);
		break;
	case LLC_PUMP_VENT_LONGRUN_TEST:
		LLC_long_run_test();
		break;
	default:
		break;
	}

	Invalidate(FALSE);
}

void CSystemTestDlg::RunMaskSlipTest()
{
	int ret = 0;
	int count = 0;
	while (m_bTestStop==FALSE && count < 300)
	{
		count++;
		if (m_bTestStop == TRUE)
			return;

		//if (g_pNavigationStage != NULL)
		//	g_pNavigationStage->MoveStageDBPositionNo(99);
		if (m_bTestStop == TRUE)
			return;
		WaitSec(10);
		//if (g_pNavigationStage != NULL)
		//	g_pNavigationStage->MoveStageDBPositionNo(99);
		if (m_bTestStop == TRUE)
			return;
		WaitSec(10);
		//if (g_pNavigationStage != NULL)
		//	g_pNavigationStage->MoveStageDBPositionNo(99);
		if (m_bTestStop == TRUE)
			return;
		WaitSec(10);

		g_pRecipe->SearchPMModel(0);
	}
}

void CSystemTestDlg::SaveTestResult(int nTestType, double param1, double param2, double param3, double param4)
{
	CString str;
	static int count = 0;
	count = count + 1;

	switch (nTestType)
	{
	case MASK_SLIP_TEST:
		if (count == 1)
			str.Format("No    Pos.X,   Pos.Y,   Angle,   Score \n");
		else
			str.Format("%d   %.6f, %.6f, %.6f,  %.1f  ", count, param1, param2, param3, param4);
		//SaveLogFile();
		break;
	case EUV_STAGE_REPEATABILITY_TEST:
		break;
	case OM_STAGE_REPEATABILITY_TEST:
		break;
	case STAGE_LONGRUN_TEST:
		break;
	}

}

int CSystemTestDlg::MaskLoad()
{
	if (g_pAP == NULL || g_pLog == NULL || g_pWarning == NULL)
		return -1;

	CAutoMessageDlg MsgBoxAuto(g_pTest);

	g_pWarning->m_strWarningMessageVal = " Auto Sequence 가동 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	m_bAutoSequenceProcessing = TRUE;

	WaitSec(3);	//혹시 가동 취소 버튼 누를 시간 확보
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		return -1;
	}

	g_pWarning->m_strWarningMessageVal = " Mask Loading 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	if (g_pAP->MaskLoadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -1;
	}

	while (TRUE)
	{
		//ProcessMessages();
		//Sleep(10);
		WaitSec(1);
		if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->LOADING_COMPLETE)
			break;

		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			return -1;
		}

		if (g_pAP->m_nProcessErrorCode != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -1;
		}
	}
	WaitSec(3);

	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Mask Loading이 완료되었습니다! "), 3);
	return 0;
}

int CSystemTestDlg::MaskUnload()
{
	if (g_pAP == NULL || g_pLog == NULL || g_pWarning == NULL)
		return -1;

	CAutoMessageDlg MsgBoxAuto(g_pTest);

	g_pWarning->m_strWarningMessageVal = " Auto Sequence 가동 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	m_bAutoSequenceProcessing = TRUE;

	WaitSec(3);
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		return -1;
	}

	g_pWarning->m_strWarningMessageVal = " Mask Unloading 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	if (g_pAP->MaskUnloadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -1;
	}
	while (TRUE)
	{
		//ProcessMessages();
		//Sleep(10);
		WaitSec(1);
		if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->UNLOADING_COMPLETE)
			break;

		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			return -1;
		}

		if (g_pAP->m_nProcessErrorCode != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -1;
		}
	}
	WaitSec(3);

	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Mask Unloading이 완료되었습니다! "), 3);
	return 0;
}

void CSystemTestDlg::OnBnClickedLlcTestButton()
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CheckTESTTYPE(LLC_PUMP_VENT_LONGRUN_TEST);
}

void CSystemTestDlg::LLC_long_run_test()
{
	CString user_num;
	CString Log_str;
	GetDlgItem(IDC_EDIT_TEST_REPEATNO)->GetWindowTextA(user_num);
	num = _ttoi(user_num);


	if (IDYES != AfxMessageBox("LLC VENTING / PUMPING Long Run Test를 시작하시겠습니까?", MB_YESNO)) return;

	if (g_pIO->Is_CREVIS_Connected() != TRUE)
	{
		Log_str = "Crevis Open Fail 이므로 Long Run Test 실행 불가";
		SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(Log_str)));
		g_pLog->Display(0, Log_str);
		return;
	}
	m_pVaccum_Run_Thread = ::AfxBeginThread(LLC_Vacuum_Run_Thread, this, THREAD_PRIORITY_NORMAL, 0, 0);
}

UINT CSystemTestDlg::LLC_Vacuum_Run_Thread(LPVOID pParam)
{
	int ret = 0;

	CSystemTestDlg*  g_pRunTest = (CSystemTestDlg*)pParam;
	
	g_pRunTest->m_bVacuum_Run_ThreadStop = FALSE;
	
	while (!g_pRunTest->m_bVacuum_Run_ThreadStop)
	{
		ret = g_pRunTest->LLC_Start_Long_Run_Test();
		g_pRunTest->LLC_Start_Long_Run_Test_View();
		if(ret != 0) g_pRunTest->m_bVacuum_Run_ThreadStop = TRUE;
	}
		
	return ret;
}

int CSystemTestDlg::LLC_Start_Long_Run_Test()
{
	int ret = 0;
	//int set_num = 0;
	CString set_num_str;

	if (set_num != num)
	{
		set_num_str.Format("%d", set_num);
		GetDlgItem(IDC_EDIT_TEST_REPEATNO2)->SetWindowTextA(set_num_str);

		if (LLC_Vacuum_Test_State == Pumping_START_State)
		{
			str = " :: LLC PUMPING START";
			SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(set_num_str + str)));
//			ret = g_pVP->LLC_Pumping_Sequence();
			
			if (ret != 0)
			{
				LLC_Vacuum_Test_State = Pumping_START_State;
				str = ":: LLC Pumping Thread Start Fail";
				SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(set_num_str + str)));
				g_pLog->Display(0, str);
				Long_Run_Error();

				return -1;
			}
			else
			{
				str = " :: LLC PUMPING END";
				SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(set_num_str + str)));
				LLC_Vacuum_Test_State = Venting_START_State;
				g_pLog->Display(0, str);
				WaitSec(10);
			}

		}

		if (LLC_Vacuum_Test_State == Venting_START_State)
		{
			str = " :: LLC VENTING START";
			SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(set_num_str + str)));
//			ret = g_pVP->LLC_Venting_Sequence();

			if (ret != 0)
			{
				LLC_Vacuum_Test_State = Venting_START_State;
				str = " :: LLC Venting Thread Start Fail";
				g_pLog->Display(0, str);
				SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(set_num_str + str)));
				Long_Run_Error();
				return -1;
			}
			else
			{
				str = " :: LLC VENTING END";
				LLC_Vacuum_Test_State = Pumping_START_State;
				SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(set_num_str + str)));
				g_pLog->Display(0, str);
				set_num++;
				WaitSec(10);
			}

		}
	}
	else
	{
		//m_bVacuum_Run_ThreadStop = TRUE;
		Long_Run_Stop();
	}
}

void CSystemTestDlg::LLC_Start_Long_Run_Test_View()
{

	switch (g_pVP->m_nSequence_State)
	{
		case LLC_Venting_PreWork_Error_State :
			break;
		case LLC_Venting_SlowVent_Error_State :
			break;
		case LLC_Venting_FastVent_Error_State :
			break;
		case LLC_Pumping_PreWork_Error_State :
			break;
		case LLC_Pumping_Slow_Rough_Error_State :
			break;
		case LLC_Pumping_Fast_Rough_Error_State :
			break;
		case LLC_Pumping_Tmp_Rough_Error_State :
			break;
		case  Pumping_ERROR_State :
			break;
		case  Venting_ERROR_State :
			break;
		case  State_IDLE_State  :
			break;
		case  Pumping_START_State  :
			break;
		case  Venting_START_State :
			break;
		case  Pumping_SLOWROUGH_State  :
			break;
		case  Pumping_FASTROUGH_State  :
			break;
		case  Pumping_TMPROUGH_State  :
			break;
		case  Pumping_COMPLETE_State  :
			break;
		case  Venting_SLOWVENT_State :
			break;
		case  Venting_FASTVENT_State  :
			break;
		case  Venting_COMPLETE_State  :
			break;
		default:
		break;
	}

}

void CSystemTestDlg::Long_Run_Stop()
{
	int ret = 0;

	if (m_bVacuum_Run_ThreadStop == FALSE)
	{
		m_bVacuum_Run_ThreadStop = TRUE;	// 쓰레드내의 while()에 들어가는 변수 => Thread를 정상 종료시킴
		Sleep(100);
	}

	// Thread Stop
	if (!m_bVacuum_Run_ThreadStop)		// 현재 쓰레드가 종료가 안되었으면
	{
		if (m_pVaccum_Run_Thread != NULL)
		{
			HANDLE threadHandle = m_pVaccum_Run_Thread->m_hThread;
			DWORD dwResult;
			dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);

			if (dwResult == WAIT_TIMEOUT)
			{
				DWORD dwExitCode = STILL_ACTIVE;
				::GetExitCodeThread(threadHandle, &dwExitCode); // *요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
				if (dwExitCode == STILL_ACTIVE)//259
				{
					//AfxEndThread(nExitCode);
					TerminateThread(threadHandle, 0/*dwExitCode*/);
					CloseHandle(threadHandle);
				}
			}
		}
		m_bVacuum_Run_ThreadStop = TRUE;
	}
}

void CSystemTestDlg::Long_Run_Error()
{
	m_bVacuum_Run_ThreadStop = TRUE;
	Long_Run_Stop();
}

//LONG RUN STOP
void CSystemTestDlg::OnBnClickedLoadTestButton3()
{
	Long_Run_Stop();
	set_num = 0;
}
