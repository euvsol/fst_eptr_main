﻿/**
 * Mask Trasfer System control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once


// CMaskEFEMDlg 대화 상자

class CMaskEFEMDlg : public CDialogEx , public CCymechsMTSCtrl, public IObserver
{
	DECLARE_DYNAMIC(CMaskEFEMDlg)

public:
	CMaskEFEMDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CMaskEFEMDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MASK_EFEM_DIALOG };
//#endif

	typedef enum {
		MTS_INIT,
		LPM_INIT,
		LPM_LOAD,
		LPM_UNLOAD,
		LPM_OPEN,
		LPM_CLOSE,
		LPM_CLAMP,
		LPM_UNCLAMP,
		ROTATOR_INIT,
		ROTATE_MASK,
		ROTATOR_ORIGIN,
		ROBOT_INIT,
		ROBOT_HOME,
		ROBOT_PICK_LPM,
		ROBOT_PLACE_LPM,
		ROBOT_PICK_ROTATOR,
		ROBOT_PLACE_ROTATOR,
		ROBOT_PICK_STAGE,
		ROBOT_PLACE_STAGE
	} MtsCommandState;

	MtsCommandState MtsState;

protected:
	HICON			m_LedIcon[3];
	int				m_nParamValue;
	CComboBox		m_comboAngleSelect;

	CWinThread*		m_pMtsThread;

	void			GetDeviceStatus();
	void			InitializeControls();
	
	static UINT MtsCommandThread(LPVOID pClass);
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()
public:
	
	int				OpenDevice();

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedPickLoadportButton();
	afx_msg void OnBnClickedPlaceLoadportButton();
	afx_msg void OnBnClickedPickAlignerButton();
	afx_msg void OnBnClickedPlaceAlignerButton();
	afx_msg void OnBnClickedPickStageButton();
	afx_msg void OnBnClickedPlaceStageButton();
	afx_msg void OnBnClickedInitMtsButton();
	afx_msg void OnBnClickedClearAlarmButton();
	afx_msg void OnBnClickedFoupLoadButton();
	afx_msg void OnBnClickedFoupUnloadButton();
	afx_msg void OnBnClickedFoupClampButton();
	afx_msg void OnBnClickedFoupUnclampButton();
	afx_msg void OnBnClickedFoupOpenButton();
	afx_msg void OnBnClickedFoupCloseButton();
	afx_msg void OnBnClickedRobotHomeButton();
	afx_msg void OnBnClickedRobotStopButton();
	afx_msg void OnBnClickedRobotSetSpeedButton();
	afx_msg void OnBnClickedAlignButton();
	afx_msg void OnBnClickedPioRetryButton();
	afx_msg void OnBnClickedPioCompleteButton();
	afx_msg void OnBnClickedPioResetButton();
	afx_msg void OnBnClickedRobotPauseCheck();
	afx_msg void OnBnClickedRfidDataButton();
	afx_msg void OnBnClickedAlignOriginButton();
	afx_msg void OnBnClickedMtsLpmInitButton();
	afx_msg void OnBnClickedMtsRobotInitButton();
	afx_msg void OnBnClickedRobotGetSpeedButton();
	afx_msg void OnBnClickedMtsAlignerInitButton();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	//물류 Sequence에서 필요로하는 함수
	int  Is_MTS_OK();												//MTS 정상상태이면 0, 아니면 Error Code 반환
	BOOL Is_MTS_Connected()			{ return m_bConnected; }
	BOOL Is_POD_OnLPM()				{ return m_bPodOnLPM; }			//MTS에 POD가 있으면 TRUE, 없으면 FALSE
	BOOL Is_POD_Open()				{ return m_bPodOpened; }
	BOOL Is_MASK_OnMTSRobot()		{ return m_bRobotWithMask; }
	BOOL Is_MASK_InPOD()			{ return m_bMaskInPod; }
	BOOL Is_MASK_InRotator()		{ return m_bRotatorWithMask; }

	BOOL Is_LPM_Initialized()		{ return m_bLpmInitComp; }
	BOOL Is_LPM_Working()			{ return m_bLpmWorking; }
	BOOL Is_LPM_Error()				{ return m_bLpmError; }

	BOOL Is_ATR_Initialized()		{ return m_bRobotInitComp; }
	BOOL Is_ATR_Working()			{ return m_bRobotWorking; }
	BOOL Is_ATR_Error()				{ return m_bRobotError; }

	BOOL Is_Rotator_Initialized()	{ return m_bRotatorInitComp; }
	BOOL Is_Rotator_Working()		{ return m_bRotatorWorking; }
	BOOL Is_Rotator_Error()			{ return m_bRotatorError; }

	int MoveReadyPos_toFrontLLC();
	int Transfer_Mask(int nSource, int nTarget);
	int Rotate_Mask(int nAngle, BOOL bLoading);
	int Load_POD();
	int Unload_POD();
	int Open_POD();
	int Close_POD();

	void EmergencyStop();

	CEdit m_ERRCodeCtrl;
	afx_msg void OnBnClickedPioManualButton();
	afx_msg void OnBnClickedPioAmhsButton();
};
