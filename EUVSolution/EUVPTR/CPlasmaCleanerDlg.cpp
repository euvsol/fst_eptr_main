﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include <vector>

using namespace std;

// CPlasmaCleanerDlg 대화 상자
#define UI_UPDATE 0
#define GETDATA	  1
#define UPDATE	  2

IMPLEMENT_DYNAMIC(CPlasmaCleanerDlg, CDialogEx)

CPlasmaCleanerDlg::CPlasmaCleanerDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PLASMA_CLEANER_DIALOG, pParent)
	, m_strActualWatt(_T("00"))
	, m_strActualWattR(_T("00"))
	, m_strActualHour(_T("00"))
	, m_strActualMinute(_T("00"))
	, m_strActualSecond(_T("00"))
	, m_strActualTorr(_T("0e0"))
	, m_strActualVDC(_T("0.0"))
	, m_strBookHour(_T("00"))
	, m_strBookMinute(_T("00"))
{
	m_currentIndex = 0;
	sampleSize = 20000;
	DataInterval = 1000;
	initialFullRange = 300;

	m_pThreadConnectBISS = NULL;
	m_pThreadCheckStatusduringCleanning = NULL;

	m_strReceiveEndOfStreamSymbol = "\n";

	m_bBISSStatus = FALSE; 
	m_bisInterlockFail = FALSE;
	m_bExitFlagConnectBISSThread = FALSE;
	m_bExitFlagInterlockCheckDuringCleanning = FALSE;

	m_nPlasmaCleaningStatus = 0;
	m_nPlasmaCleaningOldStatus = 0;

	m_nActualWatt = 0; 
	m_nActualWattR = 0;
	m_nActualHour = 0;
	m_nActualMinute = 0;
	m_nActualSecond = 0;
	m_nBookHour = 0;
	m_nBookMinute = 0;
	m_dActualTorr = 0.0;
	m_dActualVDC = 0.0;
}

CPlasmaCleanerDlg::~CPlasmaCleanerDlg()
{

}

void CPlasmaCleanerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PLASMA_CHART, m_ChartViewer);
	DDX_Control(pDX, IDC_PLASMA_SCROLLBAR, m_chartScroll);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPWATT, m_strActualWatt);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPWATTRR, m_strActualWattR);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPHOUR, m_strActualHour);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPMINUTE, m_strActualMinute);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPSECOND, m_strActualSecond);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPTORR, m_strActualTorr);
	DDX_Text(pDX, IDC_PLASMA_EDIT_DISPVDC, m_strActualVDC);
	DDX_Text(pDX, IDC_PLASMA_EDIT_BOOKHOUR, m_strBookHour);
	DDX_Text(pDX, IDC_PLASMA_EDIT_BOOKMINUTE, m_strBookMinute);
}


BEGIN_MESSAGE_MAP(CPlasmaCleanerDlg, CDialogEx)
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_UPDATE, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonShow)
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_RUN, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonRun)
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_TIMERRUN, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonTimerrun)
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_OPEN, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonOpen)
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_CLOSE, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonClose)
	ON_BN_CLICKED(IDC_PLASMA_BUTTON_CANCEL, &CPlasmaCleanerDlg::OnBnClickedPlasmaButtonCancel)
	ON_BN_CLICKED(IDC_PLASMA_CHECK, &CPlasmaCleanerDlg::OnClickedPlasmaCheck)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PLASMA_SPIN_BOOKHOUR, &CPlasmaCleanerDlg::OnDeltaposPlasmaSpinBookhour)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PLASMA_SPIN_BOOKMINUTE, &CPlasmaCleanerDlg::OnDeltaposPlasmaSpinBookminute)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PLASMA_SPIN_SETHOUR, &CPlasmaCleanerDlg::OnDeltaposPlasmaSpinSethour)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PLASMA_SPIN_SETMINUTE, &CPlasmaCleanerDlg::OnDeltaposPlasmaSpinSetminute)
	ON_NOTIFY(UDN_DELTAPOS, IDC_PLASMA_SPIN_SETSECOND, &CPlasmaCleanerDlg::OnDeltaposPlasmaSpinSetsecond)
	ON_CONTROL(CVN_ViewPortChanged, IDC_PLASMA_CHART, OnViewPortChanged)
	ON_WM_HSCROLL()
END_MESSAGE_MAP()


// CPlasmaCleanerDlg 메시지 처리기
void CPlasmaCleanerDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch (nIDEvent)
	{
	case UI_UPDATE:
		updateui();
		break;
	case GETDATA:
		getChartData();
		break;
	case UPDATE:
		OnChartUpdateTimer();
		break;
	}
	__super::OnTimer(nIDEvent);
}


void CPlasmaCleanerDlg::OnDestroy()
{
	__super::OnDestroy();

	m_bExitFlagConnectBISSThread = TRUE;
	m_bExitFlagInterlockCheckDuringCleanning = TRUE;
	KillTimer(UI_UPDATE);
	KillTimer(GETDATA);
	KillTimer(UPDATE);
	if (m_pThreadConnectBISS != NULL)
	{
		HANDLE threadHandle = m_pThreadConnectBISS->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pThreadConnectBISS = NULL;
	}

	if (m_pThreadCheckStatusduringCleanning != NULL)
	{
		HANDLE threadHandle = m_pThreadCheckStatusduringCleanning->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pThreadCheckStatusduringCleanning = NULL;
	}
	CloseTcpIpSocket();

}


BOOL CPlasmaCleanerDlg::OnInitDialog()
{
	__super::OnInitDialog();
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_PLASMA_ICON_RUN))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_PLASMA_ICON_COMMUNICATION))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MAINCHAMBERPRESSURE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_PLASMA_ICON_LOADINGPOSITION))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MATERIALPOSITION))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_PLASMA_ICON_EUV))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_PLASMA_ICON_GAGUE))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MAINCHAMBER))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_PLASMA_ICON_LOADLOCKCHAMBER))->SetIcon(m_LedIcon[0]);

	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_SETHOUR))->SetRange(0, 99);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_SETMINUTE))->SetRange(0, 59);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_SETSECOND))->SetRange(0, 59);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_BOOKHOUR))->SetRange(0, 23);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_BOOKMINUTE))->SetRange(0, 59);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_SETHOUR))->SetPos(0);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_SETMINUTE))->SetPos(0);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_SETSECOND))->SetPos(0);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_BOOKHOUR))->SetPos(0);
	((CSpinButtonCtrl*)GetDlgItem(IDC_PLASMA_SPIN_BOOKMINUTE))->SetPos(0);

	SetDlgItemTextA(IDC_PLASMA_EDIT_SETWATT, "50");
	SetDlgItemTextA(IDC_PLASMA_EDIT_SETHOUR, "03");
	SetDlgItemTextA(IDC_PLASMA_EDIT_SETMINUTE, "00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_SETSECOND, "00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_DISPWATT, "00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_DISPWATTRR, "00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_DISPHOUR, "00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_DISPMINUTE, "00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_DISPSECOND, "00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_DISPVDC, "0.0");
	SetDlgItemTextA(IDC_PLASMA_EDIT_DISPTORR, "0e0");
	SetDlgItemTextA(IDC_PLASMA_EDIT_BOOKHOUR, "00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_BOOKMINUTE, "00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_DISPCURRENTTIME, "00:00:00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_DISPSTARTTIME, "00:00:00");
	SetDlgItemTextA(IDC_PLASMA_EDIT_DISPENDTIME, "00:00:00");

	m_extBgColor = getDefaultBgColor();

	ResetChartData();

	OnClickedPlasmaCheck();

	m_pThreadConnectBISS = AfxBeginThread(ConnectBISSThreadFunc, this, THREAD_PRIORITY_NORMAL, 0, 0);
	
	((CDateTimeCtrl*)GetDlgItem(IDC_PLASMA_DATETIMEPICKER))->SetFormat("dd");

	SetTimer(UI_UPDATE, 500, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CPlasmaCleanerDlg::updateui()
{
	if (m_bConnected == TRUE)
	{
		UpdateData(TRUE);
		m_nActualWatt = _ttoi(m_strActualWatt);
		m_nActualWattR = _ttoi(m_strActualWattR);
		m_nActualHour = _ttoi(m_strActualHour);
		m_nActualMinute = _ttoi(m_strActualMinute);
		m_nActualSecond = _ttoi(m_strActualSecond);
		m_nBookHour = _ttoi(m_strBookHour);
		m_nBookMinute = _ttoi(m_strBookMinute);
		m_dActualTorr = _ttof(m_strActualTorr);
		m_dActualVDC = _ttof(m_strActualVDC);
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_COMMUNICATION))->SetIcon(m_LedIcon[1]);

		if (m_nPlasmaCleaningStatus == 0 || m_nPlasmaCleaningStatus == 1) //stand by || timer
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_RUN))->SetIcon(m_LedIcon[0]);
		}
		else if (m_nPlasmaCleaningStatus == 2) // run
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_RUN))->SetIcon(m_LedIcon[1]);
		}
		else if (m_nPlasmaCleaningStatus == -1) // error or cancel
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_RUN))->SetIcon(m_LedIcon[2]);
		}

		if (CheckLoadingPosition() == FALSE)
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_LOADINGPOSITION))->SetIcon(m_LedIcon[2]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_LOADINGPOSITION))->SetIcon(m_LedIcon[1]);
		}

		if (CheckMaterialPosition() == FALSE)
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MATERIALPOSITION))->SetIcon(m_LedIcon[2]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MATERIALPOSITION))->SetIcon(m_LedIcon[1]);
		}

		if (CheckEUVStatus() == FALSE)
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_EUV))->SetIcon(m_LedIcon[2]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_EUV))->SetIcon(m_LedIcon[1]);
		}

		if (CheckMainChamberStatus() == FALSE)
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MAINCHAMBER))->SetIcon(m_LedIcon[2]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MAINCHAMBER))->SetIcon(m_LedIcon[1]);
		}

		if (CheckGagueStatus() == FALSE)
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_GAGUE))->SetIcon(m_LedIcon[2]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_GAGUE))->SetIcon(m_LedIcon[1]);
		}

		if (CheckLoadlockChamberStatus() == FALSE)
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_LOADLOCKCHAMBER))->SetIcon(m_LedIcon[2]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_LOADLOCKCHAMBER))->SetIcon(m_LedIcon[1]);
		}

		if (m_nPlasmaCleaningStatus != 2 && m_bBISSStatus == TRUE)
		{
			if (CheckPressureBeforeCleaningStart() == FALSE)
			{
				((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MAINCHAMBERPRESSURE))->SetIcon(m_LedIcon[2]);
			}
			else
			{
				((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MAINCHAMBERPRESSURE))->SetIcon(m_LedIcon[1]);
			}
		}
		else
		{
			if (m_bBISSStatus == FALSE)
			{
				((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MAINCHAMBERPRESSURE))->SetIcon(m_LedIcon[0]);
			}
			else
			{
				((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MAINCHAMBERPRESSURE))->SetIcon(m_LedIcon[1]);
			}
		}

		if (m_bBISSStatus == TRUE)
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_BISS))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_PLASMA_ICON_BISS))->SetIcon(m_LedIcon[2]);
		}
	}
	else
	{
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_RUN))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_COMMUNICATION))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MAINCHAMBERPRESSURE))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_LOADINGPOSITION))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MATERIALPOSITION))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_EUV))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_GAGUE))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_MAINCHAMBER))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_LOADLOCKCHAMBER))->SetIcon(m_LedIcon[0]);
		((CStatic*)GetDlgItem(IDC_PLASMA_ICON_BISS))->SetIcon(m_LedIcon[0]);
	}
}

void CPlasmaCleanerDlg::setControlUI(bool status)
{
	GetDlgItem(IDC_PLASMA_EDIT_SETWATT)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_SETHOUR)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_SETMINUTE)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_SETSECOND)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_DISPWATT)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_DISPWATTRR)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_DISPHOUR)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_DISPMINUTE)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_EDIT_DISPSECOND)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_SPIN_SETHOUR)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_SPIN_SETMINUTE)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_SPIN_SETSECOND)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_BUTTON_OPEN)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_BUTTON_CLOSE)->EnableWindow(status);
	GetDlgItem(IDC_PLASMA_CHECK)->EnableWindow(status);

	if (IsDlgButtonChecked(IDC_PLASMA_CHECK) == BST_CHECKED && status == TRUE)
	{
		GetDlgItem(IDC_PLASMA_BUTTON_RUN)->EnableWindow(FALSE);
		GetDlgItem(IDC_PLASMA_BUTTON_TIMERRUN)->EnableWindow(status);
		setTimerWindowState(status);
	}
	else
	{
		GetDlgItem(IDC_PLASMA_BUTTON_RUN)->EnableWindow(status);
		GetDlgItem(IDC_PLASMA_BUTTON_TIMERRUN)->EnableWindow(FALSE);
		setTimerWindowState(FALSE);
	}
}

void CPlasmaCleanerDlg::getSetParameter()
{
	if (m_bConnected == TRUE)
	{
		int nRet = sendMessageGetSetParameter();
		if (nRet == 0) //check
		{
			Display(_T((LPSTR)(LPCTSTR)("SREM :: Set Parameter Update Finished")));	//통신 상태 기록.
		}
		else // time out error
		{
			CString strTemp;
			strTemp.Format(_T("SREM :: Fail to read setting parameters !! (Error Code : {%d})"), nRet);
			Display(strTemp);	//통신 상태 기록.
			AfxMessageBox(strTemp);
		}
	}
	else
	{
		Display(_T((LPSTR)(LPCTSTR)("SREM :: EtherNet Communication Error")));	//통신 상태 기록.
		AfxMessageBox(_T("Device not connected !!"));
	}
}

int CPlasmaCleanerDlg::checkSetParameter()
{
	int nRet = 0;
	CString strTemp;

	if (m_bConnected == TRUE)
	{
		nRet = sendMessageGetSetParameter();
		if (nRet == 0) //check
		{
			//GetDlgItemTextA(IDC_PLASMA_EDIT_SETWATT, strTemp);
			//if (_ttoi(strTemp) != m_nSetWatt)
			//{
			//	SaveLogFile("PlasmaCleaning_SREM", _T((LPSTR)(LPCTSTR)("Parameter Match Error : Watt")));	//통신 상태 기록.
			//	return -1;
			//}
			//GetDlgItemTextA(IDC_PLASMA_EDIT_SETHOUR, strTemp);
			//if (_ttoi(strTemp) != m_nSetHour)
			//{
			//	SaveLogFile("PlasmaCleaning_SREM", _T((LPSTR)(LPCTSTR)("Parameter Match Error : Hour")));	//통신 상태 기록.
			//	return -1;
			//}
			//GetDlgItemTextA(IDC_PLASMA_EDIT_SETMINUTE, strTemp);
			//if (_ttoi(strTemp) != m_nSetMinute)
			//{
			//	SaveLogFile("PlasmaCleaning_SREM", _T((LPSTR)(LPCTSTR)("Parameter Match Error : Minute")));	//통신 상태 기록.
			//	return -1;
			//}
			//GetDlgItemTextA(IDC_PLASMA_EDIT_SETSECOND, strTemp);
			//if (_ttoi(strTemp) != m_nSetSecond)
			//{
			//	SaveLogFile("PlasmaCleaning_SREM", _T((LPSTR)(LPCTSTR)("Parameter Match Error : Second")));	//통신 상태 기록.
			//	return -1;
			//}
		}
		else // time out error
		{
			strTemp.Format(_T("SREM :: Fail to read setting parameters !! sendGetParameterSet (Error Code : {%d})"), nRet);
			Display(strTemp);	//통신 상태 기록.
		}
	}
	return nRet;
}

void CPlasmaCleanerDlg::OnViewPortChanged()
{
	updateControls(&m_ChartViewer);

	if (m_ChartViewer.needUpdateChart())
		drawChart(&m_ChartViewer);
}

void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonShow()
{
	Display(_T(("SREM :: Show Button Clicked")));	//통신 상태 기록.

	if (m_bConnected == FALSE)
	{
		ShellExecute(NULL, "open", "..\\x64\\Release\\Plasma_cleaning.exe", NULL, NULL, SW_SHOW);
	}
	else
	{
		sendMessageUIStatus(TRUE);
	}
}


void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonRun()
{
	Display(_T((LPSTR)(LPCTSTR)("SREM :: Run Button Clicked")));	//통신 상태 기록.
	CString strTemp = "";
	int nTempRet = 0;
	if (m_bConnected == TRUE && m_bBISSStatus == TRUE)	// communicaton check
	{
		strTemp = "플라즈마 클리닝을 즉시 실행하시겠습니까";
		int nRet = AfxMessageBox(strTemp, MB_YESNO);
		if (nRet == IDNO)
		{
			Display(_T((LPSTR)(LPCTSTR)("SREM :: Message Box Cancel Button Clicked")));	//통신 상태 기록.
			return;
		}

		Display(_T("SREM :: Message Box Yes Button Clicked"));	//통신 상태 기록.

		if (CheckPressureBeforeCleaningStart(TRUE) != FALSE && CheckInterlock() != FALSE)
		{
			runPlasmacleaning();
		}
		else
		{
			AfxMessageBox("SREM :: Fail to start cleanning.");
		}
	}
	else
	{
		AfxMessageBox("통신 상태를 확인하세요.");
		Display(_T((LPSTR)(LPCTSTR)("SREM :: Communication Not connected")));	//통신 상태 기록.
	}
}

void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonTimerrun()
{
	Display(_T((LPSTR)(LPCTSTR)("SREM :: Timer Run Button Clicked")));	//통신 상태 기록.
	CString strTemp = "";
	BOOL isTimer = FALSE;
	int nTempRet = 0;
	if (m_bConnected == TRUE)	// communicaton check
	{
		strTemp = "플라즈마 클리닝을 타이머로 실행하시겠습니까";
		int nRet = AfxMessageBox(strTemp, MB_YESNO);
		if (nRet == IDNO)
		{
			Display(_T((LPSTR)(LPCTSTR)("SREM :: Message Box Cancel Button Clicked")));	//통신 상태 기록.
			return;
		}

		Display(_T("SREM :: Message Box Yes Button Clicked"));	//통신 상태 기록.
		if (checkTimer() == FALSE)
		{
			AfxMessageBox("Timer 설정을 확인하세요.");
			return;
		}
		runPlasmacleaning();
	
	}
	else
	{
		AfxMessageBox("통신 상태를 확인하세요.");
		Display(_T((LPSTR)(LPCTSTR)("SREM :: Communication Not connected")));	//통신 상태 기록.
	}
}

void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonCancel()
{
	Display(_T((LPSTR)(LPCTSTR)("SREM :: Cancel Button Clicked")));	//통신 상태 기록.
	sendMessageStop();
	//setControlUI(TRUE);
	if (m_bConnected == FALSE)
	{
		m_bExitFlagInterlockCheckDuringCleanning = TRUE;
	}
}

void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonOpen()
{
	Display(_T((LPSTR)(LPCTSTR)("SREM :: TCP Open button clicked!")));	//통신 상태 기록.
	if (m_bExitFlagConnectBISSThread == TRUE)
	{
		m_bExitFlagConnectBISSThread = FALSE;
		m_pThreadConnectBISS = AfxBeginThread(ConnectBISSThreadFunc, this, THREAD_PRIORITY_NORMAL, 0, 0);
	}
	else
	{
		Display(_T((LPSTR)(LPCTSTR)("SREM :: Client Thread Alread ON!")));	//통신 상태 기록.
	}
}

void CPlasmaCleanerDlg::OnBnClickedPlasmaButtonClose()
{
	Display(_T((LPSTR)(LPCTSTR)("SREM :: TCP Close button clicked!")));	//통신 상태 기록.
	if (m_bConnected == TRUE)
	{
		m_bExitFlagConnectBISSThread = TRUE;
		int nRet = WaitForSingleObject(m_pThreadConnectBISS->m_hThread, 3000);
		if (nRet != 0)
		{
			//강제 종료
		}
		else
		{
			m_pThreadConnectBISS = NULL;
		}
		CloseTcpIpSocket();
	}
	else
	{
		Display(_T((LPSTR)(LPCTSTR)("SREM :: Communication Already OFF!")));	//통신 상태 기록.
	}
}

void CPlasmaCleanerDlg::OnClickedPlasmaCheck()
{
	if (IsDlgButtonChecked(IDC_PLASMA_CHECK) == BST_CHECKED)
	{
		setTimerWindowState(TRUE);
		GetDlgItem(IDC_PLASMA_BUTTON_RUN)->EnableWindow(FALSE);
		GetDlgItem(IDC_PLASMA_BUTTON_TIMERRUN)->EnableWindow(TRUE);
	}
	else
	{
		setTimerWindowState(FALSE);
		GetDlgItem(IDC_PLASMA_BUTTON_RUN)->EnableWindow(TRUE);
		GetDlgItem(IDC_PLASMA_BUTTON_TIMERRUN)->EnableWindow(FALSE);
		((CDateTimeCtrl*)GetDlgItem(IDC_PLASMA_DATETIMEPICKER))->SetTime(&CTime::GetCurrentTime());
	}
}


void CPlasmaCleanerDlg::OnDeltaposPlasmaSpinBookhour(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString strTemp = "";
	GetDlgItemTextA(IDC_PLASMA_EDIT_BOOKHOUR, strTemp);
	pNMUpDown->iPos = (_ttoi(strTemp));
	int nValue = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nValue > -1) && (nValue < 24))
	{
		strTemp.Format(_T("%02d"), nValue);
		SetDlgItemTextA(IDC_PLASMA_EDIT_BOOKHOUR, strTemp);
		*pResult = 0;
	}
}
void CPlasmaCleanerDlg::OnDeltaposPlasmaSpinBookminute(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString strTemp = "";
	GetDlgItemTextA(IDC_PLASMA_EDIT_BOOKMINUTE, strTemp);
	pNMUpDown->iPos = (_ttoi(strTemp));
	int nValue = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nValue > -1) && (nValue < 60))
	{
		strTemp.Format(_T("%02d"), nValue);
		SetDlgItemTextA(IDC_PLASMA_EDIT_BOOKMINUTE, strTemp);
		*pResult = 0;
	}
}
void CPlasmaCleanerDlg::OnDeltaposPlasmaSpinSethour(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString strTemp = "";
	GetDlgItemTextA(IDC_PLASMA_EDIT_SETHOUR, strTemp);
	pNMUpDown->iPos = (_ttoi(strTemp));
	int nValue = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nValue > -1) && (nValue < 100))
	{
		strTemp.Format(_T("%02d"), nValue);
		SetDlgItemTextA(IDC_PLASMA_EDIT_SETHOUR, strTemp);
		*pResult = 0;
	}
}
void CPlasmaCleanerDlg::OnDeltaposPlasmaSpinSetminute(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString strTemp = "";
	GetDlgItemTextA(IDC_PLASMA_EDIT_SETMINUTE, strTemp);
	pNMUpDown->iPos = (_ttoi(strTemp));
	int nValue = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nValue > -1) && (nValue < 60))
	{
		strTemp.Format(_T("%02d"), nValue);
		SetDlgItemTextA(IDC_PLASMA_EDIT_SETMINUTE, strTemp);
		*pResult = 0;
	}
}
void CPlasmaCleanerDlg::OnDeltaposPlasmaSpinSetsecond(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString strTemp = "";
	GetDlgItemTextA(IDC_PLASMA_EDIT_SETSECOND, strTemp);
	pNMUpDown->iPos = (_ttoi(strTemp));
	int nValue = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nValue > -1) && (nValue < 60))
	{
		strTemp.Format(_T("%02d"), nValue);
		SetDlgItemTextA(IDC_PLASMA_EDIT_SETSECOND, strTemp);
		*pResult = 0;
	}
}


BOOL CPlasmaCleanerDlg::CheckInterlock()
{
	BOOL bReturn = TRUE;

	if (g_pConfig->m_nEquipmentMode == 0)
	{
		return bReturn;
	}
	else
	{
		if (CheckLoadingPosition(TRUE) == FALSE)
		{
			bReturn = FALSE;
		}

		if (CheckMaterialPosition(TRUE) == FALSE)
		{
			bReturn = FALSE;
		}

		if (CheckEUVStatus(TRUE) == FALSE)
		{
			bReturn = FALSE;
		}

		if (CheckGagueStatus(TRUE) == FALSE)
		{
			bReturn = FALSE;
		}

		if (CheckMainChamberStatus(TRUE) == FALSE)
		{
			bReturn = FALSE;
		}

		if (CheckLoadlockChamberStatus(TRUE) == FALSE)
		{
			bReturn = FALSE;
		}
	}

	return bReturn;
}


BOOL CPlasmaCleanerDlg::CheckPressureBeforeCleaningStart(BOOL isDisplay)
{
	if (g_pConfig->m_nEquipmentMode == 0)
	{
		return TRUE;
	}
	else
	{
		if (g_pVP->GetLLCVacuumStatus() != CHAMBER_PUMPED)
		{
			if(isDisplay == TRUE)
			Display(_T((LPSTR)(LPCTSTR)("SREM :: Pressure Check Fail :: Loadlock Chamber Not Pumped")));	//통신 상태 기록.
			return FALSE;
		}

		if (g_pVP->GetMCVacuumStatus() != CHAMBER_PUMPED)
		{
			if(isDisplay == TRUE)
			Display(_T((LPSTR)(LPCTSTR)("SREM :: Pressure Check Fail :: Main Chamber Not Pumped")));	//통신 상태 기록.
			return FALSE;
		}


		if (g_pGauge_IO->GetGaugeState() == GAUGE_NORMAL)
		{
			double MCPressure = g_pGauge_IO->GetMcVacuumRate();
			//double LLCPressure = g_pGauge_IO->m_dPressure_LLC;
			if (MCPressure < PLASMACLEANER_START_MCPRESSURE)
			{
				return TRUE;
			}
			else
			{
				CString strTemp = "";
				strTemp.Format(_T("SREM :: Pressure Check Fail :: Main Chamber Pressure %f"), MCPressure);
				if (isDisplay == TRUE)
				Display(_T((LPSTR)(LPCTSTR)(strTemp)));	
				return FALSE;
			}
		}
		else
		{
			if (isDisplay == TRUE)
			Display(_T((LPSTR)(LPCTSTR)("SREM :: Pressure Check Fail :: IO Gauge Abnormal")));	//통신 상태 기록.
			return FALSE;
		}
	}
}

BOOL CPlasmaCleanerDlg::CheckLoadingPosition(BOOL isDisplay)
{
	if (g_pNavigationStage->Is_Loading_Positioin() == FALSE)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: Not Loading Position")));	//통신 상태 기록.
		return FALSE;
	}
	return TRUE;
}

BOOL CPlasmaCleanerDlg::CheckMaterialPosition(BOOL isDisplay)
{
	if (g_pConfig->m_nMaterialLocation >= LLC)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: Material Not located Pod")));	//통신 상태 기록.
		return FALSE;
	}
	return TRUE;
}

BOOL CPlasmaCleanerDlg::CheckEUVStatus(BOOL isDisplay)
{

	if (g_pEUVSource->Is_EUV_On() == TRUE)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: EUV ON")));	//통신 상태 기록.
		return FALSE;
	}

	if (g_pEUVSource->Is_Shutter_Opened() == TRUE)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: MECH Shutter Open")));	//통신 상태 기록.
		return FALSE;
	}

	if (g_pEUVSource->Is_LaserShutter_Opened() == TRUE)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: Laser Shutter Open")));	//통신 상태 기록.
		return FALSE;
	}

	return TRUE;
}

BOOL CPlasmaCleanerDlg::CheckGagueStatus(BOOL isDisplay)
{
	switch (g_pGauge_IO->GetGaugeState())
	{
	case GAUGE_OFFLINE:
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: GAUGE_OFFLINE")));	//통신 상태 기록.
		return FALSE;
	case GAUGE_ERROR:
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: GAUGE_ERROR")));	//통신 상태 기록.
		return FALSE;
	default:
		break;
	}

	return TRUE;
}

BOOL CPlasmaCleanerDlg::CheckMainChamberStatus(BOOL isDisplay)
{
	switch (g_pIO->Is_MC_DryPump_Status()) {
	case DRYPUMP_WARNING:
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: DRYPUMP_WARNING")));	//통신 상태 기록.
		return FALSE;
	case DRYPUMP_ERROR:
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: DRYPUMP_ERROR")));	//통신 상태 기록.
		return FALSE;
	case DRYPUMP_STOP:
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: DRYPUMP_STOP")));	//통신 상태 기록.
		return FALSE;
	default:
		break;
	}

	switch (g_pMCTmp_IO->GetMcTmpState()) {
	case TMP_ACCELERATION:
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: TMP_ACCELERATION")));	//통신 상태 기록.
		return FALSE;
	case TMP_DECELERATION:
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: TMP_DECELERATION")));	//통신 상태 기록.
		return FALSE;
	case TMP_OFFLINE:
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: TMP_OFFLINE")));	//통신 상태 기록.
		return FALSE;
	case TMP_ERROR:
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: TMP_ERROR")));	//통신 상태 기록.
		return FALSE;
	default:
		break;
	}

	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: TR GATE VALVE CLOSED")));	//통신 상태 기록.
		return FALSE;
	}

	if (g_pIO->Is_MC_TMP_GateValve_Open() != VALVE_OPENED)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: Main Chamber Gate VALVE CLOSED")));	//통신 상태 기록.
		return FALSE;
	}

	if (g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: Main Chamber Foreline VALVE CLOSED")));	//통신 상태 기록.
		return FALSE;
	}

	if (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: Main Chamber SlowRough VALVE OPENED")));	//통신 상태 기록.
		return FALSE;
	}

	if (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: Main Chamber FastRough VALVE OPENED")));	//통신 상태 기록.
		return FALSE;
	}
	return TRUE;
}

BOOL CPlasmaCleanerDlg::CheckLoadlockChamberStatus(BOOL isDisplay)
{
	switch (g_pIO->Is_LLC_DryPump_Status()) {
	case DRYPUMP_WARNING:
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: DRYPUMP_WARNING")));	//통신 상태 기록.
		return FALSE;
	case DRYPUMP_ERROR:
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: DRYPUMP_ERROR")));	//통신 상태 기록.
		return FALSE;
	case DRYPUMP_STOP:
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: DRYPUMP_STOP")));	//통신 상태 기록.
		return FALSE;
	default:
		break;
	}

	switch (g_pLLCTmp_IO->GetLlcTmpState())		//jkseo 리턴값과 값이 다름
	{
	case g_pLLCTmp_IO->Starting:
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: TMP_ACCELERATION")));	//통신 상태 기록.
		return FALSE;
	case g_pLLCTmp_IO->Breaking:
		if (isDisplay == TRUE)
			Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: TMP_DECELERATION")));	//통신 상태 기록.
		return FALSE;
	case g_pLLCTmp_IO->Stop:
		if (isDisplay == TRUE)
			Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: TMP_OFFLINE")));	//통신 상태 기록.
		return FALSE;
	case g_pLLCTmp_IO->Fail:
		if (isDisplay == TRUE)
			Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: TMP_ERROR")));	//통신 상태 기록.
		return FALSE;
	default:
		break;
	}

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: Loadlock Gate VALVE OPENED")));	//통신 상태 기록.
		return FALSE;
	}

	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_OPENED)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: Loadlock Chamber Gate VALVE CLOSED")));	//통신 상태 기록.
		return FALSE;
	}

	if (g_pIO->Open_LLC_TMP_ForelineValve() == VALVE_OPENED)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: Loadlock Chamber Foreline VALVE CLOSED")));	//통신 상태 기록.
		return FALSE;
	}

	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: Loadlock Chamber SlowRough VALVE OPENED")));	//통신 상태 기록.
		return FALSE;
	}

	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		if (isDisplay == TRUE)
		Display(_T((LPSTR)(LPCTSTR)("SREM :: INTERLOCK CHECK FAIL :: Loadlock Chamber FastRough VALVE OPENED")));	//통신 상태 기록.
		return FALSE;
	}
	return TRUE;
}

void CPlasmaCleanerDlg::drawChart(CChartViewer * viewer)
{

	double viewPortStartDate = viewer->getValueAtViewPort("x", viewer->getViewPortLeft());
	double viewPortEndDate = viewer->getValueAtViewPort("x", viewer->getViewPortLeft() +
		viewer->getViewPortWidth());

	// Extract the part of the data arrays that are visible.
	DoubleArray viewPortTimeStamps;
	DoubleArray viewPortDataSeriesA;
	DoubleArray viewPortDataSeriesB;

	int startIndex = (int)floor(Chart::bSearch(DoubleArray(m_timeStamps, m_currentIndex), viewPortStartDate));
	int endIndex = (int)ceil(Chart::bSearch(DoubleArray(m_timeStamps, m_currentIndex), viewPortEndDate));
	int noOfPoints = endIndex - startIndex + 1;
	if (m_currentIndex > 0)
	{
		viewPortTimeStamps = DoubleArray(m_timeStamps + startIndex, noOfPoints);
		viewPortDataSeriesA = DoubleArray(m_dataSeriesA + startIndex, noOfPoints);
		viewPortDataSeriesB = DoubleArray(m_dataSeriesB + startIndex, noOfPoints);
	}

	Multiline_chart = new XYChart(1600, 400);
	Multiline_chart->setRoundedFrame(m_extBgColor);
	Multiline_chart->addTitle("Vaccum During Plasma Cleaning", "arial.ttf", 20
	)->setBackground(0xdddddd, 0x000000, Chart::glassEffect());

	Multiline_chart->setPlotArea(100, 100, 1470, 220, 0xffffff, -1, -1, 0xcccccc, 0xcccccc);
	Multiline_chart->setBackground(LIGHT_GRAY, Transparent, 0);

	if (Clear_state != true)
	{
		LegendBox *title = Multiline_chart->addLegend(55, 45, false, "arial.ttf", 15);
		title->setBackground(Chart::Transparent, Chart::Transparent);
		title->setWidth(1000);
	}

	Clear_state = false;

	Multiline_chart->xAxis()->setLabelStyle("arial.ttf", 12);
	Multiline_chart->yAxis()->setLabelStyle("arial.ttf", 12);

	Multiline_chart->xAxis()->setColors(Chart::Transparent, Chart::TextColor, Chart::TextColor, 0xaaaaaa);
	Multiline_chart->yAxis()->setColors(Chart::Transparent);

	Multiline_chart->xAxis()->setTickLength(10, 0);

	Multiline_chart->xAxis()->setTickDensity(80);
	Multiline_chart->yAxis()->setTickDensity(40);

	Multiline_chart->xAxis()->setWidth(2);
	Multiline_chart->yAxis()->setWidth(2);

	Multiline_chart->yAxis()->setTitle("Vaccum", "arial.ttf", 14, 0x555555);
	Multiline_chart->xAxis()->setTitle("Time (s)", "arial.ttf", 14, 0x555555);

	Multiline_chart->xAxis()->setLabelFormat("{value|hh:nn:ss}");
	Multiline_chart->yAxis()->setLabelFormat("{value|e1}");
	Multiline_chart->xAxis()->setDateScale(viewPortStartDate, viewPortEndDate);

	LineLayer *layer = Multiline_chart->addLineLayer();

	layer->setXData(viewPortTimeStamps);
	if (m_currentIndex > 0)
	{
		char buffer[1024];
		sprintf(buffer, "Main Chamber: <*bgColor=FFCCCC*> %01.2e ", viewPortDataSeriesA[noOfPoints - 1]);
		layer->addDataSet(viewPortDataSeriesA, 0xff0000, buffer);
		sprintf(buffer, "Loadlock Chamber: <*bgColor=CCFFCC*> %01.2e ", viewPortDataSeriesB[noOfPoints - 1]);
		layer->addDataSet(viewPortDataSeriesB, 0x00cc00, buffer);
	}
	else
	{
		layer->addDataSet(viewPortDataSeriesA, 0xff0000, "0.0");
		layer->addDataSet(viewPortDataSeriesB, 0x00cc00, "0.0");
	}

	//mark 
	Mark *yMarkTop = Multiline_chart->yAxis()->addMark(5.0e-4, 0xff0000, "High Set Point");
	yMarkTop->setLineWidth(2);
	yMarkTop->setAlignment(Chart::TopCenter);

	Mark *yMarkBottom = Multiline_chart->yAxis()->addMark(3.0e-4, 0xff0000, "Low Set Point");
	yMarkBottom->setLineWidth(2);
	yMarkBottom->setAlignment(Chart::BottomCenter);

	Multiline_chart->yAxis()->addZone(3.0e-4, 5.0e-4, 0xffcc66);
	viewer->setChart(Multiline_chart);
	delete Multiline_chart;
}

int CPlasmaCleanerDlg::getDefaultBgColor()
{
	LOGBRUSH LogBrush;
	HBRUSH hBrush = (HBRUSH)SendMessage(WM_CTLCOLORDLG, (WPARAM)CClientDC(this).m_hDC,
		(LPARAM)m_hWnd);
	::GetObject(hBrush, sizeof(LOGBRUSH), &LogBrush);
	int ret = LogBrush.lbColor;
	return ((ret & 0xff) << 16) | (ret & 0xff00) | ((ret & 0xff0000) >> 16);
}

void CPlasmaCleanerDlg::ResetChartData()
{
	StopDrawChart();
	m_currentIndex = 0;
	for (int i = 0; i < sampleSize; ++i)
	{ 
		m_timeStamps[i] = m_dataSeriesA[i] = m_dataSeriesB[i] = Chart::NoValue;
	}

	SYSTEMTIME st;
	GetLocalTime(&st);
	m_nextDataTime = Chart::chartTime(st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute,
		st.wSecond) + st.wMilliseconds / 1000.0;

	drawChart(&m_ChartViewer);
}

void CPlasmaCleanerDlg::DrawPressureChart()
{
	SetTimer(GETDATA, 1000, NULL);
	SetTimer(UPDATE, 1000, NULL);
}

void CPlasmaCleanerDlg::StopDrawChart()
{
	KillTimer(GETDATA);
	KillTimer(UPDATE);
}

void CPlasmaCleanerDlg::getChartData()
{
	SYSTEMTIME st;
	GetLocalTime(&st);
	double now = Chart::chartTime(st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute,
		st.wSecond) + st.wMilliseconds / 1000.0;
	//CTime st = CTime::GetCurrentTime();
	//double now = Chart::chartTime(st.GetYear(), st.GetMonth(), st.GetDay(), st.GetHour(), st.GetMinute(), st.GetSecond());

	do
	{
		float dataA = 0.0, dataB = 0.0;
		double p = m_nextDataTime * 4;

		if (g_pConfig->m_nEquipmentMode == 0)
		{
			dataA = 6e-7;
			dataB = 4e-4 * (20 + cos(p * 2.2) + 1 * (cos(p) * cos(p) + 0.01)) / 10 - 4e-4;
		}
		else
		{
			dataA = g_pGauge_IO->GetMcVacuumRate();
			dataB = g_pGauge_IO->GetLlcVacuumRate();
		}

		if (m_currentIndex >= sampleSize)
		{
			m_currentIndex = sampleSize * 99 / 100 - 1;

			for (int i = 0; i < m_currentIndex; ++i)
			{
				int srcIndex = i + sampleSize - m_currentIndex;
				m_timeStamps[i] = m_timeStamps[srcIndex];
				m_dataSeriesA[i] = m_dataSeriesA[srcIndex];
				m_dataSeriesB[i] = m_dataSeriesB[srcIndex];
			}
		}
		m_timeStamps[m_currentIndex] = m_nextDataTime;
		m_dataSeriesA[m_currentIndex] = dataA;
		m_dataSeriesB[m_currentIndex] = dataB;
		++m_currentIndex;

		m_nextDataTime += DataInterval / 1000.0;
	} while (m_nextDataTime < now);
	return;
}

void CPlasmaCleanerDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	double newViewPortLeft = moveScrollBar(nSBCode, nPos, pScrollBar);
	if (newViewPortLeft != m_ChartViewer.getViewPortLeft())
	{
		m_ChartViewer.setViewPortLeft(newViewPortLeft);
		m_ChartViewer.updateViewPort(true, false);
	}

	__super::OnHScroll(nSBCode, nPos, pScrollBar);
}

double CPlasmaCleanerDlg::moveScrollBar(UINT nSBCode, UINT nPos, CScrollBar * pScrollBar)
{
	SCROLLINFO info;
	info.cbSize = sizeof(SCROLLINFO);
	info.fMask = SIF_ALL;
	pScrollBar->GetScrollInfo(&info);

	int newPos = info.nPos;
	switch (nSBCode)
	{
	case SB_LEFT:
		newPos = info.nMin;
		break;
	case SB_RIGHT:
		newPos = info.nMax;
		break;
	case SB_LINELEFT:
		newPos -= (info.nPage > 10) ? info.nPage / 10 : 1;
		break;
	case SB_LINERIGHT:
		newPos += (info.nPage > 10) ? info.nPage / 10 : 1;
		break;
	case SB_PAGELEFT:
		newPos -= info.nPage;
		break;
	case SB_PAGERIGHT:
		newPos += info.nPage;
		break;
	case SB_THUMBTRACK:
		newPos = info.nTrackPos;
		break;
	}
	if (newPos < info.nMin) newPos = info.nMin;
	if (newPos > info.nMax) newPos = info.nMax;

	pScrollBar->SetScrollPos(newPos);

	return ((double)(newPos - info.nMin)) / (info.nMax - info.nMin);
}

void CPlasmaCleanerDlg::updateControls(CChartViewer * viewer)
{
	m_chartScroll.EnableWindow(viewer->getViewPortWidth() < 1);
	if (viewer->getViewPortWidth() < 1)
	{
		SCROLLINFO info;
		info.cbSize = sizeof(SCROLLINFO);
		info.fMask = SIF_ALL;
		info.nMin = 0;
		info.nMax = 0x1fffffff;
		info.nPage = (int)ceil(viewer->getViewPortWidth() * (info.nMax - info.nMin));
		info.nPos = (int)(0.5 + viewer->getViewPortLeft() * (info.nMax - info.nMin)) + info.nMin;
		m_chartScroll.SetScrollInfo(&info);
	}
}

void CPlasmaCleanerDlg::OnChartUpdateTimer()
{
	if (m_currentIndex > 0)
	{
		double startDate = m_timeStamps[0];
		double endDate = m_timeStamps[m_currentIndex - 1];

		double duration = endDate - startDate;
		if (duration < initialFullRange)
			endDate = startDate + initialFullRange;

		int updateType = Chart::ScrollWithMax;
		if (m_ChartViewer.getViewPortLeft() + m_ChartViewer.getViewPortWidth() < 0.999)
			updateType = Chart::KeepVisibleRange;
		bool scaleHasChanged = m_ChartViewer.updateFullRangeH("x", startDate, endDate, updateType);

		if (scaleHasChanged || (duration < initialFullRange))
			m_ChartViewer.updateViewPort(true, false);
	}
}

int CPlasmaCleanerDlg::connectBISSviaEtherNet()
{
	CString strtemp;
	while (m_bExitFlagConnectBISSThread == FALSE)
	{
		if (m_bConnected == FALSE)
		{
			m_nRet = OpenTcpIpSocket("127.0.0.1", 9192, FALSE);
			Sleep(500);
			if (m_nRet == 0)
			{
				m_nPlasmaCleaningStatus = 0;
				sendMessageGetSetParameter();
				sendMessageGetTimerParameter();
				sendMessageCheck();
				sendMessageGetActualParameter();
			}
		}
		Sleep(500);
	}
	return 0;
}

int CPlasmaCleanerDlg::checkStatusduringCleanning()
{
	CTime timeStart = CTime::GetCurrentTime();
	CTimeSpan spanPressure(0, 0, 2, 0);
	int nRet = -1;
	g_pPlasmaCleaner->Display((LPSTR)(LPCTSTR)("SREM :: Interlock Check Thread Start ::"));

	while (m_bExitFlagInterlockCheckDuringCleanning == FALSE && m_bConnected == TRUE && m_nPlasmaCleaningStatus == 2)
	{
		nRet = 0;
		if (g_pPlasmaCleaner->CheckInterlock() == FALSE)
		{
			if (g_pConfig->m_nEquipmentMode == 0)
			{
				int test = 0;
			}
			else
			{
				if (m_bExitFlagInterlockCheckDuringCleanning != TRUE)
				{
					g_pPlasmaCleaner->Display((LPSTR)(LPCTSTR)("SREM :: Interlock Check Thread Stop :: Interlock Fail"));
				}
				sendMessageStop();
				break;
			}
		}
		CString strTemp;
		strTemp.Format(_T("Pressure Check :: MC\t %0.3e,LLC\t %0.3e"), g_pGauge_IO->GetMcVacuumRate(), g_pGauge_IO->GetLlcVacuumRate());
		SaveLogFile("PlasmaCleaning_Pressure", (LPSTR)(LPCTSTR)strTemp);
		CTime timeCurrent = CTime::GetCurrentTime();
		if (timeCurrent >= timeStart + spanPressure)
		{
			if (g_pGauge_IO->GetMcVacuumRate() < 1e-5)
			{
				if (g_pConfig->m_nEquipmentMode == 0)
				{
					int test = 0;
				}
				else
				{
					if (m_bExitFlagInterlockCheckDuringCleanning != TRUE)
					{
						g_pPlasmaCleaner->Display((LPSTR)(LPCTSTR)("SREM :: Interlock Check Thread Stop :: Main Chambeer Pressure Error"));
					}
					sendMessageStop();
					break;
				}
			}
		}
		sendMessageGetActualParameter();
		Sleep(1000);
	}

	if (m_bConnected == FALSE)
	{
		g_pPlasmaCleaner->StopDrawChart();
		g_pPlasmaCleaner->Display((LPSTR)(LPCTSTR)("SREM :: Interlock Check Thread Close :: Communication Closed"));
		return 0;
	}
	if (m_bExitFlagInterlockCheckDuringCleanning != TRUE)
	{
		g_pPlasmaCleaner->Display((LPSTR)(LPCTSTR)("SREM :: Interlock Check Thread Close :: "));
	}
	return 0;
}

int CPlasmaCleanerDlg::ReceiveData(char * lParam)
{
	CString strTemp, strText(_T(""));
	vector<CString> vecRecvData;

	int nIndex = 0;
	while (FALSE != AfxExtractSubString(strText, lParam, nIndex, _T('\r')))
	{
		vecRecvData.push_back(strText);
		nIndex++;
	}

	for (int i = 0; i < nIndex - 1; i++)
	{
		if (vecRecvData[i].Left(1) == "I")
		{
			BOOL isPressure = CheckPressureBeforeCleaningStart(TRUE);
			BOOL isInterlock = CheckInterlock();
			if (g_pConfig->m_nEquipmentMode == 0)  //offline test
			{
				strTemp.Format(_T("I=%02d,%02d\r\n"), TRUE,TRUE);
			}
			else
			{
				strTemp.Format(_T("I=%02d,%02d\r\n"), isPressure, isInterlock);
			}
			Send((LPSTR)(LPCTSTR)strTemp, 0, 1);
			g_pPlasmaCleaner->ShowWindow(SW_SHOW);
		}
		else if (vecRecvData[i].Mid(0,4) == "SetP")  //get set value
		{
			SaveLogFile("PlasmaCleaning_SREM", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength())))); //통신 상태 기록.

			vector<CString> vecTempData;
			strText = _T("");
			SetEvent(m_hGetSetParameterEvent);

			int nIdx = 0;
			while (FALSE != AfxExtractSubString(strText, vecRecvData[i], nIdx, _T(',')))
			{
				vecTempData.push_back(strText);
				nIdx++;
			}
			SetDlgItemTextA(IDC_PLASMA_EDIT_SETWATT, vecTempData[1]);
			SetDlgItemTextA(IDC_PLASMA_EDIT_SETHOUR, vecTempData[2]);
			SetDlgItemTextA(IDC_PLASMA_EDIT_SETMINUTE, vecTempData[3]);
			SetDlgItemTextA(IDC_PLASMA_EDIT_SETSECOND, vecTempData[4]);
			vecTempData.clear();
		}
		
		else if (vecRecvData[i] == "ON") // get Watt
		{
			ResetChartData();
			DrawPressureChart();
			setControlUI(FALSE);
			m_nPlasmaCleaningStatus = 2;
			m_bExitFlagInterlockCheckDuringCleanning = FALSE;
			SetDlgItemTextA(IDC_PLASMA_EDIT_DISPCURRENTTIME, "00:00:00");
			m_pThreadCheckStatusduringCleanning = ::AfxBeginThread(CheckStatusThreadFunc, this, THREAD_PRIORITY_NORMAL, 0, 0);
			g_pPlasmaCleaner->Display("SREM :: PLASMA CLEANING ON");
			g_pPlasmaCleaner->SetWindowPos(&this->wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
		}
		else if (vecRecvData[i] == "OFF") // get Watt
		{
			StopDrawChart();
			setControlUI(TRUE);
			m_nPlasmaCleaningStatus = 0;
			g_pPlasmaCleaner->Display("SREM :: PLASMA CLEANING OFF");
			//g_pPlasmaCleaner->SetWindowPos(&this->wndNoTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
		}
		else if (vecRecvData[i] == "BISSOn") // get Watt
		{
			m_bBISSStatus = TRUE;
		}
		else if (vecRecvData[i] == "BISSOff") // get Watt
		{
			m_bBISSStatus = FALSE;
		}
		else if (vecRecvData[i].Mid(0,5) == "TimeR") // get Watt
		{
			SetDlgItemTextA(IDC_PLASMA_EDIT_DISPCURRENTTIME, vecRecvData[i].Mid(5, 8));
			setControlUI(FALSE);
			m_nPlasmaCleaningStatus = 1;
		}
		else if (vecRecvData[i].Mid(0, 4) == "Time") // get Watt
		{
			SaveLogFile("PlasmaCleaning_SREM", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength())))); //통신 상태 기록.

			vector<CString> vecTempData;
			strText = _T("");
			SetEvent(m_hGetSetParameterEvent);

			int nIdx = 0;
			while (FALSE != AfxExtractSubString(strText, vecRecvData[i], nIdx, _T(',')))
			{
				vecTempData.push_back(strText);
				nIdx++;
			}
			SetDlgItemTextA(IDC_PLASMA_EDIT_DISPSTARTTIME, vecTempData[1]);
			SetDlgItemTextA(IDC_PLASMA_EDIT_DISPENDTIME, vecTempData[2]);
			vecTempData.clear();

		}
		else if (vecRecvData[i].Mid(0, 4) == "SetT")  //get set value
		{
			SaveLogFile("PlasmaCleaning_SREM", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength())))); //통신 상태 기록.

			vector<CString> vecTempData;
			strText = _T("");
			SetEvent(m_hGetSetParameterEvent);

			int nIdx = 0;
			while (FALSE != AfxExtractSubString(strText, vecRecvData[i], nIdx, _T(',')))
			{
				vecTempData.push_back(strText);
				nIdx++;
			}
			if (vecTempData[1].Right(1) == "1")
			{
				((CButton*)GetDlgItem(IDC_PLASMA_CHECK))->SetCheck(TRUE);
				setTimerWindowState(TRUE);
			}
			else
			{
				((CButton*)GetDlgItem(IDC_PLASMA_CHECK))->SetCheck(FALSE);
				setTimerWindowState(FALSE);
			}
			SetDlgItemTextA(IDC_PLASMA_EDIT_BOOKHOUR, vecTempData[2].Mid(1, 2));
			SetDlgItemTextA(IDC_PLASMA_EDIT_BOOKMINUTE, vecTempData[3].Mid(1, 2));
			SetDlgItemTextA(IDC_PLASMA_EDIT_DISPSTARTTIME, vecTempData[4]);
			SetDlgItemTextA(IDC_PLASMA_EDIT_DISPENDTIME, vecTempData[5]);
			CTime tempTime(_ttoi(vecTempData[6]), _ttoi(vecTempData[7]), _ttoi(vecTempData[8]), _ttoi(vecTempData[2].Mid(1, 2)),_ttoi(vecTempData[3].Mid(1, 2)), 0);
			((CDateTimeCtrl*)GetDlgItem(IDC_PLASMA_DATETIMEPICKER))->SetTime(&tempTime);
			vecTempData.clear();
			OnClickedPlasmaCheck();
		}
		else if (vecRecvData[i] == "ThreadStart") // get Watt
		{

		}
		else if (vecRecvData[i] == "ThreadEnd") // get Watt
		{
			setControlUI(TRUE);
			m_nPlasmaCleaningStatus = -1;
		}
		else if (vecRecvData[i].Mid(0, 7) == "ActualP") // get Watt
		{
			SaveLogFile("PlasmaCleaning_SREM", _T((LPSTR)(LPCTSTR)("Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength())))); //통신 상태 기록.

			vector<CString> vecTempData;
			strText = _T("");
			SetEvent(m_hGetSetParameterEvent);

			int nIdx = 0;
			while (FALSE != AfxExtractSubString(strText, vecRecvData[i], nIdx, _T(',')))
			{
				vecTempData.push_back(strText);
				nIdx++;
			}
			SetDlgItemTextA(IDC_PLASMA_EDIT_DISPWATT, vecTempData[1]);
			SetDlgItemTextA(IDC_PLASMA_EDIT_DISPWATTRR, vecTempData[2]);
			SetDlgItemTextA(IDC_PLASMA_EDIT_DISPHOUR, vecTempData[3]);
			SetDlgItemTextA(IDC_PLASMA_EDIT_DISPMINUTE, vecTempData[4]);
			SetDlgItemTextA(IDC_PLASMA_EDIT_DISPSECOND, vecTempData[5]);
			SetDlgItemTextA(IDC_PLASMA_EDIT_DISPVDC, vecTempData[6]);
			SetDlgItemTextA(IDC_PLASMA_EDIT_DISPTORR, vecTempData[7]);
			if (_ttoi(vecTempData[8]) == TRUE)
			{
				m_nPlasmaCleaningStatus = 2;
			}
			vecTempData.clear();
		}
		else
		{
			SaveLogFile("PlasmaCleaning_SREM", _T((LPSTR)(LPCTSTR)("Parsing Error, Plasma -> PC : " + vecRecvData[i].Mid(0, vecRecvData[i].GetLength() - 1))));
		}
	}
	vecRecvData.clear();
	return 0;
}

int CPlasmaCleanerDlg::SendData(char * lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;

	CString strSendData = lParam;

	nRet = Send(lParam, nTimeOut, nRetryCnt);
	if (nRet != 0)
	{
		CString strTemp;
		strTemp.Format(_T("SREM -> PLASMA : SEND FAIL (%d)"), nRet);
		SaveLogFile("PlasmaCleaning_SREM", (LPSTR)(LPCTSTR)strTemp);	//통신 상태 기록.
	}
	else
		SaveLogFile("PlasmaCleaning_SREM", _T((LPSTR)(LPCTSTR)("SREM -> PLASMA : " + strSendData.Mid(0, strSendData.GetLength()-1))));	//통신 상태 기록.

	return nRet;
}

int CPlasmaCleanerDlg::sendMessageGetActualParameter()
{
	int nRet = 0;
	ResetEvent(m_hGetSetParameterEvent);
	CString strTemp = _T("GetA\r\n");
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	//if (nRet != 0)
	//{
	//	SetEvent(m_hGetSetParameterEvent);
	//}
	//else
	//{
	//	nRet = WaitEvent(m_hGetSetParameterEvent, 1000);
	//}

	return nRet;
}

int CPlasmaCleanerDlg::sendMessageGetSetParameter()
{
	int nRet = 0;
	ResetEvent(m_hGetSetParameterEvent);
	CString strTemp = _T("GetS\r\n");
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	//if (nRet != 0)
	//{
	//	SetEvent(m_hGetSetParameterEvent);
	//}
	//else
	//{
	//	nRet = WaitEvent(m_hGetSetParameterEvent, 1000);
	//}

	return nRet;
}

int CPlasmaCleanerDlg::sendMessageGetTimerParameter()
{
	int nRet = 0;
	ResetEvent(m_hGetSetParameterEvent);
	CString strTemp = _T("GetT\r\n");
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	//if (nRet != 0)
	//{
	//	SetEvent(m_hGetSetParameterEvent);
	//}
	//else
	//{
	//	nRet = WaitEvent(m_hGetSetParameterEvent, 1000);
	//}

	return nRet;
}

int CPlasmaCleanerDlg::sendMessageSetSetParameter()
{
	int nRet = 0;
	ResetEvent(m_hGetSetParameterEvent);
	CString strTemp = "", strSetWatt = "", strSetHour ="", strSetMinute = "", strSetSecond = "";
	GetDlgItemTextA(IDC_PLASMA_EDIT_SETWATT, strSetWatt);
	GetDlgItemTextA(IDC_PLASMA_EDIT_SETHOUR, strSetHour);
	GetDlgItemTextA(IDC_PLASMA_EDIT_SETMINUTE, strSetMinute);
	GetDlgItemTextA(IDC_PLASMA_EDIT_SETSECOND, strSetSecond);
	strTemp.Format(_T("SetP=W%s,H%s,M%s,S%s\r\n"), strSetWatt, strSetHour, strSetMinute, strSetSecond);
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	//if (nRet != 0)
	//{
	//	SetEvent(m_hGetSetParameterEvent);
	//}
	//else
	//{
	//	nRet = WaitEvent(m_hGetSetParameterEvent, 1000);
	//}

	return nRet;
}

int CPlasmaCleanerDlg::sendMessageSetTimerParameter()
{
	int nRet = 0;
	BOOL isChcked = FALSE;
	if (IsDlgButtonChecked(IDC_PLASMA_CHECK) == BST_CHECKED)
	{
		isChcked = TRUE;
	}
	else
	{
		isChcked = FALSE;
	}
	UpdateData(TRUE);
	ResetEvent(m_hGetSetParameterEvent);
	CString strTemp = "";
	CTime timeTemp;
	((CDateTimeCtrl*)GetDlgItem(IDC_PLASMA_DATETIMEPICKER))->GetTime(timeTemp);
	int nYear = timeTemp.GetYear();
	int nMonth = timeTemp.GetMonth();
	int nDay = timeTemp.GetDay();
	strTemp.Format(_T("SetT=S%02d,H%02d,M%02d,%04d,%02d,%02d\r\n"), isChcked, m_nBookHour, m_nBookMinute,nYear,nMonth,nDay);
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	//if (nRet != 0)
	//{
	//	SetEvent(m_hGetSetParameterEvent);
	//}
	//else
	//{
	//	nRet = WaitEvent(m_hGetSetParameterEvent, 1000);
	//}
	
	return nRet;
}

int CPlasmaCleanerDlg::sendMessageRun()
{
	int nRet = 0;
	CString strTemp = "";

	strTemp = _T("RUN\r\n");

	//ResetEvent(m_hPowerOffEvent);
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		//SetEvent(m_hpowero);
	}
	else
	{
		//	nRet = CECommon::WaitEvent(m_hPowerOffEvent, 1000);
	}
	return nRet;
}

int CPlasmaCleanerDlg::sendMessageStop()
{
	int nRet = 0;
	CString strTemp = "";

	strTemp = _T("STOP\r\n");

	//ResetEvent(m_hPowerOffEvent);
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		//SetEvent(m_hpowero);
	}
	else
	{
		//	nRet = CECommon::WaitEvent(m_hPowerOffEvent, 1000);
	}
	return nRet;
}

int CPlasmaCleanerDlg::sendMessageCheck()
{
	int nRet = 0;
	CString strTemp = "";

	strTemp = _T("CHECK\r\n");

	//ResetEvent(m_hPowerOffEvent);
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		//SetEvent(m_hpowero);
	}
	else
	{
		//	nRet = CECommon::WaitEvent(m_hPowerOffEvent, 1000);
	}
	return nRet;
}

int CPlasmaCleanerDlg::sendMessageUIStatus(BOOL status)
{
	int nRet = 0;
	CString strTemp = "";
	if (status == TRUE)
	{
		strTemp = _T("APP SHOW\r\n");
	}
	else
	{
		strTemp = _T("APP HIDE\r\n");
	}
	//ResetEvent(m_hPowerOffEvent);
	nRet = SendData((LPSTR)(LPCTSTR)strTemp, 0, 1);
	if (nRet != 0)
	{
		//SetEvent(m_hpowero);
	}
	else
	{
		//	nRet = CECommon::WaitEvent(m_hPowerOffEvent, 1000);
	}
	return nRet;
}

BOOL CPlasmaCleanerDlg::checkTimer()
{
	CTime timeSet;
	((CDateTimeCtrl*)GetDlgItem(IDC_PLASMA_DATETIMEPICKER))->GetTime(timeSet);
	CString strHour, strMinute;
	GetDlgItemTextA(IDC_PLASMA_EDIT_BOOKHOUR, strHour);
	GetDlgItemTextA(IDC_PLASMA_EDIT_BOOKMINUTE, strMinute);

	CTime timeStart(timeSet.GetYear(), timeSet.GetMonth(), timeSet.GetDay(), _ttoi(strHour), _ttoi(strMinute), 0);
	CTime timeCur = CTime::GetCurrentTime();
	if (timeStart < timeCur)
	{
		SaveLogFile("PlasmaCleaning_Status", _T((LPSTR)(LPCTSTR)("SREM :: Timer Setting Fail")));
		return FALSE;
	}
	return TRUE;
}

void CPlasmaCleanerDlg::EmergencyStop()
{
}


BOOL CPlasmaCleanerDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->wParam == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_F1)
		{
			return TRUE;
		}
		if (pMsg->wParam == VK_F2)
		{
			return TRUE;
		}
	}
	return __super::PreTranslateMessage(pMsg);
}

void CPlasmaCleanerDlg::setTimerWindowState(BOOL state)
{
	GetDlgItem(IDC_PLASMA_DATETIMEPICKER)->EnableWindow(state);
	GetDlgItem(IDC_PLASMA_EDIT_BOOKHOUR)->EnableWindow(state);
	GetDlgItem(IDC_PLASMA_EDIT_BOOKMINUTE)->EnableWindow(state);
	GetDlgItem(IDC_PLASMA_SPIN_BOOKHOUR)->EnableWindow(state);
	GetDlgItem(IDC_PLASMA_SPIN_BOOKMINUTE)->EnableWindow(state);
}

void CPlasmaCleanerDlg::runPlasmacleaning()
{
	int nRet = 0;
	nRet = sendMessageSetSetParameter();
	nRet = sendMessageSetTimerParameter();
	nRet = sendMessageRun();
	if (nRet == 0)
	{
		if (g_pConfig->m_nEquipmentMode != 0)
		{
			sendMessageUIStatus(FALSE);
		}
		return;
	}
}

int CPlasmaCleanerDlg::WaitReceiveEventThread()
{
	int		nRet = 0;
	int		i = 0;
	char*	pInBuf = new char[m_nReceiveMaxBufferSize];
	char*	pOutBuf = new char[m_nReceiveMaxBufferSize];
	int		total = 0;


	int		nCondition = 0;
	unsigned long	tmp = 0;
	struct timeval timeout;
	fd_set fds;

	SOCKET	tmpSocket = INVALID_SOCKET;
	if (m_bIsServerSocket)
	{
		tmpSocket = m_AcceptSocket;
	}
	else
	{
		tmpSocket = m_Socket;
	}

	//TRACE("TCPIP Receive Thread Start..\n");

	memset(pInBuf, '\0', m_nReceiveMaxBufferSize);
	while (m_bKillReceiveThread == FALSE)
	{
		m_bConnected = TRUE;
		timeout.tv_sec = 0;
		timeout.tv_usec = 100;
		FD_ZERO(&fds);
		FD_SET(m_Socket, &fds);
		m_nSocketStatus = select(sizeof(fds) * 8, &fds, NULL, NULL, &timeout);

		if (m_nSocketStatus == -1)
		{
			m_bKillReceiveThread = TRUE;
			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);

			CString Temp;
			Temp.Format(_T("::::::: m_nSocketStatus [ -1 ] IP [ %s ] Port [ %d ]"), m_IpAddr, m_nPort);
			SaveLogFile("TcpComm", (LPSTR)(LPCTSTR)Temp);
		}

		if (tmpSocket != INVALID_SOCKET)
		{
			memset(pOutBuf, '\0', m_nReceiveMaxBufferSize);
			int nResult = recv(tmpSocket, pInBuf, m_nReceiveMaxBufferSize, NULL);
			if (m_bKillReceiveThread)
			{
				break;
			}

			if (strcmp(m_IpAddr, "192.168.20.10") == 0)
				TRACE("nResult=%d, pInBuf=%s", nResult, pInBuf);
			/*	CString str;
				str = pInBuf;
				SaveLogFile("ADAM_Com", _T((LPSTR)(LPCTSTR)("ADAM -> MainPC_TCP/IP Socket:" + str + "Transfered")));*/
			SetEvent(m_hSocketEvent);
			total = 0;
			if (nResult > 0)
			{
				for (i = 0; i < nResult; i++)
				{
					pOutBuf[total] = pInBuf[i];
					if (pOutBuf[total] == (char)m_strReceiveEndOfStreamSymbol.GetAt(0))
					{
						pOutBuf[total] = '\0';
						nRet = ReceiveData(pOutBuf);
						m_nRet = nRet;
						memset(pOutBuf, '\0', m_nReceiveMaxBufferSize);
						total = 0;
						if (i == nResult)
							memset(pInBuf, '\0', m_nReceiveMaxBufferSize);
					}
					else
					{
						total++;
					}
				}
			}
			else
			{
				if (nResult == 0)
				{
					if (strcmp(m_IpAddr, "192.168.20.10") != 0)
					{
						m_bKillReceiveThread = TRUE;
						m_bConnected = FALSE;

						CString Temp;
						Temp.Format(_T("::::::: recv return value [ 0 ] IP [ %s ] Port [ %d ]"), m_IpAddr, m_nPort);
						SaveLogFile("TcpComm", (LPSTR)(LPCTSTR)Temp);
					}
				}


				if (nResult == SOCKET_ERROR)
				{
					m_bKillReceiveThread = TRUE;
					m_bConnected = FALSE;
					break;
				}
				//SaveLogFile("ADAM_Com", (LPSTR)(LPCTSTR)Temp); //ihlee test


			}
		}
		else
		{
			break;
		}
		//Sleep(1); //김영덕 변경
		usleep(50);
	}
	int iResult = 0;
	if (m_bIsServerSocket)
	{
		iResult = shutdown(m_AcceptSocket, SD_SEND);
		if (iResult == SOCKET_ERROR) {
			//	printf("shutdown failed with error: %d\n", WSAGetLastError());
			closesocket(m_AcceptSocket);
			WSACleanup();
		}
	}
	else
	{
		iResult = shutdown(m_Socket, SD_SEND);
		if (iResult == SOCKET_ERROR) {
			//	printf("shutdown failed with error: %d\n", WSAGetLastError());
			closesocket(m_Socket);
			WSACleanup();
		}
	}

	if (m_bIsServerSocket)
	{
		m_AcceptSocket = INVALID_SOCKET;
	}
	else
	{
		m_Socket = INVALID_SOCKET;
	}

	m_bConnected = FALSE;
	nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
	m_nRet = nRet;
	SetEvent(m_hSocketEvent);

	delete[] pInBuf;
	pInBuf = NULL;
	delete[] pOutBuf;
	pOutBuf = NULL;
	TRACE("TCPIP Receive Thread End..\n");

	//CString Temp;
	//Temp.Format(_T("::::::: Line TCPIP Socket Disconnected IP [ %s ] Port [ %d ]"), m_IpAddr, m_nPort);
	//SaveLogFile("TcpComm", (LPSTR)(LPCTSTR)Temp);

	return nRet;
}

BOOL CPlasmaCleanerDlg::Display(CString strLogMsg, BOOL bDisplay, BOOL bSaveFile)
{
	CString strOuptLog;
	CListBox* pBox = (CListBox*)GetDlgItem(IDC_PLASMA_LIST_LOG);
	if (pBox == NULL)
	{
		return FALSE;
	}
	int numItem = pBox->GetCount();
	if (numItem >= 1000)
	{
		pBox->ResetContent();
	}

	if (bDisplay)
	{
		CString strDate, strTime;
		GetSystemDateTime(&strDate, &strTime);

		CString str;
		str.Format("%s %s\r\n", strTime, strLogMsg);

		pBox->SetCurSel(pBox->AddString(str));
		pBox->GetHorizontalExtent();
		pBox->SetHorizontalExtent(256);
	}

	if (bSaveFile)
	{
		SaveLogFile("PlasmaCleaning_Status", (LPSTR)(LPCTSTR)strLogMsg);
	}

	return TRUE;
}

