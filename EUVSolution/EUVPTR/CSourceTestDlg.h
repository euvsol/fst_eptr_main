﻿#pragma once


// CSourceTestDlg 대화 상자

class CSourceTestDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSourceTestDlg)

public:
	CSourceTestDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CSourceTestDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SOURCE_TEST_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
};
