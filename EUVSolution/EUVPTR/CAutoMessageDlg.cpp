﻿// CAutoMessageDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "include.h"

// CAutoMessageDlg 대화 상자

IMPLEMENT_DYNAMIC(CAutoMessageDlg, CDialogEx)

CAutoMessageDlg::CAutoMessageDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_AUTOMESSAGE_DIALOG, pParent)
	, m_strMessage(_T(""))
{
	m_strMessage = _T("");
	m_WaitTime = 1;
}

CAutoMessageDlg::~CAutoMessageDlg()
{
}

void CAutoMessageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_MESSAGE_TXT, m_strMessage);
}

BEGIN_MESSAGE_MAP(CAutoMessageDlg, CDialogEx)
	ON_WM_TIMER()
END_MESSAGE_MAP()

// CAutoMessageDlg 메시지 처리기
INT_PTR CAutoMessageDlg::DoModal(CString str, double WaitTime)
{
	m_strMessage = str;
	m_WaitTime = WaitTime;
	return CDialogEx::DoModal();
}

BOOL CAutoMessageDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	UpdateData(false);
	SetTimer(1, m_WaitTime * 1000, NULL);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CAutoMessageDlg::OnOK()
{
	CDialogEx::OnOK();
}

void CAutoMessageDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(1);
	OnOK();
	CDialogEx::OnTimer(nIDEvent);
}
