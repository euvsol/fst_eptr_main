﻿/**
 * Equipment Configuration Editor Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once

#define DATA_STRING_SIZE		128

 // CConfigurationEditorDlg 대화 상자
class CConfigurationEditorDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CConfigurationEditorDlg)

public:
	CConfigurationEditorDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CConfigurationEditorDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CONFIG_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	
	HICON			m_LedIcon[3];

	void			InitialICon();
	void			PortCheck();

	void			PortOpen(int OpenModuleNumber);
	void			PortClose(int OpenModuleNumber);

	CFont	m_font;

	int				ReadFile();
	int				SaveFile();

	int				m_nEquipmentMode;					///< 0: OFF_LINE, 1: ONLINE
	int				m_nEquipmentType;					///< 0: SREM033 1~: Other Equipment(TBD)

//[Communication Config]
	//ETHERNET
	char			m_chNAME[ETHERNET_COM_NUMBER][125];
	char			m_chIP[ETHERNET_COM_NUMBER][125];
	char			m_chIPPORT[ETHERNET_COM_NUMBER][125];
	int				m_nPORT[ETHERNET_COM_NUMBER];
	//COM
	char			m_chPORT[SERIAL_COM_NUMBER][125];
	char			m_ch_nBAUD_RATE[SERIAL_COM_NUMBER][125];
	char			m_ch_nUSE_BIT[SERIAL_COM_NUMBER][125];
	char			m_ch_nSTOP_BIT[SERIAL_COM_NUMBER][125];
	char			m_ch_nPARITY[SERIAL_COM_NUMBER][125];

	int				m_nBAUD_RATE[SERIAL_COM_NUMBER];
	int				m_nUSE_BIT[SERIAL_COM_NUMBER];
	int				m_nSTOP_BIT[SERIAL_COM_NUMBER];
	int				m_nPARITY[SERIAL_COM_NUMBER];

	//IO
	char			m_chDi[DIGITAL_INPUT_NUMBER][125];
	char			m_chDo[DIGITAL_OUTPUT_NUMBER][125];
	char			m_chAi[ANALOG_INPUT_NUMBER][125];
	char			m_chAo[ANALOG_INPUT_NUMBER][125];

	// Ethernet 정보 읽어 오는 함수
	int			EthernetRead();

	// Ethernet 정보 파일에 쓰는 함수
	int			EthernetSave();

	// Serial 정보 읽어 오는 함수
	int			SerialRead();

	// Serial 정보 파일에 쓰는 함수
	int			SerialSave();

	int			ip_config_mode;
	int			serial_config_mode;

	afx_msg void OnBnClickedButtonConfigIpLoad();
	afx_msg void OnBnClickedButtonConfigIpEdit();
	afx_msg void OnBnClickedButtonConfigIpSave();
	afx_msg void OnBnClickedButtonConfigComLoad();
	afx_msg void OnBnClickedButtonConfigComEdit();
	afx_msg void OnBnClickedButtonConfigComSave();

//[Vacuum Config]
	double	m_dPressure_ChangeToFast_MC_Rough_ini;
	double	m_dPressure_ChangeToFast_Rough_ini;
	double	m_dPressure_ChangeToMCTMP_Rough_ini;
	double	m_dPressure_Rough_End_ini;
	double	m_dPressure_ChangeToSlow_Vent_ini;
	double	m_dPressure_Vent_End_ini;
	double	m_dPressure_ChangeToVent_ini;
	double	m_dPressure_ChangeToSlow2_Vent_ini;
	double	m_dPressure_ChangeToVent_LLC_ini;
	double	m_dPressure_ChangeToTMP_Rough_ini;

	double  m_dPressure_ChangeToVent;			//McStandbyVentValue
	double	m_dPressure_ChangeToSlow_Vent;		//McSlowVentValue
	double	m_dPressure_Vent_End;				//LlcFastVentValue, McFastVentValue
	double	m_dPressure_ChangeToFast_MC_Rough;	//McSlowRoughValue
	double  m_dPressure_ChangeToMCTMP_Rough;	//McFastRoughValue
	double	m_dPressure_Rough_End;				//LlcTmpRoughValue, McTmpRoughValue

	double	m_dPressure_ChangeToVent_LLC;		//LlcStandbyVentValue
	double	m_dPressure_ChangeToSlow2_Vent;		//LlcSlowVentValue
	double	m_dPressure_ChangeToFast_Rough;		//LlcSlowRoughValue
	double	m_dPressure_ChangeToTMP_Rough;		//LlcFastRoughValue
	
	double	m_dPressure_ChangeToFast_Vent;
	double	m_dPressure_Vent_Tolerance;

	int		m_nTimeout_sec_MCLLCVent;		//McStandbyVentTime
	int		m_nTimeout_sec_MCSlow1Vent;		//McSlowVentTime
	int		m_nTimeout_sec_MCFastVent;		//McFastVentTime
	int		m_nTimeout_sec_MCSlowRough;		//McSlowRoughTime
	int		m_nTimeout_sec_MCFastRough;		//McFastRoughTime
	int		m_nTimeout_sec_MCTmpEnd;		//McTmpRoughTime

	int     m_nTimeout_sec_LLKStandbyVent;  //LlcStandbyVentTime
	int		m_nTimeout_sec_LLKSlow1Vent;    //LlcSlowVentTime
	int		m_nTimeout_sec_LLKFastVent;		//LlcFastVentTime
	int		m_nTimeout_sec_LLKSlowRough;	//LlcSlowRoughTime
	int		m_nTimeout_sec_LLKFastRough;	//LlcFastRoughTime
	int		m_nTimeout_sec_LLKRoughEnd;		//LlcTmpRoughTime
	
	int		m_nTimeout_sec_LLKVentEnd;
	int		m_nTimeout_sec_MCVentEnd;
	int		m_nTimeout_sec_MCRoughEnd;
	int		m_nTimeout_sec_ValveOperation;

	//time initial
	int		m_nTimeout_sec_LLKSlowRough_ini;
	int		m_nTimeout_sec_LLKFastRough_ini;
	int		m_nTimeout_sec_LLKRoughEnd_ini;
	int		m_nTimeout_sec_LLKSlow1Vent_ini;
	int     m_nTimeout_sec_LLKStandbyVent_ini;
	int		m_nTimeout_sec_LLKFastVent_ini;
	int		m_nTimeout_sec_LLKVentEnd_ini;
	int		m_nTimeout_sec_ValveOperation_ini;
	int		m_nTimeout_sec_MCSlowRough_ini;
	int		m_nTimeout_sec_MCFastRough_ini;
	int		m_nTimeout_sec_MCRoughEnd_ini;
	int		m_nTimeout_sec_MCTmpEnd_ini;
	int		m_nTimeout_sec_MCLLCVent_ini;
	int		m_nTimeout_sec_MCSlow1Vent_ini;
	int		m_nTimeout_sec_MCFastVent_ini;
	int		m_nTimeout_sec_MCVentEnd_ini;

	// Slow MFC Inlet Valve Open Range Setting ( 50 ~ 0.02 )
	// [ m_dPressure_ChangeToVent_LLC ~ m_dPressure_ChangeToFast_Rough ] 
	double	m_dPressure_SlowMFC_Inlet_Valve_Open_Value;

	void ReadVacuumSeqData();
	void SaveVacuumSeqData();


//[Navigation Stage Config]
	struct _Pos {
		double x;
		double y;
		char chStagePositionString[128];
	};
	_Pos m_stStagePos[MAX_STAGE_POSITION];


	struct PI_Pos {
		double x;
		double y;
		char chStagePositionString[128];
	};
	PI_Pos m_stStagePos_PI[4];

	struct PI_Posum {
		double x;
		double y;
		char chStageumPositionString[128];
	};
	PI_Posum m_stStageumPos_PI[20];
	
		
	/** Stage Position 데이터를  File에서 읽어 오는 함수 */
	void		ReadStagePositionFromDB();

	/** Stage Position 데이터를  File에 쓰는 함수 */
	void		SaveStagePositionToDB();

	void		BackupStagePositionToDB(CString sPath);

	BOOL		m_bNaviStageMoveAtZorigin;	//필요없으면 지우자

	/** PI Stage 를 Check 하기 위한 Point Position 데이터를 File에서 읽어 오는 함수 */
	void		ReadPIStageTestPositionFromDB();

	/** PI Stage 를 Check 하기 위한 Point Position 데이터를 File에서 읽어 오는 함수 (단위 um) */
	void		ReadPIStageTestPositionFromDBum();


//[Recovery Config]

	/** Align 완료된 경우 Mask Unloading 되지 않았다면 프로그램 껏다켜도 Align 하지 않아도되게 추가 */
	double		m_dOMAlignPointLB_X_mm, m_dOMAlignPointLB_Y_mm;
	double		m_dOMAlignPointLT_X_mm, m_dOMAlignPointLT_Y_mm;
	double		m_dOMAlignPointRT_X_mm, m_dOMAlignPointRT_Y_mm;
	double		m_dEUVAlignPointLB_X_mm, m_dEUVAlignPointLB_Y_mm;
	double		m_dEUVAlignPointLT_X_mm, m_dEUVAlignPointLT_Y_mm;
	double		m_dEUVAlignPointRT_X_mm, m_dEUVAlignPointRT_Y_mm;
	double		m_dNotchAlignPoint1_X_mm, m_dNotchAlignPoint1_Y_mm;
	double		m_dNotchAlignPoint2_X_mm, m_dNotchAlignPoint2_Y_mm;
	double		m_dEUVNotchAlignPoint1_X_mm, m_dEUVNotchAlignPoint1_Y_mm;
	double		m_dEUVNotchAlignPoint2_X_mm, m_dEUVNotchAlignPoint2_Y_mm;
	BOOL		m_bOMAlignCompleteFlag;
	BOOL		m_bEUVAlignCompleteFlag;
	int			m_nEUVAlignFOV;			//Align한 FOV Size	nm
	int			m_nEUVAlignGrid;		//Align한 Grid Size

	/** Z axis 높이 설정 관련 */
	BOOL		m_bIsZInterlockSet;
	double		m_dZInterlock_um;
	double		m_dZFocusPos_um;
	double		m_dZThroughFocusUp_um;
	double		m_dZCapStageOffset_um;

	/** Tip Tilt설정 관련 */
	BOOL		m_bIsTipTiltSet;
	//double		m_dConfigCalTx_urad;
	//double		m_dConfigCalTy_urad;

	/** Laser Interferometer feedback 사용 */
	//BOOL		m_bLaserFeedbackAvailable_Flag;
	BOOL		m_bLaserFeedback_Flag;

	/** Recovery 시 MTS Rotate 및 Flip 했는지 확인 용 */
	BOOL		m_bMtsRotateDone_Flag;
	BOOL		m_bMtsFlipDone_Flag;

	int			m_nPreviousProcess;

	/** 현재 Material Location */
	int			m_nMaterialLocation;

	/** Recovery 데이터를  File에서 읽어 오는 함수 */
	void		ReadRecoveryData();

	/** Recovery 데이터를  File에 쓰는 함수 */
	void		SaveRecoveryData();
	
	void		SaveCurrentProcess(int nProcess);

	void		SaveMaterialLocation(int nLoc);

	void		SaveMtsRecoveryData();


//[Calibration Config]

	/** Stage Area */
	CRect		m_rcOMStageAreaRect;
	CRect		m_rcEUVStageAreaRect;

	/** OM Pixel Size */
	double		m_dOM_PixelSizeX_um;
	double		m_dOM_PixelSizeY_um;

	/** EUV 영역에서 Laser-Scale Offset 변수 */
	double		m_dLB_LaserSwitching_OffsetX_mm;
	double		m_dLB_LaserSwitching_OffsetY_mm;
	double		m_dLT_LaserSwitching_OffsetX_mm;
	double		m_dLT_LaserSwitching_OffsetY_mm;
	double		m_dRT_LaserSwitching_OffsetX_mm;
	double		m_dRT_LaserSwitching_OffsetY_mm;

	/** Mask 평탄도 위한 Tip,Tilt 변수 */
	double		m_dConfigCalTx_urad;
	double		m_dConfigCalTy_urad;

	/** OM-EUV Offset */
	double		m_dGlobalOffsetX_mm;  // 140.7108;   // 140.727(LidOpen,SourceAlign); // 140.736(LidOpen); // 140.724; // 140.732;
	double		m_dGlobalOffsetY_mm;	 //   2.4086;   // 2.347(LidOpen,SourceAlign);   // 2.403(LidOpen);   // 2.445;   // 2.462;
	double		m_dLBOffsetX_mm;		 // 140.7098;   // 140.725(LidOpen,SourceAlign); // 140.735(LidOpen); // 140.721; // 140.732;
	double		m_dLBOffsetY_mm;		 //   2.4082;   // 2.347(LidOpen,SourceAlign);   // 2.403(LidOpen);   // 2.444;   // 2.461;
	double		m_dLTOffsetX_mm;		 // 140.7100;   // 140.725(LidOpen,SourceAlign); // 140.735(LidOpen); // 140.721; // 140.733;
	double		m_dLTOffsetY_mm;		 //   2.4079;   // 2.347(LidOpen,SourceAlign);   // 2.403(LidOpen);   // 2.444;   // 2.463;
	double		m_dRTOffsetX_mm;		 // 140.7126;   // 140.730(LidOpen,SourceAlign); // 140.739(LidOpen); // 140.727; // 140.732;
	double		m_dRTOffsetY_mm;		 //   2.4096;   // 2.348(LidOpen,SourceAlign);   // 2.405(LidOpen);   // 2.447;   // 2.464;

	double		m_dZdistanceCap1nStage_um;
	double		m_dZdistanceCap2nStage_um;
	double		m_dZdistanceCap3nStage_um;
	double		m_dZdistanceCap4nStage_um;


	double		m_dInspectorOffsetX_um;
	double		m_dInspectorOffsetY_um;

	/** 각종 Calibration 데이터를  File에서 읽어 오는 함수 */
	void		ReadCalibrationInfo();

	/** 각종 Calibration 데이터를  File에 쓰는 함수 */
	void		SaveCalibrationInfo();
	

	afx_msg void OnBnClickedButtonConfigCalLoad();
	afx_msg void OnBnClickedButtonConfigCalSave();
	afx_msg void OnEnChangeEditConfigCalTx();
	afx_msg void OnEnChangeEditConfigCalTy();
	afx_msg void OnEnChangeEditConfigCalGlobalOffsetx();
	afx_msg void OnEnChangeEditConfigCalGlobalOffsety();
	afx_msg void OnEnChangeEditConfigCalLbOffsetx();
	afx_msg void OnEnChangeEditConfigCalLbOffsety();
	afx_msg void OnEnChangeEditConfigCalLtOffsetx();
	afx_msg void OnEnChangeEditConfigCalLtOffsety();
	afx_msg void OnEnChangeEditConfigCalRtOffsetx();
	afx_msg void OnEnChangeEditConfigCalRtOffsety();
	afx_msg void OnBnClickedButtonSetAutoOffset();
	afx_msg void OnBnClickedButtonSetLbOffset();
	afx_msg void OnBnClickedButtonSetLtOffset();
	afx_msg void OnBnClickedButtonSetRtOffset();
	afx_msg void OnEnChangeEditConfigCalFocusCap1();
	afx_msg void OnEnChangeEditConfigCalFocusCap2();
	afx_msg void OnEnChangeEditConfigCalFocusCap3();
	afx_msg void OnEnChangeEditConfigCalFocusCap4();
	afx_msg void OnEnChangeEditConfigCalInspectorOffsetx();
	afx_msg void OnEnChangeEditConfigCalInspectorOffsety();

//[Imaging Option Configuration]
	BOOL		m_bBidirection;


//[Phase Configuration]

	/** Align시 사용할 Camera setting(slit 교체시 변경 가능성 있어서 config에 포함) */
	int			m_nAlignCcdX;
	int			m_nAlignCcdY;
	int			m_nAlignCcdWidth;
	int			m_nAlignCcdHeight;
	int			m_nAlignCcdBinning;	
	int			m_nAlignImageX;
	int			m_nAlignImageY;
	int			m_nAlignImageWidth;
	int			m_nAlignImageHeight;
	double		m_dAlignExposureTime;

	/** 측정 시 사용할 Camera setting */
	int			m_nMeasureCcdX;
	int			m_nMeasureCcdY;
	int			m_nMeasureCcdWidth;
	int			m_nMeasureCcdHeight;
	int			m_nMeasureCcdBinning;	
	int			m_nMeasureImageX;
	int			m_nMeasureImageY;
	int			m_nMeasureImageWidth;
	int			m_nMeasureImageHeight;
	double		m_dMeasureExposureTime;
	
	/** Coarse align parameter */
	double		m_dHorizentalMargin_um;			
	double		m_dVerticalMargin_um;			
	int			m_nMaximumTryCount;	
	double		m_dCoarseAlignIntensityTolerancePercent;
	double		m_dCoarseLsWidth;

	/** Fine align parameter */
	double		m_dStepWidthX_um;				
	int			m_nHalfNumberOfStepX;	
	double		m_dStepWidthY_um;
	int			m_nHalfNumberOfStepY;
	int			m_nNumOfStdPoint;
	int			m_nFineAlignAlgorithmType = 1;

	/** Slit parameter */
	double		m_dSlitWidth_um;				
	double		m_dSlitPitch_um;				
	double		m_dSlitLength_um;			

	/** Parameter */
	//double		m_dInterlockMargin_um; // 삭제
	//double		m_dZControlTolerance_um; //삭제
	//
	//double		m_dCap1PosLimitMin_um; //삭제
	//double		m_dCap1PosLimitMax_um; //삭제
	//double		m_dCap2PosLimitMin_um; //삭제
	//double		m_dCap2PosLimitMax_um; //삭제
	//double		m_dCap3PosLimitMin_um; //삭제
	//double		m_dCap3PosLimitMax_um; //삭제
	//double		m_dCap4PosLimitMin_um; //삭제
	//double		m_dCap4PosLimitMax_um; //삭제
	//
	//double		m_dCap1MeasurePos_um; //삭제
	//double		m_dCap2MeasurePos_um; //삭제
	//double		m_dCap3MeasurePos_um; //삭제
	//double		m_dCap4MeasurePos_um; //삭제
	//
	//double		m_dZStageLimitMin_um; //삭제
	//double		m_dZStageLimitMax_um; //삭제

	double		m_dImageRotationAngle_deg;
	double		m_dXrayCameraReadTimeout_ms;

	double		m_dDownZHeightWhenMove_um;

	int			m_nNfft;

	double		m_dContinuousWave;
	int			m_nContinuousWaveDate;

	int			m_nBackGroundHeight;

	/** 각종 Phase 측정 Configuration 데이터를  File에서 읽어 오는 함수 */
	void		ReadPhaseInfo();
	void		SavePhaseInfo();

	afx_msg void OnBnClickedButtonConfigPhaseLoad();
	afx_msg void OnBnClickedButtonConfigPhaseSave();

	//[Ptr Configuration]
	
	double		m_dD1Ref;
	double		m_dD2Ref;
	double		m_dD3Ref;

	double		m_dD1RefStd;
	double		m_dD2RefStd;
	double		m_dD3RefStd;

	double		m_RefReflectance;

	/** 각종 PTR 측정 Configuration 데이터를  File에서 읽어 오는 함수 */
	void		ReadPTRInfo();
	void		SavePTRInfo();


//[ADAM Configuration]
	double		m_I0Reference			= 0.8;
	int			m_I0FilterWindowSize	= 100;
	double		m_LowPassFilterSigma	= 15.0;

	int			m_AdamMaxWidth			= 1000;
	int			m_AdamMaxHeight			= 1000;
	int			m_AdamMaxWidthRegrid	= 3000;
	int			m_AdamMaxHeightRegrid	= 3000;

	void		ReadAdamInfo();
	void		SaveAdamInfo();

	

//[ETC Configuration]

	//MTS Info
	BOOL		m_bUseFlip;
	BOOL		m_bUseRotate;
	int			m_nRotateAngle;

	int			m_nSourceAutoOffTime_min;
	double		m_dEUVVacuumRate;

	CButton		m_UseMtsRotateCtrl;
	CComboBox	m_MtsRotateAngleCtrl;

	CString m_sFilterPos1;
	CString m_sFilterPos2;
	CString m_sFilterPos3;

	/** 각종 ETC 데이터(분류되지 않은 데이터)를  File에서 읽어 오는 함수 */
	void		ReadETCInfo();

	/** 각종 ETC 데이터(분류되지 않은 데이터)를  File에 쓰는 함수 */
	void		SaveETCInfo();

	void		ReadFilterInfo();
	void		SaveFilterInfo();

	afx_msg void OnBnClickedButtonConfigEtcLoad();
	afx_msg void OnBnClickedButtonConfigEtcSave();
	afx_msg void OnBnClickedBtnIoCon();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedBtnAdamCon();
	afx_msg void OnBnClickedBtnAfCon();
	afx_msg void OnBnClickedBtnZpCon();
	afx_msg void OnBnClickedBtnMirrorCon();
	afx_msg void OnBnClickedBtnSourceCon();
	afx_msg void OnBnClickedBtnMtsCon();
	afx_msg void OnBnClickedBtnVmtrCon();
	afx_msg void OnBnClickedBtnNaviCon();
	afx_msg void OnBnClickedBtnScanCon();
	afx_msg void OnBnClickedBtnBaemCon();
	afx_msg void OnBnClickedBtnIoDiscon();
	afx_msg void OnBnClickedBtnAdamDiscon();
	afx_msg void OnBnClickedBtnAfDiscon();
	afx_msg void OnBnClickedBtnZpDiscon();
	afx_msg void OnBnClickedBtnMirrorDiscon();
	afx_msg void OnBnClickedBtnSourceDiscon();
	afx_msg void OnBnClickedBtnMtsDiscon();
	afx_msg void OnBnClickedBtnVmtrDiscon();
	afx_msg void OnBnClickedBtnScanDiscon();
	afx_msg void OnBnClickedBtnNaviDiscon();
	afx_msg void OnBnClickedBtnBaemDiscon();
	afx_msg void OnBnClickedBtnNaviCon2();
	afx_msg void OnStnClickedIconScanConState();
	int m_nOMStageAreaLB_X;
	int m_nOMStageAreaLB_Y;
	int m_nOMStageAreaRT_X;
	int m_nOMStageAreaRT_Y;
	int m_nEUVStageAreaLB_X;
	int m_nEUVStageAreaLB_Y;
	int m_nEUVStageAreaRT_X;
	int m_nEUVStageAreaRT_Y;
};
