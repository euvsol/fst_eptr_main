/**
 * Vacuum Process Control Module Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 * Description: Load Lock Pumping/Venting Thread 가동, Main Chamber Pumping/Venting Thread 가동
 *              Vaccumm Sequence를 Thread를 이용하여 실행하는 Class, Vacuum 관련 상태 Data Update도 수행
 *              모든 Sequence는 동시에 진행 불가
 */
#pragma once

class CVacuumProcess : public CECommon, public IObserver
{
public:
	// Construction
	CVacuumProcess();
	~CVacuumProcess();

	typedef enum LlcSequenceState {
		State_IDLE = 0,				
		Pumping_START,				
		Pumping_SLOWROUGH,			
		Pumping_FASTROUGH,			
		Pumping_TMPROUGH,			
		Pumping_COMPLETE,				
		Venting_START,				
		Venting_SLOWVENT,			
		Venting_FASTVENT,			
		Venting_COMPLETE,			
	};
	LlcSequenceState	m_LlcVacuumState;

	typedef enum McSequenceState {
		MC_State_IDLE = 0,
		MC_Pumping_START,
		MC_Pumping_SLOWROUGH,
		MC_Pumping_FASTROUGH,
		MC_Pumping_TMPROUGH,
		MC_Pumping_COMPLETE,
		MC_Venting_START,
		MC_Venting_SLOWVENT,
		MC_Venting_FASTVENT,
		MC_Venting_COMPLETE,
	};
	McSequenceState	m_McVacuumState;

	clock_t	m_start_time_mc, m_finish_time_mc;
	clock_t	m_start_time, m_finish_time;


	/** 모든 Vacuum Component를 초기화 한다. */
	int VacuumInitAll();

	/**
	* Description:  TMP,Drypump,Guage,IO 이상상태를 Check한다.
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Is_VacuumModule_OK();

	/** 진공 Sequence Thread를 중지시키는 함수  */
	int VacThreadStop();
	int MC_VacThreadStop();

	/** LLC 쳄버가 Pumped 상태인지, Vented 상태인지 판단하는 함수 */
	int	 GetLLCVacuumStatus();
	/** Main 쳄버가 Pumped 상태인지, Vented 상태인지 판단하는 함수 */
	int	 GetMCVacuumStatus();

	/** 진공 Sequence Thread의 종료 여부를 나타내는 변수  */
	BOOL			m_bVacuumThreadStop;

	/** 진공 Sequence Thread 포인터 변수  */
	CWinThread		*m_pVaccumThread;

	int m_nLlcErrorCode;
	int m_nMcErrorCode;

	/** 벤팅/펌핑 구분하기 위한 변수 */
	BOOL m_bLlcVenting;
	BOOL m_bMcVenting;

	///////////////////////////////////// --------- LLC Pumping or Venting Thread --------- /////////////////////////////////////////////

	/** LLC Pumping Sequence Thread Start 함수 . 물류 Sequence에서 호출한다. */
	int		LLC_Pumping_Start();

	/** LLC Venting Sequence Thread Start 함수 . 물류 Sequence에서 호출한다. */
	int		LLC_Venting_Start();

	/** LLC Vacuum Sequence Thread 함수  */
	static UINT LLC_VacuumThread(LPVOID pParam);

	/** LLC Vacuum Sequence Loop 함수  */
	int		LLC_Vacuum_Loop();


	/**  LLC Pumping 준비 함수  */
	int LLC_Pumping_PreWork();

	/**  LLC Slow Roughing 함수  */
	int LLC_SlowRough();

	/**  LLC Fast Roughing 함수  */
	int LLC_FastRough();

	/**  LLC TMP Pumping 함수  */
	int LLC_TMPRough();

	/**  LLC Pumping 완료 후 후속작업을 처리하는 함수  */
	int LLC_Pumping_Complete();


	/**  LLC Venting 준비 함수  */
	int LLC_Venting_PreWork();

	/**  LLC Slow Venting 함수  */
	int LLC_SlowVent();

	/**  LLC Fast Venting 함수  */
	int LLC_FastVent();

	/**  LLC Venting 완료 후 후속작업을 처리하는 함수  */
	int LLC_Venting_Complete();


	///////////////////////////////////// --------- MAIN 쳄버 Pumping or Venting Thread --------- /////////////////////////////////////////////


	/** MC Pumping Sequence Thread Start 함수 . 물류 Sequence에서 호출한다. */
	int		MC_Pumping_Start();

	/** MC Venting Sequence Thread Start 함수 . 물류 Sequence에서 호출한다. */
	int		MC_Venting_Start();

	/** MC Vacuum Sequence Thread 함수  */
	static UINT MC_VacuumThread(LPVOID pParam);

	/** MC Vacuum Sequence Loop 함수  */
	int		MC_Vacuum_Loop();


	/** 진공 Sequence Thread의 종료 여부를 나타내는 변수  */
	BOOL			m_b_MC_VacuumThreadStop;

	/** 진공 Sequence Thread 포인터 변수  */
	CWinThread		*m_p_MC_VaccumThread;


	/**  MC Pumping 준비 함수  */
	int MC_Pumping_PreWork();

	/**  MC Slow Roughing 함수  */
	int MC_SlowRough();

	/**  MC Fast Roughing 함수  */
	int MC_FastRough();

	/**  MC TMP Pumping 함수  */
	int MC_TMPRough();

	/**  MC Pumping 완료 후 후속작업을 처리하는 함수  */
	int MC_Pumping_Complete();


	/**  MC Venting 준비 함수  */
	int MC_Venting_PreWork();

	/**  MC Slow Venting 함수  */
	int MC_SlowVent();

	/**  MC Fast Venting 함수  */
	int MC_FastVent();

	/**  MC Venting 완료 후 후속작업을 처리하는 함수  */
	int MC_Venting_Complete();



	/** Error 발생 후 실행 되는 함수 */
	int MC_Pumping_Error();
	int MC_Venting_Error();
	int LLC_Pumping_Error();
	int LLC_Venting_Error();

	/** Error 발생시 Error Code Define 변수 */
	//int m_nSequence_Error_Code_Define;

	int m_nSequence_State;

	//HANDLE m_LLC_Pumping_Run_Test;

	bool TR_Gate_Open_Check();

	bool m_bMcVentingState;


	void N2Gas_Flow_Check();
	int Cnt_Error_Slow_MFC;
	int Cnt_Error_Fast_MFC;

	bool m_bSlowMfcInlet_Open_State;

	int m_nMc_slow_rough_time_cnt;
	int m_nMc_fast_rough_time_cnt;
	int m_nMc_tmp_rough_time_cnt;
	int m_nMc_standby_vent_time_cnt;
	int m_nMc_slow_vent_time_cnt;
	int m_nMc_fast_vent_time_cnt;
	int m_nLlc_slow_rough_time_cnt;
	int m_nLlc_fast_rough_time_cnt;
	int m_nLlc_tmp_rough_time_cnt;
	int m_nLlc_standby_vent_time_cnt;
	int m_nLlc_slow_vent_time_cnt;
	int m_nLlc_fast_vent_time_cnt;

	int LLCVentingSequenceStop();
	int LLCPumpingSequenceStop();

	void	EmergencyStop();
};

