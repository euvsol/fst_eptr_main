﻿#pragma once


// CSubMenuDlg 대화 상자

class CSubMenuDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSubMenuDlg)

public:
	CSubMenuDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CSubMenuDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SUB_MENU_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedMtsButton();
	afx_msg void OnBnClickedVmtrButton();
	afx_msg void OnBnClickedOmButton();
	afx_msg void OnBnClickedStageButton();
	afx_msg void OnBnClickedPumpButton();
	afx_msg void OnBnClickedDioButton();
	afx_msg void OnBnClickedHwstateButton();
	afx_msg void OnBnClickedGemautoButton();

	int SetDisplay(int Mode);
	afx_msg void OnBnClickedSourceButton();
	afx_msg void OnBnClickedXrayCameraButton();
	virtual BOOL OnInitDialog();
};
