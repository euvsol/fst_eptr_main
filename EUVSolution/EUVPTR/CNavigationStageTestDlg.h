﻿#pragma once


// CNavigationStageTestDlg 대화 상자

class CNavigationStageTestDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CNavigationStageTestDlg)

public:
	CNavigationStageTestDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CNavigationStageTestDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_NAVIGATION_STAGE_TEST_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();

	HICON m_LedIcon[3];

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedStageMeasurementStartCtl();

	clock_t	m_start_time, m_finish_time;

	/* Navigatin Stage Test 시작 함수 */
	int m_nNavigation_Stage_Test_Start();

	/* Navigatin Stage Test 진행 함수 */
	int m_nNavigation_Stage_Check();

	/* Navigation Stage 측정 플러그 변수 */
	BOOL m_bNavigation_Stage_Test_Start;
	CProgressCtrl m_progress_stage_check;
	afx_msg void OnBnClickedStageMeasurementStopCtl();
	CComboBox m_comboxvelo;
	afx_msg void OnCbnSelendokComboVel();


	struct StageTrakingErrorInf
	{
		CNavigationStageTestDlg* pNavigationStageTest;
		double xpos;
		double ypos;
		int TestNo;
		int TestAxis;
		BOOL CoordRef;
	};

	struct StageErrorAxis
	{
		CNavigationStageTestDlg* pNavigationStageTest;
		int TestAxis;
	};


	
	int m_nSetVelocity;

	void StageVelocityCheck();
	void StageFollowingErrorCheck(int TestPointNum, int TestAxis);

	BOOL StageMove(double maskCoordX_um, double maskCoordY_um, int num, int TestAxis, BOOL CoordRef);
	void StageCheck();

	vector <double> m_VectrackingErrorTestPoint;

	afx_msg void OnBnClickedBtnTestSet0();

	void SH_Stage_Check_Axis_Check();

	CWinThread* m_pFollowingErrorCheckThread = NULL;
	static UINT	FollowingErrorCheckThread(LPVOID pParam);
	BOOL m_bFollowingTestThreadStopFlag = TRUE;

	CWinThread* m_pFollowingErrorStageMoveThread = NULL;
	static UINT	FollowingErrorStageMoveThread(LPVOID pParam);
	BOOL m_bFollowingTestStageMoveThreadStopFlag = TRUE;

	int m_nTestCnt = 0;

	HANDLE StageTestEvent;

	CEdit m_Defectcoordxtest_x;
	CEdit m_Defectcoordxtest_y;
	CEdit m_maskcentercoordx_test;
	CEdit m_maskcentercoordy_test;
	CEdit m_masklbmcoordx_test;
	CEdit m_masklbmcoordy_test;
	afx_msg void OnBnClickedBtnTestSet1();
	CEdit m_error_value_0;
	CEdit m_error_value_1;
	CEdit m_error_value_2;
	CEdit m_error_value_3;
	CEdit m_error_value_4;
	CEdit m_error_value_5;
	CEdit m_error_value_6;
	CEdit m_error_value_7;
	CEdit m_error_value_8;
	CEdit m_error_value_9;
	CEdit m_error_value_10;
	CEdit m_error_value_11;
	CEdit m_error_value_12;
	CEdit m_error_value_13;
	CEdit m_error_value_14;
	CEdit m_error_value_15;
	CEdit m_avg_value_0;
	CEdit m_avg_value_1;
	CEdit m_avg_value_2;
	CEdit m_avg_value_3;
	CEdit m_avg_value_4;
	CEdit m_avg_value_5;
	CEdit m_avg_value_6;
	CEdit m_avg_value_7;
	CEdit m_avg_value_8;
	CEdit m_avg_value_9;
	CEdit m_avg_value_10;
	CEdit m_avg_value_11;
	CEdit m_avg_value_12;
	CEdit m_avg_value_13;
	CEdit m_avg_value_14;
	CEdit m_avg_value_15;
	CEdit m_max_value_0;
	CEdit m_max_value_1;
	CEdit m_max_value_2;
	CEdit m_max_value_3;
	CEdit m_max_value_4;
	CEdit m_max_value_5;
	CEdit m_max_value_6;
	CEdit m_max_value_7;
	CEdit m_max_value_8;
	CEdit m_max_value_9;
	CEdit m_max_value_10;
	CEdit m_max_value_11;
	CEdit m_max_value_12;
	CEdit m_max_value_13;
	CEdit m_max_value_14;
	CEdit m_max_value_15;
	CEdit m_min_value_0;
	CEdit m_min_value_1;
	CEdit m_min_value_2;
	CEdit m_min_value_3;
	CEdit m_min_value_4;
	CEdit m_min_value_5;
	CEdit m_min_value_6;
	CEdit m_min_value_7;
	CEdit m_min_value_8;
	CEdit m_min_value_9;
	CEdit m_min_value_10;
	CEdit m_min_value_11;
	CEdit m_min_value_12;
	CEdit m_min_value_13;
	CEdit m_min_value_14;
	CEdit m_min_value_15;
	CEdit m_edit_test_cnt_0;
	CEdit m_edit_test_cnt_1;
	CEdit m_edit_test_cnt_2;
	CEdit m_edit_test_cnt_3;
	CEdit m_edit_test_cnt_4;
	CEdit m_edit_test_cnt_5;
	CEdit m_edit_test_cnt_6;
	CEdit m_edit_test_cnt_7;
	CEdit m_edit_test_cnt_8;
	CEdit m_edit_test_cnt_9;
	CEdit m_edit_test_cnt_10;
	CEdit m_edit_test_cnt_11;
	CEdit m_edit_test_cnt_12;
	CEdit m_edit_test_cnt_13;
	CEdit m_edit_test_cnt_14;
	CEdit m_edit_test_cnt_15;
	CEdit m_edit_test_vel_0;
	CEdit m_edit_test_vel_1;
	CEdit m_edit_test_vel_2;
	CEdit m_edit_test_vel_3;
	CEdit m_edit_test_vel_4;
	CEdit m_edit_test_vel_5;
	CEdit m_edit_test_vel_6;
	CEdit m_edit_test_vel_7;
	CEdit m_edit_test_vel_8;
	CEdit m_edit_test_vel_9;
	CEdit m_edit_test_vel_10;
	CEdit m_edit_test_vel_11;
	CEdit m_edit_test_vel_12;
	CEdit m_edit_test_vel_13;
	CEdit m_edit_test_vel_14;
	CEdit m_edit_test_vel_15;

	afx_msg void OnBnClickedBtnTestSet2();
	afx_msg void OnBnClickedBtnTestSet3();
	afx_msg void OnBnClickedBtnTestSet4();
	afx_msg void OnBnClickedBtnTestSet5();
	afx_msg void OnBnClickedBtnTestSet6();
	afx_msg void OnBnClickedBtnTestSet7();
	afx_msg void OnBnClickedBtnTestSet8();
	afx_msg void OnBnClickedBtnTestSet9();
	afx_msg void OnBnClickedBtnTestSet10();
	afx_msg void OnBnClickedBtnTestSet11();
	afx_msg void OnBnClickedBtnTestSet12();
	afx_msg void OnBnClickedBtnTestSet13();
	afx_msg void OnBnClickedBtnTestSet14();
	afx_msg void OnBnClickedBtnTestSet15();
	afx_msg void OnBnClickedBtnTestSetAll();
	afx_msg void OnBnClickedBtnTestSetAll2();
	CEdit m_stageencoderx_test;
	CEdit m_stageencodery_test;
};
