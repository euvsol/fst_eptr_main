#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include <vector>


//double Macro::target_x_pos = 0.0;
//double Macro::target_y_pos = 0.0;
//double Macro::target_z_pos = 0.0;
//
//
//Macro::Macro()
//{
//
//}
//
//Macro::~Macro()
//{
//	Clear();
//}
//
//
//void Macro::AddCommand(Command *com)
//{
//	m_VectorMacroCommands.push_back(com);
//}
//
//void Macro::Clear()
//{
//	int VectorSize = m_VectorMacroCommands.size();
//
//	for (int i = 0; i < VectorSize; i++)
//	{
//		delete m_VectorMacroCommands[i];
//	}
//	
//	m_VectorMacroCommands.clear();
//}
//
//BOOL Macro::RecipeExecute()
//{
//	BOOL ReturnFlag = TRUE;
//	CString strLog;
//
//	for (vector<Command*>::size_type x = 0; x < m_VectorMacroCommands.size(); x++)
//	{
//		if (((m_VectorMacroCommands[x]->execute()) != TRUE) && (g_pMaskMap->m_bMacroSystemRunFlag != FALSE))
//		{
//			if (!g_pMaskMap->m_bMacroSystemRunFlag)
//			{
//				::AfxMessageBox(_T(" Macro 동작 Stop button Click!"));
//				strLog.Format(_T(" Macro 동작 Stop button Click!"));
//				SaveLogFile("Macro", strLog);
//			}
//			else
//			{
//				::AfxMessageBox(" Macro 동작 Error 발생!");
//				strLog.Format(_T("Macro 동작 Error 발생!"));
//				SaveLogFile("Macro", strLog);
//			}
//			ReturnFlag = FALSE;
//			break;
//		}
//	}
//
//	return ReturnFlag;
//
//}
//
//
//BOOL StageMoveCommand::StageMoveStart(double x_pos, double y_pos)
//{
//	BOOL ReturnFlag = TRUE;
//	CString strLog;
//
//	g_pLog->Display(0, _T("Macro system :: Stage Move Start!"));
//	strLog.Format(_T("Stage Move Start"));
//	SaveLogFile("Macro", strLog);
//	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
//
//
//	g_pWarning->m_strWarningMessageVal = " MacroSystem :: Stage Move Start! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//
//	/* Stage Encoder 기준 */
//	//if (g_pNavigationStage->MoveAbsolutePosition(x_pos, y_pos) != XY_NAVISTAGE_OK)
//
//	/* 레시피의 X,Y 좌표는 Mask LB Aligh Point Coordinate 0,0 기준 */
//	//if (g_pNavigationStage->MoveToMaskCenterCoordinate(x_pos, y_pos) != XY_NAVISTAGE_OK)
//	//{
//	//
//	//	::AfxMessageBox(" Stage 동작 불가!");
//	//	SaveLogFile("MacroSystem", _T("Stage Move Error"));
//	//	g_pWarning->ShowWindow(SW_HIDE);
//	//	ReturnFlag = FALSE;
//	//	return ReturnFlag;
//	//}
//
//
//	/* Mask Center Coordinate 0.0 기준 */
//
//	if (g_pNavigationStage->MoveToMaskCenterCoordinate(x_pos + g_pConfig->m_dInspectorOffsetX_um, y_pos + g_pConfig->m_dInspectorOffsetY_um) != XY_NAVISTAGE_OK) //+ 방향
//	{
//		::AfxMessageBox(" Stage 동작 불가!");
//		strLog.Format(_T("Stage Move Error"));
//		SaveLogFile("Macro", strLog);
//		g_pWarning->ShowWindow(SW_HIDE);
//		ReturnFlag = FALSE;
//		return ReturnFlag;
//	}
//
//
//
//	WaitSec(5);
//
//	strLog.Format(_T("Stage Move End"));
//	SaveLogFile("Macro", strLog);
//
//	g_pWarning->ShowWindow(SW_HIDE);
//	return ReturnFlag;
//}
//
//StageMoveCommand::StageMoveCommand(MacroStruct * GetVectorData)
//{
//	//CString strLog;
//	//
//	//CommandState = TRUE;
//	//int paramcnt = stoi(GetVectorData->paramcnt);
//	//
//	//if (paramcnt < 2)
//	//{
//	//	CommandState = FALSE;
//	//	strLog.Format(_T("Stage Move Command 실행 Error = 인수 입력이 2개 이상 입니다. (입력 인수 : %d)"), paramcnt);
//	//	SaveLogFile("Macro", strLog);
//	//}
//	//else
//	//{
//	//	x = stof(GetVectorData->Parameter.at(0));
//	//	y = stof(GetVectorData->Parameter.at(1));
//	//
//	//	strLog.Format(_T("Stage Move Command = Xpos : %f, Ypos :%f"), x, y);
//	//	SaveLogFile("Macro", strLog);
//	//}
//}
//
//StageStepMoveCommand::StageStepMoveCommand(MacroStruct * GetVectorData)
//{
//	//CommandState = TRUE;
//	//CString strLog;
//	//int paramcnt = stoi(GetVectorData->paramcnt);
//	//
//	//if (paramcnt < 2)
//	//{
//	//	CommandState = FALSE;
//	//	strLog.Format(_T("Stage Step Move Command 실행 Error = 인수 입력이 2개 이상 입니다. (입력 인수 : %d)"), paramcnt);
//	//	SaveLogFile("Macro", strLog);
//	//}
//	//else
//	//{
//	//	step_x_um = stof(GetVectorData->Parameter.at(0));
//	//	step_y_um = stof(GetVectorData->Parameter.at(1));
//	//	strLog.Format(_T("Stage Step Move Command = Xpos : %f, Ypos :%f"), step_x_um, step_y_um);
//	//	SaveLogFile("Macro", strLog);
//	//}
//}
//
//BOOL StageStepMoveCommand::StageStepMoveStart(double x_pos_um, double y_pos_um)
//{
//	BOOL ReturnFlag = TRUE;
//	CString strLog;
//
//
//	strLog.Format(_T("Stage Step Move Start!. (Xpos : %f, Ypos : %f)"),x_pos_um, y_pos_um);
//	SaveLogFile("Macro", strLog);
//	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);
//
//
//	g_pWarning->m_strWarningMessageVal = " MacroSystem :: Stage Step Move Start! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//
//	/* Stage Encoder 기준 */
//
//	if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, (x_pos_um / 1000)) != XY_NAVISTAGE_OK)
//	{
//		::AfxMessageBox(" X Stage 동작 불가!");
//		strLog.Format(_T("Stage Step Move Error!. (Xpos : %f)"), x_pos_um);
//		SaveLogFile("Macro", strLog);
//		g_pWarning->ShowWindow(SW_HIDE);
//		ReturnFlag = FALSE;
//		return ReturnFlag;
//	}
//	
//	WaitSec(5);
//
//	if (g_pNavigationStage->MoveRelativePosition(STAGE_Y_AXIS, (y_pos_um / 1000)) != XY_NAVISTAGE_OK)
//	{
//		::AfxMessageBox(" Y Stage 동작 불가!");
//		strLog.Format(_T("Stage Step Move Error!.  Ypos : %f)"),y_pos_um);
//		SaveLogFile("Macro", strLog);
//		g_pWarning->ShowWindow(SW_HIDE);
//		ReturnFlag = FALSE;
//		return ReturnFlag;
//	}
//
//	WaitSec(5);
//	strLog.Format(_T("Stage Step Move End!"));
//	SaveLogFile("Macro", strLog);
//
//	g_pWarning->ShowWindow(SW_HIDE);
//	return ReturnFlag;
//}
//
//EuvOnCommand::EuvOnCommand(MacroStruct * GetVectorData)
//{
//	CommandState = TRUE;
//}
//
//BOOL EuvOnCommand::EuvOn()
//{
//	BOOL ReturnFlag = TRUE;
//	CString strLog;
//
//	g_pLog->Display(0, _T("Macro system :: Euv On Start!"));
//	strLog.Format(_T("Euv On Start!"));
//	SaveLogFile("Macro", strLog);
//	g_pWarning->m_strWarningMessageVal = " MacroSystem :: EuvOn! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
//	{
//		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
//		strLog.Format(_T("Euv On Error == 디바이스와 연결이 되지 않았습니다"));
//		SaveLogFile("Macro", strLog);
//		ReturnFlag = FALSE;
//		g_pWarning->ShowWindow(SW_HIDE);
//		SaveLogFile("MacroSystem", _T("Euv On Fail"));
//		return ReturnFlag;
//	}
//
//	if (g_pIO->Is_SourceGate_OpenOn_Check() != TRUE)
//	{
//		AfxMessageBox(_T("기준 진공도 미달로 인해 EUV를 사용할 수 없습니다."));
//		strLog.Format(_T("Euv On Error == 기준 진공도 미달로 인해 EUV 사용 불가"));
//		SaveLogFile("Macro", strLog);
//		g_pWarning->ShowWindow(SW_HIDE);
//		ReturnFlag = FALSE;
//		return ReturnFlag;
//	}
//	strLog.Format(_T("Euv On == Shtter Open"));
//	SaveLogFile("Macro", strLog);
//	g_pEUVSource->SetMechShutterOpen(FALSE);
//
//	g_pEUVSource->SetEUVSourceOn(TRUE);
//
//	WaitSec(5);
//	if (g_pEUVSource->Is_EUV_On() != TRUE)
//	{
//		ReturnFlag = FALSE;
//		g_pWarning->ShowWindow(SW_HIDE);
//		strLog.Format(_T("Euv On Error == Eun On 불가"));
//		SaveLogFile("Macro", strLog);
//		return ReturnFlag;
//	}
//
//	strLog.Format(_T("Euv On End"));
//	SaveLogFile("Macro", strLog);
//
//	g_pWarning->ShowWindow(SW_HIDE);
//	return ReturnFlag;
//}
//
//EuvOffCommand::EuvOffCommand(MacroStruct * GetVectorData)
//{
//	CommandState = TRUE;
//}
//
//BOOL EuvOffCommand::EuvOff()
//{
//	BOOL ReturnFlag = TRUE;
//	CString strLog;
//
//	g_pLog->Display(0, _T("Macro system :: Euv Off Start!"));
//
//	strLog.Format(_T("Euv Off Start"));
//	SaveLogFile("Macro", strLog);
//
//	g_pWarning->m_strWarningMessageVal = " MacroSystem :: EuvOff ! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
//	{
//		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
//		strLog.Format(_T("Euv Off Error == 디바이스와 연결이 되지 않았습니다"));
//		SaveLogFile("Macro", strLog);
//		ReturnFlag = FALSE;
//		g_pWarning->ShowWindow(SW_HIDE);
//		return ReturnFlag;
//	}
//
//	if(g_pEUVSource->SetEUVSourceOn(FALSE) != 0)
//		return FALSE;
//
//	WaitSec(5);
//
//	//if ((g_pEUVSource->Is_EUV_On() != FALSE) || (g_pEUVSource->Is_LaserShutter_Opened() != FALSE))
//	if(g_pEUVSource->Is_EUV_On() != FALSE)
//	{
//		ReturnFlag = FALSE;
//		g_pWarning->ShowWindow(SW_HIDE);
//		strLog.Format(_T("Euv Off Error"));
//		SaveLogFile("Macro", strLog);
//		return ReturnFlag;
//	}
//
//	strLog.Format(_T("Euv Off End"));
//	SaveLogFile("Macro", strLog);
//
//	g_pWarning->ShowWindow(SW_HIDE);
//	return ReturnFlag;
//}
//
//ShutterOnCommand::ShutterOnCommand(MacroStruct * GetVectorData)
//{
//	CommandState = TRUE;
//}
//
//BOOL ShutterOnCommand::ShutterOn()
//{
//	BOOL ReturnFlag = TRUE;
//	CString strLog;
//
//	g_pLog->Display(0, _T("Macro system :: Shutter On !"));
//
//	strLog.Format(_T("Shtter On Start"));
//	SaveLogFile("Macro", strLog);
//
//	g_pWarning->m_strWarningMessageVal = " MacroSystem :: ShutterOn ! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//
//	g_pEUVSource->SetMechShutterOpen(TRUE);
//
//	WaitSec(5);
//
//
//	//if ((g_pEUVSource->Is_EUV_On() != FALSE) || (g_pEUVSource->Is_LaserShutter_Opened() != FALSE))
//	if(g_pEUVSource->Is_EUV_On() != TRUE)
//	{
//		ReturnFlag = FALSE;
//		strLog.Format(_T("Shtter On Error == Euv 가 켜지지 않았습니다"));
//		SaveLogFile("Macro", strLog);
//		g_pWarning->ShowWindow(SW_HIDE);
//		return ReturnFlag;
//	}
//
//	strLog.Format(_T("Shtter On End"));
//	SaveLogFile("Macro", strLog);
//	g_pWarning->ShowWindow(SW_HIDE);
//	return ReturnFlag;
//}
//
//ShutterOffCommand::ShutterOffCommand(MacroStruct * GetVectorData)
//{
//	CommandState = TRUE;
//}
//
//BOOL ShutterOffCommand::ShutterOff()
//{
//	BOOL ReturnFlag = TRUE;
//	CString strLog;
//
//	g_pLog->Display(0, _T("Macro system :: Shutter Off !"));
//
//	strLog.Format(_T("Shtter Off Start"));
//	SaveLogFile("Macro", strLog);
//
//	g_pWarning->m_strWarningMessageVal = " MacroSystem :: ShutterOff ! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//
//	g_pEUVSource->SetMechShutterOpen(FALSE);
//
//	WaitSec(5);
//
//	strLog.Format(_T("Shtter Off End"));
//	SaveLogFile("Macro", strLog);
//	g_pWarning->ShowWindow(SW_HIDE);
//	return ReturnFlag;
//}
//
//MaskLoadCommand::MaskLoadCommand(MacroStruct * GetVectorData)
//{
//	CommandState = TRUE;
//}
//
//BOOL MaskLoadCommand::MaskLoad()
//{
//	BOOL bReturnFlag = TRUE;
//	CString strLog;
//
//	g_pLog->Display(0, _T("Macro system :: Mask load Start!"));
//
//	strLog.Format(_T("Mask Load Start"));
//	SaveLogFile("Macro", strLog);
//
//	g_pWarning->m_strWarningMessageVal = " MacroSystem :: Mask Load 중! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
//	{
//		::AfxMessageBox(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "));
//		strLog.Format(_T("Mask Load Error ==  이미 Auto Sequence 가동 중. 진행할 수 없습니다 !"));
//		SaveLogFile("Macro", strLog);
//		bReturnFlag = FALSE;
//		g_pWarning->ShowWindow(SW_HIDE);
//		return bReturnFlag;
//	}
//
//	if (g_pMaskMap->MaskLoad() != 0)
//	{
//		::AfxMessageBox(_T(" Mask Load 에 실패 하였습니다 ! "));
//		strLog.Format(_T("Mask Load Error == Mask load Fail!"));
//		SaveLogFile("Macro", strLog);
//		g_pWarning->ShowWindow(SW_HIDE);
//		bReturnFlag = FALSE;
//		return bReturnFlag;
//	}
//
//	strLog.Format(_T("Mask Load End !"));
//	SaveLogFile("Macro", strLog);
//	g_pWarning->ShowWindow(SW_HIDE);
//	return bReturnFlag;
//}
//
//MaskUnLoadCommand::MaskUnLoadCommand(MacroStruct * GetVectorData)
//{
//	CommandState = TRUE;
//}
//
//BOOL MaskUnLoadCommand::MaskUnLoad()
//{
//	BOOL bReturnFlag = TRUE;
//	CString strLog;
//
//	g_pLog->Display(0, _T("Macro system :: Mask Unload Start!"));
//	
//	strLog.Format(_T("Mask UnLoad Start !"));
//	SaveLogFile("Macro", strLog);
//
//
//	g_pWarning->m_strWarningMessageVal = " MacroSystem :: Mask UnLoad 중! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
//	{
//		AfxMessageBox(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "));
//		SaveLogFile("MacroSystem", _T("Mask UnLoad Fail"));
//		g_pWarning->ShowWindow(SW_HIDE);
//		bReturnFlag = FALSE;
//		return bReturnFlag;
//	}
//
//	if (g_pMaskMap->MaskUnload() != 0)
//	{
//		::AfxMessageBox(_T(" Mask UnLoad 에 실패 하였습니다 ! "));
//		SaveLogFile("MacroSystem", _T("Mask UnLoad Fail"));
//		g_pWarning->ShowWindow(SW_HIDE);
//		bReturnFlag = FALSE;
//		return bReturnFlag;
//	}
//
//	SaveLogFile("MacroSystem", _T("Mask UnLoad end"));
//	g_pWarning->ShowWindow(SW_HIDE);
//	return bReturnFlag;
//}
//
//BOOL AlignCommand::Align()
//{
//	int ret = 0;
//	BOOL bReturnFlag = TRUE;
//
//	//////////////////////////////////////////////////////
//	//g_pMaskMap->OnBnClickedButtonMaskalign();
//	/////////////////////////////////////////////////////
//
//	g_pLog->Display(0, _T("Macro system :: Align Start!"));
//
//	if (g_pConfig->m_nEquipmentMode == OFFLINE)
//	{
//		bReturnFlag = FALSE;
//		return bReturnFlag;
//	}
//
//	CAutoMessageDlg MsgBoxAuto(g_pAdam);
//
//	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
//	{
//		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
//		bReturnFlag = FALSE;
//		return bReturnFlag;
//	}
//
//
//	switch (g_pConfig->m_nEquipmentType)
//	{
//	
//	
//	case EUVPTR:
//		ret = g_pMaskMap->MaskAlign_OM(g_pMaskMap->m_bManualAlign);
//		if (ret == 0)//EUV 영역은 OM-EUV Offset을 이용해서 Align 완료
//		{
//			g_pMaskMap->m_dEUVAlignPointRT_posx_mm = g_pMaskMap->m_dOMAlignPointRT_posx_mm - g_pConfig->m_dGlobalOffsetX_mm;
//			g_pMaskMap->m_dEUVAlignPointRT_posy_mm = g_pMaskMap->m_dOMAlignPointRT_posy_mm + g_pConfig->m_dGlobalOffsetY_mm;
//			g_pConfig->m_dEUVAlignPointRT_X_mm = g_pMaskMap->m_dEUVAlignPointRT_posx_mm;
//			g_pConfig->m_dEUVAlignPointRT_Y_mm = g_pMaskMap->m_dEUVAlignPointRT_posy_mm;
//			g_pMaskMap->m_dEUVAlignPointLT_posx_mm = g_pMaskMap->m_dOMAlignPointLT_posx_mm - g_pConfig->m_dGlobalOffsetX_mm;
//			g_pMaskMap->m_dEUVAlignPointLT_posy_mm = g_pMaskMap->m_dOMAlignPointLT_posy_mm + g_pConfig->m_dGlobalOffsetY_mm;
//			g_pConfig->m_dEUVAlignPointLT_X_mm = g_pMaskMap->m_dEUVAlignPointLT_posx_mm;
//			g_pConfig->m_dEUVAlignPointLT_Y_mm = g_pMaskMap->m_dEUVAlignPointLT_posy_mm;
//			g_pMaskMap->m_dEUVAlignPointLB_posx_mm = g_pMaskMap->m_dOMAlignPointLB_posx_mm - g_pConfig->m_dGlobalOffsetX_mm;
//			g_pMaskMap->m_dEUVAlignPointLB_posy_mm = g_pMaskMap->m_dOMAlignPointLB_posy_mm + g_pConfig->m_dGlobalOffsetY_mm;
//			g_pConfig->m_dEUVAlignPointLB_X_mm = g_pMaskMap->m_dEUVAlignPointLB_posx_mm;
//			g_pConfig->m_dEUVAlignPointLB_Y_mm = g_pMaskMap->m_dEUVAlignPointLB_posy_mm;
//
//			g_pMaskMap->m_bEUVAlignComplete = TRUE;
//			g_pConfig->m_bEUVAlignCompleteFlag = g_pMaskMap->m_bEUVAlignComplete;
//			g_pConfig->SaveRecoveryData();
//			g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
//			g_pWarning->ShowWindow(SW_HIDE);
//			MsgBoxAuto.DoModal(_T(" Mask Align이 완료되었습니다! "), 2);
//			g_pMaskMap->m_strStageStatus.Format(_T("Mask Align(EUV) 완료!"));
//			g_pMaskMap->m_MaskMapWnd.Invalidate(FALSE);
//			g_pMaskMap->UpdateData(FALSE);
//			g_pLog->Display(0, _T("CMaskMapDlg::MaskAlign_EUV_SREM() End!"));
//		}
//		else
//			bReturnFlag = FALSE;
//		break;
//	default:
//		break;
//	}
//
//	CString str;
//	str.Format(_T("MaskAlign return: %d"), ret);
//	g_pLog->Display(0, str);
//
//	return bReturnFlag;
//}
//
//OMTOEUVCommand::OMTOEUVCommand(MacroStruct * GetVectorData)
//{
//	CommandState = TRUE;
//}
//
//BOOL OMTOEUVCommand::ChangeOmToEuv()
//{
//	BOOL bReturnFlag = TRUE;
//
//	g_pLog->Display(0, _T("Macro system :: Change Om To Euv!"));
//	SaveLogFile("MacroSystem", _T("Change Om To Euv Start"));
//	g_pWarning->m_strWarningMessageVal = " MacroSystem ::Change Om To Euv Start! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//	g_pMaskMap->ChangeOMEUVSet(OMTOEUV);
//	g_pWarning->ShowWindow(SW_HIDE);
//	SaveLogFile("MacroSystem", _T("Change Om To Euv End"));
//	return bReturnFlag;
//}
//
//EUVTOOMCommand::EUVTOOMCommand(MacroStruct * GetVectorData)
//{
//	CommandState = TRUE;
//}
//
//BOOL EUVTOOMCommand::ChangeEuvToOm()
//{
//
//	BOOL bReturnFlag = TRUE;
//
//	g_pLog->Display(0, _T("Macro system :: Change Euv To Om!"));
//	SaveLogFile("MacroSystem", _T("Change Euv To Om Start"));
//	g_pWarning->m_strWarningMessageVal = " MacroSystem ::Change Euv To Om Start! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//	g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
//	g_pWarning->ShowWindow(SW_HIDE);
//	SaveLogFile("MacroSystem", _T("Change Euv To Om End"));
//	return bReturnFlag;
//}
//
//ZAXISORIGINCommand::ZAXISORIGINCommand(MacroStruct * GetVectorData)
//{
//	CommandState = TRUE;
//}
//
//BOOL ZAXISORIGINCommand::ZMoveOrigin()
//{
//	BOOL bReturnFlag = TRUE;
//
//	SaveLogFile("MacroSystem", _T("Z Move Origin "));
//	g_pWarning->m_strWarningMessageVal = " MacroSystem ::Z Axis Move Origin ! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
////	g_pScanStage->Move_Origin();
//
//	g_pWarning->ShowWindow(SW_HIDE);
//	SaveLogFile("MacroSystem", _T("Z Axis Move Origin End"));
//
//	return bReturnFlag;
//}
//
//BOOL WAITSECCommand::WaitSecond(int sec)
//{
//	BOOL bReturnFlag = TRUE;
//
//
//	SaveLogFile("MacroSystem", _T("WaitSec "));
//	g_pWarning->m_strWarningMessageVal = " MacroSystem ::Wait Second ! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//	WaitSec(sec);
//
//	g_pWarning->ShowWindow(SW_HIDE);
//	SaveLogFile("MacroSystem", _T("WaitSec End "));
//
//	return bReturnFlag;
//}
//
//WAITSECCommand::WAITSECCommand(MacroStruct * GetVectorData)
//{
//	//CommandState = TRUE;
//	//int paramcnt = stoi(GetVectorData->paramcnt);
//	//
//	//if (paramcnt < 1)
//	//{
//	//	CommandState = FALSE;
//	//}
//	//else
//	//{
//	//	second = stoi(GetVectorData->Parameter.at(0));
//	//}
//
//}
//
//BOOL ZFOCUSMOVECommand::ZMove(double delta_target)
//{
//	BOOL bReturnFlag = TRUE;
//
//	SaveLogFile("MacroSystem", _T("ZMove "));
//	g_pWarning->m_strWarningMessageVal = " MacroSystem :: Z Move ! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//
//	if (g_pAdam->MoveZCapsensorFocusPosition(FALSE, FALSE, 5, NULL, NULL, NULL, delta_target) != 0)
//	{
//		bReturnFlag = FALSE;
//		g_pWarning->ShowWindow(SW_HIDE);
//		SaveLogFile("MacroSystem", _T(" Z Move Error "));
//		return bReturnFlag;
//	}
//	
//
//	g_pWarning->ShowWindow(SW_HIDE);
//	SaveLogFile("MacroSystem", _T(" Z Move End "));
//
//	return bReturnFlag;
//}
//
//ZFOCUSMOVECommand::ZFOCUSMOVECommand(MacroStruct * GetVectorData)
//{
//	//CommandState = TRUE;
//	//int paramcnt = stoi(GetVectorData->paramcnt);
//	//
//	//if (paramcnt < 1)
//	//{
//	//	CommandState = FALSE;
//	//}
//	//else
//	//{
//	//	target = stof(GetVectorData->Parameter.at(0));
//	//}
//}
//
//BOOL ScanStartCommand::ScanStart(double* parameter)
//{
//	double* CommandParameter = parameter;
//	CString Filename ;
//
//	int EuvImage_Fov = static_cast<int>(CommandParameter[0]);
//	int EuvImage_StepSize = static_cast<int>(CommandParameter[1]);
//	int EuvImage_Repeatno = static_cast<int>(CommandParameter[2]);
//
//
//	SaveLogFile("MacroSystem", _T("Scan Start "));
//	g_pWarning->m_strWarningMessageVal = " MacroSystem :: Scan ! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//	g_pAdam->m_bEUVCrossLineDisplay = FALSE;
//	//g_pAdam->Invalidate(FALSE);
//	g_pAdam->UpdateData(FALSE);
//
//
//	// Laser Feedback으로 전환
//	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//	{
//		//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
//		g_pNavigationStage->SetLaserMode();
//	}
//	
//	// Set FOV info and Update ADAM UI
//	g_pAdam->m_nEuvImage_Fov = EuvImage_Fov*1000.;
//	g_pAdam->m_nEuvImage_ScanGrid = EuvImage_StepSize;
//	g_pAdam->m_nEuvImage_ScanNumber = EuvImage_Repeatno;
//	g_pAdam->SetAdamFovInfoUi();
//
//
//	g_pWarning->m_strWarningMessageVal = " MacroSystem :: Scan Start! ";
//	g_pWarning->UpdateData(FALSE);
//	
//	//AdamStart
//	g_pAdam->ADAMRunStart();
//	g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
////	g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, g_pAdam->m_nEuvImage_ScanNumber);
//
//	while (g_pAdam->m_bIsScaningOn)
//	{
//		//ProcessMessages();
//		//Sleep(10);
//		WaitSec(1);
//	}
//
//	g_pAdam->ADAMAbort();
//
//	//MacroSystem.target_x_pos = StageMove.x;
//	//MacroSystem.target_y_pos = StageMove.y;
//	//MacroSystem.target_z_pos = ZFoucsMove.target;
//	//Filename.Format(_T("X%.4f_Y%.4f_Z%.4f"), MacroSystem.target_x_pos, MacroSystem.target_y_pos, MacroSystem.target_z_pos);
//
//	
//	Filename.Format(_T("X_%.3f_Y_%.3f_Z_%.3f"), MacroSystem.target_x_pos, MacroSystem.target_y_pos, MacroSystem.target_z_pos);
//	g_pAdam->MacroScanFilteredImageFileSave_Resize(Filename, 50, g_pAdam->m_dFilteredImage_Data3D[0]);
//
////	if (g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, EuvImage_Fov, EuvImage_StepSize, EuvImage_Repeatno) != 0)// Scan Stage Run
////
////	{
////		return FALSE;
////	}
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_HIDE);
//	SaveLogFile("MacroSystem", _T("Scan end"));
//
//	return TRUE;
//}
//
//ScanStartCommand::ScanStartCommand(MacroStruct * GetVectorData)
//{
//	//CommandState = TRUE;
//	//int paramcnt = stoi(GetVectorData->paramcnt);
//	//
//	//if (paramcnt < 3)
//	//{
//	//	CommandState = FALSE;
//	//}
//	//else
//	//{
//	//	SanStartCommandParameter = new double[paramcnt];
//	//
//	//	for (int i = 0; i < paramcnt; i++)
//	//	{
//	//		SanStartCommandParameter[i] = stof(GetVectorData->Parameter.at(i));
//	//	}
//	//}
//
//}
//
//BOOL ThroughFocusScanCommand::ThroughFocusScan(double* parameter)
//{
//	double* CommandParameter = parameter;
//	CString Filename;
//
//	int EuvImage_Fov = static_cast<int>(CommandParameter[0]);
//	int EuvImage_StepSize = static_cast<int>(CommandParameter[1]);
//	int EuvImage_Repeatno = static_cast<int>(CommandParameter[2]);
//	int nThroughFocusNo = static_cast<int>(CommandParameter[3]);
//	double dThroughFocusStep = CommandParameter[4];
//
//
//	SaveLogFile("MacroSystem", _T("ThroughFocusScan Start "));
//	g_pWarning->m_strWarningMessageVal = " MacroSystem :: Through Focus Scan Start ! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//
//	g_pAdam->m_bEUVCrossLineDisplay = FALSE;
//	g_pAdam->UpdateData(FALSE);
//
//	// Laser Feedback으로 전환
//	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
//	{
//		
//		g_pNavigationStage->SetLaserMode();
//	}
//
//	// Set FOV info and Update ADAM UI
//	g_pAdam->m_nEuvImage_Fov = EuvImage_Fov * 1000.;
//	g_pAdam->m_nEuvImage_ScanGrid = EuvImage_StepSize;
//	g_pAdam->m_nEuvImage_ScanNumber = EuvImage_Repeatno;
//	g_pAdam->SetAdamFovInfoUi();
//
//	double refY = g_pNavigationStage->GetTargetPosmm(STAGE_Y_AXIS);
//	double delta_z=0;
//
//	// Through Focus
//	for (int j = 0; j < nThroughFocusNo; j++)
//	{					
//		double y;
//		
//		int step_z;
//
//		//Cross Step으로 이동			
//		step_z = ((j + 1) / 2) * pow(-1, j)*-1;
//		delta_z = dThroughFocusStep * step_z;
//			
//
//		g_pAdam->MoveZCapsensorFocusPosition(FALSE, FALSE, 5, NULL, NULL, NULL, delta_z) != 0;
//			
//		//y축 보상
//		if (nThroughFocusNo > 0)
//		{
//			y = refY + delta_z * 0.105 / 1000.0;
//			g_pNavigationStage->MoveAbsolute(STAGE_Y_AXIS, y);
//			g_pNavigationStage->WaitInPosition(STAGE_Y_AXIS, y);
//		}
//				
//		g_pAdam->ADAMRunStart();
//		g_pAdam->SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
////		g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, g_pAdam->m_nEuvImage_ScanNumber);
//
//		while (g_pAdam->m_bIsScaningOn)
//		{
//			WaitSec(1);
//		}
//		//SaveFile 추가 필요
//		Filename.Format(_T("X_%.3f_Y_%.3f_Z_%.3f"), Macro::target_x_pos, Macro::target_y_pos, delta_z);
//		g_pAdam->MacroScanFilteredImageFileSave_Resize(Filename, 50, g_pAdam->m_dFilteredImage_Data3D[0]);
//	}
//
//	g_pAdam->ADAMAbort();
//
//	SaveLogFile("MacroSystem", _T("ThroughFocusScan end"));
//	g_pWarning->ShowWindow(SW_HIDE);
//
//	return TRUE;
//}
//
//ThroughFocusScanCommand::ThroughFocusScanCommand(MacroStruct * GetVectorData)
//{
//	//CommandState = TRUE;
//	//int paramcnt = stoi(GetVectorData->paramcnt);
//	//
//	//if (paramcnt < 5)
//	//{
//	//	CommandState = FALSE;
//	//}
//	//else
//	//{
//	//	ThroughScanCommandParameter = new double[paramcnt];
//	//	for (int i = 0; i < paramcnt; i++)
//	//	{
//	//		ThroughScanCommandParameter[i] = stof(GetVectorData->Parameter.at(i));
//	//	}
//	//}
//}
//
//
//BOOL SQOneStageMoveOnCommand::SQOneStageMoveConrtol(double* parameter)
//{
//	BOOL bReturnFlag = TRUE;
//
//	g_pLog->Display(0, _T("Macro system :: SQOne Stage Move Start!"));
//	SaveLogFile("MacroSystem", _T(" SQOne Stage Move Start!"));
//	g_pWarning->m_strWarningMessageVal = " MacroSystem :: SQOne Stage Move Start! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//	double* CommandParameter = parameter;
//
//	double Horizontal = CommandParameter[0];
//	double dVertical = CommandParameter[1];
//	double dBeam = CommandParameter[2];
//	double dPitch = CommandParameter[3];
//	double dYaw = CommandParameter[4];
//	double dRoll = CommandParameter[5];
//
//	if (g_pSqOneControl->moveSQ1(Horizontal, dVertical, dBeam, dPitch, dYaw, dRoll) != 0)
//	{
//		AfxMessageBox("SQOne Stage Move Error");
//		SaveLogFile("MacroSystem", _T(" SQOne Stage Move Error!"));
//		g_pWarning->ShowWindow(SW_HIDE);
//		bReturnFlag = FALSE;
//
//		return bReturnFlag;
//	}
//
//	SaveLogFile("MacroSystem", _T(" SQOne Stage Move End!"));
//	g_pWarning->ShowWindow(SW_HIDE);
//	return bReturnFlag;
//}
//
//SQOneStageMoveOnCommand::SQOneStageMoveOnCommand(MacroStruct * GetVectorData)
//{
//	//CommandState = TRUE;
//	//int paramcnt = stoi(GetVectorData->paramcnt);
//	//
//	//if (paramcnt < 6)
//	//{
//	//	CommandState = FALSE;
//	//}
//	//else
//	//{
//	//	SQOneStageMoveCommandParameter = new double[paramcnt];
//	//	for (int i = 0; i < paramcnt; i++)
//	//	{
//	//		SQOneStageMoveCommandParameter[i] = stof(GetVectorData->Parameter.at(i));
//	//	}
//	//}
//}
//
//StageZRelativeMoveCommand::StageZRelativeMoveCommand(MacroStruct * GetVectorData)
//{
//	//CommandState = TRUE;
//	//int paramcnt = stoi(GetVectorData->paramcnt);
//	//
//	//if (paramcnt < 1)
//	//{
//	//	CommandState = FALSE;
//	//}
//	//else
//	//{
//	//	ZRelativeMoveParameter = new double[paramcnt];
//	//	for (int i = 0; i < paramcnt; i++)
//	//	{
//	//		ZRelativeMoveParameter[i] = stof(GetVectorData->Parameter.at(i));
//	//	}
//	//}
//}
//
//BOOL StageZRelativeMoveCommand::StageZRelativeMove(double step_z_um)
//{
//	BOOL ReturnFlag = TRUE;
//
//	if (step_z_um > 0)
//	{
//	//	g_pScanStage->MoveZRelative_SlowInterlock(step_z_um, MINUS_DIRECTION);
//	}
//	else
//	{
////		g_pScanStage->MoveZRelative_SlowInterlock(step_z_um, PLUS_DIRECTION);
//	}
//
//	return ReturnFlag;
//}
//
//BOOL Macro::MakeRecipeCommand(vector <MacroStruct> &GetVectorData)
//{
//	BOOL ReturnFlag = TRUE;
//
//	Clear();
//
//	for (int i = 0; i < GetVectorData.size(); i++)
//	{
//
//		////MacroParamStruct = GetVectorData[i];
//		//if (GetVectorData[i].command == "STAGE_MOVE")
//		//{
//		//	AddCommand(new StageMoveCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "EUV_ON")
//		//{
//		//	AddCommand(new EuvOnCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "EUV_OFF")
//		//{
//		//	AddCommand(new EuvOffCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "LOAD")
//		//{
//		//	AddCommand(new MaskLoadCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "UNLOAD")
//		//{
//		//	AddCommand(new MaskUnLoadCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "SHUTTER_ON")
//		//{
//		//	AddCommand(new ShutterOnCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "SHUTTER_OFF")
//		//{
//		//	AddCommand(new ShutterOffCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "EUV_TO_OM")
//		//{
//		//	AddCommand(new EUVTOOMCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "OM_TO_EUV")
//		//{
//		//	AddCommand(new OMTOEUVCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "Z_MOVE_ORIGIN")
//		//{
//		//	AddCommand(new ZAXISORIGINCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "WAIT_SEC")
//		//{
//		//	AddCommand(new WAITSECCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "Z_MOVE")
//		//{
//		//	AddCommand(new ZFOCUSMOVECommand((&GetVectorData[i])));
//		//}
//		//else if (GetVectorData[i].command == "THROUGH_FOCUS_SCAN")
//		//{
//		//	AddCommand(new ThroughFocusScanCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "SCAN_START")
//		//{		
//		//	AddCommand(new ScanStartCommand((&GetVectorData[i])));
//		//}
//		//else if (GetVectorData[i].command == "SQ_ONE_STAGE_MOVE")
//		//{
//		//	AddCommand(new SQOneStageMoveOnCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "STEP_STAGE_MOVE")
//		//{
//		//	AddCommand(new StageStepMoveCommand(&GetVectorData[i]));
//		//}
//		//else if (GetVectorData[i].command == "STEP_Z_MOVE")
//		//{
//		//	AddCommand(new StageZRelativeMoveCommand(&GetVectorData[i]));
//		//}
//	}
//
//	return ReturnFlag;
//
//}


