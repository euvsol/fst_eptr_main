﻿#pragma once
#define GatherNum 10000

// CBeamOptimizationDlg 대화 상자

class CBeamAlignMainDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CBeamAlignMainDlg)

public:
	CBeamAlignMainDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CBeamAlignMainDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_BEAMALIGNMAIN_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	HICON m_LedIcon[3];
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnPaint();
	afx_msg void OnDestroy();

	afx_msg void OnBnClickedButtonSqoneShow2DScanDlg();
	afx_msg void OnBnClickedButtonSqoneShow1DScanDlg();
	afx_msg void OnBnClickedButtonSqoneShowautoalignDlg();
	afx_msg void OnBnClickedButtonBeammainShowautoaligndlg2d();
	afx_msg void OnBnClickedButtonSqoneConnect();
	afx_msg void OnBnClickedButtonSqoneDisconnect();
	afx_msg void OnBnClickedButtonSqoneStop();
	afx_msg void OnBnClickedButtonBeammainTest();

	int isCommunicationError = 0;
	void Tcip_EthernetCom_Check();

	double m_chartx[GatherNum];
	double m_charty[GatherNum];
	double m_chartz[GatherNum];

	double m_d2DScanIntensity[GatherNum];
	double m_d2DScanpPrameter1[GatherNum];
	double m_d2DScanpPrameter2[GatherNum];

	double m_dCurcleScanMaxIntensity[GatherNum];
	double m_dCurcleScanMaxParameter1[GatherNum];
	double m_dCurcleScanMaxParameter2[GatherNum];

	double m_dIntensity;

	void resetDataset();
	int checkLimit(double dHorizontal, double dVertical, double dBeam, double dPitch, double dYaw, double dRoll);


	BOOL Display(int nModule = 0, CString strLogMsg = _T(""), BOOL bDisplay = TRUE, BOOL bSaveFile = TRUE);
	virtual BOOL PreTranslateMessage(MSG* pMsg);


	//////////////////////////////simulator//////////////////////////////////
	double m_dBeamIniParaHorizontal;
	double m_dBeamIniParaVertical;
	double m_dBeamIniParaBeam;
	double m_dBeamIniParaPitch;
	double m_dBeamIniParaYaw;
	double m_dBeamIniParaRoll;
	double m_dBeamIniParaUx;
	double m_dBeamIniParaUy;
	double m_dBeamIniParaUz;

	double m_dBeamSetParaHorizontal;
	double m_dBeamSetParaVertical;
	double m_dBeamSetParaPitch;
	double m_dBeamSetParaYaw;

	void setOptimizationInitial();

	void setBeamInitail();
	void caculateBeamReflectVec(double *pu, double *bu, double *uuu);
	void caculateBeamReflectPos(double *pp, double *pu, double *bp, double *bu, double *ppp);
	void caculateBeamIntensity(double *Lb0, double *b0u, double *intensity);
	void caculateIntensty();
	//////////////////////////////simulator//////////////////////////////////

	int m_nDetectorIndex;

	afx_msg void OnBnClickedCheckBeammainDetector1();
	afx_msg void OnBnClickedCheckBeammainDetector2();
	afx_msg void OnBnClickedCheckBeammainDetector3();
	afx_msg void OnBnClickedCheckBeammainDetector4();
	virtual BOOL DestroyWindow();

	void getIntensityfromADAM();
	int getMeanValuefromXrayCam();
	int executeMeasureformPhase();
	afx_msg void OnBnClickedCheckBeammainSimulator();
};
