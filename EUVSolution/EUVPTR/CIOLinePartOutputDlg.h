﻿#pragma once


// CDigitalOutputLinePart 대화 상자

class CDigitalOutputLinePart : public CDialogEx
{
	DECLARE_DYNAMIC(CDigitalOutputLinePart)

public:
	CDigitalOutputLinePart(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CDigitalOutputLinePart();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IO_LINE_PART_OUTPUT_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();

	HICON	m_LedIcon[3];

	CBrush  m_brush;
	CBrush  m_brush2;
	CFont	m_font;
	void InitControls_DO_LINE_PART();
	void OnUpdateDigitalOutput_Line_Part();
	void SetOutputLinePartBitOnOff(int nIndex);
	afx_msg void OnBnClickedCheckDigitaloutLinePartY000();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY001();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY002();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY003();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY004();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY005();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY006();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY007();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY008();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY009();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY010();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY011();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY012();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY013();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY014();
	afx_msg void OnBnClickedCheckDigitaloutLinePartY015();
};
