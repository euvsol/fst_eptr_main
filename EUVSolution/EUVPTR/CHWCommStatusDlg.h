﻿/**
 * HW Communication Status Check Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
  * Description: H/W Module들의 통신상태,초기화상태,Homming상태,에러상태를 점검하고 Display한다.
*/
#pragma once


// CHWCommStatusDlg 대화 상자

class CHWCommStatusDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CHWCommStatusDlg)

public:
	CHWCommStatusDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CHWCommStatusDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_HW_COMM_STATUS_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();

	void		ADAMCommStatus(BOOL bCommStat);
	void		EfemCommStatus(BOOL bCommStat);
	void		CrevisCommStatus(BOOL bCommStat);
	void		VacuumRobotCommStatus(BOOL bCommStat);
	void		LinearRevolverCommStatus(BOOL bCommStat);
	void		AFModuleCommStatus(BOOL bCommStat);
	void		MCGaugeCommStatus(BOOL bCommStat);
	void		LLCGaugeCommStatus(BOOL bCommStat);
	void		EUVSourceCommStatus(BOOL bCommStat);
	void		MCTmpCommStatus(BOOL bCommStat);
	void		LLCTmpCommStatus(BOOL bCommStat);
	void		NavigationStageCommStatus(BOOL bCommStat);
	void		ScanStageCommStatus(BOOL bCommStat);
	void		OmCameraCommStatus(BOOL bCommStat);
	void		FilterStageCommStatus(BOOL bCommStat);
	void		XRayCameraCommStatus(BOOL bCommStat);


};
