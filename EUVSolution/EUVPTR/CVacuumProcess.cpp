#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


CVacuumProcess::CVacuumProcess()
{
	m_bVacuumThreadStop = TRUE;
	m_pVaccumThread = NULL;

	m_b_MC_VacuumThreadStop = TRUE;
	m_p_MC_VaccumThread = NULL;

	m_nLlcErrorCode = 0;
	m_nMcErrorCode  = 0;

	m_bLlcVenting = FALSE;
	m_bMcVenting  = FALSE;

	VacuumInitAll();

	g_pDevMgr->RegisterObserver(this);
}


CVacuumProcess::~CVacuumProcess()
{
	if (m_pVaccumThread != NULL)
	{
		m_bVacuumThreadStop = TRUE;
		if (WaitForSingleObject(m_pVaccumThread->m_hThread, 5000) != WAIT_OBJECT_0)
		{
			::TerminateThread(m_pVaccumThread->m_hThread, 0);
		}
	}

	if (m_p_MC_VaccumThread != NULL)
	{
		m_bVacuumThreadStop = TRUE;
		if (WaitForSingleObject(m_p_MC_VaccumThread->m_hThread, 5000) != WAIT_OBJECT_0)
		{
			::TerminateThread(m_p_MC_VaccumThread->m_hThread, 0);
		}
	}
}

int CVacuumProcess::VacuumInitAll()
{
	int ret = 0;

	if (g_pMCTmp_IO == NULL || g_pLLCTmp_IO == NULL || g_pGauge_IO == NULL)
		return -999;

	//port reopen
	//pVac->OpenComm();
	//m_nSequence_Error_Code_Define = SEQUENCE_DONE;
	//TMP Error Reset
//	pMainTMP->ResetOperation(); //only used for shimatu TMP
//	pLLKTMP->ResetOperation();  //only used for shimatu TMP

	//TMP Init
	//g_pMCTmp_IO->initialize();
	//g_pMC2Tmp->initialize();
	//g_pLLCTmp_IO->initialize();

	//Vacuum Gauge initialize
	//g_pMCGauge->initialize();
	//g_pGauge_IO->initialize();

	// Pumping LLC Sequence Init
	m_LlcVacuumState = State_IDLE;
	m_bVacuumThreadStop = TRUE;

	// Pumping MC Sequence lnit
	m_McVacuumState = MC_State_IDLE;
	m_b_MC_VacuumThreadStop = TRUE;
	

	m_bMcVentingState = false;

	m_nMc_slow_rough_time_cnt = 0;
	m_nMc_fast_rough_time_cnt = 0;
	m_nMc_tmp_rough_time_cnt = 0;
	m_nMc_standby_vent_time_cnt = 0;
	m_nMc_slow_vent_time_cnt = 0;
	m_nMc_fast_vent_time_cnt = 0;
	m_nLlc_slow_rough_time_cnt = 0;
	m_nLlc_fast_rough_time_cnt = 0;
	m_nLlc_tmp_rough_time_cnt = 0;
	m_nLlc_standby_vent_time_cnt = 0;
	m_nLlc_slow_vent_time_cnt = 0;
	m_nLlc_fast_vent_time_cnt = 0;

	return ret;
}

int CVacuumProcess::Is_VacuumModule_OK()
{
	int ret = 0;

	return ret;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////// Vacuum Thread Stop Sequence //////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

int CVacuumProcess::VacThreadStop()
{
	int ret = 0;
	
	m_bVacuumThreadStop = TRUE;	// 쓰레드내의 while()에 들어가는 변수 => Thread를 정상 종료시킴
	Sleep(100);

	if (m_pVaccumThread != NULL)
	{
		HANDLE threadHandle = m_pVaccumThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);

		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // *요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)//259
			{
				//AfxEndThread(nExitCode);
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
	}

	return ret;
}


int CVacuumProcess::MC_VacThreadStop()
{
	int ret = 0;

	m_b_MC_VacuumThreadStop = TRUE;	// 쓰레드내의 while()에 들어가는 변수 => Thread를 정상 종료시킴
	Sleep(100);

	if (m_p_MC_VaccumThread != NULL)
	{
		HANDLE threadHandle = m_p_MC_VaccumThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);

		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // *요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)//259
			{
				//AfxEndThread(nExitCode);
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
	}

	return ret;
}

int CVacuumProcess::GetLLCVacuumStatus()
{
	int ret = -999;
	double a = g_pGauge_IO->GetLlcVacuumRate();

	if (g_pGauge_IO == NULL /*or Gauge Error or 통신 끊김*/)
		return -999;

	if (g_pGauge_IO->GetLlcVacuumRate() < 0.0)
		return -999;

	if (g_pGauge_IO->GetLlcVacuumRate() < g_pConfig->m_dPressure_Rough_End)
		ret = CHAMBER_PUMPED;
	else if (g_pGauge_IO->GetLlcVacuumRate() >= g_pConfig->m_dPressure_Vent_End - g_pConfig->m_dPressure_Vent_Tolerance
		&& g_pGauge_IO->GetLlcVacuumRate() < g_pConfig->m_dPressure_Vent_End + g_pConfig->m_dPressure_Vent_Tolerance)
		ret = CHAMBER_VENTED;
	else if (g_pGauge_IO->GetLlcVacuumRate() >= g_pConfig->m_dPressure_Vent_End + g_pConfig->m_dPressure_Vent_Tolerance)
		ret = CHAMBER_OVER_VENTED;
	else
		ret = CHAMBER_MIDDLE_PUMPED;

	return ret;
}


int CVacuumProcess::GetMCVacuumStatus()
{
	int ret = -999;

	if (g_pGauge_IO == NULL)
		return -999;

	if(g_pGauge_IO->GetMcVacuumRate() < 0.0)
		return -999;

	if (g_pGauge_IO->GetMcVacuumRate() < g_pConfig->m_dPressure_Rough_End) // 0.00003  = 3.0x10^-5
		ret = CHAMBER_PUMPED;
	else if (g_pGauge_IO->GetMcVacuumRate() >= g_pConfig->m_dPressure_Vent_End - g_pConfig->m_dPressure_Vent_Tolerance
		&& g_pGauge_IO->GetMcVacuumRate() < g_pConfig->m_dPressure_Vent_End + g_pConfig->m_dPressure_Vent_Tolerance)
		ret = CHAMBER_VENTED;
	else if (g_pGauge_IO->GetMcVacuumRate() >= g_pConfig->m_dPressure_Vent_End + g_pConfig->m_dPressure_Vent_Tolerance)
		ret = CHAMBER_OVER_VENTED;
	else
		ret = CHAMBER_MIDDLE_PUMPED;

	return ret;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////// MC Pumping or Venting Thread Start ///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int CVacuumProcess::MC_Pumping_Start()
{
	CString str;
	
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if (m_p_MC_VaccumThread == NULL)
		{
			m_McVacuumState = MC_Pumping_START;
			m_bMcVenting = FALSE;

			m_p_MC_VaccumThread = ::AfxBeginThread(MC_VacuumThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
			
			str = "MC Pumping Thread 가동 시작 !";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);

			//////////////////////////////////
			// OPER MODE ON 동작 시퀀스 (임시삭제)
			//////////////////////////////////

			//if (g_pIO->IO_MODE == OPER_MODE_ON)
			//{
			//	m_McVacuumState = MC_Pumping_START;
			//	m_MC_seq_state = Pumping_START_State;
			//	m_p_MC_VaccumThread = ::AfxBeginThread(MC_VacuumThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
			//	str = "MC Pumping Thread 가동 시작 !";
			//	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			//	g_pLog->Display(0, str);
			//}
			//else
			//{
			//	str = "MC Pumping Thread 불가 !! OPER MODE 가 아닙니다";
			//	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			//	g_pLog->Display(0, str);
			//
			//
			//}

		}
		else
		{
			str = "MC Pumping Thread가 이미 동작 중이므로 가동 실패 !";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			return -999;
		}
	}
	else
	{
		str = "Crevis_Open_Port Fail 이므로 가동 실패 !";
		::AfxMessageBox("Crevis_Open_Port Fail 이므로 가동 실패 !", MB_ICONINFORMATION);
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return -999;
	}

	return 0;
}

int CVacuumProcess::MC_Venting_Start()
{
	CString str;

	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if (m_p_MC_VaccumThread == NULL)
		{
			m_McVacuumState = MC_Venting_START;
			m_bMcVenting = TRUE;

			m_p_MC_VaccumThread = ::AfxBeginThread(MC_VacuumThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
			
			str = "MC Venting Thread 가동 시작 !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);

			//////////////////////////////////
			// OPER MODE ON 동작 시퀀스 (임시삭제)
			//////////////////////////////////

			//if (g_pIO->IO_MODE == MAINT_MODE_ON)
			//{
			//	m_McVacuumState = MC_Venting_START;
			//	m_MC_seq_state = Venting_START_State;
			//	m_p_MC_VaccumThread = ::AfxBeginThread(MC_VacuumThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
			//	//g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(false);
			//	//g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(false);
			//	//g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(false);
			//	//g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(false);
			//	str = "MC Venting Thread 가동 시작 !";
			//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			//	g_pLog->Display(0, str);
			//
			//}
			//else
			//{
			//	str = "MC Venting Thread 불가 !! MAINT MODE 가 아닙니다";
			//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			//	g_pLog->Display(0, str);
			//	g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
			//	g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
			//	g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
			//	g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
			//}
		}
		else
		{
			str = "MC Venting Thread가 이미 동작 중이므로 가동 실패 !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			return -999;
		}
	}
	else
	{
		str = "Crevis_Open_Port Fail 이므로 가동 실패 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return -999;
	}

	return 0;
}


UINT CVacuumProcess::MC_VacuumThread(LPVOID pParam)
{
	CVacuumProcess*  pVP_MC = (CVacuumProcess*)pParam;

	pVP_MC->m_b_MC_VacuumThreadStop = FALSE;
	pVP_MC->m_nMcErrorCode = 0;

	while (!pVP_MC->m_b_MC_VacuumThreadStop)
	{
		pVP_MC->m_nMcErrorCode = pVP_MC->MC_Vacuum_Loop();

		if (pVP_MC->m_McVacuumState == MC_Pumping_COMPLETE && pVP_MC->m_nMcErrorCode == 0)
		{
			pVP_MC->m_b_MC_VacuumThreadStop = TRUE;
			pVP_MC->MC_Pumping_Complete();
		}

		if (pVP_MC->m_McVacuumState == MC_Venting_COMPLETE && pVP_MC->m_nMcErrorCode == 0)
		{
			pVP_MC->m_b_MC_VacuumThreadStop = TRUE;
			pVP_MC->MC_Venting_Complete();
			pVP_MC->m_bMcVentingState = false;
		}

		if (pVP_MC->m_nMcErrorCode != 0)
		{
			pVP_MC->m_bMcVentingState = false;

			if (pVP_MC->m_bMcVenting == TRUE)
			{
				pVP_MC->MC_Venting_Error();
			}
			else
			{
				pVP_MC->MC_Pumping_Error();
			}

			pVP_MC->m_b_MC_VacuumThreadStop = TRUE;
		}
	}

	pVP_MC->m_p_MC_VaccumThread = NULL;

	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////// MC Pumping or Venting Thread Start ///////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



int CVacuumProcess::MC_Vacuum_Loop()
{
	int	ret = 0;
	char* str;

	switch (m_McVacuumState) 
	{
		case MC_Pumping_START:
		{
			str = "[MC Pumping Start]";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);

			if (GetMCVacuumStatus() == CHAMBER_PUMPED)
			{
				str = "[MC PUMPED] MC 가 Pumping 되어 있습니다";
				SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				g_pLog->Display(0, str);
				  
				m_McVacuumState = MC_Pumping_COMPLETE;
			}
			else if (GetMCVacuumStatus() == VACUUM_ERROR)
			{
				str = "[MC Vacuum Gauge Error 발생]";
				SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				g_pLog->Display(0, str);
				ret = -999;
			}
			else
			{
				str = "[MC Pumping PreWork Start]";
				SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				g_pLog->Display(0, str);
				m_bMcVentingState = false;
				ret = MC_Pumping_PreWork();
				if (ret != OPERATION_COMPLETED)
					return ret;
				else
					m_McVacuumState = MC_Pumping_SLOWROUGH;
			}
			break;
		}
		case MC_Pumping_SLOWROUGH:
		{
			str = "[MC SlowRough Start]";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			ret = MC_SlowRough();
			if (ret != OPERATION_COMPLETED)
				return ret;
			else
				m_McVacuumState = MC_Pumping_FASTROUGH;
			break;
		}
		case MC_Pumping_FASTROUGH:
		{
			str = "[MC FastRough Start]";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			ret = MC_FastRough();
			if (ret != OPERATION_COMPLETED)
				return ret;
			else
				m_McVacuumState = MC_Pumping_TMPROUGH;
			break;
		}
		case MC_Pumping_TMPROUGH:
		{
			str = "[MC TMPRough Start]";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			ret = MC_TMPRough();
			if (ret != OPERATION_COMPLETED)
				return ret;
			else
				m_McVacuumState = MC_Pumping_COMPLETE;
			break;
		}
		case MC_Venting_START:
		{
			str = "[MC Venting Start]";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			///////////////////////////////////////
			// MC_Venting Sequence state
			////////////////////////////////////////


			//진공 상태에 따라 중간에서 시작할 수 있도록 하자.
			if (GetMCVacuumStatus() == CHAMBER_VENTED)
			{
				str = "[MC VENTED]";
				SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				g_pLog->Display(0, str);
				  
				m_McVacuumState = MC_Venting_COMPLETE;
			}
			else if (GetMCVacuumStatus() == VACUUM_ERROR)
			{
				str = "[MC 진공 Gauge Error 발생]";
				SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				g_pLog->Display(0, str);
				  
				ret = -999;
			}
			else
			{
				str = "[MC Venting PreWork() Start]";
				SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				g_pLog->Display(0, str);
				m_bMcVentingState = true;
				ret = MC_Venting_PreWork();
				if (ret != OPERATION_COMPLETED)
					return ret;
				else
					m_McVacuumState = MC_Venting_SLOWVENT;
			}
			break;
		}
		case MC_Venting_SLOWVENT:
		{
			str = "[MC_SlowVent() Start]";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			ret = MC_SlowVent();
			if (ret != OPERATION_COMPLETED)
				return ret;
			else
				m_McVacuumState = MC_Venting_FASTVENT;
			break;
		}
		case MC_Venting_FASTVENT:
		{
			str = "[MC_FastVent() Start]";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			ret = MC_FastVent();
			if (ret != OPERATION_COMPLETED)
				return ret;
			else
				m_McVacuumState = MC_Venting_COMPLETE;
			break;
		}
		default:
			break;
	}

	return ret;
}
int CVacuumProcess::MC_Pumping_Error()
{
	int	ret = 0;
	char* str;

	str = "MC_Pumpin_Error_발생";
	g_pLog->Display(0, str);
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));

	g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
	// MC PUMPING 도중 Error 시
	// MC Slow Rough Close
	// MC Fast Rough Close
	// LLC Slow Rough Close
	// LLC Fast Rough Close

	ret = g_pIO->Close_MC_SlowRoughValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_MC_SlowRoughValve();
		if(ret != 0)
		{
			str = " MC_Pumpin_Error() : Error 발생 후 MC Slow Rough Valve Close 명령 에러 발생 !";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_MC_SlowRoughValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = "MC_Pumpin_Error() : Error 발생 후 MC Slow Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	}

	ret = g_pIO->Close_MC_FastRoughValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_MC_FastRoughValve();
		if (ret != 0)
		{
			str = " MC_Pumpin_Error() : Error 발생 후 MC Fast Rough Valve Close 명령 에러 발생 !";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_MC_FastRoughValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " MC_Pumpin_Error() : Error 발생 후 MC Fast Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	}

	ret = g_pIO->Close_LLC_SlowRoughValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_LLC_SlowRoughValve();
		if (ret != 0)
		{
			str = " MC_Pumpin_Error() : Error 발생 후 LLC Slow Rough Valve Close 명령 에러 발생 !";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}

	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_LLC_SlowRoughValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = "MC_Pumpin_Error() : Error 발생 후 LLC Slow Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	}

	ret = g_pIO->Close_LLC_FastRoughValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_LLC_FastRoughValve();
		if (ret != 0)
		{
			str = " MC_Pumpin_Error() : Error 발생 후 LLC Fast Rough Valve Close 명령 에러 발생 !";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_LLC_FastRoughValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " MC_Pumpin_Error() : Error 발생 후 LLC Fast Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	}

	return ret;
}

int CVacuumProcess::MC_Venting_Error()
{
	int	ret = 0;
	char* str;

	// MC VENTING 도중 Error 시
	// Slow Vent Close
	// Fast Vent Close
	
	g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
	
	ret = g_pIO->Close_SlowVentValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_SlowVentValve();
		if (ret != 0)
		{
			str = " MC_Venting_Error(): Error 발생 후 Slow Vent Valve Open 명령 에러 발생 !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_SlowVentValve_Open() == VALVE_CLOSED) break;

	}

	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = "  MC_Venting_Error(): Error 발생 후 Slow Vent Valve Close Time Out 에러 발생";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
	}

	ret = g_pIO->Close_FastVentValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_FastVentValve();
		if (ret != 0)
		{
			str = "  MC_Venting_Error(): Error 발생 후 Fast Vent Valve Open 명령 에러 발생 !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_FastVentValve_Open() == VALVE_CLOSED) break;
	}

	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " MC_Venting_Error(): Error 발생 후 Fast Vent Valve Close Time Out 에러 발생";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
	}

	return ret;
}

int CVacuumProcess::LLC_Pumping_Error()
{
	int ret = 0;
	char* str;

	g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);

	ret = g_pIO->Close_LLC_SlowRoughValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_LLC_SlowRoughValve();
		if (ret != 0)
		{
			str = " LLC_Pumpin_Error() : Error 발생 후 LLC Slow Rough Valve Close 명령 에러 발생 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_SlowRoughValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_SlowRoughValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	//m_finish_time = 10;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLC_SlowRoughValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = "LLC_Pumpin_Error() : Error 발생 후 LLC Slow Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	}

	ret = g_pIO->Close_LLC_FastRoughValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_LLC_FastRoughValve();
		if (ret != 0)
		{
			str = " LLC_Pumpin_Error() : Error 발생 후 LLC Fast Rough Valve Close 명령 에러 발생 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_FastRoughValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	//m_finish_time = 10;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLC_FastRoughValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_Pumpin_Error() : Error 발생 후 LLC Fast Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	}

	return ret;
}

int CVacuumProcess::LLC_Venting_Error()
{
	int	ret = 0;
	char* str;

	

	ret = g_pIO->Close_SlowVentValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_SlowVentValve();
		if (ret != 0)
		{
			str = " LLC_Venting_Error(): Error 발생 후 Slow Vent Valve Open 명령 에러 발생 !";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_SlowVentValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}
	SaveLogFile("LC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_SlowVentValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_SlowVentValve_Open() == VALVE_CLOSED) 
			break;
	}

	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = "  LLC_Venting_Error(): Error 발생 후 Slow Vent Valve Close Time Out 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
	}

	ret = g_pIO->Close_FastVentValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_FastVentValve();
		if (ret != 0)
		{
			str = "  LLC_Venting_Error(): Error 발생 후 Fast Vent Valve Open 명령 에러 발생 !";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_FastVentValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}

	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_FastVentValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_FastVentValve_Open() == VALVE_CLOSED) 
			break;
	}

	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_Venting_Error(): Error 발생 후 Fast Vent Valve Close Time Out 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
	}

	return ret;
}

int CVacuumProcess::MC_Pumping_PreWork()
{
	int ret = SEQUENCE_DONE;
	
	char* str;

	//1. MC Dry Pump 정상 가동 확인(Run,Warning,Error 상태)
	//2. MC TMP 정상 가동 확인(Run,Normalization,온도,RPM,Warning,Error 상태)
	//3. MC Guage 정상 가동 확인
	//4. Source Gate Valve Close
	//5. TR Gate Valve Close
	//6. Mc Gate Valve Close 
	//7. MC TMP Foreline Valve Open -> 공정상태 상시 OPEN
	//8. MC Fast Rough Valve Close
	//9. MC Slow Rough Valve Close

	switch (g_pIO->Is_MC_DryPump_Status()) {
	case DRYPUMP_WARNING:
		str = " MC Dry Pump Warning 발생 ! ";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return MC_DRY_PUMP_ALARM_STATE;
	case DRYPUMP_ERROR:
		str = " MC Dry Pump Error 발생 ! ";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return MC_DRY_PUMP_ERROR_STATE;
	case DRYPUMP_STOP:
		str = " MC Dry Pump Stop 상태 ! ";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return MC_DRY_PUMP_STOP_STATE;
	default:
		str = "MC DryPump 정상 가동 확인";
		break;
	}
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	switch (g_pMCTmp_IO->GetMcTmpState()) {
	case g_pMCTmp_IO->Acceleration:
		str = " MC Turbo Pump 가속 중 ! ";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return MC_TMP_ACCELERATION_STATE;
	case g_pMCTmp_IO->Deceleration:
		str = " MC Turbo Pump 감속 중 ! ";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return MC_TMP_DECELERATION_STATE;
	case g_pMCTmp_IO->TMP_TURN_OFF:
		str = " MC Turbo Pump Offline ! ";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return MC_TMP_OFFLINE_STATE;
	case g_pMCTmp_IO->Error:
		str = " MC Turbo Pump Error 발생 ! ";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return MC_TMP_ERROR_STATE;
	default:
		break;
	}
	str = "MC TMP 정상 가동 확인";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  


	switch (g_pGauge_IO->GetGaugeState()) {
	case g_pGauge_IO->NotConnected:
		str = " MC Guage Offline ! ";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return VACCUM_SENSOR_OFFLINE_STATE;
	case g_pGauge_IO->Error:
		str = " MC Guage Error 발생 ! ";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return VACCUM_SENSOR_ERROR_STATE;
	default:
		break;
	}
	str = " MC Guage 정상 가동 확인 ";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	//if (g_pIO->Close_MC_Source_Gate_Valve() != TRUE)
	//{
	//	m_McVacuumState = MC_Pumping_ERROR;
	//	str = " Source Gate Valve Close 명령 에러 발생 !";
	//	g_pLog->Display(0, str);
	//	g_pLog->Display(0, g_pIO->Log_str);
	//	  
	//	return MC_Pumping_ERROR;
	//}

	ret = g_pIO->Close_TRGateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Pumping] TR Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	ret = g_pIO->Close_MC_TMP_GateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Pumping] MC TMP Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	ret = g_pIO->Close_MC_TMP_ForelineValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Pumping] MC TMP Foreline Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  
	ret = g_pIO->Close_MC_FastRoughValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Pumping] MC Fast Rough Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}

	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  
	ret = g_pIO->Close_MC_SlowRoughValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Pumping] MC Slow Rough Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}

	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		//if (g_pIO->Is_LaserSource_Open() == VALVE_CLOSED && g_pIO->Is_TRGateValve_Open() == VALVE_CLOSED && g_pIO->Is_MC_TMP_GateValve_Open() == VALVE_CLOSED
		if (g_pIO->Is_TRGateValve_Open() == VALVE_CLOSED 
			&& g_pIO->Is_MC_TMP_GateValve_Open() == VALVE_CLOSED
			&& g_pIO->Is_MC_TMP_ForelineValve_Open() == VALVE_CLOSED 
			&& g_pIO->Is_MC_FastRoughValve_Open() == VALVE_CLOSED 
			&& g_pIO->Is_MC_SlowRoughValve_Open() == VALVE_CLOSED) 
			break;
	
		//&& g_pIO->Is_MC_FastRoughValve_Open() == VALVE_CLOSED && g_pIO->Is_MC_SlowRoughValve_Open() == VALVE_CLOSED) break;
	}

	//str = " MC Pumping시 Gate Close 확인";
	//SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//g_pLog->Display(0, str);
	  

	//	if (g_pIO->Is_LaserSource_Open() != VALVE_CLOSED)
	//	{
	//		m_McVacuumState = MC_Pumping_ERROR;
	//		str = " MC Pumping시 Laser Source Gate Valve Close 에러 발생";
	//		g_pLog->Display(0, str);
	//		g_pLog->Display(0, g_pIO->Log_str);
	//		  
	//		return MC_Pumping_ERROR;
	//	}
	if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC Pumping시 TR Gate Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_TRGATE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = "[MC Pumping] MC Pumping시 MC TMP Gate Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCTMPGATE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC Pumping시 MC TMP Foreline Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCFORELINE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC Pumping시 MC Fast Rough Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCFASTROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC Pumping시 MC Slow Rough Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCSLOWROUGH_CLOSE_CHECK_FAIL;
	}
	
	return ret;
}

int CVacuumProcess::MC_SlowRough()
{
	int ret = SEQUENCE_DONE;

	char* str;

	CString MC_Pressure;
	MC_Pressure.Empty();

	m_nMc_slow_rough_time_cnt = 0;

	str = " [MC Pumping] MC_SlowRough(): START";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	//1. 상태확인:LLC Gate Valve Close,TR Gate Valve Close,LLC TMP Gate Valve Close,LLC TMP Foreline Valve Close,LLC Fast & Slow Rough Valve Close,Fast & Slow1,2 Vent Valve Close,LLC Lid Close
	//2. LLC 진공도가 환경설정에 기록된 Slow Rough->Fast Rough 전환 진공도보다 낮은지 확인
	//3. 높다면 LLC Slow Rough Valve Open
	//4. LLC 진공도가 환경설정에 기록된 Slow Rough->Fast Rough 전환 진공도에 도달했는지 확인
	//5. LLC Slow Rough Valve Close 확인 후 상태 변경(Fast Rough 완료시 Close해야 되는지 확인 필요)

	str = " [MC Pumping] MC_SlowRough(): MC TMP Foreline Valve Close 명령 !";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	ret = g_pIO->Close_MC_TMP_ForelineValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Pumping] MC_SlowRough(): MC TMP Foreline Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}

	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  
	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_MC_TMP_ForelineValve_Open() == VALVE_CLOSED)	
			break;
	}

	if (g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_SlowRough(): MC Pumping시 MC TMP Foreline Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCFORELINE_CLOSE_CHECK_FAIL;
	}

	str = " [MC Pumping] MC_SlowRough(): MC TMP Foreline Valve Close 완료 !";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	//  
	// SOURCE GATE CHECK 
	// setting 전이므로 setting 후 적용 예정.
	//
	//if (g_pIO->Is_LaserSource_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Pumping_ERROR;
	//	str = " MC_SlowRough(): MC Pumping시 Laser Source Gate Valve Close 에러 발생";
	//	g_pLog->Display(0, str);
	//	g_pLog->Display(0, g_pIO->Log_str);
	//	  
	//	return MC_Pumping_ERROR;
	//}


	if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_SlowRough(): MC Pumping시 TR Gate Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_TRGATE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_SlowRough(): MC Pumping시 MC TMP Gate Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCTMPGATE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_SlowRough(): MC Pumping시 MC TMP Foreline Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCFORELINE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_SlowRough(): MC Pumping시 MC Fast Rough Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCFASTROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_SlowRough(): MC Pumping시 MC Slow Rough Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCSLOWROUGH_CLOSE_CHECK_FAIL;
	}


	/////////////////////////////////////////////
	// mc 진공도 50 torr 이상이면 Slow Rough 진행.
	// 50 이하면 Slow Rough Pass
	/////////////////////////////////////////////
	if (g_pGauge_IO->GetMcVacuumRate() >= g_pConfig->m_dPressure_ChangeToFast_MC_Rough) // 50torr
	{
		str = " [MC Pumping] MC_SlowRough(): 50 torr 진공 이상 확인 완료 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		str = " [MC Pumping] Start Slow Roughing";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		str = " [MC Pumping] MC_SlowRough(): MC Slow Rough Valve Open 명령 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		ret = g_pIO->Open_MC_SlowRoughValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = " [MC Pumping] MC_SlowRough(): MC Slow Rough Valve Open 명령 에러 발생 !";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
			return ret;
		}
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, g_pIO->Log_str);
		  

		str = " [MC Pumping] MC_SlowRough(): MC Slow Rough Valve Open 명령 완료!";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		m_start_time_mc = clock();
		m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
		{
			if (g_pIO->Is_MC_SlowRoughValve_Open() == VALVE_OPENED)
				break;
		}

		if (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_OPENED)
		{
			str = " [MC Pumping] MC_SlowRough(): MC Pumping시 MC Slow Rough Valve Open 에러 발생";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			return INTERLOCK_MCSLOWROUGH_OPEN_CHECK_FAIL;
		}

		str = " [MC Pumping] MC_SlowRough(): MC Slow Rough Valve Open 확인 완료!";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		///////////////////////////////////
		////50 Torr 까지 Pumping 진행
		///////////////////////////////////

		str = " [MC Pumping] MC_SlowRough(): Slow Rough Valve Open 후 50 Torr 까지 진행 (600초) ";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		m_start_time_mc = clock();
		m_finish_time_mc = g_pConfig->m_nTimeout_sec_MCSlowRough; // 600초(10분) 동안 Rough open 후  pumping 시작 ( 50 torr 까지 )

		while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
		{
			m_nMc_slow_rough_time_cnt = ((clock() - m_start_time_mc) / CLOCKS_PER_SEC);
			if (g_pGauge_IO->GetMcVacuumRate() < g_pConfig->m_dPressure_ChangeToFast_MC_Rough) //50 torr
			{
				str = " [MC Pumping] MC_SlowRough(): Slow Rough Break 시점의 진공값 확인 !";
				SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				MC_Pressure.Format("%f", g_pGauge_IO->GetMcVacuumRate());
				SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(MC_Pressure)));
			//	mc_slow_rough_time_cnt = 0;
				break;
			}
		}
		


		if (g_pGauge_IO->GetMcVacuumRate() >= g_pConfig->m_dPressure_ChangeToFast_MC_Rough)
		{
			str = " [MC Pumping] MC_SlowRough(): MC Slow Roughing Timeout 발생 !";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			return -999;
		}
	}
	str = " [MC Pumping] MC_SlowRough():  MC Slow Rough Valve Close 명령 !";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	ret = g_pIO->Close_MC_SlowRoughValve();
	if (ret != 0)
	{
		str = " [MC Pumping] MC_SlowRough(): MC Slow Rough Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_MC_SlowRoughValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_SlowRough(): MC Pumping시 MC Slow Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCSLOWROUGH_CLOSE_CHECK_FAIL;
	}

	str = " [MC Pumping] MC_SlowRough():  MC Slow Rough Valve Close 확인 완료!";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	str = " [MC Pumping] MC_SlowRough():  MC Slow Rough 완료!";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	
	return ret;
}

int CVacuumProcess::MC_FastRough()
{
	int ret = SEQUENCE_DONE;

	char* str;
	//CString str;
	CString MC_Pressure;
	MC_Pressure.Empty();

	m_nMc_fast_rough_time_cnt = 0;

	str = " [MC Pumping] MC_FastRough(): START";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	//1. 상태확인:Laser Source Gate Valve Close,TR Gate Valve Close, MC TMP Gate Valve Close, MC TMP Foreline Valve Close, MC Fast & Slow Rough Valve Close
	//2. MC 진공도가 환경설정에 기록된 Fast Rough->TMP Rough 전환 진공도보다 낮은지 확인
	//3. 높다면 MC Fast Rough Valve Open
	//4. MC 진공도가 환경설정에 기록된 Fast Rough->TMP Rough 전환 진공도에 도달했는지 확인
	//5. MC Fast Rough Valve Close 확인 후 상태 변경


	//  
	// SOURCE GATE CHECK 
	// setting 전이므로 setting 후 적용 예정.
	//
	//if (g_pIO->Is_LaserSource_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Pumping_ERROR;
	//	str = " MC_FastRough(): MC Pumping시 Laser Source Gate Valve Close 에러 발생";
	//	g_pLog->Display(0, str);
	//	g_pLog->Display(0, g_pIO->Log_str);
	//	  
	//	return MC_Pumping_ERROR;
	//}

	if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_FastRough(): MC Pumping시 TR Gate Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_TRGATE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_FastRough(): MC Pumping시 MC TMP Gate Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCTMPGATE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSED)
	{
		str = "[MC Pumping]  MC_FastRough(): MC Pumping시 MC TMP Foreline Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCFORELINE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_FastRough(): MC Pumping시 MC Fast Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCFASTROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_FastRough(): MC Pumping시 MC Slow Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCSLOWROUGH_CLOSE_CHECK_FAIL;
	}


	//if (g_pGauge_IO->GetMcVacuumRate() >= 0.03) //0.02 torr //0.1 torr

	/////////////////////////////////////////////////////////////////
	// 0.035 (3.5x10^-2) ~ 50 Torr 사이에서 Fast Rough 진행 확인
	/////////////////////////////////////////////////////////////////
	if ((g_pGauge_IO->GetMcVacuumRate() >= g_pConfig->m_dPressure_ChangeToMCTMP_Rough) && (g_pGauge_IO->GetMcVacuumRate() <= g_pConfig->m_dPressure_ChangeToFast_MC_Rough)) //0.035 torr ~ 50 torr
	{
		MC_Pressure.Empty();
		MC_Pressure.Format("%f", g_pGauge_IO->GetMcVacuumRate());

		CString MC_Config;
		MC_Config.Format("%f", g_pConfig->m_dPressure_ChangeToMCTMP_Rough);

		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(MC_Pressure)));
		g_pLog->Display(0, MC_Pressure);

		CString mc_str;
		mc_str = MC_Pressure + _T(" :: 현재 진공 값 ") + MC_Config + _T(" 보다 이상 조건 확인 ! ");
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(mc_str)));
		g_pLog->Display(0, mc_str);

		//str = " MC_FastRough(): 0.035 Torr 이상 진공 값 조건 확인 !";
		//SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		//g_pLog->Display(0, str);
		  

		str = " [MC Pumping] MC_FastRough():  MC Pumping시 Fast Rough Valve Open !!";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		ret = g_pIO->Open_MC_FastRoughValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = " [MC Pumping] MC_FastRough(): MC Fast Rough Valve Open 명령 에러 발생 !";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
			return ret;
		}
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, g_pIO->Log_str);
		  

		m_start_time_mc = clock();
		m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
		{
			if (g_pIO->Is_MC_FastRoughValve_Open() == VALVE_OPENED)
				break;
		}

		if (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_OPENED)
		{
			str = "[MC Pumping] MC_FastRough(): MC Pumping시 MC Fast Rough Valve Open 에러 발생";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			return INTERLOCK_MCFASTROUGH_OPEN_CHECK_FAIL;
		}

		str = " [MC Pumping] MC_FastRough(): MC Fast Rough Valve Open 완료 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		str = "[MC Pumping] Start Fast Roughing";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  


		str = " [MC Pumping] MC_FastRough() : MC Fast Rough Valve Open 후 0.035 (3.5 x 10^-2) torr 까지 진행 (600초) ";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		m_start_time_mc = clock();
		m_finish_time_mc = g_pConfig->m_nTimeout_sec_MCFastRough; // 600초(10분) 동안 Fast Rough open 후  pumping 시작(0.035 torr 까지)
		while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
		{
			m_nMc_fast_rough_time_cnt = ((clock() - m_start_time_mc) / CLOCKS_PER_SEC);
			if (g_pGauge_IO->GetMcVacuumRate() < g_pConfig->m_dPressure_ChangeToMCTMP_Rough) //0.035
			{
				MC_Pressure.Empty();
				MC_Pressure.Format("%f", g_pGauge_IO->GetMcVacuumRate());
				str = " [MC Pumping] MC_FastRough(): MC Fast Roughing Break 시점 진공 값 확인 !";
				SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(MC_Pressure)));
				break;
			}
		}

	

		if (g_pGauge_IO->GetMcVacuumRate() >= g_pConfig->m_dPressure_ChangeToMCTMP_Rough)
		{
			str = " [MC Pumping] MC_FastRough(): MC Fast Roughing Timeout 발생 !";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			return -999;
		}
	}

	str = " [MC Pumping] End Fast Roughing";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	str = " [MC Pumping] MC_FastRough(): MC Fast Roughing Valve Close !";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	ret = g_pIO->Close_MC_FastRoughValve();
	if (ret != 0)
	{
		str = " [MC Pumping] MC_FastRough(): MC Fast Rough Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_MC_FastRoughValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_FastRough(): MC Pumping시 MC Fast Rough Valve Close 에러 발생 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCFASTROUGH_CLOSE_CHECK_FAIL;
	}

	str = " [MC Pumping] MC_FastRough(): MC Fast Roughing Valve Close 확인 완료!";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	str = " [MC Pumping] MC_FastRough(): END";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	
	return ret;
}

int CVacuumProcess::MC_TMPRough()
{
	int ret = SEQUENCE_DONE;

	char* str;
	//CString str;
	CString MC_Pressure;
	MC_Pressure.Empty();

	str = " [MC Pumping] MC_TMPRough(): START";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	m_nMc_tmp_rough_time_cnt = 0;

	//1. 상태확인:LLC Gate Valve Close,TR Gate Valve Close,LLC TMP Gate Valve Close,LLC TMP Foreline Valve Close,LLC Fast & Slow Rough Valve Close,Fast & Slow1,2 Vent Valve Close,LLC Lid Close
	//2. LLC 진공도가 환경설정에 기록된 Fast Rough->TMP Rough 전환 진공도보다 낮은지 확인
	//3. LLC Foreline Valve Open
	//4. LLC TMP Gate Valve Open
	//5. LLC 진공도가 환경설정에 기록된 LLC Pumping End 진공도에 도달여부 확인 후 상태 변경

	//if (g_pIO->Is_LaserSource_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Pumping_ERROR;
	//	str = " MC_TMPRough(): MC Pumping시 Laser Source Gate Valve Close 에러 발생";
	//	g_pLog->Display(0, str);
	//	g_pLog->Display(0, g_pIO->Log_str);
	//	  
	//	return MC_Pumping_ERROR;
	//}
	if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_TMPRough(): MC Pumping시 TR Gate Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_TRGATE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_TMPRough(): MC Pumping시 MC TMP Gate Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCTMPGATE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_TMPRough(): MC Pumping시 MC TMP Foreline Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCFORELINE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_TMPRough(): MC Pumping시 MC Fast Rough Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCFASTROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Pumping] MC_TMPRough(): MC Pumping시 MC Slow Rough Valve Close 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCSLOWROUGH_CLOSE_CHECK_FAIL;
	}

	str = " [MC Pumping] MC_TMPRough(): MC Pumping시 TMP Pumping 가능 압력 확인!";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	/////////////////////////////////////////////////////////
	// 0.035 (3.5x10^-2) torr 이하 이면 Tmp rough 진행
	// 이상일 경우 pumping Error
	/////////////////////////////////////////////////////////

	if (g_pGauge_IO->GetMcVacuumRate() >= g_pConfig->m_dPressure_ChangeToMCTMP_Rough) //0.035 torr
	{

		MC_Pressure.Empty(); 
		MC_Pressure.Format("%f", g_pGauge_IO->GetMcVacuumRate());

		CString MC_Config;
		MC_Config.Format("%f", g_pConfig->m_dPressure_ChangeToMCTMP_Rough);

		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(MC_Pressure)));
		g_pLog->Display(0, MC_Pressure);

		CString mc_str;
		mc_str = MC_Pressure + _T(" :: 현재 진공 값 ") + MC_Config + _T(" 보다 이상 ! ");
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(mc_str)));
		g_pLog->Display(0, mc_str);

		str = " [MC Pumping] MC_TMPRough(): MC Pumping시 TMP Pumping 가능 압력이 아님 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_NOT_PUMPABLE_RANGE_CHECK_FAIL;
	}
	else
	{
		MC_Pressure.Empty();
		MC_Pressure.Format("%f", g_pGauge_IO->GetMcVacuumRate());

		CString MC_Config;
		MC_Config.Format("%f", g_pConfig->m_dPressure_ChangeToMCTMP_Rough);

		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(MC_Pressure)));
		g_pLog->Display(0, MC_Pressure);

		CString mc_str;
		mc_str = MC_Pressure + _T(" :: 현재 진공 값 ") + MC_Config + _T(" 보다 이하 ! ");
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(mc_str)));
		g_pLog->Display(0, mc_str);

		mc_str = " [MC Pumping] MC_TMPRough(): MC Pumping시 TMP Pumping 가능 압력 확인!";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, mc_str);
	}

	str = " [MC Pumping] MC_TMPRough(): MC Pumping시 MC Foreline Open !!";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	ret = g_pIO->Open_MC_TMP_ForelineValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Pumping] MC_TMPRough(): MC TMP Foreline Valve Open 명령 에러 발생 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_MC_TMP_ForelineValve_Open() == VALVE_OPENED)
			break;
	}

	if (g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " [MC Pumping] MC_TMPRough(): MC Pumping시 MC TMP Foreline Valve Open 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCFORELINE_OPEN_CHECK_FAIL;
	}

	str = " [MC Pumping] MC_TMPRough(): MC Pumping시 MC Forline Open 확인!!";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	str = " [MC Pumping] MC_TMPRough(): MC Pumping시 MC Tmp gate Open !!";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	ret = g_pIO->Open_MC_TMP_GateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Pumping] MC_TMPRough(): MC TMP Gate Valve Open 명령 에러 발생 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_MC_TMP_GateValve_Open() == VALVE_OPENED)
			break;
	}

	if (g_pIO->Is_MC_TMP_GateValve_Open() != VALVE_OPENED)
	{
		str = " [MC Pumping] MC_TMPRough(): MC Pumping시 MC TMP Gate Valve Open 에러 발생";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCTMPGATE_OPEN_CHECK_FAIL;
	}

	str = " [MC Pumping] MC_TMPRough(): MC Pumping시 MC Tmp gate Open 확인 !!";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	str = " [MC Pumping] Start MC TMP Roughing";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	//
	// 3600초 동안 0.000009 (9x10^-6) torr 진행
	//
	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_MCTmpEnd; //60분 3600초 동안 진행
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		m_nMc_tmp_rough_time_cnt = ((clock() - m_start_time_mc) / CLOCKS_PER_SEC);
		if (g_pGauge_IO->GetMcVacuumRate() < g_pConfig->m_dPressure_Rough_End) //0.000009; 9*10^-6
		{
			MC_Pressure.Empty();
			MC_Pressure.Format("%f", g_pGauge_IO->GetMcVacuumRate());
			str = " [MC Pumping] MC_TMPRough(): MC TMP Roughing Break 시점 진공 값 확인 !";
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(MC_Pressure)));
			break;
		}
	}
	


	if (g_pGauge_IO->GetMcVacuumRate() >= g_pConfig->m_dPressure_Rough_End)
	{
		str = " [MC Pumping] MC_TMPRough(): MC TMP Roughing Timeout 발생 !";
		SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		
		g_pLog->Display(0, str);
		return INTERLOCK_TMPROUGHING_TIMEOUT_FAIL;
	}

	SetEvent(g_pIO->m_TRGate_Vauccum_Value_On);

	str = " MC_TMPRough(): END";
	SaveLogFile("MC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	
	return ret;
}

int CVacuumProcess::MC_Pumping_Complete()
{
	int ret = 0;
	char* str;

	CString slow_pump_time;
	CString fast_pump_time;
	CString tmp_pump_time;

	slow_pump_time.Empty();
	fast_pump_time.Empty();
	tmp_pump_time.Empty();

	//1. TR Gate Valve Open

	//if (g_pIO->Open_TRGateValve() != TRUE)
	//{
	//	//Pumping은 완료되었으므로 Error 처리는 안해도 됨.
	//	str = " LLC_Pumping_Complete(): TR Gate Valve Open 명령 에러 발생 !";
	//}
	
	slow_pump_time.Format("%d", m_nMc_slow_rough_time_cnt);
	fast_pump_time.Format("%d", m_nMc_fast_rough_time_cnt);
	tmp_pump_time.Format("%d", m_nMc_tmp_rough_time_cnt);

	SaveLogFile("SREM_Sequence_Time_Report", _T((LPSTR)(LPCTSTR)(" MC Pumping Sequence Time :: Slow Rough Pumping [ " + slow_pump_time + " ] , Fast Rough Pumping [ " + fast_pump_time + "  ] , Tmp Pumping [ " + tmp_pump_time + " ] ")));

	g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);


	m_McVacuumState = MC_State_IDLE;
	
	return ret;
}

int CVacuumProcess::MC_Venting_PreWork()
{
	int ret = SEQUENCE_DONE;

	char* str;

	//1. MC Gauage 정상 가동 확인
	//2. TR Gate Valve Close
	//4. MC TMP Gate Valve Close 
	//5. MC TMP Foreline Valve Close

	//10. LLC Fast Vent Valve Close
	//11. LLC Lid Close 상태 확인 후 상태 변경

	str = " [MC Venting] MC_Venting_PreWork() : VALVE OPEN/CLOSE CONTORL START";

	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	switch (g_pIO->Is_LLC_DryPump_Status()) {
	case DRYPUMP_WARNING:
		str = " [MC Venting] LLC Dry Pump Warning 발생 ! ";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return LLC_DRY_PUMP_ALARM_STATE;
	case DRYPUMP_ERROR:
		str = "  [MC Venting] LLC Dry Pump Error 발생 ! ";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return LLC_DRY_PUMP_ERROR_STATE;
	case DRYPUMP_STOP:
		str = " [MC Venting] LLC Dry Pump Stop 상태 ! ";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return LLC_DRY_PUMP_STOP_STATE;
	default:
		str = " [MC Venting] LLC DryPump 정상 확인 완료";
		break;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	switch (g_pIO->Is_MC_DryPump_Status()) {
	case DRYPUMP_WARNING:
		str = " [MC Venting] MC Dry Pump Warning 발생 ! ";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return MC_DRY_PUMP_ALARM_STATE;
	case DRYPUMP_ERROR:
		str = " [MC Venting] MC Dry Pump Error 발생 ! ";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return MC_DRY_PUMP_ERROR_STATE;
	case DRYPUMP_STOP:
		str = " [MC Venting] MC Dry Pump Stop 상태 ! ";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return MC_DRY_PUMP_STOP_STATE;
	default:
		str = " [MC Venting] MC DryPump 정상 확인 완료";
		break;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	switch (g_pGauge_IO->GetGaugeState()) {
	case g_pGauge_IO->NotConnected:
		str = "[MC Venting] MC_Venting_PreWork(): Guage Offline ! ";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return VACCUM_SENSOR_OFFLINE_STATE;
	case g_pGauge_IO->Error:
		str = " [MC Venting] MC_Venting_PreWork(): Guage Error 발생 ! ";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return VACCUM_SENSOR_ERROR_STATE;
	default:
		break;
	}


	ret = g_pIO->Close_LLCGateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = "[MC Venting] MC_Venting_PreWork(): LLC Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;

	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_LLCGateValve_Open() == VALVE_CLOSED) 
			break;
	}

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 LLC Gate Valve Close Time Out 에러 발생";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}

	////////////////////////
	// TR GATE CLOSE 부분
	////////////////////////

	// MC Venting  시, TR GATE 열고 Venting 진행. ( LLC 과 함께 Venting 진행 )
	//
	//if (g_pIO->Close_TRGateValve() != TRUE)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_Venting_PreWork(): TR Gate Valve Close 명령 에러 발생 !";
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	//	g_pLog->Display(0, str);
	//	g_pLog->Display(0, g_pIO->Log_str);
	//	  
	//	return MC_Venting_ERROR;
	//}
	//SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	//g_pLog->Display(0, g_pIO->Log_str);
	//  
	//
	//m_start_time = clock();
	//m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	//while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	//{
	//	if (g_pIO->Is_TRGateValve_Open() == VALVE_CLOSED) break;
	//
	//}
	//
	//if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_Venting_PreWork(): MC Venting시 TR Gate Valve Close Time Out 에러 발생";
	//
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	g_pLog->Display(0, str);
	//	  
	//	return MC_Venting_ERROR;
	//}


	ret = g_pIO->Close_MC_TMP_GateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC TMP Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_MC_TMP_GateValve_Open() == VALVE_CLOSED) 
			break;
	}

	if (g_pIO->Is_MC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 MC TMP Gate Valve Close Time Out 에러 발생";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCTMPGATE_CLOSE_CHECK_FAIL;
	}


	ret = g_pIO->Close_LLC_TMP_GateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): LLC TMP Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  
		
	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_LLC_TMP_GateValve_Open() == VALVE_CLOSED) 
			break;
	}

	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 LLC TMP Gate Valve Close Time Out 에러 발생";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL;
	}

	///////////////////////////////
	//FORELINE V/V OPEN
	///////////////////////////////
	ret = g_pIO->Open_LLC_TMP_ForelineValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): LLC TMP Foreline Valve Open 명령 에러 발생 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_OPENED) 
			break;
	}

	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 LLC Foreline Gate Valve Open Time Out 에러 발생";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_LLCFORELINE_OPEN_CHECK_FAIL;
	}

	ret = g_pIO->Open_MC_TMP_ForelineValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC TMP Foreline Valve Open 명령 에러 발생 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_MC_TMP_ForelineValve_Open() == VALVE_OPENED) 
			break;
	}

	if (g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 MC Foreline Gate Valve Open Time Out 에러 발생";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCFORELINE_OPEN_CHECK_FAIL;
	}


	ret = g_pIO->Close_LLC_FastRoughValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Venting]  MC_Venting_PreWork(): LLC Fast Rough Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_LLC_FastRoughValve_Open() == VALVE_CLOSED) 
			break;
	}

	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = "[MC Venting] MC_Venting_PreWork(): MC Venting시 LLC Fast Rough Valve Open Time Out 에러 발생";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
	}



	ret = g_pIO->Close_MC_FastRoughValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Fast Rough Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_MC_FastRoughValve_Open() == VALVE_CLOSED) 
			break;
	}

	if (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = "[MC Venting] MC_Venting_PreWork(): MC Venting시 MC Fast Rough Valve Open Time Out 에러 발생";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_MCFASTROUGH_CLOSE_CHECK_FAIL;
	}
	

	//if (g_pIO->Close_MC_Source_Gate_Valve() != TRUE)
	//{
	//	m_LlcVacuumState = Venting_ERROR;
	//	str = " LLC_Venting_PreWork(): MC Source Gate Valve Close 명령 에러 발생 !";
	//	g_pLog->Display(0, str);
	//	g_pLog->Display(0, g_pIO->Log_str);
	//	  
	//	return Venting_ERROR;
	//}

	ret = g_pIO->Close_SlowVentValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Venting] MC_Venting_PreWork():Slow Vent Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_SlowVentValve_Open() == VALVE_CLOSED) 
			break;
	}

	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 Slow Vent Valve Close Time Out 에러 발생";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
	}

	ret = g_pIO->Close_FastVentValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): Fast Vent Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_FastVentValve_Open() == VALVE_CLOSED) 
			break;
	}

	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " MC_Venting_PreWork(): MC Venting시 Fast Vent Valve Close Time Out 에러 발생";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
	}

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;

	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_LLCGateValve_Open() == VALVE_CLOSED 
			&& g_pIO->Is_MC_TMP_GateValve_Open() == VALVE_CLOSED 
			&& g_pIO->Is_LLC_TMP_GateValve_Open() == VALVE_CLOSED
			&& g_pIO->Is_MC_FastRoughValve_Open() == VALVE_CLOSED 
			&& g_pIO->Is_LLC_FastRoughValve_Open() == VALVE_CLOSED 
			&& g_pIO->Is_MC_TMP_ForelineValve_Open() == VALVE_OPENED 
			&& g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_OPENED
			&& g_pIO->Is_SlowVentValve_Open() == VALVE_CLOSED 
			&& g_pIO->Is_FastVentValve_Open() == VALVE_CLOSED)	
			break;


		//if (g_pIO->Is_LLCGateValve_Open() == VALVE_CLOSED && g_pIO->Is_MC_TMP_GateValve_Open() == VALVE_CLOSED && g_pIO->Is_LLC_TMP_GateValve_Open() == VALVE_CLOSED
		//	&& g_pIO->Is_MC_FastRoughValve_Open() == VALVE_CLOSED && g_pIO->Is_LLC_FastRoughValve_Open() == VALVE_CLOSED && g_pIO->Is_MC_TMP_ForelineValve_Open() == VALVE_OPENED && g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_OPENED
		//	&& g_pIO->Is_SlowVentValve_Open() == VALVE_CLOSED && g_pIO->Is_FastVentValve_Open() == VALVE_CLOSED)	break;	
	}

	str = "MC_Venting_PreWork() : VALVE OPEN/CLOSE CHECK START";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 LLC Gate Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}

	// MC VENTING 시 TR 열고 진행 해야 함. 
	//
	//if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_Venting_PreWork(): MC Venting시 TR Gate Valve Close 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	  
	//	return MC_Venting_ERROR;
	//}

	if (g_pIO->Is_MC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 MC TMP Gate Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCTMPGATE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 LLC TMP Gate Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL;
	}

	if(g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 MC TMP Foreline Valve Open 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCFORELINE_OPEN_CHECK_FAIL;
	}

	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 LLC TMP Foreline Valve Open 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_LLCFORELINE_OPEN_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 MC Fast Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCFASTROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = "[MC Venting] MC_Venting_PreWork(): MC Venting시 LLC Fast Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 Slow Vent Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_Venting_PreWork(): MC Venting시 Fast Vent Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
	}

	str = "[MC Venting] MC_Venting_PreWork() : VALVE OPEN/CLOSE CHECK END";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	return ret;
}

int CVacuumProcess::MC_SlowVent()
{
	int ret = 0;

	char* str;
	CString MC_Pressure, LLC_Pressure;

	m_nMc_standby_vent_time_cnt = 0;

	//1. 상태확인
	//		LLC Gate Valve Close, TR Gate Valve Open,
	//		MC, LLC TMP Gate Valve Close, MC, LLC TMP Foreline Valve Open,
	//		MC, LLC Fast & Slow Rough Valve Close, Fast & Slow Vent Valve Close,
	//2. LLC, MC 진공도가 Slow Vent 시작 조건이 될때 까지 대기 Vent.
	//3. MC, LLC 진공도가 환경설정에 기록된 Slow Vent 전환 진공도보다 높은지 확인
	//4. 낮다면 Slow Vent Valve #1 Open
	//5. MC, LLC 진공도가 환경설정에 기록된 Fast Vent 전환 진공도에 도달했는지 확인
	//6. Slow Vent Valve #1 Close 후 상태 변경



	// KJH
	// 진공 값 log 에서 확인하여 진행 상황 보기 위한 TEST 값 모니터.
	// (삭제 예정)
	////////////////////////////////////////////////////////////////////////////

	CString Gauge;
	CString Gauge_str;
	double m_mc_Pressure = 0.0;
	double m_llc_Presure = 0.0;
	double m_mc_llc_presure = 0.0;
	double m_mc_llc_presure2 = 0.0;

	m_mc_Pressure = (g_pGauge_IO->GetMcVacuumRate());
	m_llc_Presure = (g_pGauge_IO->GetLlcVacuumRate());
	m_mc_llc_presure = (m_mc_Pressure - m_llc_Presure);
	m_mc_llc_presure2 = (m_llc_Presure - m_mc_Pressure);
	
	str = "[MC Venting] MC_SlowVent(): TR Gate Valve Open 전 MC , LLC Gauge 값 확인 !";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	Gauge.Format("%f", g_pGauge_IO->GetMcVacuumRate());
	Gauge_str = " [MC Venting] MC_SlowVent(): MC 진공 값 ====" + Gauge;
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(Gauge_str)));
	g_pLog->Display(0, Gauge_str);

	Gauge.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
	Gauge_str = " [MC Venting] MC_SlowVent(): LLC 진공 값 ====" + Gauge;
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(Gauge_str)));
	g_pLog->Display(0, Gauge_str);

	Gauge.Format("%f", m_mc_llc_presure);
	Gauge_str = " [MC Venting] MC_SlowVent(): MC - LLC 진공 차이 값 ====" + Gauge;
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(Gauge_str)));
	g_pLog->Display(0, Gauge_str);

	Gauge.Format("%f", m_mc_llc_presure2);
	Gauge_str = " [MC Venting] MC_SlowVent(): LLC - MC 진공 차이 값 ====" + Gauge;
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(Gauge_str)));
	g_pLog->Display(0, Gauge_str);

	//////////////////////////////////////////////////////////////////////////////////


	str = " [MC Venting]  MC_SlowVent(): LLC Gate Valve Close !";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	ret = g_pIO->Close_LLCGateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = "  [MC Venting] MC_SlowVent(): LLC Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);


	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;

	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_LLCGateValve_Open() == VALVE_CLOSED) 
			break;
	}

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = "  [MC Venting] MC_SlowVent(): MC Venting시 LLC Gate Valve Close Time Out 에러 발생";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}

	str = " [MC Venting]  MC_SlowVent(): LLC Gate Valve Close 확인 완료!";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	str = " [MC Venting]  MC_SlowVent(): LLC tmp Gate Valve Close !";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	ret = g_pIO->Close_LLC_TMP_GateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = "  [MC Venting] MC_SlowVent(): LLC TMP Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;

	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_LLC_TMP_GateValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 LLC TMP Gate Valve Close Time Out 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL;
	}
	str = "[MC Venting] MC_SlowVent(): LLC TMP Gate Valve Close 확인 완료!";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	str = " [MC Venting] MC_SlowVent(): MC tmp Gate Valve Close !";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	ret = g_pIO->Close_MC_TMP_GateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " MC_SlowVent(): MC TMP Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return ret;
	}
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_MC_TMP_GateValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_MC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 MC TMP Gate Valve Close Time Out 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		return INTERLOCK_MCTMPGATE_CLOSE_CHECK_FAIL;
	}
	str = " [MC Venting] MC_SlowVent(): MC TMP Gate Valve Close 확인 완료!";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);


	str = "  [MC Venting] MC_SlowVent(): MC / LLC Tmp Gate Close 확인 완료 !";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	
	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 LLC Gate Valve Close 에러 발생";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}
	//if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " [MC Venting] MC_SlowVent(): MC Venting시 TR Gate Valve Open 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	  
	//	return MC_Venting_ERROR;
	//}
	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 LLC TMP Gate Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 MC TMP Gate Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCTMPGATE_CLOSE_CHECK_FAIL;
	}

	//FORELINE OPEN VENTING.
	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 LLC TMP Foreline Valve Open 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_LLCFORELINE_OPEN_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 MC TMP Foreline Valve Open 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCFORELINE_OPEN_CHECK_FAIL;
	}

	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 LLC Fast Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		  
		return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 MC Fast Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCFASTROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 LLC Slow Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_LLCSLOWROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 MC Slow Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCSLOWROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 Fast Vent Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 Slow Vent Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
	}

	
	//if (((0 < m_mc_llc_presure2) && (m_mc_llc_presure2 < 10)) || ((0 > m_mc_llc_presure2) && (m_mc_llc_presure2 > -10)))

	//////////////////////////////////////////////
	//
	// MC Venting 전 TR Gate 열고 해야함.
	// LLC 과 MC 진공 값 차이 확인 후 TR GATE OPEN.
	//
	//////////////////////////////////////////////

	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		str = " [MC Venting] MC_SlowVent(): TR GATE 가 닫혀 있습니다 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		if (TR_Gate_Open_Check())
		{
			str = "[MC Venting]  MC_SlowVent(): TR Gate Valve Open 전 진공 값 확인 완료 (MC & LLC) !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);

			str = "[MC Venting]  MC_SlowVent(): TR Gate Valve Open 명령 !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);

			ret = g_pIO->Open_TRGateValve();
			if (ret != OPERATION_COMPLETED)
			{
				str = " MC_SlowVent(): TR Gate Valve Open 명령 에러 발생 !";
				SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
				g_pLog->Display(0, str);
				g_pLog->Display(0, g_pIO->Log_str);
				return ret;
			}
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, g_pIO->Log_str);


			m_start_time_mc = clock();
			m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
			while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
			{
				if (g_pIO->Is_TRGateValve_Open() == VALVE_OPENED) 
					break;
			}

			if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
			{
				str = " [MC Venting] MC_SlowVent(): MC Venting시 TR Gate Valve Open 에러 발생";
				g_pLog->Display(0, str);
				SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				return INTERLOCK_TRGATE_OPEN_CHECK_FAIL;
			}

			str = "[MC Venting] MC_SlowVent(): TR Gate Valve Open 확인 완료!";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
		}
		else
		{
			str = "[MC Venting]  MC_SlowVent(): TR Gate Valve Open 불가! TR / LLC 진공 값 차이 발생 !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			return INTERLOCK_VACUUM_DIFFERENCE_BETWEEN_MC_AND_LLC_FAIL;
		}
	}
	else
	{
		str = " [MC Venting] MC_SlowVent(): TR GATE 가 열려 있으므로, Slow Vent 를 시작 합니다!";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
	}

	str = " [MC Venting] MC_SlowVent(): START !";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	str = " [MC Venting] MC_SlowVent(): MC / LLC TMP Gate Close 후 0.003 torr 까지 대기 Venting (1800초 : 30분)  !";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	//////////////////////////////////////////////
	// 대기 벤팅 시간을 줄이기 위함.
	// Fast Vent Inlet Open 후 바로 Close
	/////////////////////////////////////////////
	ret = g_pIO->Open_FastVent_Inlet_Valve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 Fast Vent Inlet Open 명령 에러 발생";
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		return ret;
	}
	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;

	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_FastVent_Inlet_Valve_Open() == VALVE_OPENED)
			break;
	}

	if (g_pIO->Is_FastVent_Inlet_Valve_Open() != VALVE_OPENED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 Fast Vent Inlet Valve Open Time Out 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_FASTVENT_OPEN_CHECK_FAIL;
	}
	str = " [MC Venting] MC_SlowVent(): Fast Vent Inlet Valve Open 확인 완료!";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	WaitSec(1);

	ret = g_pIO->Close_FastVent_Inlet_Valve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 Fast Vent Inlet Close 명령 에러 발생";
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		return ret;
	}
	m_start_time_mc = clock();
	m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;

	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
	{
		if (g_pIO->Is_FastVent_Inlet_Valve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_FastVent_Inlet_Valve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_SlowVent(): MC Venting시 Fast Vent Inlet Valve Close Time Out 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
	}

	str = " [MC Venting] MC_SlowVent(): Fast Vent Inlet Valve Close 확인 완료!";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	m_start_time_mc = clock();
	m_finish_time_mc =g_pConfig->m_nTimeout_sec_MCLLCVent;

	while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc) //1800 초 (30분)
	{
		m_nMc_standby_vent_time_cnt = ((clock() - m_start_time_mc) / CLOCKS_PER_SEC);
		if ((g_pGauge_IO->GetLlcVacuumRate() > g_pConfig->m_dPressure_ChangeToVent) && (g_pGauge_IO->GetMcVacuumRate() > g_pConfig->m_dPressure_ChangeToVent)) //0.003 torr
		{
			MC_Pressure.Empty();
			LLC_Pressure.Empty();
			MC_Pressure.Format("%f", g_pGauge_IO->GetMcVacuumRate());
			LLC_Pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
			str = " [MC Venting] MC_SlowVent(): MC 대기 Venting Break 시점 진공 값 확인 !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(("MC : " + MC_Pressure + ", LLC : " + LLC_Pressure))));
			break;
		}
	}

	if ((g_pGauge_IO->GetLlcVacuumRate() <= g_pConfig->m_dPressure_ChangeToVent) || (g_pGauge_IO->GetMcVacuumRate() <= g_pConfig->m_dPressure_ChangeToVent))
	{
		str = " [MC Venting] MC_SlowVent(): Slow Vent Tmp Gate Valve Close 후 대기 venting Timeout 발생 !";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MC_STANDBYVENTING_TIMEOUT_FAIL;
	}

	str = " [MC Venting] MC_SlowVent(): TMP Gate Valve Close 후 대기 Venting 완료 !";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  


	//if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_SlowVent(): MC Venting시 LLC Gate Valve Close 에러 발생";
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	g_pLog->Display(0, str);
	//	  
	//	return MC_Venting_ERROR;
	//}
	//if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_SlowVent(): MC Venting시 TR Gate Valve Open 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	  
	//	return MC_Venting_ERROR;
	//}
	//if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_SlowVent(): MC Venting시 LLC TMP Gate Valve Close 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	  
	//	return MC_Venting_ERROR;
	//}
	//if (g_pIO->Is_MC_TMP_GateValve_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_SlowVent(): MC Venting시 MC TMP Gate Valve Close 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	  
	//	return MC_Venting_ERROR;
	//}
	//
	////FORELINE OPEN VENTING.
	//if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_OPENED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_SlowVent(): MC Venting시 LLC TMP Foreline Valve Open 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	  
	//	return MC_Venting_ERROR;
	//}
	//if (g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_OPENED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_SlowVent(): MC Venting시 MC TMP Foreline Valve Open 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	  
	//	return MC_Venting_ERROR;
	//}
	//if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_SlowVent(): MC Venting시 LLC Fast Rough Valve Close 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	  
	//	return MC_Venting_ERROR;
	//}
	//if (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_SlowVent(): MC Venting시 MC Fast Rough Valve Close 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	  
	//	return MC_Venting_ERROR;
	//}
	//if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_SlowVent(): MC Venting시 LLC Slow Rough Valve Close 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	  
	//	return MC_Venting_ERROR;
	//}
	//if (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_SlowVent(): MC Venting시 MC Slow Rough Valve Close 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	  
	//	return MC_Venting_ERROR;
	//}
	//if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_SlowVent(): MC Venting시 Fast Vent Valve Close 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	  
	//	return MC_Venting_ERROR;
	//}
	//if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	//{
	//	m_McVacuumState = MC_Venting_ERROR;
	//	str = " MC_SlowVent(): MC Venting시 Slow Vent Valve #1 Close 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	  
	//	return MC_Venting_ERROR;
	//}
	//

	double mfc_ch1 = 0.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
	unsigned long long pBuffer = mfc_ch1;
	unsigned long long p_ret = 0x00;

	CString LLC_pressure;
	CString MC_pressure;
	CString pressure;
	//	g_pIO->WriteOutputData(0, &pBuffer, 2);
	
	m_nMc_slow_vent_time_cnt = 0;

	LLC_pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
	MC_pressure.Format("%f", g_pGauge_IO->GetMcVacuumRate());

	str = " [MC Venting] MC_SlowVent(): MC Venting시  Slow Vent 가능 조건 확인 !";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	str = " [MC Venting] MC_SlowVent(): MC Venting시 Slow Vent 조건 :: [0.1 Torr 이하 조건]!";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	pressure = _T("[MC Venting] MC_SlowVent(): LLC Vaccum :: ") + LLC_pressure + _T(",  MC Vaccum :: ") + MC_pressure;
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(pressure)));
	g_pLog->Display(0, pressure);

	

	if (g_pGauge_IO->GetLlcVacuumRate() <= g_pConfig->m_dPressure_ChangeToSlow_Vent && g_pGauge_IO->GetMcVacuumRate() <= g_pConfig->m_dPressure_ChangeToSlow_Vent) // 0.1 torr 보다 낮으면.
	{
		str = "[MC Venting] MC_SlowVent(): MC Venting시  Slow Vent Valve Open !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		ret = g_pIO->Open_SlowVentValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = "[MC Venting] MC_SlowVent(): MC Venting시 Slow Vent Valve #1 Open 명령 에러 발생 !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
			g_pIO->WriteOutputData(0, &p_ret, 2); // ????
			return ret;
		}

		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, g_pIO->Log_str);
		  

		m_start_time_mc = clock();
		m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
		{
			if (g_pIO->Is_SlowVentValve_Open() == VALVE_OPENED)
			{
				g_pIO->WriteOutputData(0, &pBuffer, 2);
				break;
			}
		}

		if (g_pIO->Is_SlowVentValve_Open() != VALVE_OPENED)
		{
			str = "[MC Venting] MC_SlowVent(): MC Venting시 Slow Vent Valve #1 Open Time out 에러 발생";
			g_pLog->Display(0, str);
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pIO->WriteOutputData(0, &p_ret, 2);	//????
			return INTERLOCK_SLOWVENT_OPEN_CHECK_FAIL;
		}

		str = " [MC Venting] MC_SlowVent(): Slow Vent Valve Open 완료 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		str = "[MC Venting] Start Slow Venting";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		str = " [MC Venting] MC_SlowVent() : Slow Vent Open 후 0.1 torr 까지 진행 (1800초 : 30분)";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		m_start_time_mc = clock();
		m_finish_time_mc = g_pConfig->m_nTimeout_sec_MCSlow1Vent; // Slow Venting 0.1 torr 될때까지 1800초 진행.
		while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
		{
			m_nMc_slow_vent_time_cnt = ((clock() - m_start_time_mc) / CLOCKS_PER_SEC);
			if ((g_pGauge_IO->GetLlcVacuumRate() < 0.001) && (g_pGauge_IO->GetMcVacuumRate() < 0.001))
			{
				mfc_ch1 = 1 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch1;
				g_pIO->WriteOutputData(0, &pBuffer, 2);

			}

			if (((g_pGauge_IO->GetLlcVacuumRate() > 0.001) && (g_pGauge_IO->GetLlcVacuumRate() < 0.01)) && ((g_pGauge_IO->GetMcVacuumRate() > 0.001) && (g_pGauge_IO->GetMcVacuumRate() < 0.01)))
			{
				mfc_ch1 = 3 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch1;
				g_pIO->WriteOutputData(0, &pBuffer, 2);
			}

			if (((g_pGauge_IO->GetLlcVacuumRate() > 0.01) && (g_pGauge_IO->GetLlcVacuumRate() < 0.05)) && ((g_pGauge_IO->GetMcVacuumRate() > 0.01) && (g_pGauge_IO->GetMcVacuumRate() < 0.05)))
			{
				mfc_ch1 = 4 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch1;
				g_pIO->WriteOutputData(0, &pBuffer, 2);
			}

			if (((g_pGauge_IO->GetLlcVacuumRate() > 0.05) && (g_pGauge_IO->GetLlcVacuumRate() <= 0.1)) && ((g_pGauge_IO->GetMcVacuumRate() > 0.05) && (g_pGauge_IO->GetMcVacuumRate() <= 0.1)))
			{
				mfc_ch1 = 5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch1;
				g_pIO->WriteOutputData(0, &pBuffer, 2);
			}

			if ((g_pGauge_IO->GetLlcVacuumRate() > g_pConfig->m_dPressure_ChangeToSlow_Vent) && (g_pGauge_IO->GetMcVacuumRate() > g_pConfig->m_dPressure_ChangeToSlow_Vent)) // 0.1 torr
			{
				MC_Pressure.Empty();
				LLC_Pressure.Empty();
				MC_Pressure.Format("%f", g_pGauge_IO->GetMcVacuumRate());
				LLC_Pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
				str = " [MC Venting] MC_SlowVent(): MC Slow Venting Break 시점 진공 값 확인 !";
				SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(("MC : " + MC_Pressure + ", LLC : " + LLC_Pressure))));
				break;
			}
		}



		if ((g_pGauge_IO->GetLlcVacuumRate() <= g_pConfig->m_dPressure_ChangeToSlow_Vent) || (g_pGauge_IO->GetMcVacuumRate() <= g_pConfig->m_dPressure_ChangeToSlow_Vent))
		{
			str = " [MC Venting] MC_SlowVent(): MC_SlowVent 시 Slow Vent Timeout 발생 !";
			g_pLog->Display(0, str);
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pIO->WriteOutputData(0, &p_ret, 2);	//????
			return INTERLOCK_MC_SLOWVENTING_TIMEOUT_FAIL;
		}


		CString LLC_pressure;
		CString MC_pressure;
		CString pressure;
		//	g_pIO->WriteOutputData(0, &pBuffer, 2);

		LLC_pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
		MC_pressure.Format("%f", g_pGauge_IO->GetMcVacuumRate());

	
		str = " [MC Venting] MC_SlowVent(): MC Venting시 Slow Vent 후 값 확인 :: [0.1 Torr 이상 조건]!";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		pressure = _T(" [MC Venting] MC_SlowVent() : LLC Vaccum :: ") + LLC_pressure + _T(",  MC Vaccum :: ") + MC_pressure;
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(pressure)));
		g_pLog->Display(0, pressure);

		str = " [MC Venting] MC_SlowVent():MC Slow Venting 완료";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  


		//str = " MC_SlowVent(): LLC Slow Vent Valve Open 확인 !";
		//g_pLog->Display(0, str);
		//  
		//m_start_time = clock();
		//m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation; //5초
		//while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		//{
		//	if (g_pIO->Is_SlowVentValve_Open() == VALVE_OPENED)
		//		break;
		//}
		//
		//
		//if (g_pIO->Is_SlowVentValve_Open() != VALVE_OPENED)
		//{
		//	m_LlcVacuumState = Venting_ERROR;
		//	str = " LLC_SlowVent(): LLC Venting시 Slow Vent Valve #1 Open 에러 발생";
		//	g_pLog->Display(0, str);
		//	g_pLog->Display(0, g_pIO->Log_str);
		//	  
		//	g_pIO->WriteOutputData(0, &p_ret, 2);
		//	return Venting_ERROR;
		//}
		//
		//str = " LLC_SlowVent(): LLC Slow Vent Valve Open 확인 완료 !";
		//g_pLog->Display(0, str);
		//  
		//
		/*
		str = " Start Slow2 Venting";
		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_LLKSlow2Vent;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (g_pLLCGauge->m_dPressure > g_pConfig->m_dPressure_ChangeToFast_Vent)
				break;
		}

		if (g_pLLCGauge->m_dPressure <= g_pConfig->m_dPressure_ChangeToFast_Vent)
		{
			m_LlcVacuumState = Venting_ERROR;
			str = " LLC_SlowVent(): Slow Vent Valve #2 Timeout 발생 !";
			return Venting_ERROR;
		}
		*/
		str = "[MC Venting]  MC_SlowVent(): MC_SlowVent시 Slow Vent Valve Close !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		ret = g_pIO->Close_SlowVentValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = " [MC Venting] MC_SlowVent(): MC_SlowVent 시 Slow Vent Valve #1 Close 명령 에러 발생 !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
			return ret;
		}
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, g_pIO->Log_str);
		  

		m_start_time_mc = clock();
		m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
		{
			if (g_pIO->Is_SlowVentValve_Open() == VALVE_CLOSED) 
				break;
		}

		if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
		{
			str = " [MC Venting] MC_SlowVent(): MC Venting시 Slow Vent Valve #1 Close 에러 발생";
			g_pLog->Display(0, str);
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
		}

		str = "[MC Venting] MC_SlowVent(): MC Slow Vent Valve Close 확인 완료 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		g_pIO->WriteOutputData(0, &p_ret, 2);
		str = " [MC Venting] MC_SlowVent(): MC Slow Vent MFC OFF !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		g_pIO->WriteOutputData(0, &p_ret, 2);
	
		str = " [MC Venting] MC_SlowVent(): MC Slow Vent 완료 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		g_pIO->WriteOutputData(0, &p_ret, 2);

		
	}
	else
	{
		str = "[MC Venting] MC_SlowVent(): MC Slow Vent 가능 범위가 아님!!";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		//g_pIO->WriteOutputData(0, &p_ret, 2);
	}

	return ret;
}

int CVacuumProcess::MC_FastVent()
{
	int ret = SEQUENCE_DONE;

	char* str;
	CString MC_Pressure, LLC_Pressure;

	m_nMc_fast_vent_time_cnt = 0;

	str = " [MC Venting] MC_FastVent(): MC FAST Vent 시작 !";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	str = " [MC Venting] MC_FastVent(): MC FAST Vent Valve Check Start ! ";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
	{
		str = " [MC Venting] MC_FastVent(): MC Venting시 TR Gate Valve Open 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_TRGATE_OPEN_CHECK_FAIL;
	}

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_FastVent(): MC Venting시 LLC Gate Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_FastVent(): MC Venting시 LLC TMP Gate Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_FastVent(): MC Venting시 MC TMP Gate Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCTMPGATE_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " [MC Venting] MC_FastVent(): MC Venting시 LLC TMP Foreline Valve Open 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_LLCFORELINE_OPEN_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " [MC Venting] MC_FastVent(): MC Venting시 MC TMP Foreline Valve Open 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCFORELINE_OPEN_CHECK_FAIL;
	}

	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_FastVent(): MC Venting시 LLC Fast Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_FastVent(): MC Venting시 LLC Slow Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_LLCSLOWROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_FastVent(): MC Venting시 MC Fast Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCFASTROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_MC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_FastVent(): MC Venting시 MC Slow Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_MCSLOWROUGH_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_FastVent(): MC Venting시 Fast Vent Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
	}

	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = " [MC Venting] MC_FastVent(): MC Venting시 Slow Vent Valve #1 Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
	}

	str = " [MC Venting] MC_FastVent(): MC FAST Vent Valve Check 완료 ! ";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  


	double mfc_ch2 = 0.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
	unsigned long long pBuffer = mfc_ch2;
	unsigned long long p_ret = 0x00;

	//g_pIO->WriteOutputData(2, &pBuffer, 2);

	CString LLC_pressure;
	CString MC_pressure;
	CString pressure;
	//	g_pIO->WriteOutputData(0, &pBuffer, 2);

	LLC_pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
	MC_pressure.Format("%f", g_pGauge_IO->GetMcVacuumRate());


	str = " [MC Venting] MC_FastVent(): MC Venting시 Fast Vent 조건 값 확인 :: [760 Torr 이하 조건]!";
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	pressure = _T(" [MC Venting] LLC Vaccum :: ") + LLC_pressure + _T(",  MC Vaccum :: ") + MC_pressure;
	SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(pressure)));
	g_pLog->Display(0, pressure);


	if ((g_pGauge_IO->GetLlcVacuumRate() <= g_pConfig->m_dPressure_Vent_End) && (g_pGauge_IO->GetMcVacuumRate() <= g_pConfig->m_dPressure_Vent_End)) // 760 
	{
		g_pIO->WriteOutputData(2, &pBuffer, 2);

		str = " [MC Venting] MC_FastVent(): MC Venting 시 진공도 760Torr 보다 작음 확인 !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		str = " [MC Venting] MC_FastVent(): MC Venting 시 FastVentValve Open  !";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		ret = g_pIO->Open_FastVentValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = "[MC Venting] MC_FastVent(): Fast Vent Valve Open 명령 에러 발생 !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
			  
			g_pIO->WriteOutputData(2, &p_ret, 2);	//???
			return ret;
		}
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, g_pIO->Log_str);
		  

		m_start_time_mc = clock();
		m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
		{
			if (g_pIO->Is_FastVentValve_Open() == VALVE_OPENED)
				break;
		}

		if (g_pIO->Is_FastVentValve_Open() != VALVE_OPENED)
		{
			str = "[MC Venting] MC_FastVent(): MC Venting시 Fast Vent Valve Open 에러 발생";
			g_pLog->Display(0, str);
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			  
			g_pIO->WriteOutputData(2, &p_ret, 2);
			return INTERLOCK_FASTVENT_OPEN_CHECK_FAIL;
		}

		str = "[MC Venting] MC_FastVent(): MC Venting 시 FastVentValve Open 확인 완료 !!";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		str = "[MC Venting] Start Fast Venting";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		str = "[MC Venting] MC_FastVent(): FastVent Open 후 760torr 까지 진행 (1800초 : 30분) ";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		m_start_time_mc = clock();
		m_finish_time_mc = g_pConfig->m_nTimeout_sec_MCFastVent;
		while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
		{
			m_nMc_fast_vent_time_cnt = ((clock() - m_start_time_mc) / CLOCKS_PER_SEC);
			if ((g_pGauge_IO->GetLlcVacuumRate() < 1) && (g_pGauge_IO->GetMcVacuumRate() < 1))
			{
				mfc_ch2 = 1 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}

			if (((g_pGauge_IO->GetLlcVacuumRate() > 1) && (g_pGauge_IO->GetLlcVacuumRate() < 10)) && ((g_pGauge_IO->GetMcVacuumRate() > 1) && (g_pGauge_IO->GetMcVacuumRate() < 10)))
			{
				mfc_ch2 = 1.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}

			if (((g_pGauge_IO->GetLlcVacuumRate() > 10) && (g_pGauge_IO->GetLlcVacuumRate() < 50)) && ((g_pGauge_IO->GetMcVacuumRate() > 10) && (g_pGauge_IO->GetMcVacuumRate() < 50)))
			{
				mfc_ch2 = 2 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}

			if (((g_pGauge_IO->GetLlcVacuumRate() > 10) && (g_pGauge_IO->GetLlcVacuumRate() < 20)) && ((g_pGauge_IO->GetMcVacuumRate() > 10) && (g_pGauge_IO->GetMcVacuumRate() < 20)))
			{
				mfc_ch2 = 2.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}

			if (((g_pGauge_IO->GetLlcVacuumRate() > 20) && (g_pGauge_IO->GetLlcVacuumRate() <= 100)) && ((g_pGauge_IO->GetMcVacuumRate() > 20) && (g_pGauge_IO->GetMcVacuumRate() <= 100)))
			{
				mfc_ch2 = 3 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}

			if (((g_pGauge_IO->GetLlcVacuumRate() > 100) && (g_pGauge_IO->GetLlcVacuumRate() <= 150)) && ((g_pGauge_IO->GetMcVacuumRate() > 100) && (g_pGauge_IO->GetMcVacuumRate() <= 150)))
			{
				mfc_ch2 = 3.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}

			if (((g_pGauge_IO->GetLlcVacuumRate() > 150) && (g_pGauge_IO->GetLlcVacuumRate() <= 200)) && ((g_pGauge_IO->GetMcVacuumRate() > 150) && (g_pGauge_IO->GetMcVacuumRate() <= 200)))
			{
				mfc_ch2 = 4 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}
			if (((g_pGauge_IO->GetLlcVacuumRate() > 200) && (g_pGauge_IO->GetLlcVacuumRate() <= 250)) && ((g_pGauge_IO->GetMcVacuumRate() > 200) && (g_pGauge_IO->GetMcVacuumRate() <= 250)))
			{
				mfc_ch2 = 4.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}
			if (((g_pGauge_IO->GetLlcVacuumRate() > 250) && (g_pGauge_IO->GetLlcVacuumRate() <= 770)) && ((g_pGauge_IO->GetMcVacuumRate() > 250) && (g_pGauge_IO->GetMcVacuumRate() <= 770)))
			{
				mfc_ch2 = 5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}

			if ((g_pGauge_IO->GetLlcVacuumRate() > g_pConfig->m_dPressure_Vent_End) && (g_pGauge_IO->GetMcVacuumRate() > g_pConfig->m_dPressure_Vent_End)) //760 torr 
			{
				MC_Pressure.Empty();
				LLC_Pressure.Empty();
				MC_Pressure.Format("%f", g_pGauge_IO->GetMcVacuumRate());
				LLC_Pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
				str = " [MC Venting] MC_FastVent(): MC Fast Venting Break 시점 진공 값 확인 !";
				SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(("MC : " + MC_Pressure + ", LLC : " + LLC_Pressure))));
				break;
			}
		}

		if ((g_pGauge_IO->GetLlcVacuumRate() <= g_pConfig->m_dPressure_Vent_End) || (g_pGauge_IO->GetMcVacuumRate() <= g_pConfig->m_dPressure_Vent_End))
		{
			str = " [MC Venting] MC_FastVent(): Fast Vent Timeout 발생 !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			return INTERLOCK_MC_FASTVENTING_TIMEOUT_FAIL;
		}

		str = " [MC Venting] MC_FastVent(): MC Fast Venting 완료 !!";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  


		str = " [MC Venting] MC_FastVent(): MC Venting 시 FastVentValve CLOSE !!";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		ret = g_pIO->Close_FastVentValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = " [MC Venting] MC_FastVent(): Fast Vent Valve Close 명령 에러 발생 !";

			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
			  
			return ret;
		}
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, g_pIO->Log_str);
		  

		m_start_time_mc = clock();
		m_finish_time_mc = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time_mc) / CLOCKS_PER_SEC <= m_finish_time_mc)
		{
			if (g_pIO->Is_FastVentValve_Open() == VALVE_CLOSED)
				break;
		}

		if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
		{
			str = " [MC Venting] MC_FastVent(): MC Venting시 Fast Vent Valve Close 에러 발생 !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
		}
		str = " [MC Venting] MC_FastVent(): MC Venting 시 FastVentValve CLOSE 완료!!";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		str = " [MC Venting] MC_FastVent(): MC Venting 시 Fast MFC OFF 완료!!";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		g_pIO->WriteOutputData(2, &p_ret, 2);


		str = " [MC Venting] MC_FastVent(): MC Venting 시 Fast Venting 완료!!";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		
		return ret;

	}
	else
	{
		str = " [MC Venting] MC_FastVent(): MC Venting 시 FastVentValve Open 가능 범위가 아님!! (OVER VENTING)";
		SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		g_pIO->WriteOutputData(2, &p_ret, 2);
		return INTERLOCK_NOT_VENTINGABLE_RANGE_CHECK_FAIL;
	}
}

int CVacuumProcess::MC_Venting_Complete()
{
	int ret = 0;
	char* str;

	CString standby_vent_time;
	CString slow_vent_time;
	CString fast_vent_time;

	standby_vent_time.Empty();
	slow_vent_time.Empty();
	fast_vent_time.Empty();

	standby_vent_time.Format("%d", m_nMc_standby_vent_time_cnt);
	slow_vent_time.Format("%d", m_nMc_slow_vent_time_cnt);
	fast_vent_time.Format("%d", m_nMc_fast_vent_time_cnt);

	SaveLogFile("SREM_Sequence_Time_Report", _T((LPSTR)(LPCTSTR)(" MC Venting Sequence Time :: Standby Venting [ " + standby_vent_time + " ] , Slow Venting [ " + slow_vent_time + "  ] , Fast Venting [ " + fast_vent_time + " ] ")));

	g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
	
	m_McVacuumState = MC_State_IDLE;
	return ret;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////// LLC Pumping or Venting Thread Start /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int CVacuumProcess::LLC_Pumping_Start()
{
	CString str;

	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if (m_pVaccumThread == NULL)
		{
			m_LlcVacuumState = Pumping_START;
			m_bLlcVenting = FALSE;

			m_pVaccumThread = ::AfxBeginThread(LLC_VacuumThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
			
			str = "LLC Pumping Thread 가동 시작 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
	
			//////////////////////////////////
			// OPER MODE ON 동작 시퀀스 (임시삭제)
			//////////////////////////////////
	
			//if (g_pIO->IO_MODE == OPER_MODE_ON)
			//{
			//	m_LlcVacuumState = Pumping_START;
			//	m_LLC_seq_state = Pumping_START_State;
			//	m_pVaccumThread = ::AfxBeginThread(LLC_VacuumThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
			//	//g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(false);
			//	//g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(false);
			//	//g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(false);
			//	//g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(false);
			//	str = "LLC Pumping Thread 가동 시작 !";
			//	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			//	g_pLog->Display(0, str);
			//}
			//else
			//{
			//	g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
			//	g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
			//	g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
			//	g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
			//	str = "LLC Pumping Thread 가동 불가 ! OPER MODE 가 아닙니다";
			//	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			//	g_pLog->Display(0, str);
			//	ret = VACUUM_ERROR;
			//}
		}
		else
		{
			str = "LLC Pumping Thread가 이미 동작 중이므로 가동 실패 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			return -999;
		}
	}
	else
	{
		str = "Crevis_Open_Port Fail 이므로 가동 실패 !";
		::AfxMessageBox("Crevis_Open_Port Fail 이므로 가동 실패 !", MB_ICONINFORMATION);
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return -999;
	}

	return 0;
}

int CVacuumProcess::LLC_Venting_Start()
{
	CString str;

	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if (m_pVaccumThread == NULL)
		{
			m_LlcVacuumState = Venting_START;
			m_bLlcVenting = TRUE;

			m_pVaccumThread = ::AfxBeginThread(LLC_VacuumThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
			
			str = "LLC Venting Thread 가동 시작 !";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		
			//////////////////////////////////
			// OPER MODE ON 동작 시퀀스 
			//////////////////////////////////
		
		
			//if (g_pIO->IO_MODE == OPER_MODE_ON)
			//{
			//	m_LlcVacuumState = Venting_START;
			//	m_LLC_seq_state = Venting_START_State;
			//	m_pVaccumThread = ::AfxBeginThread(LLC_VacuumThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
			//	str = "LLC Venting Thread 가동 시작 !";
			//	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			//	//g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
			//	//g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
			//	//g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
			//	//g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
			//
			//	g_pLog->Display(0, str);
			//}
			//else
			//{
			//	str = "LLC Venting Thread 가동 불가 ! OPER MODE 가 아닙니다";
			//	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			//	g_pLog->Display(0, str);
			//	g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
			//	g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
			//	g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
			//	g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
			//}
		}
		else
		{
			str = "LLC Venting Thread가 이미 동작 중이므로 가동 실패 !";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			g_pMaindialog->KillTimer(LLC_SEQUENCE_CHECK_TIMER);
			return -999;
		}
	}
	else
	{
		g_pMaindialog->KillTimer(LLC_SEQUENCE_CHECK_TIMER);
		str = "Crevis_Open_Port Fail 이므로 가동 실패 !";
		::AfxMessageBox("Crevis_Open_Port Fail 이므로 가동 실패 !", MB_ICONINFORMATION);	//20210819 jkseo, 요기에 필요없.
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return -999;
	}

	return 0;
}

UINT CVacuumProcess::LLC_VacuumThread(LPVOID pParam)
{
	CVacuumProcess*  g_pVP = (CVacuumProcess*)pParam;

	int ret = 0;
	g_pVP->m_bVacuumThreadStop = FALSE;
	g_pVP->m_nLlcErrorCode = 0;

	while (!g_pVP->m_bVacuumThreadStop)
	{
		/* 질소 유량 확인 함수 */


		//////////////////////////////////////////////////////

		g_pVP->m_nLlcErrorCode = g_pVP->LLC_Vacuum_Loop();

		if (g_pVP->m_LlcVacuumState == Venting_COMPLETE && g_pVP->m_nLlcErrorCode == 0)
		{
			g_pVP->m_bVacuumThreadStop = TRUE;

			ret = g_pVP->LLC_Venting_Complete();		//jkseo llc gate open 함수가 요기에 ??

		}

		if(g_pVP->m_LlcVacuumState == Pumping_COMPLETE && g_pVP->m_nLlcErrorCode == 0)
		{
			g_pVP->m_bVacuumThreadStop = TRUE;
			
			ret = g_pVP->LLC_Pumping_Complete();
		}

		if (g_pVP->m_nLlcErrorCode != 0)
		{
			if (g_pVP->m_bLlcVenting == TRUE)
			{
				g_pVP->LLC_Venting_Error();
			}
			else
			{
				g_pVP->LLC_Pumping_Error();
			}

			g_pVP->m_bVacuumThreadStop = TRUE;
		}
	}

	g_pVP->m_pVaccumThread = NULL;

	return 0;
}

int CVacuumProcess::LLC_Vacuum_Loop()
{
	int	ret = 0;
	CString str;

	switch (m_LlcVacuumState)
	{
		case Pumping_START:
		{
			str.Format(_T(" [LLC Pumping] [LLC Pumping Start]"));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);

			if (GetLLCVacuumStatus() == CHAMBER_PUMPED)
			{
				str.Format(_T(" [LLC Pumping] [LLC PUMPED]"));
				SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				g_pLog->Display(0, str);
				  
				m_LlcVacuumState = Pumping_COMPLETE;
			}
			else if (GetLLCVacuumStatus() == VACUUM_ERROR)
			{
				str.Format(_T(" [LLC Pumping] [LLC 진공 Gauge Error 발생]"));
				SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				g_pLog->Display(0, str);
				ret = -999;
			}
			else
			{
				str.Format(_T(" [LLC Pumping] [LLC Pumping PreWork Start]"));
				SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				g_pLog->Display(0, str);
				  
				ret = LLC_Pumping_PreWork();
				
				if (ret != OPERATION_COMPLETED)
					return ret;
				else
					m_LlcVacuumState = Pumping_SLOWROUGH;
			}
			break;
		}
		case Pumping_SLOWROUGH:
		{
			str.Format(_T(" [LLC Pumping] [LLC_SlowRough Start]"));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			ret =LLC_SlowRough();
			if (ret != OPERATION_COMPLETED)
				return ret;
			else
				m_LlcVacuumState = Pumping_FASTROUGH;
			break;
		}
		case Pumping_FASTROUGH:
		{
			str.Format(_T("[LLC_FastRough Start]"));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			ret = LLC_FastRough();
			if (ret != OPERATION_COMPLETED)
				return ret;
			else
				m_LlcVacuumState = Pumping_TMPROUGH;
			break;
		}
		case Pumping_TMPROUGH:
		{
			str.Format(_T("[LLC_TMPRough Start]"));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			ret = LLC_TMPRough();
			if (ret != OPERATION_COMPLETED)
				return ret;
			else
				m_LlcVacuumState = Pumping_COMPLETE;
			break;
		}
		case Venting_START:
		{
			str.Format(_T(" [LLC Venting] [LLC Venting Start]"));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);

			//진공 상태에 따라 중간에서 시작할 수 있도록 하자.
			if (GetLLCVacuumStatus() == CHAMBER_VENTED)
			{
				str.Format(_T(" [LLC Venting] [LLC VENTED]"));
				SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				g_pLog->Display(0, str);
				  
				m_LlcVacuumState = Venting_COMPLETE;
			}
			else if (GetLLCVacuumStatus() == VACUUM_ERROR)
			{
				str.Format(_T("[LLC Venting] [LLC 진공 Gauge Error 발생]"));
				SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				g_pLog->Display(0, str);
				  
				ret = -999;
			}
			else
			{
				str = "[LLC Venting] [LLC_Venting_PreWork Start]";
				SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				g_pLog->Display(0, str);
				m_bMcVentingState = false;

				ret = LLC_Venting_PreWork();
				if (ret != OPERATION_COMPLETED)
					return ret;
				else
					m_LlcVacuumState = Venting_SLOWVENT;
			}
			break;
		}
		case Venting_SLOWVENT:
		{
			str.Format(_T("[LLC Venting] [LLC_SlowVent Start]"));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			ret = LLC_SlowVent();
			if (ret != OPERATION_COMPLETED)
				return ret;
			else
				m_LlcVacuumState = Venting_FASTVENT;
			break;
		}
		case Venting_FASTVENT:
		{
			str.Format(_T("[LLC_FastVent Start]"));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			ret = LLC_FastVent();
			if (ret != OPERATION_COMPLETED)
				return ret;
			else
				m_LlcVacuumState = Venting_COMPLETE;
			break;
		}
		default:
			break;
	}

	return ret;
}

int CVacuumProcess::LLC_Pumping_Complete()
{
	int ret = 0;
	char* str;

	CString str_tmp;
	CString slow_pump_time;
	CString fast_pump_time;
	CString tmp_pump_time;
	CString LLC_pressure;
	str_tmp.Empty();
	slow_pump_time.Empty();
	fast_pump_time.Empty();
	tmp_pump_time.Empty();



	str = " [LLC Pumping] LLC_Pumping_Complete(): LLC Pumping 완료!";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));

	slow_pump_time.Format("%d", m_nLlc_slow_rough_time_cnt);
	fast_pump_time.Format("%d", m_nLlc_fast_rough_time_cnt);
	tmp_pump_time.Format("%d", m_nLlc_tmp_rough_time_cnt);

	SaveLogFile("SREM_Sequence_Time_Report", _T((LPSTR)(LPCTSTR)(" LLC Pumping Sequence Time :: Slow Rough Pumping [ " + slow_pump_time + " ] , Fast Rough Pumping [ " + fast_pump_time + "  ] , Tmp Pumping [ " + tmp_pump_time + " ] ")));

	g_pLog->Display(0, str);

	g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
	//g_pWarning->ShowWindow(SW_HIDE);

	return ret;
}

int CVacuumProcess::LLC_TMPRough()
{
	int ret = SEQUENCE_DONE;

	CString str;
	CString str_tmp;

	m_nLlc_tmp_rough_time_cnt = 0;

	//1. 상태확인:LLC Gate Valve Close,TR Gate Valve Close,LLC TMP Gate Valve Close,LLC TMP Foreline Valve Close,LLC Fast & Slow Rough Valve Close,Fast & Slow1,2 Vent Valve Close,LLC Lid Close
	//2. LLC 진공도가 환경설정에 기록된 Fast Rough->TMP Rough 전환 진공도보다 낮은지 확인
	//3. LLC Foreline Valve Open
	//4. LLC TMP Gate Valve Open
	//5. LLC 진공도가 환경설정에 기록된 LLC Pumping End 진공도에 도달여부 확인 후 상태 변경

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_TMPRough(): LLC Pumping시 LLC Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_TMPRough(): LLC Pumping시 TR Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_TRGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_TMPRough(): LLC Pumping시 LLC TMP Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_TMPRough(): LLC Pumping시 LLC TMP Foreline Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFORELINE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_TMPRough(): LLC Pumping시 LLC Fast Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_TMPRough(): LLC Pumping시 LLC Slow Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCSLOWROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_TMPRough(): LLC Pumping시 Fast Vent Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_TMPRough(): LLC Pumping시 Slow Vent Valve #1 Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
	}



	CString LLC_pressure;
	CString pressure;

	LLC_pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());


	str = " [LLC Pumping] LLC_TMPRough(): LLC Pumping 시 TMP Pumping 가능 조건 확인 :: [0.1 Torr 이하 조건]!";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	pressure = _T("[LLC Pumping] LLC Vaccum :: ") + LLC_pressure;
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(pressure)));
	g_pLog->Display(0, pressure);



	//if (g_pGauge_IO->GetLlcVacuumRate() >= g_pConfig->m_dPressure_ChangeToTMP_Rough) // 0.1 torr
	if (g_pGauge_IO->GetLlcVacuumRate() >= 0.1) // 0.1 torr
	{
		str = " [LLC Pumping] LLC_TMPRough(): LLC Pumping시 TMP Pumping 가능 압력이 아님 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		return INTERLOCK_NOT_PUMPABLE_RANGE_CHECK_FAIL;
	}

	str = "[LLC Pumping] LLC_TMPRough(): LLC Pumping시 TMP Pumping 가능 압력 확인 !";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	str = " [LLC Pumping] LLC_TMPRough(): LLC TMP Foreline Valve Open 명령 !!!";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	ret = g_pIO->Open_LLC_TMP_ForelineValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [LLC Pumping] LLC_TMPRough(): LLC TMP Foreline Valve Open 명령 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_OPENED)
			break;
	}

	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " [LLC Pumping] LLC_TMPRough(): LLC Pumping시 LLC TMP Foreline Valve Open 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFORELINE_OPEN_CHECK_FAIL;
	}
	
	str = " [LLC Pumping] LLC_TMPRough(): LLC TMP Foreline Valve Open 확인 완료 !!!";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	str = " [LLC Pumping] LLC_TMPRough(): LLC TMP Gate Valve Open !!!";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	ret = g_pIO->Open_LLC_TMP_GateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " LLC_TMPRough(): LLC TMP Gate Valve Open 명령 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}

	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLC_TMP_GateValve_Open() == VALVE_OPENED)
			break;
	}

	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_OPENED)
	{
		str = " LLC_TMPRough(): LLC Pumping시 LLC TMP Gate Valve Open 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCTMPGATE_OPEN_CHECK_FAIL;
	}
	
	str = " [LLC Pumping] LLC_TMPRough(): LLC TMP Gate Valve Open 확인 완료!!!";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);


	str = " [LLC Pumping] Start TMP Roughing";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	str = " [LLC Pumping]  LLC_TMPRough(): LLC Tmp Pumping시 0.00003 torr 까지 진행 (1000초)";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);


	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_LLKRoughEnd; 
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		m_nLlc_tmp_rough_time_cnt = ((clock() - m_start_time) / CLOCKS_PER_SEC);
		if (g_pGauge_IO->GetLlcVacuumRate() < g_pConfig->m_dPressure_Rough_End) // 0.000009 torr 까지 진행
		{
			LLC_pressure.Empty();
			LLC_pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
			str = "  [LLC Pumping]  LLC_TMPRough(): LLC Tmp Rough Break 시점 진공 값 확인 !";
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)((LLC_pressure))));
			break;
		}
			
	}

	if (g_pGauge_IO->GetLlcVacuumRate() >= g_pConfig->m_dPressure_Rough_End) 
	{
		str = "[LLC Pumping] LLC_TMPRough(): LLC TMP Roughing Timeout 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_TMPROUGHING_TIMEOUT_FAIL;
	}

	str = " [LLC Pumping] LLC_TMPRough() : End TMP Roughing";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	str = " [LLC Pumping] LLC_TMPRough() : TR Gate Open 진행";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	// TMP Rough Pumping 후 TR Gate Open 
	// TR Gate Valve Open

	str = " [LLC Pumping] LLC_TMPRough(): TR Gate Valve Open 전 MC 진공도 확인 중!";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_LLKRoughEnd; // 1200 초 동안 진행
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		m_nLlc_tmp_rough_time_cnt = ((clock() - m_start_time) / CLOCKS_PER_SEC);
		if (g_pGauge_IO->GetLlcVacuumRate() < g_pConfig->m_dPressure_Rough_End) // 0.000009 torr 까지 진행
		{
			LLC_pressure.Empty();
			LLC_pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
			str = "  [LLC Pumping]  LLC_TMPRough():LLC_Pumping_Complete Break 시점 진공 값 확인 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)((LLC_pressure))));
			break;
		}

	}

	ResetEvent(g_pIO->m_TRGate_Vauccum_Value_On);
	//if (g_pGauge_IO->m_dPressure_MC <= 0.00003)
	//if (g_pGauge_IO->m_dPressure_MC <= g_pConfig->m_dPressure_Rough_End)
	//if (g_pGauge_IO->m_dPressure_MC <= 0.0000009)
	if (g_pGauge_IO->GetMcVacuumRate() <= g_pConfig->m_dPressure_Rough_End) // 0.000009 torr 9x10^-6
	{
		str = " [LLC Pumping] LLC_TMPRough(): TR Gate Valve Open 전 MC 진공 값 확인 완료!";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		str = " [LLC Pumping] LLC_TMPRough(): TR Gate Valve Open 전 MC 진공 값 :: !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		str_tmp.Format("%Ef", g_pGauge_IO->GetMcVacuumRate());
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str_tmp)));
		g_pLog->Display(0, str_tmp);

		str = " [LLC Pumping] LLC_TMPRough(): TR Gate Valve Open 전 LLC 진공 값 :: !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		str_tmp.Format("%Ef", g_pGauge_IO->GetLlcVacuumRate());
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str_tmp)));
		g_pLog->Display(0, str_tmp);


		str = " [LLC Pumping] LLC_TMPRough(): TR Gate Valve Open 명령!";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		if (g_pIO->Is_LLC_Vac_Check())
			g_pIO->On_LLCSetPoint_VAC();
		else
			return -9999;

		if (g_pIO->Is_MC_Vac_Check())
			g_pIO->On_MCSetPoint_VAC();
		else
			return -9999;
		
		ret = g_pIO->Open_TRGateValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = " [LLC Pumping] LLC_TMPRough(): TR Gate Valve Open 명령 에러 발생 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T(("Open_TRGateValve() Return Value :: ")));
			 
			g_pLog->Display(0, str);
			return ret;
		}
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T(("Open_TRGateValve() Return Value :: ")));
		 

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (g_pIO->Is_TRGateValve_Open() == VALVE_OPENED)
				break;
		}

		if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
		{
			str = " [LLC Pumping] LLC_TMPRough(): LLC Pumping시 TR Gate Open 에러 발생";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);

			return -999;		//error code
		}

		str = " [LLC Pumping] LLC_TMPRough(): TR Gate Valve Open 확인 완료!";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
	}
	//else if (WaitForSingleObject(g_pIO->m_TRGate_Vauccum_Value_On, 1800000) == WAIT_OBJECT_0) //30분
	else if (WaitForSingleObject(g_pIO->m_TRGate_Vauccum_Value_On, 3600000) == WAIT_OBJECT_0) //60분
	{
		str = " [LLC Pumping] LLC_TMPRough(): TR Gate Valve Open 명령!";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		ret = g_pIO->Open_TRGateValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = " [LLC Pumping] LLC_TMPRough(): TR Gate Valve Open 명령 에러 발생 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T(("Open_TRGateValve() Return Value :: ")));
			 
			g_pLog->Display(0, str);
			return ret;
		}
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T(("Open_TRGateValve() Return Value :: ")));
		 

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (g_pIO->Is_TRGateValve_Open() == VALVE_OPENED)
				break;
		}

		if (g_pIO->Is_TRGateValve_Open() != VALVE_OPENED)
		{
			str = " [LLC Pumping] LLC_TMPRough(): LLC Pumping시 TR Gate Open 에러 발생";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);

			return -999;		//error code
		}

		str = " [LLC Pumping] LLC_TMPRough(): TR Gate Valve Open 확인 완료!";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
	}
	else
	{
		str = " [LLC Pumping] LLC_TMPRough(): TR Gate Valve Open Time Out Error !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return -999;
	}

	str = " [LLC Pumping] LLC_TMPRough() : End TMP Roughing";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	
	return ret;
}

int CVacuumProcess::LLC_FastRough()
{
	int ret = SEQUENCE_DONE;

	char* str;

	m_bSlowMfcInlet_Open_State = true; // SLOW MFC INLET VALVE OPEN 1 번 실행을 위한 플래그

	m_nLlc_fast_rough_time_cnt = 0;

	//CString str;

	//1. 상태확인:LLC Gate Valve Close,TR Gate Valve Close,LLC TMP Gate Valve Close,LLC TMP Foreline Valve Close,LLC Fast & Slow Rough Valve Close,Fast & Slow1,2 Vent Valve Close,LLC Lid Close
	//2. LLC 진공도가 환경설정에 기록된 Fast Rough->TMP Rough 전환 진공도보다 낮은지 확인
	//3. 높다면 LLC Fast Rough Valve Open
	//4. LLC 진공도가 환경설정에 기록된 Fast Rough->TMP Rough 전환 진공도에 도달했는지 확인
	//5. LLC Fast Rough Valve Close 확인 후 상태 변경

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_FastRough(): LLC Pumping시 LLC Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_FastRough(): LLC Pumping시 TR Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_TRGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_FastRough(): LLC Pumping시 LLC TMP Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_FastRough(): LLC Pumping시 LLC TMP Foreline Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFORELINE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_FastRough(): LLC Pumping시 LLC Fast Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_FastRough(): LLC Pumping시 LLC Slow Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCSLOWROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_FastRough(): LLC Pumping시 Fast Vent Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_FastRough(): LLC Pumping시 Slow Vent Valve #1 Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
	}

	CString LLC_pressure;
	CString pressure;

	LLC_pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());


	str = " [LLC Pumping] LLC_FastRough(): LLC Pumping 시 Fast Rough 가능 조건 확인 :: [0.02 Torr 이상 ~ 50 Torr 이하 조건]!";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	pressure = _T(" [LLC Pumping] LLC_FastRough() :: LLC Vaccum :: ") + LLC_pressure;
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(pressure)));
	g_pLog->Display(0, pressure);

	//
	// 0.02 (2.0x10^-2) 이상 50 Torr 이하 사이 진공 값일 경우 Fast Rough 진행 
	// 아닐 경우 PASS
	//
	if ((g_pGauge_IO->GetLlcVacuumRate() >= g_pConfig->m_dPressure_ChangeToTMP_Rough) && (g_pGauge_IO->GetLlcVacuumRate() <= g_pConfig->m_dPressure_ChangeToFast_Rough))// 0.1 torr ~ 50 Torr 
	{
		str = " [LLC Pumping] LLC_FastRough(): 0.02 Torr 이상 및 50 Torr 이하 진공 값 확인 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		str = " [LLC Pumping] Start Fast Roughing";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);


		str = " [LLC Pumping] LLC_FastRough():  LLC Pumping시 Fast Rough Valve Open !!";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		ret = g_pIO->Open_LLC_FastRoughValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = " [LLC Pumping] LLC_FastRough(): LLC Fast Rough Valve Open 명령 에러 발생 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Open_LLC_FastRoughValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
			  
			return ret;
		}
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Open_LLC_FastRoughValve() Return Value :: "));
		 
		g_pLog->Display(0, g_pIO->Log_str);

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (g_pIO->Is_LLC_FastRoughValve_Open() == VALVE_OPENED)
				break;
		}

		if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_OPENED)
		{
			str = "[LLC Pumping]  LLC_FastRough(): LLC Pumping시 LLC Fast Rough Valve Open 에러 발생";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
		}


		str = " [LLC Pumping] LLC_FastRough():  LLC Pumping시 Fast Rough Valve Open 완료 !!";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);


		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_LLKFastRough; // 300 초

		str = " [LLC Pumping] LLC_FastRough(): LLC Fast Rough 0.02 torr 까지 진행 (300초 : 5분)";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			m_nLlc_fast_rough_time_cnt=((clock() - m_start_time) / CLOCKS_PER_SEC);
			////////////////////////////////////////////////////////
			// 1^10^-1 torr 됬을 때 Inlet valve 열어 질소 빼줌.
			// 최초 1번 실행 
			////////////////////////////////////////////////////////
			if (g_pGauge_IO->GetLlcVacuumRate() <= g_pConfig->m_dPressure_SlowMFC_Inlet_Valve_Open_Value) //0.1 torr
			{

				//double mfc_ch1 = 0 * 409.5;
				//unsigned long long pBuffer = mfc_ch1;
				unsigned long long p_ret = 0x00;
				g_pIO->WriteOutputData(0, &p_ret, 2);
				//////////////////////////////////////////////
				// Slow MFC Inlet 라인의 질소를 제거 해주기 위함. ( Venting 작업 시 질소가 갑자기 투입 되는 것을 방지 하기 위함 )
				// Slow Vent Inlet Open 후 바로 Close
				/////////////////////////////////////////////
				if (m_bSlowMfcInlet_Open_State)
				{
					str = " [LLC Pumping] LLC_FastRough(): 0.1 torr 진공 값 확인 ! Slow MFC Inlet Valve Open ! ";
					SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);
					///////////////////////////////
					// SLOW MFC Inlet Valve Open
					///////////////////////////////		  
					ret = g_pIO->Open_SlowVent_Inlet_Valve();
					if (ret != OPERATION_COMPLETED)
					{
						str = "  [LLC Pumping] LLC_FastRough(): LLC Fast Rough 시 Slow MFC Inlet Open 명령 에러 발생";

						g_pLog->Display(0, str);
						g_pLog->Display(0, g_pIO->Log_str);
						SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
						SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
						SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Open_SlowVent_Inlet_Valve1() Return Value :: "));
						 
						return ret;
					}
					SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Open_SlowVent_Inlet_Valve1() Return Value :: "));
					 

					clock_t m_start_time_valve;
					clock_t m_finish_time_valve;

					m_start_time_valve = clock();
					m_finish_time_valve = g_pConfig->m_nTimeout_sec_ValveOperation;

					while ((clock() - m_start_time_valve) / CLOCKS_PER_SEC <= m_finish_time_valve)
					{
						if (g_pIO->Is_SlowVent_Inlet_Valve_Open() == VALVE_OPENED)
							break;

					}
					if (g_pIO->Is_SlowVent_Inlet_Valve_Open() != VALVE_OPENED)
					{
						str = " [LLC Pumping] LLC_FastRough(): LLC Fast Rough 시 Slow MFC Valve Open Time Out 에러 발생";
						g_pLog->Display(0, str);
						SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));

						return INTERLOCK_SLOWVENT_OPEN_CHECK_FAIL;
					}
					str = "  [LLC Pumping] LLC_FastRough(): Slow Vent Inlet Valve Open 확인 완료!";
					SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);

					///////////////////////////////////////////
					//Slow MFC Inlet Valve Open 후  2 초 대기
					//////////////////////////////////////////
					WaitSec(2);


					///////////////////////////////
					// SLOW MFC Inlet Valve Close
					///////////////////////////////
					ret = g_pIO->Close_SlowVent_Inlet_Valve();
					if (ret != OPERATION_COMPLETED)
					{
						str = "  [LLC Pumping] LLC_FastRough(): LLC Fast Rough 시 Slow MFC Inlet Close 명령 에러 발생";

						g_pLog->Display(0, str);
						g_pLog->Display(0, g_pIO->Log_str);
						SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
						SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
						SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_SlowVent_Inlet_Valve() Return Value :: "));
						 

						return ret;
					}
					SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_SlowVent_Inlet_Valve() Return Value :: "));
					 


					m_start_time_valve = clock();
					m_finish_time_valve = g_pConfig->m_nTimeout_sec_ValveOperation;

					while ((clock() - m_start_time_valve) / CLOCKS_PER_SEC <= m_finish_time_valve)
					{
						if (g_pIO->Is_SlowVent_Inlet_Valve_Open() == VALVE_CLOSED)
							break;

					}
					if (g_pIO->Is_SlowVent_Inlet_Valve_Open() != VALVE_CLOSED)
					{
						str = "  [LLC Pumping] LLC_FastRough(): LLC Fast Rough 시 Slow MFC Inlet Valve Close TimeOut 발생 !";
						g_pLog->Display(0, str);
						SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));

						return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
					}
					str = "  [LLC Pumping] LLC_FastRough(): Slow MFC Inlet Valve Close 확인 완료!";
					SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);

					m_bSlowMfcInlet_Open_State = false;
				}
			}

			if (g_pGauge_IO->GetLlcVacuumRate() < g_pConfig->m_dPressure_ChangeToTMP_Rough)  //0.02 torr
			{
			
				LLC_pressure.Empty();
				LLC_pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
				str = " [LLC Pumping] LLC_FastRough(): LLC Fast Roughing Break 시점 진공 값 확인!";
				SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(LLC_pressure)));
				break;

			}
		}

		if (g_pGauge_IO->GetLlcVacuumRate() >= g_pConfig->m_dPressure_ChangeToTMP_Rough)
		{
			str = " [LLC Pumping] LLC_FastRough(): LLC Fast Roughing Timeout 발생 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			return INTERLOCK_LLC_FASTROUOGH_TIMEOUT_FAIL;
		}
	}


	str = " [LLC Pumping] End Fast Roughing";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	str = " [LLC Pumping] LLC_FastRough():  LLC Pumping시 Fast Rough Valve Close 명령 !!";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	ret = g_pIO->Close_LLC_FastRoughValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [LLC Pumping] LLC_FastRough(): LLC Fast Rough Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_FastRoughValve() Return Value :: "));
		 

		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLC_FastRoughValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_FastRough(): LLC Pumping시 LLC Fast Rough Valve Close 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));

		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
	}

	str = " [LLC Pumping] LLC_FastRough():  LLC Pumping시 Fast Rough Valve Close 확인 완료 !!";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	
	return ret;
}

int CVacuumProcess::LLC_SlowRough()
{
	int ret = SEQUENCE_DONE;

	CString str;

	m_nLlc_slow_rough_time_cnt = 0;

	//1. 상태확인:LLC Gate Valve Close,TR Gate Valve Close,LLC TMP Gate Valve Close,LLC TMP Foreline Valve Close,LLC Fast & Slow Rough Valve Close,Fast & Slow1,2 Vent Valve Close,LLC Lid Close
	//2. LLC 진공도가 환경설정에 기록된 Slow Rough->Fast Rough 전환 진공도보다 낮은지 확인
	//3. 높다면 LLC Slow Rough Valve Open
	//4. LLC 진공도가 환경설정에 기록된 Slow Rough->Fast Rough 전환 진공도에 도달했는지 확인
	//5. LLC Slow Rough Valve Close 확인 후 상태 변경(Fast Rough 완료시 Close해야 되는지 확인 필요)



	str = "  [LLC Pumping] LLC_SlowRough(): LLC TMP Foreline Valve Close !";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	ret = g_pIO->Close_LLC_TMP_ForelineValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = "  [LLC Pumping] LLC_SlowRough(): LLC TMP Foreline Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_TMP_ForelineValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_TMP_ForelineValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_CLOSED)	break;
	}


	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_SlowRough(): LLC Venting시 LLC TMP Foreline Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));

		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFORELINE_CLOSE_CHECK_FAIL;
	}

	str = " [LLC Pumping] LLC_SlowRough(): LLC TMP Foreline Valve Close 완료 !";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  




	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = "  [LLC Pumping] LLC_SlowRough(): LLC Pumping시 LLC Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_SlowRough(): LLC Pumping시 TR Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_TRGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_SlowRough(): LLC Pumping시 LLC TMP Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_SlowRough(): LLC Pumping시 LLC TMP Foreline Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFORELINE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_SlowRough(): LLC Pumping시 LLC Fast Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_SlowRough(): LLC Pumping시 LLC Slow Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCSLOWROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_SlowRough(): LLC Pumping시 Fast Vent Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_SlowRough(): LLC Pumping시 Slow Vent Valve #1 Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
	}
	
	CString LLC_pressure;
	CString pressure;

	LLC_pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());

	str = " [LLC Pumping] LLC_SlowRough(): LLC Pumping 시 Slow Rough 가능 조건 확인 :: [50 Torr 이상 조건]!";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	pressure = _T(" [LLC Pumping] LLC_SlowRough(): LLC Vaccum Value :: ") + LLC_pressure;
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(pressure)));
	g_pLog->Display(0, pressure);

	//
	// 50 torr 이상일 경우, LLC Slow Rough 진행.
	// 50 torr 이하일 경우, Pass
	//
	if (g_pGauge_IO->GetLlcVacuumRate() >= g_pConfig->m_dPressure_ChangeToFast_Rough) //50 torr
	{
		str = " [LLC Pumping] LLC_SlowRough(): 50 torr 진공 이상 값 확인 완료 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		str = " [LLC Pumping] LLC_SlowRough(): LLC Slow Rough Valve Open 명령 실행!";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		ret = g_pIO->Open_LLC_SlowRoughValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = " [LLC Pumping] LLC_SlowRough(): LLC Slow Rough Valve Open 명령 에러 발생 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Open_LLC_SlowRoughValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
			  
			return ret;
		}
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Open_LLC_SlowRoughValve() Return Value :: "));
		 

		g_pLog->Display(0, g_pIO->Log_str);
		  

		str = " [LLC Pumping] LLC_SlowRough(): LLC Slow Rough Valve Open 확인!";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (g_pIO->Is_LLC_SlowRoughValve_Open() == VALVE_OPENED)
				break;
		}

		if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_OPENED)
		{
			str = " [LLC Pumping] LLC_SlowRough(): LLC Pumping시 LLC Slow Rough Valve Open 에러 발생";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			return INTERLOCK_LLCSLOWROUGH_OPEN_CHECK_FAIL;
		}

		str = " [LLC Pumping] Start Slow Roughing";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_LLKSlowRough; // 90초

		str = " [LLC Pumping] LLC_SlowRough(): LLC Slow Rough 50 torr 까지 진행 (90초 : 1분 30초)";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			m_nLlc_slow_rough_time_cnt = ((clock() - m_start_time) / CLOCKS_PER_SEC);
			if (g_pGauge_IO->GetLlcVacuumRate() < g_pConfig->m_dPressure_ChangeToFast_Rough) // 50 torr
			{
				LLC_pressure.Empty();
				LLC_pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
				str = " [LLC Pumping] LLC_SlowRough(): LLC Slow Roughing Break 시점 진공 값 확인!";
				SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(LLC_pressure)));
				break;
			}
		}
	

		if (g_pGauge_IO->GetLlcVacuumRate() >= g_pConfig->m_dPressure_ChangeToFast_Rough)
		{
			str = " [LLC Pumping] LLC_SlowRough(): LLC Slow Roughing Timeout 발생 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			return INTERLOCK_LLC_SLOWROUGH_TIMEOUT_FAIL;
		}
	}


	str = " [LLC Pumping] LLC_SlowRough(): LLC Slow Rough Pumping 완료 !";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	str = " [LLC Pumping] LLC_SlowRough(): Close LLC Slow Rough Valve 명령 !";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	ret = g_pIO->Close_LLC_SlowRoughValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [LLC Pumping] LLC_SlowRough(): LLC Slow Rough Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_SlowRoughValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}

	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_SlowRoughValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLC_SlowRoughValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Pumping] LLC_SlowRough(): LLC Pumping시 LLC Slow Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCSLOWROUGH_CLOSE_CHECK_FAIL;
	}

	str = " [LLC Pumping] LLC_SlowRough(): LLC Slow Rough Valve Close 확인 완료 !";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	
	return ret;
}

int CVacuumProcess::LLC_Pumping_PreWork()
{
	int ret = SEQUENCE_DONE;

	CString str;

	//1. LLC Dry Pump 정상 가동 확인(Run,Warning,Error 상태)
	//2. LLC TMP 정상 가동 확인(Run,Normalization,온도,RPM,Warning,Error 상태)
	//3. LLC Guage 정상 가동 확인
	//4. LLC Gate Valve Close
	//5. TR Gate Valve Close
	//6. LLC TMP Gate Valve Close 
	//7. LLC TMP Foreline Valve Close
	//8. LLC Fast Rough Valve Close
	//9. LLC Slow Rough Valve Close
	//10. LLC Slow Vent Valve1 Close
	//11. LLC Slow Vent Valve2 Close
	//12. LLC Fast Vent Valve Close
	//13. LLC Lid Close 상태 확인 후 상태 변경

	switch (g_pIO->Is_LLC_DryPump_Status()) {
	case DRYPUMP_WARNING:
		str = "LLC_Pumping_PreWork() : LLC Dry Pump Warning 발생 ! ";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return LLC_DRY_PUMP_ALARM_STATE;
	case DRYPUMP_ERROR:
		str = "LLC_Pumping_PreWork() : LLC Dry Pump Error 발생 ! ";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return LLC_DRY_PUMP_ERROR_STATE;
	case DRYPUMP_STOP:
		str = "LLC_Pumping_PreWork() : LLC Dry Pump Stop 상태 ! ";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return LLC_DRY_PUMP_STOP_STATE;
	default:
		str = "LLC_Pumping_PreWork() : LLC Dry Pump State 확인 완료";
		break;
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	switch (g_pLLCTmp_IO->GetLlcTmpState()) //jkseo 리턴값하고 디파인하고 값이 다름
	{
	case TMP_ACCELERATION:
		str = "LLC_Pumping_PreWork() : LLC Turbo Pump 가속 중 ! ";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return LLC_TMP_ACCELERATION_STATE;
	case TMP_DECELERATION:
		str = "LLC_Pumping_PreWork() : LLC Turbo Pump 감속 중 ! ";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return LLC_TMP_DECELERATION_STATE;
	case TMP_OFFLINE:
		str = "LLC_Pumping_PreWork() : LLC Turbo Pump Offline ! ";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return LLC_TMP_OFFLINE_STATE;
	case TMP_ERROR:
		str = "LLC_Pumping_PreWork() : LLC Turbo Pump Error 발생 ! ";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return LLC_TMP_ERROR_STATE;
	default:
		str = "LLC_Pumping_PreWork() : LLC Turbo Pump State 확인 완료";

		break;
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	switch (g_pGauge_IO->GetGaugeState()) {
	case g_pGauge_IO->NotConnected:
		str = "LLC_Pumping_PreWork() : LLC Guage Offline ! ";
		g_pLog->Display(0, str);
		  
		return VACCUM_SENSOR_OFFLINE_STATE;
	case g_pGauge_IO->Error:
		str = "LLC_Pumping_PreWork() : LLC Guage Error 발생 ! ";
		g_pLog->Display(0, str);
		  
		return VACCUM_SENSOR_ERROR_STATE;
	default:
		str = "LLC_Pumping_PreWork() : LLC Guage State 확인 완료";
		break;
	}

	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	str = " LLC_Pumping_PreWork() : LLC Pumping시 Gate Close Start";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	
	ret = g_pIO->Close_LLCGateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " LLC_Pumping_PreWork() : LLC Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLCGateValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLCGateValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  
	ret = g_pIO->Close_TRGateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " LLC_Pumping_PreWork() : TR Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_TRGateValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}

	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_TRGateValve() Return Value :: "));
	 

	g_pLog->Display(0, g_pIO->Log_str);
	  
	ret = g_pIO->Close_LLC_TMP_GateValve();
	if ( ret != OPERATION_COMPLETED)
	{
		str = " LLC_Pumping_PreWork(): LLC TMP Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_TMP_GateValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_TMP_GateValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  
	ret = g_pIO->Close_LLC_TMP_ForelineValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " LLC_Pumping_PreWork() : LLC TMP Foreline Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_TMP_ForelineValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_TMP_ForelineValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  
	ret = g_pIO->Close_LLC_FastRoughValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = "LLC_Pumping_PreWork() :  LLC Fast Rough Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_FastRoughValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_FastRoughValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  
	ret = g_pIO->Close_LLC_SlowRoughValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " LLC_Pumping_PreWork() : LLC Slow Rough Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_SlowRoughValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_SlowRoughValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  
	ret = g_pIO->Close_FastVentValve();
	if ( ret != OPERATION_COMPLETED)
	{
		str = " LLC_Pumping_PreWork() : Fast Vent Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_FastVentValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_FastVentValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  
	ret = g_pIO->Close_SlowVentValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " LLC_Pumping_PreWork() : Slow Vent Valve #1 Close 명령 에러 발생 !";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_SlowVentValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_SlowVentValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  


	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLCGateValve_Open() == VALVE_CLOSED && g_pIO->Is_TRGateValve_Open() == VALVE_CLOSED && g_pIO->Is_LLC_TMP_GateValve_Open() == VALVE_CLOSED
				&& g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_CLOSED && g_pIO->Is_LLC_FastRoughValve_Open() == VALVE_CLOSED && g_pIO->Is_LLC_SlowRoughValve_Open() == VALVE_CLOSED
				&& g_pIO->Is_FastVentValve_Open() == VALVE_CLOSED && g_pIO->Is_SlowVentValve_Open() == VALVE_CLOSED) 	break;
			//&& g_pIO->Is_SlowVentValve2_Open() == VALVE_CLOSED)
		
	}

	str = " LLC_Pumping_PreWork() : LLC Pumping시 Gate Close Check Start";
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_Pumping_PreWork() : LLC Pumping시 LLC Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	{
		str = "LLC_Pumping_PreWork() : LLC Pumping시 TR Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_TRGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = "LLC_Pumping_PreWork() : LLC Pumping시 LLC TMP Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSED)
	{
		str = "LLC_Pumping_PreWork() : LLC Pumping시 LLC TMP Foreline Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFORELINE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = "LLC_Pumping_PreWork() : LLC Pumping시 LLC Fast Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = "LLC_Pumping_PreWork() : LLC Pumping시 LLC Slow Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCSLOWROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = "LLC_Pumping_PreWork() : LLC Venting시 Fast Vent Valve Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = "LLC_Pumping_PreWork() : LLC Pumping시 Slow Vent Valve #1 Close 에러 발생";
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
	}

	

	return ret;
}

int CVacuumProcess::LLC_Venting_PreWork()
{
	int ret = SEQUENCE_DONE;

	char* str;
	//CString str;

	//1. LLC Guage 정상 가동 확인
	//2. LLC Gate Valve 가 이미 열려있는지 확인
	//3. TR Gate Valve Close
	//4. LLC TMP Gate Valve Close 
	//5. LLC TMP Foreline Valve Close
	//6. LLC Fast Rough Valve Close
	//7. LLC Slow Rough Valve Close
	//8. LLC Slow Vent Valve1 Close
	//9. LLC Slow Vent Valve2 Close
	//10. LLC Fast Vent Valve Close
	//11. LLC Lid Close 상태 확인 후 상태 변경


	switch (g_pGauge_IO->GetGaugeState()) {
	case g_pGauge_IO->NotConnected:
		str = " [LLC Venting] LLC_Venting_PreWork(): LLC Guage Offline ! ";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return VACCUM_SENSOR_OFFLINE_STATE;
	case g_pGauge_IO->Error:
		str = "[LLC Venting] LLC_Venting_PreWork(): LLC Guage Error 발생 ! ";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return VACCUM_SENSOR_ERROR_STATE;
	default:
		break;
	}

	ret = g_pIO->Close_LLCGateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = "[LLC Venting] LLC_Venting_PreWork(): LLC Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_LLCGateValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_LLCGateValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLCGateValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_Venting_PreWork(): LLC Gate Valve Open 되어있음 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}

	ret = g_pIO->Close_TRGateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [LLC Venting] LLC_Venting_PreWork(): TR Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_TRGateValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_TRGateValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	 
	ret = g_pIO->Close_LLC_TMP_GateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = "[LLC Venting] LLC_Venting_PreWork(): LLC TMP Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_LLC_TMP_GateValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_LLC_TMP_GateValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  
	ret = g_pIO->Open_LLC_TMP_ForelineValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [LLC Venting] LLC_Venting_PreWork(): LLC TMP Foreline Valve Open 명령 에러 발생 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Open_LLC_TMP_ForelineValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Open_LLC_TMP_ForelineValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  

	ret = g_pIO->Close_LLC_FastRoughValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [LLC Venting] LLC_Venting_PreWork(): LLC Fast Rough Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_LLC_FastRoughValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_LLC_FastRoughValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  

	ret = g_pIO->Close_LLC_SlowRoughValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [LLC Venting] LLC_Venting_PreWork(): LLC Slow Rough Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_LLC_SlowRoughValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_LLC_SlowRoughValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  

	ret = g_pIO->Close_FastVentValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [LLC Venting] LLC_Venting_PreWork(): Fast Vent Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_FastVentValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_FastVentValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  
	ret = g_pIO->Close_SlowVentValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [LLC Venting] LLC_Venting_PreWork(): Slow Vent Valve #1 Close 명령 에러 발생 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_SlowVentValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_SlowVentValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  
	
	
	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation; //10 초

	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		//if (g_pIO->Is_LLCGateValve_Open() == VALVE_CLOSED && g_pIO->Is_TRGateValve_Open() == VALVE_CLOSED && g_pIO->Is_LLC_TMP_GateValve_Open() == VALVE_CLOSED
		//	&& g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_CLOSED && g_pIO->Is_LLC_FastRoughValve_Open() == VALVE_CLOSED && g_pIO->Is_LLC_SlowRoughValve_Open() == VALVE_CLOSED
		//	&& g_pIO->Is_FastVentValve_Open() == VALVE_CLOSED && g_pIO->Is_SlowVentValve_Open() == VALVE_CLOSED && g_pIO->Is_SlowVentValve2_Open() == VALVE_CLOSED)
		//	break;

		if (g_pIO->Is_LLCGateValve_Open() == VALVE_CLOSED && g_pIO->Is_TRGateValve_Open() == VALVE_CLOSED && g_pIO->Is_LLC_TMP_GateValve_Open() == VALVE_CLOSED
			&& g_pIO->Is_LLC_FastRoughValve_Open() == VALVE_CLOSED && g_pIO->Is_LLC_SlowRoughValve_Open() == VALVE_CLOSED && g_pIO->Is_FastVentValve_Open() == VALVE_CLOSED
			&& g_pIO->Is_SlowVentValve_Open() == VALVE_CLOSED && g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_OPENED) 		break;
	}

//	if (g_pIO->Open_SlowVent_Inlet_Valve1() != TRUE)
//	{
//		m_LlcVacuumState = Venting_ERROR;
//		str = " LLC_Venting_PreWork(): Slow Vent Valve Inlet Valve #1 Open 명령 에러 발생 !";
//		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
//		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
//		g_pLog->Display(0, str);
//		g_pLog->Display(0, g_pIO->Log_str);
//
//		return Venting_ERROR;
//	}
//	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
//	g_pLog->Display(0, g_pIO->Log_str);
//
//	if (g_pIO->Is_SlowVent_Inlet_Valve_Open() != VALVE_OPENED)
//	{
//		m_LlcVacuumState = Venting_ERROR;
//		str = " LLC_Venting_PreWork(): LLC Venting시 Slow Vent Valve Inlet Valve #1 Open 에러 발생";
//		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
//		g_pLog->Display(0, str);
//
//		return Venting_ERROR;
//	}
//
//	if (g_pIO->Close_SlowVent_Inlet_Valve() != TRUE)
//	{
//		m_LlcVacuumState = Venting_ERROR;
//		str = " LLC_Venting_PreWork(): Slow Vent Valve Inlet Valve #1 Close 명령 에러 발생 !";
//		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
//		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
//		g_pLog->Display(0, str);
//		g_pLog->Display(0, g_pIO->Log_str);
//
//		return Venting_ERROR;
//	}
//	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
//	g_pLog->Display(0, g_pIO->Log_str);
//

//	if (g_pIO->Is_SlowVent_Inlet_Valve_Open() != VALVE_CLOSED)
//	{
//		m_LlcVacuumState = Venting_ERROR;
//		str = " LLC_Venting_PreWork(): LLC Venting시 Slow Vent Valve Inlet Valve #1 Close 에러 발생";
//		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
//		g_pLog->Display(0, str);
//
//		return Venting_ERROR;
//	}
	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_Venting_PreWork(): LLC Venting시 LLC Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_Venting_PreWork(): LLC Venting시 TR Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_TRGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_Venting_PreWork(): LLC Venting시 LLC TMP Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " LLC_Venting_PreWork(): LLC Venting시 LLC TMP Foreline Valve Open 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFORELINE_OPEN_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_Venting_PreWork(): LLC Venting시 LLC Fast Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_Venting_PreWork(): LLC Venting시 LLC Slow Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCSLOWROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_Venting_PreWork(): LLC Venting시 Fast Vent Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_Venting_PreWork(): LLC Venting시 Slow Vent Valve #1 Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
	}

	

	return ret;
}

int CVacuumProcess::LLC_SlowVent()
{
	char* str;
	CString LLC_Pressure;

	int ret = SEQUENCE_DONE;

	m_nLlc_standby_vent_time_cnt = 0;

	//1. 상태확인:LLC Gate Valve Close,TR Gate Valve Close,LLC TMP Gate Valve Close,LLC TMP Foreline Valve Open,LLC Fast & Slow Rough Valve Close,Fast & Slow1, Vent Valve Close
	//2. LLC 진공도가 환경설정에 기록된 Slow Vent1 전환 진공도보다 높은지 확인
	//3. 낮다면 Slow Vent Valve #1 Open
	//4. LLC 진공도가 환경설정에 기록된 Slow Vent1->Fast Vent 전환 진공도에 도달했는지 확인
	//5. Slow Vent Valve #1 Close 후 상태 변경
		  
	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 LLC Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 TR Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_TRGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 LLC TMP Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 LLC TMP Foreline Valve Open 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFORELINE_OPEN_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 LLC Fast Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 LLC Slow Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCSLOWROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 Fast Vent Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 Slow Vent Valve #1 Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
	}

	str = "[LLC Venting]  LLC_SlowVent(): LLC TMP Gate Valve Close !";
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, g_pIO->Log_str);

	ret = g_pIO->Close_LLC_TMP_GateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC TMP Gate Valve Close 명령 에러 발생 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_LLC_TMP_GateValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		  
		return ret;
	}
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_LLC_TMP_GateValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);
	  
	//20210811 jkseo, event handler로 밸브 클로즈 명령시 리셋하고 실제로 클로즈되면 셋을 해줌... 근데 다시 시간체크???
	m_start_time = clock();
	//m_finish_time = 10;
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;

	while (((clock() - m_start_time) / CLOCKS_PER_SEC) <= m_finish_time)
	{
		if (g_pIO->Is_LLC_TMP_GateValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 LLC TMP Gate Valve Open 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}
	str = " [LLC Venting] LLC_SlowVent(): LLC TMP Gate Valve Close 확인 완료!";
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	str = " [LLC Venting] LLC_SlowVent(): LLC TMP Gate Valve Close 후 대기 Venting 중 !";
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	///////////////////////////////////////////////////////
	// Version 1
	// 10분동안 8x10^-4 torr 까지 대기 Venting 진행  
	///////////////////////////////////////////////////////

	///////////////////////////////////////////////////////
	// Version 2
	// 10분동안 5x10^-5 torr 까지 대기 Venting 진행
	// 추가 사항
	// - 대기 벤팅 동시에 Slow Vent Inlet Open 후 Close 동작 추가
	///////////////////////////////////////////////////////


	//////////////////////////////////////////////
	// LLC Venting 시 대기 벤팅 시간을 줄이기 위함.
	// Slow Vent Inlet Open 후 바로 Close
	/////////////////////////////////////////////
	//if (g_pIO->Open_SlowVent_Inlet_Valve1() != OPERATION_COMPLETED)
	//{
	//	m_LlcVacuumState = Venting_ERROR;
	//	str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 Slow Vent Inlet Open 명령 에러 발생";
	//
	//	g_pLog->Display(0, str);
	//	g_pLog->Display(0, g_pIO->Log_str);
	//	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	//
	//	return Venting_ERROR;
	//}
	//m_start_time = clock();
	//m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	//
	//while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	//{
	//	if (g_pIO->Is_SlowVent_Inlet_Valve_Open() == VALVE_OPENED)
	//		break;
	//
	//}
	//if (g_pIO->Is_SlowVent_Inlet_Valve_Open() != VALVE_OPENED)
	//{
	//	m_LlcVacuumState = Venting_ERROR;
	//	str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 Slow Vent Inlet Valve Open Time Out 에러 발생";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//
	//	return Venting_ERROR;
	//}
	//str = " [LLC Venting] LLC_SlowVent(): Slow Vent Inlet Valve Open 확인 완료!";
	//SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//g_pLog->Display(0, str);
	//
	//WaitSec(1);
	//
	//if (g_pIO->Close_SlowVent_Inlet_Valve() != OPERATION_COMPLETED)
	//{
	//	m_LlcVacuumState = Venting_ERROR;
	//	str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 Slow Vent Inlet Close 명령 에러 발생";
	//
	//	g_pLog->Display(0, str);
	//	g_pLog->Display(0, g_pIO->Log_str);
	//	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	//
	//	return Venting_ERROR;
	//}
	//m_start_time = clock();
	//m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	//
	//while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	//{
	//	if (g_pIO->Is_SlowVent_Inlet_Valve_Open() == VALVE_CLOSED)
	//		break;
	//
	//}
	//if (g_pIO->Is_SlowVent_Inlet_Valve_Open() != VALVE_CLOSED)
	//{
	//	m_LlcVacuumState = Venting_ERROR;
	//	str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 Slow Vent Inlet Valve Close TimeOut 발생 !";
	//	g_pLog->Display(0, str);
	//	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//
	//	return Venting_ERROR;
	//}
	//str = " [LLC Venting] LLC_SlowVent(): Slow Vent Inlet Valve Close 확인 완료!";
	//SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//g_pLog->Display(0, str);



	/////////////////////////////////////////////////////////
	// Slow Vent 진행 전 대기Venting 범위 확인
	// 5x10^-5 까지의 진공 값 확인 후 Slow Venting 으로 넘어감
	/////////////////////////////////////////////////////////
	m_start_time = clock();
	//m_finish_time = 600;
	m_finish_time = g_pConfig->m_nTimeout_sec_LLKStandbyVent; //600s (10분)
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		m_nLlc_standby_vent_time_cnt = ((clock() - m_start_time) / CLOCKS_PER_SEC );
		if (g_pGauge_IO->GetLlcVacuumRate() > g_pConfig->m_dPressure_ChangeToVent_LLC)
		{
			LLC_Pressure.Empty();
			LLC_Pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
			str = "  [LLC Venting] LLC_SlowVent(): LLC 대기 Venting Break 시점 진공 값 확인 !";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)((LLC_Pressure))));
			break;
		}

		
		
	}

	if (g_pGauge_IO->GetLlcVacuumRate() <= g_pConfig->m_dPressure_ChangeToVent_LLC)
	{
		str = " [LLC Venting] LLC_SlowVent(): Slow Vent Tmp Gate Valve Close 후 대기 venting Timeout 발생 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLC_STANDBYVENTING_TIMEOUT_FAIL;
	}

	str = " [LLC Venting] LLC_SlowVent(): LLC TMP Gate Valve Close 후 대기 Venting 완료 !";
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 LLC Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_SlowVent(): LLC Venting시 TR Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_TRGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = "[LLC Venting] LLC_SlowVent(): LLC Venting시 LLC TMP Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));

		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL;
	}

	//FORELINE OPEN VENTING.
	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 LLC TMP Foreline Valve Open 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFORELINE_OPEN_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 LLC Fast Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = "[LLC Venting] LLC_SlowVent(): LLC Venting시 LLC Slow Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCSLOWROUGH_CLOSE_CHECK_FAIL;
	}


	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 Fast Vent Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
	}


	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 Slow Vent Valve #1 Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
	}

	m_nLlc_slow_vent_time_cnt = 0;

	double mfc_ch1 = 0 * ANALOG_OUTPUT_UNIT_CONVERSION;
	unsigned long long pBuffer = mfc_ch1;
	unsigned long long p_ret = 0x00;
	   
	if (g_pGauge_IO->GetLlcVacuumRate() <= g_pConfig->m_dPressure_ChangeToFast_Vent) // 10 torr
	{
		//g_pIO->WriteOutputData(0, &pBuffer, 2);
		g_pIO->WriteOutputData(0, &p_ret, 2);

		str = " [LLC Venting] LLC_SlowVent(): LLC Venting 시 진공도 10 Torr 보다 작음 확인 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		str = " [LLC Venting] LLC_SlowVent(): Slow Vent Valve Open !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		ret = g_pIO->Open_SlowVentValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = " [LLC Venting] LLC_SlowVent(): Slow Vent Valve #1 Open 명령 에러 발생 !";
			g_pLog->Display(0, str);
			  
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Open_SlowVentValve1() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
			return ret;
		}
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Open_SlowVentValve1() Return Value :: "));
		 
		g_pLog->Display(0, g_pIO->Log_str);

		//20210811 jkseo, 위에서 이미 대기 함...
		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (g_pIO->Is_SlowVentValve_Open() == VALVE_OPENED)
			{
				//g_pIO->WriteOutputData(0, &pBuffer, 2);
				g_pIO->WriteOutputData(0, &p_ret, 2);
				break;
			}
			
		}

		if (g_pIO->Is_SlowVentValve_Open() != VALVE_OPENED)
		{
			str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 Slow Vent Valve #1 Open 에러 발생";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			return INTERLOCK_SLOWVENT_OPEN_CHECK_FAIL;
		}
		str = "[LLC Venting] LLC_SlowVent(): Slow Vent Valve Open 완료 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		
		str = " [LLC Venting] Start Slow Venting";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_LLKSlow1Vent; // 120 초
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			m_nLlc_slow_vent_time_cnt = ((clock() - m_start_time) / CLOCKS_PER_SEC);
			if (g_pGauge_IO->GetLlcVacuumRate() < 0.0001)
			{
				mfc_ch1 = 0.1 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch1;
				g_pIO->WriteOutputData(0, &pBuffer, 2);
			}
			else if ((g_pGauge_IO->GetLlcVacuumRate() > 0.0001) && (g_pGauge_IO->GetLlcVacuumRate() < 0.001))
			{
				mfc_ch1 = 0.1 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch1;
				g_pIO->WriteOutputData(0, &pBuffer, 2);
			}
			else if ((g_pGauge_IO->GetLlcVacuumRate() > 0.001) && (g_pGauge_IO->GetLlcVacuumRate() < 0.01))
			{
				mfc_ch1 = 0.1 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch1;
				g_pIO->WriteOutputData(0, &pBuffer, 2);
			}
			else if ((g_pGauge_IO->GetLlcVacuumRate() > 0.01) && (g_pGauge_IO->GetLlcVacuumRate() < 0.1))
			{
				mfc_ch1 = 1.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch1;
				g_pIO->WriteOutputData(0, &pBuffer, 2);
			}
			else if ((g_pGauge_IO->GetLlcVacuumRate() > 0.1) && (g_pGauge_IO->GetLlcVacuumRate() < 0.4))
			{
				mfc_ch1 = 3.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch1;
				g_pIO->WriteOutputData(0, &pBuffer, 2);
			}
			else if ((g_pGauge_IO->GetLlcVacuumRate() > 0.4) && (g_pGauge_IO->GetLlcVacuumRate() <= 1))
			{
				mfc_ch1 = 5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch1;
				g_pIO->WriteOutputData(0, &pBuffer, 2);
			}
			else if (g_pGauge_IO->GetLlcVacuumRate() > g_pConfig->m_dPressure_ChangeToSlow2_Vent) // 1 torr
			//else if (g_pGauge_IO->GetLlcVacuumRate() > 1) // 0.1 torr
			{
				LLC_Pressure.Empty();
				LLC_Pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
				str = " [LLC Venting] LLC_SlowVent(): LLC Slow Venting Break 시점 진공 값 확인 !";
				SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)((LLC_Pressure))));
				break;
			}
	

			//20210811 jkseo, 요기서 질소 부족 체크?????
		}



		if (g_pGauge_IO->GetLlcVacuumRate() <= g_pConfig->m_dPressure_ChangeToSlow2_Vent)
		{
			str = " [LLC Venting] LLC_SlowVent(): Slow Vent Valve #1 Timeout 발생 !";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			return INTERLOCK_LLC_SLOWVENTING_TIMEOUT_FAIL;
		}

		str = " [LLC Venting] LLC_SlowVent(): LLC Slow Venting 완료";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		str = " [LLC Venting] LLC_SlowVent(): LLC Slow Venting Close !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		ret = g_pIO->Close_SlowVentValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = " [LLC Venting] LLC_SlowVent(): Slow Vent Valve #1 Close 명령 에러 발생 !";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_SlowVentValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
			  
			return ret;
		}
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_SlowVentValve() Return Value :: "));
		 
		g_pLog->Display(0, g_pIO->Log_str);
		  
		

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (g_pIO->Is_SlowVentValve_Open() == VALVE_CLOSED)
				break;
		}

		if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
		{
			str = " [LLC Venting] LLC_SlowVent(): LLC Venting시 Slow Vent Valve #1 Close 에러 발생";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
		}

		str = " [LLC Venting] LLC_SlowVent(): LLC Slow Venting Close 확인 완료!";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		str = " [LLC Venting] LLC_SlowVent(): LLC Slow MFC#1 Close !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		if (!(g_pIO->WriteOutputData(0, &p_ret, 2)))
		{
			str = " [LLC Venting] LLC_SlowVent(): LLC Venting 시 Slow MFC#1 OFF 완료!!";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
		}
		else
		{
			str = " [LLC Venting] LLC_SlowVent(): LLC Venting 시 Slow MFC OFF 에러 발생 !!";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
		}


		//if (g_pIO->Open_SlowVentValve2() != TRUE)
		//{
		//	m_LlcVacuumState = Venting_ERROR;
		//	str = " LLC_SlowVent(): Slow Vent Valve #2 Open 명령 에러 발생 !";
		//	return Venting_ERROR;
		//}
		//
		//m_start_time = clock();
		//m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		//while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		//{
		//	if (g_pIO->Is_SlowVentValve2_Open() == VALVE_OPENED)
		//		break;
		//}
		//
		//if (g_pIO->Is_SlowVentValve_Open() != VALVE_OPENED)
		//{
		//	m_LlcVacuumState = Venting_ERROR;
		//	str = " LLC_SlowVent(): LLC Venting시 Slow Vent Valve #1 Open 에러 발생";
		//	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		//	return Venting_ERROR;
		//}
		//
		//str = " Start Slow Venting";
		//m_start_time = clock();
		//m_finish_time = g_pConfig->m_nTimeout_sec_LLKSlow2Vent;
		//while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		//{
		//	if (g_pGauge_IO->GetLlcVacuumRate() > g_pConfig->m_dPressure_ChangeToFast_Vent)
		//		break;
		//}
		//
		//if (g_pGauge_IO->GetLlcVacuumRate() <= g_pConfig->m_dPressure_ChangeToFast_Vent)
		//{
		//	m_LlcVacuumState = Venting_ERROR;
		//	str = " LLC_SlowVent(): Slow Vent Valve #2 Timeout 발생 !";
		//	return Venting_ERROR;
		//}


		//if (g_pIO->Close_SlowVentValve2() != TRUE)
		//{
		//	m_LlcVacuumState = Venting_ERROR;
		//	str = " LLC_Venting_PreWork(): Slow Vent Valve #2 Close 명령 에러 발생 !";
		//	return Venting_ERROR;
		//}

	//	m_start_time = clock();
	//	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	//	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	//	{
	//		if (g_pIO->Is_SlowVentValve_Open() == VALVE_CLOSED && g_pIO->Is_SlowVentValve2_Open() == VALVE_CLOSED)
	//			break;
	//	}
	//
	//	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	//	{
	//		m_LlcVacuumState = Venting_ERROR;
	//		str = " LLC_SlowVent(): LLC Venting시 Slow Vent Valve #1 Close 에러 발생";
	//		return Venting_ERROR;
	//	}
	//	if (g_pIO->Is_SlowVentValve2_Open() != VALVE_CLOSED)
	//	{
	//		m_LlcVacuumState = Venting_ERROR;
	//		str = " LLC_SlowVent(): LLC Venting시 Slow Vent Valve #2 Close 에러 발생";
	//		return Venting_ERROR;
	//	}
	}

	str = " [LLC Venting] LLC_SlowVent(): LLC Slow Vent Sequence 완료 !";
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	
	return ret;
}

int CVacuumProcess::LLC_FastVent()
{
	CString str, LLC_Pressure;

	int ret = SEQUENCE_DONE;

	m_nLlc_fast_vent_time_cnt = 0;

	//1. 상태확인:LLC Gate Valve Close,TR Gate Valve Close,LLC TMP Gate Valve Close,LLC TMP Foreline Valve Open, LLC Fast & Slow Rough Valve Close, Fast & Slow1 Vent Valve Close
	//2. LLC 진공도가 환경설정에 기록된 Fast Rough-> TMP Rough 전환 진공도보다 낮은지 확인
	//3. 높다면 LLC Fast Rough Valve Open
	//4. LLC 진공도가 환경설정에 기록된 Fast Rough->TMP Rough 전환 진공도에 도달했는지 확인
	//5. LLC Fast Rough Valve Close 확인 후 상태 변경

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_FastVent(): LLC Venting시 LLC Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_TRGateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_FastVent(): LLC Venting시 TR Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_TRGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_FastVent(): LLC Venting시 LLC TMP Gate Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCTMPGATE_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_OPENED)
	{
		str = " [LLC Venting] LLC_FastVent(): LLC Venting시 LLC TMP Foreline Valve Open 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFORELINE_OPEN_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = "[LLC Venting]  LLC_FastVent(): LLC Venting시 LLC Fast Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCFASTROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_FastVent(): LLC Venting시 LLC Slow Rough Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_LLCSLOWROUGH_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " [LLC Venting] LLC_FastVent(): LLC Venting시 Fast Vent Valve Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
	}
	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = "[LLC Venting] LLC_FastVent(): LLC Venting시 Slow Vent Valve #1 Close 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return INTERLOCK_SLOWVENT_CLOSE_CHECK_FAIL;
	}

	double mfc_ch2 = 0.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
	unsigned long long pBuffer = mfc_ch2;
	unsigned long long p_ret = 0x00;

	if (g_pGauge_IO->GetLlcVacuumRate() >= g_pConfig->m_dPressure_ChangeToSlow2_Vent && g_pGauge_IO->GetLlcVacuumRate() <= g_pConfig->m_dPressure_Vent_End) // 1torr ~ 760 torr
	{
		g_pIO->WriteOutputData(2, &pBuffer, 2);

		str = " [LLC Venting] LLC_FastVent(): LLC Venting 시 진공도 1torr 보다 크고 760Torr 보다 작음 확인 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		str = "[LLC Venting]  LLC_FastVent(): LLC Venting 시 FastVentValve Open  !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		ret = g_pIO->Open_FastVentValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = " [LLC Venting] LLC_FastVent(): Fast Vent Valve Open 명령 에러 발생 !";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Open_FastVentValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
			  
			return ret;
		}
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Open_FastVentValve() Return Value :: "));
		 
		g_pLog->Display(0, g_pIO->Log_str);
		  


		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (g_pIO->Is_FastVentValve_Open() == VALVE_OPENED)
				break;
		}

		if (g_pIO->Is_FastVentValve_Open() != VALVE_OPENED)
		{
			str = " [LLC Venting] LLC_FastVent(): LLC Venting시 Fast Vent Valve Open 에러 발생";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			return INTERLOCK_FASTVENT_OPEN_CHECK_FAIL;
		}


		str = " [LLC Venting] LLC_FastVent(): LLC Venting 시 FastVentValve Open 확인 완료 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);


		str = "[LLC Venting]  Start Fast Venting";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_LLKFastVent; // 120 초
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			m_nLlc_fast_vent_time_cnt = ((clock() - m_start_time) / CLOCKS_PER_SEC);
			if (g_pGauge_IO->GetLlcVacuumRate() < 1)
			{
				mfc_ch2 = 0.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}
			if ((g_pGauge_IO->GetLlcVacuumRate() > 1) && (g_pGauge_IO->GetLlcVacuumRate() < 10))
			{
				mfc_ch2 = 1 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}
			if ((g_pGauge_IO->GetLlcVacuumRate() > 10) && (g_pGauge_IO->GetLlcVacuumRate() < 50))
			{
				mfc_ch2 = 1.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}
			if ((g_pGauge_IO->GetLlcVacuumRate() > 50) && (g_pGauge_IO->GetLlcVacuumRate() <= 100))
			{
				mfc_ch2 = 2 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}
			if ((g_pGauge_IO->GetLlcVacuumRate() > 100) && (g_pGauge_IO->GetLlcVacuumRate() <= 200))
			{
				mfc_ch2 = 2.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}
			if ((g_pGauge_IO->GetLlcVacuumRate() > 200) && (g_pGauge_IO->GetLlcVacuumRate() <= 300))
			{
				mfc_ch2 = 3 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}
			if ((g_pGauge_IO->GetLlcVacuumRate() > 300) && (g_pGauge_IO->GetLlcVacuumRate() <= 400))
			{
				mfc_ch2 = 3.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				g_pIO->WriteOutputData(2, &pBuffer, 2);
			}
			if ((g_pGauge_IO->GetLlcVacuumRate() > 400) && (g_pGauge_IO->GetLlcVacuumRate() <= 550))
			{
				mfc_ch2 = 4 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				int ret = g_pIO->WriteOutputData(2, &pBuffer, 2);
			}
			if ((g_pGauge_IO->GetLlcVacuumRate() > 550) && (g_pGauge_IO->GetLlcVacuumRate() <= 770))
			{
				mfc_ch2 = 4.5 * ANALOG_OUTPUT_UNIT_CONVERSION;
				pBuffer = mfc_ch2;
				int ret = g_pIO->WriteOutputData(2, &pBuffer, 2);
			}
			if (g_pGauge_IO->GetLlcVacuumRate() > g_pConfig->m_dPressure_Vent_End) //760 torr 
			{
				LLC_Pressure.Empty();
				LLC_Pressure.Format("%f", g_pGauge_IO->GetLlcVacuumRate());
				str = "  [LLC Venting] LLC_FastVent(): LLC Fast Venting Break 시점 진공 값 확인 !";
				SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
				SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)((LLC_Pressure))));
				break;
			}
			
			//20210811 jkseo, 질소 체크
		
		}



		if (g_pGauge_IO->GetLlcVacuumRate() <= g_pConfig->m_dPressure_Vent_End)
		{
			str = " [LLC Venting] LLC_FastVent(): Fast Vent Timeout 발생 !";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			return INTERLOCK_LLC_FASTVENTING_TIMEOUT_FAIL;
		}

		str = " [LLC Venting] LLC_FastVent(): LLC Fast Venting 완료 !!";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  

		str = " [LLC Venting] LLC_FastVent(): LLC Fast Vent Valve Close !!";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		ret = g_pIO->Close_FastVentValve();
		if (ret != OPERATION_COMPLETED)
		{
			str = " [LLC Venting] LLC_FastVent(): Fast Vent Valve Close 명령 에러 발생 !";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_FastVentValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
			  
			return ret;
		}
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_FastVentValve() Return Value :: "));
		 
		g_pLog->Display(0, g_pIO->Log_str);
		  

		m_start_time = clock();
		m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
		while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
		{
			if (g_pIO->Is_FastVentValve_Open() == VALVE_CLOSED)
				break;
		}

		if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
		{
			str = " [LLC Venting] LLC_FastVent(): LLC Venting시 Fast Vent Valve Close 에러 발생 !";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
			return INTERLOCK_FASTVENT_CLOSE_CHECK_FAIL;
		}


		str = " [LLC Venting] LLC_FastVent(): LLC Fast Vent Valve Close 확인 완료!!";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);


		str = " [LLC Venting] LLC_FastVent(): LLC Fast MFC#2 Valve Close!!";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		if (!(g_pIO->WriteOutputData(2, &p_ret, 2)))
		{
			str = " [LLC Venting] LLC_FastVent(): LLC Venting 시 Fast MFC#2 OFF 완료!!";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
		}
		else
		{
			str = " [LLC Venting] LLC_FastVent(): LLC Venting 시 Fast MFC OFF 에러 발생 !!";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			g_pLog->Display(0, str);
			  
		}
	}

	str = "[LLC Venting] LLC_FastVent(): LLC Fast Vent Sequence 완료 !";
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	  
	/* 임시 */
	g_pIO->Open_LLCGateValve();

	return ret;
}

int CVacuumProcess::LLC_Venting_Complete()
{
	char* str;
	//CString str;

	int ret = SEQUENCE_DONE;

	CString standby_vent_time;
	CString slow_vent_time;
	CString fast_vent_time;
	CString strVacuumRate;

	standby_vent_time.Empty();
	slow_vent_time.Empty();
	fast_vent_time.Empty();

	//str = " [LLC Venting] LLC_Venting_Complete(): LLC Venting 완료!!";
	//SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	//g_pLog->Display(0, str);

	// LLC GAUGE 대기 값 확인.
	str = " [LLC Venting] LLC_Venting_Complete(): 현재 LLC Chamber 대기 값 (Torr) !!";
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	strVacuumRate.Format(_T("%.2e"), g_pGauge_IO->GetLlcVacuumRate());
	g_pLog->Display(0, strVacuumRate);

	if (g_pGauge_IO->GetLlcVacuumRate() < 750)
	{
		str = " [LLC Venting] LLC_Venting_Complete(): 750 torr 이하 Venting 미 완료 에러 발생!";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);

		return  INTERLOCK_VACUUMVALUE_CHECK_FAIL;
	}


	str = " [LLC Venting] LLC_Venting_Complete(): LLC 41B 센서 확인 (LLC ATM SENSOR) !!";
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);
	//1. LLC 41B SENSOR CHECK ( 저진공 )
	if (g_pIO->Is_LLC_Atm_Check() == ATM_STATE)
	{
		str = "  [LLC Venting] LLC_Venting_Complete(): LLC ATM SENSOR STATUS 에러 발생!";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		  
		return  INTERLOCK_VACUUMVALUE_CHECK_FAIL;
	}

	str = " [LLC Venting] LLC_Venting_Complete(): LLC 41B 센서 (LLC ATM SENSOR) 확인 완료!!";
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);


	str = " [LLC Venting] LLC_Venting_Complete(): Venting 완료에 따른 LLC Gate Open!!!!!";
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	//2. LLC Gate Valve Open
	ret = g_pIO->Open_LLCGateValve();
	if (ret != OPERATION_COMPLETED)
	{
		str = " [LLC Venting] LLC_Venting_Complete(): LLC Gate Valve Open 명령 에러 발생 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Open_LLCGateValve() Return Value :: "));
		 
		g_pLog->Display(0, str);
		g_pLog->Display(0, g_pIO->Log_str);
		return  ret;
	}

	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Open_LLCGateValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);


	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLCGateValve_Open() == VALVE_OPEN)
			break;
	}

	if (g_pIO->Is_LLCGateValve_Open() != VALVE_OPENED)
	{
		str = " [LLC Venting] LLC_Venting_Complete(): LLC Venting Complete 시 LLC Gate Open 에러 발생 !";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
		return INTERLOCK_LLCGATE_OPEN_CHECK_FAIL;
	}

	str = " [LLC Venting] LLC_Venting_Complete(): Venting 완료에 따른 LLC Gate Open 확인 완료!!!!!";
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	str = " [LLC Venting] LLC_Venting_Complete(): VENTING 완료 !!!!!";
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);

	standby_vent_time.Format("%d", m_nLlc_standby_vent_time_cnt);
	slow_vent_time.Format("%d", m_nLlc_slow_vent_time_cnt);
	fast_vent_time.Format("%d", m_nLlc_fast_vent_time_cnt);

	SaveLogFile("SREM_Sequence_Time_Report", _T((LPSTR)(LPCTSTR)(" LLC Venting Sequence Time :: Standby Venting [ " + standby_vent_time + " ] , Slow Venting [ " + slow_vent_time + "  ] , Fast Venting [ " + fast_vent_time + " ] ")));

	g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
	g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
	//g_pWarning->ShowWindow(SW_HIDE);

	return ret;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////// LLC Pumping or Venting Thread End ///////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


bool CVacuumProcess::TR_Gate_Open_Check()
{
	bool ret = true;

	double m_mc_Pressure = 0.0;
	double m_llc_Presure = 0.0;
	double m_mc_llc_presure = 0.0;
	double m_mc_llc_presure2 = 0.0;

	double abs_presure = 0.0;

	m_mc_Pressure = (g_pGauge_IO->GetMcVacuumRate());
	m_llc_Presure = (g_pGauge_IO->GetLlcVacuumRate());
	m_mc_llc_presure = (m_mc_Pressure - m_llc_Presure);
	m_mc_llc_presure2 = (m_llc_Presure - m_mc_Pressure);


	/////////////////////////////////////////////////////////////
	// abs_presure = LLC Vacuum 값 - MC Vacuum 값 의 절대 값
	/////////////////////////////////////////////////////////////

	abs_presure = fabs(m_mc_llc_presure2);

	////////////////////////////////////////////
	// 0보다 큰 진공 차이 값.
	// LLC 와 MC 차이 값이 10 이상일 경우 FALSE
	////////////////////////////////////////////
	if (0.0 < m_mc_Pressure)
	{
		if (abs_presure > 30)
			ret = false;
	}
	//////////////////////////////////////////////
	// 0보다 작은 진공 차이 값.
	// LLC 와 MC 차이 값이 기준치 벗어나면 FALSE
	// MC 값 기준으로 차이 값 비교.
	// Ex)
	// MC : 0.001 , LLC : 0.3
	// 차이 값 : 0.299 -------> return false
	// MC : 0.001 ,LLC : 0.09
	// 차이 값 : 0.089 -------> return true
	/////////////////////////////////////////////

	else if (0.0 > m_mc_Pressure)
	{
		if (m_mc_Pressure > 0.1)
		{
			if (abs_presure > 0.9)
				ret = false;
		}
		else if (0.1 > m_mc_Pressure > 0.001)
		{
			if (abs_presure > 0.099)
				ret = false;
		}
		else if (0.001 > m_mc_Pressure > 0.00001)
		{
			if (abs_presure > 0.00099)
				ret = false;
		}
		else if (0.00001 > m_mc_Pressure > 0.0000001)
		{
			if (abs_presure > 0.0000099)
				ret = false;
		}
		else if (0.0000001 > m_mc_Pressure > 0.000000001)
		{
			if (abs_presure > 0.000000099)
				ret = false;
		}
	}

	return ret;
}

/*

질소 유량 체크 함수 추가

void CVacuumProcess::N2Gas_Flow_Check()
{
	CString str;
	CString nCh_5_str;
	CString nCh_6_str;
	CString nCh_5_str_sccm;
	CString nCh_6_str_sccm;
	CString nCh_5_out_str_sccm;
	CString nCh_6_out_str_sccm;

	unsigned long long mfc_ret = 0x00;

	double nCh_5_diff;
	double nCh_6_diff;

	double nCh_5 = (double)(g_pIO->m_nDecimal_Analog_in_ch_5) / 1638.0; // #mfc#2 N2 10 sccm
	double nCh_6 = (double)(g_pIO->m_nDecimal_Analog_in_ch_6) / 1638.0; // #mfc#1 N2 20000 sccm

	double nCh_5_out = (double)(g_pIO->m_nDecimal_Analog_out_ch_1) / 409.5; // #mfc#2 N2 10 sccm
	double nCh_6_out = (double)(g_pIO->m_nDecimal_Analog_out_ch_2) / 409.5; // #mfc#1 N2 20000 sccm


	double nCh_5_MFC2 = nCh_5 * 2;
	double nCh_6_MFC1 = nCh_6 * 4000;

	double nCh_5_MFC2_OUT = nCh_5_out * 2;
	double nCh_6_MFC1_OUT = nCh_6_out * 4000;



	// (MFC 입력값 - MFC 출력값)
	nCh_5_diff = (nCh_5_MFC2_OUT - nCh_5_MFC2);
	nCh_6_diff = (nCh_6_MFC1_OUT - nCh_6_MFC1);

	//nCh_5_diff = (nCh_5_MFC2 - nCh_5_MFC2_OUT);
	//nCh_6_diff = (nCh_6_MFC1 - nCh_6_MFC1_OUT);

	nCh_5_str.Format("%0.2f", nCh_5);
	nCh_6_str.Format("%0.2f", nCh_6);

	nCh_5_str_sccm.Format("%0.2f", nCh_5_MFC2);
	nCh_6_str_sccm.Format("%0.1f", nCh_6_MFC1);

	nCh_5_out_str_sccm.Format("%0.2f", nCh_5_MFC2_OUT);
	nCh_6_out_str_sccm.Format("%0.1f", nCh_6_MFC1_OUT);


	// Sequence 시 MFC 유량 Log 
	if ((m_bVacuumThreadStop == FALSE) || (m_b_MC_VacuumThreadStop == FALSE)) // MC 또는 LLC sequence 가 실행 중이라면..
	{
		if ((m_McVacuumState == MC_Venting_SLOWVENT) || (m_McVacuumState == MC_Venting_FASTVENT) || (m_LlcVacuumState == Venting_SLOWVENT) || (m_LlcVacuumState == Venting_FASTVENT))
		{
			SaveLogFile("SLOW_MFC_Log", _T((LPSTR)(LPCTSTR)(" Input : " + nCh_5_str_sccm + " Output : " + nCh_5_out_str_sccm))); // input mfc
			SaveLogFile("FAST_MFC_Log", _T((LPSTR)(LPCTSTR)(" Input : " + nCh_6_str_sccm + " Output : " + nCh_6_out_str_sccm))); // input mfc
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 질소 유량 적을 시 알람 띄우고 멈춤 함수
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////
	// SLOW MFC 유량 확인
	///////////////////////

	if ((SLOW_MFC_LIMIT < nCh_5_diff))
	{
		Cnt_Error_Slow_MFC++;
		if (CHECK_MFC_NUM < Cnt_Error_Slow_MFC)
		{
			if (g_pIO->Is_SlowVentValve_Open() == VALVE_OPENED)
			{
				str = " Slow Vent Error :: 질소 유량 부족에 따른 Error 발생";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				str = " Slow Vent Error :: 질소 유량 부족에 따른 Slow Vent Valve Close";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				/////////////////////////////////////
				// 별도의 ERROR SEQUENCE 추가 해야함.
				/////////////////////////////////////
				if (g_pIO->Close_SlowVentValve() != TRUE)
				{
					str = " Slow Vent Error :: 질소 확인 ERROR 에 따른 SLOW VENT VALVE CLOSE 실패";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);
				}
				g_pLog->Display(0, g_pIO->Log_str);

				if (g_pVP->m_bVacuumThreadStop == FALSE) // LLC sequence 가 실행 중이라면..
				{
					str = " Slow Vent Error :: 질소 유량 부족에 따른 LLC Sequence 정지";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

					g_pVP->m_pVaccumThread->SuspendThread();
					DWORD dwResult;
					::GetExitCodeThread(g_pVP->m_pVaccumThread, &dwResult);

					delete g_pVP->m_pVaccumThread;
					g_pVP->m_pVaccumThread = NULL;

					m_McVacuumState = Pumping_ERROR;

					str = " [LLC Venting] Slow Vent Error :: 질소 유량 부족에 따른 LLC Vacuum Sequence 정지";
					SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);

					//m_VentingState = FALSE;

					//g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
					//g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
				}
				else if (g_pVP->m_b_MC_VacuumThreadStop == FALSE) // MC sequence 가 실행 중이라면..
				{
					str = " Slow Vent Error :: 질소 유량 부족에 따른 MC Sequence 정지";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

					g_pVP->m_p_MC_VaccumThread->SuspendThread();
					DWORD dwResult;
					::GetExitCodeThread(g_pVP->m_p_MC_VaccumThread, &dwResult);

					delete g_pVP->m_p_MC_VaccumThread;
					g_pVP->m_p_MC_VaccumThread = NULL;
					g_pVP->m_Mc_Vacuum_State = g_pVP->MC_Pumping_ERROR;

					str = " [MC Venting] Slow Vent Error :: 질소 유량 부족에 따른 MC Vacuum Sequence 정지";
					SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);
					g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
				}

				str = " Slow Vent Error :: 질소 유량 부족에 따른 Slow MFC Module 정지";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				g_pIO->WriteOutputData(0, &mfc_ret, 2);  //Slow Mfc 유량 0 으로 reset
				::AfxMessageBox("질소 부족 Slow Vent Error", MB_ICONERROR);
				Cnt_Error_Slow_MFC = 0;
			}
			else
			{
				SetDlgItemText(IDC_SLOW_MFC_STATE, "ERROR : Valve Line 확인 필요");
				str = " Slow Vent Error :: Valve Line 확인 필요 ";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
				Cnt_Error_Slow_MFC = 0;
				//m_VentingState = FALSE;
			}
		}
	}
	else
	{
		SetDlgItemText(IDC_SLOW_MFC_STATE, "정상 작동 중");
		Cnt_Error_Slow_MFC = 0;
		//m_VentingState = TRUE;
	}


	/////////////////////////
	//// FAST MFC 유량 확인
	/////////////////////////

	if ((FAST_MFC_LIMIT < nCh_6_diff))
	{
		Cnt_Error_Fast_MFC++;
		if (CHECK_MFC_NUM < Cnt_Error_Fast_MFC)
		{
			if (g_pIO->Is_FastVentValve_Open() == VALVE_OPENED)
			{
				SetDlgItemText(IDC_FAST_MFC_STATE, "ERROR : 질소 확인 필요");
				str = " Fast Vent Error :: 질소 유량 부족에 따른 Error 발생";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				str = " Fast Vent Error :: 질소 유량 부족에 따른 Fast Vent Valve Close";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				/////////////////////////////////////////////////
				// Fast Vent Valve 가 Open 되어 있다면 Close.
				/////////////////////////////////////////////////
				if (g_pIO->Close_FastVentValve() != OPERATION_COMPLETED)
				{
					str = " Fast Vent Error :: 질소 확인 ERROR 에 따른 FAST VENT VALVE CLOSE 실패";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
				}
				g_pLog->Display(0, g_pIO->Log_str);

				/////////////////////////////////////////////////
				// LLC sequence 가 실행 중이라면..Sequence 종료
				/////////////////////////////////////////////////
				if (g_pVP->m_bVacuumThreadStop == FALSE)
				{

					str = " Fast Vent Error :: 질소 유량 부족에 따른 LLC Sequence 정지";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

					g_pVP->m_pVaccumThread->SuspendThread();
					DWORD dwResult;
					::GetExitCodeThread(g_pVP->m_pVaccumThread, &dwResult);

					delete g_pVP->m_pVaccumThread;
					g_pVP->m_pVaccumThread = NULL;
					g_pVP->m_Vacuum_State = g_pVP->Pumping_ERROR;

					str = " [LLC Venting] Fast Vent Error :: 질소 유량 부족에 따른 LLC Vacuum Sequence 정지";
					SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);
					//m_VentingState = FALSE;
					g_pMaindialog->GetDlgItem(IDC_LLC_PUMP)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_LLC_VENT)->EnableWindow(true);
				}
				/////////////////////////////////////////////////
				// MC sequence 가 실행 중이라면...Sequence 종료
				/////////////////////////////////////////////////
				else if (g_pVP->m_b_MC_VacuumThreadStop == FALSE)
				{

					str = " Fast Vent Error :: 질소 유량 부족에 따른 MC Sequence 정지";
					g_pLog->Display(0, str);
					CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

					g_pVP->m_p_MC_VaccumThread->SuspendThread();
					DWORD dwResult;
					::GetExitCodeThread(g_pVP->m_p_MC_VaccumThread, &dwResult);

					delete g_pVP->m_p_MC_VaccumThread;
					g_pVP->m_p_MC_VaccumThread = NULL;
					g_pVP->m_Mc_Vacuum_State = g_pVP->MC_Pumping_ERROR;

					str = " [MC Venting] Fast Vent Error :: 질소 유량 부족에 따른 MC Vacuum Sequence 정지";
					SaveLogFile("MC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
					g_pLog->Display(0, str);

					g_pMaindialog->GetDlgItem(IDC_MC_PUMP)->EnableWindow(true);
					g_pMaindialog->GetDlgItem(IDC_MC_VENT)->EnableWindow(true);
				}


				str = " Fast Vent Error :: 질소 유량 부족에 따른 Fast MFC Module 정지";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));

				g_pIO->WriteOutputData(2, &mfc_ret, 2); //Fast Mfc 유량 0 으로 reset
				::AfxMessageBox("질소 부족 Fast Vent Error", MB_ICONERROR);
				Cnt_Error_Fast_MFC = 0;
			}
			else
			{
				SetDlgItemText(IDC_FAST_MFC_STATE, "ERROR : Valve Line 확인 필요");
				str = " Fast Vent Error :: Valve Line 확인 필요 ";
				g_pLog->Display(0, str);
				CECommon::SaveLogFile("N2__Event__Log", _T((LPSTR)(LPCTSTR)(str)));
				Cnt_Error_Fast_MFC = 0;
			}
		}
	}
	else
	{
		SetDlgItemText(IDC_FAST_MFC_STATE, "정상 작동 중");
		Cnt_Error_Fast_MFC = 0;
	}


	if (nCh_5 < 0.5)
	{
		SetDlgItemText(IDC_SLOW_MFC_STATE, "동작 대기 중");
	}


	if (nCh_6 < 0.5)
	{
		SetDlgItemText(IDC_FAST_MFC_STATE, "동작 대기 중");
	}

}


*/


int CVacuumProcess::LLCVentingSequenceStop()
{
	int	ret = 0;
	char* str;

	/* Slow Vent Valve Close */
	ret = g_pIO->Close_SlowVentValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_SlowVentValve();
		if (ret != 0)
		{
			str = " LLCVentingSequenceStop(): Stop 후 Slow Vent Valve Open 명령 에러 발생 !";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_SlowVentValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}
	SaveLogFile("LC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_SlowVentValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_SlowVentValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_SlowVentValve_Open() != VALVE_CLOSED)
	{
		str = "  LLC_Venting_Error(): Stop 후 Slow Vent Valve Close Time Out 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
	}

	/* Fast Vent Valve Close */
	ret = g_pIO->Close_FastVentValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_FastVentValve();
		if (ret != 0)
		{
			str = "  LLC_Venting_Error(): Stop 후 Fast Vent Valve Open 명령 에러 발생 !";
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_FastVentValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}

	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T("Close_FastVentValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);

	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_FastVentValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_FastVentValve_Open() != VALVE_CLOSED)
	{
		str = " LLC_Venting_Error(): Stop 후 Fast Vent Valve Close Time Out 에러 발생";
		SaveLogFile("LLC_Venting_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
		g_pLog->Display(0, str);
	}

	/*SLOW MFC OFF*/
	unsigned long long mfc_off = 0x00;
	g_pIO->WriteOutputData(0, &mfc_off, 2);
	/*FAST MFC OFF*/
	g_pIO->WriteOutputData(2, &mfc_off, 2);
	return ret;
}


int CVacuumProcess::LLCPumpingSequenceStop()
{
	int ret = 0;
	char* str;


	/*Slow Rough Valve Close */
	ret = g_pIO->Close_LLC_SlowRoughValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_LLC_SlowRoughValve();
		if (ret != 0)
		{
			str = " LLCPumpingSequenceStop() : Stop 후 LLC Slow Rough Valve Close 명령 에러 발생 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_SlowRoughValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_SlowRoughValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);


	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	//m_finish_time = 10;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLC_SlowRoughValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_LLC_SlowRoughValve_Open() != VALVE_CLOSED)
	{
		str = "LLCPumpingSequenceStop() : Stop 후 LLC Slow Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	}

	/*Fast Rough Valve Close */
	ret = g_pIO->Close_LLC_FastRoughValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_LLC_FastRoughValve();
		if (ret != 0)
		{
			str = " LLCPumpingSequenceStop() : Stop 후 LLC Fast Rough Valve Close 명령 에러 발생 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_FastRoughValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	g_pLog->Display(0, g_pIO->Log_str);


	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	//m_finish_time = 10;
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLC_FastRoughValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_LLC_FastRoughValve_Open() != VALVE_CLOSED)
	{
		str = " LLCPumpingSequenceStop() : Stop 후 LLC Fast Rough Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	}


	/*Foreline Valve Close */
	ret = g_pIO->Close_LLC_TMP_ForelineValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_LLC_TMP_ForelineValve();
		if (ret != 0)
		{
			str = " LLCPumpingSequenceStop() : Stop 후 LLC Foreline Valve Close 명령 에러 발생 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_TMP_ForelineValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_TMP_ForelineValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);


	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;
	
	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLC_TMP_ForelineValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_LLC_TMP_ForelineValve_Open() != VALVE_CLOSED)
	{
		str = "LLCPumpingSequenceStop() : Stop 후 LLC Foreline Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	}

	/*Tmp Gate Valve Close */
	ret = g_pIO->Close_LLC_TMP_GateValve();
	if (ret != 0)
	{
		ret = g_pIO->Close_LLC_TMP_GateValve();
		if (ret != 0)
		{
			str = " LLCPumpingSequenceStop() : Stop 후 LLC Tmp Gate Valve Close 명령 에러 발생 !";
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
			SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_TMP_GateValve() Return Value :: "));
			 
			g_pLog->Display(0, str);
			g_pLog->Display(0, g_pIO->Log_str);
		}
	}
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(g_pIO->Log_str)));
	SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T("Close_LLC_TMP_GateValve() Return Value :: "));
	 
	g_pLog->Display(0, g_pIO->Log_str);


	m_start_time = clock();
	m_finish_time = g_pConfig->m_nTimeout_sec_ValveOperation;

	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		if (g_pIO->Is_LLC_TMP_GateValve_Open() == VALVE_CLOSED)
			break;
	}

	if (g_pIO->Is_LLC_TMP_GateValve_Open() != VALVE_CLOSED)
	{
		str = "LLCPumpingSequenceStop() : Stop 후 LLC Tmp Gate Valve Close 에러 발생";
		g_pLog->Display(0, str);
		SaveLogFile("LLC_Pumping_Auto_Sequence_Log", _T((LPSTR)(LPCTSTR)(str)));
	}
	return ret;
}

void CVacuumProcess::EmergencyStop()
{
	//시퀀스 정지 동작
	TRACE(_T("VP START\n"));
	WaitSec(5);
	TRACE(_T("VP END\n"));
}
