#pragma once

#include "CDeviceManager.h"
#include <mil.h>
#include <iostream>
#include <fstream>
#include "atltypes.h"
#include "afxdialogex.h"
#include "resource.h"
#include "General_define.h"
#include "Error_define.h"
#include "ColorStaticST.h"
#include "ProcessData.h"
#include "CPasswordDlg.h"
#include "CCameraWnd.h"
#include "CCameraDlg.h"
#include "CIODlg.h"
#include "CLogDisplayDlg.h"
#include "CMaskEFEMDlg.h"
#include "CTurboPumpDlg.h"
#include "CTurboPumpMCDlg.h"
#include "CVacuumGaugeDlg.h"
#include "CVacuumRobotDlg.h"
#include "CConfigurationEditorDlg.h"
#include "CRecipeEditorDlg.h"
#include "CProcessEditorDlg.h"
#include "CHWCommStatusDlg.h"
#include "CSystemTestDlg.h"
#include "CAutoProcess.h"
#include "CVacuumProcess.h"
#include "CWarningDlg.h"
#include "CMessageDlg.h"
#include "CAutoMessageDlg.h"
#include "CAlarmDlg.h"
#include "CSubMenuDlg.h"
#include "CAnimationGUI.h"
#include "CEUVSourceDlg.h"
#include "CNavigationStageDlg.h"
#include "MaskMapWnd.h"
#include "CMaskMapDlg.h"
#include "EUVPTR.h"
#include "EUVPTRDlg.h"
#include "CMaindialog.h"
#include "CMirrorShiftDlg.h"
#include "CLoadingScreenDlg.h"
#include "CChartdirDlg.h"
#include "CChartdirLineDlg.h"
#include "CChartdirStageDlg.h"
#include "CFilterStageDlg.h"
//#include "CXRayCameraDlg.h"
//#include "CXRayCameraConfigDlg.h"
#include "CCamZoneplateDlg.h"
//#include "CPhaseDlg.h"
#include "CLightControllerDlg.h"
#include "CPTRDlg.h"
#include "XLEzAutomation.h"
#include "XLAutomation.h"
#include "CSubTestMenuDlg.h"
#include "CNavigationStageTestDlg.h"
#include "CSourceTestDlg.h"
//#include "CBeamConnect.h"
#include "CMacro.h"

#include "CSqOneManuaDlg.h"
#include "CSqOneConfigDlg.h"
#include "CSQOneControlDlg.h"
#include "CSQOneDataBaseDlg.h"
#include "CBeamAlignMainDlg.h"
#include "CBeamAutoAlignDlg.h"
#include "CBeamSearch1DDlg.h"
#include "CBeamSearch2DDlg.h"
#include "CBeamAlignConfigDlg.h"
#include "CPlasmaCleanerDlg.h"
#include "CSubModuleDlg.h"
//#include "sqlite3.h"