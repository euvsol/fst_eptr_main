﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

#include "..\..\CommonModule\DllADAM\CADAMAlgorithm.h"		
#include "..\..\CommonModule\DllADAM\CADAMAlgorithm.cpp"
#include <random>

// CPTRDlg 대화 상자
enum _ResultData
{
	Transmitance = 6,
	Reflectance,
	BackgroundDetector1,
	BackgroundDetector2,
	BackgroundDetector3,
	ReferenceDetector1,
	ReferenceDetector2,
	ReferenceDetector3,
	WithoutPellicleDetector1,
	WithoutPellicleDetector2,
	WithoutPellicleDetector3,
	WithPellicleDetector1,
	WithPellicleDetector2,
	WithPellicleDetector3,
};

IMPLEMENT_DYNAMIC(CPTRDlg, CDialogEx)

CPTRDlg::CPTRDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PTR_DIALOG, pParent)
	, m_nPTRDataNum(1000)
	, mBackGroundN(10000)
	, mReadRefReflectanceN(10000)
	, mMeasureWithOutPellicleN(10000)
	, mMeasureWithPellicleN(1000)
	, m_strMousePosition(_T("Mouse position"))
	, m_LutPercentMinCurrent(0)
	, m_LutPercentMaxCurrent(0)
	, m_bAutoScaleImage(TRUE)
	, m_CurrentMean(0)
	, m_UpscaleRatio(10)
	, m_IsUpScale(FALSE)
{


}

CPTRDlg::~CPTRDlg()
{

}

void CPTRDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_DATA_NUM, m_nPTRDataNum);
	DDX_Text(pDX, IDC_EDIT_BACKGROUND_N, mBackGroundN);
	DDX_Text(pDX, IDC_EDIT_READ_REFERENCE_REFLECTANCE_N, mReadRefReflectanceN);
	DDX_Text(pDX, IDC_EDIT_WITHOUT_PELLICLE_N, mMeasureWithOutPellicleN);
	DDX_Text(pDX, IDC_EDIT_WITH_PELLICLE_N, mMeasureWithPellicleN);

	DDX_Text(pDX, IDC_EDIT_D1, g_pAdam->mD1);
	DDX_Text(pDX, IDC_EDIT_D2, g_pAdam->mD2);
	DDX_Text(pDX, IDC_EDIT_D3, g_pAdam->mD3);
	DDX_Text(pDX, IDC_EDIT_D1_STD, g_pAdam->mD1Std);
	DDX_Text(pDX, IDC_EDIT_D2_STD, g_pAdam->mD2Std);
	DDX_Text(pDX, IDC_EDIT_D3_STD, g_pAdam->mD3Std);

	DDX_Text(pDX, IDC_EDIT_PTR_TRANSMITTANCE, g_pAdam->mT);
	DDX_Text(pDX, IDC_EDIT_PTR_REFLECTANCE, g_pAdam->mR);

	DDX_Text(pDX, IDC_EDIT_PTR_MOUSE_POSITION, m_strMousePosition);

	DDX_Text(pDX, IDC_EDIT_PTR_LUT_LOW, m_LutPercentMinCurrent);
	DDX_Text(pDX, IDC_EDIT_PTR_LUT_HIGH, m_LutPercentMaxCurrent);

	DDX_Check(pDX, IDC_CHECK_PTR_AUTO_SCALE, m_bAutoScaleImage);
	DDX_Control(pDX, IDC_COMBO_SELECT_COUPON, m_comboCouponSelect);

	DDX_Text(pDX, IDC_EDIT_PTR_SCAN_X_START, m_XStart_um);
	DDX_Text(pDX, IDC_EDIT_PTR_SCAN_Y_START, m_YStart_um);
	DDX_Text(pDX, IDC_EDIT_PTR_SCAN_X_END, m_XEnd_um);
	DDX_Text(pDX, IDC_EDIT_PTR_SCAN_Y_END, m_YEnd_um);
	DDX_Text(pDX, IDC_EDIT_PTR_SCAN_X_DISTANCE, m_XDistance_um);
	DDX_Text(pDX, IDC_EDIT_PTR_SCAN_Y_DISTANCE, m_YDistance_um);

	DDX_Text(pDX, IDC_EDIT_PTR_SCAN_X_INTERVAL, m_XInterval_um);
	DDX_Text(pDX, IDC_EDIT_PTR_SCAN_Y_INTERVAL, m_YInterval_um);

	DDX_Text(pDX, IDC_EDIT_PTR_SCAN_X_NUM_OF_GRID, m_NumOfXGrid);
	DDX_Text(pDX, IDC_EDIT_PTR_SCAN_Y_NUM_OF_GRID, m_NumOfYGrid);

	DDX_Text(pDX, IDC_EDIT_AUTO_SCALE_WIDTH, m_AutoScaleWidth);
	DDX_Text(pDX, IDC_EDIT_CURRENT_MEAN, m_CurrentMean);
	DDX_Text(pDX, IDC_EDIT_UPSCALE, m_UpscaleRatio);
	DDX_Check(pDX, IDC_CHECK_UPSCALE, m_IsUpScale);
}


BEGIN_MESSAGE_MAP(CPTRDlg, CDialogEx)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_PTR_TEST, &CPTRDlg::OnBnClickedButtonPtrTest)
	ON_BN_CLICKED(IDC_BUTTON_STAGE_BUFFER_RUN, &CPTRDlg::OnBnClickedButtonStageBufferRun)
	ON_BN_CLICKED(IDC_BUTTON_PTR_GET_BACKGROUND, &CPTRDlg::OnBnClickedButtonPtrGetBackground)
	ON_BN_CLICKED(IDC_BUTTON_PTR_READ_SENSOR, &CPTRDlg::OnBnClickedButtonPtrReadSensor)
	ON_BN_CLICKED(IDC_BUTTON_PTR_READ_REF_REFLECTANCE, &CPTRDlg::OnBnClickedButtonPtrReadRefReflectance)
	ON_BN_CLICKED(IDC_BUTTON_PTR_MEASURE_WITHOUT_PELLICLE, &CPTRDlg::OnBnClickedButtonPtrMeasureWithoutPellicle)
	ON_BN_CLICKED(IDC_BUTTON_PTR_MEASURE_WITH_PELLICLE, &CPTRDlg::OnBnClickedButtonPtrMeasureWithPellicle)
	ON_BN_CLICKED(IDC_BUTTON_CALCULATE_PTR, &CPTRDlg::OnBnClickedButtonCalculatePtr)
	ON_BN_CLICKED(IDC_BUTTON_PTR_CLEAR_GRID, &CPTRDlg::OnBnClickedButtonPtrClearGrid)
	ON_BN_CLICKED(IDC_BUTTON_PTR_GET_REF_POS, &CPTRDlg::OnBnClickedButtonPtrGetRefPos)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_READ_REFERENCE, &CPTRDlg::OnBnClickedButtonReadReference)
	ON_BN_CLICKED(IDC_BUTTON_SAVE_REFERENCE, &CPTRDlg::OnBnClickedButtonSaveReference)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_FIT, &CPTRDlg::OnBnClickedButtonFit)
	ON_BN_CLICKED(IDC_BUTTON_PTR_SET_SCALE, &CPTRDlg::OnBnClickedButtonPtrSetScale)
	ON_BN_CLICKED(IDC_CHECK_PTR_AUTO_SCALE, &CPTRDlg::OnBnClickedCheckPtrAutoScale)
	ON_BN_CLICKED(IDC_BUTTON_PTR_ALGORITHM_TEST, &CPTRDlg::OnBnClickedButtonPtrAlgorithmTest)
	ON_BN_CLICKED(IDC_BUTTON_PTR_DISP_TEST, &CPTRDlg::OnBnClickedButtonPtrDispTest)
	ON_BN_CLICKED(IDC_BUTTON3, &CPTRDlg::OnBnClickedButton3)
	ON_CBN_SELCHANGE(IDC_COMBO_SELECT_COUPON, &CPTRDlg::OnSelchangeComboSelectCoupon)
	ON_BN_CLICKED(IDC_BUTTON_PTR_SCAN_START, &CPTRDlg::OnBnClickedButtonPtrScanStart)
	ON_BN_CLICKED(IDC_BUTTON_PTR_SCAN_SET, &CPTRDlg::OnBnClickedButtonPtrScanSet)
	ON_BN_CLICKED(IDC_BUTTON_PTR_SCAN_SET_FROM_RECIPE, &CPTRDlg::OnBnClickedButtonPtrScanSetFromRecipe)
	ON_BN_CLICKED(IDC_BUTTON_PTR_TEST3, &CPTRDlg::OnBnClickedButtonPtrTest3)
	ON_EN_CHANGE(IDC_EDIT_AUTO_SCALE_WIDTH, &CPTRDlg::OnEnChangeEditAutoScaleWidth)
	ON_BN_CLICKED(IDC_BUTTON1, &CPTRDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON_UPSCALE, &CPTRDlg::OnBnClickedButtonUpscale)
	ON_BN_CLICKED(IDC_CHECK_UPSCALE, &CPTRDlg::OnBnClickedCheckUpscale)	
END_MESSAGE_MAP()


// CPTRDlg 메시지 처리기


BOOL CPTRDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	g_pAdam->MemoryAllocation();

	MilInitialize();

	InitResultGrid();
	InitStageGrid();
	InitSensorGrid();
	SensorGridUpdate();

	// Reference Data update from config file
	ReadRefDataFromConfig();

	GetDlgItem(IDC_EDIT_PTR_LUT_LOW)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDIT_PTR_LUT_HIGH)->EnableWindow(FALSE);
	//GetDlgItem(IDC_BUTTON_PTR_SET_SCALE)->EnableWindow(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CPTRDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CPTRDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CDialogEx::OnTimer(nIDEvent);
}


void CPTRDlg::InitResultGrid()
{
	CRect rect;
	GetDlgItem(IDC_TEXT_PTR_RESULT_GRID)->GetWindowRect(rect);
	ScreenToClient(&rect);
	m_PTRResultGrid.Create(rect, this, IDC_TEXT_PTR_RESULT_GRID);

	int nGap, col, row;
	col = 0;
	row = 0;
	nGap = 8;

	m_PTRResultGrid.SetEditable(false);
	m_PTRResultGrid.SetListMode(true);
	m_PTRResultGrid.SetSingleRowSelection(false);
	m_PTRResultGrid.EnableDragAndDrop(false);
	m_PTRResultGrid.SetRowCount(1);	//추후 변경. 측정 영역 개수
	m_PTRResultGrid.SetFixedRowCount(1);
	m_PTRResultGrid.SetColumnCount(20);	//추후 변경. 결과 항목 개수
	m_PTRResultGrid.SetFixedColumnCount(1);
	m_PTRResultGrid.SetBkColor(RGB(192, 192, 192));
	m_PTRResultGrid.GetDefaultCell(FALSE, FALSE)->SetBackClr(WHITE);
	//m_PTRResultGrid.SetRedraw(TRUE, TRUE);
	m_PTRResultGrid.ShowScrollBar(SB_HORZ, TRUE);

	LOGFONT lf;
	CFont Font;
	m_PTRResultGrid.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	Font.CreateFontIndirect(&lf);
	m_PTRResultGrid.SetFont(&Font);
	Font.DeleteObject();

	CString str;
	str.Format("%s", "No");
	m_PTRResultGrid.SetItemText(0, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Coupon");
	m_PTRResultGrid.SetItemText(0, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Region");
	m_PTRResultGrid.SetItemText(0, 2, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Type");
	m_PTRResultGrid.SetItemText(0, 3, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Pos X");
	m_PTRResultGrid.SetItemText(0, 4, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Pos Y");
	m_PTRResultGrid.SetItemText(0, 5, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Transmitance");
	m_PTRResultGrid.SetItemText(0, Transmitance, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Reflectance");
	m_PTRResultGrid.SetItemText(0, Reflectance, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "BackgroundDetector1");
	m_PTRResultGrid.SetItemText(0, BackgroundDetector1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "BackgroundDetector2");
	m_PTRResultGrid.SetItemText(0, BackgroundDetector2, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "BackgroundDetector3");
	m_PTRResultGrid.SetItemText(0, BackgroundDetector3, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "ReferenceDetector1");
	m_PTRResultGrid.SetItemText(0, ReferenceDetector1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "ReferenceDetector2");
	m_PTRResultGrid.SetItemText(0, ReferenceDetector2, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "ReferenceDetector3");
	m_PTRResultGrid.SetItemText(0, ReferenceDetector3, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "WithoutPellicleDetector1");
	m_PTRResultGrid.SetItemText(0, WithoutPellicleDetector1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "WithoutPellicleDetector2");
	m_PTRResultGrid.SetItemText(0, WithoutPellicleDetector2, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "WithoutPellicleDetector3");
	m_PTRResultGrid.SetItemText(0, WithoutPellicleDetector3, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "WithPellicleDetector1");
	m_PTRResultGrid.SetItemText(0, WithPellicleDetector1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "WithPellicleDetector2");
	m_PTRResultGrid.SetItemText(0, WithPellicleDetector2, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "WithPellicleDetector3");
	m_PTRResultGrid.SetItemText(0, WithPellicleDetector3, (LPSTR)(LPCTSTR)str);

	//m_PTRResultGridRowCnt = 1;

	m_PTRResultGrid.SetRowHeight(0, 35);
	m_PTRResultGrid.SetColumnWidth(0, 100);
	m_PTRResultGrid.SetColumnWidth(1, 100);
	m_PTRResultGrid.SetColumnWidth(2, 100);
	m_PTRResultGrid.SetColumnWidth(3, 100);
	m_PTRResultGrid.SetColumnWidth(4, 100);
	m_PTRResultGrid.SetColumnWidth(5, 100);
	m_PTRResultGrid.SetColumnWidth(6, 100);
	m_PTRResultGrid.SetColumnWidth(7, 100);
	m_PTRResultGrid.SetColumnWidth(8, 100);
	m_PTRResultGrid.SetColumnWidth(9, 100);
	m_PTRResultGrid.SetColumnWidth(10, 100);
	m_PTRResultGrid.SetColumnWidth(11, 100);
	m_PTRResultGrid.SetColumnWidth(12, 100);
	m_PTRResultGrid.SetColumnWidth(13, 100);
	m_PTRResultGrid.SetColumnWidth(14, 100);
	m_PTRResultGrid.SetColumnWidth(15, 100);
	m_PTRResultGrid.SetColumnWidth(16, 100);
	m_PTRResultGrid.SetColumnWidth(17, 100);
	m_PTRResultGrid.SetColumnWidth(18, 100);
	m_PTRResultGrid.SetColumnWidth(19, 100);
	//m_PTRResultGrid.ExpandColumnsToFit();
}

void CPTRDlg::addResultGrid(int nCurrentCoupon,int nCurrentMeasure)
{
	if (nCurrentCoupon <= 0 || nCurrentMeasure <= 0)
	{
		return;
	}
	_MeasurePointList test = g_pMaskMap->m_MaskMapWnd.m_ProcessData.GetCoupons()[nCurrentCoupon - 1].GetPoints()[nCurrentMeasure - 1];
	int nGridIndex = m_PTRResultGrid.GetRowCount();
	CString strTemp = "";
	strTemp.Format(_T("%d"), nGridIndex);
	m_PTRResultGrid.InsertRow(strTemp);
	strTemp.Format(_T("%03d"), nCurrentCoupon);
	m_PTRResultGrid.SetItemText(nGridIndex, 1, strTemp);
	strTemp.Format(_T("%03d"), nCurrentMeasure);
	m_PTRResultGrid.SetItemText(nGridIndex, 2, strTemp);
	strTemp.Format(_T("%03.4f"), test.CenterPosX/1000);
	m_PTRResultGrid.SetItemText(nGridIndex, 3, strTemp);
	strTemp.Format(_T("%03.4f"), test.CenterPosY/1000);
	m_PTRResultGrid.SetItemText(nGridIndex, 4, strTemp);
	strTemp.Format(_T("%03.4f"),(double)100 / 1000);
	m_PTRResultGrid.SetItemText(nGridIndex, 5, strTemp);
	m_PTRResultGrid.Refresh();
}

void CPTRDlg::InitStageGrid()
{
	CRect rect;
	GetDlgItem(IDC_TEXT_PTR_STAGE_GRID)->GetWindowRect(rect);
	ScreenToClient(&rect);
	m_StageGrid.Create(rect, this, IDC_TEXT_PTR_STAGE_GRID);

	int nGap, col, row;
	col = 3;
	row = 5;

	m_StageGrid.SetEditable(false);
	m_StageGrid.SetListMode(true);
	m_StageGrid.SetSingleRowSelection(false);
	m_StageGrid.EnableDragAndDrop(false);
	m_StageGrid.SetFixedRowCount(1);
	m_StageGrid.SetFixedColumnCount(1);
	m_StageGrid.SetColumnCount(col);	//추후 변경. 결과 항목 개수
	m_StageGrid.SetRowCount(row);	//추후 변경. 측정 영역 개수
	m_StageGrid.SetBkColor(RGB(192, 192, 192));
	m_StageGrid.GetDefaultCell(FALSE, FALSE)->SetBackClr(WHITE);

	LOGFONT lf;
	CFont Font;
	m_StageGrid.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	Font.CreateFontIndirect(&lf);
	m_StageGrid.SetFont(&Font);
	Font.DeleteObject();

	CString str;

	str.Format("%s", "Coordinate");
	m_StageGrid.SetItemText(0, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "X(mm)");
	m_StageGrid.SetItemText(0, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Y(mm)");
	m_StageGrid.SetItemText(0, 2, (LPSTR)(LPCTSTR)str);

	str.Format("%s", "Reference");
	m_StageGrid.SetItemText(1, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Absolute");
	m_StageGrid.SetItemText(2, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Mask");
	m_StageGrid.SetItemText(3, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Relative");
	m_StageGrid.SetItemText(4, 0, (LPSTR)(LPCTSTR)str);

	m_StageGrid.SetRowHeight(0, 35);
	m_StageGrid.SetColumnWidth(0, 70);
	m_StageGrid.SetColumnWidth(1, 70);
	m_StageGrid.SetColumnWidth(2, 70);
	m_StageGrid.ExpandColumnsToFit();
}


void CPTRDlg::InitSensorGrid()
{
	CRect rect;
	GetDlgItem(IDC_TEXT_PTR_SENSOR_GRID)->GetWindowRect(rect);
	ScreenToClient(&rect);
	m_SensorGrid.Create(rect, this, IDC_TEXT_PTR_SENSOR_GRID);

	int nGap, col, row;
	row = 5;
	col = 7;

	m_SensorGrid.SetEditable(false);
	m_SensorGrid.SetListMode(true);
	m_SensorGrid.SetSingleRowSelection(false);
	m_SensorGrid.EnableDragAndDrop(false);
	m_SensorGrid.SetFixedRowCount(1);
	m_SensorGrid.SetFixedColumnCount(1);
	m_SensorGrid.SetColumnCount(col);	//추후 변경. 결과 항목 개수
	m_SensorGrid.SetRowCount(row);	//추후 변경. 측정 영역 개수
	m_SensorGrid.SetBkColor(RGB(192, 192, 192));
	m_SensorGrid.GetDefaultCell(FALSE, FALSE)->SetBackClr(WHITE);

	LOGFONT lf;
	CFont Font;
	m_SensorGrid.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	Font.CreateFontIndirect(&lf);
	m_SensorGrid.SetFont(&Font);
	Font.DeleteObject();

	CString str;
	str.Format("%s", "D1");
	m_SensorGrid.SetItemText(0, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "D2");
	m_SensorGrid.SetItemText(0, 2, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "D3");
	m_SensorGrid.SetItemText(0, 3, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "D1(std)");
	m_SensorGrid.SetItemText(0, 4, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "D2(std)");
	m_SensorGrid.SetItemText(0, 5, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "D3(std)");
	m_SensorGrid.SetItemText(0, 6, (LPSTR)(LPCTSTR)str);


	str.Format("%s", "BackGround");
	m_SensorGrid.SetItemText(1, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Reference");
	m_SensorGrid.SetItemText(2, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Without Pellicle");
	m_SensorGrid.SetItemText(3, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "With Pellicle");
	m_SensorGrid.SetItemText(4, 0, (LPSTR)(LPCTSTR)str);

	m_SensorGrid.SetRowHeight(0, 35);
	m_SensorGrid.SetColumnWidth(0, 50);
	m_SensorGrid.SetColumnWidth(1, 50);
	m_SensorGrid.SetColumnWidth(2, 50);
	m_SensorGrid.SetColumnWidth(3, 50);
	m_SensorGrid.SetColumnWidth(4, 50);
	m_SensorGrid.SetColumnWidth(5, 50);
	m_SensorGrid.SetColumnWidth(6, 50);

	m_SensorGrid.ExpandColumnsToFit();
}



void CPTRDlg::WriteGrid(double x, double y, double T, double R)
{
	CString str;

	str.Format("%d", m_PTRResultGridRowCnt);
	m_PTRResultGrid.SetItemText(m_PTRResultGridRowCnt, 0, str);

	str.Format("%4.6f", x);
	m_PTRResultGrid.SetItemText(m_PTRResultGridRowCnt, 1, str);

	str.Format("%4.6f", y);
	m_PTRResultGrid.SetItemText(m_PTRResultGridRowCnt, 2, str);

	str.Format("%0.4f", T);
	m_PTRResultGrid.SetItemText(m_PTRResultGridRowCnt, 3, str);

	str.Format("%0.4f", R);
	m_PTRResultGrid.SetItemText(m_PTRResultGridRowCnt, 4, str);



	m_PTRResultGrid.Invalidate(FALSE);

	CCellID currentCell;

	//Select Row
	m_PTRResultGrid.SetSelectedRange(m_PTRResultGridRowCnt, 1, m_PTRResultGridRowCnt, mNumOfGridCol - 1, false, true);

	// Visible 이동	
	m_PTRResultGrid.EnsureVisible(m_PTRResultGridRowCnt, 1);
	m_PTRResultGridRowCnt++;
}

void CPTRDlg::ClearGrid()
{
	m_PTRResultGrid.SetEditable(true);

	CCellRange Selection;
	Selection.SetMinRow(1);
	Selection.SetMaxRow(m_PTRResultGridRowCnt - 1);

	Selection.SetMinCol(0);
	Selection.SetMaxCol(m_PTRResultGrid.GetColumnCount() - 1);

	m_PTRResultGrid.ClearCells(Selection);

	m_PTRResultGrid.SetEditable(false);

	//DeSelect Row
	m_PTRResultGrid.SetSelectedRange(-1, -1, -1, -1);
	m_PTRResultGrid.SetFocusCell(-1, -1);

	m_PTRResultGridRowCnt = 1;

	m_PTRResultGrid.Invalidate(FALSE);
}


void CPTRDlg::OnBnClickedButtonStageBufferRun()
{
	// 예제 함수
	double varRead, varWrite;
	varRead = g_pNavigationStage->GetGlobalRealVariable("DOUT_1");

	varWrite = 100;
	g_pNavigationStage->SetGlobalRealVariable("DOUT_1", varWrite);


	int indexBuffer = 10;
	g_pNavigationStage->RunBuffer(indexBuffer);
	g_pNavigationStage->StopBuffer(indexBuffer);

}

// PTR 측정용 버튼--- kyd
//void CPTRDlg::OnEnChangeEditDatanum()
//{
//	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
//	// CDialogEx::OnInitDialog() 함수를 재지정 
//	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
//	// 이 알림 메시지를 보내지 않습니다.
//
//	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
//}





void CPTRDlg::EnableButton(BOOL isEnable)
{
	GetDlgItem(IDC_BUTTON_PTR_READ_SENSOR)->EnableWindow(isEnable);
	GetDlgItem(IDC_BUTTON_PTR_GET_BACKGROUND)->EnableWindow(isEnable);
	GetDlgItem(IDC_BUTTON_PTR_READ_REF_REFLECTANCE)->EnableWindow(isEnable);
	GetDlgItem(IDC_BUTTON_PTR_MEASURE_WITHOUT_PELLICLE)->EnableWindow(isEnable);
	GetDlgItem(IDC_BUTTON_PTR_MEASURE_WITH_PELLICLE)->EnableWindow(isEnable);
}

int  CPTRDlg::ReadSensor() //삭제해야함
{
	int nReturn = 0;

	//g_pAdam->m_nPTRDataNumADAM = m_nPTRDataNum;
	//g_pAdam->Command_ScanStart();

	EnableButton(FALSE);

	//BOOL bLoopFlag = TRUE;
	//MSG msg;

	//DWORD timeoutMargin = 5000; // ms
	//DWORD nTimeout = g_pAdam->m_nPTRDataNumADAM + timeoutMargin;

	//long Timeout = (long)nTimeout;
	//clock_t clock_start = clock();
	//clock_t clockt_current;

	//CAutoMessageDlg MsgBoxAuto(g_pPTR);


	////g_pWarning->m_strWarningMessageVal = "PTR 측정중입니다.! ";
	////g_pWarning->UpdateData(FALSE);
	////g_pWarning->ShowWindow(SW_SHOW);
	//MsgBoxAuto.DoModal(_T("PTR 센서 측정중입니다.!"), 1);


	//while (bLoopFlag)
	//{
	//	if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
	//		TranslateMessage(&msg);
	//		DispatchMessage(&msg);
	//	}

	//	switch (MsgWaitForMultipleObjects(1, &g_pAdam->m_hAcquisitionEnd, FALSE, nTimeout, QS_ALLINPUT))
	//	{
	//	case WAIT_OBJECT_0:
	//		bLoopFlag = FALSE;
	//		break;
	//	case WAIT_TIMEOUT:
	//		g_pAdam->Command_ScanStop();
	//		bLoopFlag = FALSE;
	//		return -1;
	//		break;
	//	default:
	//		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	//		{
	//			TranslateMessage(&msg);
	//			DispatchMessage(&msg);
	//		}
	//		break;
	//	}

	//	clockt_current = clock();
	//	//if (Timeout - (clockt_current - clockt) < 0)
	//	// overflow? 가능성?
	//	if (Timeout < (clockt_current - clock_start))
	//	{
	//		bLoopFlag = FALSE;
	//		g_pAdam->Command_ScanStop();

	//		MsgBoxAuto.DoModal(_T("PTR 측정에 실패하였습니다.(Timeout)! "), 2);
	//		nReturn = -1;
	//	}
	//}

	//if (nReturn == 0)
	//{
	//	g_pAdam->Statistics(g_pAdam->m_dRawImageForDisplay[Detector1], g_pAdam->m_nPTRDataNumADAM, &g_pAdam->mD1, &g_pAdam->mD1Std);
	//	g_pAdam->Statistics(g_pAdam->m_dRawImageForDisplay[Detector2], g_pAdam->m_nPTRDataNumADAM, &g_pAdam->mD2, &g_pAdam->mD2Std);
	//	g_pAdam->Statistics(g_pAdam->m_dRawImageForDisplay[Detector3], g_pAdam->m_nPTRDataNumADAM, &g_pAdam->mD3, &g_pAdam->mD3Std);
	//}

	//g_pWarning->ShowWindow(SW_HIDE);

	EnableButton(TRUE);
	return nReturn;
}

int CPTRDlg::ReadSensor(int nDataNum) //삭제해야함
{
	int nReturn = 0;

	//g_pAdam->m_nPTRDataNumADAM = nDataNum;
	//g_pAdam->Command_ADAMStart_PTR();

	EnableButton(FALSE);

	//BOOL bLoopFlag = TRUE;
	//MSG msg;

	//DWORD timeoutMargin = 5000; // ms
	//DWORD nTimeout = g_pAdam->m_nPTRDataNumADAM + timeoutMargin;

	//long Timeout = (long)nTimeout;
	//clock_t clock_start = clock();
	//clock_t clockt_current;

	//CAutoMessageDlg MsgBoxAuto(g_pPTR);


	////g_pWarning->m_strWarningMessageVal = "PTR 측정중입니다.! ";
	////g_pWarning->UpdateData(FALSE);
	////g_pWarning->ShowWindow(SW_SHOW);
	//MsgBoxAuto.DoModal(_T("PTR 센서 측정중입니다.!"), 1);


	//while (bLoopFlag)
	//{
	//	if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
	//		TranslateMessage(&msg);
	//		DispatchMessage(&msg);
	//	}

	//	switch (MsgWaitForMultipleObjects(1, &g_pAdam->m_hAcquisitionEnd, FALSE, nTimeout, QS_ALLINPUT))
	//	{
	//	case WAIT_OBJECT_0:
	//		bLoopFlag = FALSE;
	//		break;
	//	case WAIT_TIMEOUT:
	//		//g_pAdam->Command_ADAMStop_PTR();
	//		bLoopFlag = FALSE;
	//		return -1;
	//		break;
	//	default:
	//		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	//		{
	//			TranslateMessage(&msg);
	//			DispatchMessage(&msg);
	//		}
	//		break;
	//	}

	//	clockt_current = clock();
	//	//if (Timeout - (clockt_current - clockt) < 0)
	//	// overflow? 가능성?
	//	if (Timeout < (clockt_current - clock_start))
	//	{
	//		bLoopFlag = FALSE;
	//		//g_pAdam->Command_ADAMStop_PTR();

	//		MsgBoxAuto.DoModal(_T("PTR 측정에 실패하였습니다.(Timeout)! "), 2);
	//		nReturn = -1;
	//	}
	//}

	//if (nReturn == 0)
	//{
	//	g_pAdam->Statistics(g_pAdam->m_dRawImageForDisplay[Detector1], g_pAdam->m_nPTRDataNumADAM, &g_pAdam->mD1, &g_pAdam->mD1Std);
	//	g_pAdam->Statistics(g_pAdam->m_dRawImageForDisplay[Detector2], g_pAdam->m_nPTRDataNumADAM, &g_pAdam->mD2, &g_pAdam->mD2Std);
	//	g_pAdam->Statistics(g_pAdam->m_dRawImageForDisplay[Detector3], g_pAdam->m_nPTRDataNumADAM, &g_pAdam->mD3, &g_pAdam->mD3Std);
	//}

	////g_pWarning->ShowWindow(SW_HIDE);

	EnableButton(TRUE);
	return nReturn;
}

void CPTRDlg::OnBnClickedButtonPtrReadSensor()
{
	UpdateData(TRUE);

	ReadSensor();

	UpdateData(FALSE);
}


void CPTRDlg::OnBnClickedButtonPtrGetBackground()
{
	UpdateData(TRUE);
	m_nPTRDataNum = mBackGroundN;
	UpdateData(FALSE);

	ReadSensor();

	g_pAdam->mD1BackGround = g_pAdam->mD1;
	g_pAdam->mD2BackGround = g_pAdam->mD2;
	g_pAdam->mD3BackGround = g_pAdam->mD3;

	g_pAdam->mD1BackGraoundStd = g_pAdam->mD1Std;
	g_pAdam->mD2BackGraoundStd = g_pAdam->mD2Std;
	g_pAdam->mD3BackGraoundStd = g_pAdam->mD3Std;

	SensorGridUpdate();

	UpdateData(FALSE);
}

void CPTRDlg::OnBnClickedButtonPtrReadRefReflectance()
{
	UpdateData(TRUE);
	m_nPTRDataNum = mReadRefReflectanceN;
	UpdateData(FALSE);

	ReadSensor();

	g_pAdam->mD1RefReflectance = g_pAdam->mD1;
	g_pAdam->mD2RefReflectance = g_pAdam->mD2;
	g_pAdam->mD3RefReflectance = g_pAdam->mD3;

	g_pAdam->mD1RefReflectanceStd = g_pAdam->mD1Std;
	g_pAdam->mD2RefReflectanceStd = g_pAdam->mD2Std;
	g_pAdam->mD3RefReflectanceStd = g_pAdam->mD3Std;

	SensorGridUpdate();
	UpdateData(FALSE);
}

void CPTRDlg::OnBnClickedButtonPtrMeasureWithoutPellicle()
{
	UpdateData(TRUE);
	m_nPTRDataNum = mMeasureWithOutPellicleN;
	UpdateData(FALSE);

	ReadSensor();

	g_pAdam->mD1WithoutPellicle = g_pAdam->mD1;
	g_pAdam->mD2WithoutPellicle = g_pAdam->mD2;
	g_pAdam->mD3WithoutPellicle = g_pAdam->mD3;

	g_pAdam->mD1WithoutPellicleStd = g_pAdam->mD1Std;
	g_pAdam->mD2WithoutPellicleStd = g_pAdam->mD2Std;
	g_pAdam->mD3WithoutPellicleStd = g_pAdam->mD3Std;

	SensorGridUpdate();

	UpdateData(FALSE);
}


void CPTRDlg::OnBnClickedButtonPtrMeasureWithPellicle()
{
	UpdateData(TRUE);
	m_nPTRDataNum = mMeasureWithPellicleN;
	UpdateData(FALSE);

	ReadSensor();

	g_pAdam->mD1WithPellicle = g_pAdam->mD1;
	g_pAdam->mD2WithPellicle = g_pAdam->mD2;
	g_pAdam->mD3WithPellicle = g_pAdam->mD3;

	g_pAdam->mD1WithPellicleStd = g_pAdam->mD1Std;
	g_pAdam->mD2WithPellicleStd = g_pAdam->mD2Std;
	g_pAdam->mD3WithPellicleStd = g_pAdam->mD3Std;

	SensorGridUpdate();

	UpdateData(FALSE);
}

void CPTRDlg::CalculatePTR(double x_um, double y_um)
{
	g_pAdam->CaculatePTR();
	UpdateData(FALSE);

	// Data 저장
	FILE *fp;
	errno_t err;

	CString fullfilename;
	CString filename;
	CString saveDirectory;
	//CString filenameDate;

	int year, month, day, hour, minute, second;
	year = CTime::GetCurrentTime().GetYear();
	month = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	minute = CTime::GetCurrentTime().GetMinute();
	second = CTime::GetCurrentTime().GetSecond();

	saveDirectory = _T("C:\\PTR Result");

	if (m_PTRResultGridRowCnt == 1)
	{
		mSaveFilenameDate.Format(_T("_%d%02d%02d_%02d%02d%02d"), year, month, day, hour, minute, second);
	}

	filename = _T("PTR_RESULT");
	filename = filename + mSaveFilenameDate;
	fullfilename = saveDirectory + "\\" + filename + ".txt";

	err = fopen_s(&fp, fullfilename, "a");

	if (m_PTRResultGridRowCnt == 1)
	{
		fprintf(fp, "Time \tNo \tX \tY \tT \tR \tT_without_nor \tR_gain_cal");

		fprintf(fp, " \tReflectance_ref \tD2toD1_gain_theory \tD2toD1_gain_measure");

		fprintf(fp, " \tD1_with_pelicle \tD2_with_pelicle \tD3_with_pelicle \tD1_with_pelicle_std \tD2_with_pelicle_std \tD3_with_pelicle_std");

		fprintf(fp, " \tD1_background \tD2_background \tD3_background \tD1_background_std \tD2_background_std \tD3_background_std");

		fprintf(fp, " \tD1_without_pelicle \tD2_without_pelicle \tD3_without_pelicle \tD1_without_pelicle_std \tD2_without_pelicle_std \tD3_without_pelicle_std");

		fprintf(fp, " \tD1_reference \tD2_reference \tD3_reference \tD1_reference_std \tD2_reference_std \tD3_reference_std\n");

	}
	fprintf(fp, "%02d%02d%02d%02d%02d", month, day, hour, minute, second);

	//No Pos X, Pos Y, T(%), R(%), T(without nor), R(gain caculation) 7개
	fprintf(fp, "\t%d \t%4.6f \t%4.6f \t%.4f \t%.4f \t%.4f \t%.4f",
		m_PTRResultGridRowCnt, x_um, y_um, g_pAdam->mT, g_pAdam->mR, g_pAdam->mTwithoutNormalization, g_pAdam->mRbyGainCalculation);

	//refmask reflectance, D2toD1_gain ratio = 이론값, D2toD1_gain ratio = 측정값 3개
	fprintf(fp, "\t%.4f \t%.4f \t%.4f", g_pAdam->mRefReflectance, g_pAdam->mD2ToD1_GainRatioCalculation, g_pAdam->mD2ToD1_GainRatioMeasure);

	//with pellicle : D1, D2, D3, D1(std), D2(std), D3(std) 6개
	fprintf(fp, "\t%.4f \t%.4f \t%.4f \t%.6f \t%.6f \t%.6f",
		g_pAdam->mD1WithPellicle, g_pAdam->mD2WithPellicle, g_pAdam->mD3WithPellicle, g_pAdam->mD1WithPellicleStd, g_pAdam->mD2WithPellicleStd, g_pAdam->mD3WithPellicleStd);

	//background: D1, D2, D3, D1(std), D2(std), D3(std) 6개
	fprintf(fp, "\t%.4f \t%.4f \t%.4f \t%.6f \t%.6f \t%.6f",
		g_pAdam->mD1BackGround, g_pAdam->mD2BackGround, g_pAdam->mD3BackGround, g_pAdam->mD1BackGraoundStd, g_pAdam->mD2BackGraoundStd, g_pAdam->mD3BackGraoundStd);

	//withtout pellicle: D1, D2, D3, D1(std), D2(std), D3(std)  6개
	fprintf(fp, "\t%.4f \t%.4f \t%.4f \t%.6f \t%.6f \t%.6f",
		g_pAdam->mD1WithoutPellicle, g_pAdam->mD2WithoutPellicle, g_pAdam->mD3WithoutPellicle, g_pAdam->mD1WithoutPellicleStd, g_pAdam->mD2WithoutPellicleStd, g_pAdam->mD3WithoutPellicleStd);

	//reference pellicle: D1, D2, D3, D1(std), D2(std), D3(std), 6개
	fprintf(fp, "\t%.4f \t%.4f \t%.4f \t%.6f \t%.6f \t%.6f\n",
		g_pAdam->mD1RefReflectance, g_pAdam->mD2RefReflectance, g_pAdam->mD3RefReflectance, g_pAdam->mD1RefReflectanceStd, g_pAdam->mD2RefReflectanceStd, g_pAdam->mD3RefReflectanceStd);

	fclose(fp);

	// Grid update
	WriteGrid(x_um, y_um, g_pAdam->mT, g_pAdam->mR);

	UpdateData(FALSE);
}



void CPTRDlg::CalculatePtrForScan(double x_um, double y_um)
{
	g_pAdam->CaculatePTR();

	m_saveTransmittance[m_saveCount] = g_pAdam->mT;
	m_saveReflectance[m_saveCount] = g_pAdam->mR;
	m_saveCount++;
	//WriteGrid(x_um, y_um, g_pAdam->mT, g_pAdam->mR);	
}



void CPTRDlg::AllocForScan(int NumX, int NumY)
{
	DeleteForScan();

	m_saveTransmittance = new double[NumX*NumY];
	m_saveReflectance = new double[NumX*NumY];

	m_saveCount = 0;

	//SetSmallSaveGrid();
	//m_saveTransmittanceSmall = new double[m_NumOfXGridwithMarginSmall*m_NumOfYGridwithMarginSmall];
	//m_saveReflectance = new double[m_NumOfXGridwithMarginSmall*m_NumOfYGridwithMarginSmall];
	//m_saveCountSmall = 0;

}
void CPTRDlg::DeleteForScan()
{
	if (m_saveTransmittance != NULL)
	{
		delete[] m_saveTransmittance;
		m_saveTransmittance = NULL;
	}

	if (m_saveReflectance != NULL)
	{
		delete[] m_saveReflectance;
		m_saveReflectance = NULL;
	}

	//if (m_saveTransmittanceSmall != NULL)
	//{
	//	delete[] m_saveTransmittanceSmall;
	//	m_saveTransmittanceSmall = NULL;
	//}

	//if (m_saveReflectanceSmall != NULL)
	//{
	//	delete[] m_saveReflectanceSmall;
	//	m_saveReflectanceSmall = NULL;
	//}

}


void CPTRDlg::MakeSmallDataForScan(int row)
{
	double sumT = 0;
	double sumR = 0;

	//for (int row = 0; row < m_NumOfYGridwithMargin; row++)
	//{
		//평균

	sumT = 0;
	sumR = 0;
	for (int i = 0; i < m_NumOfXGridwithMarginSmall; i++)
	{

		for (int j = 0; j < m_saveWindow; j++)
		{
			sumT = sumT + m_saveTransmittance[row*m_NumOfXGrid + i * m_saveWindow + j];
			sumR = sumR + m_saveReflectance[row*m_NumOfXGrid + i * m_saveWindow + j];
		}

		m_saveTransmittanceSmall[m_saveCountSmall] = sumT / m_saveWindow;
		m_saveReflectance[m_saveCountSmall] = sumR / m_saveWindow;

		// 좌표 업데이트 필요
		WriteGrid(m_XStart_um + i * m_YInterval_um, m_YStart_um + row * m_YInterval_um, m_saveTransmittanceSmall[m_saveCountSmall], m_saveReflectance[m_saveCountSmall]);

		m_saveCountSmall++;
	}
	//WriteGrid(x_um, y_um, g_pAdam->mT, g_pAdam->mR);
	//}
}

void CPTRDlg::SaveFileForScan()
{
	// Data 저장
	FILE *fp;
	errno_t err;

	CString fullfilename;
	CString filename;
	CString saveDirectory;
	//CString filenameDate;

	int year, month, day, hour, minute, second;
	year = CTime::GetCurrentTime().GetYear();
	month = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	minute = CTime::GetCurrentTime().GetMinute();
	second = CTime::GetCurrentTime().GetSecond();

	saveDirectory = _T("C:\\PTR Result");

	CString SaveFilenameDate;
	SaveFilenameDate.Format(_T("_%d%02d%02d_%02d%02d%02d"), year, month, day, hour, minute, second);


	filename = _T("phase_result");
	filename = filename + SaveFilenameDate;
	fullfilename = saveDirectory + "\\" + filename + ".txt";
	err = fopen_s(&fp, fullfilename, "a");


	// Start position, x, y, grid 저장...

	fprintf(fp, "Start position(x,y): \t%.6f \t%.6f\n", m_XStart_um, m_YStart_um);
	fprintf(fp, "End position(x,y): \t%.6f \t%.6f\n", m_XEnd_um, m_YEnd_um);
	fprintf(fp, "Grid width(x,y): \t%.6f \t%.6f\n", m_XInterval_um, m_YInterval_um);
	fprintf(fp, "Number of Grid(x,y): \t%d \t%d\n", m_NumOfXGridwithMargin, m_NumOfYGridwithMargin);
	fprintf(fp, "\n");
	fprintf(fp, "No \tT \tR\n");

	unsigned long TotalNum;

	TotalNum = m_NumOfXGridwithMargin * m_NumOfYGridwithMargin;

	for (unsigned long i = 0; i < TotalNum; i++)
	{
		//No Pos X, Pos Y, T(%), R(%), T(without nor), R(gain caculation) 7개
		fprintf(fp, "%d \t%.4f \t%.4f\n", i + 1, m_saveTransmittance[i], m_saveReflectance[i]);
	}

	fclose(fp);
}

void CPTRDlg::FileStartEnd(BOOL flag)
{

	// Data 저장
	FILE *fp;
	errno_t err;

	CString fullfilename;
	CString filename;
	CString saveDirectory;
	//CString filenameDate;

	int year, month, day, hour, minute, second;
	year = CTime::GetCurrentTime().GetYear();
	month = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	minute = CTime::GetCurrentTime().GetMinute();
	second = CTime::GetCurrentTime().GetSecond();

	saveDirectory = _T("C:\\PTR Result");

	filename = _T("phase_result");
	filename = filename + mSaveFilenameDate;
	fullfilename = saveDirectory + "\\" + filename + ".txt";

	err = fopen_s(&fp, fullfilename, "a");

	if (flag)
	{
		fprintf(fp, "%d%02d%02d_%02d%02d%02d: PTR Process START", year, month, day, hour, minute, second);
	}
	else
	{
		//mSaveFilenameDate.Format(_T("_%d%02d%02d_%02d%02d%02d"), year, month, day, hour, minute, second);
		fprintf(fp, "%d%02d%02d_%02d%02d%02d: PTR Process END", year, month, day, hour, minute, second);
	}
}
void CPTRDlg::OnBnClickedButtonCalculatePtr()
{
	double x = g_pNavigationStage->GetPosmm(STAGE_X_AXIS) * 1000;
	double y = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS) * 1000;

	CalculatePTR(x, y);

}


void CPTRDlg::OnBnClickedButtonPtrClearGrid()
{
	ClearGrid();
}


void CPTRDlg::OnBnClickedButtonPtrTest()
{
	UpdateData(TRUE);

	////ResetImage(m_MilImageWidth, m_MilImageHeight);
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<int> dis(0, 99);

	ClearGrid();

	for (int IndexCoupon = 0; IndexCoupon < m_TotalCouponNum; IndexCoupon++)
	{
		SelectDisp(IndexCoupon);

		for (int j = 0; j < m_MilImageHeightArray[IndexCoupon]; j++)
		{
			for (int i = 0; i < m_MilImageWidthArray[IndexCoupon]; i++)
			{
				double data = 82 + dis(gen) / 100.0;
				PushDispData(IndexCoupon, i, j, data);
				WriteGrid(0, 0, data, data);
			}
		}

		UpdateLut();

	}

	UpdateData(FALSE);
}


void CPTRDlg::OnBnClickedButton1()
{
	UpdateData(TRUE);

	g_pPTR->ClearGrid();
	g_pPTR->ResetPtrDisplay(&(g_pMaskMap->m_MaskMapWnd.m_ProcessData));

	poitIndex = 0;
	IndexCoupon = 0;
	indexY = 0;
	indexX = 0;

	IndexCoupon_pre = -1;


	UpdateData(FALSE);

}

void CPTRDlg::OnBnClickedButtonPtrTest3()
{
#if FALSE	//jkseo, 임시로 막음
	UpdateData(TRUE);

	//int poitIndex = 0;
	//int IndexCoupon = 0;
	//int indexY = 0;
	//int indexX = 0;
	//g_pMaskMap->

	//random_device rd;
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<int> dis(0, 99);


	if (IndexCoupon == g_pMaskMap->m_MaskMapWnd.m_ProcessData.TotalMeasureNum)
	{
		return;
	}

	if (IndexCoupon != IndexCoupon_pre)
	{
		g_pPTR->SelectDisp(IndexCoupon);
	}
	double data = 82 + dis(gen) / 100.0;

	g_pPTR->PushDispData(IndexCoupon, indexX, indexY, data);

	UpdateLut();

	IndexCoupon_pre = IndexCoupon;

	indexX++;
	if (indexX == g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementXNo)
	{
		indexX = 0;
		indexY++;
		if (indexY == g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[IndexCoupon].nMeasurementYNo)
		{
			indexY = 0;
			IndexCoupon++;
		}
	}

	UpdateData(FALSE);
#endif
}


void CPTRDlg::OnBnClickedButtonPtrGetRefPos()
{
	m_dRefPosX = m_dAbsPosX;
	m_dRefPosY = m_dAbsPosY;
}

void CPTRDlg::PTRPositionUpdate(double PosX, double PosY)
{
	m_dAbsPosX = PosX;
	m_dAbsPosY = PosY;

	m_dRelPosX = m_dAbsPosX - m_dRefPosX;
	m_dRelPosY = m_dAbsPosY - m_dRefPosY;

	g_pNavigationStage->ConvertToMaskFromStage(m_dAbsPosX, m_dAbsPosY, m_dMaskPosX, m_dMaskPosY);

	CString str;

	str.Format("%3.6f", m_dRefPosX);
	m_StageGrid.SetItemText(1, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%3.6f", m_dRefPosY);
	m_StageGrid.SetItemText(1, 2, (LPSTR)(LPCTSTR)str);

	str.Format("%3.6f", m_dAbsPosX);
	m_StageGrid.SetItemText(2, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%3.6f", m_dAbsPosY);
	m_StageGrid.SetItemText(2, 2, (LPSTR)(LPCTSTR)str);

	str.Format("%3.6f", m_dMaskPosX);
	m_StageGrid.SetItemText(3, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%3.6f", m_dMaskPosY);
	m_StageGrid.SetItemText(3, 2, (LPSTR)(LPCTSTR)str);

	str.Format("%3.6f", m_dRelPosX);
	m_StageGrid.SetItemText(4, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%3.6f", m_dRelPosY);
	m_StageGrid.SetItemText(4, 2, (LPSTR)(LPCTSTR)str);

	m_StageGrid.Invalidate(FALSE);

}

void CPTRDlg::SensorGridUpdate()
{
	CString str;
	str.Format("%.6f", g_pAdam->mD1BackGround);
	m_SensorGrid.SetItemText(1, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD2BackGround);
	m_SensorGrid.SetItemText(1, 2, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD3BackGround);
	m_SensorGrid.SetItemText(1, 3, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD1BackGraoundStd);
	m_SensorGrid.SetItemText(1, 4, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD2BackGraoundStd);
	m_SensorGrid.SetItemText(1, 5, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD3BackGraoundStd);
	m_SensorGrid.SetItemText(1, 6, (LPSTR)(LPCTSTR)str);

	str.Format("%.6f", g_pAdam->mD1RefReflectance);
	m_SensorGrid.SetItemText(2, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD2RefReflectance);
	m_SensorGrid.SetItemText(2, 2, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD3RefReflectance);
	m_SensorGrid.SetItemText(2, 3, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD1RefReflectanceStd);
	m_SensorGrid.SetItemText(2, 4, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD2RefReflectanceStd);
	m_SensorGrid.SetItemText(2, 5, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD3RefReflectanceStd);
	m_SensorGrid.SetItemText(2, 6, (LPSTR)(LPCTSTR)str);

	str.Format("%.6f", g_pAdam->mD1WithoutPellicle);
	m_SensorGrid.SetItemText(3, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD2WithoutPellicle);
	m_SensorGrid.SetItemText(3, 2, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD3WithoutPellicle);
	m_SensorGrid.SetItemText(3, 3, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD1WithoutPellicleStd);
	m_SensorGrid.SetItemText(3, 4, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD2WithoutPellicleStd);
	m_SensorGrid.SetItemText(3, 5, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD3WithoutPellicleStd);
	m_SensorGrid.SetItemText(3, 6, (LPSTR)(LPCTSTR)str);

	str.Format("%.6f", g_pAdam->mD1WithPellicle);
	m_SensorGrid.SetItemText(4, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD2WithPellicle);
	m_SensorGrid.SetItemText(4, 2, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD3WithPellicle);
	m_SensorGrid.SetItemText(4, 3, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD1WithPellicleStd);
	m_SensorGrid.SetItemText(4, 4, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD2WithPellicleStd);
	m_SensorGrid.SetItemText(4, 5, (LPSTR)(LPCTSTR)str);
	str.Format("%.6f", g_pAdam->mD3WithPellicleStd);
	m_SensorGrid.SetItemText(4, 6, (LPSTR)(LPCTSTR)str);

	m_SensorGrid.Invalidate(FALSE);
}


void CPTRDlg::MilInitialize()
{
	m_pWndDisplay = (CWnd *)(GetDlgItem(IDC_IMAGE_VIEW_PTR));

	m_MilSystem = g_milSystemHost;
	MdispAlloc(m_MilSystem, M_DEFAULT, M_DISPLAY_SETUP, M_DEFAULT, &m_MilDisplay);

	MdispControl(m_MilDisplay, M_MOUSE_USE, M_ENABLE);
	MdispControl(m_MilDisplay, M_MOUSE_CURSOR_CHANGE, M_ENABLE);
	MdispControl(m_MilDisplay, M_BACKGROUND_COLOR, M_COLOR_BLACK);
	MdispControl(m_MilDisplay, M_CENTER_DISPLAY, M_ENABLE);

	MdispHookFunction(m_MilDisplay, M_MOUSE_MOVE, MouseMoveFct, (void*)this);

	//LUT
	m_MilColorLut = MbufAllocColor(m_MilSystem, 3, m_constLutMax + 1, 1, 8 + M_UNSIGNED, M_LUT, M_NULL);

	//MgenLutFunction(m_MilColorLut, M_COLORMAP_JET, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT);
	MgenLutFunction(m_MilColorLut, M_COLORMAP_JET, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT);
	MdispLut(m_MilDisplay, m_MilColorLut);
	MdispControl(m_MilDisplay, M_UPDATE, M_ENABLE);

	// 이미지 확대 축소시 깨지지 않게 하기 위해 추가	
	MgraAlloc(m_MilSystem, &m_MilGraphContext);
	MgraAllocList(m_MilSystem, M_DEFAULT, &m_MilGraphList);
	MgraColor(m_MilGraphContext, M_COLOR_RED);
	MdispControl(m_MilDisplay, M_ASSOCIATED_GRAPHIC_LIST_ID, m_MilGraphList);
	//MgraHookFunction(m_MilGraphList, M_INTERACTIVE_GRAPHIC_STATE_MODIFIED, GraphicListModifiedHookFct, (void*)this);
	MgraControl(m_MilGraphContext, M_ROTATABLE, M_DISABLE);
	//MgraControlList(m_MilGraphList, M_GRAPHIC_LABEL(m_RoiLabel), M_DEFAULT, M_INTERACTIVE_ANNOTATIONS_COLOR, M_COLOR_DARK_RED);
	MdispControl(m_MilDisplay, M_GRAPHIC_LIST_INTERACTIVE, M_ENABLE);
}

void CPTRDlg::MilDestroy()
{	
	if (m_MilImageUpscale != M_NULL)
	{
		MbufFree(m_MilImageUpscale);
		m_MilImageUpscale = M_NULL;
	}

	if (m_MilDisplay != M_NULL)
	{
		MdispHookFunction(m_MilDisplay, M_MOUSE_MOVE + M_UNHOOK, MouseMoveFct, (void*)this);
		//MdispHookFunction(m_MilDisplay, M_MOUSE_LEFT_BUTTON_UP + M_UNHOOK, MouseLefeButtonUpFct, (void*)this);
	}
	if (m_MilDisplay != M_NULL)
	{
		MdispControl(m_MilDisplay, M_ASSOCIATED_GRAPHIC_LIST_ID, M_NULL);
	}
	if (m_MilGraphList)
	{
		MgraFree(m_MilGraphList);
		m_MilGraphList = M_NULL;
	}
	if (m_MilGraphContext)
	{
		MgraFree(m_MilGraphContext);
		m_MilGraphContext = M_NULL;
	}
	if (m_MilDisplay != M_NULL)
	{
		MdispFree(m_MilDisplay);
		m_MilDisplay = M_NULL;
	}

	if (m_MilColorLut != M_NULL)
	{
		MbufFree(m_MilColorLut);
		m_MilColorLut = M_NULL;
	}

}
//void CPTRDlg::UpdateDisplayLut()
//{
//
//	MgenLutFunction(m_MilColorLut, M_COLORMAP_JET, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT);
//	MdispLut(m_MilDisplay, m_MilColorLut);
//}

void CPTRDlg::SetMousePosition(MOUSEPOSITION MousePosition)
{
	UINT16 intensity = 0;
	m_ImageMousePosition = MousePosition;

	int x = round(m_ImageMousePosition.m_BufferPositionX);
	int y = round(m_ImageMousePosition.m_BufferPositionY);

	if (m_dispSelected >= 0)
	{
		if (m_IsUpScale)
		{
			MIL_INT sizex, sizey;

			MbufInquire(m_MilImageUpscale, M_SIZE_X, &sizex);
			MbufInquire(m_MilImageUpscale, M_SIZE_Y, &sizey);

			if (x >= 0 && x <= sizex - 1 && y >= 0 && y <= sizey - 1)
			{
				UINT16 value;
				MbufGet2d(m_MilImageUpscale, x, y, 1, 1, &value);
				double valueDouble = Scale16bitToDoulble(value);
				m_strMousePosition.Format("(%d, %d) T:%.3f", x, y, valueDouble);
				UpdateData(FALSE);
			}
		}
		else
		{
			if (x >= 0 && x <= m_MilImageWidthArray[m_dispSelected] - 1 && y >= 0 && y <= m_MilImageHeightArray[m_dispSelected] - 1)
			{

				UINT16 value;
				MbufGet2d(m_MilImageArray[m_dispSelected], x, y, 1, 1, &value);

				double valueDouble = Scale16bitToDoulble(value);

				m_strMousePosition.Format("(%d, %d) T:%.3f %.3f", x, y, m_ImageDataDoubleArray[m_dispSelected][m_MilImageWidthArray[m_dispSelected] * y + x], valueDouble);
				UpdateData(FALSE);
			}
		}

	}

	//m_dispSelected = indexDisp;

	//MdispSelectWindow(m_MilDisplay, M_NULL, M_NULL); //선택이 없을때  실행하면 error 발생
	//MdispSelectWindow(m_MilDisplay, , m_pWndDisplay->m_hWnd);

	//m_dispSelected

	//int posX = (int)round(m_ImageMousePosition.m_BufferPositionX);
	//int posY = (int)round(m_ImageMousePosition.m_BufferPositionY);

	/*if (posX > 0 && posX < m_MilImageWidth && posY >0 && posY < m_MilImageHeight)
	{
		m_strMousePosition.Format("(%d, %d)  Gray:%u", posX, posY, intensity);
	}*/


}

MIL_INT MFTYPE CPTRDlg::MouseMoveFct(MIL_INT HookType, MIL_ID EventID, void* UserDataPtr)
{
	CPTRDlg* pCurrentView = (CPTRDlg*)UserDataPtr;

	if (pCurrentView)
	{
		MOUSEPOSITION MousePosition;
		MdispGetHookInfo(EventID, M_MOUSE_POSITION_X, &MousePosition.m_DisplayPositionX);
		MdispGetHookInfo(EventID, M_MOUSE_POSITION_Y, &MousePosition.m_DisplayPositionY);
		MdispGetHookInfo(EventID, M_MOUSE_POSITION_BUFFER_X, &MousePosition.m_BufferPositionX);
		MdispGetHookInfo(EventID, M_MOUSE_POSITION_BUFFER_Y, &MousePosition.m_BufferPositionY);

		pCurrentView->SetMousePosition(MousePosition);

		//pCurrentView->UpdateData(false);
	}
	return 0;
}


void CPTRDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	DeleteArray();
	MilDestroy();
}


void CPTRDlg::OnBnClickedButtonReadReference()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	ReadRefDataFromConfig();
}

void CPTRDlg::ReadRefDataFromConfig()
{
	g_pConfig->ReadPTRInfo();

	g_pAdam->mD1RefReflectance = g_pConfig->m_dD1Ref;
	g_pAdam->mD2RefReflectance = g_pConfig->m_dD2Ref;
	g_pAdam->mD3RefReflectance = g_pConfig->m_dD3Ref;


	g_pAdam->mD1RefReflectanceStd = g_pConfig->m_dD1RefStd;
	g_pAdam->mD2RefReflectanceStd = g_pConfig->m_dD2RefStd;
	g_pAdam->mD3RefReflectanceStd = g_pConfig->m_dD3RefStd;

	g_pAdam->mRefReflectance = g_pConfig->m_RefReflectance;

	SensorGridUpdate();

	UpdateData(FALSE);
}

void CPTRDlg::OnBnClickedButtonSaveReference()
{
	UpdateData(TRUE);

	g_pConfig->m_dD1Ref = g_pAdam->mD1RefReflectance;
	g_pConfig->m_dD2Ref = g_pAdam->mD2RefReflectance;
	g_pConfig->m_dD3Ref = g_pAdam->mD3RefReflectance;

	g_pConfig->m_dD1RefStd = g_pAdam->mD1RefReflectanceStd;
	g_pConfig->m_dD2RefStd = g_pAdam->mD2RefReflectanceStd;
	g_pConfig->m_dD3RefStd = g_pAdam->mD3RefReflectanceStd;

	g_pConfig->m_RefReflectance = g_pAdam->mRefReflectance;

	g_pConfig->SavePTRInfo();

	SensorGridUpdate();
	UpdateData(FALSE);
}


HBRUSH CPTRDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  여기서 DC의 특성을 변경합니다.

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}


void CPTRDlg::OnBnClickedButtonFit()
{
	MdispControl(m_MilDisplay, M_SCALE_DISPLAY, M_ENABLE);
}


void CPTRDlg::OnBnClickedButtonPtrSetScale()
{
	UpdateData(TRUE);

	UpdateLut();

	UpdateData(FALSE);
}


void CPTRDlg::OnBnClickedCheckPtrAutoScale()
{
	UpdateData(TRUE);

	UpdateLut();

	if (m_bAutoScaleImage)
	{
		GetDlgItem(IDC_EDIT_PTR_LUT_LOW)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_PTR_LUT_HIGH)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_EDIT_PTR_LUT_LOW)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_PTR_LUT_HIGH)->EnableWindow(TRUE);
	}

	UpdateData(FALSE);
}


void CPTRDlg::ResetPtrDisplay(CProcessData *ProcessData)
{
#if FALSE	//jkseo, 임시로 막음
	int* widthArray;
	int* heightArray;

	widthArray = new int[ProcessData->TotalMeasureNum];
	heightArray = new int[ProcessData->TotalMeasureNum];

	for (int i = 0; i < ProcessData->TotalMeasureNum; i++)
	{
		widthArray[i] = ProcessData->pMeasureList[i].nMeasurementXNo;
		heightArray[i] = ProcessData->pMeasureList[i].nMeasurementYNo;
	}

	AllocArray(ProcessData->TotalMeasureNum, widthArray, heightArray);

	//combo box update
	CString str;
	for (int i = 0; i < m_TotalCouponNum; i++)
	{
		str.Format(_T("%d"), i + 1);
		m_comboCouponSelect.AddString(str);
	}
#endif
}

void CPTRDlg::OnBnClickedButtonPtrDispTest()
{
	CProcessData m_ProcessData;
	ResetPtrDisplay(&g_pMaskMap->m_MaskMapWnd.m_ProcessData);
	testCount = 0;
	testCount_i = 0;
	testCount_j = 0;
}

void CPTRDlg::DeleteArray()
{
	if (m_MilImageArray != NULL)
	{
		for (int i = 0; i < m_TotalCouponNum; i++)
		{
			MbufFree(m_MilImageArray[i]);
		}

		delete[] m_MilImageArray;
		m_MilImageArray = NULL;
	}
	if (m_MilImageWidthArray != NULL)
	{
		delete[] m_MilImageWidthArray;
		m_MilImageWidthArray = NULL;
	}

	if (m_MilImageHeightArray != NULL)
	{
		delete[] m_MilImageHeightArray;
		m_MilImageHeightArray = NULL;
	}

	if (m_DataMinArray != NULL)
	{
		delete[] m_DataMinArray;
		m_DataMinArray = NULL;
	}
	if (m_DataMaxArray != NULL)
	{
		delete[] m_DataMaxArray;
		m_DataMaxArray = NULL;
	}



	if (m_CurrentDataNum != NULL)
	{
		delete[] m_CurrentDataNum;
		m_CurrentDataNum = NULL;
	}

	if (m_DataMeanArray != NULL)
	{
		delete[] m_DataMeanArray;
		m_DataMeanArray = NULL;
	}
	if (m_DataSumArray != NULL)
	{
		delete[] m_DataSumArray;
		m_DataSumArray = NULL;
	}

	if (m_LutMinArray != NULL)
	{
		delete[] m_LutMinArray;
		m_LutMinArray = NULL;
	}

	if (m_LutMaxArray != NULL)
	{
		delete[] m_LutMaxArray;
		m_LutMaxArray = NULL;
	}

	if (m_ImageDataDoubleArray != NULL)
	{
		for (int i = 0; i < m_TotalCouponNum; i++)
		{
			delete[] m_ImageDataDoubleArray[i];
		}
		delete[] m_ImageDataDoubleArray;
		m_ImageDataDoubleArray = NULL;
	}

	if (m_ImageDataUint16Array != NULL)
	{
		for (int i = 0; i < m_TotalCouponNum; i++)
		{
			delete[] m_ImageDataUint16Array[i];
		}
		delete[] m_ImageDataUint16Array;
		m_ImageDataUint16Array = NULL;
	}

	for (int i = 0; i < m_TotalCouponNum; i++)
	{
		m_comboCouponSelect.DeleteString(m_TotalCouponNum - 1 - i);
	}

	m_TotalCouponNum = 0;
	m_dispSelected = -1;
}


void CPTRDlg::AllocArray(int NumCoupon, int* widthArray, int* heightArray)
{
	DeleteArray();

	m_TotalCouponNum = NumCoupon;

	m_MilImageArray = new MIL_ID[m_TotalCouponNum];
	m_MilImageWidthArray = new int[m_TotalCouponNum];
	m_MilImageHeightArray = new int[m_TotalCouponNum];
	m_DataMinArray = new double[m_TotalCouponNum];
	m_DataMaxArray = new double[m_TotalCouponNum];

	m_CurrentDataNum = new int[m_TotalCouponNum];
	memset(m_CurrentDataNum, 0, sizeof(int) *m_TotalCouponNum);

	m_DataMeanArray = new double[m_TotalCouponNum];
	m_DataSumArray = new double[m_TotalCouponNum];
	memset(m_DataSumArray, 0, sizeof(double) *m_TotalCouponNum);
	memset(m_DataMeanArray, 0, sizeof(double) *m_TotalCouponNum);

	m_LutMinArray = new int[m_TotalCouponNum];
	m_LutMaxArray = new int[m_TotalCouponNum];


	m_ImageDataDoubleArray = new double *[m_TotalCouponNum];
	m_ImageDataUint16Array = new UINT16 *[m_TotalCouponNum];

	for (int i = 0; i < m_TotalCouponNum; i++)
	{
		m_MilImageWidthArray[i] = widthArray[i];
		m_MilImageHeightArray[i] = heightArray[i];

		m_ImageDataDoubleArray[i] = new double[m_MilImageWidthArray[i] * m_MilImageHeightArray[i]];
		m_ImageDataUint16Array[i] = new UINT16[m_MilImageWidthArray[i] * m_MilImageHeightArray[i]];

		MbufCreate2d(m_MilSystem, m_MilImageWidthArray[i], m_MilImageHeightArray[i], 16 + M_UNSIGNED, M_IMAGE + M_DISP + M_PROC + M_ALLOCATION_OVERSCAN, M_DEFAULT, M_DEFAULT, m_ImageDataUint16Array[i], &m_MilImageArray[i]);
		MbufClear(m_MilImageArray[i], 0);

		memset(m_ImageDataDoubleArray[i], 0.0, sizeof(double) * m_MilImageWidthArray[i] * m_MilImageHeightArray[i]);
	}

}

void CPTRDlg::SelectDisp(int indexDisp)
{
	if (indexDisp <= m_TotalCouponNum && indexDisp >= 0)
	{
		m_IsUpScale = FALSE;

		//if (m_dispSelected != indexDisp)
		{
			m_dispSelected = indexDisp;

			//MdispSelectWindow(m_MilDisplay, M_NULL, M_NULL); //선택이 없을때  실행하면 error 발생
			MdispSelectWindow(m_MilDisplay, m_MilImageArray[indexDisp], m_pWndDisplay->m_hWnd);
			MdispControl(m_MilDisplay, M_SCALE_DISPLAY, M_ENABLE);
			//MdispControl(m_MilDisplay, M_CENTER_DISPLAY, M_ENABLE);
			//MdispControl(m_MilDisplay, M_MOUSE_USE, M_ENABLE);
		}
		m_comboCouponSelect.SetCurSel(m_dispSelected);
	}
}

void CPTRDlg::PushDispData(int indexDisp, int indexX, int indexY, double value)
{
	int w = m_MilImageWidthArray[indexDisp];
	int h = m_MilImageHeightArray[indexDisp];

	m_ImageDataDoubleArray[indexDisp][w*(h - 1 - indexY) + indexX] = value;
	m_ImageDataUint16Array[indexDisp][w*(h - 1 - indexY) + indexX] = ScaleDoubleToU16bit(value);

	if (indexX == 0 && indexY == 0)
	{
		m_DataMinArray[indexDisp] = value;
		m_DataMaxArray[indexDisp] = value;
	}

	m_DataMinArray[indexDisp] = (value < m_DataMinArray[indexDisp]) ? value : m_DataMinArray[indexDisp];
	m_DataMaxArray[indexDisp] = (value > m_DataMaxArray[indexDisp]) ? value : m_DataMaxArray[indexDisp];

	m_CurrentDataNum[indexDisp]++;

	m_DataSumArray[indexDisp] = m_DataSumArray[indexDisp] + value;
	m_DataMeanArray[indexDisp] = m_DataSumArray[indexDisp] / m_CurrentDataNum[indexDisp];

}
void CPTRDlg::UpdateLut()
{
	m_CurrentMean = m_DataMeanArray[m_dispSelected];

	if (m_bAutoScaleImage)
	{
		m_LutPercentMinCurrent = m_DataMeanArray[m_dispSelected] - m_AutoScaleWidth;

		if (m_LutPercentMinCurrent < 0)
		{
			m_LutPercentMinCurrent = 0;
		}
		m_LutPercentMaxCurrent = m_DataMeanArray[m_dispSelected] + m_AutoScaleWidth;
		if (m_LutPercentMaxCurrent > 100)
		{
			m_LutPercentMaxCurrent = 100;
		}

		m_LutLow = ScaleDoubleToU16bit(m_LutPercentMinCurrent);
		m_LutHigh = ScaleDoubleToU16bit(m_LutPercentMaxCurrent);

		UpdateDisplayLut(m_LutLow, m_LutHigh);
		MdispControl(m_MilDisplay, M_UPDATE, M_NOW);
	}
	else
	{
		m_LutLow = ScaleDoubleToU16bit(m_LutPercentMinCurrent);
		m_LutHigh = ScaleDoubleToU16bit(m_LutPercentMaxCurrent);

		UpdateDisplayLut(m_LutLow, m_LutHigh);
		MdispControl(m_MilDisplay, M_UPDATE, M_NOW);
	}
}


void CPTRDlg::UpdateDisplayLut(int low, int high)
{
	int end = m_constLutMax;

	MIL_ID MilDisplayLutChild;

	if (low > 0)
	{
		MilDisplayLutChild = MbufChild1d(m_MilColorLut, 0, low, M_NULL);
		MbufClear(MilDisplayLutChild, M_COLOR_DARK_BLUE);
		MbufFree(MilDisplayLutChild);
	}

	if (high - low + 1 > 0)
	{
		MilDisplayLutChild = MbufChild1d(m_MilColorLut, low, high - low + 1, M_NULL);
		MgenLutFunction(MilDisplayLutChild, M_COLORMAP_JET, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT);
		MbufFree(MilDisplayLutChild);
	}

	if (end - high > 0)
	{
		MilDisplayLutChild = MbufChild1d(m_MilColorLut, high + 1, end - high, M_NULL);
		MbufClear(MilDisplayLutChild, M_COLOR_DARK_RED);
		MbufFree(MilDisplayLutChild);
	}

	MdispLut(m_MilDisplay, m_MilColorLut);

}



UINT16 CPTRDlg::ScaleDoubleToU16bit(double data)
{
	UINT16 out = (double)m_constLutMax / 100.0 * data;
	return out;
}
//
//double CPTRDlg::Scale16bitToDoulble(int data)
//{
//	double out = 100.0 / (double)m_constLutMax * data;
//	return out;
//}

double CPTRDlg::Scale16bitToDoulble(UINT16 data)
{
	double out = 100.0 / (double)m_constLutMax * data;
	return out;
}



void CPTRDlg::OnBnClickedButton3()
{
	UpdateData(TRUE);
	DeleteArray();
}


void CPTRDlg::OnSelchangeComboSelectCoupon()
{
	UpdateData(TRUE);

	int dispNum = m_comboCouponSelect.GetCurSel();

	SelectDisp(m_comboCouponSelect.GetCurSel());

	UpdateLut();

	UpdateData(FALSE);

}


void CPTRDlg::OnBnClickedButtonPtrScanStart()
{



	g_pAdam->Command_Run();
	// EUV on, shutter open

	//g_pMaskMap->PtrScanProcess();
}


void CPTRDlg::OnBnClickedButtonPtrScanSet()
{
	g_pAdam->Command_Stop();
	/*UpdateData(TRUE);

	m_XDistance_um = m_XEnd_um - m_XStart_um;
	m_YDistance_um = m_YEnd_um - m_YStart_um;


	m_NumOfXGrid = m_XDistance_um / m_XInterval_um + 1;
	m_NumOfYGrid = m_YDistance_um / m_YInterval_um + 1;

	UpdateData(FALSE);*/
}


void CPTRDlg::OnBnClickedButtonPtrAlgorithmTest()
{

	unsigned int testValue;


	testValue = 3;

	//g_pAdam->Command_Run();
	//g_pAdam->Command_Stop();
	


	g_pAdam->Command_SetAverageCount(testValue);
	g_pAdam->Command_AverageRun();

	//g_pAdam->Command_AverageRunTimeout();
	//g_pAdam->Command_MPacketRun(testValue);
	//g_pAdam->Command_MPacketRunTimeout(testValue);
	//g_pAdam->Command_LifReset();


	//
	//g_pAdam->mD1RefReflectance = 0.0002;
	//g_pAdam->mD2RefReflectance = 4.3254;
	//g_pAdam->mD3RefReflectance = 0.3899;
	//
	//g_pAdam->mD1WithPellicle = 2.2882;
	//g_pAdam->mD2WithPellicle = 0.0105;
	//g_pAdam->mD3WithPellicle = 0.6860;
	//
	//g_pAdam->CaculatePTR();
	//
	//g_pAdam->mT;
	//g_pAdam->mR;
	//
	//int test = 0;
	/*m_PTRResultGrid.DeleteNonFixedRows();
	int length = 10;
	for (size_t i = 0; i < length; i++)
	{
		addResultGrid(1, i + 1);
	}*/
}

void CPTRDlg::SetSmallSaveGrid()
{

	//double *m_saveTransmittanceSmall = NULL;
	//double *m_saveReflectanceSmall = NULL;

	m_NumOfYGridwithMarginSmall = m_NumOfYGridwithMargin;

	m_XIntervalSmall_um = m_YInterval_um;
	m_YIntervalSmall_um = m_YInterval_um;

	m_saveWindow = m_XIntervalSmall_um / m_XInterval_um;

	m_NumOfXGridwithMarginSmall = m_NumOfXGridwithMargin / m_saveWindow;


}

void CPTRDlg::OnBnClickedButtonPtrScanSetFromRecipe()
{
		unsigned int testValue;


		testValue = 5;
		g_pAdam->Command_MPacketRun(testValue);
	


		//g_pAdam->Command_SetAverageCount(testValue);
		//g_pAdam->Command_AverageRun();

		//g_pAdam->Command_AverageRunTimeout();
		
		//g_pAdam->Command_MPacketRunTimeout(testValue);
		//g_pAdam->Command_LifReset();


#if FALSE	//jkseo, 임시로 막음
	//UpdateData(TRUE);

	g_pMaskMap->m_MaskMapWnd.m_ProcessData;

	int test = 0;

	//m_YInterval_um = g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[0].dMeasureDistance_um;

	m_XStart_um = g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[0].dMeasurementXPos_um;
	m_YStart_um = g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[0].dMeasurementYPos_um;

	m_XEnd_um = m_XStart_um + (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[0].nMeasurementXNo - 1)*g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[0].dMeasureDistance_um;
	m_YEnd_um = m_YStart_um + (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[0].nMeasurementYNo - 1)*g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[0].dMeasureDistance_um;

	m_XDistance_um = 20;
	m_YInterval_um = g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[0].dMeasureDistance_um;

	m_XDistance_um = m_XEnd_um - m_XStart_um;
	m_YDistance_um = m_YEnd_um - m_YStart_um;

	m_NumOfXGrid = m_XDistance_um / m_XInterval_um + 1;
	m_NumOfYGrid = m_YDistance_um / m_YInterval_um + 1;

	UpdateData(FALSE);
#endif
}




void CPTRDlg::OnEnChangeEditAutoScaleWidth()
{
	UpdateData(TRUE);
}




void CPTRDlg::OnBnClickedButtonUpscale()
{
#if FALSE	//jkseo, 임시로 막음
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	if (m_IsUpScale)
	{
	
		if (m_MilImageUpscale != M_NULL)
		{
			MbufFree(m_MilImageUpscale);
			m_MilImageUpscale = M_NULL;
		}

		if (m_dispSelected >= 0)
		{		
						
			MIL_INT widthOri = g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[m_dispSelected].nMeasurementXNo;
			MIL_INT heightOri = g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[m_dispSelected].nMeasurementYNo;

			//MbufInquire(m_MilImageArray[m_dispSelected], M_SIZE_X, &widthOri);
			//MbufInquire(m_MilImageArray[m_dispSelected], M_SIZE_Y, &heightOri);

			int width = widthOri * m_UpscaleRatio;
			int height = heightOri * m_UpscaleRatio;

			int overscanMargin = 2;


			int widthOverscan = widthOri + overscanMargin * 2;
			int heightOverscan = heightOri + overscanMargin * 2;

			MIL_ID milImageOverscan = M_NULL;
			MIL_ID milImageOrigin = M_NULL;

			MbufAlloc2d(m_MilSystem, widthOverscan, heightOverscan, 16 + M_UNSIGNED, M_IMAGE + M_DISP + M_PROC, &milImageOverscan);
			MbufChild2d(milImageOverscan, overscanMargin, overscanMargin, widthOri, heightOri, &milImageOrigin);
			MbufCopy(m_MilImageArray[m_dispSelected], milImageOrigin);

			//Mirroring 

			MIL_ID milChildOrigin, milChildOverscan;
			
			//left
			MbufChild2d(milImageOrigin, 0, 0, overscanMargin, heightOri, &milChildOrigin);
			MbufChild2d(milImageOverscan, 0, overscanMargin, overscanMargin, heightOri, &milChildOverscan);
			MimFlip(milChildOrigin, milChildOverscan, M_FLIP_HORIZONTAL, M_DEFAULT);
			MbufFree(milChildOrigin);
			MbufFree(milChildOverscan);

			//right
			MbufChild2d(milImageOrigin, widthOri- overscanMargin, 0, overscanMargin, heightOri, &milChildOrigin);
			MbufChild2d(milImageOverscan, widthOverscan - overscanMargin, overscanMargin, overscanMargin, heightOri, &milChildOverscan);
			MimFlip(milChildOrigin, milChildOverscan, M_FLIP_HORIZONTAL, M_DEFAULT);
			MbufFree(milChildOrigin);
			MbufFree(milChildOverscan);
	
			//top
			MbufChild2d(milImageOrigin, 0, 0, widthOri, overscanMargin, &milChildOrigin);
			MbufChild2d(milImageOverscan, overscanMargin, 0, widthOri, overscanMargin, &milChildOverscan);
			MimFlip(milChildOrigin, milChildOverscan, M_FLIP_VERTICAL, M_DEFAULT);
			MbufFree(milChildOrigin);
			MbufFree(milChildOverscan);
			
			// Bottom
			MbufChild2d(milImageOrigin, 0, heightOri- overscanMargin, widthOri, overscanMargin, &milChildOrigin);
			MbufChild2d(milImageOverscan, overscanMargin, heightOverscan- overscanMargin, widthOri, overscanMargin, &milChildOverscan);
			MimFlip(milChildOrigin, milChildOverscan, M_FLIP_VERTICAL, M_DEFAULT);
			MbufFree(milChildOrigin);
			MbufFree(milChildOverscan);

			//left top corner
			MbufChild2d(milImageOverscan, overscanMargin, 0, overscanMargin, overscanMargin, &milChildOverscan);
			MbufChild2d(milImageOverscan, 0, 0, overscanMargin, overscanMargin, &milChildOverscan);
			MimFlip(milChildOrigin, milChildOverscan, M_FLIP_HORIZONTAL, M_DEFAULT);
			MbufFree(milChildOrigin);
			MbufFree(milChildOverscan);

			//right top corner
			MbufChild2d(milImageOverscan, widthOverscan - 2*overscanMargin, 0, overscanMargin, overscanMargin, &milChildOverscan);
			MbufChild2d(milImageOverscan, widthOverscan- overscanMargin, 0, overscanMargin, overscanMargin, &milChildOverscan);			
			MimFlip(milChildOrigin, milChildOverscan, M_FLIP_HORIZONTAL, M_DEFAULT);
			MbufFree(milChildOrigin);
			MbufFree(milChildOverscan);

			//left bottom corner
			MbufChild2d(milImageOverscan, overscanMargin, heightOverscan- overscanMargin, overscanMargin, overscanMargin, &milChildOverscan);
			MbufChild2d(milImageOverscan, 0, heightOverscan - overscanMargin, overscanMargin, overscanMargin, &milChildOverscan);
			MimFlip(milChildOrigin, milChildOverscan, M_FLIP_HORIZONTAL, M_DEFAULT);
			MbufFree(milChildOrigin);
			MbufFree(milChildOverscan);

			//right bottom corner
			MbufChild2d(milImageOverscan, widthOverscan - 2 * overscanMargin, heightOverscan - overscanMargin, overscanMargin, overscanMargin, &milChildOverscan);
			MbufChild2d(milImageOverscan, widthOverscan - overscanMargin, heightOverscan - overscanMargin, overscanMargin, overscanMargin, &milChildOverscan);
			MimFlip(milChildOrigin, milChildOverscan, M_FLIP_HORIZONTAL, M_DEFAULT);
			MbufFree(milChildOrigin);
			MbufFree(milChildOverscan);

			MbufAlloc2d(m_MilSystem, width, height, 16 + M_UNSIGNED, M_IMAGE + M_DISP + M_PROC, &m_MilImageUpscale);
			MimResize(milImageOrigin, m_MilImageUpscale, m_UpscaleRatio, m_UpscaleRatio, M_BICUBIC);			
		

			MdispSelectWindow(m_MilDisplay, m_MilImageUpscale, m_pWndDisplay->m_hWnd);

			//MdispSelectWindow(m_MilDisplay, m_MilImageUpscaleChild, m_pWndDisplay->m_hWnd);
			MdispControl(m_MilDisplay, M_SCALE_DISPLAY, M_ENABLE);
			MdispControl(m_MilDisplay, M_CENTER_DISPLAY, M_ENABLE);
			UpdateLut();

			MbufFree(milImageOrigin);
			MbufFree(milImageOverscan);

			int test = 0;
		}
	}
	UpdateData(FALSE);
#endif
}

void CPTRDlg::OnBnClickedCheckUpscale()
{
	UpdateData(TRUE);

	if (!m_IsUpScale)
	{
		SelectDisp(m_dispSelected);
	}
}
