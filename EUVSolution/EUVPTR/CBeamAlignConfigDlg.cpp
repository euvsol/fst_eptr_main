﻿// CBeamOptimizationConfigDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CBeamOptimizationConfigDlg 대화 상자

IMPLEMENT_DYNAMIC(CBeamAlignConfigDlg, CDialogEx)

CBeamAlignConfigDlg::CBeamAlignConfigDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_BEAMALIGNCONFIG_DIALOG, pParent)
	, m_strOmegaPitch(_T("0.5"))
	, m_strOmegaYaw(_T("0.5"))
	, m_strMaxIntensity(_T("0.18"))
	, m_strMinIntensity(_T("0.02"))
	, m_strDetPercent(_T("20"))
	, m_strDetIntensity(_T("0.0520"))
	, m_strSatPercent(_T("60"))
	, m_strSatIntensity(_T("0.1080"))
	, m_strIncrementPercent(_T("70"))
	, m_strGridPercent(_T("60"))
	, m_strFineScanGrid(_T("0.05"))
{

}

CBeamAlignConfigDlg::~CBeamAlignConfigDlg()
{
}

void CBeamAlignConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_BEAMALIGNCONFIG_EDIT_OMEGAPITCH, m_strOmegaPitch);
	DDX_Text(pDX, IDC_BEAMALIGNCONFIG_EDIT_OMEGAYAW, m_strOmegaYaw);
	DDX_Text(pDX, IDC_BEAMALIGNCONFIG_EDIT_MAXINTENSITY, m_strMaxIntensity);
	DDX_Text(pDX, IDC_BEAMALIGNCONFIG_EDIT_MININTENSITY, m_strMinIntensity);
	DDX_Text(pDX, IDC_BEAMALIGNCONFIG_EDIT_DETPERCENT, m_strDetPercent);
	DDX_Text(pDX, IDC_BEAMALIGNCONFIG_EDIT_DETECTINTENSITY, m_strDetIntensity);
	DDX_Text(pDX, IDC_BEAMALIGNCONFIG_EDIT_SATPERCENT, m_strSatPercent);
	DDX_Text(pDX, IDC_BEAMALIGNCONFIG_EDIT_SATECTINTENSITY, m_strSatIntensity);
	DDX_Text(pDX, IDC_BEAMALIGNCONFIG_EDIT_INCREMENTPERCENT, m_strIncrementPercent);
	DDX_Text(pDX, IDC_BEAMALIGNCONFIG_EDIT_GRIDPERCENT, m_strGridPercent);
	DDX_Text(pDX, IDC_BEAMALIGNCONFIG_EDIT_FINESCANGRID, m_strFineScanGrid);
}


BEGIN_MESSAGE_MAP(CBeamAlignConfigDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BEAMALIGNCONFIG_BUTTON_SETINTENSITYPARAMETER, &CBeamAlignConfigDlg::OnBnClickedBeamconfigButtonSetintensityparameter)
END_MESSAGE_MAP()


// CBeamOptimizationConfigDlg 메시지 처리기


BOOL CBeamAlignConfigDlg::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::DestroyWindow();
}


BOOL CBeamAlignConfigDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	changeConfig(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CBeamAlignConfigDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreTranslateMessage(pMsg);
}

void CBeamAlignConfigDlg::changeConfig(BOOL isSimulator)
{
	CString strTemp = "";
	if (isSimulator == TRUE)
	{
		m_strOmegaPitch = "1.0";
		m_strOmegaYaw = "1.0";
		m_strMaxIntensity = "1.0";
		m_strMinIntensity = "0.0";
		m_strDetPercent = "20";
		m_strDetIntensity.Format(_T("%02.4f"), (_ttof(m_strMaxIntensity) - _ttof(m_strMinIntensity)) *_ttof(m_strDetPercent) / 100 + _ttof(m_strMinIntensity));
		m_strSatPercent = "60";
		m_strSatIntensity.Format(_T("%02.4f"), (_ttof(m_strMaxIntensity)) *_ttof(m_strSatPercent) / 100);
		m_strIncrementPercent = "70";
		m_strGridPercent = "60";
		m_strFineScanGrid = "0.05";
		g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_DSIPDETINTENSITY, m_strDetIntensity);
		strTemp.Format(_T("%02.4f"), _ttof(m_strOmegaPitch) * _ttof(m_strIncrementPercent) / 100);
		g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_INCREMENT, strTemp);
		strTemp.Format(_T("%02.4f"), _ttof(m_strOmegaYaw) * _ttof(m_strIncrementPercent) / 100);
		g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_INCREMENT, strTemp);
		UpdateData(FALSE);
	}
	else
	{
		m_strOmegaPitch = "0.5";
		m_strOmegaYaw = "0.5";
		m_strMaxIntensity = "0.18";
		m_strMinIntensity = "0.02";
		m_strDetPercent = "20";
		m_strDetIntensity.Format(_T("%02.4f"), (_ttof(m_strMaxIntensity) - _ttof(m_strMinIntensity)) *_ttof(m_strDetPercent) / 100 + _ttof(m_strMinIntensity));
		m_strSatPercent = "60";
		m_strSatIntensity.Format(_T("%02.4f"), (_ttof(m_strMaxIntensity)) *_ttof(m_strSatPercent) / 100);
		m_strIncrementPercent = "70";
		m_strGridPercent = "60";
		m_strFineScanGrid = "0.05";
		g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_DSIPDETINTENSITY, m_strDetIntensity);
		strTemp.Format(_T("%02.4f"), _ttof(m_strOmegaPitch) * _ttof(m_strIncrementPercent) / 100);
		g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_INCREMENT, strTemp);
		strTemp.Format(_T("%02.4f"), _ttof(m_strOmegaYaw) * _ttof(m_strIncrementPercent) / 100);
		g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_INCREMENT, strTemp);
		UpdateData(FALSE);
	}
}

void CBeamAlignConfigDlg::OnBnClickedBeamconfigButtonSetintensityparameter()
{
	CString strTemp = "";
	UpdateData(TRUE);
	if (g_pBeamAutoAlign->m_isAutoAlignOn == FALSE)
	{
		g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_DSIPDETINTENSITY, m_strDetIntensity);
		strTemp.Format(_T("%02.4f"), _ttof(m_strOmegaPitch) * _ttof(m_strIncrementPercent) / 100);
		g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_PITCH_INCREMENT, strTemp);
		strTemp.Format(_T("%02.4f"), _ttof(m_strOmegaYaw) * _ttof(m_strIncrementPercent) / 100);
		g_pBeamAutoAlign->SetDlgItemTextA(IDC_BEAMALIGN_EDIT_YAW_INCREMENT, strTemp);
	}

	//UpdateData(FALSE);
}
