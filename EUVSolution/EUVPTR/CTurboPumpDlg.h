﻿/**
 * LLC Turbo Pump Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once


// CTurboPumpDlg 대화 상자

class CTurboPumpDlg : public CDialogEx , public CAgilentTwisTorr304TMPCtrl, public IObserver
{
	DECLARE_DYNAMIC(CTurboPumpDlg)

public:
	CTurboPumpDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CTurboPumpDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TURBO_PUMP_DIALOG };
//#endif

protected:
	bool		m_LLCTmpThreadExitFlag;
	CWinThread*	m_LLCTmpThread;
	static UINT	Update_tmp_Thread(LPVOID pParam);

	HICON m_LedIcon[3];

	clock_t	m_start_time, m_finish_time;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();

	afx_msg void OnDestroy();
	afx_msg void OnBnClickedLlcTmpBtnOn();
	afx_msg void OnBnClickedLlcTmpBtnOff();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	DECLARE_MESSAGE_MAP()

	void LLCTmpSendUpdate();
	void LLCTmpReceiveDataUpdate();

public:
	int  OpenDevice();
	int	 LLC_Tmp_Off();
	int	 LLC_Tmp_On();
	void EmergencyStop();				// Error 발생 시 실행 함수.

	BOOL Is_LLC_Tmp_Connected()			{ return m_bSerialConnected; }
	int  GetLlcTmpState()				{ return m_nTmpState; }
	int  GetLlcTmphz()					{ return m_nTmpReceiveDataHz; }
	int  GetLlcTmprpm()					{ return m_nTmpReceiveDataRpm; }
	int  GetLlcTmpErrorCode()			{ return m_nTmpReceiveDataErrorCode; }
	int  GetLlcTmpTemperature()			{ return m_nTmpReceiveDataTemperature; }
	int  GetLlcTmpTemperatureHeatSink()	{ return m_nTmpReceiveDataTemperatureHeatsink; }
	int  GetLlcTmpETemperatureAir()		{ return m_nTmpReceiveDataTemperatureAir; }

	enum PumpStatus
	{
		Stop = 0,
		Working_Intlk,
		Starting,
		Auto_Tuning,
		Breaking,
		Running,
		Fail
	};

	enum PumpErrorStatus
	{
		NoError = 0,
		NoConnection,
		PumpOverTemp,
		ControllOverTemp = 4,
		PowerFail = 8,
		AuxFail = 16,
		OverVlotage = 32,
		HIghLoad = 128,
	};
 };
