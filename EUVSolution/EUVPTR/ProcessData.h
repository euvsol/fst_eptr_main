/**
 * Process Data Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once

struct _ResultPoint
{
	double Transmitance;
	double Reflectance;
	double BackgroundDetector1;
	double BackgroundDetector2;
	double BackgroundDetector3;
	double BackgroundStdDetector1;
	double BackgroundStdDetector2;
	double BackgroundStdDetector3;
	double ReferenceDetector1;
	double ReferenceDetector2;
	double ReferenceDetector3;
	double ReferenceStdDetector1;
	double ReferenceStdDetector2;
	double ReferenceStdDetector3;
	double WithoutPellicleDetector1;
	double WithoutPellicleDetector2;
	double WithoutPellicleDetector3;
	double WithoutPellicleStdDetector1;
	double WithoutPellicleStdDetector2;
	double WithoutPellicleStdDetector3;
	double WithPellicleDetector1;
	double WithPellicleDetector2;
	double WithPellicleDetector3;
	double WithPellicleStdDetector1;
	double WithPellicleStdDetector2;
	double WithPellicleStdDetector3;
};

struct _MeasurePointList
{
	int No;
	int CouponNo;
	int GridType;
	double CenterPosX;
	double CenterPosY;
	double PitchX;
	double PitchY;
	double ExposureTime;
	int RepeatNum;
	_ResultPoint Result;
};

struct _ResultScan
{
	double Transmitance;
	double Reflectance;
};

struct _MeasureScanList
{
	int No;
	int CouponNo;
	int GridType;
	double Width;
	double Height;
	double CenterPosX;
	double CenterPosY;
	double PitchX;
	double PitchY;
	double Velocity;
	double Distance;
	double trnas;
	_ResultScan Result;
};

class _CouponList
{
private: 
	vector<_MeasurePointList> m_Points;
	vector<_MeasureScanList>  m_Scans;

public:
	int No;
	//int GridType;
	//CString CouponNo;
	double Width;
	double Height;
	double CenterPosX;
	double CenterPosY;
	//double PitchX;
	//double PitchY;
	
	void AddPointData(_MeasurePointList data)
	{
		m_Points.push_back(data);
	}

	vector<_MeasurePointList>& GetPoints()
	{
		return m_Points;
	}

	void AddScanData(_MeasureScanList data)
	{
		m_Scans.push_back(data);
	}

	vector<_MeasureScanList>& GetScans()
	{
		return m_Scans;
	}

	void ClearMeasureData()
	{
		if (!m_Points.empty())
		{
			m_Points.clear();
			vector <_MeasurePointList>().swap(m_Points);
		}

		if (!m_Scans.empty())
		{
			m_Scans.clear();
			vector <_MeasureScanList>().swap(m_Scans);
		}
	}
};

class CProcessData : public CFile
{
public:	

	CProcessData();
	virtual ~CProcessData();
	
	int TotalCouponNum;
	int TotalAlignNum;
	int MeasurePointNum;
	int MeasureScanNum;
	int TotalMeasureNum;
	int CurrentMeasureNum;
	int CurrentCouponNum;

	CPath	curFilePath;
	CPath	orgFilePath;

	////////////////////////////////////////////////
	CString RecipeGenDateTime;
	CString EquipmentModelName;
	CString Substrate;
	CString SubstrateID;

	double SubstrateWidth;
	double SubstrateHeight;

	CString Lot;
	CString Step;
	CString Device;
	int Slot;
	CString SubstrateDirection;	

	struct _Coordinate
	{
		double X;
		double Y;
	};

	_Coordinate m_stSubstrateCenterPos_um;
	_Coordinate m_stOriginCoordinate_um;
	_Coordinate m_stAlignmentPoint_um[4];
	_Coordinate m_stBeamDiagnosisPos_um;
	
	int m_nBeamDiagPosExposureTime_ms;

private:
		vector<_CouponList> m_Coupons;

public:

	void ResetData();
	int LoadHeaderInfo(char *fpath);
	int LoadPTRMeasureData(char *fpath);

	void AddCouponData(_CouponList data)
	{
		m_Coupons.push_back(data);
	}

	vector<_CouponList>& GetCoupons()
	{
		return m_Coupons;
	}

	void ClearCouponData()
	{
		if (!m_Coupons.empty())
		{
			for (int i = 0; i < m_Coupons.size(); i++)
				m_Coupons[i].ClearMeasureData();

			m_Coupons.clear();
			vector <_CouponList>().swap(m_Coupons);
		}
	}
};
