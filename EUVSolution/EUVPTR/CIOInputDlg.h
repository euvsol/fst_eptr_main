﻿#pragma once


// CDigitalInput 대화 상자

class CDigitalInput : public CDialogEx , public CECommon
{
	DECLARE_DYNAMIC(CDigitalInput)

public:
	CDigitalInput(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CDigitalInput();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IO_INPUT_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	bool						m_Message_ThreadExitFlag;
	CWinThread*					m_Message_Thread;
	static UINT					Message_UpdataThread(LPVOID pParam);

	DECLARE_MESSAGE_MAP()

public:
	int		m_nChannel;
	HICON	m_LedIcon[3];

	void InitControls_DI(int nChannel);
	bool OnUpdateDigitalInput();

	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();

	CBrush  m_brush;
	CFont	m_font;

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	int m_nLlc_DryPumpOn_Cnt;
	int m_nLlc_DryPumpAlarm_Cnt;
	int m_nLlc_DryPumpWarning_Cnt;
	int m_nMc_DryPumpOn_Cnt;
	int m_nMc_DryPumpAlarm_Cnt;
	int m_nMc_DryPumpWarning_Cnt;
	int m_nLlc_Tmp_Leak_Cnt;
	int m_nMc_Tmp_Leak_Cnt;
	int m_nACRack_SmokeDectect_Cnt;
	int m_nControlRack_SmokeDectect_Cnt;
	int m_nWaterReturnTemp_Alarm_Cnt;
	int m_nMainAir_Alarm_Cnt;
	int	m_nMainWater_Alarm_Cnt;
	int	m_nWater_Temp_Alarm_Cnt;
	int	m_Lid_Alarm_Cnt;
	int	m_Isolator_Alarm_Cnt;

	int m_nLLCMaskCheck_Cnt;
	int m_nLLCMaskTilt1_Cnt;
	int m_nLLCMaskTilt2_Cnt;
	int m_nMCMaskCheck_Cnt;
	int m_nMCMaskTilt1_Cnt;
	int m_nMCMaskTilt2_Cnt;
	int m_nLLCMaskCheck_VacRobot_Cnt;
	int m_nMCMaskCheck_VacRobot_Cnt;
	
	int m_nFanCnt[14];

	int Dry_Pump_State;
	void Message_Thread();
	
};
