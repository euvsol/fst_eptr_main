﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

// CHWCommStatusDlg 대화 상자

IMPLEMENT_DYNAMIC(CHWCommStatusDlg, CDialogEx)

CHWCommStatusDlg::CHWCommStatusDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_HW_COMM_STATUS_DIALOG, pParent)
{

}

CHWCommStatusDlg::~CHWCommStatusDlg()
{
}

void CHWCommStatusDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CHWCommStatusDlg, CDialogEx)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CHWCommStatusDlg 메시지 처리기


BOOL CHWCommStatusDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CHWCommStatusDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CHWCommStatusDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CHWCommStatusDlg::ADAMCommStatus(BOOL bCommStat)
{

}

void CHWCommStatusDlg::EfemCommStatus(BOOL bCommStat)
{

}

void CHWCommStatusDlg::CrevisCommStatus(BOOL bCommStat)
{

}

void CHWCommStatusDlg::VacuumRobotCommStatus(BOOL bCommStat)
{

}

void CHWCommStatusDlg::LinearRevolverCommStatus(BOOL bCommStat)
{

}

void CHWCommStatusDlg::AFModuleCommStatus(BOOL bCommStat)
{

}

void CHWCommStatusDlg::MCGaugeCommStatus(BOOL bCommStat)
{

}

void CHWCommStatusDlg::LLCGaugeCommStatus(BOOL bCommStat)
{

}

void CHWCommStatusDlg::EUVSourceCommStatus(BOOL bCommStat)
{

}

void CHWCommStatusDlg::MCTmpCommStatus(BOOL bCommStat)
{

}

void CHWCommStatusDlg::LLCTmpCommStatus(BOOL bCommStat)
{

}

void CHWCommStatusDlg::NavigationStageCommStatus(BOOL bCommStat)
{

}

void CHWCommStatusDlg::ScanStageCommStatus(BOOL bCommStat)
{

}

void CHWCommStatusDlg::OmCameraCommStatus(BOOL bCommStat)
{
}

void CHWCommStatusDlg::FilterStageCommStatus(BOOL bCommStat)
{
}

void CHWCommStatusDlg::XRayCameraCommStatus(BOOL bCommStat)
{
}
