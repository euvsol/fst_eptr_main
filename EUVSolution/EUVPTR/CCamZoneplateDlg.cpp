﻿// CCamZoneplateDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CCamZoneplateDlg 대화 상자

IMPLEMENT_DYNAMIC(CCamZoneplateDlg, CDialogEx)

CCamZoneplateDlg::CCamZoneplateDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CAM_ZONEPLATE_DIALOG, pParent)
{
	MilDigitizer	  = M_NULL;
	m_pStatusThread   = NULL;
	m_bThreadExitFlag = FALSE;
	m_bGrab			  = TRUE;
	m_bConnected	  = FALSE;
}

CCamZoneplateDlg::~CCamZoneplateDlg()
{
}

void CCamZoneplateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CCamZoneplateDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CAM_ZONEPLATE_START_BUTTON, &CCamZoneplateDlg::OnBnClickedCamZoneplateStartButton)
	ON_BN_CLICKED(IDC_CAM_ZONEPLATE_STOP_BUTTON, &CCamZoneplateDlg::OnBnClickedCamZoneplateStopButton)
	ON_BN_CLICKED(IDC_CAM_ZONEPLATE_SAVE_BUTTON, &CCamZoneplateDlg::OnBnClickedCamZoneplateSaveButton)
	ON_BN_CLICKED(IDC_ZPCAM_SHOW_OM_BUTTON, &CCamZoneplateDlg::OnBnClickedZpcamShowOmButton)
END_MESSAGE_MAP()


// CCamZoneplateDlg 메시지 처리기


BOOL CCamZoneplateDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CCamZoneplateDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	CRect rect;
	CWnd *pWnd = GetDlgItem(IDC_ZONEPLATE_VIEWER_STATIC);
	pWnd->GetClientRect(rect);
	m_CameraWnd.Create(WS_CHILD | WS_VISIBLE /*| WS_VSCROLL | WS_HSCROLL*/, rect, pWnd);
	m_CameraWnd.ShowWindow(SW_SHOW);

	m_CameraWnd.m_bCrossLine = FALSE;
	m_CameraWnd.m_bMouseMove = FALSE;

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
	{
		MbufAllocColor(g_milSystemGigEVision, m_CameraWnd.m_nDigBand, m_CameraWnd.m_nDigSizeX, m_CameraWnd.m_nDigSizeY, 8 + M_UNSIGNED, M_IMAGE + M_PROC + M_DISP + M_DIB + M_GDI, &m_CameraWnd.m_MilOMDisplay);
		MbufClear(m_CameraWnd.m_MilOMDisplay, M_COLOR_GRAY);
		CString str;
		str.Format(_T("%s\\OM_IMAGE.BMP"), OM_IMAGEFILE_PATH);
		if (IsFileExist(str))
		{
			MbufLoad((char*)(LPCTSTR)str, m_CameraWnd.m_MilOMDisplay);
		}
		m_CameraWnd.SetTimer(ZONEPLATE_DISPLAY_TIMER, 100, 0);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CCamZoneplateDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	m_bThreadExitFlag = TRUE;

	if (m_pStatusThread != NULL)
	{
		HANDLE threadHandle = m_pStatusThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandle, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandle, 0/*dwExitCode*/);
				CloseHandle(threadHandle);
			}
		}
		m_pStatusThread = NULL;
	}

	if (MilDigitizer != M_NULL)
		MdigHalt(MilDigitizer);

	if (MilDigitizer != M_NULL)
	{
		MdigFree(MilDigitizer);
		MilDigitizer = M_NULL;
	}
}

int CCamZoneplateDlg::OpenDevice()
{
	int nRet = 0;

	try
	{
		char szCCFFile[128] = { 0, };
		sprintf(szCCFFile, OM_CAMERA_CCF_FILE_FULLPATH);

		MdigAlloc(g_milSystemGigEVision, M_DEV1, (MIL_TEXT_PTR)szCCFFile, M_DEFAULT, &MilDigitizer);

		m_CameraWnd.m_nDigSizeX = MdigInquire(MilDigitizer, M_SIZE_X, M_NULL);
		m_CameraWnd.m_nDigSizeY = MdigInquire(MilDigitizer, M_SIZE_Y, M_NULL);
		m_CameraWnd.m_nDigBand = MdigInquire(MilDigitizer, M_SIZE_BAND, M_NULL);

		MbufAllocColor(g_milSystemGigEVision, m_CameraWnd.m_nDigBand, m_CameraWnd.m_nDigSizeX, m_CameraWnd.m_nDigSizeY, 8 + M_UNSIGNED, M_IMAGE + M_DISP + M_GRAB, &m_CameraWnd.m_MilImage);
		MbufClear(m_CameraWnd.m_MilImage, 0x0);
		if (m_CameraWnd.m_MilOMDisplay != M_NULL)
		{
			MbufFree(m_CameraWnd.m_MilOMDisplay);
			m_CameraWnd.m_MilOMDisplay = M_NULL;
		}

		MbufAllocColor(g_milSystemGigEVision, m_CameraWnd.m_nDigBand, m_CameraWnd.m_nDigSizeX, m_CameraWnd.m_nDigSizeY, 8 + M_UNSIGNED, M_IMAGE + M_PROC + M_DIB + M_GDI, &m_CameraWnd.m_MilOMDisplay);
		MbufClear(m_CameraWnd.m_MilOMDisplay, 0x0);
		
		m_CameraWnd.SetTimer(ZONEPLATE_DISPLAY_TIMER, 100, 0);
	}
	catch (int exception)
	{
		nRet = -1;
	}

	if(nRet == 0)
	{
		m_pStatusThread = AfxBeginThread(ZpCamGrabThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);
		m_bConnected = TRUE;
	}
	
	return nRet;
}

UINT CCamZoneplateDlg::ZpCamGrabThread(LPVOID pParam)
{
	CCamZoneplateDlg* pDlg = (CCamZoneplateDlg*)pParam;

	while (!pDlg->m_bThreadExitFlag)
	{
		if (pDlg->m_bGrab)
			MdigGrab(pDlg->MilDigitizer, pDlg->m_CameraWnd.m_MilImage);

		Sleep(50);
	}

	return 0;
}

void CCamZoneplateDlg::OnBnClickedCamZoneplateStartButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CCameraDlg::OnBnClickedCamZoneplateStartButton() 버튼 클릭!"));

	m_bGrab = TRUE;
}


void CCamZoneplateDlg::OnBnClickedCamZoneplateStopButton()
{
	if (g_pLog != NULL)
		g_pLog->Display(0, _T("CCameraDlg::OnBnClickedCamZoneplateStopButton() 버튼 클릭!"));

	m_bGrab = FALSE;
}


void CCamZoneplateDlg::OnBnClickedCamZoneplateSaveButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CCamZoneplateDlg::OnBnClickedZpcamShowOmButton()
{
	g_pCamZoneplate->ShowWindow(SW_HIDE);
	g_pCamera->ShowWindow(SW_SHOW);
}
