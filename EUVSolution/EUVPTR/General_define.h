#pragma once

//1.SYSTEM FILE PATH

#define MAIN_PATH								_T("C:\\EUVPTR")											// 설비 루트 디렉토리 경로





#define CONFIG_PATH								_T("C:\\EUVPTR\\Config")									// Config. 디렉토리 경로





#define CONFIG_BACKUP_PATH						_T("C:\\EUVPTR\\Config\\Backup")							// Config Backup 디렉토리 경로





#define CONFIG_FILE_FULLPATH					_T("C:\\EUVPTR\\Config\\ESOL_Config.ini")					// Config. 파일 경로





#define RECOVERY_FILE_FULLPATH					_T("C:\\EUVPTR\\Config\\Recovery.ini")						// Recovery 파일 경로





#define IOMAP_FILE_FULLPATH						_T("C:\\EUVPTR\\Config\\IOMap_PTR.ini")					// IO map 파일 경로





//#define LOG_PATH								_T("C:\\EUVPTR\\Log")										// Log 디렉토리 경로





#define LOG_RUN_PATH							_T("C:\\EUVPTR\\Long_Run_Log")							// Log 디렉토리 경로





#define RECIPE_PATH								_T("C:\\EUVPTR\\Recipe")									// Recipe 파일 경로





#define RECIPE_MAP_IMAGE_PATH					_T("C:\\EUVPTR\\Recipe\\MapImage")							// Map Image 파일 경로





#define ALARMCODEFILE							_T("C:\\EUVPTR\\Config\\ALARMCode.ini")					// AlarmCode 파일 경로





#define OM_IMAGEFILE_PATH						_T("C:\\EUVPTR\\OM")										// Om Image 파일 경로





#define STAGEPOSITION_FILE_FULLPATH				_T("C:\\EUVPTR\\Config\\Stage_Position_DB.cdb")			// Stage Position 저장 파일 경로





#define PISTAGE_TEST_POSITION_FILE_FULLPATH		_T("C:\\EUVPTR\\Config\\PI_Stage_Test_Position_DB.cdb")	// PI Stage Test Point Position 저장 파일 경로





#define OM_CAMERA_CCF_FILE_FULLPATH				_T("C:\\EUVPTR\\Config\\OmCameraSetting.dcf")	// Om Camera CCF 파일 경로





#define XRAY_CAMERA_CONFIG_FILE_FULLPATH		_T("C:\\EUVPTR\\Config\\XRAY_Config.ini")
#define PHASE_RESULT_PATH						_T("C:\\Phase Result")
#define SQONESTAGE_POSITION_FILE_FULLPATH		_T("C:\\EUVPTR\\Config\\SQONE_Stage_Position.cdb")	// SQONE Stage Test Point Position 저장 파일 경로





//2. 윈도우 생성/소멸





#define CREATECLASS(ptr, Class)										if(ptr == NULL) { ptr = new Class;}
#define CREATEWINDOW(ptr, Class, pWnd, rect, dwStyle, nShow)		if(ptr == NULL) { ptr = new Class; LPCTSTR className = AfxRegisterWndClass(CS_DBLCLKS,  AfxGetApp()->LoadStandardCursor(IDC_ARROW)); ptr->CreateEx(NULL, className, NULL, dwStyle, rect, this, 0); ptr->ShowWindow(nShow);}
#define CREATEDIALOG(ptr, Class, pWnd, rectoffset, nShow)			if(ptr == NULL) { ptr = new Class; ptr->Create(Class::IDD, pWnd);	CRect rt; ptr->GetWindowRect(&rt); rt.OffsetRect(rectoffset); ptr->MoveWindow(rt); ptr->ShowWindow(nShow);}

#define DELETECLASS(ptr)											if(ptr) { delete ptr; ptr = NULL;}
#define DELETEWINDOW(ptr)											if(ptr) {(ptr)->DestroyWindow(); delete (ptr); ptr = NULL;}
#define DELETEDIALOG(ptr)											if(ptr) {(ptr)->DestroyWindow(); delete (ptr); ptr = NULL;}


//3. 컬러 Definition
#define WHITE										RGB(255,255,255)
#define BLACK										RGB(0,0,0)
#define RED											RGB(255,0,0)
#define GREEN										RGB(0,255,0) 
#define BLUE										RGB(0,0,255)
#define YELLOW										RGB(255,255,0)
#define ORANGE										RGB(255,155,50)
#define LIGHT_GRAY									RGB(225,225,225)	
#define GRAY										RGB(200, 200, 200)
#define MIDDLE_GRAY									RGB(180,180,180)
#define DARK_GRAY									RGB(128,128,128)
#define FOREST_GREEN								RGB(0,192,0)
#define DARK_GREEN									RGB(0,128,0)
#define LOW_BLUE									RGB(200, 255, 255)
#define SKY_BLUE									RGB(140,255,255)
#define GREENBLUE									RGB(0, 255, 255)
#define DARKBLUE									RGB(30,30,60)
#define DARK_BLUE									RGB(0,0,128)
#define ROYAL_BLUE									RGB(0,0,190)
#define PURPLE										RGB(155,0,200)
#define BROWN										RGB(80,50,0)
#define HOT_PINK									RGB(255,50,150)
#define CYAN										RGB(0,255,255)
#define LAVENDER									RGB(200,175,255)
#define PEACH										RGB(255,225,175)
#define TURQUOISE									RGB(0,190,190)
#define TAN											RGB(255,200,100)
#define MAROON										RGB(128,0,0)
#define DUSK										RGB(255,140,110)


//4. 설비 Configuration
#define	ETHERNET_COM_NUMBER		12
#define	SERIAL_COM_NUMBER		8

#define OFFLINE					0
#define ONLINE					1

#define SREM033					0
#define PHASE					1
#define EUVPTR					2
#define ELITHO					3

#define OPERATOR				0
#define ENGINEER				1
#define CONFIGURATION			2
#define EVALUATION				3
#define HELP					4

#define ETHERNET_CREVIS			0
#define ETHERNET_ADAM			1
#define ETHERNET_MONITOR_STAGE	2
#define ETHERNET_ZP_CAMERA		3
#define ETHERNET_SCAN_STAGE		4
#define ETHERNET_NAVI_STAGE		5
#define ETHERNET_MIRROR_SHIFT	6
#define ETHERNET_SRC_PC			7
#define ETHERNET_MTS			8
#define ETHERNET_VMTR			9
#define ETHERNET_LOCAL			10
#define ETHERNET_BEAM			11


#define SERIAL_ISOLATOR			0
#define SERIAL_VACUUMGAUGE		1
#define SERIAL_LIGHT_CTRL		2
#define SERIAL_AFMODULE			3
#define SERIAL_REVOLVER			4
#define SERIAL_MC_TMP			5
#define SERIAL_LLC_TMP			6
#define SERIAL_FILTER_STAGE		7

// Port Check Variable Define
#define MODULE_ETHERNET_CREVIS			0
#define MODULE_ETHERNET_ADAM			1
#define MODULE_ETHERNET_MONITOR_STAGE	2
#define MODULE_ETHERNET_ZP_CAMERA		3
#define MODULE_ETHERNET_MIRROR_SHIFT	4
#define MODULE_ETHERNET_SRC_PC			5
#define MODULE_ETHERNET_MTS				6
#define MODULE_ETHERNET_VMTR			7
#define MODULE_ETHERNET_LOCAL			8
#define MODULE_ETHERNET_NAVI_STAGE		9
#define MODULE_ETHERNET_SCAN_STAGE		10
#define MODULE_ETHERNET_BEAM			11
#define MODULE_SERIAL_ISOLATOR			12
#define MODULE_SERIAL_VACUUMGAUGE		13
#define MODULE_SERIAL_LIGHT_CTRL		14
#define MODULE_SERIAL_AFMODULE			15
#define MODULE_SERIAL_REVOLVER			16
#define MODULE_SERIAL_MC_TMP			17
#define MODULE_SERIAL_LLC_TMP			18
#define MODULE_SERIAL_FILTER_STAGE		19


//5. MASK가 존재가능한 Port 위치 Define
#define     UNKNOWN			   -1
#define		MTS_POD				0
#define		MTS_ROBOT_HAND		1
#define		MTS_ROTATOR			2
#define		MTS_FLIPPER			3
#define		LLC					4
#define		VACUUM_ROBOT_HAND	5
#define		CHUCK				6




//7. SUB 메뉴





#define	DISPLAY_MTS						0
#define DISPLAY_VMTR					1
#define DISPLAY_ISOLATOR				2
#define DISPLAY_OM						3
#define DISPLAY_STAGE					4
#define DISPLAY_TURBOPUMP				5
#define DISPLAY_DIO						6
#define DISPLAY_HWSTATUS				7
#define DISPLAY_GEMAUTO					8
#define DISPLAY_XRAYCAMERA				9
#define DISPLAY_SOURCE					10
#define DISPLAY_BASIC_TEST				11
#define DISPLAY_FLATNESS_TEST			12
#define DISPLAY_NAVIGATION_STAGE_TEST	13
#define DISPLAY_SOURCE_TEST				14


//8. D-Input/Output, A-Input/Output

#define DIGITAL_INPUT_NUMBER			160  
#define DIGITAL_OUTPUT_NUMBER			64
#define DIGITAL_IO_VIEW_NUMBER			16
#define ANALOG_INPUT_NUMBER				16  // ANALOG_INPUT_OUPUT 최소 수량 16 -> Dialog 에서의 최소 출력 화면 숫자가 16 개 이므로, 최소 수량 16.
#define ANALOG_OUTPUT_NUMBER			16
#define ANALOG_IO_VIEW_NUMBER			16
#define ANALOG_MAIN_TAB					0
#define DIGITAL_MAIN_TAB				1
#define ANALOG_SUB_TAB					2
#define DIGITAL_SUB_IN_TAB				10
#define DIGITAL_SUB_OUT_TAB				4
#define ANALOG_INPUT_SIZE				24 // CREVIS MODULE ANALOG INPUT SIZE
#define DIGITAL_INPUT_SIZE				20 // CREVIS MODULE DIGITAL INPUT SIZE
#define ANALOG_OUTPUT_SIZE				16  // CREVIS MODULE ANALOG OUTPUT SIZE
#define DIGITAL_OUTPUT_SIZE				8	// CREVIS MODULE DIGITAL OUTPUT SIZE
#define ANALOG_INPUT_UNIT_CONVERSION	409.5
#define ANALOG_OUTPUT_UNIT_CONVERSION	409.5
#define ANALOG_TEMP_UNIT_CONVERSION		10.0
#define MAINT_MODE_ON					0	
#define OPER_MODE_ON					1


//9. Vacuum Robot

#define LLC_STATION		1
#define CHUCK_STATION	2
#define GET_READY_POS	_T("G1")
#define PUT_READY_POS	_T("P1")


//10. Timer Define
#define ADAMDATA_DISPLAY_TIMER						2		//1초(1000ea data)마다 ADAM에서 날아오는 각종 data 추이를 graph로 갱신해주는 타이머





#define NAVISTAGE_UPDATE_TIMER						3		//0.5초마다 XY Navigation Stage의 상태를 갱신해주는 타이머





#define MAP_UPDATE_TIMER							4
#define SCANSTAGE_UPDATE_TIMER						5
#define VMTRROBOT_UPDATE_TIMER						6
#define MTS_UPDATE_TIMER							7
#define AFM_UPDATE_TIMER							8
#define LLC_TMP_UPDATE_TIMER						9
#define MC_TMP_UPDATE_TIMER							10
#define GAUGE_UPDATE_TIMER							11
#define IO_UPDATE_TIMER								12
#define REVOLVER_UPDATE_TIMER						13
#define OMIMAGE_DISPLAY_TIMER						14
#define EUV_SOURCE_UPDATE_TIMER						15
#define MAIN_DIALOG_INFO_TIMER						16
#define OUTPUT_DIALOG_TIMER							17
#define INPUT_DIALOG_TIMER							18
#define OUTPUT_PART_DIALOG_TIMER					19
#define INPUT_PART_DIALOG_TIMER						20
#define INPUT_LINE_PART_DIALOG_TIMER				21
#define OUTPUT_LINE_PART_DIALOG_TIMER				22
#define MASK_FLATNESS_MEASUREMENT_TIMER				23
#define SOURCE_MEASUREMENT_TIMER					24
#define FILTERSTAGE_UPDATE_TIMER					25
#define PI_STAGE_MEASUREMENT_TIMER					26
#define VACCUM_GAUGE_WRITE_TIMER					27
#define XRAY_CAMERA_UPDATE_TIMER					28
//#define XRAY_CAMERA_ACQUISITION_TIMER				29
#define ZONEPLATE_DISPLAY_TIMER						30
#define LLC_TMP_UPDATE_TIMER_LOGGING				31
#define MC_TMP_UPDATE_TIMER_LOGGING					32
#define STAGE_MEASUREMENT_TIMER						33
#define MC_SEQUENCE_CHECK_TIMER						34
#define LLC_SEQUENCE_CHECK_TIMER					35
#define REFERENCE_POS_CHECK_TIMER					36
#define ADAMDATA_CONNECTION_TIMER					37
#define XRAYCAMERA_TEMP_CHECK_TIMER_ONLY_MCVENTING	38
#define PORT_CHECK_TIMER							41
#define NAVISTAGE_LOG_WRITE_TIMER					42
#define MONITORING_STAGE_UPDATE_TIMER				43

//11. Vacuum Components(Chamber,Pump,Gauge,Valve들)의 상태 정의





#define VACUUM_ERROR				-1
#define CHAMBER_OPENED				1	//Status
#define CHAMBER_CLOSED				0	//Status
#define CHAMBER_NOT_OPENED_CLOSED	-1



#define CHAMBER_PUMPED				2
#define CHAMBER_MIDDLE_PUMPED		1
#define	CHAMBER_VENTED				0
#define	CHAMBER_OVER_VENTED			-1
#define VALVE_OPENED				1
#define VALVE_CLOSED				0
#define VALVE_NOT_OPENED_CLOSED		-1
#define VALVE_OPEN					1	//Command
#define VALVE_CLOSE					0	//Command
#define DRYPUMP_RUN					1
#define DRYPUMP_STOP				0
#define DRYPUMP_WARNING				-1
#define DRYPUMP_ERROR				-2
#define TMP_NORMAL					1
#define TMP_ACCELERATION			2
#define TMP_DECELERATION			3
#define TMP_AUTOTUNING				4
#define TMP_WORKINGINTLK			5
#define TMP_STOP					6
#define TMP_ON						7
#define TMP_FAIL					8
#define TMP_OFFLINE					0
#define TMP_ERROR					-1
#define TMP_UNKNOWN					-2
#define GAUGE_NORMAL				1
#define GAUGE_OFFLINE				0
#define GAUGE_ERROR					-1

//12. 각종 Timeout 시간 정의 -> 추후 Config 항목 ?
#define	LLC_VENT_TIMEOUT		150000;

//13. MTS UNIT NUMBER
#define LPM_UNIT	_T("4")
#define ROBOT_UNIT	_T("1")
#define ALIGN_UNIT	_T("3")
#define FLIP_UNIT	_T("K")
#define STAGE_UNIT	_T("A")


//14. 대기 및 진공 상태 
#define ATM_STATE 0
#define VACUUM_STATE 1

//15. 공급 상태





#define RUN					1
#define RUN_OFF				0
#define OPERATION_COMPLETED 0

 //16. Stage Position DB: 80~99는 System에서 사용(SYSMTE_ 으로 시작, 1~79는 USER_ 로 시작)
#define MAX_STAGE_POSITION						100		//총 Stage위치 티칭수 
#define SYSTEM_LOADING_POSITION					99	
#define SYSTEM_EUV_MASKCENTER_POSITION			98	
#define SYSTEM_OM_MASKCENTER_POSITION			97	
#define SYSTEM_EUV_MASKLEFTDOWN_POSITION		96	
#define SYSTEM_OM_MASKLEFTDOWN_POSITION			95	
#define SYSTEM_EUV_LWS_POSITION					94
#define SYSTEM_OM_LWS_POSITION					93
#define SYSTEM_TBD13_POSITION					92	
#define SYSTEM_TBD12_POSITION					91	
#define SYSTEM_TBD11_POSITION					90	
#define SYSTEM_TBD10_POSITION					89	
#define SYSTEM_TBD9_POSITION					88	
#define SYSTEM_TBD8_POSITION					87
#define SYSTEM_TBD7_POSITION					86
#define SYSTEM_TBD6_POSITION					85
#define SYSTEM_TBD5_POSITION					84
#define SYSTEM_TBD4_POSITION					83
#define SYSTEM_TBD3_POSITION					82
#define SYSTEM_TBD2_POSITION					81
#define SYSTEM_TBD1_POSITION					80


//17.MASK 유무 상태.

#define MASK_NONE	0
#define MASK_ON		1
#define MASK_SLANT	2
#define MASK_ERROR	-1

//18.TRIGGER 상태.
#define TRIGGER_ON  0
#define TRIGGER_OFF 1
#define TRIGGER_ERROR -1


//19.ARM 상태.
#define EXTEND 0
#define RETRACT 1
#define ARM_ERROR -1

#define ISOLATOR_DOWN   0
#define ISOLATOR_UP		1

//20. Pumping / Venting 상태 정의





#define LLC_Venting_PreWork_Error_State -10
#define LLC_Venting_SlowVent_Error_State -9
#define LLC_Venting_FastVent_Error_State -8
#define LLC_Pumping_PreWork_Error_State -7
#define LLC_Pumping_Slow_Rough_Error_State -6
#define LLC_Pumping_Fast_Rough_Error_State -5
#define LLC_Pumping_Tmp_Rough_Error_State -4
#define LLC_Pumping_Complete_Error_State -3
#define  Pumping_ERROR_State  -2
#define  Venting_ERROR_State -1
#define  State_IDLE_State  0		
#define  Pumping_START_State  1	
#define  Venting_START_State  2
#define  Pumping_SLOWROUGH_State  3
#define  Pumping_FASTROUGH_State  4				
#define  Pumping_TMPROUGH_State  5			
#define  Pumping_COMPLETE_State  6		
#define  Venting_SLOWVENT_State  7
#define  Venting_FASTVENT_State  8
#define  Venting_COMPLETE_State  9

//21. LinearRevolver 동작





#define LR_HOME  0
#define LR_LEFT  1
#define LR_RIGHT 2

//22. Scope Type
#define EUV				0
#define SCOPE_OM4X		1
#define SCOPE_OM100X	2
#define SCOPE_ALL		3

//23. MFC 유량 제어 Limit 값





#define SLOW_MFC_LIMIT	2		//Slow MFC 10 용량의 20% = 2
#define FAST_MFC_LIMIT	2000	//Fast MFC 20,000 용량의 10% = 2000
#define CHECK_MFC_NUM	40		//범위 넘어 가는 반복 횟수 

//24. USER MESSAGE ID


//25. EUV Phase 측정





#define X_DIRECTION 0
#define Y_DIRECTION 1

//#define CAP_SENSOR1 1
//#define CAP_SENSOR2 2
//#define CAP_SENSOR3 3
//#define CAP_SENSOR4 4


#define LB 0 //LEFT_BOTTOM
#define LT 1 //LEFT_TOP
#define RT 2 //RIGHT_TOP
#define RB 3 //RIGHT_BOTTOM

#define NUM_OF_PHASE_ALIGN_POINT 3
#define NUM_OF_PHASE_ALIGN_AXIS 2


#define DOWN_RIGHT 0
#define DOWN_LEFT 1
#define UP_RIGHT 2
#define UP_LEFT 3

#define COARSE_ALIGN_PAD 0
#define COARSE_ALIGN_LS 1

#define FIRSRT_POINT 0
#define SECOND_POINT 1

//26. Stage Max Size
#define X_AXIS_1_SIZE 350 
#define Y_AXIS_0_SIZE 170

//#define PHASE_ALIGN 0
//#define PHASE_MEASURE 1


//┏ down right ┓down left
//┗(up right   ┛  up left

//Alignment Point
#define REFERENCE	 0
#define LEFT_TOP	 1
#define RIGHT_TOP	 2
#define RIGHT_BOTTOM 3


//27. Sequence Stage
#define SEQUENCE_READ						-1
#define SEQUENCE_DONE						0
#define SEQUENCE_LLC_PUMP_PREWORK_DONE		1
#define SEQUENCE_LLC_SLOW_PUMP_DONE			2
#define SEQUENCE_LLC_FAST_PUMP_DONE			3
#define SEQUENCE_LLC_TMP_PUMP_DONE			4
#define SEQUENCE_LLC_VENT_PREWORK_DONE		5
#define SEQUENCE_LLC_SLOW_VENT_DONE			6
#define SEQUENCE_LLC_FAST_VETN_DONE			7
#define SEQUENCE_LLC_VENT_COMPLETE_DONE		8
#define SEQUENCE_MC_PUMP_PREWORK_DONE		1
#define SEQUENCE_MC_SLOW_PUMP_DONE			2
#define SEQUENCE_MC_FAST_PUMP_DONE			3
#define SEQUENCE_MC_TMP_PUMP_DONE			4
#define SEQUENCE_MC_VENT_PREWORK_DONE		5
#define SEQUENCE_MC_SLOW_VENT_DONE			6
#define SEQUENCE_MC_FAST_VETN_DONE			7
#define SEQUENCE_MC_VENT_COMPLETE_DONE		8

#define IO_WRITE_COMPLETE	0

#define PLASMACLEANER_START_MCPRESSURE			5.0e-6