﻿#pragma once


// CCamZoneplateDlg 대화 상자

class CCamZoneplateDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CCamZoneplateDlg)

public:
	CCamZoneplateDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CCamZoneplateDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CAM_ZONEPLATE_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	BOOL			m_bThreadExitFlag;
	CWinThread*		m_pStatusThread;
	BOOL			m_bGrab;
	BOOL			m_bConnected;

	static UINT	ZpCamGrabThread(LPVOID pParam);

	DECLARE_MESSAGE_MAP()

public:
	int OpenDevice();

	MIL_ID MilDigitizer;


	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();

	CCameraWnd	m_CameraWnd;
	BOOL m_bCrossLineCheck;
	BOOL m_bMouseMoveCheck;
	afx_msg void OnBnClickedCamZoneplateStartButton();
	afx_msg void OnBnClickedCamZoneplateStopButton();
	afx_msg void OnBnClickedCamZoneplateSaveButton();
	afx_msg void OnBnClickedZpcamShowOmButton();
};
