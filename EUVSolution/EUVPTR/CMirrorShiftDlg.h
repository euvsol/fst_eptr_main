﻿#pragma once


// CMirrorShiftDlg 대화 상자

class CMirrorShiftDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CMirrorShiftDlg)

public:
	CMirrorShiftDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CMirrorShiftDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MIRROR_SHIFT_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()


public:

};
