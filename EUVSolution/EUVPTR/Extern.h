#pragma once

extern	CLogDisplayDlg*							g_pLog;
extern  CEUVPTRDlg*								g_pEUVSolution;
extern	CCameraDlg*								g_pCamera;
extern	CIODlg*									g_pIO;
extern	CLogDisplayDlg*							g_pLog;
extern	CMaskEFEMDlg*							g_pEfem;
extern	CNavigationStageDlg*					g_pNavigationStage;
extern	CTurboPumpMCDlg*						g_pMCTmp_IO;
extern	CTurboPumpDlg*							g_pLLCTmp_IO;
extern	CVacuumGaugeDlg*						g_pGauge_IO;
extern	CVacuumRobotDlg*						g_pVacuumRobot;
extern  CConfigurationEditorDlg*				g_pConfig;
extern  CRecipeEditorDlg*						g_pRecipe;
extern	CProcessEditorDlg*						g_pProcessEditor;
extern	CHWCommStatusDlg*						g_pCommStat;
extern	CSystemTestDlg*							g_pTest;
extern	CAutoProcess*							g_pAP;
extern	CVacuumProcess*							g_pVP;
extern  CWarningDlg*							g_pWarning;
extern	CAlarmDlg*								g_pAlarm;
extern  CSubMenuDlg*							g_pSubMenu;
extern  CSubTestMenuDlg*						g_pSubTestMenu;
extern  CEUVSourceDlg*							g_pEUVSource;
extern  CAnimationGUI*							g_pAnimationGUI;
extern	CMaskMapDlg*							g_pMaskMap;
extern	CMaindialog*							g_pMaindialog;
extern  CMirrorShiftDlg*						g_pMS;
extern CLoadingScreenDlg						g_pLoadingScreen;
extern CChartdirDlg*							g_pChart;
extern CChartdirLineDlg*						g_pChartline;
extern CChartdirStageDlg*						g_pChartstage;
extern CFilterStageDlg*							g_pFilterStage;
extern CCamZoneplateDlg*						g_pCamZoneplate;
extern CLightControllerDlg*						g_pLightCtrl;
extern CPTRDlg*									g_pPTR;
extern AdamPtr*									g_pAdam;

extern CNavigationStageTestDlg*					g_pNavigationStageTest;
extern CSourceTestDlg*							g_pSourceTest;
//extern CBeamConnect*							g_pBeamCon;

extern CSQOneControlDlg*						g_pSqOneControl;
extern CSqOneManuaDlg*							g_pSqOneManual;
extern CSqOneConfigDlg*							g_pSqOneConfig;
extern CSQOneDataBaseDlg*						g_pSqOneDB;

extern CBeamSearch1DDlg*						g_pBeamSearch1D;
extern CBeamSearch2DDlg*						g_pBeamSearch2D;
extern CBeamAutoAlignDlg*						g_pBeamAutoAlign;
extern CBeamAlignMainDlg*						g_pBeamMain;
extern CBeamAlignConfigDlg*						g_pBeamConfig;
extern CPlasmaCleanerDlg*						g_pPlasmaCleaner;
extern CSubModuleDlg*							g_pSubModule;
extern CDeviceManager*							g_pDevMgr;

extern MIL_ID	                        g_milApplication;
extern MIL_ID	                        g_milSystemHost;
extern MIL_ID                           g_milSystemGigEVision;
