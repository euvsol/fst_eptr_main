﻿// CSqOneManuaDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
// CSqOneManuaDlg 대화 상자

IMPLEMENT_DYNAMIC(CSqOneManuaDlg, CDialogEx)

CSqOneManuaDlg::CSqOneManuaDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SQONEMANUAL_DIALOG, pParent)
	, m_strHorizontal(_T("0.0"))
	, m_strVertical(_T("0.0"))
	, m_strBeam(_T("0.0"))
	, m_strPitch(_T("0.0"))
	, m_strYaw(_T("0.0"))
	, m_strRoll(_T("0.0"))
{
	m_dSetPositionX = 0.0;
	m_dSetPositionY = 0.0;
	m_dSetPositionRx = 0.0;
	m_dSetPositionRy = 0.0;
	m_dSetPositionRz = 0.0;
	m_dSetPositionCx = 0.0;
	m_dSetPositionCy = 0.0;
	m_dSetPositionCz = 0.0;
	m_dSetPositionIncrement = 0.0;
}

CSqOneManuaDlg::~CSqOneManuaDlg()
{
}

void CSqOneManuaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_SQONEMANUAL_EDIT_DISPHORIZONTAL, m_strHorizontal);
	DDX_Text(pDX, IDC_SQONEMANUAL_EDIT_DISPVERTICAL, m_strVertical);
	DDX_Text(pDX, IDC_SQONEMANUAL_EDIT_DISPBEAM, m_strBeam);
	DDX_Text(pDX, IDC_SQONEMANUAL_EDIT_DISPPITCH, m_strPitch);
	DDX_Text(pDX, IDC_SQONEMANUAL_EDIT_DISPYAW, m_strYaw);
	DDX_Text(pDX, IDC_SQONEMANUAL_EDIT_DISPROLL, m_strRoll);
}


BEGIN_MESSAGE_MAP(CSqOneManuaDlg, CDialogEx)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_MOVEUNTILCOMPLETE, &CSqOneManuaDlg::OnBnClickedBtnSqmanualMove)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_GET, &CSqOneManuaDlg::OnBnClickedBtnSqmanualGet)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_XPLUS, &CSqOneManuaDlg::OnBnClickedBtnSqonemanuslXplus)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_XMINUS, &CSqOneManuaDlg::OnBnClickedBtnSqonemanuslXminus)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_YPLUS, &CSqOneManuaDlg::OnBnClickedBtnSqonemanuslYplus)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_YMINUS, &CSqOneManuaDlg::OnBnClickedBtnSqonemanuslYminus)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_ZPLUS, &CSqOneManuaDlg::OnBnClickedBtnSqonemanuslZplus)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_ZMINUS, &CSqOneManuaDlg::OnBnClickedBtnSqonemanuslZminus)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_PITCHPLUS, &CSqOneManuaDlg::OnBnClickedBtnSqonemanuslPitchplus)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_PITCHMINUS, &CSqOneManuaDlg::OnBnClickedBtnSqonemanuslPitchminus)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_YAWPLUS, &CSqOneManuaDlg::OnBnClickedBtnSqonemanuslYawplus)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_YAWMINUS, &CSqOneManuaDlg::OnBnClickedBtnSqonemanuslYawminus)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_ROLLPLUS, &CSqOneManuaDlg::OnBnClickedBtnSqonemanuslRollplus)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_ROLLMINUS, &CSqOneManuaDlg::OnBnClickedBtnSqonemanuslRollminus)
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_TEST, &CSqOneManuaDlg::OnBnClickedBtnSqmanualMove2)
	ON_BN_CLICKED(IDC_SQONEMANUAL_CHECK_MACRO, &CSqOneManuaDlg::OnBnClickedCheckSqmanualMacro)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_SQONEMANUAL_BUTTON_SAVE, &CSqOneManuaDlg::OnBnClickedSqonemanualButtonSave)
END_MESSAGE_MAP()


// CSqOneManuaDlg 메시지 처리기


BOOL CSqOneManuaDlg::OnInitDialog()
{
	__super::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	// ICON
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);


	m_ArrowIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ARW2DN), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_ArrowIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ARW2UP), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_ArrowIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ARW2LT), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);
	m_ArrowIcon[3] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ARW2RT), IMAGE_ICON, 22, 22, LR_DEFAULTCOLOR);


	m_SqoneStage[0] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_SQONE1), IMAGE_BITMAP, 150, 150, LR_DEFAULTCOLOR);
	m_SqoneStage[1] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_SQONE2), IMAGE_BITMAP, 150, 150, LR_DEFAULTCOLOR);
	m_SqoneStage[2] = (HBITMAP)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_SQONE3), IMAGE_BITMAP, 150, 150, LR_DEFAULTCOLOR);


	((CStatic*)GetDlgItem(IDC_SQONEMANUAL_BITMAP_HORIZONTAL))->SetBitmap(m_SqoneStage[0]);
	((CStatic*)GetDlgItem(IDC_SQONEMANUAL_BITMAP_VERTICAL))->SetBitmap(m_SqoneStage[1]);
	((CStatic*)GetDlgItem(IDC_SQONEMANUAL_BITMAP_BEAM))->SetBitmap(m_SqoneStage[0]);
	((CStatic*)GetDlgItem(IDC_SQONEMANUAL_BITMAP_PITCH))->SetBitmap(m_SqoneStage[1]);
	((CStatic*)GetDlgItem(IDC_SQONEMANUAL_BITMAP_YAW))->SetBitmap(m_SqoneStage[0]);
	((CStatic*)GetDlgItem(IDC_SQONEMANUAL_BITMAP_ROLL))->SetBitmap(m_SqoneStage[2]);

	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_XPLUS))->SetIcon(m_ArrowIcon[0]);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_XMINUS))->SetIcon(m_ArrowIcon[1]);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_YPLUS))->SetIcon(m_ArrowIcon[1]);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_YMINUS))->SetIcon(m_ArrowIcon[0]);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_ZPLUS))->SetIcon(m_ArrowIcon[2]);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_ZMINUS))->SetIcon(m_ArrowIcon[3]);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_PITCHPLUS))->SetIcon(m_ArrowIcon[1]);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_PITCHMINUS))->SetIcon(m_ArrowIcon[1]);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_YAWPLUS))->SetIcon(m_ArrowIcon[1]);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_YAWMINUS))->SetIcon(m_ArrowIcon[1]);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_ROLLPLUS))->SetIcon(m_ArrowIcon[1]);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_ROLLMINUS))->SetIcon(m_ArrowIcon[1]);

	GetDlgItem(IDC_SQONEMANUAL_BUTTON_MOVEUNTILCOMPLETE)->EnableWindow(false);
	GetDlgItem(IDC_SQONEMANUAL_EDIT_HORIZONTAL)->EnableWindow(false);
	GetDlgItem(IDC_SQONEMANUAL_EDIT_VERTICAL)->EnableWindow(false);
	GetDlgItem(IDC_SQONEMANUAL_EDIT_BEAM)->EnableWindow(false);
	GetDlgItem(IDC_SQONEMANUAL_EDIT_PITCH)->EnableWindow(false);
	GetDlgItem(IDC_SQONEMANUAL_EDIT_YAW)->EnableWindow(false);
	GetDlgItem(IDC_SQONEMANUAL_EDIT_ROLL)->EnableWindow(false);

	SetDlgItemTextA(IDC_SQONEMANUAL_EDIT_INCREMENTAL, "0.1");

	SetTimer(0, 100, NULL);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CSqOneManuaDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case 0:
		updataUI();
	default:
		break;
	}
	__super::OnTimer(nIDEvent);
}

void CSqOneManuaDlg::getDlgEditValue(BOOL isMacro)
{
	CString temp = "";
	if (isMacro == TRUE)
	{
		GetDlgItemTextA(IDC_SQONEMANUAL_EDIT_HORIZONTAL, temp);
		m_dSetPositionX = atof(temp);
		GetDlgItemTextA(IDC_SQONEMANUAL_EDIT_VERTICAL, temp);
		m_dSetPositionY = atof(temp);
		GetDlgItemTextA(IDC_SQONEMANUAL_EDIT_BEAM, temp);
		m_dSetPositionZ = atof(temp);
		GetDlgItemTextA(IDC_SQONEMANUAL_EDIT_PITCH, temp);
		m_dSetPositionRx = atof(temp);
		GetDlgItemTextA(IDC_SQONEMANUAL_EDIT_YAW, temp);
		m_dSetPositionRy = atof(temp);
		GetDlgItemTextA(IDC_SQONEMANUAL_EDIT_ROLL, temp);
		m_dSetPositionRz = atof(temp);
		m_dSetPositionCx = g_pSqOneControl->m_SQOneParseData.Cx;
		m_dSetPositionCy = g_pSqOneControl->m_SQOneParseData.Cy;
		m_dSetPositionCz = g_pSqOneControl->m_SQOneParseData.Cz;
		GetDlgItemTextA(IDC_SQONEMANUAL_EDIT_INCREMENTAL, temp);
		m_dSetPositionIncrement = atof(temp);
	}
	else
	{
		m_dSetPositionX = g_pSqOneControl->m_SQOneParseData.X;
		m_dSetPositionY = g_pSqOneControl->m_SQOneParseData.Y;
		m_dSetPositionZ = g_pSqOneControl->m_SQOneParseData.Z;
		m_dSetPositionRx = g_pSqOneControl->m_SQOneParseData.Rx;
		m_dSetPositionRy = g_pSqOneControl->m_SQOneParseData.Ry;
		m_dSetPositionRz = g_pSqOneControl->m_SQOneParseData.Rz;
		m_dSetPositionCx = g_pSqOneControl->m_SQOneParseData.Cx;
		m_dSetPositionCy = g_pSqOneControl->m_SQOneParseData.Cy;
		m_dSetPositionCz = g_pSqOneControl->m_SQOneParseData.Cz;
		GetDlgItemTextA(IDC_SQONEMANUAL_EDIT_INCREMENTAL, temp);
		m_dSetPositionIncrement = atof(temp);
	}
}


void CSqOneManuaDlg::updataUI()
{
	if (g_pSqOneControl->m_bConnected)
	{
		m_strHorizontal.Format(_T("%f"), g_pSqOneControl->m_SQOneParseData.X);
		m_strVertical.Format(_T("%f"), g_pSqOneControl->m_SQOneParseData.Y);
		m_strBeam.Format(_T("%f"), g_pSqOneControl->m_SQOneParseData.Z);
		m_strPitch.Format(_T("%f"), g_pSqOneControl->m_SQOneParseData.Rx);
		m_strYaw.Format(_T("%f"), g_pSqOneControl->m_SQOneParseData.Ry);
		m_strRoll.Format(_T("%f"), g_pSqOneControl->m_SQOneParseData.Rz);
		UpdateData(false);
	}
	if (g_pBeamAutoAlign->m_isAutoAlignOn == TRUE || g_pBeamSearch2D->m_bisScan == TRUE ||  g_pSqOneControl->m_isMoving == TRUE)
	{
		if (IsDlgButtonChecked(IDC_SQONEMANUAL_CHECK_MACRO) == TRUE)
		{
			GetDlgItem(IDC_SQONEMANUAL_BUTTON_GET)->EnableWindow(FALSE);
			GetDlgItem(IDC_SQONEMANUAL_CHECK_MACRO)->EnableWindow(FALSE);
			setMacroEditWindow(FALSE);
		}
		else
		{
			GetDlgItem(IDC_SQONEMANUAL_CHECK_MACRO)->EnableWindow(FALSE);
			setMacroButtonWindow(FALSE);
		}
	}
	else
	{
		if (IsDlgButtonChecked(IDC_SQONEMANUAL_CHECK_MACRO) == TRUE)
		{
			GetDlgItem(IDC_SQONEMANUAL_BUTTON_GET)->EnableWindow(TRUE);
			GetDlgItem(IDC_SQONEMANUAL_CHECK_MACRO)->EnableWindow(TRUE);
			setMacroEditWindow(TRUE);
		}
		else
		{	
			GetDlgItem(IDC_SQONEMANUAL_CHECK_MACRO)->EnableWindow(TRUE);
			setMacroButtonWindow(TRUE);
		}
	}
}

void CSqOneManuaDlg::OnBnClickedBtnSqmanualGet()
{
	SetDlgItemTextA(IDC_SQONEMANUAL_EDIT_HORIZONTAL, m_strHorizontal);
	SetDlgItemTextA(IDC_SQONEMANUAL_EDIT_VERTICAL, m_strVertical);
	SetDlgItemTextA(IDC_SQONEMANUAL_EDIT_BEAM, m_strBeam);
	SetDlgItemTextA(IDC_SQONEMANUAL_EDIT_PITCH, m_strPitch);
	SetDlgItemTextA(IDC_SQONEMANUAL_EDIT_YAW, m_strYaw);
	SetDlgItemTextA(IDC_SQONEMANUAL_EDIT_ROLL, m_strRoll);
}

void CSqOneManuaDlg::OnBnClickedBtnSqmanualMove()
{
	int nRet = 0;
	g_pSqOneControl->m_bStopFlag = false;
	g_pSqOneControl->Display(0, "Manual Move Button Click");
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		getDlgEditValue(TRUE);
		nRet = g_pSqOneControl->checkLimit(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			if (nRet == -2)
			{
				::AfxMessageBox("Sq-1 Stage Stoped");
				return;
			}
			else if (nRet != 0)
			{
				::AfxMessageBox("Sq-1 Timeout Error");
				return;
			}
			else
			{
				return;
			}
		}
		else
		{
			g_pSqOneControl->Display(0, "Limit Error");
		}
	}
	else
	{
		g_pSqOneControl->Display(0, "Communication Error");
	}
}

void CSqOneManuaDlg::OnBnClickedBtnSqonemanuslXplus()
{
	g_pSqOneControl->Display(0, "Macro Horizontal Plus Move Button Click");
	g_pSqOneControl->m_bStopFlag = false;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		getDlgEditValue(FALSE);
		int nRet = 0;
		nRet = g_pSqOneControl->checkLimit(m_dSetPositionX + m_dSetPositionIncrement, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), m_dSetPositionX + m_dSetPositionIncrement, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(m_dSetPositionX + m_dSetPositionIncrement, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			if (nRet == -2)
			{
				::AfxMessageBox("Sq-1 Stage Stoped");
				return;
			}
			else if (nRet != 0)
			{
				::AfxMessageBox("Sq-1 Timeout Error");
				return;
			}
			else
			{
				return;
			}
		}
	}
	else
	{
		g_pSqOneControl->Display(0, "Communication Error");
	}
}


void CSqOneManuaDlg::OnBnClickedBtnSqonemanuslXminus()
{

	g_pSqOneControl->Display(0, "Macro Horizontal Minus Move Button Click");
	g_pSqOneControl->m_bStopFlag = false;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		getDlgEditValue(FALSE);
		int nRet = 0;
		nRet = g_pSqOneControl->checkLimit(m_dSetPositionX - m_dSetPositionIncrement, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), m_dSetPositionX - m_dSetPositionIncrement, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(m_dSetPositionX - m_dSetPositionIncrement, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			if (nRet == -2)
			{
				::AfxMessageBox("Sq-1 Stage Stoped");
				return;
			}
			else if (nRet != 0)
			{
				::AfxMessageBox("Sq-1 Timeout Error");
				return;
			}
			else
			{
				return;
			}
		}
	}
	else
	{
		g_pSqOneControl->Display(0, "Communication Error");
	}
}


void CSqOneManuaDlg::OnBnClickedBtnSqonemanuslYplus()
{
	g_pSqOneControl->Display(0, "Macro Vertical Plus Move Button Click");
	g_pSqOneControl->m_bStopFlag = false;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		getDlgEditValue(FALSE);
		int nRet = 0;
		nRet = g_pSqOneControl->checkLimit(m_dSetPositionX, m_dSetPositionY + m_dSetPositionIncrement, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), m_dSetPositionX, m_dSetPositionY + m_dSetPositionIncrement, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(m_dSetPositionX, m_dSetPositionY + m_dSetPositionIncrement, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			if (nRet == -2)
			{
				::AfxMessageBox("Sq-1 Stage Stoped");
				return;
			}
			else if (nRet != 0)
			{
				::AfxMessageBox("Sq-1 Timeout Error");
				return;
			}
			else
			{
				return;
			}
		}
	}
	else
	{
		g_pSqOneControl->Display(0, "Communication Error");
	}
}


void CSqOneManuaDlg::OnBnClickedBtnSqonemanuslYminus()
{
	g_pSqOneControl->Display(0, "Macro Vertical Minus Move Button Click");
	g_pSqOneControl->m_bStopFlag = false;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		getDlgEditValue(FALSE);
		int nRet = 0;
		nRet = g_pSqOneControl->checkLimit(m_dSetPositionX, m_dSetPositionY - m_dSetPositionIncrement, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), m_dSetPositionX, m_dSetPositionY - m_dSetPositionIncrement, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(m_dSetPositionX, m_dSetPositionY - m_dSetPositionIncrement, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			if (nRet == -2)
			{
				::AfxMessageBox("Sq-1 Stage Stoped");
				return;
			}
			else if (nRet != 0)
			{
				::AfxMessageBox("Sq-1 Timeout Error");
				return;
			}
			else
			{
				return;
			}
		}
	}
	else
	{
		g_pSqOneControl->Display(0, "Communication Error");
	}
}


void CSqOneManuaDlg::OnBnClickedBtnSqonemanuslZplus()
{
	g_pSqOneControl->Display(0, "Macro Beam Plus Move Button Click");
	g_pSqOneControl->m_bStopFlag = false;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		getDlgEditValue(FALSE);
		int nRet = 0;
		nRet = g_pSqOneControl->checkLimit(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ + m_dSetPositionIncrement, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ + m_dSetPositionIncrement, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ + m_dSetPositionIncrement, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			if (nRet == -2)
			{
				::AfxMessageBox("Sq-1 Stage Stoped");
				return;
			}
			else if (nRet != 0)
			{
				::AfxMessageBox("Sq-1 Timeout Error");
				return;
			}
			else
			{
				return;
			}
		}
	}
	else
	{
		g_pSqOneControl->Display(0, "Communication Error");
	}
}


void CSqOneManuaDlg::OnBnClickedBtnSqonemanuslZminus()
{
	g_pSqOneControl->Display(0, "Macro Beam Minus Move Button Click");
	g_pSqOneControl->m_bStopFlag = false;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		getDlgEditValue(FALSE);
		int nRet = 0;
		nRet = g_pSqOneControl->checkLimit(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ - m_dSetPositionIncrement, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ - m_dSetPositionIncrement, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ - m_dSetPositionIncrement, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			if (nRet == -2)
			{
				::AfxMessageBox("Sq-1 Stage Stoped");
				return;
			}
			else if (nRet != 0)
			{
				::AfxMessageBox("Sq-1 Timeout Error");
				return;
			}
			else
			{
				return;
			}
		}
	}
	else
	{
		g_pSqOneControl->Display(0, "Communication Error");
	}
}


void CSqOneManuaDlg::OnBnClickedBtnSqonemanuslPitchplus()
{
	g_pSqOneControl->Display(0, "Macro Pitch Plus Move Button Click");
	g_pSqOneControl->m_bStopFlag = false;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		getDlgEditValue(FALSE);
		int nRet = 0;
		nRet = g_pSqOneControl->checkLimit(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx + m_dSetPositionIncrement, m_dSetPositionRy, m_dSetPositionRz);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx + m_dSetPositionIncrement, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx + m_dSetPositionIncrement, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			if (nRet == -2)
			{
				::AfxMessageBox("Sq-1 Stage Stoped");
				return;
			}
			else if (nRet != 0)
			{
				::AfxMessageBox("Sq-1 Timeout Error");
				return;
			}
			else
			{
				return;
			}
		}
	}
	else
	{
		g_pSqOneControl->Display(0, "Communication Error");
	}
}


void CSqOneManuaDlg::OnBnClickedBtnSqonemanuslPitchminus()
{
	g_pSqOneControl->Display(0, "Macro Pitch Minus Move Button Click");
	g_pSqOneControl->m_bStopFlag = false;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		getDlgEditValue(FALSE);
		int nRet = 0;
		nRet = g_pSqOneControl->checkLimit(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx - m_dSetPositionIncrement, m_dSetPositionRy, m_dSetPositionRz);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx - m_dSetPositionIncrement, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx - m_dSetPositionIncrement, m_dSetPositionRy, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			if (nRet == -2)
			{
				::AfxMessageBox("Sq-1 Stage Stoped");
				return;
			}
			else if (nRet != 0)
			{
				::AfxMessageBox("Sq-1 Timeout Error");
				return;
			}
			else
			{
				return;
			}
		}
	}
	else
	{
		g_pSqOneControl->Display(0, "Communication Error");
	}
}


void CSqOneManuaDlg::OnBnClickedBtnSqonemanuslYawplus()
{
	g_pSqOneControl->Display(0, "Macro Yaw Plus Move Button Click");
	g_pSqOneControl->m_bStopFlag = false;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		getDlgEditValue(FALSE);
		int nRet = 0;
		nRet = g_pSqOneControl->checkLimit(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy + m_dSetPositionIncrement, m_dSetPositionRz);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy + m_dSetPositionIncrement, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy + m_dSetPositionIncrement, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			if (nRet == -2)
			{
				::AfxMessageBox("Sq-1 Stage Stoped");
				return;
			}
			else if (nRet != 0)
			{
				::AfxMessageBox("Sq-1 Timeout Error");
				return;
			}
			else
			{
				return;
			}
		}
	}
	else
	{
		g_pSqOneControl->Display(0, "Communication Error");
	}
}


void CSqOneManuaDlg::OnBnClickedBtnSqonemanuslYawminus()
{
	g_pSqOneControl->Display(0, "Macro Yaw Minus Move Button Click");
	g_pSqOneControl->m_bStopFlag = false;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		getDlgEditValue(FALSE);
		int nRet = 0;
		nRet = g_pSqOneControl->checkLimit(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy - m_dSetPositionIncrement, m_dSetPositionRz);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy - m_dSetPositionIncrement, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy - m_dSetPositionIncrement, m_dSetPositionRz, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			if (nRet == -2)
			{
				::AfxMessageBox("Sq-1 Stage Stoped");
				return;
			}
			else if (nRet != 0)
			{
				::AfxMessageBox("Sq-1 Timeout Error");
				return;
			}
			else
			{
				return;
			}
		}
	}
	else
	{
		g_pSqOneControl->Display(0, "Communication Error");
	}
}


void CSqOneManuaDlg::OnBnClickedBtnSqonemanuslRollplus()
{
	g_pSqOneControl->Display(0, "Macro Roll Plus Move Button Click");
	g_pSqOneControl->m_bStopFlag = false;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		getDlgEditValue(FALSE);
		int nRet = 0;
		nRet = g_pSqOneControl->checkLimit(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz + m_dSetPositionIncrement);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz + m_dSetPositionIncrement, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz + m_dSetPositionIncrement, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			if (nRet == -2)
			{
				::AfxMessageBox("Sq-1 Stage Stoped");
				return;
			}
			else if (nRet != 0)
			{
				::AfxMessageBox("Sq-1 Timeout Error");
				return;
			}
			else
			{
				return;
			}
		}
	}
	else
	{
		g_pSqOneControl->Display(0, "Communication Error");
	}
}


void CSqOneManuaDlg::OnBnClickedBtnSqonemanuslRollminus()
{
	g_pSqOneControl->Display(0, "Macro Roll Minus Move Button Click");
	g_pSqOneControl->m_bStopFlag = false;
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		getDlgEditValue(FALSE);
		int nRet = 0;
		nRet = g_pSqOneControl->checkLimit(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz - m_dSetPositionIncrement);
		if (nRet == 0)
		{
			CString strCommandlog = "";
			strCommandlog.Format(_T("%f, %f,%f,%f,%f,%f,%f,%f,%f"), m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz - m_dSetPositionIncrement, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			g_pSqOneControl->Display(0, strCommandlog);
			nRet = g_pSqOneControl->waitUntilMoveComplete(m_dSetPositionX, m_dSetPositionY, m_dSetPositionZ, m_dSetPositionRx, m_dSetPositionRy, m_dSetPositionRz - m_dSetPositionIncrement, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
			if (nRet == -2)
			{
				::AfxMessageBox("Sq-1 Stage Stoped");
				return;
			}
			else if (nRet != 0)
			{
				::AfxMessageBox("Sq-1 Timeout Error");
				return;
			}
			else
			{
				return;
			}
		}
	}
	else
	{
		g_pSqOneControl->Display(0, "Communication Error");
	}
}


void CSqOneManuaDlg::OnBnClickedBtnSqmanualMove2()
{
	//GetDlgItem(IDC_SQONEMANUAL_CHECK_MACRO)->EnableWindow(true);
	g_pSqOneControl->sendSetCommand(0, 0, 0, 0, 0, 0, m_dSetPositionCx, m_dSetPositionCy, m_dSetPositionCz);
}


void CSqOneManuaDlg::OnBnClickedCheckSqmanualMacro()
{
	if (IsDlgButtonChecked(IDC_SQONEMANUAL_CHECK_MACRO) == BST_CHECKED)
	{
		setMacroEditWindow(true);
		setMacroButtonWindow(false);
		((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_MOVEUNTILCOMPLETE))->EnableWindow(true);
		SetDlgItemTextA(IDC_SQONEMANUAL_EDIT_HORIZONTAL, m_strHorizontal);
		SetDlgItemTextA(IDC_SQONEMANUAL_EDIT_VERTICAL, m_strVertical);
		SetDlgItemTextA(IDC_SQONEMANUAL_EDIT_BEAM, m_strBeam);
		SetDlgItemTextA(IDC_SQONEMANUAL_EDIT_PITCH, m_strPitch);
		SetDlgItemTextA(IDC_SQONEMANUAL_EDIT_YAW, m_strYaw);
		SetDlgItemTextA(IDC_SQONEMANUAL_EDIT_ROLL, m_strRoll);
	}
	else
	{
		setMacroEditWindow(false);
		setMacroButtonWindow(true);
		((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_MOVEUNTILCOMPLETE))->EnableWindow(false);
	}
}

void CSqOneManuaDlg::setMacroButtonWindow(bool state)
{
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_XPLUS))->EnableWindow(state);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_XMINUS))->EnableWindow(state);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_YPLUS))->EnableWindow(state);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_YMINUS))->EnableWindow(state);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_ZPLUS))->EnableWindow(state);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_ZMINUS))->EnableWindow(state);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_PITCHPLUS))->EnableWindow(state);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_PITCHMINUS))->EnableWindow(state);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_YAWPLUS))->EnableWindow(state);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_YAWMINUS))->EnableWindow(state);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_ROLLPLUS))->EnableWindow(state);
	((CButton*)GetDlgItem(IDC_SQONEMANUAL_BUTTON_ROLLMINUS))->EnableWindow(state);
	GetDlgItem(IDC_SQONEMANUAL_EDIT_INCREMENTAL)->EnableWindow(state);
}

void CSqOneManuaDlg::setMacroEditWindow(bool state)
{
	GetDlgItem(IDC_SQONEMANUAL_EDIT_HORIZONTAL)->EnableWindow(state);
	GetDlgItem(IDC_SQONEMANUAL_EDIT_VERTICAL)->EnableWindow(state);
	GetDlgItem(IDC_SQONEMANUAL_EDIT_BEAM)->EnableWindow(state);
	GetDlgItem(IDC_SQONEMANUAL_EDIT_PITCH)->EnableWindow(state);
	GetDlgItem(IDC_SQONEMANUAL_EDIT_YAW)->EnableWindow(state);
	GetDlgItem(IDC_SQONEMANUAL_EDIT_ROLL)->EnableWindow(state);
	GetDlgItem(IDC_SQONEMANUAL_BUTTON_MOVEUNTILCOMPLETE)->EnableWindow(state);
}


void CSqOneManuaDlg::OnDestroy()
{
	CDialogEx::OnDestroy();


}


BOOL CSqOneManuaDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CSqOneManuaDlg::OnBnClickedSqonemanualButtonSave()
{
	if (g_pSqOneControl->m_bConnected == TRUE && g_pSqOneDB->m_isLoaded == TRUE)
	{
		g_pSqOneDB->SetDlgItemTextA(IDC_SQONEDB_EDIT_NAME, "");
		g_pSqOneDB->SetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, m_strHorizontal);
		g_pSqOneDB->SetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, m_strVertical);
		g_pSqOneDB->SetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, m_strBeam);
		g_pSqOneDB->SetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, m_strPitch);
		g_pSqOneDB->SetDlgItemTextA(IDC_SQONEDB_EDIT_YAW, m_strYaw);
		g_pSqOneDB->SetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, m_strRoll);

		g_pSqOneDB->addSQONEStagePosition();
	}
	else
	{
		AfxMessageBox("통신 상태를 확인하세요");
	}
}
