#pragma once

#include "stdafx.h"
#include "CSqOne.h"
//#include "Include.h"


CSqOne::CSqOne()
{
	m_hParsingEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_IpAddr = _T("127.0.0.1");
	m_Port = 50122;


	InitialData.X = -0.262100;
	InitialData.Y = -2.978900;
	InitialData.Z = 5.950100;

	InitialData.Rx = 0.881500;
	InitialData.Ry = 0.600400;
	InitialData.Rz = 0.000200;

	InitialData.Cx = -983.0;
	InitialData.Cy = 450.0;
	InitialData.Cz = 1600.0;

}

CSqOne::~CSqOne()
{
	Close();
	CloseHandle(m_hParsingEvent);
}

void CSqOne::Close()
{
	CloseTcpIpSocket();
}

void CSqOne::Stop()
{
	SendStopCommand();
}

void CSqOne::EmergencyStop()
{
}



int CSqOne::Open()
{
	int nRet = 0;

	nRet = OpenTcpIpSocket((LPSTR)(LPCTSTR)m_IpAddr, m_Port, FALSE);

	if (nRet != 0)
	{		
		nRet = -1;
	}
	else
	{
		nRet = 0;
	}
	return nRet;
}


int	CSqOne::ReceiveData(char *lParam, int nLength)
{
	int nRet = 0;

	nRet = MakeDataSet(lParam, nLength);

	return nRet;
}


void CSqOne::SendGetCommand()
{
	m_DataSetIndex = 0;

	if (m_bConnected == 1)
	{
		char *text;
		ResetEvent(m_hParsingEvent);
		text = _T("Get\r\n");
		Send(text, 0, 1);
	}
}

void CSqOne::SendSetCommand(double x, double y, double z, double rx, double ry, double rz, double cx, double cy, double cz)
{
	m_DataSetIndex = 0;

	if (m_bConnected)
	{
		CString sendtext;
				//자릿수 제한을 해야 하나? ihlee
		sendtext.Format(_T("Set,%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n"), x, y, z, rx, ry, rz, cx, cy, cz);
		m_SendString = sendtext;
		char* text = (LPSTR)(LPCTSTR)sendtext;
		ResetEvent(m_hParsingEvent);
		Send(text, 100, 1);
	}
}

void CSqOne::SendStopCommand()
{
	if (m_bConnected)
	{
		char *text;
		text = _T("Stop\r\n");
		Send(text, 100, 1);
		usleep(100);
	}
}


int CSqOne::ParseData(char * bufrecv, int nLen)
{
	//변수 초기화
	char *token = NULL, *contex = NULL;
	char chrPackData[200] = "";
	char ttoken[540] = "";
	char *buffrecv = NULL;

	//결과 구조 초기화maxdata
	int bParseError = 0;

	memcpy(ttoken, bufrecv, nLen); //bufrecv 주소 복제로 인한 값 변경 방지

	m_ReceiveString = ttoken;

	token = ttoken;

	int nmNum = 0;

	while (token != NULL)
	{
		token = strstr(token + 1, ",");  // den 포인터에 1을 더하여 e부터 검색
		nmNum++;

	}

	if (strstr(ttoken, "\r\n") != NULL) // String Handling
	{
		token = strtok_s(ttoken, "\r\n", &contex); //문자열 

		if (nmNum == 3)  // stop command reply
		{
			//return 0;
		}
		else if (nmNum == 8) // set command reply
		{
			//return 0;
		}
		else if (nmNum == 12)
		{
			//memset(&SqData, 0, sizeof(SQ_DATA) + 1);// 이거?  괜찮음??? ihlee
			//testtt++;
			if (token != NULL)
			{
				char *ptrPack = NULL, *ptrEnd = NULL;
				strcpy_s(chrPackData, token);
				ptrPack = &chrPackData[0];

				//2. X Position 확인.
				double dX = strtod(ptrPack, &ptrEnd);
				if (ptrEnd != NULL)
				{
					SqData.X = dX;// +testtt;
					ptrPack = ptrEnd;
					if (*ptrPack == ',')
						ptrPack++;
					else
					{
						bParseError = 1;
					}
				}
				else
				{
					bParseError = 1;
				}

				//3. Y Position 확인.
				double dY = strtod(ptrPack, &ptrEnd);
				if (ptrEnd != NULL)
				{
					SqData.Y = dY;
					ptrPack = ptrEnd;
					if (*ptrPack == ',')
						ptrPack++;
					else
					{
						bParseError = 1;
					}
				}
				else
				{
					bParseError = 1;
				}
				//3. Y Position 확인.
				double dZ = strtod(ptrPack, &ptrEnd);
				if (ptrEnd != NULL)
				{
					SqData.Z = dZ;
					ptrPack = ptrEnd;
					if (*ptrPack == ',')
						ptrPack++;
					else
					{
						bParseError = 1;
					}
				}
				else
				{
					bParseError = 1;
				}
				//3. Y Position 확인.
				double dRx = strtod(ptrPack, &ptrEnd);
				if (ptrEnd != NULL)
				{
					SqData.Rx = dRx;
					ptrPack = ptrEnd;
					if (*ptrPack == ',')
						ptrPack++;
					else
					{
						bParseError = 1;
					}
				}
				else
				{
					bParseError = 1;
				}
				//3. Y Position 확인.
				double dRy = strtod(ptrPack, &ptrEnd);
				if (ptrEnd != NULL)
				{
					SqData.Ry = dRy;
					ptrPack = ptrEnd;
					if (*ptrPack == ',')
						ptrPack++;
					else
					{
						bParseError = 1;
					}
				}
				else
				{
					bParseError = 1;
				}
				//3. Y Position 확인.
				double dRz = strtod(ptrPack, &ptrEnd);
				if (ptrEnd != NULL)
				{
					SqData.Rz = dRz;
					ptrPack = ptrEnd;
					if (*ptrPack == ',')
						ptrPack++;
					else
					{
						bParseError = 1;
					}
				}
				else
				{
					bParseError = 1;
				}
				//3. Y Position 확인.
				double dCx = strtod(ptrPack, &ptrEnd);
				if (ptrEnd != NULL)
				{
					SqData.Cx = dCx;
					ptrPack = ptrEnd;
					if (*ptrPack == ',')
						ptrPack++;
					else
					{
						bParseError = 1;
					}
				}
				else
				{
					bParseError = 1;
				}
				//3. Y Position 확인.
				double dCy = strtod(ptrPack, &ptrEnd);
				if (ptrEnd != NULL)
				{
					SqData.Cy = dCy;
					ptrPack = ptrEnd;
					if (*ptrPack == ',')
						ptrPack++;
					else
					{
						bParseError = 1;
					}
				}
				else
				{
					bParseError = 1;
				}

				//4. Angle 확인.
				double dCz = strtod(ptrPack, &ptrEnd);
				if (ptrEnd != NULL)
				{
					SqData.Cz = dCz;
					ptrPack = ptrEnd;
					if (*ptrPack == ',')
						ptrPack++;
					else
					{
						bParseError = 1;
					}
				}
				else
				{
					bParseError = 1;
				}

				double dFEb = strtod(ptrPack, &ptrEnd);
				if (ptrEnd != NULL)
				{
					SqData.TargetExceedsRange = (int)dFEb;
					ptrPack = ptrEnd;
					if (*ptrPack == ',')
						ptrPack++;
					else
					{
						bParseError = 1;
					}
				}
				else
				{
					bParseError = 1;
				}

				double dSSb = strtod(ptrPack, &ptrEnd);
				if (ptrEnd != NULL)
				{
					SqData.TriSphereStatus = (int)dSSb;
					ptrPack = ptrEnd;
					if (*ptrPack == ',')
						ptrPack++;
					else
					{
						bParseError = 1;
					}
				}
				else
				{
					bParseError = 1;
				}

				double dMCb = strtod(ptrPack, &ptrEnd);
				if (ptrEnd != NULL)
				{

					SqData.MoveComplete = (int)dMCb;

					if (SqData.MoveComplete == 1)
					{
						//cbeam_dlg->m_radio_movecompletebit.SetCheck(true);
						//cbeam_dlg->m_icon_mc_bit.SetIcon(cbeam_dlg->m_LedIcon[2]);
					}
					else
					{
						//cbeam_dlg->m_radio_movecompletebit.SetCheck(false);
						//cbeam_dlg->m_icon_mc_bit.SetIcon(cbeam_dlg->m_LedIcon[1]);
					}
					ptrPack = ptrEnd;
					if (*ptrPack != 0)
					{
						bParseError = 1;
					}
				}
				else
				{
					bParseError = 1;
				}
				// 여기까지 무사히 넘어오면 데이터는 성공적인 것임.
				SetEvent(m_hParsingEvent);
			}

		}

	}
	else
	{
		return 1;
	}
	return 0;	// success

}




int CSqOne::MakeDataSet(char *lParam, int nLength)
{
	CString ttemp = "";
	int nRet = 0, j = 0, ev = 0, i = 0;
	for (i = 0; i < nLength; i++)
	{
		storedbuff[i - j + m_DataSetIndex] = lParam[i];
		if (lParam[i] == '\n')
		{
			nRet = ParseData(storedbuff, sizeof(char)*(i - j + m_DataSetIndex + 1));
			memset(storedbuff, '\0', sizeof(char)*(i - j + m_DataSetIndex + 1));

			j = i + 1;
			m_DataSetIndex = 0;
		}
	}
	m_DataSetIndex += i - j;
	return nRet;
}

int CSqOne::GetUntilReceiveData()
{
	int  ret = 0;;

	SendGetCommand();

	ret = WaitEvent(m_hParsingEvent, 10000);

	if (ret == 0)
	{
		ResetEvent(m_hParsingEvent); //Event Object Nonsignaled 상태로 변환
		ret = 0;

	}
	else
	{
		ret = -1;
	}

	return ret;

}

int CSqOne::SetUntilInposition(double x, double y, double z, double rx, double ry, double rz, double cx, double cy, double cz)
{
	int nRet = 0;;
	int tmpRet = 0;

	SendSetCommand(x, y, z, rx, ry, rz, cx, cy, cz);

	int count = 0;

	do//wait move complete bit , double check
	{
		tmpRet = GetUntilReceiveData();
		if (tmpRet == -1) 
		{
			nRet = -1;
			goto FUNC_ERROR;
		}
		WaitSec(1);
		count++;

		if (count > 100) 
		{
			nRet = -2;
			goto FUNC_ERROR;
		}

	} while (!SqData.MoveComplete);

	return nRet;

FUNC_ERROR:
	return nRet;

}

int CSqOne::WaitReceiveEventThread()
{
	int		nRet = 0;
	int		i = 0;
	char*	pInBuf = new char[m_nReceiveMaxBufferSize];
	char*	pOutBuf = new char[m_nReceiveMaxBufferSize];
	int		total = 0;


	int		nCondition = 0;
	unsigned long	tmp = 0;
	struct timeval timeout;
	fd_set fds;

	SOCKET	tmpSocket = INVALID_SOCKET;
	tmpSocket = m_Socket;

	TRACE("TCPIP Receive Thread Start..\n");

	memset(pInBuf, '\0', m_nReceiveMaxBufferSize);
	while (!m_bKillReceiveThread)
	{
		m_bConnected = TRUE;
		timeout.tv_sec = 0;
		timeout.tv_usec = 100;
		FD_ZERO(&fds);
		FD_SET(m_Socket, &fds);
		m_nSocketStatus = select(sizeof(fds) * 8, &fds, NULL, NULL, &timeout);

		if (m_nSocketStatus == -1)
		{
			m_bKillReceiveThread = TRUE;
			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);
		}

		if (tmpSocket != INVALID_SOCKET)
		{
			memset(pOutBuf, '\0', m_nReceiveMaxBufferSize);
			int bytesReceived = recv(tmpSocket, pInBuf, m_nReceiveMaxBufferSize, NULL);

			if (m_bKillReceiveThread)
			{
				break;
			}

			SetEvent(m_hSocketEvent);
			BOOL isParsing = FALSE;
			if (bytesReceived > 0)
			{
				memcpy(pOutBuf, pInBuf, sizeof(byte) * bytesReceived);
				ReceiveData(pOutBuf, bytesReceived);
			}
			else
			{
				// 아랫 부분 있으면 통신 THREAD 종료 지연됨, 메모리 릭 발생
				//bytesReceived==0 : gracefully closed 접속 해제 상태ihlee 
				memset(pOutBuf, '\0', m_nReceiveMaxBufferSize);
				memset(pInBuf, '\0', m_nReceiveMaxBufferSize);
				m_bKillReceiveThread = TRUE;
				m_bConnected = FALSE;
				nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
				m_nRet = nRet;
			}
		}
		else
		{
			m_Socket = INVALID_SOCKET;
			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);
		}
		usleep(50);
	}

	delete[] pInBuf;
	pInBuf = NULL;
	delete[] pOutBuf;
	pOutBuf = NULL;
	TRACE("TCPIP Receive Thread End..\n");

	return nRet;
}